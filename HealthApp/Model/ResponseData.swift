//
//  ResponseData.swift
//
//
//  Created by Apple on 1/27/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

protocol ResponseData {
    var status: Bool { get  }
    var message: String? { get  }
}

struct CommonResponseData: ResponseData, Codable {
    
    var status: Bool
    
    var message: String?
    
    
}
struct MedResponseData: ResponseData, Codable {
    
    var status: Bool
    
    var message: String?
    //var data: [String?]
    
}

struct ImgResponseData: ResponseData, Codable {
    
    var status: Bool
    var message: String?
    var data: [String]?
}

struct PrescData: ResponseData, Codable {
    
    var status: Bool
    var message: String?
    var data: Int?
}


