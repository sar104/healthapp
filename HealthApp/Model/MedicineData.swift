//
//  MedicineData.swift
//  HealthApp
//
//  Created by Apple on 09/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct MedicineData: Codable {
    
    var id: Int?
    var medicine_name: String?
    var compositions: String?
    var packaging_of_medicines: String?
    var product_use: String?
    var type_of_medicine: String?
    var quantity: Int = 0
    var type: String = "PM"
    
    private enum CodingKeys: String, CodingKey {
        case id, medicine_name, compositions, packaging_of_medicines, product_use, type_of_medicine
    }
    
    init() {
        
    }
    
    init(coreDataMed: Medicines) {
        
        self.medicine_name = coreDataMed.medicine_name?.components(separatedBy: "\n").joined() ?? ""
        self.packaging_of_medicines = coreDataMed.medicine_package ?? ""
        self.type_of_medicine = coreDataMed.medicine_type ?? ""
    }
    
    static func getMedicineData(_ array: [Medicines])-> [MedicineData] {
        
        var arrMedData: [MedicineData] = []
        for medicine in array {
            
            arrMedData.append(MedicineData(coreDataMed: medicine))
        }
        return arrMedData
    }
    
    static func getDrugsData(_ array: [MedicineData])-> [DrugData] {
        
        var drugData: [DrugData] = []
        
        
        for med in array {
        
            let medicineName = MedicineName(medicine_name: med.medicine_name, suggested_name: "", medicine_type: med.type)
            let drug = DrugData(medicine_name: medicineName, quantity: med.quantity, unit: med.type_of_medicine)
            drugData.append(drug)
            
        }
        return drugData
    }
}

struct Medicine: Codable {
    
    var medicine_name: String?
    var quantity: Int?
    var unit: String?
}

struct DrugData: Codable {
    
    var medicine_name: MedicineName?
    var quantity: Int?
    var unit: String?
}

struct CDrugData: Codable {
    
    var medicine_name: MedicineName?
    var quantity: Int?
    var unit: String?
    var available_status: String?
}

struct MedicineName: Codable {
    
    var medicine_name: String?
    var suggested_name: String?
    var medicine_type: String?
}
