//
//  ArticleData.swift
//  HealthApp
//
//  Created by Apple on 01/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation


struct ArticleData: Codable {
    
    var author: String?
    var title: String?
    var description: String?
    var url: String?
    var urlToImage: String?
    var publishedAt: String?
    var content: String?
}

struct NewsData {
    
    var datePublished: String?
    var description: String?
    var name: String?
    var url: String?
    var image: [String:Any]?
    var provider: [String:Any]?
    var imgurl: String?
    
    mutating func parseData(params: [String: Any]){
      
        if let datePublished = params["datePublished"] as? String {
            self.datePublished = datePublished
        }
        if let description = params["description"] as? String {
            self.description = description
        }
        if let name = params["name"] as? String {
            self.name = name
        }
        if let url = params["url"] as? String {
            self.url = url
        }
        if let image = params["image"] as? [String:Any] {
            self.image = image
            if let thumbnail = image["thumbnail"] as? [String:Any] {
                self.imgurl = thumbnail["contentUrl"] as! String
            }
        }
        if let provider = params["provider"] as? [String:Any] {
            self.provider = provider
        }
    }
}

/*
{
           datePublished = "2020-09-01T12:19:00.0000000Z";
           description = "People living with type 2 diabetes who have been diagnosed with the condition in the last six years will be considered for the scheme. After a few months on the shakes and soups,";
           image =             {
               thumbnail =                 {
                   contentUrl = "https://www.bing.com/th?id=ON.17CAD0A01F3F2BFB06452E121DB3EE20&pid=News";
                   height = 393;
                   width = 700;
               };
           };
           name = "Soup-and-shake diet offered on NHS to fight diabetes";
           provider =             (
                               {
                   "_type" = Organization;
                   image =                     {
                       thumbnail =                         {
                           contentUrl = "https://www.bing.com/th?id=AR_b639c1691c4fa767d85fd87b7042f9e6&pid=news";
                       };
                   };
                   name = BBC;
               }
           );
           url = "https://www.bbc.co.uk/news/health-53983095";
       }
**/
/*
 {
         "source":{
            "id":null,
            "name":"ESPN"
         },
         "author":"Michael Rothstein",
         "title":"Anonymous kidney donation gives former NFL lineman Armonty Bryant new life",
         "description":"Armonty Bryant has a new kidney and a new lease on life. But for this former NFL player to completely depend on others for his life, it was something he needed to learn.",
         "url":"https://www.espn.com/nfl/story/_/id/29778813/anonymous-kidney-donation-gives-former-nfl-lineman-armonty-bryant-new-life",
         "urlToImage":"https://a1.espncdn.com/combiner/i?img=%2Fphoto%2F2020%2F0831%2Fr738836_900x942cc.jpg",
         "publishedAt":"2020-09-01T11:50:12Z",
         "content":"Armonty Bryant was sitting in his dining room when a number he didn't recognize popped up on his phone. The former NFL defensive end doesn't usually answer those calls but he was feeling good this da… [+14310 chars]"
      }
 **/
