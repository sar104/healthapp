//
//  Location.swift
//  HealthApp
//
//  Created by Apple on 09/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct Location {
    
    var latitude: Double
    var longitude: Double
}
