//
//  Notification.swift
//  HealthApp
//
//  Created by Apple on 24/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UserNotifications

struct LocalNotification {
    var id:String
    var title:String
    var datetime:DateComponents
    
    
}
