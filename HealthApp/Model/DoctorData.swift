//
//  DoctorData.swift
//  HealthApp
//
//  Created by Apple on 05/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct DoctorData: Codable {
    
    /*
     {
         "id": 111,
         "doctor_name": "Suraj",
         "mobile_no": 9657136379,
         "email": "asd@asdf.sdd",
         "profile_image": "1bSTjJuxtYQXV9Zrc1lD.jpg"
     }
     {
         address = ", Lokhand Bazar, Gotane Wada, Nashik, Maharashtra , India";
         "address_id" = null;
         "address_type" = 459;
         "avg_rating" = 5;
         city = NASHIK;
         "clinic_lab_hospital_name" = "Nashik Fitness Center";
         email = "abcd@gmail.com";
         "gst_no" = jfjfjfifjfjfjfj;
         id = 308;
         latitude = "19.9974540000000011730207916116341948509";
         longitude = "73.789803000000006250047590583562850952";
         "mobile_no" = 8390287339;
         pincode = 422001;
         "registration_no" = 1;
         "shop_image" = "BfqEqCutaXm21aMhyQpG.jpg";
         state = MAHARASHTRA;
         "user_name" = "Mr. Mayuri";
     }
     **/
    var id: Int?
    var doctor_name: String?
    var mobile_no: Int?
    var email: String?
    var shop_image: String?
    var user_name: String?
    var favourite: Bool?
}
/*
 {
     "status": true,
     "message": "Record found.",
     "data": [
         {
             "user_name": "Surajj",
             "mobile_no": 1010101010,
             "address_id": "11",
             "shop_image": "zzh5z41chygvevhh2e.jpg",
             "clinic_lab_hospital_name": "Test hospital1",
             "email": "sr1@gmail.com",
             "registration_no": "1234567891",
             "gst_no": "1212121",
             "address": "jail road,Nashik road",
             "state": "WEST BENGAL",
             "city": "Bagnan - II",
             "avg_rating": 2.5
         }
 {
     address = nasik;
     "address_id" = "-";
     "address_type" = "-";
     "avg_rating" = 0;
     city = "-";
     "clinic_lab_hospital_name" = bakshi;
     email = "-";
     "gst_no" = "-";
     id = 415;
     latitude = "-";
     longitude = "-";
     "mobile_no" = 9999999992;
     pincode = "-";
     "registration_no" = "-";
     "shop_image" = "-";
     state = "-";
     "user_name" = bakshi;
 }
     ]
 }
 **/

struct FavDoctor: Codable {
    
    var address: String?
    var address_id: String?
    var address_type: String?
    var avg_rating: Double?
    var city: String?
    var clinic_lab_hospital_name: String?
    var email: String?
    var gst_no: String?
    var id: Int?
    var latitude: String?
    var longitude: String?
    var mobile_no: String?
    var pincode: String? = ""
    var registration_no: String?
    var shop_image: String?
    var state: String?
    var name: String?
    var user_name: String?
    var shop_opening_time: String?
    var shop_closing_time: String?
    var afternoon_opening_time: String?
    var afternoon_closing_time: String?
    var off_day: String?
    var favourite: Bool?
    var distance: Double?
    var assistant_name: String?
    var alt_contact_no: String?
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do {
            self.id = try container.decode(Int.self, forKey: .id)
            self.address = try container.decode(String.self, forKey: .address)
            self.avg_rating = try container.decode(Double.self, forKey: .avg_rating)
            self.city = try container.decode(String.self, forKey: .city)
            self.clinic_lab_hospital_name = try container.decode(String.self, forKey: .clinic_lab_hospital_name)
            self.email = try container.decode(String.self, forKey: .email)
            self.gst_no = try container.decode(String.self, forKey: .gst_no)
            self.latitude = try container.decode(String.self, forKey: .latitude)
            self.longitude = try container.decode(String.self, forKey: .longitude)
            self.mobile_no = try container.decode(String.self, forKey: .mobile_no)
            
            self.registration_no = try container.decode(String.self, forKey: .registration_no)
            self.shop_image = try container.decode(String.self, forKey: .shop_image)
            self.state = try container.decode(String.self, forKey: .state)
            self.name = try container.decodeIfPresent(String.self, forKey: .name)
            self.user_name = try container.decodeIfPresent(String.self, forKey: .user_name)
            self.shop_opening_time = try container.decodeIfPresent(String.self, forKey: .shop_opening_time)
            self.shop_closing_time = try container.decodeIfPresent(String.self, forKey: .shop_closing_time)
            self.off_day = try container.decodeIfPresent(String.self, forKey: .off_day)
            self.pincode = try container.decodeIfPresent(String.self, forKey: .pincode)
            self.favourite = try container.decodeIfPresent(Bool.self, forKey: .favourite)
            self.distance = try container.decodeIfPresent(Double.self, forKey: .distance)
            
        } catch DecodingError.typeMismatch {
            
            if self.latitude == nil {
                let value = try container.decode(Double.self, forKey: .latitude)
                self.latitude = "\(value)"
            }
            if self.longitude == nil {
                let value1 = try container.decode(Double.self, forKey: .longitude)
                self.longitude = "\(value1)"
            }
            
            //self.pincode = try container.decode(String.self, forKey: .pincode)
            let mobile_no = try container.decode(Int.self, forKey: .mobile_no)
            self.mobile_no = "\(mobile_no)"
            self.registration_no = try container.decode(String.self, forKey: .registration_no)
            self.shop_image = try container.decode(String.self, forKey: .shop_image)
            self.state = try container.decode(String.self, forKey: .state)
            self.name = try container.decodeIfPresent(String.self, forKey: .name)
            self.user_name = try container.decodeIfPresent(String.self, forKey: .user_name)
            self.shop_opening_time = try container.decodeIfPresent(String.self, forKey: .shop_opening_time)
            self.shop_closing_time = try container.decodeIfPresent(String.self, forKey: .shop_closing_time)
            self.off_day = try container.decodeIfPresent(String.self, forKey: .off_day)
            self.pincode = try container.decodeIfPresent(String.self, forKey: .pincode)
            self.favourite = try container.decodeIfPresent(Bool.self, forKey: .favourite)
            self.distance = try container.decodeIfPresent(Double.self, forKey: .distance)
        }
    }
}
struct DiagnosticData: Codable {
    
    var address: String?
    var address_id: String?
    var address_type: String?
    var avg_rating: Double?
    var city: String?
    var clinic_lab_hospital_name: String?
    var email: String?
    var gst_no: String?
    var id: Int?
    var latitude: Double?
    var longitude: Double?
    var mobile_no: Int?
    var pincode: String?
    var registration_no: String?
    var shop_image: String?
    var state: String?
    var name: String?
    var shop_opening_time: String?
    var shop_closing_time: String?
    var off_day: String?
    var distance: Double?
    var favourite: Bool?
    var delivery_status: String?
    
}
struct DetailData: Codable {
    
    var address: String?
    var address_id: String?
    var address_type: String?
    var avg_rating: Double?
    var city: String?
    var clinic_lab_hospital_name: String?
    var email: String?
    var gst_no: String?
    var id: Int?
    var latitude: Double?
    var longitude: Double?
    var mobile_no: Int?
    var pincode: String?
    var registration_no: String?
    var shop_image: String?
    var state: String?
    var name: String?
    var shop_opening_time: String?
    var shop_closing_time: String?
    var off_day: String?
    var distance: Double?
    var favourite: Bool?
    var delivery_status: String?
    var about_clinic: String?
    var afternoon_opening_time: String?
    var afternoon_closing_time: String?
    var alt_contact_no: String?
    var assistant_name: String?
    var specialization: String?
    var qualification: String?
    var clinic_lab_hospital_phone_no: String?
}
/*
 {
     "about_clinic" = trss;
     address = ", Lokhand Bazar, Gotane Wada, Nashik, Maharashtra , India";
     "address_id" = "<null>";
     "address_type" = 459;
     "afternoon_closing_time" = "<null>";
     "afternoon_opening_time" = "<null>";
     "alt_contact_no" = null;
     "assistant_name" = null;
     "avg_rating" = 0;
     city = NASHIK;
     "clinic_lab_hospital_name" = "Mayuri chemist shopp";
     "delivery_status" = Shop;
     email = "ma@gmail.comm";
     "gst_no" = "jQWrtwkwrClv2nIOwLRQ.jpg";
     id = 305;
     latitude = "19.9974540000000011730207916116341948509";
     longitude = "73.789803000000006250047590583562850952";
     "mobile_no" = 9823379677;
     name = "Ms. Mayuri";
     "off_day" = "Monday, Tuesday, Wednesday, Thursday, Friday,";
     pincode = 422001;
     qualification = "<null>";
     "registration_no" = 686866868686;
     "shop_closing_time" = "05:49 PM";
     "shop_image" = "g7a3sK3GQeNYQ7jg3Yqg.jpg";
     "shop_opening_time" = "05:49 PM";
     specialization = "Nutritionist/Dietition";
     state = MAHARASHTRA;
 };
{
    "status": true,
    "message": "Record Found.",
    "data": [
        {
            "id": 61,
            "shop_image": "62i88pwqii6kswbf3a.jpg",
            "name": "btbtttt",
            "clinic_lab_hospital_name": "fddddg",
            "email": "fff@gg.in",
            "registration_no": "gfhgf101",
            "mobile_no": 1234567890,
            "gst_no": "dsf",
            "state": "TRIPURA",
            "city": "Kakraban",
            "address": "8",
            "avg_rating": 5,
            "favourite": false
        }
    ]
}
 
 {
         address = "Nasik road";
         "address_id" = 1;
         "address_type " = "<null>";
         "alt_contact_no" = 9856232010;
         "assistant_name" = "Dr.ashwini";
         "avg_rating" = "3.75";
         city = NASHIK;
         "clinic_lab_hospital_name" = "Ashoka Hspital";
         "delivery_status" = "";
         distance = "6.181074518817847973650714266113936901";
         email = "asd@asdf.sdd";
         favourite = 0;
         "gst_no" = dfgh6546dfg;
         id = 111;
         latitude = "19.944244000000001193484422401525080204";
         longitude = "73.797240999999999644387571606785058975";
         "mobile_no" = 9657136379;
         name = Suraj;
         "off_day" = sun;
         pincode = "<null>";
         "registration_no" = 54fg654;
         "shop_closing_time" = 20;
         "shop_image" = "Jellyfish.png";
         "shop_opening_time" = 10;
         state = MAHARASTRA;
     }
**/
/*
 ["message": Record Found., "data": {
     "about_clinic" = "well experienced staff in this clinic. high educated staff also there";
     address = "Modkeshwar Nagar Rd, Mhasoba Nagar, Modkeshwar Nagar, Nashik, Maharashtra , India";
     "address_id" = null;
     "address_type" = "Modkeshwar Nagar Road";
     "afternoon_closing_time" = "09:22  PM";
     "afternoon_opening_time" = "04:22  PM";
     "alt_contact_no" = "7878787878,7979797979,";
     "assistant_name" = "Dr.  assistant 1 for main branch , assistant 2 for main branch";
     "avg_rating" = "3.5625";
     city = NASHIK;
     "clinic_lab_hospital_name" = "Hoh Test Shop";
     "delivery_status" = No;
     email = "sanketkshirsagar86@gmail.com";
     "gst_no" = 123451234512345;
     id = 219;
     latitude = "19.9724749999999993121946317842230200767";
     longitude = "73.753838000000001784428604878485202789";
     "mobile_no" = 9766854743;
     name = "Dr.  Hoh Test Doctor";
     "off_day" = Sunday;
     pincode = 422009;
     qualification = MD;
     "registration_no" = 158158158;
     "shop_closing_time" = "12:22  PM";
     "shop_image" = "IBH9DiS4FJI2WgqIxp02.jpg";
     "shop_opening_time" = "10:0 AM";
     specialization = Neurologist;
     state = MAHARASHTRA;
 }, "status": 1]

 **/

struct AppointmentData: Codable {
    
    var id: Int?
    var appointment_date: String?
    var appointment_for: String?
    var appointment_status: String?
    var appointment_time: String?
    var doctor_id: Int?
    var patient_age: String?
    var patient_location: String?
    var patient_mobile: String?
    var patient_name: String
    var notes: String?
    
    func getAptDate() -> Date {
        
        let aptDate = self.appointment_date!.toDate(format: df_dd_MM_yyyy)
        
        let month = aptDate.get(.month)
        let year = aptDate.get(.year)
        let day = aptDate.get(.day)
        
        let arrTime = self.appointment_time!.components(separatedBy: ":")
        let hour = Int(arrTime[0])!
        let arr = arrTime[1].components(separatedBy: " ")
        let min = Int(arr[0])!
        let time = self.appointment_time!.toDate(format: "h:mm a")
        let date = Date.init(year: year, month: month, day: day, hour: hour, minute: min, second: 0)
        
        return date
    }
    
    func getReason() -> String {
        
        let data = self.appointment_for!.data(using: .utf8)!
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
            {
               print(json) // use the json here
                 
                return json["appointment_for"] as! String
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(error)
        }
        /*
        do {
            let meddata = try JSONSerialization.data(withJSONObject: self.appointment_for!, options: .prettyPrinted)
        
        let json = try JSONDecoder().decode(ApptFor.self, from: meddata)
            
            return json.appointment_for ?? ""
            
        } catch let err {
            return ""
        }*/
        return ""
    }
    
    func getPrescrID() -> String {
        
        let data = self.appointment_for!.data(using: .utf8)!
        do {
            if let json = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
            {
               print(json) // use the json here
                 
                return json["prescription_id"] as! String
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(error)
        }
       
        return ""
    }
    /*
     {
         "appointment_date" = "0000-00-00";
         "appointment_for" = "{\"appointment_for\":\"Neck pain and leg pain\",\"prescription_id\":\"388\"}";
         "appointment_status" = "PAYMENT DONE";
         "appointment_time" = "05:00 PM";
         "doctor_id" = 219;
         id = 22;
         "patient_age" = "31.9";
         "patient_location" = "- 422010";
         "patient_mobile" = 9657618996;
         "patient_name" = "Ms. HOH Test Customer";
     }
     **/
    /*
     ["status": 1, "message": Record Found., "data": {
         "appointment_date" = "0000-00-00";
         "appointment_for" = "{\"prescription_id\":\"null\",\"appointment_for\":\"Hair test\"}";
         "appointment_status" = NEW;
         "appointment_time" = "04:45 PM";
         "doctor_name" = "Dr.  Hoh Test Doctor";
         email = "sanketkshirsagar86@gmail.com";
         id = 155;
         "mobile_no" = 9766854743;
         "patient_age" = 28;
         "patient_location" = Test;
         "patient_mobile" = 9850059684;
         "patient_name" = "Miss Sarika";
     }]
     **/
}

struct ApptFor: Codable {
    var appointment_for: String?
    var prescription_id: Int?
}
