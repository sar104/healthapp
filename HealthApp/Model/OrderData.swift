//
//  OrderData.swift
//  HealthApp
//
//  Created by Apple on 05/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct OrderData: Codable {
    
    /*
     {
         "order_date": "2020-03-05",
         "total_orders": 1
     }
     **/
    var order_date: String?
    var total_orders: Int?
}
/*
    {
        address = "-";
        "auto_generated_date" = "2020-08-20 19:01:47";
        chemist = "Mr. Shsh";
        "chemist_id" = 403;
        "chemist_shop_name" = shdhd;
        "customer_address" = test;
        "delivery_type" = Delivery;
        description = "-";
        "doctor_name" = pqrs;
        "estimated_cost" = "<null>";
        "estimated_date" = "21-08-2020";
        "flat_house_no" = 401;
        id = 352;
        "mobile_no" = 9850059684;
        "order_details" =             (
                            {
                "available_status" = "<null>";
                "medicine_name" = "{\"medicine_name\":\"Paramol 500mg Tablet\",\"suggested_name\":\"\",\"medicine_type\":\"PM\"}";
                "order_id" = 352;
                quantity = 1;
                unit = strip;
            }
        );
        pincode = 422101;
        "prescription_image" = "JOvXIlbM0kzESVDfZX4Z.jpeg";
        "prescription_type" = Prescribed;
        "recipient_name" = "{\"rec_id\":\"401\",\"rec_name\":\"Miss Sarika\",\"rec_mobile\":\"9850059684\"}";
        "reject_reason" = "<null>";
        slot = "09:00 AM To 12:00 PM";
        status = Pending;
        "user_name" = "Miss Sarika";
    }
 
 
    **/

struct CustOrderData: Codable {
    
    var address: String?
    var auto_generated_date: String?
    var chemist: String?
    var chemist_id: Int?
    var chemist_shop_name: String?
    var customer_address: String?
    var delivery_type: String?
    var description: String?
    var doctor_name: String?
    var estimated_cost: Int?
    var estimated_date: String?
    var flat_house_no: String?
    var id: Int?
    var mobile_no: Int?
    var pincode: Int?
    var prescription_image: String?
    var prescription_type: String?
    var recipient_name: String?
    var reject_reason: String?
    var slot: String?
    var status: String?
    var user_name: String?
    var order_details: [OrderDetails]?
    
    
    func getRecepient() -> String {
        
        do {
            //let data = try JSONSerialization.data(withJSONObject: self.recipient_name, options: [])
            guard let data = self.recipient_name?.data(using: .utf8) else { return "" }
            let recdata = try JSONDecoder().decode(RecepientData.self, from: data)
            
            return recdata.rec_name ?? ""
                
        } catch let err {
            
        }
        return ""
    }
    
    func getRecepientData() -> RecipientData? {
        
        do {
            //let data = try JSONSerialization.data(withJSONObject: self.recipient_name, options: [])
            guard let data = self.recipient_name?.data(using: .utf8) else { return nil }
            let recdata = try JSONDecoder().decode(RecipientData.self, from: data)
            
            return recdata
                
        } catch let err {
            
        }
        return nil
    }
    
    func getDeliveryAddress() -> String {
        
        let house = "Flat/House No - " + self.flat_house_no! + "\n"
        let address = "Address - " + self.customer_address! + "\n"
        let pin = "Pincode - " + String(self.pincode!)
        
        return house + address + pin
    }
    
    func getMedOrderData() -> MedicineOrderData {
        
        var medOrderData = MedicineOrderData()
        medOrderData.chemist_id = self.chemist_id
        medOrderData.prescription_type = self.prescription_type
        medOrderData.description = self.description ?? "-"
        medOrderData.delivery_type = self.delivery_type
        medOrderData.estimated_date = self.estimated_date
        medOrderData.slot = self.slot
        medOrderData.pincode = String(self.pincode!)
        medOrderData.prescription_image = self.prescription_image
        medOrderData.doctor_name = self.doctor_name
        medOrderData.customer_address = self.customer_address
        medOrderData.flat_house_no = self.flat_house_no
        //medOrderData.recipient_name = self.getRecepientData()
        
        return medOrderData
    }
}
struct OrderDetails: Codable {
    
    var available_status: String?
    var medicine_name: String?//MedicineName?
    var suggested_name: String?
    var order_id: Int?
    var quantity: Int?
    var unit: String?
    /*
     {
         "available_status" = "<null>";
         "medicine_name" = "{\"medicine_name\":\"Paramol 500mg Tablet\",\"suggested_name\":\"\",\"medicine_type\":\"PM\"}";
         "order_id" = 352;
         quantity = 1;
         unit = strip;
     }
     **/
    
    func getMedicineData() -> MedicineName {
        
        do {
            
            guard let data = self.medicine_name?.data(using: .utf8) else { return MedicineName() }
            let recdata = try JSONDecoder().decode(MedicineName.self, from: data)
            
            return recdata
            
        } catch let err {
            
        }
        return MedicineName()
    }
    
    func getMedicine() -> String {
        
        do {
            
            guard let data = self.medicine_name?.data(using: .utf8) else { return "" }
            let recdata = try JSONDecoder().decode(MedicineName.self, from: data)
            
            return recdata.medicine_name ?? ""
                
        } catch let err {
            
        }
        return ""
    }
}
struct RecepientData: Codable {
    
    var rec_id: String?
    var rec_name: String?
    var rec_mobile: String?
}

struct ChemistOrderData: Codable {
    /*
     {
         "id": 2,
         "user_name": "-",
         "doctor_name": "5",
         "description": "Test1",
         "prescription_type": "Prescription",
         "pincode": 4221011,
         "address": "Nashik road",
         "delivery_type": "pickup1",
         "estimated_date": "17-0-2021",
         "slot": "test1",
         "estimated_cost": null,
         "status": "Pending",
         "order_details": []
     }
     **/
    /*
     elements
     ▿ 0 : 2 elements
       - key : "message"
       - value : Record Found.
     ▿ 1 : 2 elements
       - key : "data"
       ▿ value : 4 elements
         ▿ 0 : 24 elements
           ▿ 0 : 2 elements
             - key : doctor_name
             - value : ghh
           ▿ 1 : 2 elements
             - key : description
             - value : -
           ▿ 2 : 2 elements
             - key : slot
             - value : 07:00 PM To 09:00 PM
           ▿ 3 : 2 elements
             - key : status
             - value : Pending
           ▿ 4 : 2 elements
             - key : flat_house_no
             - value : 459
           ▿ 5 : 2 elements
             - key : shop_name
             - value : Test Chemist Shop
           ▿ 6 : 2 elements
             - key : auto_generated_date
             - value : 2020-09-03 14:20:47
           ▿ 7 : 2 elements
             - key : customer_address
             - value : -
           ▿ 8 : 2 elements
             - key : prescription_image
             - value : RjnqZrN8BvquiPiFH61R.pdf
           ▿ 9 : 2 elements
             - key : estimated_cost
             - value : <null>
               - super : NSObject
           ▿ 10 : 2 elements
             - key : updated_at
             - value : 2020-09-18 11:20:54
           ▿ 11 : 2 elements
             - key : shop_pincode
             - value : 422009
           ▿ 12 : 2 elements
             - key : delivery_type
             - value : Pickup
           ▿ 13 : 2 elements
             - key : shop_address
             - value : Ambad CIDCO Link RoadAmbad CIDCO Link Rd, Mhasoba Nagar, Modkeshwar Nagar, Nashik, Maharashtra , India
           ▿ 14 : 2 elements
             - key : chemist_name
             - value : Mr. Hoh Test Chemist
           ▿ 15 : 2 elements
             - key : prescription_type
             - value : Prescribed
           ▿ 16 : 2 elements
             - key : estimated_date
             - value : 03-09-2020
           ▿ 17 : 2 elements
             - key : id
             - value : 411
           ▿ 18 : 2 elements
             - key : mobile_no
             - value : 7719957969
           ▿ 19 : 2 elements
             - key : pincode
             - value : 0
           ▿ 20 : 2 elements
             - key : order_details
             ▿ value : 2 elements
               ▿ 0 : 5 elements
                 ▿ 0 : 2 elements
                   - key : medicine_name
                   - value : {"medicine_name":"PRIMACOR IV 10 MG\/10ML","suggested_name":"","medicine_type":"PM"}
                 ▿ 1 : 2 elements
                   - key : quantity
                   - value : 1
                 ▿ 2 : 2 elements
                   - key : order_id
                   - value : 411
                 ▿ 3 : 2 elements
                   - key : available_status
                   - value : <null> { ... }
                 ▿ 4 : 2 elements
                   - key : unit
                   - value : Ampoule
               ▿ 1 : 5 elements
                 ▿ 0 : 2 elements
                   - key : medicine_name
                   - value : {"medicine_name":"p","suggested_name":"","medicine_type":"NPM"}
                 ▿ 1 : 2 elements
                   - key : quantity
                   - value : 2
                 ▿ 2 : 2 elements
                   - key : order_id
                   - value : 411
                 ▿ 3 : 2 elements
                   - key : available_status
                   - value : <null> { ... }
                 ▿ 4 : 2 elements
                   - key : unit
                   - value : Other
           ▿ 21 : 2 elements
             - key : recipient_name
             - value : {"rec_id":"406","rec_name":"Mr. Ghh","rec_mobile":"7945661919"}
           ▿ 22 : 2 elements
             - key : address
             - value : -
           ▿ 23 : 2 elements
             - key : user_name
             - value : Pri
         ▿ 1 : 24 elements
           ▿ 0 : 2 elements
             - key : doctor_name
             - value : pp
           ▿ 1 : 2 elements
             - key : description
             - value : gkg
           ▿ 2 : 2 elements
             - key : slot
             - value : 04:00 PM To 07:00 PM
           ▿ 3 : 2 elements
             - key : status
             - value : Pending
           ▿ 4 : 2 elements
             - key : flat_house_no
             - value : 459
           ▿ 5 : 2 elements
             - key : shop_name
             - value : Test Chemist Shop
           ▿ 6 : 2 elements
             - key : auto_generated_date
             - value : 2020-09-03 12:44:14
           ▿ 7 : 2 elements
             - key : customer_address
             - value : , Lokhand Bazar, Gotane Wada, Nashik, Maharashtra , India
           ▿ 8 : 2 elements
             - key : prescription_image
             - value : GW7QxxdunDYz3NbOIAs0.pdf
           ▿ 9 : 2 elements
             - key : estimated_cost
             - value : <null> { ... }
           ▿ 10 : 2 elements
             - key : updated_at
             - value : 2020-09-18 11:20:54
           ▿ 11 : 2 elements
             - key : shop_pincode
             - value : 422009
           ▿ 12 : 2 elements
             - key : delivery_type
             - value : Delivery
           ▿ 13 : 2 elements
             - key : shop_address
             - value : Ambad CIDCO Link RoadAmbad CIDCO Link Rd, Mhasoba Nagar, Modkeshwar Nagar, Nashik, Maharashtra , India
           ▿ 14 : 2 elements
             - key : chemist_name
             - value : Mr. Hoh Test Chemist
           ▿ 15 : 2 elements
             - key : prescription_type
             - value : Prescribed
           ▿ 16 : 2 elements
             - key : estimated_date
             - value : 03-09-2020
           ▿ 17 : 2 elements
             - key : id
             - value : 409
           ▿ 18 : 2 elements
             - key : mobile_no
             - value : 7719957969
           ▿ 19 : 2 elements
             - key : pincode
             - value : 422001
           ▿ 20 : 2 elements
             - key : order_details
             ▿ value : 1 element
               ▿ 0 : 5 elements
                 ▿ 0 : 2 elements
                   - key : medicine_name
                   - value : {"medicine_name":"AZELAST NASAL SPRAY","suggested_name":"","medicine_type":"PM"}
                 ▿ 1 : 2 elements
                   - key : quantity
                   - value : 1
                 ▿ 2 : 2 elements
                   - key : order_id
                   - value : 409
                 ▿ 3 : 2 elements
                   - key : available_status
                   - value : <null> { ... }
                 ▿ 4 : 2 elements
                   - key : unit
                   - value : Other
           ▿ 21 : 2 elements
             - key : recipient_name
             - value : {"rec_id":"405","rec_name":"Mr. Pri","rec_mobile":"7719957969"}
           ▿ 22 : 2 elements
             - key : address
             - value : , Lokhand Bazar, Gotane Wada, Nashik, Maharashtra , India
           ▿ 23 : 2 elements
             - key : user_name
             - value : Pri
         ▿ 2 : 24 elements
           ▿ 0 : 2 elements
             - key : doctor_name
             - value : sjje
           ▿ 1 : 2 elements
             - key : description
             - value : -
           ▿ 2 : 2 elements
             - key : slot
             - value : 07:00 PM To 09:00 PM
           ▿ 3 : 2 elements
             - key : status
             - value : Pending
           ▿ 4 : 2 elements
             - key : flat_house_no
             - value : -
           ▿ 5 : 2 elements
             - key : shop_name
             - value : Test Chemist Shop
           ▿ 6 : 2 elements
             - key : auto_generated_date
             - value : 2020-09-02 19:54:10
           ▿ 7 : 2 elements
             - key : customer_address
             - value : -
           ▿ 8 : 2 elements
             - key : prescription_image
             - value : Z4nfovTg27qE6heO7eII.jpg
           ▿ 9 : 2 elements
             - key : estimated_cost
             - value : <null> { ... }
           ▿ 10 : 2 elements
             - key : updated_at
             - value : 2020-09-18 11:20:54
           ▿ 11 : 2 elements
             - key : shop_pincode
             - value : 422009
           ▿ 12 : 2 elements
             - key : delivery_type
             - value : Pickup
           ▿ 13 : 2 elements
             - key : shop_address
             - value : Ambad CIDCO Link RoadAmbad CIDCO Link Rd, Mhasoba Nagar, Modkeshwar Nagar, Nashik, Maharashtra , India
           ▿ 14 : 2 elements
             - key : chemist_name
             - value : Mr. Hoh Test Chemist
           ▿ 15 : 2 elements
             - key : prescription_type
             - value : Prescribed
           ▿ 16 : 2 elements
             - key : estimated_date
             - value : 02-09-2020
           ▿ 17 : 2 elements
             - key : id
             - value : 407
           ▿ 18 : 2 elements
             - key : mobile_no
             - value : 7719957969
           ▿ 19 : 2 elements
             - key : pincode
             - value : 0
           ▿ 20 : 2 elements
             - key : order_details
             ▿ value : 3 elements
               ▿ 0 : 5 elements
                 ▿ 0 : 2 elements
                   - key : medicine_name
                   - value : {"medicine_name":"ABCLOX","suggested_name":"","medicine_type":"PM"}
                 ▿ 1 : 2 elements
                   - key : quantity
                   - value : 2
                 ▿ 2 : 2 elements
                   - key : order_id
                   - value : 407
                 ▿ 3 : 2 elements
                   - key : available_status
                   - value : <null> { ... }
                 ▿ 4 : 2 elements
                   - key : unit
                   - value : Capsule
               ▿ 1 : 5 elements
                 ▿ 0 : 2 elements
                   - key : medicine_name
                   - value : {"medicine_name":"pqr","suggested_name":"","medicine_type":"NPM"}
                 ▿ 1 : 2 elements
                   - key : quantity
                   - value : 2
                 ▿ 2 : 2 elements
                   - key : order_id
                   - value : 407
                 ▿ 3 : 2 elements
                   - key : available_status
                   - value : <null> { ... }
                 ▿ 4 : 2 elements
                   - key : unit
                   - value : Other
               ▿ 2 : 5 elements
                 ▿ 0 : 2 elements
                   - key : medicine_name
                   - value : {"medicine_name":"XYZAL 5 mg","suggested_name":"","medicine_type":"NPM"}
                 ▿ 1 : 2 elements
                   - key : quantity
                   - value : 2
                 ▿ 2 : 2 elements
                   - key : order_id
                   - value : 407
                 ▿ 3 : 2 elements
                   - key : available_status
                   - value : <null> { ... }
                 ▿ 4 : 2 elements
                   - key : unit
                   - value : Tablet
           ▿ 21 : 2 elements
             - key : recipient_name
             - value : {"rec_id":"405","rec_name":"Mr. Pri","rec_mobile":"7719957969"}
           ▿ 22 : 2 elements
             - key : address
             - value : -
           ▿ 23 : 2 elements
             - key : user_name
             - value : Pri
         ▿ 3 : 24 elements
           ▿ 0 : 2 elements
             - key : doctor_name
             - value : tester dr
           ▿ 1 : 2 elements
             - key : description
             - value : -
           ▿ 2 : 2 elements
             - key : slot
             - value : 07:00 PM To 09:00 PM
           ▿ 3 : 2 elements
             - key : status
             - value : Pending
           ▿ 4 : 2 elements
             - key : flat_house_no
             - value : Modkeshwar Nagar Road
           ▿ 5 : 2 elements
             - key : shop_name
             - value : Test Chemist Shop
           ▿ 6 : 2 elements
             - key : auto_generated_date
             - value : 2020-07-30 18:17:35
           ▿ 7 : 2 elements
             - key : customer_address
             - value : Modkeshwar Nagar Rd, Mhasoba Nagar, Modkeshwar Nagar, Nashik, Maharashtra , India
           ▿ 8 : 2 elements
             - key : prescription_image
             - value : jCoOVzrHQJh912np4kQn.jpg
           ▿ 9 : 2 elements
             - key : estimated_cost
             - value : 500
           ▿ 10 : 2 elements
             - key : updated_at
             - value : 2020-09-18 11:20:54
           ▿ 11 : 2 elements
             - key : shop_pincode
             - value : 422009
           ▿ 12 : 2 elements
             - key : delivery_type
             - value : Delivery
           ▿ 13 : 2 elements
             - key : shop_address
             - value : Ambad CIDCO Link RoadAmbad CIDCO Link Rd, Mhasoba Nagar, Modkeshwar Nagar, Nashik, Maharashtra , India
           ▿ 14 : 2 elements
             - key : chemist_name
             - value : Mr. Hoh Test Chemist
           ▿ 15 : 2 elements
             - key : prescription_type
             - value : Prescribed
           ▿ 16 : 2 elements
             - key : estimated_date
             - value : 30-07-2020
           ▿ 17 : 2 elements
             - key : id
             - value : 338
           ▿ 18 : 2 elements
             - key : mobile_no
             - value : 9657618996
           ▿ 19 : 2 elements
             - key : pincode
             - value : 422007
           ▿ 20 : 2 elements
             - key : order_details
             ▿ value : 1 element
               ▿ 0 : 5 elements
                 ▿ 0 : 2 elements
                   - key : medicine_name
                   - value : {"medicine_name":"NECLOX-LS","suggested_name":"","medicine_type":"PM"}
                 ▿ 1 : 2 elements
                   - key : quantity
                   - value : 2
                 ▿ 2 : 2 elements
                   - key : order_id
                   - value : 338
                 ▿ 3 : 2 elements
                   - key : available_status
                   - value : <null> { ... }
                 ▿ 4 : 2 elements
                   - key : unit
                   - value : Capsule
           ▿ 21 : 2 elements
             - key : recipient_name
             - value : {"rec_id":"295","rec_name":"Ms. HOH Test Customer","rec_mobile":"9657618996"}
           ▿ 22 : 2 elements
             - key : address
             - value : Modkeshwar Nagar Rd, Mhasoba Nagar, Modkeshwar Nagar, Nashik, Maharashtra , India
           ▿ 23 : 2 elements
             - key : user_name
             - value : Ms. HOH Test Customer
     ▿ 2 : 2 elements
       - key : "status"
       - value : 1

     **/
    
    var id: Int?
    var user_name: String?
    var mobile_no: String?
    var doctor_name: String?
    var description: String?
    var prescription_type: String?
    var prescription_image: String?
    var pincode: Int?
    var address: String?
    var delivery_type: String?
    var estimated_date: String?
    var slot: String?
    var estimated_cost: Double?
    var status: String?
    var order_details: [OrderDetails] = []
    var customer_address: String?
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do {
            self.id = try container.decode(Int.self, forKey: .id)
            self.address = try container.decodeIfPresent(String.self, forKey: .address)
            self.user_name = try container.decode(String.self, forKey: .user_name)
            self.doctor_name = try container.decode(String.self, forKey: .doctor_name)
            self.description = try container.decode(String.self, forKey: .description)
            self.prescription_type = try container.decode(String.self, forKey: .prescription_type)
            self.prescription_image = try container.decode(String.self, forKey: .prescription_image)
            self.pincode = try container.decode(Int.self, forKey: .pincode)
            self.delivery_type = try container.decode(String.self, forKey: .delivery_type)
            self.estimated_date = try container.decode(String.self, forKey: .estimated_date)
            self.slot = try container.decode(String.self, forKey: .slot)
            self.estimated_cost = try container.decodeIfPresent(Double.self, forKey: .estimated_cost)
            self.status = try container.decode(String.self, forKey: .status)
            self.order_details = try container.decode([OrderDetails].self, forKey: .order_details)
            self.customer_address = try container.decode(String.self, forKey: .customer_address)
            self.mobile_no = try container.decode(String.self, forKey: .mobile_no)
            
        } catch DecodingError.typeMismatch {
            
            let mobile_no = try container.decode(Int.self, forKey: .mobile_no)
            self.mobile_no = "\(mobile_no)"
        }
    }
}
