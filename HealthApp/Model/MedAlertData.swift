//
//  MedAlertData.swift
//  HealthApp
//
//  Created by Apple on 24/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct MedAlertData: Codable {
    
    var id: Int?
    var user_id: Int?
    var user_name: String?
    var medicine_name: String?
    var alert_type: String?
    var medicine_unit: String?
    var alert_timmings: [AlertTiming] = []
    var strAlertTime: [String] = []
    var visit_reason: String?
    var doctor_address: String?
    var child_name: String?
    
    private enum CodingKeys: String, CodingKey {
        case id, user_name, medicine_name, alert_type, medicine_unit, alert_timmings
    }
    
    mutating func parseTimeCategory() {
        
        var cnt = 0
        for var data in self.alert_timmings {
            
            do {
                let meddata = data.time_category!.data(using: .utf8)!
                //try JSONSerialization.data(withJSONObject: data.time_category!, options: .prettyPrinted)
            
            let json = try JSONDecoder().decode(TimeCategory.self, from: meddata)
                data.timecategory = json
                
                if data.timecategory?.category == "before" || data.timecategory?.category == "after" {
                    //self.alert_timmings.remove(at: cnt)
                    
                } else {
                    self.strAlertTime.append(data.timecategory?.category ?? "")
                    self.strAlertTime.append("\n")
                    self.strAlertTime.append(data.timecategory?.weeks ?? "")
                    self.strAlertTime.append("\n")
                    self.strAlertTime.append("\n")
                }
            } catch let err {
                print(err)
            }
            cnt = cnt + 1
        }
    }
    
    mutating func parseVaccineData() {
        
        do {
            let med_unit = self.medicine_unit
            let medunitData = med_unit?.data(using: .utf8)
            let jsondata = try JSONSerialization.jsonObject(with: medunitData!, options: .mutableContainers) as! [String:Any]
            self.child_name = jsondata["member_name"] as? String
            
            var arrTiming: [AlertTiming] = []
            for var alerttiming in self.alert_timmings {
                let meddata = alerttiming.time_category!.data(using: .utf8)!
                
                
                let json = try JSONDecoder().decode(TimeCategory.self, from: meddata)
                alerttiming.timecategory = json
                arrTiming.append(alerttiming)
            }
            self.alert_timmings = arrTiming
            self.alert_timmings = self.alert_timmings.filter{ ($0.timecategory?.alert_status != "hidden") }
            
        } catch let err {
            print(err)
        }
    }
    
    mutating func parseMCData() {
        
        do {
            let med_unit = self.medicine_unit
            let medunitData = med_unit?.data(using: .utf8)
            let jsondata = try JSONSerialization.jsonObject(with: medunitData!, options: .mutableContainers) as! [String:Any]
            
            var arrTiming: [AlertTiming] = []
            for var alerttiming in self.alert_timmings {
                let meddata = alerttiming.time_category!.data(using: .utf8)!
                
                
                let json = try JSONDecoder().decode(TimeCategory.self, from: meddata)
                alerttiming.timecategory = json
                arrTiming.append(alerttiming)
            }
            self.alert_timmings = arrTiming
            
            
        } catch let err {
            print(err)
        }
    }
    
    func getMemberName()-> String {
        
        
        if let unit = self.medicine_unit {
            do {
            let medunitData = unit.data(using: .utf8)
            let jsondata = try JSONSerialization.jsonObject(with: medunitData!, options: .mutableContainers) as! [String:Any]
                
                let name = jsondata["member_name"] as! String
                return name
            } catch let err {
                
            }
        }
        return ""
    }
}

struct AlertTiming: Codable {
    
    var medicine_alert_id: Int?
    var time_category: String?//TimeCategory?
    var timming: String?
    var time_in_miles: String?
    var alert_status: String?
    var timecategory: TimeCategory?
    var arrWeeks: [String] = []
    var category: String?
    var selected: Bool = false
    
    private enum CodingKeys: String, CodingKey {
        case medicine_alert_id, time_category, timming, time_in_miles, alert_status, timecategory
    }
}
struct TimeCategory: Codable {
    
    var alert_name: String?
    var alert_text: String?
    var alert_status: String?
    var category: String?
    var weeks: String?
}
    /*
     alert_timmings" =             (
                         {
             "alert_status" = "<null>";
             "medicine_alert_id" = 2335;
             "time_category" = "{\"alert_name\":\"Tentative Pre-Period Day\",\"alert_text\":\"Hey! We estimate that your period is due in 2 days. Take care\",\"alert_status\":\"Pending\"}";
             "time_in_miles" = 1599453011394;
             timming = "07-09-2020";
         },
                         {
             "alert_status" = "<null>";
             "medicine_alert_id" = 2336;
             "time_category" = "{\"alert_name\":\"Tentative Pre-Period Day\",\"alert_text\":\"Hello! You might get your period tomorrow.\",\"alert_status\":\"Pending\"}";
             "time_in_miles" = 1599539411394;
             timming = "08-09-2020";
         },
                         {
             "alert_status" = "<null>";
             "medicine_alert_id" = 2337;
             "time_category" = "{\"alert_name\":\"Tentative Period Day\",\"alert_text\":\"Today might be the day for your period! Take care\",\"alert_status\":\"Pending\"}";
             "time_in_miles" = 1599625811394;
             timming = "09-09-2020";
         },
                         {
             "alert_status" = "<null>";
             "medicine_alert_id" = 2338;
             "time_category" = "{\"alert_name\":\"Tentative Period Day\",\"alert_text\":\"Today might be the day for your period! Take care\",\"alert_status\":\"Pending\"}";
             "time_in_miles" = 1599712211394;
             timming = "10-09-2020";
         },
                         {
             "alert_status" = "<null>";
             "medicine_alert_id" = 2339;
             "time_category" = "{\"alert_name\":\"Tentative Period Day\",\"alert_text\":\"Today might be the day for your period! Take care\",\"alert_status\":\"Pending\"}";
             "time_in_miles" = 1599798611394;
             timming = "11-09-2020";
         },
                         {
             "alert_status" = "<null>";
             "medicine_alert_id" = 2340;
             "time_category" = "{\"alert_name\":\"Tentative Peak Ovulation Date\",\"alert_text\":\"Today might be the day for your Peak Ovulation! Take care\",\"alert_status\":\"Pending\"}";
             "time_in_miles" = 1600489811394;
             timming = "19-09-2020";
         },
                         {
             "alert_status" = "<null>";
             "medicine_alert_id" = 2341;
             "time_category" = "{\"alert_name\":\"Tentative Peak Ovulation Date\",\"alert_text\":\"Today might be the day for your Peak Ovulation! Take care\",\"alert_status\":\"Pending\"}";
             "time_in_miles" = 1600576211394;
             timming = "20-09-2020";
         },
                         {
             "alert_status" = "<null>";
             "medicine_alert_id" = 2342;
             "time_category" = "{\"alert_name\":\"Tentative Peak Ovulation Date\",\"alert_text\":\"Today might be the day for your Peak Ovulation! Take care\",\"alert_status\":\"Pending\"}";
             "time_in_miles" = 1600662611394;
             timming = "21-09-2020";
         },
                         {
             "alert_status" = "<null>";
             "medicine_alert_id" = 2343;
             "time_category" = "{\"alert_name\":\"Tentative Peak Ovulation Date\",\"alert_text\":\"Today might be the day for your Peak Ovulation! Take care\",\"alert_status\":\"Pending\"}";
             "time_in_miles" = 1600749011394;
             timming = "22-09-2020";
         },
                         {
             "alert_status" = "<null>";
             "medicine_alert_id" = 2344;
             "time_category" = "{\"alert_name\":\"Tentative Peak Ovulation Date\",\"alert_text\":\"Today might be the day for your Peak Ovulation! Take care\",\"alert_status\":\"Pending\"}";
             "time_in_miles" = 1600835411394;
             timming = "23-09-2020";
         }
     );
     "alert_type" = "mc_tracker";
     id = 235;
     "medicine_name" = "Menstrual Cycle Tracker";
     "medicine_unit" = "{\"member_id\":\"\",\"member_name\":\"\",\"member_dob\":\"10-08-2020\"}";
     "user_name" = "Miss Sarika";
     **/

/*
 http://www.doctoronhire.com/health/api/add_medicine_alert?
 user_id=405&
 medicine_name=Dr. akash&
 medicine_unit={"visit_reason":"general visit","doctor_address":"nashik"}&
 alert_timmings=[
 {"time_category":"before 24","timming":"15:00","time_in_miles":"1598434242782"},
 {"time_category":"before 4","timming":"11:00","time_in_miles":"1598506242782"},
 {"time_category":"before 2","timming":"13:00","time_in_miles":"1598513442782"},
 {"time_category":"27-08-2020","timming":"15:00","time_in_miles":"1598520642782"}
 ]&
 alert_type=doctor_visits



 http://www.doctoronhire.com/health/api/add_medicine_alert
 Pritamsing Pardeshi, Today at 13:25

 **/

/*
 http://www.doctoronhire.com/health/api/add_medicine_alert?
 user_id=405&
 medicine_name=ABCID 20 mg&
 medicine_unit=Capsule&
 alert_timmings=[
 {"time_category":"{"category":"Morning","weeks":"Sunday, Thursday, Friday"}","timming":"09:00","time_in_miles":"1598326254984"},
 {"time_category":"{"category":"before","weeks":"Sunday, Thursday, Friday"}","timming":"08:55","time_in_miles":"1598325954984"},
 {"time_category":"{"category":"after","weeks":"Sunday, Thursday, Friday"}","timming":"09:05","time_in_miles":"1598326554984"},
 {"time_category":"{"category":"Evening","weeks":"Monday, Thursday, Friday, Saturday"}","timming":"18:00","time_in_miles":"1598358654984"},
 {"time_category":"{"category":"before","weeks":"Monday, Thursday, Friday, Saturday"}","timming":"17:55","time_in_miles":"1598358354984"},
 {"time_category":"{"category":"after","weeks":"Monday, Thursday, Friday, Saturday"}","timming":"18:05","time_in_miles":"1598358954984"}
 ]&
 alert_type=medicine_alerts
 **/
