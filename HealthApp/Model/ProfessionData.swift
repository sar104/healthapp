//
//  ProfessionData.swift
//  HealthApp
//
//  Created by Apple on 12/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct ProfessionData: Codable {
    
    var id: Int?
    var profession: String?
}
