//
//  PincodeData.swift
//  HealthApp
//
//  Created by Apple on 01/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct PincodeData: Codable {
    /*
     {
         area = BITCO;
         district = NASHIK;
         "office_name" = Nashik;
         pincode = 422101;
         "pincode_id" = 185717;
         state = MAHARASHTRA;
         "sub_district" = Nashik;
     }
     **/
    var area: String?
    var district: String?
    var office_name: String?
    var pincode: String?
    var pincode_id: Int?
    var state: String?
    var sub_district: String?
}
