//
//  PrescriptionData.swift
//  HealthApp
//
//  Created by Apple on 16/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct PrescriptionData: Codable {
 
    var id: Int?
    var user_id: Int?
    var user_name: String?
    var child_id: Int?
    var sharing_type: Int?
    var doctor_name: String?
    var illness_type: String?
    var description: String?
    var illness_date: String?
    var images: [Images] = []
    var image: String?
    var chemist: ChemistData?
    
    func getDrugs()-> String {
        
        var strDrugs = ""
        do {
            if let description = description {
                
                let jsondata = Data(description.utf8)
                let medArr = try JSONDecoder().decode([Medicine].self, from: jsondata)
                let str = NSMutableString()
                
                for med in medArr {
                    //let medstr = (med.medicine_name ?? "") + "-" + String(med.quantity!) + "(" + med.unit + "), "
                    str.append(med.medicine_name ?? "")
                    str.append("-")
                    str.append(String(med.quantity!))
                    str.append("(")
                    str.append(med.unit ?? "")
                    str.append("), ")
                    
                }
                strDrugs = str as String
            }
            
            return strDrugs
            
        } catch let err {
            print(err)
        }
        return strDrugs
    }
    
    func getMedicines()-> [DrugData] {
        
        var drugData: [DrugData] = []
        do {
            if let description = description {
                
                let jsondata = Data(description.utf8)
                let medArr = try JSONDecoder().decode([Medicine].self, from: jsondata)
                
                for med in medArr {
                
                    let medicineName = MedicineName(medicine_name: med.medicine_name, suggested_name: "", medicine_type: med.unit)
                    let drug = DrugData(medicine_name: medicineName, quantity: med.quantity, unit: med.unit)
                    drugData.append(drug)
                    
                }
                return drugData
            }
            
        } catch let err {
            print(err)
        }
        return []
    }
    
    func getDrugsArr()-> [MedicineData] {
        
        var medicineData: [MedicineData] = []
        do {
            if let description = description {
                
                let jsondata = Data(description.utf8)
                let medArr = try JSONDecoder().decode([Medicine].self, from: jsondata)
                for data in medArr {
                    var medData = MedicineData()
                    medData.medicine_name = data.medicine_name
                    medData.type_of_medicine = data.unit
                    medData.quantity = data.quantity!
                    
                    medicineData.append(medData)
                }
                return medicineData
            }
        }catch let err {
            print(err)
        }
        return []
    }
    
    func getImage() -> String {
        
        return self.images.count > 0 ? self.images[0].image ?? "" : ""
    }
            
}
struct Images: Codable {
    var id: Int?
    var image: String?
    var user_id: Int?
}

struct MedicalHistoryData {

   var id: Int?
   var user_id: Int?
   var child_id: Int?
   var sharing_type: Int?
   var doctor_name: String?
   var illness_type: String?
    var description: [String:Any]?
   var illness_date: String?
   var images: [Images] = []
   var image: String?
   var chemist: ChemistData?
    var auto_generated_date: String?
    var user_name: String?
    var new_user_name: String?
    var new_user_mobile: String?
    var notes: String?
    
    mutating func parseData(params: [String: Any]){
        
        //var arrMed: [MedicalHistoryData] = []
        //return arrMed
        if let id = params["id"] as? Int {
            self.id = id
        }
        if let user_id = params["user_id"] as? Int {
            self.user_id = user_id
        }
        if let child_id = params["child_id"] as? Int {
            self.child_id = child_id
        }
        if let sharing_type = params["sharing_type"] as? Int {
            self.sharing_type = sharing_type
        }
        if let doctor_name = params["doctor_name"] as? String {
            self.doctor_name = doctor_name
        }
        if let user_name = params["user_name"] as? String {
            self.user_name = user_name
        }
        if let new_user_name = params["new_user_name"] as? String {
            self.new_user_name = new_user_name
        }
        if let new_user_mobile = params["new_user_mobile"] as? String {
            self.new_user_mobile = new_user_mobile
        }
        if let doctor_name = params["doctor_name"] as? String {
            self.doctor_name = doctor_name
        }
        if let notes = params["notes"] as? String {
            self.notes = notes
        }
        if let illness_type = params["illness_type"] as? String {
            self.illness_type = illness_type
        }
        if let illness_date = params["illness_date"] as? String {
            self.illness_date = illness_date
        }
        if let description = params["description"] as? [String:Any] {
            self.description = description
        }
        if let image = params["image"] as? String {
            self.image = image
        }
        if let auto_generated_date = params["auto_generated_date"] as? String {
            self.auto_generated_date = auto_generated_date
        }
        if let images = params["images"] as? NSArray {
            
            do {
                let imgdata = try JSONSerialization.data(withJSONObject: images, options: [])
                
                let json = try JSONDecoder().decode([Images].self, from: imgdata)
                self.images = json
            } catch let err {
                
            }
        }
    }
    
    func getSharingStatus() -> String {
        
        return self.sharing_type == 0 ? "Private" : "Family"
    }
    
    func getDate() -> String {
        
        let date = self.auto_generated_date?.toDate(format: "yyyy-MM-dd HH:mm:ss")
        let strdate = date?.toString(format: "dd-MMM-yyyy HH:mm")
        return strdate!
    }
}

/*
 {
     "child_id" = 401;
     description = "[[\"quantity\": 1, \"unit\": \"strip\", \"medicine_name\": \"Paracad 500mg Tablet\"]]";
     "doctor_name" = xyz;
     id = 518;
     "illness_date" = "10/08/2020";
     "illness_type" = "Other Report";
     images =             (
                         {
             "created_at" = "<null>";
             "deleted_at" = "<null>";
             id = 457;
             image = "92T4JfHtgLvylmYb2vAO.jpeg";
             "medical_history_id" = 518;
             "updated_at" = "<null>";
             "user_id" = 401;
         }
     );
     "sharing_type" = 0;
     "user_id" = 401;
 }
 **/
/*
 {
            "id": 97,
            "user_id": 137,
            "child_id": 137,
            "sharing_type": 1,
            "doctor_name": "-",
            "illness_type": "Blood Pressure",
            "description": {
                "name": "Systolic Level",
                "name1": "Diastolic Level",
                "description": "119",
                "unit": "mm Hg",
                "description1": "70",
                "unit1": "mm Hg"
            },
            "illness_date": "20-03-2020",
            "auto_generated_date": "2020-03-20 14:05:32",
            "images": []
        }
 **/

/*
 {
            "auto_generated_date" = "2020-08-08 17:43:40";
            "child_id" = 401;
            description = "<null>";
            "doctor_name" = null;
            id = 499;
            "illness_date" = "2020-08-07";
            "illness_type" = "Other Report";
            images =             (
            );
            notes = "<null>";
            "sharing_type" = 0;
            "user_id" = 401;
        }
 **/
