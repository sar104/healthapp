//
//  SlotData.swift
//  HealthApp
//
//  Created by Apple on 17/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct SlotData: Codable {
    
    var id: Int?
    var slot: String?
    
    func isEarlier() -> Bool {
        
        if let arr = self.slot?.components(separatedBy: " to "), arr.count > 0 {
            var time1 = Int(arr[0])!
            var time2 = Int(arr[1])!
            
            if time2 >= 3 &&  time2 != 12 {
                if time1 < 12 {
                    time1 = time1 + 12
                }
                time2 = time2 + 12
            }
            let currTime = Date().get(.hour)
            
            if time1 < currTime {
                return true
            }
        }
        
        return false
            
    }
    
    func isEarlierSlot() -> Bool {
        
        if let arr = self.slot?.components(separatedBy: " To "), arr.count > 0 {
            //let time1 = arr[0].toDate(format: "hh:mm a")//Int(arr[0])!
            let time2 = arr[1].toDate(format: "hh:mm a")//Int(arr[1])!
            
            let currtime = Date().toString(format: "hh:mm a").toDate(format: "hh:mm a")
            
            if time2 < currtime {
                return true
            }
            
        }
        
        return false
            
    }
}
