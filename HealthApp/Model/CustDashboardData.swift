//
//  CustDashboardData.swift
//  HealthApp
//
//  Created by Apple on 05/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct CustDashboardData: Codable {
    
    var status: Bool
    var message: String?
    var reports: [ReportData] = []
    var orders: [OrderData] = []
    var doctors: [DoctorData] = []
}

struct ChemistDashData: Codable {
    /*
    {
        "approval_pending" = 0;
        delivered = 0;
        message = "Record Found.";
        pending = 4;
        "pending_delivery" = 0;
        status = 1;
    }
    **/
    
    var approval_pending: Int?
    var delivered: Int?
    var message: String?
    var pending: Int?
    var pending_delivery: Int?
    var status: Bool?
}

struct DoctorDashData: Codable {
    /*
    {
        ["status": 1, "message": Record Found., "today appointment": 1]
    }
    **/
    
    
    var message: String?
    var todays_appointment: Int?
    var status: Bool?
}
