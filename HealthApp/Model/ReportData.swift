//
//  ReportData.swift
//  HealthApp
//
//  Created by Apple on 05/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct ReportData: Codable {
    /*
     {
         "child_name" = "Mr. Ghh";
         "report_date" = "2020-07-31";
         type = Prescription;
     }
     **/
    var child_name: String?
    var report_date: String?
    var type: String?
}
