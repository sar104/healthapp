//
//  ChemistData.swift
//  HealthApp
//
//  Created by Apple on 14/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct ChemistData: Codable {
    
    var id: Int?
    var name: String?
    var mobile_no: Int?
    var latitude: Double?
    var longitude: Double?
    var address_id: String?
    var distance: Double?
    var address: String?
    var address_type: String?
    var avg_rating: Double?
    var city: String?
    var clinic_lab_hospital_name: String?
    var delivery_status: String?
    var email: String?
    var favourite: Bool?
    var gst_no: String?
    var off_day: String?
    var pincode: String?
    var registration_no: String?
    var shop_closing_time: String?
    var shop_image: String?
    var shop_opening_time: String?
    var state: String?
    var alt_contact_no: String?
    var assistant_name: String?
    
    func getShopTiming()-> String {
        
        let strOpenTime = "Open " + (self.shop_opening_time ?? "") + "."
        let strCloseTime = "Closes " + (self.shop_closing_time ?? "")
        
        let time = strOpenTime + strCloseTime
        return time
    }
    
    mutating func parseData(dic: [String:Any]) {
        
        if let address = dic["address"] as? String {
            self.address = address
        }
        if let address_id = dic["address_id"] as? String {
            self.address_id = address_id
        }
        if let address_type = dic["address_type"] as? String {
            self.address_type = address_type
        }
        if let alt_contact_no = dic["alt_contact_no"] as? String {
            self.alt_contact_no = alt_contact_no
        }
        if let assistant_name = dic["assistant_name"] as? String {
            self.assistant_name = assistant_name
        }
        if let avg_rating = dic["avg_rating"] as? Double {
            self.avg_rating = avg_rating
        }
        if let city = dic["city"] as? String {
            self.city = city
        }
        if let clinic_lab_hospital_name = dic["clinic_lab_hospital_name"] as? String {
            self.clinic_lab_hospital_name = clinic_lab_hospital_name
        }
        if let delivery_status = dic["delivery_status"] as? String {
            self.delivery_status = delivery_status
        }
        if let email = dic["email"] as? String {
            self.email = email
        }
        if let gst_no = dic["gst_no"] as? String {
            self.gst_no = gst_no
        }
        if let id = dic["id"] as? Int {
            self.id = id
        }
        if let latitude = dic["latitude"] as? Double {
            self.latitude = latitude
        }
        if let longitude = dic["longitude"] as? Double {
            self.longitude = longitude
        }
        if let mobile_no = dic["mobile_no"] as? Int {
            self.mobile_no = mobile_no
        }
        if let name = dic["name"] as? String {
            self.name = name
        }
        if let off_day = dic["off_day"] as? String {
            self.off_day = off_day
        }
        if let pincode = dic["pincode"] as? String {
            self.pincode = pincode
        }
        if let registration_no = dic["registration_no"] as? String {
            self.registration_no = registration_no
        }
        if let shop_closing_time = dic["shop_closing_time"] as? String {
            self.shop_closing_time = shop_closing_time
        }
        if let shop_opening_time = dic["shop_opening_time"] as? String {
            self.shop_opening_time = shop_opening_time
        }
        if let shop_image = dic["shop_image"] as? String {
            self.shop_image = shop_image
        }
        if let state = dic["state"] as? String {
            self.state = state
        }
    }
}
/*
{
          address = ", Lokhand Bazar, Gotane Wada, Nashik, Maharashtra , India";
          "address_id" = "<null>";
          "address_type " = 459;
          "avg_rating" = 0;
          city = NASHIK;
          "clinic_lab_hospital_name" = shdhd;
          "delivery_status" = Yes;
          distance = "2.04728349981022850201384244428481906652";
          email = "sjjs@fjkd.com";
          favourite = 0;
          "gst_no" = fgghygffjjgdfmm;
          id = 403;
          latitude = "19.9974540000000011730207916116341948509";
          longitude = "73.789803000000006250047590583562850952";
          "mobile_no" = 7385043853;
          name = "Mr. Shsh";
          "off_day" = Monday;
          pincode = 422001;
          "registration_no" = 5;
          "shop_closing_time" = "12:33 AM";
          "shop_image" = null;
          "shop_opening_time" = "12:33 AM";
          state = MAHARASHTRA;
      },
**/
