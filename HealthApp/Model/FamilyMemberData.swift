//
//  FamilyMemberData.swift
//  HealthApp
//
//  Created by Apple on 10/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct FamilyMemberData: Codable {
    
    var id: Int?
    var name: String?
    var gender: String?
    var dob: String?
    var mobile_no: Int?
    var email: String?
    var profession: String?
    var profession_id: String?
    var bloodgroup: String?
    var state: String?
    var city: String?
    var height: String?
    var weight: String?
    var weight_unit: String?
    var height_unit: String?
    var is_blood_donor: String?
    var profile_image: String?
    var relation_id: String?
    var relation: String?
    var role: String?
    var fam_relation: String?
    
    func getDetails() -> String {
        
        let details = NSMutableString()
        
        details.append("Gender: ")
        details.append(self.gender ?? "")
        details.append(" Blood Group: ")
        details.append(self.bloodgroup ?? "")
        details.append(" Height: ")
        details.append(self.height ?? "")
        details.append(" Weight: ")
        details.append(self.weight ?? "" + "Kg")
        
        return details as String
    }
    
    func getRequestData() -> AddFamilyRequestData {
        
        let familyData = self
        var requestData = AddFamilyRequestData()
        
        requestData.id = familyData.id
        requestData.name = familyData.name ?? ""
        requestData.gender = familyData.gender ?? ""
        requestData.dob = familyData.dob ?? ""
        requestData.email = familyData.email ?? ""
        if let mob_no = familyData.mobile_no {
            requestData.mobile_no = String(mob_no)
        }
        if let prof = familyData.profession_id {
            requestData.profession = Int(prof)
        }
        requestData.bloodgroup = familyData.bloodgroup ?? "NA"
        requestData.state = familyData.state ?? "-"
        requestData.city = familyData.city ?? "-"
        requestData.height = familyData.height ?? ""
        requestData.height_unit = familyData.height_unit ?? ""
        requestData.weight = familyData.weight ?? ""
        requestData.weight_unit = familyData.weight_unit ?? ""
        requestData.is_blood_donor = familyData.is_blood_donor ?? "No"
        if let rel = familyData.relation_id {
            requestData.relation = Int(rel)
        }
        requestData.strRelation = familyData.relation
        requestData.profile_image = familyData.profile_image ?? ""
        
        return requestData
    }
}
/*
{
    "status": true,
    "message": "Record Found.",
    "data": [
        {
            "id": 88,
            "name": "Arindam Sarkar",
            "gender": null,
            "dob": null,
            "mobile_no": 9818099777,
            "email": null,
            "profession_id": "2",
            "profession": "Businessman",
            "bloodgroup": null,
            "state": null,
            "city": null,
            "height": null,
            "weight": null,
            "weight_unit": null,
            "height_unit": null,
            "is_blood_donor": null,
            "relation_id": "3",
            "relation": "son",
            "profile_image": null
        },
        {
            "id": 92,
            "name": "akash",
            "gender": "Male",
            "dob": "2020-02-01",
            "mobile_no": null,
            "email": null,
            "profession_id": "0",
            "profession": "-",
            "bloodgroup": "B+",
            "state": "KARNATAKA",
            "city": "Ankola",
            "height": "2",
            "weight": "10",
            "weight_unit": "Kg",
            "height_unit": "Feets",
            "is_blood_donor": "No",
            "relation_id": "3",
            "relation": "son",
            "profile_image": null
        }
    ]
}
**/
