//
//  BannerData.swift
//  HealthApp
//
//  Created by Apple on 29/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct BannerData: Codable {
    
    var id: Int?
    var image_video: String?
    var title: String?
    var description: String?
    var created_at: String?
}
/*
 {
     "status": true,
     "message": "Record Found.",
     "data": [
         {
             "id": 23,
             "image_video": "ki9saslf7xbd0dzxdw.jpg",
             "title": "test title1",
             "description": "test description1"
         },
         {
             "id": 24,
             "image_video": "82xe0xqai4hihwjxc4.jpg",
             "title": "test2",
             "description": "test desc2"
         },
         {
             "id": 25,
             "image_video": "ubn53wdu4evalvc19c.jpg",
             "title": "test title3",
             "description": "Test desc3"
         },
         {
             "id": 26,
             "image_video": "lz3v2a4e88usbhhyls.jpg",
             "title": "Test title4",
             "description": "test desc4"
         },
         {
             "id": 27,
             "image_video": "5zsoinbd233prl1pw0.jpg",
             "title": "Test title5",
             "description": "test desc5"
         }
     ]
 }
 **/
