//
//  Role.swift
//  HealthApp
//
//  Created by Apple on 23/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct RoleData: Codable, ResponseData {
    
    var status: Bool
    var message: String?
    var data: [Role]?
}
struct Role: Codable {
    
    var id: Int?
    var role: String?
}
/*
 (
             {
         id = 2;
         role = Customer;
     },
             {
         id = 3;
         role = Chemist;
     },
             {
         id = 4;
         role = "Diagnostic Centre";
     },
             {
         id = 5;
         role = Doctor;
     },
             {
         id = 6;
         role = "Service Provider ";
     }
 )
 **/
