
import Foundation

struct NotificationData: Codable {
    
    var id: Int?
    var sender_id: Int?
    var userid: String?
    var title: String?
    var username: String?
    var shop_name: String?
    var mobile_no: String?
    var channel: String?
    var currdate: String?
    var currtime: String?
    var remark: String?
    
    func populateCoreData(data: RemoteNotification) {
        
        if let id = self.id {
            data.id = Int32(id)
        }
        if let sender_id = self.sender_id {
            data.sender_id = Int32(sender_id)
        }
        data.title = self.title
        data.username = self.username
        data.shop_name = self.shop_name
        data.mobile_no = self.mobile_no
        data.channel = self.channel
        data.currdate = self.currdate
        data.currtime = self.currtime
        data.remark = self.remark
    }
    
    mutating func populateNotifData(dic: [AnyHashable:Any]) {
        
        self.userid = dic["userid"] as? String
        self.username = dic["username"] as? String
        self.shop_name = dic["shop_name"] as? String
        self.mobile_no = dic["mobile_no"] as? String
        self.channel = dic["channel"] as? String
        self.title = dic["title"] as? String
    }
}
