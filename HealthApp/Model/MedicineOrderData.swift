//
//  MedicineOrderData.swift
//  HealthApp
//
//  Created by Apple on 17/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct MedicineOrderData: Codable {
    
    var user_id: Int?
    var chemist_id: Int?
    var prescription_type: String?
    var description: String = "null"
    var delivery_type: String?
    var estimated_date: String?
    var slot: String?
    var address_id: Int = 0
    var pincode: String?
    //var drug_list: [DrugData] = []
    var prescription_image: String?
    var doctor_name: String?
    var customer_address: String?
    var recipient_name: RecipientData?
    var flat_house_no: String?
    
    /*
     user_id=405&
     chemist_id=306&
     doctor_name=akash&
     prescription_type=Prescribed&
     description=-&
     delivery_type=Delivery&
     estimated_date=17-08-2020&
     slot=07:00 PM To 09:00 PM&
     address_id=null&
     pincode=422004&
     drug_list=[{"medicine_name":"{"medicine_name":"COMBITIDE","suggested_name":"","medicine_type":"PM"}","quantity":"2","unit":"Others"},{"medicine_name":"{"medicine_name":"CYCLOPAM INJECTION","suggested_name":"","medicine_type":"PM"}","quantity":"3","unit":"Injection"},{"medicine_name":"{"medicine_name":"CHEERIO TOOTHPASTE","suggested_name":"","medicine_type":"NPM"}","quantity":"2","unit":"Others"},{"medicine_name":"{"medicine_name":"abc","suggested_name":"","medicine_type":"NPM"}","quantity":"1","unit":"Other"}]
     &prescription_image=bbQK1Pb53netn8UuzbsX.jpg
     &customer_address=, Peth Rd, Pingle Nagar, Makhmalabad, Nashik, Maharashtra , India
     &recipient_name={"rec_id":"405","rec_name":"Mr. Pri","rec_mobile":"7719957969"}
     &flat_house_no=4a
     **/
}

struct RecipientData: Codable {
    
    var rec_id: Int?
    var rec_name: String?
    var rec_mobile: String?
        
    //{"rec_id":"405","rec_name":"Mr. Pri","rec_mobile":"7719957969"}
}
