//
//  VaccineAlertData.swift
//  HealthApp
//
//  Created by Apple on 30/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct VaccineAlertData {
    
    var year: Int?
    var month: Int?
    var day: Int?
    var vaccine_name: String?
    var alertTitle: String?
    var count: Int?
    var gender: String = "Male"
    var alertDate: Date?
    var strAlertDate: String?
    
    mutating func setAlertTitle(childName: String) {
        
        self.alertTitle = self.alertTitle?.replacingOccurrences(of: "#", with: childName).replacingOccurrences(of: "@", with: self.vaccine_name!)
    }
    mutating func setAlertDate(dob: Date) {
        
        let components = DateComponents(calendar: Calendar.current, year: year, month: month, day: day)
        self.alertDate = Calendar.current.date(byAdding: components, to: dob)
    }
    
    mutating func setStrAlertDate() {
        
        self.strAlertDate = self.alertDate!.toString(format: df_dd_MM_yyyy)
    }
}
