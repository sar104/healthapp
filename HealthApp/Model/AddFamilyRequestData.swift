//
//  AddFamilyRequestData.swift
//  HealthApp
//
//  Created by Apple on 12/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct AddFamilyRequestData: Codable {
    
    var id: Int?
    var name: String = ""
    var gender: String = ""
    var dob: String = ""
    var email: String = ""
    var mobile_no: String = ""
    var profession: Int?
    var bloodgroup: String = "NA"
    var state: String = "-"
    var city: String = "-"
    var height: String = ""
    var height_unit: String = ""
    var weight: String = ""
    var weight_unit: String = ""
    var is_blood_donor: String = ""
    var relation: Int?
    var strRelation: String?
    var parent_id: Int?
    var profile_image: String = ""
    
    private enum CodingKeys: String, CodingKey {
        case id, name, gender, dob, email, mobile_no,profession,bloodgroup,state,city,height,height_unit,weight,weight_unit,is_blood_donor,relation,parent_id,profile_image
    }
}
