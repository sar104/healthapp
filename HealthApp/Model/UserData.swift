//
//  UserData.swift
//  HealthApp
//
//  Created by Apple on 24/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct UserData: Codable {
    
    /*
     success({
         "alt_contact_no" = "<null>";
         "assistant_name" = "<null>";
         data =     {
             bloodgroup = "B+";
             city = Nashik;
             dob = "1992-04-01";
             email = null;
             gender = Female;
             height = null;
             "height_unit" = null;
             id = 401;
             interest = "<null>";
             "is_blood_donor" = Yes;
             "mobile_no" = 9850059684;
             name = "Miss sarika";
             "parent_id" = "<null>";
             profession = null;
             "profile_image" = 0;
             state = MAHARASHTRA;
             weight = null;
             "weight_unit" = null;
         };
         message = "Login successfully.";
         "profile_completed" = 1;
         role = 2;
         status = 1;
     })
     
     "alt_contact_no" = "<null>";
     "assistant_name" = "<null>";
     data =     {
         bloodgroup = "<null>";
         city = "<null>";
         dob = "<null>";
         email = "<null>";
         gender = "<null>";
         height = "<null>";
         "height_unit" = "<null>";
         id = 401;
         interest = "<null>";
         "is_blood_donor" = "<null>";
         "mobile_no" = 9850059684;
         name = sarika;
         "parent_id" = "<null>";
         profession = "<null>";
         "profile_image" = "<null>";
         state = "<null>";
         weight = "<null>";
         "weight_unit" = "<null>";
     };
     message = "Login successfully.";
     "profile_completed" = 0;
     role = 2;
     status = 1;
     
     success({
         data =     {
             bloodgroup = "<null>";
             city = "<null>";
             dob = "<null>";
             email = "<null>";
             gender = "<null>";
             height = "<null>";
             "height_unit" = "<null>";
             id = 401;
             interest = "<null>";
             "is_blood_donor" = "<null>";
             "mobile_no" = 9850059684;
             name = sarika;
             "parent_id" = "<null>";
             profession = "<null>";
             "profile_image" = "<null>";
             state = "<null>";
             weight = "<null>";
             "weight_unit" = "<null>";
         };
         message = "Login successfully.";
         "profile_completed" = 0;
         role = 2;
         status = 1;
     })
     

         "status": true,
         "message": "Record Found.",
         "data": {
             "id": 80,
             "shop_image": "u6jb0rqkcw83ea87tc.jpg",
             "name": "Mayuri K",
             "clinic_lab_hospital_name": "Ashoka Hspital",
             "mobile_no": 7020432621,
             "email": "m@gmail.com",
             "registration_no": "000012MR12",
             "gst_no": "GST0967",
             "state": "MAHARASTRA",
             "city": "NASHIK",
             "latitude": "19.9806539",
             "longitude": "73.79785419999999",
             "address_id": "13",
             "address": "jail road, nashik road nashik",
             "pincode": "422101",
             "avg_rating": 2.5
         }
     }
     **/
    var status: Bool
    var message: String?
    var data: UserModel?
    var role: String?
    var profile_completed: String?
    
}
struct UserModel: Codable {
    
    var id: Int?
    var parent_id: Int?
    var name: String?
    var gender: String?
    var dob: String?
    //var mobile_no: String?
    var email: String?
    var profession: String?
    var bloodgroup: String?
    var state: String?
    var city: String?
    var height: String?
    var weight: String?
    var is_blood_donor: String?
    var profile_image: String?
    var profile_completed: String?
    var interest: String?
    
    init(user: User) {
        
        self.id = Int(user.id)
        self.name = user.name
        self.gender = user.gender
        self.dob = user.dob
        self.email = user.email
        self.profession = user.profession
        self.bloodgroup = user.bloodgroup
        self.state = user.state
        self.city = user.city
        self.height = user.height
        self.weight = user.weight
        self.is_blood_donor = user.is_blood_donor
        self.profile_image = user.profile_image
        self.profile_completed = user.profile_completed
        self.profession = user.profession
        self.interest = user.interest
    }
    
    func populateCoreData(data: User) {
        
        data.id = Int32(Int(self.id!))
        data.name = self.name
        data.gender = self.gender
        data.dob = self.dob
        data.email = self.email
        data.profession = self.profession
        data.bloodgroup = self.bloodgroup
        data.state = self.state
        data.city = self.city
        data.height = self.height
        data.weight = self.weight
        data.is_blood_donor = self.is_blood_donor
        data.profile_image = self.profile_image
        data.profession = self.profession
        data.interest = self.interest
    }
    
    func getUser() -> String {
        
        do {
            let data = try JSONEncoder().encode(self)
            
            let user = String(data: data, encoding: .utf8) ?? ""
            
            return user
        } catch {
            
        }
        return ""
    }
}

struct InterestData: Codable {
    
    var id: Int?
    var interest: String?
}

struct SpecializationData: Codable {
    
    var id: Int?
    var specialization: String?
}
