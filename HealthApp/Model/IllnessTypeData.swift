//
//  IllnessTypeData.swift
//  HealthApp
//
//  Created by Apple on 10/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct IllnessTypeData: Codable {
    
    var id: Int?
    var illness_type: String?
}
