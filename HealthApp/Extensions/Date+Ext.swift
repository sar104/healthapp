//
//  Date+Ext.swift
//  HealthApp
//
//  Created by Apple on 29/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

extension Date {
    
    func toString(format: String) -> String {
        
        let df = DateFormatter()
        df.dateFormat = format//"dd/MM/YYYY"
        let dateStr = df.string(from: self)
        return dateStr
    }
    
    func get(_ components: Calendar.Component..., calendar: Calendar = Calendar.current) -> DateComponents {
        return calendar.dateComponents(Set(components), from: self)
    }

    func get(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
        return calendar.component(component, from: self)
    }
    
    func nextDate() -> Date {
        
        return (Calendar.current.date(byAdding: .month, value: 1, to: self) ?? nil) ?? Date()
    }
    func prevDate() -> Date {
        
        return (Calendar.current.date(byAdding: .month, value: -1, to: self) ?? nil) ?? Date()
    }
    
    func getDiff()-> Int {
        
        return Calendar.current.dateComponents(Set([.year]), from: self , to: Date()).year!
        //dateComponents([.year], from: Date(), to: self).year!
        
    }
    
    func dateByAdding(component: Calendar.Component, value: Int) -> Date {
        return (Calendar.current.date(byAdding: component, value: value, to: self) ?? nil) ?? Date()
    }
    
    func convertToMilli() -> Int {
        let since1970 = self.timeIntervalSince1970
        return Int(since1970 * 1000)
    }
}
