//
//  UITextField+Ext.swift
//  HealthApp
//
//  Created by Apple on 07/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

extension UITextField {
    
    func addBottomBorder(){
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: self.frame.height - 1, width: self.frame.width, height: 0.7)
        bottomLine.backgroundColor = UIColor.gray.cgColor
        bottomLine.accessibilityValue = "bottom"
        self.borderStyle = UITextField.BorderStyle.none
        self.layer.addSublayer(bottomLine)
    }
    
    func removeBottomBorder() {
        
        if let sublayers = self.layer.sublayers {
            for layer in sublayers {
                
                if layer.accessibilityValue == "bottom" {
                    layer.removeFromSuperlayer()
                }
            }
        }
    }
}
