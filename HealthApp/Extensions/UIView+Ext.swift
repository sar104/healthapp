//
//  UIView+Ext.swift
//  HealthApp
//
//  Created by Apple on 31/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

extension UIView {
    
    func setDashBorder(borderColor: UIColor){
        
        let border = CAShapeLayer();
        border.strokeColor = borderColor.cgColor //UIColor.black.cgColor;
        border.fillColor = nil;
        border.lineDashPattern = [2, 2];
        border.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 5).cgPath
        border.frame = self.bounds;
        self.layer.addSublayer(border);
    }
    
    func pathSemiCirclesPathForView(givenView: UIView, ciclesRadius:CGFloat = 5, circlesDistance : CGFloat = 3, top:Bool = true, bottom:Bool = true ) //->UIBezierPath
    {
        let width = givenView.frame.size.width
        let height = givenView.frame.size.height
        
        let semiCircleWidth = CGFloat(ciclesRadius*2)
        
        let semiCirclesPath = UIBezierPath()
        semiCirclesPath.move(to: CGPoint(x:0, y:0))
        
        if(bottom) {
            var x = CGFloat(0)
            var i = 0
            while x < width {
                x = (semiCircleWidth) * CGFloat(i) + (circlesDistance * CGFloat(i))
                let pivotPoint = CGPoint(x: x + semiCircleWidth/2, y: height)
                semiCirclesPath.addArc(withCenter: pivotPoint, radius: ciclesRadius, startAngle: -180 * .pi / 180.0, endAngle: 0 * .pi / 180.0, clockwise: true)
                semiCirclesPath.addLine(to: CGPoint(x: semiCirclesPath.currentPoint.x + circlesDistance, y: height))
                i += 1
            }
        }
        else {
            semiCirclesPath.addLine(to: CGPoint(x: 0, y: height))
            semiCirclesPath.addLine(to: CGPoint(x: width, y: height))
        }
        
        semiCirclesPath.addLine(to: CGPoint(x:width,y: 0))
        
        if(top) {
            var x = CGFloat(width)
            var i = 0
            while x > 0 {
                x = width - (semiCircleWidth) * CGFloat(i) - (circlesDistance * CGFloat(i))
                let pivotPoint = CGPoint(x: x - semiCircleWidth/2, y: 0)
                semiCirclesPath.addArc(withCenter: pivotPoint, radius: ciclesRadius, startAngle: 0 * .pi / 180.0, endAngle: -180 * .pi / 180.0, clockwise: true)
                semiCirclesPath.addLine(to: CGPoint(x: semiCirclesPath.currentPoint.x - circlesDistance, y: 0))
                i += 1
            }
        }
        
        semiCirclesPath.close()
        
        var shapeLayer = CAShapeLayer()
        shapeLayer.path = semiCirclesPath.cgPath
        givenView.layer.mask = shapeLayer
        
        //return semiCirclesPath
    }
}
