//
//  FAQVC.swift
//  HealthApp
//
//  Created by Apple on 30/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

struct FAQData: Codable {
    
    /*
     {
         answer = "You can refer Ctrlh terms at https://ctrlh.in/terms and Policy at https://ctrlh.in/terms";
         "created_at" = "2020-09-16 13:35:03";
         "deleted_at" = "<null>";
         id = 23;
         question = "Terms & Policy";
         "updated_at" = "2020-09-16 13:35:03";
     }
     **/
    var question: String?
    var answer: String?
}
class FAQVC: BaseVC {

    @IBOutlet weak var tblData: UITableView!
    
    var faqArr: [FAQData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getFaq()
    }
    
    func getFaq() {
        
        IHProgressHUD.show()
        apiManager.getFAQ(params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                
            case .success(let data):
                print(data)
                self.faqArr = data
                DispatchQueue.main.async {
                    self.tblData.dataSource = self
                    self.tblData.delegate = self
                    self.tblData.reloadData()
                }
            case .failure(let err):
                print(err)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension FAQVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return faqArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FAQCell
        
        let data = faqArr[indexPath.row]
        
        cell.lblTitle.text = data.question
        cell.lblDesc.text = data.answer
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
}

class FAQCell: BaseCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
}
