//
//  AddDiagnoReportVC.swift
//  HealthApp
//
//  Created by Apple on 26/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD
import MobileCoreServices

class AddDiagnoReportVC: BaseDiagnoVC, AddDiagnoReportCellDelegate, UITextFieldDelegate {
    

    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var lblHead: UILabel!
    
    let imgPicker = UIImagePickerController()
    var reportImgArr:[Data] = []
    var reportPDFArr:[Data] = []
    var imageNameArr:[String] = []
    var upImageNameArr:[String] = []
    var editFlag: Bool = false
    var viewFlag: Bool = false
    var mobile: String?
    var user_id: String?
    var name: String?
    var notes: String?
    var dateOfReport: String = ""
    var reportImgFlag: Bool = false
    var reportData: MedicalHistoryData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if editFlag {
            
            name = reportData?.user_name
            dateOfReport = reportData?.illness_date ?? ""
            notes = reportData?.notes
            if let images = reportData?.images {
                imageNameArr = images.map({ ($0.image ?? "")}).filter({ !$0.isEmpty})
            }
        }
    }
    
    func uploadReports(){
        
        IHProgressHUD.show()
        apiManager.uploadImgArray(image: reportImgArr, name: "image", api: MedicalHistoryImgAPI) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let imgdata):
                    
                    if imgdata.status {
                        if let arr = imgdata.data {
                            //self.imageNameArr = arr
                            self.imageNameArr.append(contentsOf: arr)
                            if self.editFlag {
                                self.upImageNameArr.append(contentsOf: arr)
                            }
                            print("images: \(arr)")
                            //self.addMedicalHistory()
                            if self.reportPDFArr.count > 0 {
                                self.uploadPDFReport()
                            } else {
                                if self.editFlag {
                                    self.updateMedicalHistory()
                                } else {
                                    self.addMedicalHistory()
                                }
                            }
                        }
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func uploadPDFReport(){
        
        IHProgressHUD.show()
        apiManager.uploadPDFArray(image: reportPDFArr, name: "image", api: MedicalHistoryImgAPI) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let imgdata):
                    
                    if imgdata.status {
                        if let arr = imgdata.data {
                            self.imageNameArr.append(contentsOf: arr)
                            if self.editFlag {
                                self.upImageNameArr.append(contentsOf: arr)
                            }
                            print("images: \(arr)")
                            //self.addMedicalHistory()
                            if !self.reportImgFlag {
                                if self.editFlag {
                                    self.updateMedicalHistory()
                                } else {
                                    self.addMedicalHistory()
                                }
                            }
                        }
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func addMedicalHistory(){
        
        IHProgressHUD.show()
        
        do {
            
            let jsonImgs: Data = try JSONSerialization.data(withJSONObject: imageNameArr, options: [])
            //JSON(imageNameArr)
            
            let imgArrStr = String(data:jsonImgs,encoding: .utf8)
            
            let params: [String:Any] = [PARAM_USERID: user_id!,
                                          PARAM_CHILDID:  user_id!,
                                          PARAM_DOCTORID: loggedUser!.id,
                PARAM_SHARINGTYPE: 0,
                PARAM_DOCTORNAME: loggedUser!.name!,
            PARAM_ILLNESSTYPE: "Other",
            PARAM_DESCRIPTION: strNull,
            PARAM_ILLNESSDATE: dateOfReport,
            PARAM_IMAGES: imgArrStr!,
            "notes": notes ?? "-"]
            
            
            print("params: \(params)")
            
            apiManager.addMedicalHistoryAPI(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.navReportsList()
                        }
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
       
        } catch let err {
            print(err)
        }
        
    }
    
    func updateMedicalHistory(){
        
        IHProgressHUD.show()
        
        do {
            let jsonImgs: Data = try JSONSerialization.data(withJSONObject: upImageNameArr, options: [])
            //JSON(imageNameArr)
            
            let imgArrStr = String(data:jsonImgs,encoding: .utf8)
            
            let params: [String:Any] = [
                PARAM_ID: reportData!.id,
                PARAM_USERID: reportData!.user_id!,
                PARAM_CHILDID:  reportData!.user_id!,
                                          PARAM_DOCTORID: loggedUser!.id,
                PARAM_SHARINGTYPE: 0,
                PARAM_DOCTORNAME: loggedUser!.name!,
            PARAM_ILLNESSTYPE: "Other",
            PARAM_DESCRIPTION: strNull,
            PARAM_ILLNESSDATE: dateOfReport,
            PARAM_IMAGES: imgArrStr!,
            "notes": notes ?? "-"]
            
            print("params: \(params)")
        
            apiManager.updateMedicalHistory(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        if data.status {
                            self.alert(strTitle: strSuccess, strMsg: "Report updated successfully")
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.navReportsList()
                        }
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
        
        } catch let err {
            print(err)
        }
       
    }
    
    @objc func getUserByMobile(text: String) {
        
        //if let mobile = text {
        
               
            IHProgressHUD.show()
            
            let params = ["mobile_no": text]  as [String : Any]
            print("params:\(params)")
            
            apiManager.getUserByMobile(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                case .success(let data):
                    print("data: \(data)")
                    
                    self.user_id = String(data.id!)
                    self.name = data.name
                    
                    DispatchQueue.main.async {
                        
                            self.tblData.reloadData()
                        
                    }
                case .failure(let err):
                    print("err: \(err)")
                    
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                }
            }
        //}
    }
    
    func navReportsList() {
        self.navigationController?.popViewController(animated: true)
    }

    func saveReport() {
        
        if self.validate() {
            if reportImgArr.count > 0 {
                self.reportImgFlag = true
                self.uploadReports()
            }
            else if reportPDFArr.count > 0 {
                self.uploadPDFReport()
            }
            else {
                if editFlag {
                    self.updateMedicalHistory()
                } else {
                    self.addMedicalHistory()
                }
            }
        }
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        
        saveReport()
    }
    
    func validate() -> Bool{
        
        if !editFlag && ( mobile == nil || mobile!.isEmpty ) {
            
            self.alert(strTitle: strErr, strMsg: "Please enter patient mobile number")
            return false
            
        }
        if dateOfReport.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select date")
            return false
        }
        
        if reportImgArr.count == 0 && imageNameArr.count == 0 && reportPDFArr.count == 0 {
            self.alert(strTitle: strErr, strMsg: "Please upload report image")
            return false
        }
        
        return true
    }
    
    func onValueSelected(type: String, value: String) {
            
        switch type {
        case "notes":
            self.notes = value
            
        case "date":
            self.dateOfReport = value
            
        default:
            break
        }
    }
    
    func onDataSelected(type: String, value: Any) {
        
    }
    
    func onAddClicked() {
         self.showImgPicker()
    }
    
    func onImgRemoved(id: Int) {
        if id < self.reportImgArr.count {
            self.reportImgArr.remove(at: id)
        }
        if id < imageNameArr.count {
            imageNameArr.remove(at: id)
        }
        self.tblData.reloadData()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        
        if textField.accessibilityIdentifier == "mobile" {
            print("string: \(string)")
       
             
            if range.location == 9 && range.length != 1 {
                let mob = textField.text! + string
                 self.mobile = mob
                self.getUserByMobile(text: mob)

            }
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddDiagnoReportVC: UIDocumentPickerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        //self.pdfFlag = true
        do {
            let pdfData = try Data(contentsOf: myURL)
            reportPDFArr.append(pdfData)
            
        } catch let err {
            print("pdf err: \(err.localizedDescription)")
        }
        print("import result : \(myURL)")
        self.tblData.reloadData()
    }


    public func documentMenu(_ documentMenu:UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }


    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}
extension AddDiagnoReportVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
        func showImgPicker() {
            print("Tap On Image")
            imgPicker.delegate = self
        
            let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {
                action in
                
                self.imgPicker.allowsEditing = true
                self.imgPicker.sourceType = .camera
                self.imgPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                self.present(self.imgPicker, animated: true, completion: nil)
                
            }))
            alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: {
                action in
                
                self.imgPicker.allowsEditing = true
                self.imgPicker.sourceType = .photoLibrary
                self.imgPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                self.present(self.imgPicker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "PDF", style: .default, handler: {
                action in
                let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
                importMenu.delegate = self
                importMenu.modalPresentationStyle = .formSheet
                self.present(importMenu, animated: true, completion: nil)
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            
            present(alert, animated: true, completion: nil)
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.editedImage] as? UIImage else {
            return
        }
            if let imgdata = image.jpegData(compressionQuality: 0.5){
                self.reportImgArr.append(imgdata)
            }
            self.tblData.reloadData()
            dismiss(animated: true, completion: nil)
        }

        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
    
}
extension AddDiagnoReportVC: UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = AddDiagnoReportCell()
        
        switch indexPath.row {
            
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! AddDiagnoReportCell
            
            cell.txtFld.accessibilityIdentifier = "mobile"
            cell.txtFld.delegate = self
            cell.setup(id: "mobile", placeholder: "PATIENT'S MOBILE NO")
            
            return cell
            
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! AddDiagnoReportCell
            cell.txtFld.text = self.name ?? ""
            cell.setup(id: "name", placeholder: "PATIENT'S NAME")
            
            return cell
            
        case 2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "dateCell") as! AddDiagnoReportCell
            
            cell.setup(id: "date", placeholder: "Date of Report")
            cell.delegate = self
            cell.txtFld.text = dateOfReport
            
            return cell
            
        case 3:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "upCell") as! AddDiagnoReportCell
            
            if self.reportImgArr.count > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! AddDiagnoReportCell
                
                cell.imgArr = self.reportImgArr
                cell.viewFlag = self.viewFlag
                cell.collView.reloadData()
                cell.delegate = self
                return cell
                
            } else if self.reportPDFArr.count > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! AddDiagnoReportCell
                
                cell.pdfArr = self.reportPDFArr
                cell.viewFlag = self.viewFlag
                cell.collView.reloadData()
                cell.delegate = self
                return cell
                
            } else if imageNameArr.count > 0 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! AddDiagnoReportCell
                cell.imageNameArr = self.imageNameArr
                cell.viewFlag = self.viewFlag
                cell.collView.reloadData()
                cell.delegate = self
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "upCell") as! AddDiagnoReportCell
                cell.delegate = self
                return cell
            }
            
            
            return cell
            
        case 4:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! AddDiagnoReportCell
            
            cell.setup(id: "notes", placeholder: "Notes")
            cell.txtFld.text = notes
            cell.delegate = self
            
            return cell
            
        case 5:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! AddDiagnoReportCell
            
            //submitAction
            cell.btnSubmit.addTarget(self, action: #selector(submitAction(_:)), for: .touchUpInside)
            
            return cell
            
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
        if indexPath.row == 0 {
            return editFlag ? 0 : 80
        }
        if indexPath.row == 3 {
            return 120
        }
        return 80
    }
}
protocol AddDiagnoReportCellDelegate {
    
    func onValueSelected(type: String, value: String)
    func onDataSelected(type: String, value: Any)
    func onAddClicked()
    func onImgRemoved(id: Int)
}
class AddDiagnoReportCell: BaseCell {
    
    @IBOutlet weak var txtFld: RightViewTextField!
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var delegate: AddDiagnoReportCellDelegate?
    var imgArr:[Data] = []
    var pdfArr:[Data] = []
    var selRow: Int = 0
    var seldate: String?
    var imageNameArr:[String] = []
    var viewFlag: Bool = false
    
    @IBOutlet weak var lblRelation: UILabel!
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.maximumDate = Date()
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
    func setup(id: String, placeholder: String) {
        
        
        if !placeholder.isEmpty {
            txtFld.placeholder = placeholder
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
        }
        if id == "date" {
            
            txtFld.inputView = datePicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
        if id == "notes" {
            
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
        self.layoutIfNeeded()
    }
    
    @objc func dateSelected(_ picker: UIDatePicker){
        
        let dateStr = picker.date.toString(format: df_dd_MM_yyyy)//df.string(from: picker.date)
        
        txtFld.text = dateStr
        
        let date = picker.date.toString(format: df_dd_MM_yyyy)
        //delegate?.onValueSelected(type: "date", value: date)
        seldate = date
        
    }
    
    @objc override func doneCellPicker (sender: UIBarButtonItem) {
        
        let id = sender.accessibilityIdentifier
      
        if id == "date" {
            seldate = datePicker.date.toString(format: df_dd_MM_yyyy)
            txtFld.text = seldate!
            delegate?.onValueSelected(type: id!, value: seldate!)
        }
        if id == "notes" {
            
            delegate?.onValueSelected(type: id!, value: txtFld.text ?? "")
        }
       
        
        txtFld.resignFirstResponder()
    }
    
    @IBAction func addAction(_ sender: UIButton){
        
        delegate?.onAddClicked()
    }
}
extension AddDiagnoReportCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, DiagnoReportCollCellDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imgArr.count + pdfArr.count + imageNameArr.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = DiagnoReportCollCell()
        if indexPath.item == imgArr.count + pdfArr.count + imageNameArr.count {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addCell", for: indexPath) as! DiagnoReportCollCell
        } else {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath) as! DiagnoReportCollCell
            if imgArr.count > 0 && indexPath.item < imgArr.count {
                cell.imgview.image = UIImage(data: imgArr[indexPath.item])
            }
            else if pdfArr.count > 0 && (indexPath.item == imgArr.count) && (indexPath.item - imgArr.count)  < pdfArr.count + 1 {
                cell.imgview.image = UIImage(named: "pdf")
            }
            else if imageNameArr.count > 0 {
                let cnt = indexPath.item - imgArr.count
                
                if imageNameArr[cnt].contains("pdf") {
                    cell.imgview.image = UIImage(named: "pdf")
                } else {
                    let imgURL = reportImageURL + imageNameArr[cnt]
                    cell.imgview.kf.setImage(with: URL(string: imgURL))
                }
            }
            cell.btnDelete.tag = indexPath.item
            if viewFlag {
                cell.btnDelete.isHidden = true
            }
        }
        
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
      
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 90, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("didSelectItemAt \(indexPath.item)")
        if indexPath.item == imgArr.count {
            
            delegate?.onAddClicked()
        }
    }
    
    func onAddClicked(){
        delegate?.onAddClicked()
    }
    
    func onDeleteClicked(id: Int){
        
        if id < imgArr.count {
            imgArr.remove(at: id)
        }
        if id < imageNameArr.count {
            imageNameArr.remove(at: id)
        }
        delegate?.onImgRemoved(id: id)
        //self.collView.reloadData()
    }
    
}

protocol DiagnoReportCollCellDelegate {
    
    func onAddClicked()
    func onDeleteClicked(id: Int)
}
class DiagnoReportCollCell: UICollectionViewCell {
    
    @IBOutlet weak var imgview: UIImageView!
    @IBOutlet weak var btnDelete:UIButton!
    var delegate: DiagnoReportCollCellDelegate?
    
    @IBAction func addAction(_ sender: UIButton){
        delegate?.onAddClicked()
    }
    
    @IBAction func deleteAction(_ sender: UIButton){
        delegate?.onDeleteClicked(id: sender.tag)
    }
}
