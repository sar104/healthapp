//
//  ReportDetailsVC.swift
//  HealthApp
//
//  Created by Apple on 27/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ReportDetailsVC: BaseDiagnoVC {
    
    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var btnEdit: UIButton!

    var reportData: MedicalHistoryData?
    var imgArr: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if reportData != nil {
            
            if let images = reportData?.images {
                imgArr = images.map({ ($0.image ?? "")}).filter({ !$0.isEmpty})
            }
            
            tblData.reloadData()
        }
    }
    

    @IBAction func editAction(_ sender: UIButton) {
        
        let vc = self.getVC(with: "AddDiagnoReportVC", sb: IDDiagno) as! AddDiagnoReportVC
        
        vc.reportData = reportData
        vc.editFlag = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ReportDetailsVC: UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = AddDiagnoReportCell()
        
        switch indexPath.row {
            
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AddDiagnoReportCell
            
            cell.lblHead.text = "PATIENT"
            
            if let name = reportData?.user_name {
                cell.lblName.text = name
            }
            return cell
            
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AddDiagnoReportCell
            
            cell.lblHead.text = "RECOMMENDED BY"
            cell.lblName.text = "Doctor"
            
            
            return cell
            
        case 2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AddDiagnoReportCell
            
            cell.lblHead.text = "DOCTOR"
            if let name = reportData?.doctor_name {
                cell.lblName.text = name
            }
            
            return cell
            
            
        case 3:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AddDiagnoReportCell
            
            cell.lblHead.text = "DATE OF REPORT"
            if let date = reportData?.illness_date {
                cell.lblName.text = date
            }
            return cell
            
        case 4:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AddDiagnoReportCell
            
            cell.lblHead.text = "REPORT TYPE"
            cell.lblName.text = "Other"
            
            return cell
            
        case 5:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! ReportDetailCell
            
            cell.imageNameArr = imgArr
            cell.collVw.reloadData()
            
            return cell
            
        case 6:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AddDiagnoReportCell
            
            cell.lblHead.text = "REPORT NAME"
            cell.lblName.text = "Other"
            
            return cell
            
        case 7:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AddDiagnoReportCell
            
            cell.lblHead.text = "NOTES"
            if let notes = reportData?.notes {
                cell.lblName.text = notes
            }
            return cell
            
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
        if indexPath.row == 5 {
            return 120
        }
        return 70
    }
}
class ReportDetailCell: BaseCell,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var collVw: UICollectionView!
    
    var imageNameArr:[String] = []
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imageNameArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = DiagnoReportCollCell()
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath) as! DiagnoReportCollCell
        
        let cnt = indexPath.item
        
        if imageNameArr[cnt].contains("pdf") {
            cell.imgview.image = UIImage(named: "pdf")
        } else {
            let imgURL = reportImageURL + imageNameArr[cnt]
            cell.imgview.kf.setImage(with: URL(string: imgURL))
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
      
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 90, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("didSelectItemAt \(indexPath.item)")
        
    }
    
  
    
}
