//
//  BaseDiagnoVC.swift
//  HealthApp
//
//  Created by Apple on 21/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class BaseDiagnoVC: UIViewController {

    var loggedUser: User?
    let apiManager: WebServiceManager = WebServiceManager(service: WebService())
    lazy var coreDataManager: CoreDataManager = {
        let manager = CoreDataManager.shared
        return manager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let users = self.fetchUser()
        if users.count > 0 {
            loggedUser = users[0]
        }
    }
    

    func fetchUser() -> [User] {
        let users = self.coreDataManager.fetchLocalUsers()
        //self.coreDataService?.fetchLocalUsers() ?? []
        //print(user)
        return users
    }
    
    func getVC(with id: String, sb: String) -> UIViewController {
        
        let sb = UIStoryboard(name: sb, bundle: nil)
        
        let vc = sb.instantiateViewController(withIdentifier: id)
        return vc
    }
    
    func openCSV(fileName:String, fileType: String)-> String!{
        guard let filepath = Bundle.main.path(forResource: fileName, ofType: fileType)
            else {
                return nil
        }
        do {
            var contents = "" // = try String(contentsOfFile: filepath, encoding: .utf8)
            
            if fileName == "Medicines" {
                contents = try String(contentsOfFile: filepath, encoding: .isoLatin1)
            } else {
                contents = try String(contentsOfFile: filepath, encoding: .utf8)
            }
            
            return contents
        } catch let err {
            print("File Read Error for file \(filepath): \(err)")
            return nil
        }
    }
    
    func parseMedCSV(file: String)-> [[String]]{
        
        
        let content: String! = openCSV(fileName: file, fileType: "csv")
        if content != nil {
            let parsedCSV: [[String]] = content.components(separatedBy: "\r").filter{ $0.isEmpty == false }.map{ $0.components(separatedBy: ",") }
            
            //print("medicines csv: \(parsedCSV)")
            print(parsedCSV.count)
            return parsedCSV
        }
        return []
    }
    
    func saveMedData() {
        
        let medcnt = CoreDataManager.shared.getMedCount()
        
        if medcnt == 0 {
            saveMedicines()
        }
    }
    
    func saveMedicines(){
        
        
        let arrMed = self.parseMedCSV(file: "Medicines")
        /*
        self.coreDataService?
            .saveMedicineData(data: arrMed){
                (res) in
        }
        */
        
        
        var jsonMed: [[String:Any]] = []
        var cnt: Int = 0
        for row in arrMed {
            
            if cnt == 0 {
                cnt = cnt + 1
                continue
            } else {
                cnt = cnt + 1
            }
            
            
            if row.count == 3 {
                let medData = ["id": cnt, "medicine_name": row[0], "medicine_package": row[1], "medicine_type": row[2]] as [String : Any]
                jsonMed.append(medData)
            }
        }
        
        coreDataManager.syncMedicines(json: jsonMed){
            (result) in
        }
        
    }
    @objc func didDismissAlertView(alertView: SwiftAlertView) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
