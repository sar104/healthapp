//
//  DiagnoBranchListVC.swift
//  HealthApp
//
//  Created by Apple on 24/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class DiagnoBranchListVC: BaseDiagnoVC {

    @IBOutlet weak var tblData: UITableView!
    
    var branchArr: [DetailData] = []
    var isSP: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if isSP {
            fetchSPBranches()
        } else {
            fetchBranches()
        }
    }
    
    func fetchBranches() {
        
        IHProgressHUD.show()
        
        let params = ["parent_id": loggedUser!.id]
        apiManager.getDoctorBranches(params: params) { (result) in
         
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
             
                self.branchArr = data
                DispatchQueue.main.async {
                    
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
        
    }
    
    func fetchSPBranches() {
        
        IHProgressHUD.show()
        
        let params = ["parent_id": loggedUser!.id]
        apiManager.getSPBranches(params: params) { (result) in
         
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
             
                self.branchArr = data
                DispatchQueue.main.async {
                    
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
        
    }

    @IBAction func addAction(_ sender: UIButton) {
        
        if isSP {
            
            let vc = self.getVC(with: "UpdateServiceProfileVC", sb: IDServicePro) as! UpdateServiceProfileVC
            vc.addBranch = true
            vc.isSP = true
            self.navigationController?.pushViewController(vc, animated: false)
            
        } else {
            let vc = self.getVC(with: "UpdateDiagnoProfileVC", sb: IDDiagno) as! UpdateDiagnoProfileVC
            vc.addBranch = true
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DiagnoBranchListVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return branchArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "branchCell") as! DiagnoBranchListCell
        
        let data = branchArr[indexPath.row]
        if isSP {
            cell.populateSPData(data: data)
        } else {
            cell.populateData(data: data)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = branchArr[indexPath.row]
        
        if isSP {
            
            let vc = self.getVC(with: "ServiceProfileVC", sb: IDServicePro) as! ServiceProfileVC
            vc.branchFlag = true
            vc.doctorData = data
            self.navigationController?.pushViewController(vc, animated: false)
            
        } else {
            let vc = self.getVC(with: "DiagnoProfileVC", sb: IDDiagno) as! DiagnoProfileVC
            vc.branchFlag = true
            vc.doctorData = data
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
}

class DiagnoBranchListCell: BaseCell {
    
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblClinic: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var starView: CosmosView!
    
    func populateData(data: DetailData){
        
        if let shopimg = data.shop_image, !shopimg.isEmpty, shopimg != "null" {
            
            let imgURL = doctorImgUrl + shopimg
            imgVw.kf.setImage(with: URL(string: imgURL))
        } else {
            imgVw.image = UIImage(named: "ic_doctor1")
        }
        lblClinic.text = data.clinic_lab_hospital_name
        lblName.text = data.name
        let address = data.address! + ", " + data.city!
        lblAddress.text = address + ", " + data.state! + "-" + data.pincode!
        lblTime.text = data.shop_opening_time! + " to " + data.shop_closing_time!
        self.starView.rating = data.avg_rating ?? 0
    }
    
    func populateSPData(data: DetailData){
        
        if let shopimg = data.shop_image, !shopimg.isEmpty, shopimg != "null" {
            
            let imgURL = shopImgUrl + shopimg
            imgVw.kf.setImage(with: URL(string: imgURL))
        } else {
            imgVw.image = UIImage(named: "ic_doctor1")
        }
        lblClinic.text = data.clinic_lab_hospital_name
        lblName.text = data.name
        let address = data.address! + ", " + data.city!
        lblAddress.text = address + ", " + data.state! + "-" + data.pincode!
        lblTime.text = data.shop_opening_time! + " to " + data.shop_closing_time!
        self.starView.rating = data.avg_rating ?? 0
    }
}
