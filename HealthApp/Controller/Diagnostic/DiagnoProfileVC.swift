//
//  DiagnoProfileVC.swift
//  HealthApp
//
//  Created by Apple on 21/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class DiagnoProfileVC: BaseDiagnoVC {

    @IBOutlet weak var tblData: UITableView!
    
    var branchFlag: Bool = false
    var doctorData: DetailData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if !branchFlag {
            getProfile()
        } else {
            self.tblData.dataSource = self
            self.tblData.delegate = self
            self.tblData.reloadData()
        }
    }
    
    func getProfile() {
        
        let params = ["id": loggedUser!.id]
        
        IHProgressHUD.show()
        
        apiManager.getDoctorProfile(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
                
                self.doctorData = data
                
                DispatchQueue.main.async {
                    self.tblData.dataSource = self
                    self.tblData.delegate = self
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    @IBAction func editAction(_ sender: UIButton) {
        //UpdateDoctorProfileVC
        let vc = self.getVC(with: "UpdateDiagnoProfileVC", sb: IDDiagno) as! UpdateDiagnoProfileVC
        if branchFlag {
            vc.editBranch = true
            vc.doctorData = doctorData
        }
        self.navigationController?.pushViewController(vc, animated: false)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DiagnoProfileVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }
        if section == 1 {
            return 3
        }
        if section == 2 {
            return 4
        }
        if section == 3 {
            return 3
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = DiagnoProfileCell()
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! DiagnoProfileCell
            
            if let img = doctorData?.shop_image, img != "null" {
                let url = shopImgUrl + img
                cell.imgVw.kf.setImage(with: URL(string: url))
            } else {
                cell.imgVw.image = UIImage(named: "ic_doctor1")
            }
            
            return cell
            
        case 1:
            
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "editCell") as! DiagnoProfileCell
                
                cell.lblHead.text = "BUSINESS/COMPANY NAME"
                cell.lblName.text = doctorData?.clinic_lab_hospital_name
                
                cell.btnEdit.addTarget(self, action: #selector(editAction(_:)), for: .touchUpInside)
                
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DiagnoProfileCell
                
                cell.lblHead.text = "TELEPHONE NO"
                if let mob = doctorData?.mobile_no {
                    cell.lblName.text = String(mob)
                }
                
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DiagnoProfileCell
                
                cell.lblHead.text = "ADDRESSS"
                cell.lblName.text = doctorData?.address
                
                return cell
                
            default:
                break
            }
           
        case 2:
        
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DiagnoProfileCell
                
                cell.lblHead.text = "CERTIFICATE/LICENSE NUMBER"
                cell.lblName.text = doctorData?.registration_no
                
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DiagnoProfileCell
                
                cell.lblHead.text = "COMPANY PROPRIETOR PROOF TYPE"
                cell.lblName.text = doctorData?.delivery_status
                
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "imgNameCell") as! DiagnoProfileCell
                
                cell.lblHead.text = "COMPANY PROPRIETOR PROOF"
                if let img = doctorData?.gst_no, img != "null"  {
                    let url = shopImgUrl + img
                    cell.imgVw.kf.setImage(with: URL(string: url))
                } else {
                    cell.imgVw.image = UIImage(named: "ic_doctor1")
                }
                
                return cell
               
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DiagnoProfileCell
                
                cell.lblHead.text = "OWNER/DIRECTOR NAME"
                cell.lblName.text = doctorData?.name
                
                return cell
                
            default:
                break
            }
            
        case 3:
            
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DiagnoProfileCell
                
                cell.lblHead.text = "BUSINESS DESCRIPTION"
                cell.lblName.text = doctorData?.about_clinic
                
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DiagnoProfileCell
                
                cell.lblHead.text = "WORKING DAYS"
                cell.lblName.text = doctorData?.off_day
                
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DiagnoProfileCell
                
                cell.lblHead.text = "CONTACT TIMINGS"
                let time1 = doctorData!.shop_opening_time! + " To " + doctorData!.shop_closing_time!
                let time2 = doctorData!.afternoon_opening_time! + " To " + doctorData!.afternoon_closing_time!
                
                let str = NSAttributedString(htmlString: time1 + "<br>" + time2)
                cell.lblName.attributedText = str
                
                return cell
                
            default:
                break
            }
            
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            
            return 140
        }
        if indexPath.section == 2 {
            
            if indexPath.row == 2 {
                return 190
            }
        }
        
        return UITableView.automaticDimension
    }
}

class DiagnoProfileCell: BaseCell {
    
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
}
