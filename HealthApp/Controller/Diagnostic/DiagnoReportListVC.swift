//
//  DiagnoReportListVC.swift
//  HealthApp
//
//  Created by Apple on 25/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class DiagnoReportListVC: BaseDiagnoVC, UITextFieldDelegate {

    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var txtSrch: UITextField!
    
    var reportArr: [MedicalHistoryData] = []
    var mobile: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtSrch.delegate = self
        getReports()
    }
    

    func getReports() {
        
        IHProgressHUD.show()
        
        var params: [String: Any] = ["doctor_id": loggedUser!.id]
        
        if !mobile.isEmpty {
            params["new_user_mobile"] = mobile
        }
        
        apiManager.getReportsForDoctor(params: params) { (result) in
         
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
             
                do {
                    let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                    
                    let jsonmed = jsondata["data"] as! [[String:Any]]
                    
                    print("reports: \(jsonmed)")
                    self.reportArr.removeAll()
                    
                    for param in jsonmed {
                        
                        var reportData = MedicalHistoryData()
                        reportData.parseData(params: param)
                        self.reportArr.append(reportData)
                    }
                    
                    
                } catch let err {
                   
                }
                DispatchQueue.main.async {
                    
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    @IBAction func addAction(_ sender: UIButton) {
        //AddDiagnoReportVC
        let vc = self.getVC(with: "AddDiagnoReportVC", sb: IDDiagno) as! AddDiagnoReportVC
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        
        print("string: \(string)")
        
        if range.location == 9 && range.length != 1 {
            let mob = textField.text! + string
            self.mobile = mob
            self.getReports()
            
        }
        
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DiagnoReportListVC: UITableViewDataSource, UITableViewDelegate {
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reportArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reportCell") as! DiagnoReportLisCell
        
        let data = reportArr[indexPath.row]
        
        if data.images.count > 0 {
            
            if let img = data.images[0].image {
                if img.contains("pdf") {
                    cell.imgVw.accessibilityIdentifier = "pdf"
                } else {
                    cell.imgVw.accessibilityIdentifier = "img"
                }
            }
        }
        cell.populateData(data: data)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120//UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = reportArr[indexPath.row]
        
        let vc = self.getVC(with: "ReportDetailsVC", sb: IDDiagno) as! ReportDetailsVC
        vc.reportData = data
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
}
class DiagnoReportLisCell: BaseCell {
    
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblClinic: UILabel!
    //@IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    func populateData(data: MedicalHistoryData){
        
        if data.images.count > 0 {
            
            if let img = data.images[0].image {
                if img.contains("pdf") {
                    
                    imgVw.image = UIImage(named: "pdf")
                    
                } else {
                    let imgURL = reportImageURL + img
                    imgVw.kf.setImage(with: URL(string: imgURL))
                }
            }
        }
        self.lblName.text = data.illness_type
        self.lblClinic.text = data.user_name
        //self.lblAddress.text = data.illness_date
        self.lblTime.text = data.illness_date
        
    }
}
