//
//  DignoDashboardVC.swift
//  HealthApp
//
//  Created by Apple on 21/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class DignoDashboardVC: BaseDiagnoVC {

    @IBOutlet weak var tblData: UITableView!
    
    var doctorDashData: DoctorDashData?
    var arrBanner: [BannerData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getDoctorDash()
        getBannersData()
    }
    
    func getDoctorDash() {
        
        /*
         {
             "approval_pending" = 0;
             delivered = 0;
             message = "Record Found.";
             pending = 4;
             "pending_delivery" = 0;
             status = 1;
         }
         **/
        IHProgressHUD.show()
        
        let params = ["doctor_id": loggedUser!.id]
        
        apiManager.getDoctorDashboard(params: params) { (result) in
            
            DispatchQueue.main.async {
                IHProgressHUD.dismiss()
            }
            switch result {
                
                case .success(let data):
                    print(data)
                    do {
                        let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                        print("diagno dashboard: \(jsondata)")
                        var dashData = DoctorDashData()
                        if (jsondata["status"] as! Int) == 1 {
                            
                            dashData.todays_appointment = jsondata["today appointment"] as? Int
                        } else {
                            dashData.todays_appointment = 0
                        }
                        
                        self.doctorDashData = dashData
                        DispatchQueue.main.async {
                            self.tblData.reloadData()
                        }
                    } catch let err {
                        print(err.localizedDescription)
                    }
                    //self.doctorDashData = data
                case .failure(let err):
                    print(err.localizedDescription)
            }
        }
    }
    
    func getBannersData(){
        
        IHProgressHUD.show()
        
        let params = ["user_id":loggedUser!.id] as [String : Any]
        apiManager.getBanners(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                //print("data: \(data)")
                self.arrBanner = data.filter{ ( $0.image_video != "null")}
                
                DispatchQueue.main.async {
                    self.tblData.dataSource = self
                    self.tblData.delegate = self
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }

    @IBAction func ordersAction(_ sender: UIButton) {
        
        let tag = sender.tag
        
        self.navOrders(tag: tag)
    }
    
    func navOrders(tag: Int) {
      
        //DiagnoAppointmentsVC
        
        if tag == 1 || tag == 2 {
            
            let vc = self.getVC(with: "DiagnoAppointmentsVC", sb: IDDiagno) as! AppointmentsVC
            vc.diagnoFlag = true
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
        if tag == 3 {
            
            //PatientHistoryVC
            let vc = self.getVC(with: "DiagnoReportListVC", sb: IDDiagno) as! DiagnoReportListVC
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DignoDashboardVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = DoctorDashCell()
        switch indexPath.section {
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DoctorDashCell
            
            cell.lblShopName.text = loggedUser?.clinic_lab_hospital_name
            cell.lblName.text = loggedUser?.name
            cell.lblMob.text = loggedUser?.mobile_no
            
            cell.contView.layer.cornerRadius = 5
            cell.contView.layer.borderColor = UIColor.lightGray.cgColor
            cell.contView.layer.borderWidth = 1.0
            
            return cell
            
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ordersCell") as! DoctorDashCell
            
            cell.lblShopName.text = String(self.doctorDashData?.todays_appointment ?? 0)
              
            if let btnPending = cell.contentView.viewWithTag(1) as? UIButton {
                
                btnPending.addTarget(self, action: #selector(ordersAction(_:)), for: .touchUpInside)
            }
            if let btnPendingDel = cell.contentView.viewWithTag(2) as? UIButton {
                
                btnPendingDel.addTarget(self, action: #selector(ordersAction(_:)), for: .touchUpInside)
            }
            if let btnDelivered = cell.contentView.viewWithTag(3) as? UIButton {
                
                btnDelivered.addTarget(self, action: #selector(ordersAction(_:)), for: .touchUpInside)
            }
            
            cell.setup(id: "ordersCell")
            
            return cell
            
        case 2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "bannerCell") as! DoctorDashCell
            
            //cell.delegate = self
            //cell.bannerArr = self.arrBanner
            cell.bannerCollView.reloadData()
            
            return cell
            
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            return 150
            
        case 1:
            return 250
            
        case 2:
            return 200
            
        default:
            break
        }
        return 0
    }
}
