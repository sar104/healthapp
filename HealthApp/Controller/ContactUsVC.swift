//
//  ContactUsVC.swift
//  HealthApp
//
//  Created by Apple on 04/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class ContactUsVC: BaseVC, AddDoctorCellDelegate {

    @IBOutlet weak var tblData: UITableView!
    
    var email: String = ""
    var comments: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        email = loggedUser!.email!
    }
    

    func addContactUs(){
       
        IHProgressHUD.show()
        
        let params = ["name": loggedUser!.name,
                      "userid": loggedUser!.id, "mobile_no": loggedUser!.mobile_no, "email": email, "comments": comments] as [String:Any]
        
        apiManager.contactUs(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let data):
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                case .failure(let err):
                    print("err: \(err)")
                    self.alert(strTitle: strErr, strMsg: err.localizedDescription)
            }
        }
    }
    
    func validate() -> Bool{
           
        if email.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter email")
            return false
        }
        if comments.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter comments")
            return false
        }
        
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func onValueSelected(type: String, value: String) {
        
        switch type {
        
        case "comment":
            comments = value
        case "email":
            email = value
            
        default:
            break
        }
        /*
        let indexpath = IndexPath(row: row, section: 0)
        self.tblData.reloadRows(at: [indexpath], with: .none)
         */
    }
    func onSaveClicked(){
        
        if self.validate() {
            self.addContactUs()
        }
    }

}
extension ContactUsVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = AddDoctorCell()
        switch indexPath.row {
            
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! AddDoctorCell
            
            let lbltitle = cell.contentView.viewWithTag(1) as! UILabel
            let lblname = cell.contentView.viewWithTag(2) as! UILabel
            
            lbltitle.text = "NAME"
            lblname.text = loggedUser?.name
            
        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! AddDoctorCell
            
            let lbltitle = cell.contentView.viewWithTag(1) as! UILabel
            let lblname = cell.contentView.viewWithTag(2) as! UILabel
            
            lbltitle.text = "MOBILE"
            lblname.text = loggedUser?.mobile_no
            
        case 2:
            cell = tableView.dequeueReusableCell(withIdentifier: "simpleTxtFldCell") as! AddDoctorCell
            cell.delegate = self
            cell.setup(id: "email", placeHolder: "Email")
            //cell.bottomTxtFld.delegate = self
            cell.bottomTxtFld.text = loggedUser?.email
            
        case 3:
            cell = tableView.dequeueReusableCell(withIdentifier: "simpleTxtFldCell") as! AddDoctorCell
            cell.delegate = self
            cell.setup(id: "comment", placeHolder: "Comments")
            //cell.bottomTxtFld.delegate = self
            
        case 4:
            cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! AddDoctorCell
            cell.delegate = self
            
        default:
            break
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 90
    }
    
    //simpleTxtFldCell
}
