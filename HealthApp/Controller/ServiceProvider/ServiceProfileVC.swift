//
//  ServiceProfileVC.swift
//  HealthApp
//
//  Created by Apple on 05/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class ServiceProfileVC: BaseServiceProVC {

    @IBOutlet weak var tblData: UITableView!
    
    var branchFlag: Bool = false
    var doctorData: DetailData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if !branchFlag {
            getProfile()
        } else {
            self.tblData.dataSource = self
            self.tblData.delegate = self
            self.tblData.reloadData()
        }
    }
    

    func getProfile() {
        
        let params = ["id": loggedUser!.id]
        
        IHProgressHUD.show()
        
        apiManager.getDoctorProfile(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
                
                self.doctorData = data
                
                DispatchQueue.main.async {
                    self.tblData.dataSource = self
                    self.tblData.delegate = self
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    @IBAction func editAction(_ sender: UIButton) {
        //UpdateDoctorProfileVC
        let vc = self.getVC(with: "UpdateServiceProfileVC", sb: IDServicePro) as! UpdateServiceProfileVC
        if branchFlag {
            vc.editBranch = true
            vc.doctorData = doctorData
        }
        self.navigationController?.pushViewController(vc, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ServiceProfileVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }
        if section == 1 {
            return 3
        }
        if section == 2 {
            return 5
        }
        if section == 3 {
            return 3
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = ServiceProfileCell()
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! ServiceProfileCell
            
            if let img = doctorData?.shop_image, img != "null" {
                let url = shopImgUrl + img
                cell.imgVw.kf.setImage(with: URL(string: url))
            } else {
                cell.imgVw.image = UIImage(named: "ic_doctor1")
            }
            
            return cell
            
        case 1:
            
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "editCell") as! ServiceProfileCell
                
                cell.lblHead.text = "BUSINESS/COMPANY NAME"
                cell.lblName.text = doctorData?.clinic_lab_hospital_name
                
                cell.btnEdit.addTarget(self, action: #selector(editAction(_:)), for: .touchUpInside)
                
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! ServiceProfileCell
                
                cell.lblHead.text = "TELEPHONE NO"
                if let mob = doctorData?.mobile_no {
                    cell.lblName.text = String(mob)
                }
                
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! ServiceProfileCell
                
                cell.lblHead.text = "ADDRESSS"
                cell.lblName.text = doctorData?.address
                
                return cell
                
            default:
                break
            }
           
        case 2:
        
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! ServiceProfileCell
                
                cell.lblHead.text = "SERVICE CATEGORY"
                cell.lblName.text = doctorData?.specialization
                
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! ServiceProfileCell
                
                cell.lblHead.text = "SERVICE TYPE"
                cell.lblName.text = doctorData?.delivery_status
                
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! ServiceProfileCell
                
                cell.lblHead.text = "COMPANY / PROPRIETOR PROOF TYPE"
                cell.lblName.text = doctorData?.registration_no
                
                return cell
                
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "imgNameCell") as! ServiceProfileCell
                
                cell.lblHead.text = "COMPANY / PROPRIETOR PROOF"
                if let img = doctorData?.gst_no, img != "null" {
                    let url = shopImgUrl + img
                    cell.imgVw.kf.setImage(with: URL(string: url))
                } else {
                    //cell.imgVw.image = UIImage(named: "ic_doctor1")
                }
                
                return cell
               
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! ServiceProfileCell
                
                cell.lblHead.text = "OWNER/DIRECTOR NAME"
                cell.lblName.text = doctorData?.name
                
                return cell
                
            default:
                break
            }
            
        case 3:
            
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! ServiceProfileCell
                
                cell.lblHead.text = "BUSINESS DESCRIPTION"
                cell.lblName.text = doctorData?.about_clinic
                
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! ServiceProfileCell
                
                cell.lblHead.text = "WORKING DAYS"
                cell.lblName.text = doctorData?.off_day
                
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! ServiceProfileCell
                
                cell.lblHead.text = "CONTACT TIMINGS"
                
                var time = ""
                
                if let open = doctorData!.shop_opening_time, let close = doctorData!.shop_closing_time {
                    let time1 = open + " To " + close
                    time.append(time1)
                    time.append("<br>")
                }
                if let open = doctorData!.afternoon_opening_time, let close = doctorData!.afternoon_closing_time {
                    let time2 = open + " To " + close
                    time.append(time2)
                }
                let str = NSAttributedString(htmlString: time)
                cell.lblName.attributedText = str
                
                return cell
                
            default:
                break
            }
            
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            
            return 140
        }
        if indexPath.section == 2 {
            
            if indexPath.row == 3 {
                return 190
            }
            
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "headCell") as! ServiceProfileCell
        
        if section == 1 {
            cell.lblHead.text = "COMPANY INFORMATION"
        }
        if section == 2 {
            cell.lblHead.text = "SUPPORTING DOCUMENTS"
        }
        if section == 3 {
            cell.lblHead.text = "BUSINESS INFORMATION"
        }
        
        return cell.contentView
    }
}

class ServiceProfileCell: BaseCell {
    
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
}
