//
//  ServiceProDashboardVC.swift
//  HealthApp
//
//  Created by Apple on 04/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ServiceProDashboardVC: BaseServiceProVC {

    @IBOutlet weak var tblData: UITableView!
    
    var spDashData: DoctorDashData?
    var arrBanner: [BannerData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ServiceProDashboardVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = DoctorDashCell()
        switch indexPath.section {
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DoctorDashCell
            
            cell.lblShopName.text = loggedUser?.clinic_lab_hospital_name
            cell.lblName.text = loggedUser?.name
            cell.lblMob.text = loggedUser?.mobile_no
            
            cell.contView.layer.cornerRadius = 5
            cell.contView.layer.borderColor = UIColor.lightGray.cgColor
            cell.contView.layer.borderWidth = 1.0
            
            return cell
   
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "bannerCell") as! DoctorDashCell
            
            //cell.delegate = self
            //cell.bannerArr = self.arrBanner
            cell.bannerCollView.reloadData()
            
            return cell
            
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            return 150
            
        case 1:
            return 200
            
        default:
            break
        }
        return 0
    }
}
