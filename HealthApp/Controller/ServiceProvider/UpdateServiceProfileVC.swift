//
//  UpdateServiceProfileVC.swift
//  HealthApp
//
//  Created by Apple on 07/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD
import RadioGroup

class UpdateServiceProfileVC: BaseServiceProVC, UpdateServiceProfileCellDelegate {

    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var lblHead: UILabel!
    
    var doctorData: DetailData?
    var parentData: DetailData?
    var step: Int = 0
    var sectionRows = [1,8,8]
    var splArr: [SpecializationData] = []
    var statesArr:[State] = []
    var cityArr:[City] = []
    var assistantFlag: Bool = true
    var profileImgData: Data?
    var proofImgData: Data?
    let picker = UIImagePickerController()
    var addBranch: Bool = false
    var editBranch: Bool = false
    var isSP: Bool = false
    
    var proofArr = ["PAN Card", "Certificate of Incorporation", "Shops & Establishment License"]
    var category_arr = ["","Fitness/Yoga Instructor","Care Giver","Nutritionist/Dietition","Nurse","Special Service Provider","Other"]
    var type_arr = ["","Shop","Freelancer"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if !addBranch && !editBranch {
            
            lblHead.text = "UPDATE PROFILE"
            getProfile()
            
        } else {
            if addBranch {
                lblHead.text = "ADD SHOP BRANCH"
                doctorData = DetailData()
                self.tblData.dataSource = self
                self.tblData.delegate = self
                self.tblData.reloadData()
            }
            if editBranch {
                lblHead.text = "UPDATE SHOP BRANCH"
                //getProfile()
            }
            
        }
        
        fetchStates()
    }
    
    func getProfile() {
        
        let params = ["id": loggedUser!.id]
        
        IHProgressHUD.show()
        
        apiManager.getDoctorProfile(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
                
                if !self.addBranch && !self.editBranch {
                 self.doctorData = data
                }
                self.parentData = data
                self.assistantFlag = (self.doctorData?.assistant_name != "-" ? true : false)
                
                DispatchQueue.main.async {
                    self.tblData.dataSource = self
                    self.tblData.delegate = self
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func getSpecialization() {
        
        //getSpecialization
        
        IHProgressHUD.show()
        
        apiManager.getSpecialization(params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
                print("splarr: \(data)")
                self.splArr = data
                
                DispatchQueue.main.async {
                    
                    self.tblData.dataSource = self
                    self.tblData.delegate = self
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func fetchStates(){
        
        IHProgressHUD.show()
        apiManager.getStates(params: [:]) { (result) in
         
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                
                do {
                    let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
                    let status = jsonObj["status"] as! Int
                    if status == 1 {
                        if let states = jsonObj["data"]{
                            let statesData = try JSONSerialization.data(withJSONObject: states, options: .prettyPrinted)
                            let json = try JSONDecoder().decode([State].self, from: statesData)
                            self.statesArr = json
                           DispatchQueue.main.async {
                               
                               self.tblData.dataSource = self
                               self.tblData.delegate = self
                               self.tblData.reloadData()
                           }
                        }
                    } else {
                        
                    }
                } catch let err {
                    print("err: \(err)")
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
            
        }
    }
    func fetchCities(){
        
        IHProgressHUD.show()
        let params = ["state": doctorData?.state]
        apiManager.getCities(params: params) { (result) in
         
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                
                do {
                    let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
                    let status = jsonObj["status"] as! Int
                    if status == 1 {
                        if let states = jsonObj["data"]{
                            let statesData = try JSONSerialization.data(withJSONObject: states, options: .prettyPrinted)
                            let json = try JSONDecoder().decode([City].self, from: statesData)
                            self.cityArr = json
                            print("cities: \(json)")
                            DispatchQueue.main.async {
                                self.tblData.reloadData()
                            }
                        }
                    } else {
                        
                    }
                } catch let err {
                    print("err: \(err)")
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
            
        }
    }
    
    func fetchDetailsByPincode(pincode: String){
        
        IHProgressHUD.show()
        let params = ["pincode": pincode]
        apiManager.getDetailsByPincode(params: params) { (result) in
        
           IHProgressHUD.dismiss()
           switch result {
            
           case .success(let data):
            
            do {
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
                let status = jsonObj["status"] as! Int
                if status == 1 {
                    if let states = jsonObj["data"]{
                        let statesData = try JSONSerialization.data(withJSONObject: states, options: .prettyPrinted)
                        let json = try JSONDecoder().decode([PincodeData].self, from: statesData)
                        if json.count > 0 {
                        if let state = json[0].state {
                            self.doctorData?.state = state
                            self.loggedUser?.state = state
                        }
                        if let city = json[0].sub_district {
                            self.doctorData?.city = city
                            self.loggedUser?.city = city
                        }
                        }
                        print("cities: \(json)")
                        DispatchQueue.main.async {
                            self.tblData.reloadData()
                        }
                    }
                } else {
                    
                }
            } catch let err {
                print("err: \(err)")
            }
           case .failure(let err):
           print("err: \(err)")
            
            }
        }
    }
    
    func uploadImage(){
        
        IHProgressHUD.show()
        
        apiManager.uploadImg(image: profileImgData!, name: "image", api: UploadShopImageAPI) { (result) in
         
            IHProgressHUD.dismiss()
            switch result {
                case .success(let data):
                    let imgstr = String(data: data, encoding: .utf8)
                print(imgstr)
                    
                    if((imgstr?.contains("jpeg"))!){
                        self.doctorData?.shop_image = imgstr
                        if self.addBranch {
                            if self.isSP {
                                self.addSPBranchAPI()
                            } else {
                                self.addBranchAPI()
                            }
                        } else if self.editBranch {
                            self.updateProfile()
                        }
                        //self.updateProfile()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
            if !self.addBranch && !self.editBranch {
                self.uploadProofImage()
            }
        }
        
    }
    
    func uploadProofImage(){
        
        IHProgressHUD.show()
        
        apiManager.uploadImg(image: proofImgData!, name: "image", api: UploadShopImageAPI) { (result) in
         
            IHProgressHUD.dismiss()
            switch result {
                case .success(let data):
                    let imgstr = String(data: data, encoding: .utf8)
                print(imgstr)
                    
                    if((imgstr?.contains("jpeg"))!){
                        self.doctorData?.gst_no = imgstr
                        //self.updateProfile()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
            self.updateProfile()
        }
        
    }
   
    func updateProfile() {
        
        IHProgressHUD.show()
        
        var params: [String:Any] = ["id": doctorData!.id!]
        
        
        params["shop_image"] = doctorData!.shop_image ?? ""
        params["name"] = doctorData!.name!
        params["clinic_lab_hospital_name"] = doctorData!.clinic_lab_hospital_name!
        params["mobile_no"] = doctorData!.mobile_no!
        params["email"] = doctorData!.email ?? "null"
        params["registration_no"] = doctorData!.registration_no!
        params["gst_no"] = doctorData!.gst_no!
        params["state"] = doctorData!.state ?? ""
        params["city"] = doctorData!.city ?? ""
        params["address_type"] = doctorData!.address_type ?? ""
        params["address"] = doctorData!.address!
        params["pincode"] = doctorData!.pincode!
        params["off_day"] = doctorData!.off_day!
        params["shop_opening_time"] = doctorData!.shop_opening_time!
        params["shop_closing_time"] = doctorData!.shop_closing_time!
        params["afternoon_opening_time"] = doctorData!.afternoon_opening_time!
        params["afternoon_closing_time"] = doctorData!.afternoon_closing_time!
        params["latitude"] = doctorData!.latitude ?? ""
        params["longitude"] = doctorData!.longitude ?? ""
        
        params["specialization"] = doctorData!.specialization!
        params["delivery_status"] = doctorData!.delivery_status ?? ""
        params["assistant_name"] = (assistantFlag) ? doctorData!.assistant_name ?? "" : "-"
        params["alt_contact_no"] = (assistantFlag) ? doctorData!.alt_contact_no ?? "": "-"
        params["about_clinic"] = doctorData!.about_clinic ?? ""
        params["clinic_lab_hospital_phone_no"] = doctorData?.clinic_lab_hospital_phone_no ?? "-"
        
        print("update profile: \(params)")
        apiManager.updateSPProfile(params: params) { (result) in
        
            IHProgressHUD.dismiss()
            switch result {
                
            case .success(let data):
                DispatchQueue.main.async() {
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                }
                
            case .failure(let err):
                print("err: \(err)")
                DispatchQueue.main.async() {
                    self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
        }
    }
    
    func addBranchAPI() {
        
        IHProgressHUD.show()
        
        var params: [String:Any] = ["parent_id": loggedUser!.id]
        
        params["shop_image"] = doctorData!.shop_image ?? "null"
        params["name"] = doctorData!.name!
        params["clinic_lab_hospital_name"] = doctorData!.clinic_lab_hospital_name!
        params["mobile_no"] = doctorData!.mobile_no!
        params["email"] = doctorData!.email ?? "null"
        params["registration_no"] = doctorData!.registration_no ?? "null"
        params["gst_no"] = doctorData!.gst_no ?? "null"
        params["state"] = doctorData!.state ?? "null"
        params["city"] = doctorData!.city ?? "null"
        params["address_type"] = doctorData!.address_type ?? "null"
        params["address"] = doctorData!.address ?? "null"
        params["pincode"] = doctorData!.pincode ?? "null"
        params["off_day"] = doctorData!.off_day ?? "null"
        params["shop_opening_time"] = doctorData!.shop_opening_time!
        params["shop_closing_time"] = doctorData!.shop_closing_time!
        params["afternoon_opening_time"] = doctorData!.afternoon_opening_time!
        params["afternoon_closing_time"] = doctorData!.afternoon_closing_time!
        params["latitude"] = doctorData!.latitude ?? "null"
        params["longitude"] = doctorData!.longitude ?? "null"
        params["qualification"] = doctorData!.qualification ?? "null"
        params["specialization"] = doctorData!.specialization ?? "null"
        params["delivery_status"] = doctorData!.delivery_status ?? "null"
        params["assistant_name"] = (assistantFlag) ? doctorData!.assistant_name ?? "" : "-"
        params["alt_contact_no"] = (assistantFlag) ? doctorData!.alt_contact_no ?? "": "-"
        params["about_clinic"] = doctorData!.about_clinic ?? "null"
        params["clinic_lab_hospital_phone_no"] = "-"
        
        print("add branch: \(params)")
        
        apiManager.addDiagnoBranch(params: params) { (result) in
        
            IHProgressHUD.dismiss()
            switch result {
                
            case .success(let data):
                print("result: \(data)")
                DispatchQueue.main.async() {
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                }
                
            case .failure(let err):
                print("err: \(err)")
                DispatchQueue.main.async() {
                    self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
        }
    }
    
    func addSPBranchAPI() {
        
        IHProgressHUD.show()
        
        var params: [String:Any] = ["parent_id": loggedUser!.id]
        
        params["shop_image"] = doctorData!.shop_image ?? "null"
        params["name"] = doctorData!.name!
        params["clinic_lab_hospital_name"] = doctorData!.clinic_lab_hospital_name!
        params["mobile_no"] = doctorData!.mobile_no!
        params["email"] = doctorData!.email ?? "null"
        params["registration_no"] = doctorData!.registration_no ?? "null"
        params["gst_no"] = doctorData!.gst_no ?? "null"
        params["state"] = doctorData!.state ?? "null"
        params["city"] = doctorData!.city ?? "null"
        params["address_type"] = doctorData!.address_type ?? "null"
        params["address"] = doctorData!.address ?? "null"
        params["pincode"] = doctorData!.pincode ?? "null"
        params["off_day"] = doctorData!.off_day ?? "null"
        params["shop_opening_time"] = doctorData!.shop_opening_time!
        params["shop_closing_time"] = doctorData!.shop_closing_time!
        params["afternoon_opening_time"] = doctorData!.afternoon_opening_time!
        params["afternoon_closing_time"] = doctorData!.afternoon_closing_time!
        params["latitude"] = doctorData!.latitude ?? "null"
        params["longitude"] = doctorData!.longitude ?? "null"
        params["qualification"] = doctorData!.qualification ?? "null"
        params["specialization"] = doctorData!.specialization ?? "null"
        params["delivery_status"] = doctorData!.delivery_status ?? "null"
        params["assistant_name"] = (assistantFlag) ? doctorData!.assistant_name ?? "" : "-"
        params["alt_contact_no"] = (assistantFlag) ? doctorData!.alt_contact_no ?? "": "-"
        params["about_clinic"] = doctorData!.about_clinic ?? "null"
        params["clinic_lab_hospital_phone_no"] = "-"
        
        print("add branch: \(params)")
        
        apiManager.addSPBranch(params: params) { (result) in
        
            IHProgressHUD.dismiss()
            switch result {
                
            case .success(let data):
                print("result: \(data)")
                DispatchQueue.main.async() {
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                }
                
            case .failure(let err):
                print("err: \(err)")
                DispatchQueue.main.async() {
                    self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
        }
    }
    
    func validate() -> Bool {
        
        if self.doctorData?.name == nil || self.doctorData!.name!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter name")
            return false
        }
        if self.doctorData?.clinic_lab_hospital_name == nil || self.doctorData!.clinic_lab_hospital_name!.isEmpty {
            if !addBranch && !editBranch {
                self.alert(strTitle: strErr, strMsg: "Please enter business name")
                return false
            }
        }
        if addBranch && (self.doctorData?.mobile_no == nil) {
            self.alert(strTitle: strErr, strMsg: "Please enter branch head mobile number")
            return false
        }
        
        if self.doctorData?.registration_no == nil || self.doctorData!.registration_no!.isEmpty {
            if !addBranch && !editBranch {
                //self.alert(strTitle: strErr, strMsg: "Please enter registration_no")
                //return false
            }
        }/*
        if self.doctorData?.gst_no == nil || self.doctorData!.gst_no!.isEmpty {
            if !addBranch && !editBranch {
                self.alert(strTitle: strErr, strMsg: "Please enter GST number")
                return false
            }
        }*/
        if self.doctorData?.address == nil || self.doctorData!.address!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter address")
            return false
        }
        if self.doctorData?.pincode == nil || self.doctorData!.pincode!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter pincode")
            return false
        }
        /*
        if self.doctorData?.off_day == nil || self.doctorData!.off_day!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select off day")
            return false
        }*/
        if self.doctorData?.shop_opening_time == nil || self.doctorData!.shop_opening_time!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select morning opening time")
            return false
        }
        if self.doctorData?.shop_closing_time == nil || self.doctorData!.shop_closing_time!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select morning closing time")
            return false
        }
        if self.doctorData?.afternoon_opening_time == nil || self.doctorData!.afternoon_opening_time!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select afternoon opening time")
            return false
        }
        if self.doctorData?.afternoon_closing_time == nil || self.doctorData!.afternoon_closing_time!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select afternoon closing time")
            return false
        }
        if assistantFlag {
            if self.doctorData?.assistant_name == nil || self.doctorData!.assistant_name!.isEmpty {
                
                self.alert(strTitle: strErr, strMsg: "Please enter assistant name")
                return false
            }
            if self.doctorData?.alt_contact_no == nil || self.doctorData!.alt_contact_no!.isEmpty {
                
                self.alert(strTitle: strErr, strMsg: "Please enter assistant contact number")
                return false
            }
        }
        
        return true
    }
    
    @IBAction func addBranchAction(_ sender: UIButton) {
        if validate() {
            if addBranch {
                if profileImgData != nil {
                    uploadImage()
                } else {
                    if isSP {
                        addSPBranchAPI()
                    } else {
                        addBranchAPI()
                    }
                }
            }
            
            if editBranch {
                if profileImgData != nil {
                    uploadImage()
                } else {
                    updateProfile()
                }
            }
        }
    }
    
    @IBAction func submitAction(_ sender: UIButton){
        
        if validate() {
            if profileImgData != nil {
                uploadImage()
            } else if proofImgData != nil {
                uploadProofImage()
            }
            else {
                updateProfile()
            }
        }
    }
    
    func onValueSelected(id: String, value: String) {
        
        switch id {
        case "name":
            doctorData?.name = value
            
        case "clinic":
            doctorData?.clinic_lab_hospital_name = value
            
        case "mobile":
            doctorData?.mobile_no = Int(value)
             
        case "phone":
            doctorData?.clinic_lab_hospital_phone_no = value
            
        case "email":
            doctorData?.email = value
            
        case "registration":
            doctorData?.registration_no = value
            
        case "address":
            doctorData?.address = value
            
        case "pobox":
            doctorData?.address_type = value
            
        case "state":
            doctorData?.state = value
            
        case "city":
            doctorData?.city = value
            
        case "category":
            doctorData?.specialization = value
            
        case "service_type":
            doctorData?.delivery_status = value
            
        case "pincode":
            doctorData?.pincode = value
            
        case "offday":
            doctorData?.off_day = value
            
        case "morningtime_open":
            doctorData?.shop_opening_time = value
            
        case "morningtime_close":
            doctorData?.shop_closing_time = value
            
        case "eveningtime_open":
            doctorData?.afternoon_opening_time = value
            
        case "eveningtime_close":
            doctorData?.afternoon_closing_time = value
            
        case "radio":
            assistantFlag = (Int(value) == 0) ? false : true
            //doctorData?.delivery_status = (Int(value) == 0) ? "No" : "Yes"
            if !addBranch {
                let indexpath1 = IndexPath(row: 19, section: 0)
                let indexpath2 = IndexPath(row: 20, section: 0)
                
                tblData.reloadRows(at: [indexpath1,indexpath2], with: .none)
            } else {
                let indexpath1 = IndexPath(row: 2, section: 2)
                let indexpath2 = IndexPath(row: 3, section: 2)
                
                tblData.reloadRows(at: [indexpath1,indexpath2], with: .none)
            }
            
        case "about":
            doctorData?.about_clinic = value
            
        case "assistant":
            doctorData?.assistant_name = value
            
        case "assistant_contact":
            doctorData?.alt_contact_no = value
            
        default:
            break
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UpdateServiceProfileVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        
        if textField.accessibilityIdentifier == "pincode" {
            
            if range.location > 4 && range.length != 1 {
                
                let pincode = textField.text! + string
                self.doctorData?.pincode = pincode
                
                self.fetchDetailsByPincode(pincode: pincode)

            }
        }
        return true
    }
}
extension UpdateServiceProfileVC: UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if !addBranch {
            return 1
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if !addBranch {
            return 22
        }
        return sectionRows[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UpdateServiceProfileCell()
        
        if !addBranch {
            
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! UpdateServiceProfileCell
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchHappen(_:)))
                tap.delegate = self
                tap.accessibilityHint = "shop"
                cell.imgVw.addGestureRecognizer(tap)
                cell.imgVw.isUserInteractionEnabled = true
                
                if profileImgData != nil {
                    cell.imgVw.image = UIImage(data: profileImgData!)
                } else if let img = doctorData?.shop_image, img != "null" {
                    let url = shopImgUrl + img
                    cell.imgVw.kf.setImage(with: URL(string: url))
                } else {
                    cell.imgVw.image = UIImage(named: "ic_doctor1")
                }
                return cell
                
                
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "preTxtCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "name", placeholder: "Owner Name")
                cell.txtFld.text = doctorData?.name
                
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "clinic", placeholder: "Business Name")
                if let clinic = doctorData?.clinic_lab_hospital_name {
                    cell.txtFld.text = clinic
                }
                
                return cell
                
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "phone", placeholder: "Telephone/Landline No.(Optional)")
                if let mob = doctorData?.clinic_lab_hospital_phone_no {
                    cell.txtFld.text = String(mob)
                }
                
                return cell
                
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "email", placeholder: "Email")
                cell.txtFld.text = doctorData?.email
                
                return cell
                
            case 5:
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "address", placeholder: "Address")
                cell.txtFld.text = doctorData?.address
                
                return cell
                
            case 6:
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "pobox", placeholder: "Shop/Office/PO Box No.")
                cell.txtFld.text = doctorData?.address_type
                
                return cell
                
            case 7:
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.txtFld.accessibilityIdentifier = "pincode"
                cell.setup(id: "pincode", placeholder: "Pincode")
                cell.txtFld.text = doctorData?.pincode
                
                return cell
                
            case 8:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "state", placeholder: "State")
                cell.ddTxtFld.text = doctorData?.state
                cell.pickerDataArr = statesArr
                
                return cell
                
            case 9:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "city", placeholder: "City")
                cell.txtFld.text = doctorData?.city
                //cell.pickerDataArr = cityArr
                
                return cell
                
            case 10:
                let cell = tableView.dequeueReusableCell(withIdentifier: "timeCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.lblHead.text = "MORNING TIME"
                cell.setup(id: "morningtime", placeholder: "")
                
                if let time1 = doctorData?.shop_opening_time {
                    cell.txtTime1.text = time1
                }
                if let time2 = doctorData?.shop_closing_time {
                    cell.txtTime2.text = time2
                }
                return cell
                
            case 11:
                let cell = tableView.dequeueReusableCell(withIdentifier: "timeCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.lblHead.text = "EVENING TIME"
                cell.setup(id: "eveningtime", placeholder: "")
                
                if let time1 = doctorData?.afternoon_opening_time {
                    cell.txtTime1.text = time1
                }
                if let time2 = doctorData?.afternoon_closing_time {
                    cell.txtTime2.text = time2
                }
                
                return cell
                
            case 12:
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "offday", placeholder: "Working Days")
                cell.txtFld.text = doctorData?.off_day
                
                return cell
                
            case 13:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "about", placeholder: "Business Description (Maximum 512 characters)")
                cell.txtFld.text = doctorData?.about_clinic
                
                return cell
                
            case 14:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "category", placeholder: "Service Category")
                cell.ddTxtFld.text = doctorData?.specialization
                cell.pickerDataArr = category_arr
                
                return cell
                
            case 15:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "service_type", placeholder: "Service Type")
                cell.ddTxtFld.text = doctorData?.delivery_status
                cell.pickerDataArr = type_arr
                
                return cell
                
            case 16:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "registration", placeholder: "ID Proof Type")
                cell.ddTxtFld.text = doctorData?.registration_no
                cell.pickerDataArr = proofArr
                
                return cell
                
            case 17:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! UpdateServiceProfileCell
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchHappen(_:)))
                tap.delegate = self
                tap.accessibilityHint = "proof"
                
                cell.imgVw.addGestureRecognizer(tap)
                cell.imgVw.isUserInteractionEnabled = true
                
                if proofImgData != nil {
                    cell.imgVw.image = UIImage(data: proofImgData!)
                } else if let img = doctorData?.gst_no, img != "null"  {
                    let url = shopImgUrl + img
                    cell.imgVw.kf.setImage(with: URL(string: url))
                } else {
                    cell.imgVw.image = UIImage(named: "ic_doctor1")
                }
                
                return cell
                
            case 18:
                let cell = tableView.dequeueReusableCell(withIdentifier: "radioCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "radio", placeholder: "")
                cell.radioGroup.selectedIndex = assistantFlag ? 1 : 0
                
                return cell
                
            case 19:
                let cell = tableView.dequeueReusableCell(withIdentifier: "drCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "assistant", placeholder: "Assistant Name")
                cell.txtFld.text = doctorData?.assistant_name
                //cell.pickerDataArr = cityArr
                
                return cell
                
            case 20:
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                cell.setup(id: "assistant_contact", placeholder: "Assistant Contact")
                cell.txtFld.text = doctorData?.alt_contact_no
                
                return cell
                
                
            case 21:
                let cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! UpdateServiceProfileCell
                
                cell.delegate = self
                if addBranch {
                    cell.btnEdit.setTitle("ADD BRANCH", for: .normal)
                    cell.btnEdit.addTarget(self, action: #selector(addBranchAction(_:)), for: .touchUpInside)
                } else if editBranch {
                    cell.btnEdit.setTitle("UPDATE BRANCH", for: .normal)
                    cell.btnEdit.addTarget(self, action: #selector(addBranchAction(_:)), for: .touchUpInside)
                } else {
                    cell.btnEdit.setTitle("UPDATE", for: .normal)
                    cell.btnEdit.addTarget(self, action: #selector(submitAction(_:)), for: .touchUpInside)
                }
                
                
                return cell
                
            default:
                break
                
            }
        } else {
            
            switch indexPath.section {
            case 0:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! UpdateServiceProfileCell
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchHappen(_:)))
                tap.delegate = self
                tap.accessibilityHint = "shop"
                cell.imgVw.addGestureRecognizer(tap)
                cell.imgVw.isUserInteractionEnabled = true
                
                if profileImgData != nil {
                    cell.imgVw.image = UIImage(data: profileImgData!)
                } else if let img = doctorData?.shop_image, img != "null" {
                    let url = shopImgUrl + img
                    cell.imgVw.kf.setImage(with: URL(string: url))
                } else {
                    cell.imgVw.image = UIImage(named: "ic_doctor1")
                }
                return cell
                
            case 1:
               switch indexPath.row {
                    
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "preTxtCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "name", placeholder: "Branch Manager Name")
                    cell.txtFld.text = doctorData?.name
                    
                    return cell
                    
                case 1:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "mobile", placeholder: "Contact Number")
                    if let mobile = doctorData?.mobile_no {
                        cell.txtFld.text = String(mobile)
                    }
                    
                    return cell
                    
                case 2:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "phone", placeholder: "Telephone/Landline No.(Optional)")
                    if let mob = doctorData?.clinic_lab_hospital_phone_no {
                        cell.txtFld.text = String(mob)
                    }
                    
                    return cell
                    
                case 3:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "pobox", placeholder: "Shop/Office/PO Box No.")
                    cell.txtFld.text = doctorData?.address_type
                    
                    return cell
                    
                case 4:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "address", placeholder: "Address")
                    cell.txtFld.text = doctorData?.address_type
                    
                    return cell
                    
                case 5:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.txtFld.accessibilityIdentifier = "pincode"
                    cell.setup(id: "pincode", placeholder: "Pincode")
                    cell.txtFld.text = doctorData?.pincode
                    
                    return cell
                    
                case 6:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "state", placeholder: "State")
                    cell.ddTxtFld.text = doctorData?.state
                    cell.pickerDataArr = statesArr
                    
                    return cell
                    
                case 7:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "city", placeholder: "City")
                    cell.txtFld.text = doctorData?.city
                    //cell.pickerDataArr = cityArr
                    
                    return cell
                default:
                    break
                    
                }
                
            case 2:
                
                switch indexPath.row {
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "clinic", placeholder: "Brand /Outlet/ Branch Name")
                    cell.txtFld.text = doctorData?.city
                    
                    return cell
                    
                case 1:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "radioCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "radio", placeholder: "")
                    cell.radioGroup.selectedIndex = assistantFlag ? 1 : 0
                    
                    return cell
                    
                case 2:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "drCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "assistant", placeholder: "Assistant Name")
                    cell.txtFld.text = doctorData?.assistant_name
                       
                    return cell
                    
                case 3:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "assistant_contact", placeholder: "Assistant Contact")
                    cell.txtFld.text = doctorData?.alt_contact_no
                    
                    return cell
                    
                case 4:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "timeCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.lblHead.text = "MORNING TIME"
                    cell.setup(id: "morningtime", placeholder: "")
                    
                    if let time1 = doctorData?.shop_opening_time {
                        cell.txtTime1.text = time1
                    }
                    if let time2 = doctorData?.shop_closing_time {
                        cell.txtTime2.text = time2
                    }
                    return cell
                    
                case 5:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "timeCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.lblHead.text = "EVENING TIME"
                    cell.setup(id: "eveningtime", placeholder: "")
                    
                    if let time1 = doctorData?.afternoon_opening_time {
                        cell.txtTime1.text = time1
                    }
                    if let time2 = doctorData?.afternoon_closing_time {
                        cell.txtTime2.text = time2
                    }
                    
                    return cell
                    
                case 6:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "offday", placeholder: "Working Days")
                    cell.txtFld.text = doctorData?.off_day
                    
                    return cell
                 
                case 7:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! UpdateServiceProfileCell
                    
                    cell.delegate = self
                    if addBranch {
                        cell.btnEdit.setTitle("ADD BRANCH", for: .normal)
                        cell.btnEdit.addTarget(self, action: #selector(addBranchAction(_:)), for: .touchUpInside)
                    } else if editBranch {
                        cell.btnEdit.setTitle("UPDATE BRANCH", for: .normal)
                        cell.btnEdit.addTarget(self, action: #selector(addBranchAction(_:)), for: .touchUpInside)
                    }
                    
                    return cell
                    
                default:
                    break
                }
                
            default:
                break
            }
        
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "headCell") as! UpdateServiceProfileCell
        
        if section == 1 {
            cell.lblHead.text = "COMPANY INFORMATION"
        }
        if section == 2 {
            cell.lblHead.text = "SUPPORTING DOCUMENTS"
        }
       
        
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if addBranch {
            return (section == 1 || section == 2) ? 40 : 0
        }
        return 0//section == 0 ? 0 : 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if !addBranch {
            
            if indexPath.row == 0 || indexPath.row == 17 {
                return 140
            }
            if indexPath.row == 18 {
                return 110
            }
            if indexPath.row == 10 || indexPath.row == 11 {
                return 100
            }
            if indexPath.row == 19 || indexPath.row == 20 {
                return assistantFlag ? 80 : 0
            }
        } else {
            if indexPath.section == 0 {
                return 140
            }
            if indexPath.section == 2 {
                if indexPath.row == 1 {
                    return 110
                }
                if indexPath.row == 2 || indexPath.row == 3 {
                    return assistantFlag ? 80 : 0
                }
                if indexPath.row == 4 || indexPath.row == 5 {
                    return 100
                }
            }
        }
        return 80//UITableView.automaticDimension
    }
}
extension UpdateServiceProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func touchHappen(_ sender: UITapGestureRecognizer) {
            print("Tap On Image")
            picker.delegate = self
        picker.accessibilityHint = sender.accessibilityHint
        let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {
                action in
                
                self.picker.allowsEditing = true
                self.picker.sourceType = .camera
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                self.present(self.picker, animated: true, completion: nil)
                
            }))
            alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: {
                action in
                
                self.picker.allowsEditing = true
                self.picker.sourceType = .photoLibrary
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                self.present(self.picker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            
            present(alert, animated: true, completion: nil)
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.editedImage] as? UIImage else {
            return
        }
            if picker.accessibilityHint == "shop" {
                profileImgData = image.jpegData(compressionQuality: 0.5)
            }
            if picker.accessibilityHint == "proof" {
                proofImgData = image.jpegData(compressionQuality: 0.5)
            }
            self.tblData.reloadData()
            dismiss(animated: true, completion: nil)
        }

        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
}
protocol UpdateServiceProfileCellDelegate {
    
    func onValueSelected(id: String, value: String)
}
class UpdateServiceProfileCell: BaseCell {
    
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var txtFld: UITextField!
    @IBOutlet weak var ddTxtFld: RightViewTextField!
    @IBOutlet weak var txtTime1: UITextField!
    @IBOutlet weak var txtTime2: UITextField!
    @IBOutlet weak var radioGroup: RadioGroup!
    
    
    
    var selrow = 0
    var delegate: UpdateServiceProfileCellDelegate?
    var seldate: String?
    
    lazy var timePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        //picker.minimumDate = Date()
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
    lazy var timePicker1: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        //picker.minimumDate = Date()
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
    func setup(id: String, placeholder: String) {
        
        if txtFld != nil {
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
        
        if ddTxtFld != nil {
            ddTxtFld.removeBottomBorder()
            ddTxtFld.addBottomBorder()
        }
        
        switch id {
        case "name":
            txtFld.placeholder = placeholder
            
        case "mobile":
            txtFld.placeholder = placeholder
            
        case "pobox":
            txtFld.placeholder = placeholder
            
        case "clinic":
            txtFld.placeholder = placeholder
            
        case "qualification":
            txtFld.placeholder = placeholder
            
        case "specialization":
            ddTxtFld.placeholder = placeholder
            self.cellPicker.dataSource = self
            self.cellPicker.delegate = self
            ddTxtFld.inputView = self.cellPicker
            ddTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            
        case "phone":
            txtFld.placeholder = placeholder
            
        case "email":
            txtFld.placeholder = placeholder
            
        case "registration":
            ddTxtFld.placeholder = placeholder
            self.cellPicker.dataSource = self
            self.cellPicker.delegate = self
            ddTxtFld.inputView = self.cellPicker
            ddTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            
        case "gst":
            txtFld.placeholder = placeholder
            
        case "address":
            txtFld.placeholder = placeholder
            
        case "home":
            txtFld.placeholder = placeholder
            
        case "pincode":
            txtFld.placeholder = placeholder
            
        case "state":
            self.selType = "state"
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            ddTxtFld.inputView = cellPicker
            ddTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            ddTxtFld.placeholder = placeholder
            
        case "category":
            self.selType = "category"
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            ddTxtFld.inputView = cellPicker
            ddTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            ddTxtFld.placeholder = placeholder
            
        case "service_type":
            self.selType = "service_type"
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            ddTxtFld.inputView = cellPicker
            ddTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            ddTxtFld.placeholder = placeholder
            
        case "city":
            
            txtFld.placeholder = placeholder
            
        //case "offday":
            
        case "morningtime":
            
            timePicker.accessibilityIdentifier = "open"
            txtTime1.inputView = timePicker
            txtTime1.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id+"_open")
            txtTime1.placeholder = "Select Time"
            
            timePicker1.accessibilityIdentifier = "close"
            txtTime2.inputView = timePicker1
            txtTime2.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id+"_close")
            txtTime2.placeholder = "Select Time"
            
            let btn1 = self.contentView.viewWithTag(3) as! UIButton
            let btn2 = self.contentView.viewWithTag(4) as! UIButton
            
            btn1.accessibilityIdentifier = id
            btn2.accessibilityIdentifier = id
            
            btn1.addTarget(self, action: #selector(showTimePicker(_:)), for: .touchUpInside)
            btn2.addTarget(self, action: #selector(showTimePicker(_:)), for: .touchUpInside)
            
        case "eveningtime":
            
            timePicker.accessibilityIdentifier = "open"
            txtTime1.inputView = timePicker
            txtTime1.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id+"_open")
            txtTime1.placeholder = "Select Time"
            
            timePicker1.accessibilityIdentifier = "close"
            txtTime2.inputView = timePicker1
            txtTime2.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id+"_close")
            txtTime2.placeholder = "Select Time"
            
            let btn1 = self.contentView.viewWithTag(3) as! UIButton
            let btn2 = self.contentView.viewWithTag(4) as! UIButton
            
            btn1.accessibilityIdentifier = id
            btn2.accessibilityIdentifier = id
            
            btn1.addTarget(self, action: #selector(showTimePicker(_:)), for: .touchUpInside)
            btn2.addTarget(self, action: #selector(showTimePicker(_:)), for: .touchUpInside)
            
            
        case "radio":
            radioGroup.titles = [strKeepYourMobile,strAddAssistant]
            radioGroup.addTarget(self, action: #selector(optionSelected(radioGroup:)), for: .valueChanged)
            
        case "assistant_contact":
            txtFld.placeholder = placeholder
            
        case "assistant":
            txtFld.placeholder = placeholder
            
        case "offday":
            txtFld.placeholder = placeholder
            
        case "about":
            txtFld.placeholder = placeholder
            
        default:
            break
        }
        
        self.layoutIfNeeded()
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selrow = row
        
        let id = pickerView.accessibilityIdentifier
        if id == "state" {
            if let data = pickerDataArr[selrow] as? State {
                ddTxtFld.text = data.state
            }
        }
        if id == "category"  || id == "service_type" || id == "registration" {
            if let data = pickerDataArr[selrow] as? String {
                ddTxtFld.text = data
            }
        }
    }
    
    override func doneCellPicker(sender: UIBarButtonItem) {
        
        let id = sender.accessibilityIdentifier
        
        if id == "state" {
            if let data = pickerDataArr[selrow] as? State {
                ddTxtFld.text = data.state
                delegate?.onValueSelected(id: id!, value: data.state ?? "")
            }
        }
        else if id == "category"  || id == "service_type" || id == "registration" {
            if let data = pickerDataArr[selrow] as? String {
                ddTxtFld.text = data
                delegate?.onValueSelected(id: id!, value: data)
            }
        }
        else if id == "specialization" {
            if let data = pickerDataArr[selrow] as? SpecializationData {
                
                ddTxtFld.text = data.specialization
                delegate?.onValueSelected(id: id!, value: data.specialization!)
            }
        }
        else if id == "morningtime_open" {
            delegate?.onValueSelected(id: id!, value: txtTime1.text ?? "")
        }
        else if id == "morningtime_close" {
            delegate?.onValueSelected(id: id!, value: txtTime2.text ?? "")
        }
        else if id == "eveningtime_open" {
            delegate?.onValueSelected(id: id!, value: txtTime1.text ?? "")
        }
        else if id == "eveningtime_close" {
            delegate?.onValueSelected(id: id!, value: txtTime2.text ?? "")
        } else {
            
            delegate?.onValueSelected(id: id!, value: txtFld.text ?? "")
        }
        self.endEditing(true)
    }
    
    @objc func dateSelected(_ picker: UIDatePicker){
        
        let id = picker.accessibilityIdentifier
        let dateStr = picker.date.toString(format: df_hh_mm_a)//df.string(from: picker.date)
        
        if id == "open" {
            txtTime1.text = dateStr
        }
        if id == "close" {
            txtTime2.text = dateStr
        }
        
        seldate = dateStr
    }
    
    @IBAction func offDayAction(_ sender: UIButton) {
        
        let tag = sender.tag
        
        for i in 1...7 {
            let btn = self.contentView.viewWithTag(i) as! UIButton
            btn.backgroundColor = .white
            btn.isSelected = false
        }
        
        let btn = self.contentView.viewWithTag(tag) as! UIButton
        btn.backgroundColor = .black
        btn.isSelected = true
        
        delegate?.onValueSelected(id: "offday", value: btn.accessibilityIdentifier!)
    }
    
    @IBAction func showTimePicker(_ sender: UIButton) {
        
        let tag = sender.tag
        //let id = sender.accessibilityIdentifier
        
        if tag == 3 {
            txtTime1.becomeFirstResponder()
        }
        if tag == 4 {
            txtTime2.becomeFirstResponder()
        }
    }
    
    @objc func optionSelected(radioGroup: RadioGroup){
        
        delegate?.onValueSelected(id: "radio", value: String(radioGroup.selectedIndex))
    }
}
