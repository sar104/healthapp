//
//  UserRatingsVC.swift
//  HealthApp
//
//  Created by Apple on 14/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

struct RatingData: Codable {
    
}
class UserRatingsVC: BaseServiceProVC {

    @IBOutlet weak var tblData: UITableView!
    var ratingArr: [RatingData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getRatings()
    }
    
    func getRatings() {
        
        IHProgressHUD.show()
        
        let params = ["user_id": loggedUser!.id]
        apiManager.getAllRatings(params: params) { (result) in
         
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
             
                //self.ratingArr = data
                DispatchQueue.main.async {
                    
                    //self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UserRatingsVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ratingArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ratingCell") as! DiagnoBranchListCell
        
            return cell
        }
}
