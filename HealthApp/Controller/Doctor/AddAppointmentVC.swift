//
//  AddAppointmentVC.swift
//  HealthApp
//
//  Created by Apple on 10/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class AddAppointmentVC: BaseDoctorVC, AddAppointmentCellDelegate, UITextFieldDelegate {
    

    @IBOutlet weak var tblData: UITableView!
    
    var mobile: String?
    var user_id: String?
    var name: String?
    var age: String?
    var purpose: String?
    var apt_date: String?
    var apt_time: String?
    var notes: String?
    var aptData: AppointmentData?
    var editFlag: Bool = false
    var prescId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if editFlag {
            self.prescId = aptData?.getPrescrID()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tblData.reloadData()
    }
    
    func validate() -> Bool {
        
        if self.mobile == nil || self.mobile!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter patient's mobile number")
            return false
        }
        if self.name == nil || self.name!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter patient's name")
            return false
        }
        if self.age == nil || self.age!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter patient's age")
            return false
        }
        if self.purpose == nil || self.purpose!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter reason for appointment")
            return false
        }
        if self.apt_date == nil || self.apt_date!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select appointment date")
            return false
        }
        if self.apt_time == nil || self.apt_time!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select appointment time")
            return false
        }
        
        return true
    }
    
    func updateAppointment() {
        
        IHProgressHUD.show()
        
        do {
        var params = ["doctor_id": loggedUser!.id]  as [String : Any]
        
            let apt_for = ["appointment_for": aptData?.appointment_for ?? "", "prescription_id": prescId ?? "null"]
        
        let data = try JSONEncoder().encode(apt_for)
        let jsonStr = String(data: data, encoding: .utf8)
        print("jsonStr: \(jsonStr)")
            
            params["id"] = self.aptData!.id!
        params["user_id"] = self.user_id
            params["patient_name"] = self.aptData?.patient_name
        params["patient_mobile"] = self.aptData?.patient_mobile
            params["patient_age"] = self.aptData?.patient_age
        params["patient_location"] = self.notes
        params["appointment_for"] = jsonStr//["appointment_for": self.purpose ?? "", "prescription_id": "null"]//reason for appointment(its a json with appointement_for and prescription_id)
            params["appointment_date"] = self.aptData?.appointment_date
            params["appointment_time"] = self.aptData?.appointment_time
        
        print("params:\(params)")
        
        apiManager.updateAppointment(params: params) { (result) in
               
                   IHProgressHUD.dismiss()
                   switch result {
                       
                   case .success(let data):
                       DispatchQueue.main.async() {
                           self.alert(strTitle: strSuccess, strMsg: data.message!)
                       }
                       
                   case .failure(let err):
                       print("err: \(err)")
                       DispatchQueue.main.async() {
                           self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                       }
                   }
               }
        } catch let err {
            print(err)
        }
    }
    
    func addAppointment() {
        
        IHProgressHUD.show()
        
        do {
        var params = ["doctor_id": loggedUser!.id]  as [String : Any]
        
        let apt_for = ["appointment_for": self.purpose ?? "", "prescription_id": "null"]
        
        let data = try JSONEncoder().encode(apt_for)
        let jsonStr = String(data: data, encoding: .utf8)
        print("jsonStr: \(jsonStr)")
            
        params["user_id"] = self.user_id
        params["patient_name"] = self.name
        params["patient_mobile"] = self.mobile
        params["patient_age"] = self.age
        params["patient_location"] = self.notes
        params["appointment_for"] = jsonStr//["appointment_for": self.purpose ?? "", "prescription_id": "null"]//reason for appointment(its a json with appointement_for and prescription_id)
        params["appointment_date"] = self.apt_date
        params["appointment_time"] = self.apt_time
        
        print("params:\(params)")
        
        apiManager.addAppointment(params: params) { (result) in
               
                   IHProgressHUD.dismiss()
                   switch result {
                       
                   case .success(let data):
                       DispatchQueue.main.async() {
                           self.alert(strTitle: strSuccess, strMsg: data.message!)
                       }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.navigationController?.popViewController(animated: false)
                        }
                       
                   case .failure(let err):
                       print("err: \(err)")
                       DispatchQueue.main.async() {
                           self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                       }
                   }
               }
        } catch let err {
            print(err)
        }
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        
        if validate() {
            if editFlag {
                updateAppointment()
            } else {
                addAppointment()
            }
        }
    }
    
    func onValueSelected(id: String, value: String) {
        
        switch id {
        case "name":
            self.name = value
            
        case "age":
            self.age = value
            
        case "purpose":
            self.purpose = value
            if editFlag {
                self.aptData?.appointment_for = value
            }
            
        case "date":
            self.apt_date = value
            if editFlag {
                self.aptData?.appointment_date = value
            }
            
        case "time":
            self.apt_time = value
            if editFlag {
                self.aptData?.appointment_time = value
            }
            
        case "notes":
            self.notes = value
            if editFlag {
                self.aptData?.patient_location = value
            }
            
        default:
            break
        }
    }
    
    @objc func getUserByMobile(text: String) {
        
        //if let mobile = text {
        
               
            IHProgressHUD.show()
            
            let params = ["mobile_no": text]  as [String : Any]
            print("params:\(params)")
            
            apiManager.getUserByMobile(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                case .success(let data):
                    print("data: \(data)")
                    
                    self.user_id = String(data.id!)
                    
                    self.name = data.name
                    if self.editFlag {
                        self.aptData?.patient_name = data.name!
                    }
                    if let dob = data.dob {
                        self.age = String(dob.toDate(format:df_yyyy_MM_dd).getDiff())
                        if self.editFlag {
                            self.aptData?.patient_age = self.age
                        }
                    }
                    DispatchQueue.main.async {
                        
                            self.tblData.reloadData()
                        
                    }
                case .failure(let err):
                    print("err: \(err)")
                    
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                }
            }
        //}
    }
       
       func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
           replacementString string: String) -> Bool {
           
           if textField.accessibilityIdentifier == "mobile" {
               print("string: \(string)")
          
                
               if range.location == 9 && range.length != 1 {
                   let mob = textField.text! + string
                    self.mobile = mob
                if editFlag {
                    self.aptData?.patient_mobile = mob
                }
                   self.getUserByMobile(text: mob)

               }
           }
           return true
       }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddAppointmentVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = AddAppointmentCell()
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! AddAppointmentCell
            
            cell.delegate = self
            cell.txtFld.accessibilityIdentifier = "mobile"
            cell.txtFld.delegate = self
            cell.setup(id: "mobile", placeholder: "Patient's Mobile No")
            cell.txtFld.text = editFlag ? aptData?.patient_mobile : self.mobile
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! AddAppointmentCell
            
            cell.delegate = self
            cell.setup(id: "name", placeholder: "Patient's Name")
            cell.txtFld.text = editFlag ? aptData?.patient_name :self.name
            
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! AddAppointmentCell
            
            cell.delegate = self
            cell.setup(id: "age", placeholder: "Patient's Age")
            cell.txtFld.text = editFlag ? aptData?.patient_age : self.age
            
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! AddAppointmentCell
            
            cell.delegate = self
            cell.setup(id: "purpose", placeholder: "Appointment For")
            cell.txtFld.text = editFlag ? aptData?.getReason() : self.purpose
            
            return cell
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "timeCell") as! AddAppointmentCell
            
            cell.delegate = self
            cell.setup(id: "time", placeholder: "")
            cell.txtTime1.text = editFlag ? aptData?.appointment_date : self.apt_date
            cell.txtTime2.text = editFlag ? aptData?.appointment_time : self.apt_time
            
            return cell
            
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! AddAppointmentCell
            
            cell.delegate = self
            cell.setup(id: "notes", placeholder: "Notes")
            cell.txtFld.text = editFlag ? aptData?.patient_location : self.notes
            
            return cell
            
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! AddAppointmentCell
            
            //submitAction
            cell.btnSubmit.addTarget(self, action: #selector(submitAction), for: .touchUpInside)
            
            if editFlag {
                cell.btnSubmit.setTitle("Update Appointment", for: .normal)
            }
            
            return cell
            
        default:
            break
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 4 {
            return 100
        }
        return 70
    }
}

protocol AddAppointmentCellDelegate {
    
    func onValueSelected(id: String, value: String)
}
class AddAppointmentCell: BaseCell {
    
    @IBOutlet weak var txtFld: UITextField!
    @IBOutlet weak var txtTime1: UITextField!
    @IBOutlet weak var txtTime2: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        //picker.minimumDate = Date()
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
    lazy var timePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        //picker.minimumDate = Date()
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
    var delegate: AddAppointmentCellDelegate?
    
    func setup(id: String, placeholder: String) {
        
        if txtFld != nil {
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            
            txtFld.placeholder = placeholder
        }
        
        if id == "time" {
            
            datePicker.accessibilityIdentifier = "date"
            txtTime1.inputView = datePicker
            txtTime1.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "date")
            //txtTime1.placeholder = "Select Time"
            
            timePicker.accessibilityIdentifier = "time"
            txtTime2.inputView = timePicker
            txtTime2.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "time")
            //txtTime2.placeholder = "Select Time"
        }
        
        self.layoutIfNeeded()
    }
    
    @objc func dateSelected(_ picker: UIDatePicker){
        
        let id = picker.accessibilityIdentifier
        //df.string(from: picker.date)
        
        if id == "date" {
            let dateStr = picker.date.toString(format: df_dd_MM_yyyy)
            txtTime1.text = dateStr
        }
        if id == "time" {
            let dateStr = picker.date.toString(format: df_hh_mm_a)
            txtTime2.text = dateStr
        }
    }
    
    override func doneCellPicker(sender: UIBarButtonItem) {
           
        let id = sender.accessibilityIdentifier
        
        if id == "date" {
            
            delegate?.onValueSelected(id: id!, value: txtTime1.text!)
            
        } else if id == "time" {
            
            delegate?.onValueSelected(id: id!, value: txtTime2.text!)
            
        } else {
            
            delegate?.onValueSelected(id: id!, value: txtFld.text!)
            
        }
        self.endEditing(true)
    }
}
