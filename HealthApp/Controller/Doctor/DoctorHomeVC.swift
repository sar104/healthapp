//
//  DoctorHomeVC.swift
//  HealthApp
//
//  Created by Apple on 07/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class DoctorHomeVC: UITabBarController {

    @IBOutlet weak var menuBtn: UIBarButtonItem!
      //@IBOutlet weak var rightBtn: UIBarButtonItem!
    
      var menuSlide : VKSideMenu =  VKSideMenu.init()
      var menuArray : [VKSideMenuItem] = []
      var menuSectionArr: [[VKSideMenuItem]] = []
      var sectionTitleArr: [String] = []
      var loggedUser: User?
      lazy var coreDataManager: CoreDataManager = {
          let manager = CoreDataManager.shared
          return manager
      }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let users = self.fetchUser()
        if users.count > 0 {
            loggedUser = users[0]
        }
        
        if #available(iOS 13.0, *) {
            self.setTabBackground()
        } else {
            // Fallback on earlier versions
            UITabBar.appearance().backgroundColor = .black
        }
        
        self.setProfilePhoto()
        self.initSideMenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.isKind(of: DoctorDashboardVC.self) && !self.isKind(of: HealthWellnessVC.self) {
            
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            
            setNavItems()
            if let tabbarvc = self.tabBarController as? HomeVC {
                
                tabbarvc.navigationController?.setNavigationBarHidden(true, animated: false)
                
            }
        } else {
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            
            showBackButton()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if !self.isKind(of: DoctorDashboardVC.self) && !self.isKind(of: HealthWellnessVC.self)   {
            
            //self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.navigationController?.toolbarItems = nil
            if let tabbarvc = self.tabBarController as? HomeVC {
                
                tabbarvc.navigationController?.setNavigationBarHidden(false, animated: false)
            }
        } else {
            
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
    }
    
    
    
    @available(iOS 13.0, *)
    func setTabBackground() {
        let appearance = UITabBarAppearance()
        appearance.backgroundColor = .black
        tabBar.standardAppearance = appearance
    }
    
    func fetchUser() -> [User] {
        let users = self.coreDataManager.fetchLocalUsers()
        //self.coreDataService?.fetchLocalUsers() ?? []
        //print(user)
        return users
    }
    
    func setNavItems() {
        let logoImg = UIImage(named:"Logo")
        let imgView = UIImageView(image: logoImg)
        imgView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        imgView.center = (self.navigationController?.navigationBar.center)!
        
        imgView.contentMode = .scaleAspectFit
        navigationItem.titleView = imgView
        
        setBellIcon()
    }
    
    func setBellIcon() {
        let imgview = UIImageView()
        imgview.frame = CGRect(x: 0, y: 0, width: 23, height: 20)
        imgview.image = UIImage(named: "bell")
        
        let widthConstraint = imgview.widthAnchor.constraint(equalToConstant: 23)
        let heightConstraint = imgview.heightAnchor.constraint(equalToConstant: 20)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        
        let barButton = UIBarButtonItem(customView: imgview)
        //assign button to navigationbar
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    func showBackButton() {
        
        let imgview = UIImage()
        
        self.navigationController?.navigationBar.backIndicatorImage = imgview
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgview
        //self.navigationController?.navigationBar.backItem?.title = ""
        
        //self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        
        //self.navigationController?.navigationBar.tintColor = UIColor.clear
    }
    @objc func setProfilePhoto() {
        
        let users = self.coreDataManager.fetchLocalUsers()
        loggedUser = users[0]
        
        
        let logoImg = UIImage(named:"Logo")
        let imgView = UIImageView(image: logoImg)
        imgView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        imgView.center = (self.navigationController?.navigationBar.center)!
        
        imgView.contentMode = .scaleAspectFit
        navigationItem.titleView = imgView
        
        
        let image = UIImage(named: "Menu")
        let renderImg = image?.withRenderingMode(.alwaysOriginal)
        menuBtn.image = renderImg
        
        
    }
    
    @IBAction func menuAction(_ sender: Any) {
        menuSlide.show()
    }
    
    func initSideMenu(){
        
        if menuSlide != nil
        {
            //self.menuSlide =  VKSideMenu.init(direction: VKSideMenu.VKSideMenuDirection.fromLeft)
            menuSlide = VKSideMenu.init(size: 300, andDirection: VKSideMenu.VKSideMenuDirection.fromLeft)
            menuSlide.dataSource = self
            menuSlide.delegate = self
            menuSlide.textColor = UIColor.white
            menuSlide.backgroundColor = UIColor.red
            menuSlide.isEnableOverlay = false
            menuSlide.isHideOnSelection = true
            menuSlide.animationDuration = Int(0.8)
            menuSlide.selectionColor = UIColor(white: 0.0, alpha: 0.3)
            menuSlide.iconsColor = nil
            menuSlide.backgroundColor = UIColor.black
        }
        
        var options = [AnyHashable]()
        options.append(MENU_HOME)
        options.append(MENU_PROFILE)
        options.append(MENU_CLINIC_BRANCHES)
        options.append(MENU_APPOINTMENTS)
        options.append(MENU_RATINGS)
        options.append(MENU_CONTACT)
        options.append(MENU_HELP)
        options.append(MENU_LOGOUT)
        
        var sectionArr: [[AnyHashable]] = []
        sectionArr.append(options)
        
        menuSectionArr.append([VKSideMenuItem()])
        for section in sectionArr {
            menuArray = []
            for option in section {
                let item = VKSideMenuItem()
                item.title = option as! String
                if let icon = getMenuIcon(name: option as! String) {
                    item.icon = UIImage(named: icon)
                }
                menuArray.append(item)
            }
            menuSectionArr.append(menuArray)
        }
        sectionTitleArr.append("")
        sectionTitleArr.append("")
        
    }
    
    func getMenuIcon(name: String)-> String? {
        
        switch name {
        case MENU_HOME:
            return ICON_HOME
                
        case MENU_CLINIC_BRANCHES:
            return ICON_BRANCH
            
        case MENU_APPOINTMENTS:
            return ICON_BRANCH
            
        case MENU_PROFILE:
            return ICON_PROFILE
            
        case MENU_RATINGS:
            return ICON_RATING
            
        case MENU_CONTACT:
            return ICON_CONTACT
            
        case MENU_HELP:
            return ICON_HELP
            
        case MENU_LOGOUT:
            return ICON_LOGOUT
            
            
        default:
            break
        }
        
        return ""
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DoctorHomeVC: VKSideMenuDelegate, VKSideMenuDataSource {
    
    //:MARKk -Side menu delegate
       
       func numberOfSections(in sideMenu: VKSideMenu) -> Int {
        return menuSectionArr.count
       }
       
       func sideMenu(_ sideMenu: VKSideMenu, numberOfRowsInSection section: Int) -> Int {
           let arr = menuSectionArr[section]
           return section == 0 ? 1 : arr.count
       }
       
        func sideMenu(_ sideMenu: VKSideMenu, titleForHeaderInSection section: Int) -> String {
        
            return sectionTitleArr[section]
        }
        
       func sideMenu(_ sideMenu: VKSideMenu, itemForRowAt indexPath: IndexPath) -> VKSideMenuItem {
           // This solution is provided for DEMO propose only
           // It's beter to store all items in separate arrays like you do it in your UITableView's. Right?
           
           var item = VKSideMenuItem()
           if indexPath.section == 0 {
            let title = loggedUser?.name
               let code = ""
            let mobile = loggedUser?.mobile_no
                
            item.title = title!
            item.desc = mobile!
            if let imgurl = loggedUser?.profile_image {
                let url = imgBaseURL + custProfileImg + imgurl
                item.imgurl = url
            }
            
                
           }
           else {
            let menuArray = menuSectionArr[indexPath.section]
               item = menuArray[indexPath.row]
           }
           return item
       }
       
       func sideMenu(_ sideMenu: VKSideMenu, heightForRowAt indexPath: IndexPath) -> CGFloat {
           if indexPath.section == 0 {
               return 100.0
           }
           else {
               return 40.0
           }
       }
       
       // MARK: - VKSideMenuDelegate
       @objc func sideMenu(_ sideMenu: VKSideMenu, didSelectRowAt indexPath: IndexPath) {
        print("Mene selected")
        var item = VKSideMenuItem()
        
        if indexPath.section == 1 {
            let menuArray = menuSectionArr[indexPath.section]
            item = menuArray[indexPath.row]
               
            if item.title == MENU_HOME {
                //
                let sb = UIStoryboard(name: IDDoctor, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "DoctorDashboardVC") as! DoctorDashboardVC
                
                self.navigationController?.pushViewController(vc, animated: false)
            }
            //ClinicBranchListVC
            if item.title == MENU_CLINIC_BRANCHES {
                
                let sb = UIStoryboard(name: IDDrUpdateProfile, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "ClinicBranchListVC") as! ClinicBranchListVC
                
                self.navigationController?.pushViewController(vc, animated: false)
            }
            //AppointmentsVC
            
            if item.title == MENU_APPOINTMENTS {
                
                let sb = UIStoryboard(name: IDDrAppointment, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "AppointmentsVC") as! AppointmentsVC
                
                self.navigationController?.pushViewController(vc, animated: false)
            }
            if item.title == MENU_RATINGS {
                
                let sb = UIStoryboard(name: IDServicePro, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "UserRatingsVC") as! UserRatingsVC
                self.navigationController?.pushViewController(vc, animated: false)
                
            }
            if item.title == MENU_HELP {
                //MyAlertsVC
                let sb = UIStoryboard(name: IDDoctor, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "DoctorFAQVC") as! FAQVC
                
                self.navigationController?.pushViewController(vc, animated: false)
            }
            if item.title == MENU_CONTACT {
                //MyAlertsVC
                let sb = UIStoryboard(name: IDDoctor, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "DoctorContactUsVC") as! ContactUsVC
                
                self.navigationController?.pushViewController(vc, animated: false)
            }
            if item.title == MENU_LOGOUT {
                
                self.alertAction(strTitle: "Alert", strMsg: "Do you want to logout?") { (action) in
                    
                    if let title = action.title, title == "Yes" {
                        self.clearUserData()
                        NotificationCenter.default.post(.init(name: Notification.Name(rawValue: LoginNotification)))
                    }
                }
            }
        }
        
       }
    
    func clearUserData() {
        
        UserDefaults.standard.set("0", forKey: "role")
        UserDefaults.standard.set("", forKey: "deviceId")
        UserDefaults.standard.set(0, forKey: "newuser")
        UserDefaults.standard.set("", forKey: "profile_completed")
        UserDefaults.standard.set(0, forKey: "HealthRecordSegment")
        UserDefaults.standard.set(0, forKey: "OrderSegment")
        UserDefaults.standard.set(0, forKey: "doctor_id")
        //
        
        
        
        let users = self.coreDataManager.fetchLocalUsers()
        let user = users[0]
        let id = user.id
        self.coreDataManager.removeUser(id: Int(id))
    }
       
       func editOnHeaderSideMenu(){

            let navVC = self.selectedViewController as! UINavigationController
            //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
            let sb = UIStoryboard(name: IDMain, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "UpdateProfileVC")
            navVC.pushViewController(vc, animated: false)
       }
    
    func viewOnHeaderSideMenu() {
        let navVC = self.selectedViewController as! UINavigationController
        //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
        let sb = UIStoryboard(name: IDMain, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "ProfileVC")
        navVC.pushViewController(vc, animated: false)
        
    }

       @objc func sideMenuDidShow(_ sideMenu: VKSideMenu)
       {
           
       }
       @objc func sideMenuDidHide(_ sideMenu: VKSideMenu)
       {
       }
    
       func populateVKSlideMenuData(_ mArray: [String]) -> [VKSideMenuItem] {
           var temp = [VKSideMenuItem]()
           for menu: String in mArray {
               let item = VKSideMenuItem()

           }
           return temp
       }
}
