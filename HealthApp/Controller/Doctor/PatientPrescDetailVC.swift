//
//  PatientPrescDetailVC.swift
//  HealthApp
//
//  Created by Apple on 30/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class PatientPrescDetailVC: BaseDoctorVC {

    @IBOutlet weak var tblData: UITableView!
    
    var selectedMedArr: [MedicineData] = []
    var prescData: PrescriptionData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if prescData != nil {
            selectedMedArr = prescData!.getDrugsArr()
        }
    }
    

    @IBAction func editAction(_ sender: UIButton) {
        
        let vc = self.getVC(with: "AttachPrescriptionVC", sb: IDDrAppointment) as! AttachPrescriptionVC
        vc.editFlag = true
        vc.prescData = prescData
        self.navigationController?.pushViewController(vc, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PatientPrescDetailVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
    
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 4
        }
        if section == 1 {
            return selectedMedArr.count
        }
        if section == 2 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = AttachPrescriptionCell()
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AttachPrescriptionCell
                
                cell.lblHead.text = "PURPOSE OF VISIT"
                cell.lblMedName.text = prescData?.illness_type
                
                return cell
            
            case 1:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AttachPrescriptionCell
                
                cell.lblHead.text = "PATIENT"
                cell.lblMedName.text = prescData?.user_name
                
                return cell
                
            case 2:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AttachPrescriptionCell
                
                cell.lblHead.text = "PRESCRIPTION DATE"
                cell.lblMedName.text = prescData?.illness_date
                
                return cell
                
            case 3:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AttachPrescriptionCell
                
                cell.lblHead.text = "DOCTOR"
                cell.lblMedName.text = prescData?.doctor_name
                
                return cell
            default:
                break
            }
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "medListCell") as! AttachPrescriptionCell
            
            if selectedMedArr.count > 0 {
                let medicine = selectedMedArr[indexPath.row]
                cell.lblMedName.text = medicine.medicine_name
                cell.stepper.tag = indexPath.row
                cell.stepper.count = CGFloat(medicine.quantity)
                
            }
            
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! AttachPrescriptionCell
            
            if let presImgUrl = prescData?.getImage() {
                if presImgUrl.contains("pdf") {
                    cell.imgVw.image = UIImage(named: "pdf")
                } else {
                    let imgURL = prescriptionImageURL + presImgUrl
                    cell.imgVw.kf.setImage(with: URL(string: imgURL))
                }
            }
            return cell
            
        default:
            break
        }
        return cell
    }
}
