//
//  DoctorProfileVC.swift
//  HealthApp
//
//  Created by Apple on 07/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class DoctorProfileVC: BaseDoctorVC {
    
    @IBOutlet weak var tblData: UITableView!
    
    var doctorData: DetailData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getProfile()
    }
    
    func getProfile() {
        
        let params = ["id": loggedUser!.id]
        
        IHProgressHUD.show()
        
        apiManager.getDoctorProfile(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
                
                self.doctorData = data
                
                DispatchQueue.main.async {
                    self.tblData.dataSource = self
                    self.tblData.delegate = self
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    @IBAction func editAction(_ sender: UIButton) {
        //UpdateDoctorProfileVC
        let vc = self.getVC(with: "UpdateDoctorProfileVC", sb: IDDrUpdateProfile)
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
}

extension DoctorProfileVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 14
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = DoctorProfileCell()
        
        switch indexPath.row {
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! DoctorProfileCell
            
            if let img = doctorData?.shop_image {
                let url = doctorImgUrl + img
                cell.imgVw.kf.setImage(with: URL(string: url))
            } else {
                cell.imgVw.image = UIImage(named: "ic_doctor1")
            }
            return cell
            
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "editCell") as! DoctorProfileCell
            
            cell.lblHead.text = "CLINIC/HOSPITAL NAME"
            cell.lblName.text = doctorData?.clinic_lab_hospital_name
            
            cell.btnEdit.addTarget(self, action: #selector(editAction(_:)), for: .touchUpInside)
            return cell
            
        case 2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DoctorProfileCell
            
            cell.lblHead.text = "DOCTOR NAME"
            cell.lblName.text = doctorData?.name
            
            return cell
            
        case 3:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DoctorProfileCell
            
            cell.lblHead.text = "TELEPHONE NO"
            if let mob = doctorData?.mobile_no {
                cell.lblName.text = String(mob)
            }
            
            return cell
            
        case 4:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DoctorProfileCell
            
            cell.lblHead.text = "EMAIL"
            cell.lblName.text = doctorData?.email
            
            return cell
            
        case 5:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DoctorProfileCell
            
            cell.lblHead.text = "QUALIFICATION"
            cell.lblName.text = doctorData?.qualification
            
            return cell
            
        case 6:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DoctorProfileCell
            
            cell.lblHead.text = "SPECIALIZATION"
            cell.lblName.text = doctorData?.specialization
            
            return cell
            
        case 7:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DoctorProfileCell
            
            cell.lblHead.text = "ADDRESS"
            cell.lblName.text = doctorData?.address
        
            return cell
            
        case 8:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DoctorProfileCell
            
            cell.lblHead.text = "MCI REGISTRATION NUMBER"
            cell.lblName.text = doctorData?.registration_no
            
            return cell
            
        case 9:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DoctorProfileCell
            
            cell.lblHead.text = "GST"
            cell.lblName.text = doctorData?.gst_no
            
            return cell
            
        case 10:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DoctorProfileCell
            
            cell.lblHead.text = "OFF DAY"
            cell.lblName.text = doctorData?.off_day
            
            return cell
            
        case 11:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DoctorProfileCell
            
            cell.lblHead.text = "TIME"
            let time1 = doctorData!.shop_opening_time! + " To " + doctorData!.shop_closing_time!
            let time2 = doctorData!.afternoon_opening_time! + " To " + doctorData!.afternoon_closing_time!
            
            let str = NSAttributedString(htmlString: time1 + "<br>" + time2)
            cell.lblName.attributedText = str
            
            return cell
            
        case 12:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! DoctorProfileCell
            
            let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
            let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
            let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
            let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
            
            lbl1.text = "ASSISTANT NAME"
            lbl2.text = doctorData?.assistant_name
            
            lbl3.text = "ASSISTANT CONTACT NUMBER"
            lbl4.text = doctorData?.alt_contact_no
            
            return cell
            
        case 13:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! DoctorProfileCell
            
            cell.lblHead.text = "ABOUT CLINIC"
            cell.lblName.text = doctorData?.about_clinic
            
            return cell
            
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            
            return 140
        }
        
        return UITableView.automaticDimension
    }
}

class DoctorProfileCell: BaseCell {
    
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
}
