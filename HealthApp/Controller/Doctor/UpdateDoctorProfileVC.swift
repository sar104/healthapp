//
//  UpdateDoctorProfileVC.swift
//  HealthApp
//
//  Created by Apple on 07/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD
import RadioGroup

class UpdateDoctorProfileVC: BaseDoctorVC, UpdateDoctorProfileCellDelegate {

    @IBOutlet weak var tblData: UITableView!
    
    var doctorData: DetailData?
    var parentData: DetailData?
    var step: Int = 0
    var sectionRows = [9,9,9]
    var splArr: [SpecializationData] = []
    var statesArr:[State] = []
    var cityArr:[City] = []
    var assistantFlag: Bool = true
    var profileImgData: Data?
    let picker = UIImagePickerController()
    var addBranch: Bool = false
    var editBranch: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if !addBranch && !editBranch {
            if step == 0 {
                getProfile()
            }
            getSpecialization()
        } else {
            doctorData = DetailData()
            getProfile()
        }
        
        fetchStates()
        // Do any additional setup after loading the view.
    }
    
    func getProfile() {
           
           let params = ["id": loggedUser!.id]
           
           IHProgressHUD.show()
           
           apiManager.getDoctorProfile(params: params) { (result) in
               
               IHProgressHUD.dismiss()
               
               switch result {
               case .success(let data):
                if !self.addBranch && !self.editBranch {
                    self.doctorData = data
                   }
                   self.parentData = data
                   self.assistantFlag = (self.doctorData?.delivery_status == "Yes" ? true : false)
                   DispatchQueue.main.async {
                       self.tblData.dataSource = self
                       self.tblData.delegate = self
                       self.tblData.reloadData()
                   }
                   
               case .failure(let err):
                   print("err: \(err)")
               }
           }
       }
    
    func getSpecialization() {
        
        //getSpecialization
        
        IHProgressHUD.show()
        
        apiManager.getSpecialization(params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
                print("splarr: \(data)")
                self.splArr = data
                
                DispatchQueue.main.async {
                    
                    self.tblData.dataSource = self
                    self.tblData.delegate = self
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func fetchStates(){
        
        IHProgressHUD.show()
        apiManager.getStates(params: [:]) { (result) in
         
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                
                do {
                    let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
                    let status = jsonObj["status"] as! Int
                    if status == 1 {
                        if let states = jsonObj["data"]{
                            let statesData = try JSONSerialization.data(withJSONObject: states, options: .prettyPrinted)
                            let json = try JSONDecoder().decode([State].self, from: statesData)
                            self.statesArr = json
                           DispatchQueue.main.async {
                               
                               self.tblData.dataSource = self
                               self.tblData.delegate = self
                               self.tblData.reloadData()
                           }
                        }
                    } else {
                        
                    }
                } catch let err {
                    print("err: \(err)")
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
            
        }
    }
    func fetchCities(){
        
        IHProgressHUD.show()
        let params = ["state": doctorData?.state]
        apiManager.getCities(params: params) { (result) in
         
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                
                do {
                    let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
                    let status = jsonObj["status"] as! Int
                    if status == 1 {
                        if let states = jsonObj["data"]{
                            let statesData = try JSONSerialization.data(withJSONObject: states, options: .prettyPrinted)
                            let json = try JSONDecoder().decode([City].self, from: statesData)
                            self.cityArr = json
                            print("cities: \(json)")
                            DispatchQueue.main.async {
                                self.tblData.reloadData()
                            }
                        }
                    } else {
                        
                    }
                } catch let err {
                    print("err: \(err)")
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
            
        }
    }
    
    func fetchDetailsByPincode(pincode: String){
        
        IHProgressHUD.show()
        let params = ["pincode": pincode]
        apiManager.getDetailsByPincode(params: params) { (result) in
        
           IHProgressHUD.dismiss()
           switch result {
            
           case .success(let data):
            
            do {
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
                let status = jsonObj["status"] as! Int
                if status == 1 {
                    if let states = jsonObj["data"]{
                        let statesData = try JSONSerialization.data(withJSONObject: states, options: .prettyPrinted)
                        let json = try JSONDecoder().decode([PincodeData].self, from: statesData)
                        if json.count > 0 {
                        if let state = json[0].state {
                            self.doctorData?.state = state
                            self.loggedUser?.state = state
                        }
                        if let city = json[0].sub_district {
                            self.doctorData?.city = city
                            self.loggedUser?.city = city
                        }
                        }
                        print("cities: \(json)")
                        DispatchQueue.main.async {
                            self.tblData.reloadData()
                        }
                    }
                } else {
                    
                }
            } catch let err {
                print("err: \(err)")
            }
           case .failure(let err):
           print("err: \(err)")
            
            }
        }
    }
    
    func uploadImage(){
        
        IHProgressHUD.show()
        
        apiManager.uploadImg(image: profileImgData!, name: "image", api: ProfileImage) { (result) in
         
            IHProgressHUD.dismiss()
            switch result {
                case .success(let data):
                    let imgstr = String(data: data, encoding: .utf8)
                print(imgstr)
                    
                    if((imgstr?.contains("jpeg"))!){
                        self.doctorData?.shop_image = imgstr
                        self.updateProfile()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
        
    }
    
    func updateProfile() {
        
        IHProgressHUD.show()
        
        var params: [String:Any] = ["id": doctorData!.id!]
        /*= ["id": doctorData!.id!,
                      "shop_image": doctorData!.shop_image ?? "",
                      "name": doctorData!.name!,
                      "clinic_lab_hospital_name": doctorData!.clinic_lab_hospital_name!,
                      "mobile_no": doctorData!.mobile_no!,
        "email": doctorData!.email!,
        "registration_no": doctorData!.registration_no!,
        "gst_no": doctorData!.gst_no!,
        "state": doctorData!.state ?? "",
        "city": doctorData!.city ?? "",
        "address_type": doctorData!.address_type ?? "",
        "address": doctorData!.address!,
        "pincode": doctorData!.pincode!,
        "off_day": doctorData!.off_day!,
        "shop_opening_time": doctorData!.shop_opening_time!,
        "shop_closing_time": doctorData!.shop_closing_time!,
        "afternoon_opening_time": doctorData!.afternoon_opening_time!,
        "afternoon_closing_time": doctorData!.afternoon_closing_time!,
        "latitude": doctorData!.latitude ?? "",
        "longitude": doctorData!.longitude ?? "",
        "qualification": doctorData!.qualification!,
        "specialization": doctorData!.specialization!,
        "delivery_status": doctorData!.delivery_status ?? "",
        "assistant_name":  (assistantFlag) ? doctorData!.assistant_name ?? "" : "-",
        "alt_contact_no":  (assistantFlag) ? doctorData!.alt_contact_no ?? "": "-",
        "about_clinic": doctorData!.about_clinic ?? "",
        "clinic_lab_hospital_phone_no": "-"]*/
        
        params["shop_image"] = doctorData!.shop_image ?? ""
        params["name"] = doctorData!.name!
        params["clinic_lab_hospital_name"] = doctorData!.clinic_lab_hospital_name!
        params["mobile_no"] = doctorData!.mobile_no!
        params["email"] = doctorData!.email!
        params["registration_no"] = doctorData!.registration_no!
        params["gst_no"] = doctorData!.gst_no!
        params["state"] = doctorData!.state ?? ""
        params["city"] = doctorData!.city ?? ""
        params["address_type"] = doctorData!.address_type ?? ""
        params["address"] = doctorData!.address!
        params["pincode"] = doctorData!.pincode!
        params["off_day"] = doctorData!.off_day!
        params["shop_opening_time"] = doctorData!.shop_opening_time!
        params["shop_closing_time"] = doctorData!.shop_closing_time!
        params["afternoon_opening_time"] = doctorData!.afternoon_opening_time!
        params["afternoon_closing_time"] = doctorData!.afternoon_closing_time!
        params["latitude"] = doctorData!.latitude ?? ""
        params["longitude"] = doctorData!.longitude ?? ""
        params["qualification"] = doctorData!.qualification!
        params["specialization"] = doctorData!.specialization!
        params["delivery_status"] = doctorData!.delivery_status ?? ""
        params["assistant_name"] = (assistantFlag) ? doctorData!.assistant_name ?? "" : "-"
        params["alt_contact_no"] = (assistantFlag) ? doctorData!.alt_contact_no ?? "": "-"
        params["about_clinic"] = doctorData!.about_clinic ?? ""
        params["clinic_lab_hospital_phone_no"] = "-"
        
        apiManager.updateDoctorProfile(params: params) { (result) in
        
            IHProgressHUD.dismiss()
            switch result {
                
            case .success(let data):
                DispatchQueue.main.async() {
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                }
                
            case .failure(let err):
                print("err: \(err)")
                DispatchQueue.main.async() {
                    self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
        }
    }
    
    func addBranchAPI() {
        
        IHProgressHUD.show()
        
        var params: [String:Any] = ["parent_id": loggedUser!.id]
        
        params["shop_image"] = doctorData!.shop_image ?? "null"
        params["name"] = doctorData!.name!
        params["clinic_lab_hospital_name"] = parentData!.clinic_lab_hospital_name!
        params["mobile_no"] = doctorData!.mobile_no!
        params["email"] = parentData!.email!
        params["registration_no"] = parentData!.registration_no!
        params["gst_no"] = parentData!.gst_no!
        params["state"] = doctorData!.state ?? "null"
        params["city"] = doctorData!.city ?? "null"
        params["address_type"] = doctorData!.address_type ?? "null"
        params["address"] = doctorData!.address!
        params["pincode"] = doctorData!.pincode!
        params["off_day"] = doctorData!.off_day!
        params["shop_opening_time"] = doctorData!.shop_opening_time!
        params["shop_closing_time"] = doctorData!.shop_closing_time!
        params["afternoon_opening_time"] = doctorData!.afternoon_opening_time!
        params["afternoon_closing_time"] = doctorData!.afternoon_closing_time!
        params["latitude"] = doctorData!.latitude ?? "null"
        params["longitude"] = doctorData!.longitude ?? "null"
        params["qualification"] = parentData!.qualification!
        params["specialization"] = parentData!.specialization!
        params["delivery_status"] = doctorData!.delivery_status ?? "No"
        params["assistant_name"] = (assistantFlag) ? doctorData!.assistant_name ?? "" : "-"
        params["alt_contact_no"] = (assistantFlag) ? doctorData!.alt_contact_no ?? "": "-"
        params["about_clinic"] = doctorData!.about_clinic ?? "null"
        params["clinic_lab_hospital_phone_no"] = "-"
        
        apiManager.addBranch(params: params) { (result) in
        
            IHProgressHUD.dismiss()
            switch result {
                
            case .success(let data):
                DispatchQueue.main.async() {
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                }
                
            case .failure(let err):
                print("err: \(err)")
                DispatchQueue.main.async() {
                    self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
        }
    }
    
    func validate() -> Bool {
        
        if self.doctorData?.name == nil || self.doctorData!.name!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter name")
            return false
        }
        if self.doctorData?.clinic_lab_hospital_name == nil || self.doctorData!.clinic_lab_hospital_name!.isEmpty {
            if !addBranch && !editBranch {
                self.alert(strTitle: strErr, strMsg: "Please enter clinic/hospital name")
                return false
            }
        }
        if addBranch && (self.doctorData?.mobile_no == nil) {
            self.alert(strTitle: strErr, strMsg: "Please enter branch/clinic mobile number")
            return false
        }
        
        if self.doctorData?.qualification == nil || self.doctorData!.qualification!.isEmpty {
            if !addBranch && !editBranch {
                self.alert(strTitle: strErr, strMsg: "Please enter qualification")
                return false
            }
        }
        if self.doctorData?.specialization == nil || self.doctorData!.specialization!.isEmpty {
            if !addBranch && !editBranch {
                self.alert(strTitle: strErr, strMsg: "Please select specialization")
                return false
            }
        }
        if self.doctorData?.email == nil || self.doctorData!.email!.isEmpty {
            if !addBranch && !editBranch {
                self.alert(strTitle: strErr, strMsg: "Please enter email")
                return false
            }
        }
        if self.doctorData?.registration_no == nil || self.doctorData!.registration_no!.isEmpty {
            if !addBranch && !editBranch {
                self.alert(strTitle: strErr, strMsg: "Please enter registration_no")
                return false
            }
        }
        if self.doctorData?.gst_no == nil || self.doctorData!.gst_no!.isEmpty {
            if !addBranch && !editBranch {
                self.alert(strTitle: strErr, strMsg: "Please enter GST number")
                return false
            }
        }
        if self.doctorData?.address == nil || self.doctorData!.address!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter address")
            return false
        }
        if self.doctorData?.pincode == nil || self.doctorData!.pincode!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter pincode")
            return false
        }
        if self.doctorData?.off_day == nil || self.doctorData!.off_day!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select off day")
            return false
        }
        if self.doctorData?.shop_opening_time == nil || self.doctorData!.shop_opening_time!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select morning opening time")
            return false
        }
        if self.doctorData?.shop_closing_time == nil || self.doctorData!.shop_closing_time!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select morning closing time")
            return false
        }
        if self.doctorData?.afternoon_opening_time == nil || self.doctorData!.afternoon_opening_time!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select afternoon opening time")
            return false
        }
        if self.doctorData?.afternoon_closing_time == nil || self.doctorData!.afternoon_closing_time!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select afternoon closing time")
            return false
        }
        if assistantFlag {
            if self.doctorData?.assistant_name == nil || self.doctorData!.assistant_name!.isEmpty {
                
                self.alert(strTitle: strErr, strMsg: "Please enter assistant name")
                return false
            }
            if self.doctorData?.alt_contact_no == nil || self.doctorData!.alt_contact_no!.isEmpty {
                
                self.alert(strTitle: strErr, strMsg: "Please enter assistant contact number")
                return false
            }
        }
        
        return true
    }
    
    @IBAction func addBranchAction(_ sender: UIButton) {
        if validate() {
            if addBranch {
                addBranchAPI()
            }
            
            if editBranch {
                if profileImgData != nil {
                    uploadImage()
                } else {
                    updateProfile()
                }
            }
        }
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        
        if step < 2 {
            step = step + 1
            
            let vc = self.getVC(with: "UpdateDoctorProfileVC", sb: IDDrUpdateProfile) as! UpdateDoctorProfileVC
            vc.step = step
            vc.doctorData = doctorData
            
            self.navigationController?.pushViewController(vc, animated: false)
            
        } else {
            
            if validate() {
                if profileImgData != nil {
                    uploadImage()
                } else {
                    updateProfile()
                }
            }
        }
    }
    
    func onValueSelected(id: String, value: String) {
        
        switch id {
        case "name":
            doctorData?.name = value
            
        case "clinic":
            doctorData?.clinic_lab_hospital_name = value
            
        case "mobile":
            doctorData?.mobile_no = Int(value)
            
        case "qualification":
            doctorData?.qualification = value
            
        case "specialization":
            doctorData?.specialization = value
            
        //case "phone":
           // doctorData?. = value
            
        case "email":
            doctorData?.email = value
            
        case "registration":
            doctorData?.registration_no = value
            
        case "gst":
            doctorData?.gst_no = value
            
        case "address":
            doctorData?.address = value
            
        case "state":
            doctorData?.state = value
            
        case "city":
            doctorData?.city = value
            
        case "pincode":
            doctorData?.pincode = value
            
        case "offday":
            doctorData?.off_day = value
            
        case "morningtime_open":
            doctorData?.shop_opening_time = value
            
        case "morningtime_close":
            doctorData?.shop_closing_time = value
            
        case "eveningtime_open":
            doctorData?.afternoon_opening_time = value
            
        case "eveningtime_close":
            doctorData?.afternoon_closing_time = value
            
        case "radio":
            assistantFlag = (Int(value) == 0) ? false : true
            doctorData?.delivery_status = (Int(value) == 0) ? "No" : "Yes"
            
            let indexpath1 = IndexPath(row: 5, section: 2)
            let indexpath2 = IndexPath(row: 6, section: 2)
            let indexpath3 = IndexPath(row: 7, section: 2)
            
            tblData.reloadRows(at: [indexpath1,indexpath2,indexpath3], with: .none)
            
        case "about":
            doctorData?.about_clinic = value
            
        case "assistant":
            doctorData?.assistant_name = value
            
        case "assistant_contact":
            doctorData?.alt_contact_no = value
            
        default:
            break
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UpdateDoctorProfileVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        
        if textField.accessibilityIdentifier == "pincode" {
            
            if range.location > 4 && range.length != 1 {
                
                let pincode = textField.text! + string
                self.doctorData?.pincode = pincode
                
                self.fetchDetailsByPincode(pincode: pincode)

            }
        }
        return true
    }
}

extension UpdateDoctorProfileVC: UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == step {
            return sectionRows[section]
        }
        return (addBranch || editBranch) ? sectionRows[section] : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UpdateDoctorProfileCell()
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
                
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "topCell") as! UpdateDoctorProfileCell
                
                
                return cell
                
            case 1:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! UpdateDoctorProfileCell
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchHappen(_:)))
                tap.delegate = self
                
                cell.imgVw.addGestureRecognizer(tap)
                cell.imgVw.isUserInteractionEnabled = true
                
                if profileImgData != nil {
                    cell.imgVw.image = UIImage(data: profileImgData!)
                } else if let img = doctorData?.shop_image {
                    let url = doctorImgUrl + img
                    cell.imgVw.kf.setImage(with: URL(string: url))
                } else {
                    cell.imgVw.image = UIImage(named: "ic_doctor1")
                }
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "preTxtCell") as! UpdateDoctorProfileCell
                
                cell.delegate = self
                cell.setup(id: "name", placeholder: "Name")
                cell.txtFld.text = doctorData?.name
                
                return cell
                
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateDoctorProfileCell
                
                cell.delegate = self
                if !addBranch && !editBranch {
                    cell.setup(id: "clinic", placeholder: "Clinic/Hospital Name")
                    cell.txtFld.text = doctorData?.clinic_lab_hospital_name
                } else if addBranch {
                    cell.setup(id: "mobile", placeholder: "Branch Clinic Mobile Number")
                }
                
                
                return cell
            
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateDoctorProfileCell
                cell.delegate = self
                cell.setup(id: "qualification", placeholder: "Qualification")
                cell.txtFld.text = doctorData?.qualification
                
                return cell
            
            case 5:
                let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! UpdateDoctorProfileCell
                
                cell.delegate = self
                cell.pickerDataArr = self.splArr
                cell.setup(id: "specialization", placeholder: "Specialization")
                cell.ddTxtFld.text = doctorData?.specialization
                
                
                return cell
                
            case 6:
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateDoctorProfileCell
                
                cell.delegate = self
                cell.setup(id: "phone", placeholder: "Telephone/Landline")
                if let mob = doctorData?.mobile_no {
                    cell.txtFld.text = String(mob)
                }
                
                return cell
                
            case 7:
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateDoctorProfileCell
                
                cell.delegate = self
                cell.setup(id: "email", placeholder: "Email")
                cell.txtFld.text = doctorData?.email
                
                return cell
                
            case 8:
                let cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! UpdateDoctorProfileCell
                
                if !addBranch && !editBranch {
                    cell.btnEdit.addTarget(self, action: #selector(nextAction(_:)), for: .touchUpInside)
                }
                return cell
                
            default:
                break
            }
            
            case 1:
                switch indexPath.row {
                    
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "topCell") as! UpdateDoctorProfileCell
                    
                    
                    let btn1 = cell.contentView.viewWithTag(1) as! UIButton
                    btn1.setTitle("", for: .selected)
                    btn1.isSelected = true
                    btn1.backgroundColor = RGB_btnReOrder
                    
                    let btn2 = cell.contentView.viewWithTag(2) as! UIButton
                    
                    btn2.backgroundColor = RGB_btnReOrder
                    btn2.setTitleColor(.white, for: .normal)
                    
                    
                    return cell
                    
                case 1:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "headTxtCell") as! UpdateDoctorProfileCell
                    
                    cell.btnEdit.isHidden = true
                    cell.lblHead.text = "REGISTRATION"
                    cell.delegate = self
                    cell.setup(id: "registration", placeholder: "MCI Registration number")
                    cell.txtFld.text = doctorData?.registration_no
                    
                    return cell
                    
                case 2:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateDoctorProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "gst", placeholder: "GST (Optional)")
                    cell.txtFld.text = doctorData?.gst_no
                    
                    return cell
                    
                case 3:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "headTxtCell") as! UpdateDoctorProfileCell
                    
                    cell.btnEdit.isHidden = false
                    cell.lblHead.text = "ADDRESS"
                    cell.delegate = self
                    cell.setup(id: "address", placeholder: "Address")
                    cell.txtFld.text = doctorData?.address
                    
                    return cell
                    
                case 4:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateDoctorProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "home", placeholder: "Home/Address/PO Box")
                    cell.txtFld.text = doctorData?.address
                    
                    return cell
                    
                case 5:
                    if addBranch || editBranch {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateDoctorProfileCell
                        
                        cell.delegate = self
                        cell.txtFld.accessibilityIdentifier = "pincode"
                        cell.setup(id: "pincode", placeholder: "Pincode")
                        cell.txtFld.delegate = self
                        cell.txtFld.text = doctorData?.pincode
                        
                        return cell
                        
                    } else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! UpdateDoctorProfileCell
                        
                        cell.delegate = self
                        cell.setup(id: "state", placeholder: "State")
                        cell.ddTxtFld.text = doctorData?.state
                        cell.pickerDataArr = statesArr
                        
                        return cell
                    }
                    
                case 6:
                    if addBranch || editBranch {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! UpdateDoctorProfileCell
                        
                        cell.delegate = self
                        cell.setup(id: "state", placeholder: "State")
                        cell.ddTxtFld.text = doctorData?.state
                        cell.pickerDataArr = statesArr
                        
                        return cell
                        
                    } else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateDoctorProfileCell
                        
                        cell.delegate = self
                        cell.setup(id: "city", placeholder: "City")
                        cell.txtFld.text = doctorData?.city
                        //cell.pickerDataArr = cityArr
                        
                        return cell
                    }
                    
                case 7:
                    if addBranch || editBranch {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateDoctorProfileCell
                        
                        cell.delegate = self
                        cell.setup(id: "city", placeholder: "City")
                        cell.txtFld.text = doctorData?.city
                        //cell.pickerDataArr = cityArr
                        
                        return cell
                        
                    } else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateDoctorProfileCell
                        
                        cell.delegate = self
                        cell.txtFld.accessibilityIdentifier = "pincode"
                        cell.setup(id: "pincode", placeholder: "Pincode")
                        cell.txtFld.delegate = self
                        cell.txtFld.text = doctorData?.pincode
                        
                        return cell
                    }
                    
                case 8:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! UpdateDoctorProfileCell
                    if !addBranch && !editBranch {
                        cell.btnEdit.addTarget(self, action: #selector(nextAction(_:)), for: .touchUpInside)
                    }
                    return cell
                    
                default:
                    break
            }
            case 2:
                switch indexPath.row {
                    
                case 0:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "topCell") as! UpdateDoctorProfileCell
                    
                    
                    let btn1 = cell.contentView.viewWithTag(1) as! UIButton
                    btn1.setTitle("", for: .selected)
                    btn1.isSelected = true
                    btn1.backgroundColor = RGB_btnReOrder
                     
                    let btn2 = cell.contentView.viewWithTag(2) as! UIButton
                    
                    btn2.setTitle("", for: .selected)
                    btn2.isSelected = true
                    btn2.backgroundColor = RGB_btnReOrder
                    
                    return cell
                    
                case 1:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "offDayCell") as! UpdateDoctorProfileCell
                    cell.delegate = self
                    cell.setup(id: "offday", placeholder: "")
                    
                    return cell
                    
                case 2:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "timeCell") as! UpdateDoctorProfileCell
                    
                    cell.delegate = self
                    cell.lblHead.text = "MORNING TIME"
                    cell.setup(id: "morningtime", placeholder: "")
                    
                    return cell
                    
                case 3:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "timeCell") as! UpdateDoctorProfileCell
                    
                    cell.delegate = self
                    cell.lblHead.text = "EVENING TIME"
                    cell.setup(id: "eveningtime", placeholder: "")
                    
                    return cell
                    
                case 4:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateDoctorProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "about", placeholder: "About Clinic")
                    cell.txtFld.text = doctorData?.about_clinic
                    
                    return cell
                    
                case 5:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "radioCell") as! UpdateDoctorProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "radio", placeholder: "")
                    cell.radioGroup.selectedIndex = assistantFlag ? 1 : 0
                    return cell
                    
                case 6:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "drCell") as! UpdateDoctorProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "assistant", placeholder: "Assistant Name")
                    cell.txtFld.text = doctorData?.assistant_name
                    //cell.pickerDataArr = cityArr
                    
                    return cell
                    
                case 7:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! UpdateDoctorProfileCell
                    
                    cell.delegate = self
                    cell.setup(id: "assistant_contact", placeholder: "Assistant Contact")
                    cell.txtFld.text = doctorData?.alt_contact_no
                    
                    return cell
                    
                case 8:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! UpdateDoctorProfileCell
                    
                    cell.delegate = self
                    if addBranch {
                        cell.btnEdit.setTitle("ADD BRANCH", for: .normal)
                        cell.btnEdit.addTarget(self, action: #selector(addBranchAction(_:)), for: .touchUpInside)
                        
                    } else if editBranch {
                        cell.btnEdit.setTitle("UPDATE BRANCH", for: .normal)
                        cell.btnEdit.addTarget(self, action: #selector(addBranchAction(_:)), for: .touchUpInside)
                        
                    } else {
                        cell.btnEdit.setTitle("UPDATE", for: .normal)
                        cell.btnEdit.addTarget(self, action: #selector(nextAction(_:)), for: .touchUpInside)
                    }
                    
                    return cell
                    
                default:
                    break
            }
        default:
            break
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            
            return (addBranch || editBranch) ? 0 : 140
        }
        if indexPath.section == 0 && indexPath.row == 1 {
            
            return 180
        }
        if indexPath.section == 0 {
            if indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 7 || indexPath.row == 8 {
                return (addBranch || editBranch) ? 0 : 80
            }
        }
        if indexPath.section == 1 {
            if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 8 {
                return (addBranch || editBranch) ? 0 : 80
            }
        }
        if indexPath.section == 2 {
            if indexPath.row == 2 || indexPath.row == 3 {
                return 100
            }
            if indexPath.row == 4 {
                return (addBranch || editBranch) ? 0 : 80
            }
            if indexPath.row == 6 || indexPath.row == 7 {
                return !assistantFlag ? 0 : 80
            }
        }
        
        return 80//UITableView.automaticDimension
    }
}
extension UpdateDoctorProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func touchHappen(_ sender: UITapGestureRecognizer) {
            print("Tap On Image")
            picker.delegate = self
        let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {
                action in
                
                self.picker.allowsEditing = true
                self.picker.sourceType = .camera
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                self.present(self.picker, animated: true, completion: nil)
                
            }))
            alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: {
                action in
                
                self.picker.allowsEditing = true
                self.picker.sourceType = .photoLibrary
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                self.present(self.picker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            
            present(alert, animated: true, completion: nil)
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.editedImage] as? UIImage else {
            return
        }
            profileImgData = image.jpegData(compressionQuality: 0.5)
            self.tblData.reloadData()
            dismiss(animated: true, completion: nil)
        }

        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
}
protocol UpdateDoctorProfileCellDelegate {
    
    func onValueSelected(id: String, value: String)
}
class UpdateDoctorProfileCell: BaseCell {
    
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var txtFld: UITextField!
    @IBOutlet weak var ddTxtFld: RightViewTextField!
    @IBOutlet weak var txtTime1: UITextField!
    @IBOutlet weak var txtTime2: UITextField!
    @IBOutlet weak var radioGroup: RadioGroup!
    
    
    
    var selrow = 0
    var delegate: UpdateDoctorProfileCellDelegate?
    var seldate: String?
    
    lazy var timePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        //picker.minimumDate = Date()
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
    lazy var timePicker1: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        //picker.minimumDate = Date()
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
    func setup(id: String, placeholder: String) {
        
        if txtFld != nil {
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
        
        if ddTxtFld != nil {
            ddTxtFld.removeBottomBorder()
            ddTxtFld.addBottomBorder()
        }
        
        switch id {
        case "name":
            txtFld.placeholder = placeholder
            
        case "mobile":
            txtFld.placeholder = placeholder
            
        case "clinic":
            txtFld.placeholder = placeholder
            
        case "qualification":
            txtFld.placeholder = placeholder
            
        case "specialization":
            ddTxtFld.placeholder = placeholder
            self.cellPicker.dataSource = self
            self.cellPicker.delegate = self
            ddTxtFld.inputView = self.cellPicker
            ddTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            
        case "phone":
            txtFld.placeholder = placeholder
            
        case "email":
            txtFld.placeholder = placeholder
            
        case "registration":
            txtFld.placeholder = placeholder
            
        case "gst":
            txtFld.placeholder = placeholder
            
        case "address":
            txtFld.placeholder = placeholder
            
        case "home":
            txtFld.placeholder = placeholder
            
        case "pincode":
            txtFld.placeholder = placeholder
            
        case "state":
            self.selType = "state"
            cellPicker.dataSource = self
            cellPicker.delegate = self
            ddTxtFld.inputView = cellPicker
            ddTxtFld.placeholder = placeholder
            
        case "city":
            
            txtFld.placeholder = placeholder
            
        case "offday":
            
            for i in 1...7 {
                let btn = self.contentView.viewWithTag(i) as! UIButton
                let day = Days.all[i-1]
                btn.accessibilityIdentifier = day.rawValue
                btn.addTarget(self, action: #selector(offDayAction), for: .touchUpInside)
            }
            
        case "morningtime":
            
            timePicker.accessibilityIdentifier = "open"
            txtTime1.inputView = timePicker
            txtTime1.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id+"_open")
            txtTime1.placeholder = "Select Time"
            
            timePicker1.accessibilityIdentifier = "close"
            txtTime2.inputView = timePicker1
            txtTime2.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id+"_close")
            txtTime2.placeholder = "Select Time"
            
            let btn1 = self.contentView.viewWithTag(3) as! UIButton
            let btn2 = self.contentView.viewWithTag(4) as! UIButton
            
            btn1.accessibilityIdentifier = id
            btn2.accessibilityIdentifier = id
            
            btn1.addTarget(self, action: #selector(showTimePicker(_:)), for: .touchUpInside)
            btn2.addTarget(self, action: #selector(showTimePicker(_:)), for: .touchUpInside)
            
        case "eveningtime":
            
            timePicker.accessibilityIdentifier = "open"
            txtTime1.inputView = timePicker
            txtTime1.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id+"_open")
            txtTime1.placeholder = "Select Time"
            
            timePicker1.accessibilityIdentifier = "close"
            txtTime2.inputView = timePicker1
            txtTime2.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id+"_close")
            txtTime2.placeholder = "Select Time"
            
            let btn1 = self.contentView.viewWithTag(3) as! UIButton
            let btn2 = self.contentView.viewWithTag(4) as! UIButton
            
            btn1.accessibilityIdentifier = id
            btn2.accessibilityIdentifier = id
            
            btn1.addTarget(self, action: #selector(showTimePicker(_:)), for: .touchUpInside)
            btn2.addTarget(self, action: #selector(showTimePicker(_:)), for: .touchUpInside)
            
            
        case "radio":
            radioGroup.titles = [strKeepYourMobile,strAddAssistant]
            radioGroup.addTarget(self, action: #selector(optionSelected(radioGroup:)), for: .valueChanged)
            
        case "assistant_contact":
            txtFld.placeholder = placeholder
            
        case "assistant":
            txtFld.placeholder = placeholder
            
        default:
            break
        }
        
        self.layoutIfNeeded()
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selrow = row
    }
    
    override func doneCellPicker(sender: UIBarButtonItem) {
        
        let id = sender.accessibilityIdentifier
        
        if id == "specialization" {
            if let data = pickerDataArr[selrow] as? SpecializationData {
                
                ddTxtFld.text = data.specialization
                delegate?.onValueSelected(id: id!, value: data.specialization!)
            }
        }
        else if id == "morningtime_open" {
            delegate?.onValueSelected(id: id!, value: txtTime1.text ?? "")
        }
        else if id == "morningtime_close" {
            delegate?.onValueSelected(id: id!, value: txtTime2.text ?? "")
        }
        else if id == "eveningtime_open" {
            delegate?.onValueSelected(id: id!, value: txtTime1.text ?? "")
        }
        else if id == "eveningtime_close" {
            delegate?.onValueSelected(id: id!, value: txtTime2.text ?? "")
        } else {
            
            delegate?.onValueSelected(id: id!, value: txtFld.text ?? "")
        }
        self.endEditing(true)
    }
    
    @objc func dateSelected(_ picker: UIDatePicker){
        
        let id = picker.accessibilityIdentifier
        let dateStr = picker.date.toString(format: df_hh_mm_a)//df.string(from: picker.date)
        
        if id == "open" {
            txtTime1.text = dateStr
        }
        if id == "close" {
            txtTime2.text = dateStr
        }
        
        seldate = dateStr
    }
    
    @IBAction func offDayAction(_ sender: UIButton) {
        
        let tag = sender.tag
        
        for i in 1...7 {
            let btn = self.contentView.viewWithTag(i) as! UIButton
            btn.backgroundColor = .white
            btn.isSelected = false
        }
        
        let btn = self.contentView.viewWithTag(tag) as! UIButton
        btn.backgroundColor = .black
        btn.isSelected = true
        
        delegate?.onValueSelected(id: "offday", value: btn.accessibilityIdentifier!)
    }
    
    @IBAction func showTimePicker(_ sender: UIButton) {
        
        let tag = sender.tag
        //let id = sender.accessibilityIdentifier
        
        if tag == 3 {
            txtTime1.becomeFirstResponder()
        }
        if tag == 4 {
            txtTime2.becomeFirstResponder()
        }
    }
    
    @objc func optionSelected(radioGroup: RadioGroup){
        
        delegate?.onValueSelected(id: "radio", value: String(radioGroup.selectedIndex))
    }
}
