//
//  AttachPrescriptionVC.swift
//  HealthApp
//
//  Created by Apple on 10/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD
import SwiftyJSON
import MobileCoreServices

class AttachPrescriptionVC: BaseDoctorVC, SwiftAlertViewDelegate, AttachPrescriptionCellDelegate, UITextFieldDelegate {
    

    @IBOutlet weak var tblData: UITableView!
    
    var profileImgData: Data?
    var prescriptionImg:Data?
    let picker = UIImagePickerController()
    var arrMed: [DrugData] = []
    var purposeVisitArr: [IllnessTypeData] = []
    var addMedicineAlertView: SwiftAlertView!
    var medObj = MedicineData()
    var medicineArr: [MedicineData] = []
    var selectedMedArr: [MedicineData] = []
    var allMedicineArr: [MedicineData] = []
    var pdfFlag: Bool = false
    var pdfData: Data?
    var imageNameArr:[String] = []
    var doctorName: String = ""
    var selIllnessType: String?
    var dateOfReport: String = ""
    var notes: String = ""
    var userid: String?
    var aptData: AppointmentData?
    var prescData: PrescriptionData?
    var mobile: String?
    var editFlag: Bool = false
    var addFlag: Bool = false
    var prescId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let medArr = CoreDataManager.shared.fetchAllMedicines()
        
        for data in medArr {
            
            allMedicineArr.append(MedicineData(coreDataMed: data))
            
        }
        if let data = aptData {
            getUserByMobile(text: data.patient_mobile!)
        }
        if prescData == nil {
            prescData = PrescriptionData()
        } else {
            dateOfReport = prescData!.illness_date!
            doctorName = prescData!.doctor_name!
            selectedMedArr = prescData!.getDrugsArr()
            
            if let images = prescData?.images, images.count > 0 {
                self.imageNameArr.append(images[0].image!)
            }
        }
        getIllnessTypes()
    }
    
    @objc func getUserByMobile(text: String) {
        
        //if let mobile = text {
        
               
            IHProgressHUD.show()
            
            let params = ["mobile_no": text]  as [String : Any]
            print("params:\(params)")
            
            apiManager.getUserByMobile(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                case .success(let data):
                    print("data: \(data)")
                    
                    self.userid = String(data.id!)
                    self.prescData?.user_id = data.id!
                    self.prescData?.user_name = data.name!
                    //self.name = data.name
                    
                    DispatchQueue.main.async {
                        
                            self.tblData.reloadData()
                        
                    }
                case .failure(let err):
                    print("err: \(err)")
                    
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                }
            }
        //}
    }

    func getIllnessTypes() {
        
        IHProgressHUD.show()
        
        
        apiManager.getIllnessType(params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    
                    self.purposeVisitArr = memberData
                    /*
                    if self.editFlag {
                        let purposeArr = self.purposeVisitArr.filter{ $0.illness_type == self.prescriptionData?.illness_type }
                        //
                        if purposeArr.count > 0 {
                            self.selIllnessType = purposeArr[0]
                        }
                    }*/
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func initAlert() {
    
        addMedicineAlertView = SwiftAlertView(nibName: "AddMedicineAlertView", delegate: self, cancelButtonTitle: nil, otherButtonTitles: nil)
        addMedicineAlertView.dismissOnOtherButtonClicked = true
        addMedicineAlertView.dismissOnOutsideClicked = true
        addMedicineAlertView.tag = 1
        
        let txtMedName = addMedicineAlertView.viewWithTag(2) as! SearchTextField
        let txtType = addMedicineAlertView.viewWithTag(3) as! RightViewTextField
        let stepper = addMedicineAlertView.viewWithTag(4) as! UIStepperController
        stepper.delegate = self
        
        txtMedName.addBottomBorder()
        txtType.addBottomBorder()
        
        txtMedName.userStoppedTypingHandler = {
            if let criteria = txtMedName.text {
                if criteria.count > 2 {

                    // Show the loading indicator
                    txtMedName.showLoadingIndicator()
                    self.medicineArr = self.allMedicineArr.filter({ (data) -> Bool in
                        return data.medicine_name!.range(of: criteria) != nil
                    })
                    let medNames:[String] = self.medicineArr.map( {($0.medicine_name ?? "")} )
                    if medNames.count > 0 {
                        txtMedName.filterStrings(medNames)
                    } else {
                        self.medObj.medicine_name = criteria
                        self.medObj.type_of_medicine = "other"
                        txtType.text = "other"
                    }
                    txtMedName.stopLoadingIndicator()
                    
                    
                }
            }
        }
        //let medNames:[String] = self.medicineArr.map( {($0.medicine_name ?? "")} )
        //txtMedName.filterStrings(medNames)//(["asdds","dsssfd","weew","bvmbvb"])
        txtMedName.itemSelectionHandler = {filteredResults, itemPosition in
            
            let item = filteredResults[itemPosition]
            txtMedName.text = item.title
            let meddata = self.medicineArr.filter{ $0.medicine_name == item.title}.first
            if meddata != nil {
                let type = meddata?.type_of_medicine
                txtType.text = type
                
                self.medObj = meddata!
                
            } else {
                self.medObj.medicine_name = item.title
                self.medObj.type_of_medicine = "other"
            }
        }
        
        //txtMedName.delegate = self
        //txtType.delegate = self
        let qtyBtn = addMedicineAlertView.viewWithTag(4) as! UIStepperController
        let addBtn = addMedicineAlertView.viewWithTag(5) as! UIButton
        let closeBtn = addMedicineAlertView.viewWithTag(6) as! UIButton
        closeBtn.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        addBtn.addTarget(self, action: #selector(addMedicineAction), for: .touchUpInside)
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
     
        addMedicineAlertView.dismiss()
    }
    
    @objc func addMedicineAction(){
        
        if medObj.medicine_name == nil {
            
            self.alert(strTitle: strErr, strMsg: "Please select/enter medicine name ")
            
        } else {
            self.selectedMedArr.append(medObj)
            print("qty: \(medObj.quantity)")
            
            addMedicineAlertView.dismiss()
            self.tblData.reloadData()
        }
        
    }
    
    @IBAction func onAddMedClicked(_ sender: UIButton){
        
        initAlert()
        addMedicineAlertView.show()
    }
    
    func onValueSelected(id: String, value: String) {
        
        switch id {
            
        case "date":
            self.dateOfReport = value
            
        case "purpose":
            self.selIllnessType = value
            
        case "notes":
            self.notes = value
            
        default:
            break
        }
    }
    
    func uploadPrescription(){
        
        IHProgressHUD.show()
        apiManager.uploadImg(image: prescriptionImg!, name: "image", api: PrescriptionImgAPI) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let imgdata):
                    
                    let imgstr = String(data: imgdata, encoding: .utf8)
                    print(imgstr)
                        
                        if((imgstr?.contains("jpeg"))!){
                            
                            //self.addPrescription()
                            
                            if self.editFlag {
                                self.imageNameArr.removeAll()
                                self.imageNameArr.append(imgstr!)
                                self.updatePrescription()
                            } else {
                                self.imageNameArr.append(imgstr!)
                                self.addPrescription()
                            }
                        }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func uploadPDFPrescription(){
        
        IHProgressHUD.show()
        apiManager.uploadPDF(image: pdfData!, name: "image", api: PrescriptionImgAPI) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let imgdata):
                    
                    let imgstr = String(data: imgdata, encoding: .utf8)
                    print(imgstr)
                        
                        if((imgstr?.contains("pdf"))!){
                            
                            //self.addPrescription()
                            if self.editFlag {
                                self.imageNameArr.removeAll()
                                self.imageNameArr.append(imgstr!)
                               self.updatePrescription()
                           } else {
                                self.imageNameArr.append(imgstr!)
                               self.addPrescription()
                           }
                        }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func addPrescription(){
       
        /*
         doctor_id - current login id
         user_id - got by get_user_by_mobile api
         child_id - same value as user_id
         sharing_type - 0
         doctor_name - current login doctor name
         illness_type -
         description - null
         notes - optional
         illness_date
         images - single image
         **/
        
        let arrImgs = NSMutableArray.init()
        arrImgs.addObjects(from: imageNameArr)
        
        let arrDrugs = NSMutableArray.init()
       
        if selectedMedArr.count > 0 {
            
            for data in selectedMedArr {
                
                let meddata = ["medicine_name": data.medicine_name!,"quantity" :data.quantity,"unit" :data.type_of_medicine!] as [String : Any]
                arrDrugs.add(meddata)
            }
            print("drugs: \(arrDrugs)")
        }
        
        let jsonDrugs = JSON(arrDrugs)
        let strDrugs = jsonDrugs.rawString()!
        print("strDrugs: \(strDrugs)")
        
        
        //let userid = userid
        let childId = userid
    
        
        do {
            
            let params: [String:Any] = ["doctor_id": loggedUser!.id,
            PARAM_USERID: userid!,
                                       PARAM_CHILDID:  childId!,
             PARAM_SHARINGTYPE: 0,
             PARAM_DOCTORNAME: loggedUser!.name!,
         PARAM_ILLNESSTYPE: selIllnessType!,
         PARAM_DESCRIPTION: strDrugs,
         PARAM_ILLNESSDATE: dateOfReport,
         PARAM_IMAGES: arrImgs]
         
         
         print("params: \(params)")
            apiManager.addPrecriptionAPI(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        print("add prescr result: \(data)")
                        if let id = data.data {
                            self.prescId = String(id)
                            if self.aptData != nil {
                                self.updateAppointment()
                            }
                        }
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            //self.navPrescList()
                        }
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
            
        } catch let err {
            
        }
    }
    
    func updatePrescription(){
       
        IHProgressHUD.show()
        
        let arrImgs = NSMutableArray.init()
        arrImgs.addObjects(from: imageNameArr)
        
        let arrDrugs = NSMutableArray.init()
       
        if selectedMedArr.count > 0 {
            
            for data in selectedMedArr {
                
                let meddata = ["medicine_name": data.medicine_name!,"quantity" :data.quantity,"unit" :data.type_of_medicine!] as [String : Any]
                arrDrugs.add(meddata)
            }
            print("drugs: \(arrDrugs)")
        }
        
        let jsonDrugs = JSON(arrDrugs)
        let strDrugs = jsonDrugs.rawString()!
        print("strDrugs: \(strDrugs)")
            
        let childId = userid!
        
        
        do {
            let id: Int = prescData!.id!
            let params: [String:Any] = [PARAM_ID: id,
            PARAM_USERID: userid!,
            PARAM_CHILDID:  childId,
            PARAM_SHARINGTYPE: 0,
            PARAM_DOCTORNAME: loggedUser!.name!,
            PARAM_ILLNESSTYPE: selIllnessType,
            PARAM_DESCRIPTION: strDrugs,
            PARAM_ILLNESSDATE: dateOfReport,
            PARAM_IMAGES: arrImgs]
         
         
         print("params: \(params)")
            apiManager.updatePrecription(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            
                        }
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
            
        } catch let err {
            
        }
    }
    
    func updateAppointment() {
        
        IHProgressHUD.show()
        
        do {
        var params = ["doctor_id": loggedUser!.id]  as [String : Any]
        
        let apt_for = ["appointment_for": self.selIllnessType ?? "", "prescription_id": prescId ?? "null"]
        
        let data = try JSONEncoder().encode(apt_for)
        let jsonStr = String(data: data, encoding: .utf8)
        print("jsonStr: \(jsonStr)")
            
            params["id"] = self.aptData!.id!
        params["user_id"] = self.userid
            params["patient_name"] = self.aptData?.patient_name ?? self.prescData?.user_name
        params["patient_mobile"] = self.aptData?.patient_mobile
            params["patient_age"] = self.aptData?.patient_age
        params["patient_location"] = self.notes
        params["appointment_for"] = jsonStr//["appointment_for": self.purpose ?? "", "prescription_id": "null"]//reason for appointment(its a json with appointement_for and prescription_id)
            params["appointment_date"] = self.aptData?.appointment_date
            params["appointment_time"] = self.aptData?.appointment_time
        
        print("params:\(params)")
        
        apiManager.updateAppointment(params: params) { (result) in
               
                   IHProgressHUD.dismiss()
                   switch result {
                       
                   case .success(let data):
                       DispatchQueue.main.async() {
                           self.alert(strTitle: strSuccess, strMsg: data.message!)
                       }
                       
                   case .failure(let err):
                       print("err: \(err)")
                       DispatchQueue.main.async() {
                           self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                       }
                   }
               }
        } catch let err {
            print(err)
        }
    }
    
    func validate() -> Bool {
    
        if self.selIllnessType == nil || self.selIllnessType!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select purpose of appointment")
            return false
        }
        if self.dateOfReport.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select appointment date")
            return false
        }
        if prescriptionImg == nil && pdfData == nil {
            
            self.alert(strTitle: strErr, strMsg: "Please uplaod prescription image")
            return false
        }
        return true
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        
        if validate() {
            if prescriptionImg != nil {
                uploadPrescription()
            } else if pdfData != nil {
                uploadPDFPrescription()
            } else {
                if editFlag {
                    self.updatePrescription()
                } else {
                    addPrescription()
                }
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        
        if textField.accessibilityIdentifier == "mobile" {
            print("string: \(string)")
       
             
            if range.location == 9 && range.length != 1 {
                let mob = textField.text! + string
                 self.mobile = mob
                self.getUserByMobile(text: mob)

            }
        }
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AttachPrescriptionVC: UIStepperControllerDelegate {
    
    func stepperDidAddValues(stepper: UIStepperController) {
    
         print("Stepper value did change (Add) : \(stepper.count)")
        self.medObj.quantity = Int(stepper.count)
    }

    func stepperDidSubtractValues(stepper: UIStepperController) {
    
         print("Stepper value did change (Subtract) \(stepper.count)")
        self.medObj.quantity = Int(stepper.count)
         
    }
}
extension AttachPrescriptionVC: UIDocumentPickerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        self.pdfFlag = true
        do {
            self.pdfData = try Data(contentsOf: myURL)
        } catch let err {
            print("pdf err: \(err.localizedDescription)")
        }
        print("import result : \(myURL)")
        self.tblData.reloadData()
    }


    public func documentMenu(_ documentMenu:UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }


    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}
extension AttachPrescriptionVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func touchHappen(_ sender: UITapGestureRecognizer) {
            print("Tap On Image")
            picker.delegate = self
        let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {
                action in
                
                self.picker.allowsEditing = true
                self.picker.sourceType = .camera
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                self.present(self.picker, animated: true, completion: nil)
                
            }))
            alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: {
                action in
                
                self.picker.allowsEditing = true
                self.picker.sourceType = .photoLibrary
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                self.present(self.picker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            
            present(alert, animated: true, completion: nil)
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.editedImage] as? UIImage else {
            return
        }
            prescriptionImg = image.jpegData(compressionQuality: 0.5)
            self.tblData.reloadData()
            dismiss(animated: true, completion: nil)
        }

        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
}
extension AttachPrescriptionVC: UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
    
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 4
        }
        if section == 1 {
            return selectedMedArr.count + 1
        }
        if section == 2 {
            return 3
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = AttachPrescriptionCell()
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
                
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "headTxtCell") as! AttachPrescriptionCell
                
                cell.txtFld.accessibilityIdentifier = "mobile"
                cell.txtFld.delegate = self
                cell.layoutIfNeeded()
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "headTxtCell") as! AttachPrescriptionCell
                
                if let name = aptData?.patient_name {
                    cell.txtFld.text = name
                } else if let name = prescData?.user_name {
                    cell.txtFld.text = name
                }
                cell.layoutIfNeeded()
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! AttachPrescriptionCell
                
                cell.delegate = self
                cell.setup(id: "purpose", placeholder: "Select purpose")
                cell.pickerDataArr = self.purposeVisitArr
                
                if let purpose = prescData?.illness_type {
                    cell.txtFld.text = purpose
                }
                
                cell.layoutIfNeeded()
                return cell
                
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "dateCell") as! AttachPrescriptionCell
                
                cell.delegate = self
                cell.setup(id: "date", placeholder: "Prescription Date")
                
                if let date = prescData?.illness_date {
                    cell.txtFld.text = date
                }
                cell.layoutIfNeeded()
                return cell
                
            default:
                break
            }
            
        case 1:
            
            if indexPath.row  == selectedMedArr.count {
                let cell = tableView.dequeueReusableCell(withIdentifier: "addMedCell") as! AttachPrescriptionCell
                
                cell.setup(id: "addMed", placeholder: "")
                cell.btnDel.addTarget(self, action: #selector(onAddMedClicked), for: .touchUpInside)
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "medListCell") as! AttachPrescriptionCell
                
                if selectedMedArr.count > 0 {
                    let medicine = selectedMedArr[indexPath.row]
                    cell.lblMedName.text = medicine.medicine_name
                    cell.stepper.tag = indexPath.row
                    cell.stepper.count = CGFloat(medicine.quantity)
                    cell.stepper.delegate = self
                }
                cell.delegate = self
                
                return cell
            }
        case 2:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "upCell") as! AttachPrescriptionCell
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchHappen(_:)))
                tap.delegate = self
                
                cell.imgVw.addGestureRecognizer(tap)
                cell.imgVw.isUserInteractionEnabled = true
                
                if prescriptionImg != nil {
                    cell.imgVw.image = UIImage(data: prescriptionImg!)
                } 
                cell.setup(id: "upload", placeholder: "")
                
                return cell
                
            case 1:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "txtCell") as! AttachPrescriptionCell
                
                cell.delegate = self
                cell.setup(id: "notes", placeholder: "Notes")
                
                //if let notes = prescData
                cell.layoutIfNeeded()
                return cell
                
            case 2:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! AttachPrescriptionCell
                
                cell.btnDel.addTarget(self, action: #selector(submitAction), for: .touchUpInside)
                
                return cell
            default:
                break
            }
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return editFlag ? 0 : 60
            }
            return indexPath.row == 1 ? 80 : 60
        }
        if indexPath.section == 2 {
            return indexPath.row == 0 ? 160 : 60
        }
        return 60
    }
}

protocol AttachPrescriptionCellDelegate {
    
    func onValueSelected(id: String, value: String)
}

class AttachPrescriptionCell: BaseCell {
    
    @IBOutlet weak var txtFld: UITextField!
    @IBOutlet weak var ddTxtFld: RightViewTextField!
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var qty: UISwitch!
    @IBOutlet weak var btnDel: UIButton!
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblMedName: UILabel!
    @IBOutlet weak var stepper: UIStepperController!
    var delegate: AttachPrescriptionCellDelegate?
    var selRow: Int = 0
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        //picker.minimumDate = Date()
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
    func setup(id: String, placeholder: String) {
        
        if txtFld != nil {
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            
            txtFld.placeholder = placeholder
        }
        if ddTxtFld != nil {
            ddTxtFld.removeBottomBorder()
            ddTxtFld.addBottomBorder()
            ddTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            
            ddTxtFld.placeholder = placeholder
        }
        
        if id == "purpose" {
            self.selType = id
            cellPicker.dataSource = self
            cellPicker.delegate = self
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            
        }
        
        if id == "date" {
            datePicker.accessibilityIdentifier = "date"
            txtFld.inputView = datePicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "date")
        }
        
        if id == "addMed" {
            
        }
        
        if id == "upload" {
            self.bgView.setDashBorder(borderColor: .gray)
        }
    }
    @objc func dateSelected(_ picker: UIDatePicker){
        
        let id = picker.accessibilityIdentifier
        //df.string(from: picker.date)
        
        if id == "date" {
            let dateStr = picker.date.toString(format: df_dd_MM_yyyy)
            txtFld.text = dateStr
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        print("selType:\(selType)")
        selRow = row
        
        switch selType {
            case "purpose":
                let type = self.pickerDataArr[selRow] as? IllnessTypeData
                txtFld.text = type?.illness_type
            
        default:
            break
        }
        
        //delegate?.onValueSelected(type: selType, value: txtFld.text ?? "")
    }
    
    override func doneCellPicker(sender: UIBarButtonItem) {
              
           let id = sender.accessibilityIdentifier
           
           if id == "date" {
               
               delegate?.onValueSelected(id: id!, value: txtFld.text!)
               
           } else {
               
               delegate?.onValueSelected(id: id!, value: txtFld.text!)
               
           }
           self.endEditing(true)
       }
}
