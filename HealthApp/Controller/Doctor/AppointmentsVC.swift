//
//  AppointmentsVC.swift
//  HealthApp
//
//  Created by Apple on 10/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD
import CalendarKit

class AppointmentsVC: BaseDoctorVC {
 
    var eventVC: EventVC?
    var diagnoFlag: Bool = false
    
    @IBOutlet weak var btnAdd: UIButton!
    
    /*
    lazy var btnAdd: UIButton = {
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "add"), for: .normal)
        btn1.addTarget(self, action: #selector(self.addAction(_:)), for: .touchUpInside)
        
        let widthConstraint = btn1.widthAnchor.constraint(equalToConstant: 40)
        let heightConstraint = btn1.heightAnchor.constraint(equalToConstant: 40)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        
        return btn1
    }()*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.eventVC = EventVC()
        self.eventVC?.diagnoFlag = diagnoFlag
        self.eventVC?.view.frame = CGRect(x: 0, y: 100, width: self.view.frame.width, height: self.view.frame.height-100)
        self.addChild(self.eventVC!)
        self.view.addSubview(self.eventVC!.view)
        
        self.view.bringSubviewToFront(btnAdd)
    }
    
    @IBAction func addAction(_ sender: UIButton) {
        
        if diagnoFlag {
            
            let vc = self.getVC(with: "AddDiagnoAppointmentVC", sb: IDDiagno) as! AddAppointmentVC
           
            self.navigationController?.pushViewController(vc, animated: false)
            
        } else {
            let vc = self.getVC(with: "AddAppointmentVC", sb: IDDrAppointment) as! AddAppointmentVC
            //AttachPrescriptionVC
            //let vc = self.getVC(with: "AttachPrescriptionVC", sb: IDDrAppointment) as! AttachPrescriptionVC
            
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
}

class EventVC: DayViewController {

    lazy var coreDataManager: CoreDataManager = {
        let manager = CoreDataManager.shared
        return manager
    }()
    let apiManager: WebServiceManager = WebServiceManager(service: WebService())
    var loggedUser: User?
    var apptArr: [AppointmentData] = []
    var diagnoFlag: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.isTranslucent = true
        /*
        self.view.addSubview(btnAdd)
        let trailCon = NSLayoutConstraint(item: btnAdd, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 20)
        
        let bottCon = NSLayoutConstraint(item: btnAdd, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 20)
        
        self.view.addConstraint(trailCon)
        self.view.addConstraint(bottCon)
        */
        
        let users = self.fetchUser()
        if users.count > 0 {
            loggedUser = users[0]
        }
        // Do any additional setup after loading the view.
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchAppointments()
    }
    
    func fetchUser() -> [User] {
        let users = self.coreDataManager.fetchLocalUsers()
        //self.coreDataService?.fetchLocalUsers() ?? []
        //print(user)
        return users
    }
    
    func getVC(with id: String, sb: String) -> UIViewController {
        
        let sb = UIStoryboard(name: sb, bundle: nil)
        
        let vc = sb.instantiateViewController(withIdentifier: id)
        return vc
    }
    
    func fetchAppointments() {
        
        IHProgressHUD.show()
        
        let params = ["doctor_id": loggedUser!.id, "month": Date().get(.month), "year": Date().get(.year)] as [String : Any]
        
        apiManager.getDoctorAppt(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
             
                self.apptArr = data
                DispatchQueue.main.async {
                    
                    self.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
        
        /*
         appointments: ["message": Record Found., "status": 1, "data": <__NSArrayM 0x6000017f3090>(
         {
             "appointment_date" = "0000-00-00";
             "appointment_for" = "{\"appointment_for\":\"Neck pain and leg pain\",\"prescription_id\":\"388\"}";
             "appointment_status" = "PAYMENT DONE";
             "appointment_time" = "05:00 PM";
             "doctor_id" = 219;
             id = 22;
             "patient_age" = "31.9";
             "patient_location" = "- 422010";
             "patient_mobile" = 9657618996;
             "patient_name" = "Ms. HOH Test Customer";
         },
         {
             "appointment_date" = "0000-00-00";
             "appointment_for" = "{\"appointment_for\":\"test appointment\",\"prescription_id\":\"389\"}";
             "appointment_status" = NEW;
             "appointment_time" = "05:00 PM";
             "doctor_id" = 219;
             id = 23;
             "patient_age" = "0.0";
             "patient_location" = "- 422010";
             "patient_mobile" = 7719957969;
             "patient_name" = "Mr. Pri";
         },
         {
             "appointment_date" = "0000-00-00";
             "appointment_for" = "{\"appointment_for\":\"bdjidd\",\"prescription_id\":\"\"}";
             "appointment_status" = NEW;
             "appointment_time" = "06:00 PM";
             "doctor_id" = 219;
             id = 36;
             "patient_age" = "31.9";
             "patient_location" = "vsjjsjs\n - 422010";
             "patient_mobile" = 9657618996;
             "patient_name" = "Ms. HOH Test Customer";
         },
         {
             "appointment_date" = "0000-00-00";
             "appointment_for" = "{\"appointment_for\":\"gduid\",\"prescription_id\":\"\"}";
             "appointment_status" = NEW;
             "appointment_time" = "06:05 PM";
             "doctor_id" = 219;
             id = 37;
             "patient_age" = "33.10";
             "patient_location" = vzjdj;
             "patient_mobile" = 8149581516;
             "patient_name" = "Mr. Sanket";
         },
         {
             "appointment_date" = "0000-00-00";
             "appointment_for" = "{\"appointment_for\":\"test vaccine\",\"prescription_id\":\"\"}";
             "appointment_status" = NEW;
             "appointment_time" = "03:05 PM";
             "doctor_id" = 219;
             id = 56;
             "patient_age" = "31.10";
             "patient_location" = test;
             "patient_mobile" = 9657618996;
             "patient_name" = "Ms. HOH Test Customer";
         },
         {
             "appointment_date" = "0000-00-00";
             "appointment_for" = "{\"appointment_for\":\"pregnancy test\",\"prescription_id\":\"601\"}";
             "appointment_status" = NEW;
             "appointment_time" = "09:14  PM";
             "doctor_id" = 219;
             id = 57;
             "patient_age" = "31.10";
             "patient_location" = "test notes";
             "patient_mobile" = 9657618996;
             "patient_name" = "Ms. HOH Test Customer";
         },
         {
             "appointment_date" = "0000-00-00";
             "appointment_for" = "{\"appointment_for\":\"gshisos\",\"prescription_id\":\"\"}";
             "appointment_status" = NEW;
             "appointment_time" = "05:05  PM";
             "doctor_id" = 219;
             id = 59;
             "patient_age" = "31.10";
             "patient_location" = bshs;
             "patient_mobile" = 9657618996;
             "patient_name" = "Ms. HOH Test Customer";
         },
         {
             "appointment_date" = "0000-00-00";
             "appointment_for" = "{\"appointment_for\":\"gsius\",\"prescription_id\":\"\"}";
             "appointment_status" = NEW;
             "appointment_time" = "05:12  PM";
             "doctor_id" = 219;
             id = 61;
             "patient_age" = "31.10";
             "patient_location" = gzjjs;
             "patient_mobile" = 9657618996;
             "patient_name" = "Ms. HOH Test Customer";
         },
         {
             "appointment_date" = "0000-00-00";
             "appointment_for" = "{\"appointment_for\":\"test\",\"prescription_id\":\"\"}";
             "appointment_status" = NEW;
             "appointment_time" = "04:00 PM";
             "doctor_id" = 219;
             id = 69;
             "patient_age" = 31;
             "patient_location" = "test notes";
             "patient_mobile" = 9657618996;
             "patient_name" = "Ms. HOH Test Customer";
         },
         {
             "appointment_date" = "0000-00-00";
             "appointment_for" = "{\"appointment_for\":\"bdjjd\",\"prescription_id\":\"\"}";
             "appointment_status" = NEW;
             "appointment_time" = "06:00 PM";
             "doctor_id" = 219;
             id = 70;
             "patient_age" = 31;
             "patient_location" = hsjjd;
             "patient_mobile" = 9657618996;
             "patient_name" = "Ms. HOH Test Customer";
         },
         {
             "appointment_date" = "0000-00-00";
             "appointment_for" = "{\"appointment_for\":\"bzjsj\",\"prescription_id\":\"\"}";
             "appointment_status" = NEW;
             "appointment_time" = "09:40 AM";
             "doctor_id" = 219;
             id = 71;
             "patient_age" = 31;
             "patient_location" = bdjjd;
             "patient_mobile" = 9657618996;
             "patient_name" = "Ms. HOH Test Customer";
         },
         {
             "appointment_date" = "0000-00-00";
             "appointment_for" = "{\"appointment_for\":\"here appointment for\",\"prescription_id\":\"\",\"doctor_id\":\"219\",\"patient_id\":\"405\",\"video_meeting\":\"NO\",\"payment_details\":\"\",\"appointment_status\":\"NEW\"}";
             "appointment_status" = "<null>";
             "appointment_time" = "03:00 PM";
             "doctor_id" = 219;
             id = 148;
             "patient_age" = 54;
             "patient_location" = "test notes";
             "patient_mobile" = 7719957969;
             "patient_name" = Pri;
         },
         {
             "appointment_date" = "0000-00-00";
             "appointment_for" = "{\"prescription_id\":\"null\",\"appointment_for\":\"Hair test\"}";
             "appointment_status" = NEW;
             "appointment_time" = "04:45 PM";
             "doctor_id" = 219;
             id = 155;
             "patient_age" = 28;
             "patient_location" = Test;
             "patient_mobile" = 9850059684;
             "patient_name" = "Miss Sarika";
         }
         )
         ]
         **/
    }

    // Return an array of EventDescriptors for particular date
    override func eventsForDate(_ date: Date) -> [EventDescriptor] {
      var models = apptArr// Get events (models) from the storage / API

      var events = [Event]()

      for model in models {
          // Create new EventView
          let event = Event()
          // Specify StartDate and EndDate
        
          event.startDate = model.getAptDate()
        event.endDate = model.getAptDate().dateByAdding(component: .hour, value: 1)
          // Add info: event title, subtitle, location to the array of Strings
          var info = [model.getReason(), ""]
        info.append(model.appointment_time ?? "-")
          // Set "text" value of event by formatting all the information needed for display
          event.text = info.reduce("", {$0 + $1 + "\n"})
        event.userInfo = String(model.id!)
          events.append(event)
      }
      return events
    }

    override func dayViewDidSelectEventView(_ eventView: EventView) {
      guard let descriptor = eventView.descriptor as? Event else {
        return
      }
      print("Event has been selected: \(descriptor) \(String(describing: descriptor.userInfo))")
        if let info = descriptor.userInfo as? String {
            let id = Int(info)
            
            let data = apptArr.first { (data) -> Bool in
                return data.id == id
            }
            
            self.navAttachPresc(data: data!)
        }
    }
    
    func navAttachPresc(data: AppointmentData) {
        
        if diagnoFlag {
            
            
            let vc = self.getVC(with: "DiagnoAppointmentDetailsVC", sb: IDDiagno) as! AppointmentDetailsVC
            vc.diagnoFlag = true
            vc.aptData = data
            self.navigationController?.pushViewController(vc, animated: false)
            
        } else {
            
            let vc = self.getVC(with: "AppointmentDetailsVC", sb: IDDrAppointment) as! AppointmentDetailsVC
            vc.aptData = data
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

