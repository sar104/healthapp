//
//  PatientHistoryVC.swift
//  HealthApp
//
//  Created by Apple on 17/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class PatientHistoryVC: BaseDoctorVC {

    var historyArr: [PrescriptionData] = []
    @IBOutlet weak var tblData: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getHistory()
    }
    
    @IBAction func addAction(_ sender: UIButton) {
       
        let vc = self.getVC(with: "AttachPrescriptionVC", sb: IDDrAppointment) as! AttachPrescriptionVC
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func getHistory() {
        
        IHProgressHUD.show()
        
        let params = ["doctor_id": loggedUser!.id] as [String : Any]
        
        apiManager.getPatientHistory(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
             
                /*

                 {
                     "child_id" = 401;
                     description = "[\n  {\n    \"quantity\" : 1,\n    \"medicine_name\" : \"Macravit M   \",\n    \"unit\" : \"Injection \\/ Vial\"\n  }\n]";
                     "doctor_id" = 219;
                     "doctor_name" = "Dr.  Hoh Test Doctor";
                     id = 785;
                     "illness_date" = "13-11-2020";
                     "illness_type" = "Hair/Skin";
                     images =     (
                                 {
                             id = 760;
                             image = "dKg9eL7NPu11ajPTAgxq.jpeg";
                         }
                     );
                     notes = "<null>";
                     "sharing_type" = 0;
                     "user_id" = 401;
                     "user_name" = "Miss Sarika";
                 }
                 **/
                self.historyArr = data
                DispatchQueue.main.async {
                    
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PatientHistoryVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "prescCell") as! AppointmentDetailsCell
        
        let data = historyArr[indexPath.row]
        cell.populatePrescData(data:data)
        
        cell.layoutIfNeeded()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = historyArr[indexPath.row]
        
        let vc = self.getVC(with: "PatientPrescDetailVC", sb: IDDrAppointment) as! PatientPrescDetailVC
        
        //vc.editFlag = true
        vc.prescData = data
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
}
