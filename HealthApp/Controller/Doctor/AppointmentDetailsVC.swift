//
//  AppointmentDetailsVC.swift
//  HealthApp
//
//  Created by Apple on 12/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class AppointmentDetailsVC: BaseDoctorVC {

    @IBOutlet weak var tblData: UITableView!
    
    var aptData: AppointmentData?
    var patientData: FamilyMemberData?
    var prescData: PrescriptionData?
    var diagnoFlag: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getDetails()
        getAttachedPre()
        // Do any additional setup after loading the view.
        if let mob = aptData?.patient_mobile {
            getUserByMobile(text: mob)
        }
    }
    
    @objc func getUserByMobile(text: String) {
        
        //if let mobile = text {
        
               
            IHProgressHUD.show()
            
            let params = ["mobile_no": text]  as [String : Any]
            print("params:\(params)")
            
            apiManager.getUserByMobile(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                case .success(let data):
                    print("data: \(data)")
                    
                    self.patientData = data
                    
                    DispatchQueue.main.async {
                        
                            self.tblData.reloadData()
                        
                    }
                case .failure(let err):
                    print("err: \(err)")
                    
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                }
            }
        //}
    }
    
    func getDetails() {
        
        let params = ["appointment_id": aptData!.id]
        
        IHProgressHUD.show()
        
        apiManager.getAppointmentDetails(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                print("data: \(data)")
                
                self.aptData = data
                
                DispatchQueue.main.async {
                    
                        self.tblData.reloadData()
                    
                }
            case .failure(let err):
                print("err: \(err)")
                
                DispatchQueue.main.async {
                    self.tblData.reloadData()
                }
            }
        }
    }
    
    func getAttachedPre() {
        
        //getAttachedPresc
        let params = ["id": aptData!.getPrescrID()]
        
        IHProgressHUD.show()
        
        apiManager.getAttachedPresc(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                print("presc data: \(data)")
                
                self.prescData = data
                
                DispatchQueue.main.async {
                    
                        self.tblData.reloadData()
                    
                }
            case .failure(let err):
                print("err: \(err)")
                
                DispatchQueue.main.async {
                    self.tblData.reloadData()
                }
            }
        }
    }

    @IBAction func attachAction(_ sender: UIButton) {
        
        let vc = self.getVC(with: "AttachPrescriptionVC", sb: IDDrAppointment) as! AttachPrescriptionVC
        
        vc.aptData = aptData
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func editAction(_ sender: UIButton) {
        
        if diagnoFlag {
            
            let vc = self.getVC(with: "AddDiagnoAppointmentVC", sb: IDDiagno) as! AddAppointmentVC
            vc.editFlag = true
            vc.aptData = aptData
            self.navigationController?.pushViewController(vc, animated: false)
            
        } else {
            let vc = self.getVC(with: "AddAppointmentVC", sb: IDDrAppointment) as! AddAppointmentVC
            vc.editFlag = true
            vc.aptData = aptData
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AppointmentDetailsVC: UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }
        if section == 1 {
            return 1
        }
        if section == 2 {
            return 4
        }
        if section == 3 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = AppointmentDetailsCell()
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "mobileCell") as! AppointmentDetailsCell
                
                cell.btnEdit.isHidden = false
                cell.btnEdit.addTarget(self, action: #selector(editAction(_:)), for: .touchUpInside)
                
                cell.lblHead.text = aptData?.patient_name
                cell.lblName.text = aptData?.patient_mobile
                
                cell.layoutIfNeeded()
                return cell
                
            default:
                break
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ageCell") as! AppointmentDetailsCell
            
            if let age = aptData?.patient_age {
                cell.lblHead.text = age
            }
            cell.lblName.text = patientData?.gender
            
            cell.layoutIfNeeded()
            return cell
            
        case 2:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AppointmentDetailsCell
                
                cell.lblHead.text = "APPOINTMENT FOR"
                cell.lblName.text = aptData?.getReason()
                
                cell.layoutIfNeeded()
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! AppointmentDetailsCell
            
                let lblHead1 = cell.contentView.viewWithTag(1) as! UILabel
                let lblName1 = cell.contentView.viewWithTag(2) as! UILabel
                let lblHead2 = cell.contentView.viewWithTag(3) as! UILabel
                let lblName2 = cell.contentView.viewWithTag(4) as! UILabel
                
                lblHead1.text = "APPOINTMENT DATE"
                lblHead2.text = "APPOINTMENT TIME"
                
                lblName1.text = aptData?.appointment_date
                lblName2.text = aptData?.appointment_time
                    
                cell.layoutIfNeeded()
                return cell
            
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AppointmentDetailsCell
            
                cell.lblHead.text = "NOTES"
                cell.lblName.text = aptData?.patient_location
                
                cell.layoutIfNeeded()
                return cell
                
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! AppointmentDetailsCell
                
                    cell.btnAdd.addTarget(self, action: #selector(attachAction), for: .touchUpInside)
                
                    cell.layoutIfNeeded()
                    return cell
                
            default:
                break
            }
           
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "prescCell") as! AppointmentDetailsCell
            
            if prescData != nil {
                cell.populatePrescData(data:prescData!)
            }
            cell.layoutIfNeeded()
            return cell
            
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 2 {
            if indexPath.row == 3 {
                return (!diagnoFlag && aptData!.getPrescrID().isNull()) ? 50 : 0
            }
        }
        if indexPath.section == 3 {
            return (prescData != nil) ? 100 : 0
        }
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 2 {
        
            return "APPOINTMENT DETAILS"
        }
        
        if section == 3 {
            return "ATTACHED PRESCRIPTION"
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 2 {
            return 45
        }
        if section == 3 {
            return (prescData != nil) ? 45 : 0
        }
        return 0
    }
}

class AppointmentDetailsCell: BaseCell {
        
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var imgPresc: UIImageView!
    
    func setup(id: String, placeholder: String) {
        
    }
    
    func populatePrescData(data: PrescriptionData) {
        
        let img = data.getImage()
        if !img.isEmpty {
            if img.contains("pdf") {
                
                imgPresc.image = UIImage(named: "pdf")
                
            } else {
                let imgURL = prescriptionImageURL + img
                imgPresc.kf.setImage(with: URL(string: imgURL))
            }
        }
        
        self.lblHead.text = data.illness_type
        self.lblName.text = data.doctor_name
        self.lblDesc.text = data.getDrugs()
        self.lblDate.text = data.illness_date
    }
}
