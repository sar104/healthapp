//
//  ChemistOrdersDetailsVC.swift
//  HealthApp
//
//  Created by Apple on 19/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class ChemistOrdersDetailsVC: BaseChemistVC, SwiftAlertViewDelegate {
    
    @IBOutlet weak var tblData: UITableView!
    var orderDetail: ChemistOrderData?
    var user: User?
    var arrMed: [OrderDetails] = []
    var arrItem: [OrderDetails] = []
    var flagVerify: Bool = false
    var rejectAlertView: SwiftAlertView!
    let device = ""
    var alertWidth = 375.0
    let cellPicker = UIPickerView()
    var pickerDataArr = Array<Any>()
    var rejectReason = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let users = self.fetchUser()
        user = users[0]
        
        arrMed = orderDetail?.order_details.filter{ ($0.getMedicineData().medicine_type == "PM") } ?? []
        
        arrItem = orderDetail?.order_details.filter{ ($0.getMedicineData().medicine_type == "NPM") } ?? []
        
        tblData.estimatedRowHeight = 500
        
        switch UIDevice.modelName {
            
        case device + "iPhone 5", device + "iPhone 5c", device + "iPhone SE", device + "iPhone 5s":
            alertWidth = 320.0
            
        default:
            break
        }
    }
    
    func initAlert() {
        
        let nib = Bundle.main.loadNibNamed("RejectReason", owner: self, options: nil)
        let contentView = nib?[0] as! UIView
        if UIDevice.modelName == device + "iPhone 5" ||  UIDevice.modelName ==  device + "iPhone 5c" || UIDevice.modelName ==  device + "iPhone SE" || UIDevice.modelName ==   device + "iPhone 5s" {
        contentView.frame = CGRect(x: 0, y: 0, width: 320, height: contentView.frame.size.height)
        }
        //addMedAlertView = SwiftAlertView(nibName: "AddMedAlertView", delegate: self, cancelButtonTitle: nil, otherButtonTitles: nil)
        rejectAlertView = SwiftAlertView(contentView: contentView, delegate: self, cancelButtonTitle: nil)
        rejectAlertView.dismissOnOtherButtonClicked = true
        rejectAlertView.dismissOnOutsideClicked = true
        rejectAlertView.kDefaultHeight = 375
        if UIDevice.modelName == device + "iPhone 5" ||  UIDevice.modelName ==  device + "iPhone 5c" || UIDevice.modelName ==  device + "iPhone SE" || UIDevice.modelName ==   device + "iPhone 5s" {
            
            rejectAlertView.kDefaultWidth = alertWidth
        }
        
        let btnClose = rejectAlertView.viewWithTag(1) as! UIButton
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        
        let txtReason = rejectAlertView.viewWithTag(2) as! RightViewTextField
        txtReason.placeholder = "Select Reason"
        txtReason.inputView = self.cellPicker
        self.pickerDataArr = ["Medicine not available", "Improper information provided", "Incomplete address provided", "We have no delivery service in customer area"]
        self.cellPicker.dataSource = self
        self.cellPicker.delegate = self
        txtReason.inputAccessoryView = self.addDoneButton(dontTxt: "Done")
        
        let btnReject = rejectAlertView.viewWithTag(3) as! UIButton
        btnReject.addTarget(self, action: #selector(rejectAction), for: .touchUpInside)
    }

    @IBAction func closeAction(_ sender: UIButton) {
     
        rejectAlertView.dismiss()
    }
    
    @IBAction func rejectAction(_ sender: UIButton) {
     
        self.rejectOrder()
        rejectAlertView.dismiss()
    }
    
    func didDismissAlertView(alertView: SwiftAlertView) {
        
    }
    
    override func donePicker(sender: UIBarButtonItem) {
        
        let txtReason = rejectAlertView.viewWithTag(2) as! RightViewTextField
        txtReason.text = rejectReason
        self.rejectAlertView.endEditing(true)
    }
    
    func rejectOrder() {
        
        //updateRejectOrderForChemist
        let params = ["order_id": orderDetail!.id!,
                      "chemist_id": user!.id,
                      "reject_reason": rejectReason] as [String : Any]
        
        //IHProgressHUD.show()
        /*
        apiManager.updateRejectOrderForChemist(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        
                    }
                case .failure(let err):
                    print("err: \(err)")
                    self.alert(strTitle: strErr, strMsg: err.localizedDescription)
            }
        }*/
    }
    
    func updateOrder() {
        
        /*
         http://www.doctoronhire.com/health/api/update_order_for_chemist?order_id=558&drug_list=[{"medicine_name":"{\"medicine_name\":\"COMBIPLET\",\"suggested_name\":\"\",\"medicine_type\":\"NPM\"}","quantity":"3","unit":"Capsule","available_status":"Yes"},{"medicine_name":"{\"medicine_name\":\"K TRIP PLUS\",\"suggested_name\":\"NUMINIC\",\"medicine_type\":\"NPM\"}","quantity":"0","unit":"Tablet","available_status":"No"}]&estimated_cost=50
         **/
        
        var params: [String : Any] = [:]
        
        do {
            var drugArr: [CDrugData] = []
            
            if arrMed.count > 0 {
                
                for med in arrMed {
                   
                    
                    var medname = med.getMedicineData()
                    medname.suggested_name = med.suggested_name
                    
                    var  drugdata = CDrugData()
                    
                    drugdata.medicine_name = medname
                    drugdata.unit = med.unit
                    drugdata.quantity = med.quantity
                    drugdata.available_status = med.available_status
                    
                    drugArr.append(drugdata)
                }
                print("drugs: \(drugArr)")
            }
            if arrItem.count > 0 {
                
                for med in arrItem {
                   
                    
                    var medname = med.getMedicineData()
                    medname.suggested_name = med.suggested_name
                    
                    var  drugdata = CDrugData()
                    
                    drugdata.medicine_name = medname
                    drugdata.unit = med.unit
                    drugdata.quantity = med.quantity
                    drugdata.available_status = med.available_status
                    
                    drugArr.append(drugdata)
                }
                print("drugs: \(drugArr)")
            }
            
            let drugJSonData = try JSONEncoder().encode(drugArr)
            let jsonStrDrugs = String(data: drugJSonData, encoding: .utf8)
            
            params["drug_list"] = jsonStrDrugs
            params["order_id"] = orderDetail?.id
            params["estimated_cost"] = orderDetail?.estimated_cost
            
            print("params: \(params)")
            /*
            IHProgressHUD.show()
            
            apiManager.updateOrderForChemist(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            
                        }
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
                
            }
            */
        } catch let err {
            print(err)
        }
    }
    
    func validate() -> Bool {
        
        if !flagVerify {
            self.alert(strTitle: strErr, strMsg: "Please check & verify the precription")
            return false
        }
        
        if orderDetail?.estimated_cost == nil {
            self.alert(strTitle: strErr, strMsg: "Please enter the estimated cost")
            return false
        }
        
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ChemistOrdersDetailsVC: ChemistOrdersCellDelegate {
    
    
    func onEditMedicine(id: Int, value: String, type: String) {
        
        print("id: \(id), value: \(value)")
        if type == "cost" {
            orderDetail?.estimated_cost = Double(value)
        }
        if type == "PM" {
            var data = arrMed[id]
            data.suggested_name = value
            arrMed[id] = data
        }
        if type == "NPM" {
            var data = arrItem[id]
            data.suggested_name = value
            arrItem[id] = data
        }
        print("orderDetail: \(orderDetail)")
    }
    
    
    func onValueChanged(tag: Int, type: String, status: Bool) {
        
        if type == "PM" {
            var data = arrMed[tag]
            
            data.available_status = status ? "Yes" : "No"
            arrMed[tag] = data
        }
        
        if type == "NPM" {
            
            var data = arrItem[tag]
            data.available_status = status ? "Yes" : "No"
            arrItem[tag] = data
        }
        if type == "check" {
            
            flagVerify = status
        }
        
        tblData.reloadData()
    }
    
    func onSubmitClicked(tag: Int) {
        
        if tag == 0 {
            // reject
            initAlert()
            rejectAlertView.show()
        }
        if tag == 1 {
            // confirm
            if validate() {
                updateOrder()
            }
        }
    }
}
extension ChemistOrdersDetailsVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! ChemistOrdersCell
        
        let data = orderDetail
        cell.arrMed = arrMed//data!.order_details//data.order_details.filter{ ($0.getMedicineData().medicine_type == "PM") }
        cell.arrItem = arrItem//(data?.order_details.filter{ ($0.getMedicineData().medicine_type == "NPM") })!
        
        cell.setup()
        cell.delegate = self
        cell.populateData(data: data!)
        cell.tblMed.reloadData()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension//500
    }
}


extension ChemistOrdersDetailsVC: UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        
        return self.pickerDataArr.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if let strArr = self.pickerDataArr as? [String] {
            let title = strArr[row]
            return title
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.rejectReason = pickerDataArr[row] as! String
    }
}
