//
//  BaseChemistVC.swift
//  HealthApp
//
//  Created by Apple on 27/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class BaseChemistVC: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var menuBtn: UIBarButtonItem!
    
    var menuSlide : VKSideMenu =  VKSideMenu.init()
    var menuArray : [VKSideMenuItem] = []
    var menuSectionArr: [[VKSideMenuItem]] = []
    var sectionTitleArr: [String] = []
    lazy var coreDataManager: CoreDataManager = {
        let manager = CoreDataManager.shared
        return manager
    }()
    
    var loggedUser: User?
    let apiManager: WebServiceManager = WebServiceManager(service: WebService())
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let users = self.fetchUser()
        if users.count > 0 {
            loggedUser = users[0]
        }
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(setProfilePhoto), name: NSNotification.Name(rawValue: ProfileUpdateNotification), object: nil)
        
        if self.isKind(of: ChemistDashboard.self) && !self.isKind(of: BranchListVC.self)  {
            self.setProfilePhoto()
            self.initSideMenu()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.isKind(of: ChemistDashboard.self) && !self.isKind(of: BranchListVC.self)  {
            
            //self.navigationController?.setNavigationBarHidden(true, animated: false)
            showBackButton()
            
        } else {
            //self.navigationController?.setNavigationBarHidden(false, animated: false)
            
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }

    func fetchUser() -> [User] {
        let users = self.coreDataManager.fetchLocalUsers()
        //self.coreDataService?.fetchLocalUsers() ?? []
        //print(user)
        return users
    }
    
    @objc func setProfilePhoto() {
        
        let users = self.coreDataManager.fetchLocalUsers()
        loggedUser = users[0]
        
        
        let logoImg = UIImage(named:"Logo")
        let imgView = UIImageView(image: logoImg)
        imgView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        imgView.center = (self.navigationController?.navigationBar.center)!
        
        imgView.contentMode = .scaleAspectFit
        navigationItem.titleView = imgView
        
        
        let image = UIImage(named: "Menu")
        let renderImg = image?.withRenderingMode(.alwaysOriginal)
        menuBtn.image = renderImg
        
        
        if let imgurl = loggedUser?.profile_image {
            let url = imgBaseURL + custProfileImg + imgurl
            let imgview = UIImageView()
            imgview.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            imgview.layer.masksToBounds = true
            imgview.layer.cornerRadius = 20
            imgview.kf.setImage(with: URL(string: url))
            
            let widthConstraint = imgview.widthAnchor.constraint(equalToConstant: 40)
            let heightConstraint = imgview.heightAnchor.constraint(equalToConstant: 40)
            heightConstraint.isActive = true
            widthConstraint.isActive = true
            
            let barButton = UIBarButtonItem(customView: imgview)
            //assign button to navigationbar
            self.navigationItem.rightBarButtonItem = barButton
        }
    }
    
    func showBackButton() {
        
        let imgview = UIImage()
        
        self.navigationController?.navigationBar.backIndicatorImage = imgview
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgview
       
    }
    
    func addDoneButton(dontTxt: String) -> UIToolbar {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let lblDone = UIButton()
        lblDone.frame = CGRect(x: 0, y: 0, width: 80, height: 30)
        lblDone.setTitle(dontTxt, for: .normal)
        lblDone.setTitleColor(.blue, for: .normal)
        lblDone.addTarget(self, action: #selector(self.donePicker(sender:)), for: .touchUpInside)
        //lblDone.accessibilityIdentifier = id
        
        let doneButton = UIBarButtonItem(customView: lblDone)
        //UIBarButtonItem(title: dontTxt, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let lblCancel = UIButton()
        lblCancel.frame = CGRect(x: 0, y: 0, width: 80, height: 30)
        lblCancel.setTitle(strCancel, for: .normal)
        lblCancel.setTitleColor(.blue, for: .normal)
        lblCancel.addTarget(self, action: #selector(self.cancelPicker(sender:)), for: .touchUpInside)
        
        let cancelButton = UIBarButtonItem(customView: lblCancel)
        //let cancelButton = UIBarButtonItem(title: strCancel, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelPicker))
        
        
        
        let btndone = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: Selector(("donePicker")))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.backgroundColor = UIColor.blue
        

        return toolBar
    }
    
    @objc func donePicker (sender: UIBarButtonItem) {
        
    }
    
    @objc func cancelPicker (sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }
    
    func getVC(with id: String, sb: String) -> UIViewController {
        
        let sb = UIStoryboard(name: sb, bundle: nil)
        
        let vc = sb.instantiateViewController(withIdentifier: id)
        return vc
    }
    
    @IBAction func menuAction(_ sender: Any) {
        menuSlide.show()
    }
    
    func initSideMenu(){
        
        if menuSlide != nil
        {
            //self.menuSlide =  VKSideMenu.init(direction: VKSideMenu.VKSideMenuDirection.fromLeft)
            menuSlide = VKSideMenu.init(size: 300, andDirection: VKSideMenu.VKSideMenuDirection.fromLeft)
            menuSlide.dataSource = self
            menuSlide.delegate = self
            menuSlide.textColor = UIColor.white
            //menuSlide.backgroundColor = UIColor.red
            menuSlide.isEnableOverlay = false
            menuSlide.isHideOnSelection = true
            menuSlide.animationDuration = Int(0.8)
            menuSlide.selectionColor = UIColor(white: 0.0, alpha: 0.3)
            menuSlide.iconsColor = nil
            menuSlide.backgroundColor = UIColor.black
        }
        
        //if loggedUser?.role == 3 {
            
            var options = [AnyHashable]()
            options.append(MENU_HOME)
            options.append(MENU_PROFILE)
            options.append(MENU_BRANCHES)
            options.append(MENU_RATINGS)
            options.append(MENU_CONTACT)
            options.append(MENU_HELP)
            options.append(MENU_LOGOUT)
            
            var sectionArr: [[AnyHashable]] = []
            sectionArr.append(options)
            
            menuSectionArr.append([VKSideMenuItem()])
            for section in sectionArr {
                menuArray = []
                for option in section {
                    let item = VKSideMenuItem()
                    item.title = option as! String
                    if let icon = getMenuIcon(name: option as! String) {
                        item.icon = UIImage(named: icon)
                    }
                    menuArray.append(item)
                }
                menuSectionArr.append(menuArray)
            }
            sectionTitleArr.append("")
            sectionTitleArr.append("")
        //}
    }
    
    func getMenuIcon(name: String)-> String? {
        
        switch name {
            
        case MENU_HOME:
            return ICON_HOME
                
        case MENU_BRANCHES:
            return ICON_BRANCH
            
        case MENU_PROFILE:
            return ICON_PROFILE
            
        case MENU_RATINGS:
            return ICON_RATING
            
        case MENU_CONTACT:
            return ICON_CONTACT
            
        case MENU_HELP:
            return ICON_HELP
            
        case MENU_LOGOUT:
            return ICON_LOGOUT
            
        default:
            break
        }
        
        return ""
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BaseChemistVC: VKSideMenuDelegate, VKSideMenuDataSource {
    func viewOnHeaderSideMenu() {
        
    }
    
    
    //:MARKk -Side menu delegate
       
       func numberOfSections(in sideMenu: VKSideMenu) -> Int {
        return menuSectionArr.count
       }
       
       func sideMenu(_ sideMenu: VKSideMenu, numberOfRowsInSection section: Int) -> Int {
           let arr = menuSectionArr[section]
           return section == 0 ? 1 : arr.count
       }
       
        func sideMenu(_ sideMenu: VKSideMenu, titleForHeaderInSection section: Int) -> String {
        
            return sectionTitleArr[section]
        }
        
       func sideMenu(_ sideMenu: VKSideMenu, itemForRowAt indexPath: IndexPath) -> VKSideMenuItem {
           // This solution is provided for DEMO propose only
           // It's beter to store all items in separate arrays like you do it in your UITableView's. Right?
           
           var item = VKSideMenuItem()
           if indexPath.section == 0 {
            let title = loggedUser?.name
               let code = ""
            let mobile = loggedUser?.mobile_no
                
            item.title = title!
            item.desc = mobile!
            if let imgurl = loggedUser?.profile_image {
                let url = imgBaseURL + custProfileImg + imgurl
                item.imgurl = url
            }
            
                
           }
           else {
            let menuArray = menuSectionArr[indexPath.section]
               item = menuArray[indexPath.row]
           }
           return item
       }
       
       func sideMenu(_ sideMenu: VKSideMenu, heightForRowAt indexPath: IndexPath) -> CGFloat {
           if indexPath.section == 0 {
               return 100.0
           }
           else {
               return 40.0
           }
       }
       
       // MARK: - VKSideMenuDelegate
       @objc func sideMenu(_ sideMenu: VKSideMenu, didSelectRowAt indexPath: IndexPath) {
        print("Mene selected")
        var item = VKSideMenuItem()
        if indexPath.section == 1 {
            let menuArray = menuSectionArr[indexPath.section]
            item = menuArray[indexPath.row]
            
            if item.title == MENU_HOME {
                //
                let sb = UIStoryboard(name: IDChemist, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "ChemistDashboard") as! ChemistDashboard
                
                self.navigationController?.pushViewController(vc, animated: false)
            }
            if item.title == MENU_PROFILE {
                
                let sb = UIStoryboard(name: IDChemist, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "UpdateChemistProfileVC") as! UpdateChemistProfileVC
                
                self.navigationController?.pushViewController(vc, animated: false)
                
            }
            if item.title == MENU_BRANCHES {
                
                let sb = UIStoryboard(name: IDChemist, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "BranchListVC") as! BranchListVC
                //vc.addBranch = true
                self.navigationController?.pushViewController(vc, animated: false)
                
            }
            if item.title == MENU_RATINGS {
                
                let sb = UIStoryboard(name: IDServicePro, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "UserRatingsVC") as! UserRatingsVC
                self.navigationController?.pushViewController(vc, animated: false)
                
            }
            if item.title == MENU_HELP {
                
                let sb = UIStoryboard(name: IDMain, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "FAQVC") as! FAQVC
                //vc.addBranch = true
                self.navigationController?.pushViewController(vc, animated: false)
                
            }
            if item.title == MENU_CONTACT {
                
                let sb = UIStoryboard(name: IDMain, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
                //vc.addBranch = true
                self.navigationController?.pushViewController(vc, animated: false)
                
            }
            if item.title == MENU_LOGOUT {
                
                self.alertAction(strTitle: "Alert", strMsg: "Do you want to logout?") { (action) in
                    
                    if let title = action.title, title == "Yes" {
                        self.clearUserData()
                        NotificationCenter.default.post(.init(name: Notification.Name(rawValue: LoginNotification)))
                    }
                }
            }
        }
        
       }
       
    func clearUserData() {
        
        UserDefaults.standard.set("0", forKey: "role")
        UserDefaults.standard.set("", forKey: "deviceId")
        UserDefaults.standard.set(0, forKey: "newuser")
        UserDefaults.standard.set("", forKey: "profile_completed")
        UserDefaults.standard.set(0, forKey: "HealthRecordSegment")
        UserDefaults.standard.set(0, forKey: "OrderSegment")
        UserDefaults.standard.set(0, forKey: "doctor_id")
        //
        
        
        
        let users = self.coreDataManager.fetchLocalUsers()
        let user = users[0]
        let id = user.id
        self.coreDataManager.removeUser(id: Int(id))
    }
    
       func editOnHeaderSideMenu(){
            
            let sb = UIStoryboard(name: IDChemist, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "UpdateChemistProfileVC")
            self.navigationController?.pushViewController(vc, animated: false)
       }

       @objc func sideMenuDidShow(_ sideMenu: VKSideMenu)
       {
           
       }
       @objc func sideMenuDidHide(_ sideMenu: VKSideMenu)
       {
       }
    
       func populateVKSlideMenuData(_ mArray: [String]) -> [VKSideMenuItem] {
           var temp = [VKSideMenuItem]()
           for menu: String in mArray {
               let item = VKSideMenuItem()

           }
           return temp
       }
}
