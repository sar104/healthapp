//
//  ChemistDashboard.swift
//  HealthApp
//
//  Created by Apple on 27/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class ChemistDashboard: BaseChemistVC {

    @IBOutlet weak var tblData: UITableView!
    
    var user: User?
    var chemistDashData: ChemistDashData?
    var arrBanner: [BannerData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let users = self.fetchUser()
        user = users[0]
        
        getChemistDash()
        getBannersData()
    }
    
    func getChemistDash() {
        
        /*
         {
             "approval_pending" = 0;
             delivered = 0;
             message = "Record Found.";
             pending = 4;
             "pending_delivery" = 0;
             status = 1;
         }
         **/
        IHProgressHUD.show()
        
        let params = ["chemist_id": user?.id]
        
        apiManager.getChemistDashboard(params: params) { (result) in
            
            DispatchQueue.main.async {
                IHProgressHUD.dismiss()
            }
            switch result {
                
                case .success(let data):
                    print(data)
                    self.chemistDashData = data
                case .failure(let err):
                    print(err.localizedDescription)
            }
        }
    }
    
    func getBannersData(){
        
        IHProgressHUD.show()
        
        let params = ["user_id":user!.id] as [String : Any]
        apiManager.getBanners(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                //print("data: \(data)")
                self.arrBanner = data.filter{ ( $0.image_video != "null")}
                
                DispatchQueue.main.async {
                    self.tblData.dataSource = self
                    self.tblData.delegate = self
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func navOrders(tag: Int) {
        
        //ChemistOrdersVC
        
        let vc = self.getVC(with: "ChemistOrdersVC", sb: IDChemist) as! ChemistOrdersVC
        vc.type = tag
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func ordersAction(_ sender: UIButton) {
        
        let tag = sender.tag
        
        self.navOrders(tag: tag)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChemistDashboard: ChemistDashCellDelegate {
    
    func onBannerClick() {
        
    }
}

extension ChemistDashboard: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = ChemistDashCell()
        
        switch indexPath.section {
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! ChemistDashCell
            
            cell.lblShopName.text = user?.clinic_lab_hospital_name
            cell.lblName.text = user?.name
            cell.lblMob.text = user?.mobile_no
            return cell
            
        case 1:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ordersCell") as! ChemistDashCell
            cell.lblShopName.text = String(self.chemistDashData?.pending ?? 0)
            cell.lblName.text = String(self.chemistDashData?.pending_delivery ?? 0)
            cell.lblMob.text = String(self.chemistDashData?.delivered ?? 0)
            
            if let btnPending = cell.contentView.viewWithTag(1) as? UIButton {
                
                btnPending.addTarget(self, action: #selector(ordersAction(_:)), for: .touchUpInside)
            }
            if let btnPendingDel = cell.contentView.viewWithTag(2) as? UIButton {
                
                btnPendingDel.addTarget(self, action: #selector(ordersAction(_:)), for: .touchUpInside)
            }
            if let btnDelivered = cell.contentView.viewWithTag(3) as? UIButton {
                
                btnDelivered.addTarget(self, action: #selector(ordersAction(_:)), for: .touchUpInside)
            }
            
            cell.setup(id: "ordersCell")
            
            return cell
            
        case 2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "bannerCell") as! ChemistDashCell
            
            cell.delegate = self
            cell.bannerArr = self.arrBanner
            cell.bannerCollView.reloadData()
            
            return cell
            
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 2 {
            
            return "Top News"
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            return 150
            
        case 1:
            return 250
            
        case 2:
            return 200
            
        default:
            break
        }
        return 0
    }
     
}

protocol ChemistDashCellDelegate {
    
    func onBannerClick()
}

class ChemistDashCell: BaseCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblShopName: UILabel!
    @IBOutlet weak var lblMob: UILabel!
    @IBOutlet weak var lblSwitch: UILabel!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var openCloseSwitch: UISwitch!
    
    @IBOutlet weak var contView: UIView!
    @IBOutlet weak var bannerCollView: UICollectionView!
    
    var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        //gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [RGB_254_0_75.cgColor, RGB_0_197_223.cgColor]
        //gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
       return gradientLayer
    }()
    
    var bannerArr: [BannerData] = []
    var delegate: ChemistDashCellDelegate?
    
    func setup(id: String) {
        if id == "ordersCell" {
            
            self.contView.pathSemiCirclesPathForView(givenView: self.contView)
        
            gradientLayer.frame = self.contView.bounds
            self.contView.layer.insertSublayer(gradientLayer, at: 0)
            self.contView.layer.cornerRadius = 5
            self.contView.layer.masksToBounds = true
        }
    }
}
extension ChemistDashCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return bannerArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath) as! DashboardCollectionCell
        
        let bannerData = bannerArr[indexPath.row]
        if let imgname = bannerData.image_video {
            let url = bannerURL + imgname
            cell.bannerImg.kf.setImage(with: URL(string: url))
            cell.bannerImg.layer.masksToBounds = true
            cell.bannerImg.layer.cornerRadius = 10
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
      
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 150, height: 140)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //BlogVC
        delegate?.onBannerClick()
        
    }
}
