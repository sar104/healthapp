//
//  ChemistOrdersVC.swift
//  HealthApp
//
//  Created by Apple on 27/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class ChemistOrdersVC: BaseChemistVC {

    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var lblHead: UILabel!
    
    var ordersArr: [ChemistOrderData] = []
    var user: User?
    var type: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let users = self.fetchUser()
        user = users[0]
        
        tblData.estimatedRowHeight = 500
        
        getOrders()
    }
    

    func getOrders() {
        
        let params = ["chemist_id": user?.id]
        var api = GetOrderListChemistOwner
        
        if type == 1 {
            lblHead.text = "ORDER ENQUIRY"
            
            if let parent_id = user?.parent_id {
                api = GetOrderListChemist
                
            } else {
                api = GetOrderListChemistOwner
            }
        }
        if type == 2 {
            lblHead.text = "ORDER PENDING COMPLETION"
            
            if let parent_id = user?.parent_id {
                api = GetAcceptedOrdersAPI
                
            } else {
                api = GetAcceptedOrdersOwnerAPI
            }
        }
        if type == 3 {
            lblHead.text = "ORDERS COMPLETED"
            
            if let parent_id = user?.parent_id {
                api = GetDeliveredOrdersAPI
                
            } else {
                api = GetDeliveredOrdersOwnerAPI
            }
        }
        
        IHProgressHUD.show()
        
        apiManager.getOrderListChemist(api: api, params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                
                case .success(let data):
                    print(data)
                    self.ordersArr = data
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                    print(err.localizedDescription)
            }
        }
    }
    
    func updateOrder(data: ChemistOrderData) {
        
        /*
         http://www.doctoronhire.com/health/api/update_order_for_chemist?order_id=558&drug_list=[{"medicine_name":"{\"medicine_name\":\"COMBIPLET\",\"suggested_name\":\"\",\"medicine_type\":\"NPM\"}","quantity":"3","unit":"Capsule","available_status":"Yes"},{"medicine_name":"{\"medicine_name\":\"K TRIP PLUS\",\"suggested_name\":\"NUMINIC\",\"medicine_type\":\"NPM\"}","quantity":"0","unit":"Tablet","available_status":"No"}]&estimated_cost=50
         **/
        
        var params: [String : Any] = [:]
        
        do {
            
            var drugArr: [CDrugData] = []
            
            let arrMed = data.order_details.filter{ ($0.getMedicineData().medicine_type == "PM") } ?? []
            
            let arrItem = data.order_details.filter{ ($0.getMedicineData().medicine_type == "NPM") } ?? []
            
            if arrMed.count > 0 {
                
                for med in arrMed {
                   
                    
                    var medname = med.getMedicineData()
                    medname.suggested_name = med.suggested_name
                    
                    var  drugdata = CDrugData()
                    
                    drugdata.medicine_name = medname
                    drugdata.unit = med.unit
                    drugdata.quantity = med.quantity
                    drugdata.available_status = med.available_status
                    
                    drugArr.append(drugdata)
                }
                print("drugs: \(drugArr)")
            }
            if arrItem.count > 0 {
                
                for med in arrItem {
                   
                    
                    var medname = med.getMedicineData()
                    medname.suggested_name = med.suggested_name
                    
                    var  drugdata = CDrugData()
                    
                    drugdata.medicine_name = medname
                    drugdata.unit = med.unit
                    drugdata.quantity = med.quantity
                    drugdata.available_status = med.available_status
                    
                    drugArr.append(drugdata)
                }
                print("drugs: \(drugArr)")
            }
            
            let drugJSonData = try JSONEncoder().encode(drugArr)
            let jsonStrDrugs = String(data: drugJSonData, encoding: .utf8)
            
            
            params["drug_list"] = jsonStrDrugs
            params["order_id"] = data.id
            params["estimated_cost"] = data.estimated_cost
            
            print("params: \(params)")
            /*
            IHProgressHUD.show()
            
            apiManager.updateOrderForChemist(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            
                        }
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
                
            }
            */
        } catch let err {
            print(err)
        }
    }
    
    @IBAction func viewAction(_ sender: UIButton) {
        
        let tag = sender.tag
        let data = self.ordersArr[tag]
        
        //ViewOrdersDetailsVC
        let vc = self.getVC(with: "ViewOrdersDetailsVC", sb: IDChemist) as! ViewOrdersDetailsVC
        if data.status == "Delivery Pending" {
            vc.type = "pending"
        }
        if data.status == "Delivered" {
            vc.type = "completed"
        }
        vc.orderDetail = data
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func editAction(_ sender: UIButton) {
        
        let tag = sender.tag
        let data = self.ordersArr[tag]
        
        let vc = self.getVC(with: "ChemistOrdersDetailsVC", sb: IDChemist) as! ChemistOrdersDetailsVC
        
        vc.orderDetail = data
        
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func confirmAction(_ sender: UIButton) {
        
        let tag = sender.tag
        let data = self.ordersArr[tag]
        
        updateOrder(data: data)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChemistOrdersVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ordersArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! ChemistOrdersCell
        
        let data = self.ordersArr[indexPath.row]
        cell.arrMed = data.order_details//data.order_details.filter{ ($0.getMedicineData().medicine_type == "PM") }
        cell.arrItem = data.order_details.filter{ ($0.getMedicineData().medicine_type == "NPM") }
        
        cell.setup()
        cell.populateData(data: data)
        cell.btnEdit.tag = indexPath.row
        
        cell.btnConfirm.isHidden = true
        
        
        if type == 1 {
            
            cell.btnConfirm.isHidden = false
            cell.contentView.removeConstraint(cell.con_leading)
            cell.btnEdit.addTarget(self, action: #selector(editAction(_:)), for: .touchUpInside)
            cell.btnConfirm.tag = indexPath.row
            cell.btnConfirm.addTarget(self, action: #selector(confirmAction(_:)), for: .touchUpInside)
        } else {
            cell.btnEdit.setTitle("View Details", for: .normal)
            cell.btnEdit.backgroundColor = RGB_btnReOrder
            cell.btnEdit.setTitleColor(UIColor.white, for: .normal)
            cell.btnEdit.addTarget(self, action: #selector(viewAction(_:)), for: .touchUpInside)
            cell.contentView.addConstraint(cell.con_leading)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension//500
    }
}

protocol ChemistOrdersCellDelegate {
    
    func onValueChanged(tag: Int, type: String, status: Bool)
    func onEditMedicine(id: Int, value: String, type: String)
    func onSubmitClicked(tag: Int)
}

class ChemistOrdersCell: BaseCell, UITextFieldDelegate {
    
    @IBOutlet weak var tblMed: MedTableView!
    @IBOutlet weak var con_tblHgt: NSLayoutConstraint!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblOrderNo: UILabel!
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblDoctor: UILabel!
    @IBOutlet weak var lblOrderType: UILabel!
    @IBOutlet weak var lblExpDate: UILabel!
    @IBOutlet weak var lblExpTime: UILabel!
    @IBOutlet weak var lblDel: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var lblFromVal: UILabel!
    @IBOutlet weak var lblCostVal: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var con_leading: NSLayoutConstraint!
    @IBOutlet weak var medSwitch: UISwitch!
    @IBOutlet weak var txtMed: BottomLineTextField!
    var delegate: ChemistOrdersCellDelegate?
    
    var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        //gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [RGB_254_0_75.cgColor, RGB_0_197_223.cgColor]
        gradientLayer.locations = [0.0, 1.0]
       return gradientLayer
    }()
    
    var arrMed: [OrderDetails] = []
    var arrItem: [OrderDetails] = []
    
    func setup() {
        
        tblMed.dataSource = self
        tblMed.delegate = self
        /*
        gradientLayer.frame = bgView.bounds
        self.bgView.layer.insertSublayer(gradientLayer, at: 0)
        self.bgView.layer.cornerRadius = 5
        self.bgView.layer.masksToBounds = true
         */
        self.bgView.pathSemiCirclesPathForView(givenView: self.bgView)
        
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
    }
    
    func populateData(data: ChemistOrderData) {
        
        self.lblDate.text = data.estimated_date?.toDate(format: df_dd_MM_yyyy).toString(format: "dd-MMM-yyyy hh:mm a")
        self.lblOrderNo.text = String(data.id!)
        self.lblUser.text = data.user_name
        //self.lblContact.text = data
        self.lblDoctor.text = data.doctor_name
        self.lblOrderType.text = data.prescription_type
        self.lblExpDate.text = data.estimated_date
        self.lblExpTime.text = data.slot
        self.lblDel.text = data.delivery_type
        
        if data.status == "Pending" {
            
            if lblFrom != nil {
                self.lblFrom.isHidden = true
            }
            if lblFromVal != nil {
                self.lblFromVal.isHidden = true
            }
            if lblCost != nil {
                self.lblCost.isHidden = true
            }
            if lblCostVal != nil {
                self.lblCostVal.isHidden = true
            }
            
            if let img = self.contentView.viewWithTag(4) as? UIImageView {
                img.isHidden = true
            }
        } else {
            if lblFrom != nil {
                self.lblFrom.text = (data.status == "Delivered") ? "ORDER COMPLETED ON" : "ORDER PENDING FROM"
            }
            if lblFromVal != nil {
                self.lblFromVal.text = data.estimated_date
            }
            if lblCost != nil {
                self.lblCost.text = "BILL AMOUNT"
            }
            if lblCostVal != nil {
                self.lblCostVal.text = String(data.estimated_cost ?? 0)
            }
        }
        
        if let txtMed = self.txtMed {
            txtMed.delegate = self
            txtMed.accessibilityIdentifier = "cost"
            txtMed.keyboardType = .numberPad
            txtMed.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "cost")
        }
    }
    
    @IBAction func onValueChanged(_ sender: UISwitch) {
        
        let tag = sender.tag
        let type = sender.accessibilityIdentifier!
        
        delegate?.onValueChanged(tag: tag, type: type, status: sender.isOn)
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        
        delegate?.onSubmitClicked(tag: sender.tag)
    }
    
    @IBAction func checkAction(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        delegate?.onValueChanged(tag: 0, type: "check", status: sender.isSelected)
    }
    
    override func doneCellPicker(sender: UIBarButtonItem) {
        
        //let id = sender.tag
        
        //delegate?.onEditMedicine(id: id, value: txtMed.text ?? "", type: sender.accessibilityIdentifier!)
        self.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let value = (textField.text ?? "") + string
        delegate?.onEditMedicine(id: textField.tag, value: value, type: textField.accessibilityIdentifier!)
        
        return true
    }
}
extension ChemistOrdersCell: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
    
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return section == 0 ? arrMed.count : arrItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "medCell") as! ChemistOrdersCell
        var data: OrderDetails?
        
        if indexPath.section == 0 {
            data = arrMed[indexPath.row]
            
            if data?.available_status == "No" {
                cell = tableView.dequeueReusableCell(withIdentifier: "suggestCell") as! ChemistOrdersCell
                
                cell.txtMed.tag = indexPath.row
                cell.txtMed.accessibilityIdentifier = "PM"
                cell.txtMed.delegate = self
                cell.txtMed.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "PM")
            }
            if let medSwitch = cell.medSwitch {
                medSwitch.isOn = (data?.available_status == "Yes" || data?.available_status == nil) ? true : false
                medSwitch.tag = indexPath.row
                medSwitch.accessibilityIdentifier = "PM"
            }
            cell.lblDoctor.text = data?.getMedicine()
            if let qty = data?.quantity {
                cell.lblDel.text = String(qty)
            }
            cell.delegate = delegate
        }
        if indexPath.section == 1 {
            data = arrItem[indexPath.row]
            
            if data?.available_status == "No" {
                cell = tableView.dequeueReusableCell(withIdentifier: "suggestCell") as! ChemistOrdersCell
                
                cell.txtMed.tag = indexPath.row
                cell.txtMed.accessibilityIdentifier = "NPM"
                cell.txtMed.delegate = self
                cell.txtMed.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "PM")
            }
            if let medSwitch = cell.medSwitch {
                medSwitch.isOn = (data?.available_status == "Yes" || data?.available_status == nil) ? true : false
                medSwitch.tag = indexPath.row
                medSwitch.accessibilityIdentifier = "NPM"
            }
            cell.lblDoctor.text = data?.getMedicine()
            if let qty = data?.quantity {
                cell.lblDel.text = String(qty)
            }
            cell.delegate = delegate
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "headCell") as! ChemistOrdersCell
        
        if section == 0 {
            cell.lblDoctor.text = "MEDICINE DETAILS"
        }
        if section == 1 {
            cell.lblDoctor.text = "ITEM DETAILS"
        }
        
        cell.lblDoctor.textColor = .lightGray
        
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            //return arrMed.count > 0 ? 30 : 0
        }
        if section == 1 {
            //return arrItem.count > 0 ? 30 : 0
        }
        return 30//0
    }
    
    
    /*
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (cell.responds(to: #selector(getter: UIView.tintColor))) {
            let cornerRadius: CGFloat = 5
            cell.backgroundColor = UIColor.clear
            let layer: CAShapeLayer  = CAShapeLayer()
            let pathRef: CGMutablePath  = CGMutablePath()
            let bounds: CGRect  = cell.bounds.insetBy(dx: 10, dy: 0)
            var addLine: Bool  = false
            if (indexPath.row == 0 && indexPath.row == tableView.numberOfRows(inSection: indexPath.section)-1) {
                pathRef.__addRoundedRect(transform: nil, rect: bounds, cornerWidth: cornerRadius, cornerHeight: cornerRadius)
            } else if (indexPath.row == 0) {
                pathRef.move(to: CGPoint(x:bounds.minX,y:bounds.maxY))
                pathRef.addArc(tangent1End: CGPoint(x:bounds.minX,y:bounds.minY), tangent2End: CGPoint(x:bounds.midX,y:bounds.minY), radius: cornerRadius)

                pathRef.addArc(tangent1End: CGPoint(x:bounds.maxX,y:bounds.minY), tangent2End: CGPoint(x:bounds.maxX,y:bounds.midY), radius: cornerRadius)
                pathRef.addLine(to: CGPoint(x:bounds.maxX,y:bounds.maxY))
                addLine = true;
            } else if (indexPath.row == tableView.numberOfRows(inSection: indexPath.section)-1) {

                pathRef.move(to: CGPoint(x:bounds.minX,y:bounds.minY))
                pathRef.addArc(tangent1End: CGPoint(x:bounds.minX,y:bounds.maxY), tangent2End: CGPoint(x:bounds.midX,y:bounds.maxY), radius: cornerRadius)

                pathRef.addArc(tangent1End: CGPoint(x:bounds.maxX,y:bounds.maxY), tangent2End: CGPoint(x:bounds.maxX,y:bounds.midY), radius: cornerRadius)
                pathRef.addLine(to: CGPoint(x:bounds.maxX,y:bounds.minY))

            } else {
                pathRef.addRect(bounds)
                addLine = true
            }
            layer.path = pathRef
            //CFRelease(pathRef)
            //set the border color
            layer.strokeColor = UIColor.lightGray.cgColor;
            //set the border width
            layer.lineWidth = 1
            layer.fillColor = UIColor(white: 1, alpha: 1.0).cgColor


            if (addLine == true) {
                let lineLayer: CALayer = CALayer()
                let lineHeight: CGFloat  = (1 / UIScreen.main.scale)
                lineLayer.frame = CGRect(x:bounds.minX, y:bounds.size.height-lineHeight, width:bounds.size.width, height:lineHeight)
                lineLayer.backgroundColor = tableView.separatorColor!.cgColor
                layer.addSublayer(lineLayer)
            }

            let testView: UIView = UIView(frame:bounds)
            testView.layer.insertSublayer(layer, at: 0)
            testView.backgroundColor = UIColor.clear
            cell.backgroundView = testView
        }

    }
    */
}

class MedTableView: UITableView {
    
    override var intrinsicContentSize: CGSize {
        return self.contentSize
    }
    
    override var contentSize: CGSize {
        
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
}
