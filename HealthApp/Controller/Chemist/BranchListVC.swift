//
//  BranchListVC.swift
//  HealthApp
//
//  Created by Apple on 14/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class BranchListVC: BaseChemistVC {

    @IBOutlet weak var tblData: UITableView!
     
    var chemistArr: [ChemistData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getBranches()
        
        
    }
    

    func getBranches() {
        
        //getChemistBranches
        let params = ["parent_id": loggedUser!.id]
        apiManager.getChemistBranches(params: params) { (result) in
            
            switch result {
                
                case .success(let data):
                    
                    print("data: \(data)")
                    self.chemistArr = data
                    DispatchQueue.main.async {
                    
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                    
                    print("err")
            }
        }
    }
    
    @IBAction func addAction(_ sender: UIButton) {
        
        let vc = self.getVC(with: "UpdateChemistProfileVC", sb: IDChemist) as! UpdateChemistProfileVC
        vc.addBranch = true
        self.navigationController?.pushViewController(vc, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BranchListVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chemistArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "chemistCell") as! ChemistCell
        
        let data = chemistArr[indexPath.row]
        cell.populateChemistData(data: data)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = chemistArr[indexPath.row]
        
        let vc = self.getVC(with: "UpdateChemistProfileVC", sb: IDChemist) as! UpdateChemistProfileVC
        
        vc.chemistData = data
        vc.editBranch = true
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
}

class BranchCell: BaseCell {
    
    
}
