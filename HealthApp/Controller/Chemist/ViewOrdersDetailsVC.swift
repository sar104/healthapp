//
//  ViewOrdersDetailsVC.swift
//  HealthApp
//
//  Created by Apple on 05/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class ViewOrdersDetailsVC: BaseChemistVC {

    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblHead: UILabel!
    
    var orderDetail: ChemistOrderData?
    var user: User?
    var arrMed: [OrderDetails] = []
    var arrItem: [OrderDetails] = []
    var type: String?
    
    var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        //gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [RGB_254_0_75.cgColor, RGB_0_197_223.cgColor]
        gradientLayer.locations = [0.0, 1.0]
       return gradientLayer
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let users = self.fetchUser()
        user = users[0]
        
        if type == "completed" {
            lblHead.text = "ORDER COMPLETED"
        }
        if type == "pending" {
            lblHead.text = "ORDER PENDING COMPLETION"
        }
        if type == "new" {
            lblHead.text = "ORDER ENQUIRY"
        }
        
        self.tblData.estimatedRowHeight = 70
        self.bgView.pathSemiCirclesPathForView(givenView: self.bgView)
        
        gradientLayer.frame = bgView.bounds
        self.bgView.layer.insertSublayer(gradientLayer, at: 0)
        self.bgView.layer.cornerRadius = 5
        self.bgView.layer.masksToBounds = true
        
        arrMed = orderDetail?.order_details.filter{ ($0.getMedicineData().medicine_type == "PM") } ?? []
        
        arrItem = orderDetail?.order_details.filter{ ($0.getMedicineData().medicine_type == "NPM") } ?? []
    }
    
    func updateDelOrder() {
        
        let params = ["order_id": orderDetail!.id,
        "chemist_id": user!.id] as [String: Any]
        
        IHProgressHUD.show()
        
        apiManager.updateDelOrderForChemist(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        
                    }
                case .failure(let err):
                    print("err: \(err)")
                    self.alert(strTitle: strErr, strMsg: err.localizedDescription)
            }
        }
    }
    
    func updatePickOrder() {
        
        let params = ["order_id": orderDetail!.id,
        "chemist_id": user!.id] as [String: Any]
        
        IHProgressHUD.show()
        
        apiManager.updatePickOrderForChemist(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        
                    }
                case .failure(let err):
                    print("err: \(err)")
                    self.alert(strTitle: strErr, strMsg: err.localizedDescription)
            }
        }
    }
    
    @IBAction func orderAction(_ sender: UIButton) {
     
        if orderDetail?.delivery_type == "Pickup" {
            //pickup api
            updatePickOrder()
        } else {
            // deliver api
            updateDelOrder()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ViewOrdersDetailsVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 6
        case 1:
            return arrMed.count
        case 2:
            return arrItem.count
        case 3:
            return 5
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "dateCell") as! ViewOrdersDetailsCell
                let date = orderDetail?.estimated_date?.toDate(format: "dd-MM-yyyy")
                let strdate = date?.toString(format: "dd-MMM-yyyy hh:mm a")
                cell.lblName.text = strdate//orderDetail?.estimated_date
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderNoCell") as! ViewOrdersDetailsCell
                
                if let lbl1 = cell.contentView.viewWithTag(2) as? UILabel {
                    lbl1.text = String(orderDetail!.id!)
                }
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "addressCell") as! ViewOrdersDetailsCell
                
                if let lbl1 = cell.contentView.viewWithTag(1) as? UILabel {
                    lbl1.text = orderDetail?.user_name
                }
                if let lbl2 = cell.contentView.viewWithTag(2) as? UILabel {
                    if let mobile = orderDetail?.mobile_no {
                        lbl2.text = String(mobile)
                    }
                }
                if let lbl3 = cell.contentView.viewWithTag(3) as? UILabel {
                    lbl3.text = orderDetail?.customer_address
                }
                return cell
                
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell") as! ViewOrdersDetailsCell
                
                if let lbl1 = cell.contentView.viewWithTag(1) as? UILabel {
                    lbl1.text = "DOCTOR'S NAME"
                }
                if let lbl2 = cell.contentView.viewWithTag(2) as? UILabel {
                    lbl2.text = orderDetail?.doctor_name
                }
                
                return cell
                
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell") as! ViewOrdersDetailsCell
                
                if let lbl1 = cell.contentView.viewWithTag(1) as? UILabel {
                    lbl1.text = "ORDER TYPE"
                }
                if let lbl2 = cell.contentView.viewWithTag(2) as? UILabel {
                    lbl2.text = orderDetail?.prescription_type
                }
                
                return cell
                
            case 5:
                let cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! ViewOrdersDetailsCell
                if let img = orderDetail?.prescription_image {
                    let url = prescriptionImageURL + img
                    cell.imgView.kf.setImage(with: URL(string: url))
                }
                return cell
                
            default:
                break
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "medCell") as! ViewOrdersDetailsCell
            let data = arrMed[indexPath.row]
            cell.lblName.text = data.getMedicine()
            if let qty = data.quantity {
                cell.lblQty.text = String(qty)
            }
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "medCell") as! ViewOrdersDetailsCell
            let data = arrItem[indexPath.row]
            cell.lblName.text = data.getMedicine()
            if let qty = data.quantity {
                cell.lblQty.text = String(qty)
            }
            
            return cell
        case 3:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell") as! ViewOrdersDetailsCell
                
                if let lbl1 = cell.contentView.viewWithTag(1) as? UILabel {
                    lbl1.text = "ORDER INSTRUCTIONS"
                }
                if let lbl2 = cell.contentView.viewWithTag(2) as? UILabel {
                    lbl2.text = orderDetail?.description
                }
                
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ViewOrdersDetailsCell
                
                if let lbl1 = cell.contentView.viewWithTag(1) as? UILabel {
                    lbl1.text = "DELIVERY/PICKUP"
                }
                if let lbl2 = cell.contentView.viewWithTag(2) as? UILabel {
                    lbl2.text = orderDetail?.delivery_type
                }
                if let lbl3 = cell.contentView.viewWithTag(3) as? UILabel {
                    lbl3.text = "ORDER STATUS"
                }
                if let lbl4 = cell.contentView.viewWithTag(4) as? UILabel {
                    lbl4.text = orderDetail!.status!
                }
                return cell
                
            case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ViewOrdersDetailsCell
            
            if let lbl1 = cell.contentView.viewWithTag(1) as? UILabel {
                lbl1.text = "EXPECTED ON"
            }
            if let lbl2 = cell.contentView.viewWithTag(2) as? UILabel {
                lbl2.text = orderDetail?.estimated_date
            }
            if let lbl3 = cell.contentView.viewWithTag(3) as? UILabel {
                lbl3.text = "TIME"
            }
            if let lbl4 = cell.contentView.viewWithTag(4) as? UILabel {
                lbl4.text = orderDetail?.slot
            }
            
            return cell
                
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell") as! ViewOrdersDetailsCell
                
                if let lbl1 = cell.contentView.viewWithTag(1) as? UILabel {
                    lbl1.text = "BILL AMOUNT"
                }
                if let lbl2 = cell.contentView.viewWithTag(2) as? UILabel {
                    if let cost = orderDetail?.estimated_cost {
                        lbl2.text = String(cost)
                    }
                }
                
                return cell
            
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! ViewOrdersDetailsCell
                
                
                if orderDetail?.delivery_type == "Pickup" {
                    cell.btnOrder.setTitle("ORDER PICKEDUP", for: .normal)
                } else {
                    cell.btnOrder.setTitle("ORDER DELIVERED", for: .normal)
                }
                cell.btnOrder.addTarget(self, action: #selector(orderAction(_:)), for: .touchUpInside)
                
                return cell
                
            default:
                break
            }
        default:
            break
        }
        return ViewOrdersDetailsCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            if indexPath.row == 5 {
                return 150
            }
        }
        if indexPath.section == 3 {
            if indexPath.row == 4 {
                return (type == "completed") ? 0 : UITableView.automaticDimension
            }
        }
        return UITableView.automaticDimension
    }
    
}
class ViewOrdersDetailsCell: BaseCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnOrder: UIButton!
}
