//
//  UpdateChemistProfileVC.swift
//  HealthApp
//
//  Created by Apple on 28/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import RadioGroup
import IHProgressHUD

class UpdateChemistProfileVC: BaseChemistVC {

    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var btnUpdate: UIButton!
    
    var profileImgData: Data?
    
    let picker = UIImagePickerController()
    var profile_image: String?
    var user: User?
    var editFlag: Bool = false
    var chemistData: ChemistData?
    var statesArr:[State] = []
    var another_contact: Bool = false
    var addBranch: Bool = false
    var editBranch: Bool = false
    
    var labelArr = ["","MEDICAL STORE NAME","CHEMIST NAME","EMAIL","ADDRESS","LICENSE / REGISTRATION NO","GST","HOME DELIVERY","OFF DAY","TIME"]
    
    var editLabelArr = ["","Chemist Name","Medical Store Name","Email","License / Registration No","GST (15 Characters)","Address","Home/Address/PO Box","Pincode","State","City","Off Day","Home Delivery","Open Time","","",""]
    
    var branchLabelArr = ["","Chemist/Manager Name","Branch Medical Store Name","Branch Shop Mobile Number","Email","License / Registration No","GST (15 Characters)","Address","Home/Address/PO Box","Pincode","State","City","Off Day","Home Delivery","Open Time","","",""]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.fetchStates()
        if !addBranch && !editBranch {
            self.getChemistProfile()
        } else {
            if chemistData == nil {
                self.chemistData = ChemistData()
                self.chemistData?.delivery_status = "Yes"
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !addBranch && !editBranch {
            btnUpdate.isHidden = true
        }
    }
    
    
    
    func getChemistProfile() {
        
        let params = ["id": loggedUser?.id]
        
        apiManager.getChemistProfile(params: params) { (result) in
            
            switch result {
                
            case .success(let data):
                
                self.chemistData = data
                self.tblData.reloadData()
                
            case .failure(let err):
                
                self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                
            }
        }
    }
    
    func fetchStates(){
        
        IHProgressHUD.show()
        apiManager.getStates(params: [:]) { (result) in
         
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                
                do {
                    let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
                    let status = jsonObj["status"] as! Int
                    if status == 1 {
                        if let states = jsonObj["data"]{
                            let statesData = try JSONSerialization.data(withJSONObject: states, options: .prettyPrinted)
                            let json = try JSONDecoder().decode([State].self, from: statesData)
                            self.statesArr = json
                            print("states: \(json)")
                        }
                    } else {
                        
                    }
                } catch let err {
                    print("err: \(err)")
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
            
        }
    }

    func uploadImage(){
        
        IHProgressHUD.show()
        
        apiManager.uploadImg(image: profileImgData!, name: "image", api: UploadChemistImgAPI) { (result) in
         
            IHProgressHUD.dismiss()
            switch result {
                case .success(let data):
                    let imgstr = String(data: data, encoding: .utf8)
                print(imgstr)
                    
                    if((imgstr?.contains("jpeg"))!){
                        self.chemistData?.shop_image = imgstr
                        if !self.addBranch {
                            self.updateChemistProfile()
                        } else {
                            self.addChemistBranch()
                        }
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
        
    }
    
    func updateChemistProfile() {
        
        var id: Int =  Int(loggedUser!.id)
        var parent_id = loggedUser!.parent_id
        
        if editBranch {
            id = chemistData!.id!
            parent_id = loggedUser!.id
        }
        let params: [String:Any] = ["id": id, "name": chemistData!.name!,
                      "clinic_lab_hospital_name": chemistData!.clinic_lab_hospital_name!,
                      "email": chemistData!.email!,
                      "registration_no": chemistData!.registration_no!,
                      "gst_no": chemistData?.gst_no ?? "-",
                      "state": chemistData!.state ?? "",
                      "city": chemistData!.city ?? "",
                      "address_type": chemistData!.address ?? "-",
                      "pincode": chemistData!.pincode ?? "",
                      "parent_id": parent_id,
                      "delivery_status": chemistData?.delivery_status ?? "",
                      "latitude": chemistData?.latitude ?? "",
                      "longitude": chemistData?.longitude ?? "",
                      "off_day": chemistData?.off_day ?? "",
                      "shop_opening_time": chemistData?.shop_opening_time ?? "", "shop_closing_time": chemistData?.shop_closing_time ?? "", "shop_image": chemistData?.shop_image ?? "",
                      "address": chemistData?.address ?? "", "mobile_no": chemistData!.mobile_no!]
        
        IHProgressHUD.show()
        
        apiManager.updateChemistProfile(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                print("data")
                DispatchQueue.main.async {
                    
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.navigationController?.popViewController(animated: false)
                }
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func addChemistBranch() {
        
        let params: [String:Any] = ["name": chemistData!.name!,
                      "clinic_lab_hospital_name": chemistData!.clinic_lab_hospital_name!,
                      "email": chemistData!.email!,
                      "registration_no": chemistData!.registration_no!,
                      "gst_no": chemistData?.gst_no ?? "-",
                      "state": chemistData!.state ?? "",
                      "city": chemistData!.city ?? "",
                      "address_type": chemistData!.address ?? "-",
                      "pincode": chemistData!.pincode ?? "",
                      "parent_id": loggedUser!.id ?? "",
                      "delivery_status": chemistData?.delivery_status ?? "",
                      "latitude": chemistData?.latitude ?? "0.0",
                      "longitude": chemistData?.longitude ?? "0.0",
                      "off_day": chemistData?.off_day ?? "",
                      "shop_opening_time": chemistData?.shop_opening_time ?? "", "shop_closing_time": chemistData?.shop_closing_time ?? "", "shop_image": chemistData?.shop_image ?? "-",
                      "address": chemistData?.address ?? "", "mobile_no": chemistData!.mobile_no!]
        
        print("branch params: \(params)")
        
        IHProgressHUD.show()
        
        apiManager.addMedicalBranch(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                print("data")
                DispatchQueue.main.async {
                    
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.navigationController?.popViewController(animated: false)
                }
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    @IBAction func editAction(_ sender: UIButton) {
        
        if sender.accessibilityIdentifier == "edit" {
            btnUpdate.isHidden = false
            editFlag = true
            self.tblData.reloadData()
        }
        if sender.accessibilityIdentifier == "map" {
            
            let vc = self.getVC(with: "ParallaxVC", sb: IDMain) as! ParallaxVC
            vc.type = "updateChemist"
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    
    func validate() -> Bool {
        
        if self.chemistData?.name == nil {
            self.alert(strTitle: strErr, strMsg: "Please enter name")
            return false
        }
        if self.chemistData?.clinic_lab_hospital_name == nil {
            self.alert(strTitle: strErr, strMsg: "Please enter medical store name")
            return false
        }
        
        if addBranch && self.chemistData?.mobile_no == nil {
            self.alert(strTitle: strErr, strMsg: "Please enter mobile number")
            return false
        }
        if self.chemistData?.email == nil {
            self.alert(strTitle: strErr, strMsg: "Please enter email")
            return false
        }
        if self.chemistData?.registration_no == nil {
            self.alert(strTitle: strErr, strMsg: "Please enter registration number")
            return false
        }
        if self.chemistData?.address == nil {
            self.alert(strTitle: strErr, strMsg: "Please enter address")
            return false
        }
        if self.chemistData?.state == nil {
            self.alert(strTitle: strErr, strMsg: "Please select state")
            return false
        }
        if self.chemistData?.city == nil {
            self.alert(strTitle: strErr, strMsg: "Please enter city")
            return false
        }
        if self.chemistData?.off_day == nil {
            self.alert(strTitle: strErr, strMsg: "Please select off day")
            return false
        }
        if self.chemistData?.shop_opening_time == nil {
            self.alert(strTitle: strErr, strMsg: "Please select open time")
            return false
        }
        if self.chemistData?.shop_closing_time == nil {
            self.alert(strTitle: strErr, strMsg: "Please select close time")
            return false
        }
        if another_contact {
            if self.chemistData?.assistant_name == nil {
                self.alert(strTitle: strErr, strMsg: "Please enter another contact name")
                return false
            }
            if self.chemistData?.alt_contact_no == nil {
                self.alert(strTitle: strErr, strMsg: "Please enter another contact no")
                return false
            }
        }
        
        return true
    }
    
    @IBAction func updateAction(_ sender: UIButton) {
     
        if validate() {
            if profileImgData != nil {
                
                self.uploadImage()
                
            } else {
                if !addBranch {
                    self.updateChemistProfile()
                } else {
                    self.addChemistBranch()
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UpdateChemistProfileVC: ParallaxDelegate {
    
    func onSelectLocation(address: String, lat: CGFloat, lon: CGFloat) {
        
        self.chemistData?.address = address
        self.chemistData?.latitude = Double(lat)
        self.chemistData?.longitude = Double(lon)
        
        self.tblData.reloadData()
    }
    
}

extension UpdateChemistProfileVC: ChemistProfileCellDelegate {
    
    
    func optionValueChanged(id: String, value: Int) {
        
        if id == "share_contact" {
            another_contact = (value == 0) ? false : true
        }
        
        if id == "delivery" {
            self.chemistData?.delivery_status = (value == 0) ? "Yes" : "No"
        }
        self.tblData.reloadData()
    }
    
    func onStateSelected(id: String) {
        self.chemistData?.state = id
        self.tblData.reloadData()
    }
    
    func onDataSelected(type: String, value: Any) {
        
        switch type {
        case "name":
            self.chemistData?.name = value as? String
            
        case "clinic_name":
            self.chemistData?.clinic_lab_hospital_name = value as? String
            
        case "email":
            self.chemistData?.email = value as? String
            
        case "license":
            self.chemistData?.registration_no = value as? String
            
        case "gst":
            self.chemistData?.gst_no = value as? String
            
        case "address":
            self.chemistData?.address = value as? String
            
        //case "address_type":
            //self.chemistData?.address_type = value as? String
            
        case "pincode":
            self.chemistData?.pincode = value as? String
            
        case "state":
            self.chemistData?.state = value as? String
            
        case "city":
            self.chemistData?.city = value as? String
            
        case "open":
            self.chemistData?.shop_opening_time = value as? String
            
        case "close":
            self.chemistData?.shop_closing_time = value as? String
            
        case "offday":
            self.chemistData?.off_day = value as? String
            
        case "assistant_name":
            self.chemistData?.assistant_name = value as? String
            
        case "alternate_no":
            self.chemistData?.alt_contact_no = value as? String
            
        case "mobile":
            if let strmob = value as? String {
                self.chemistData?.mobile_no = Int(strmob)
            }
            
        default:
            break
        }
        print("chemistdata: \(chemistData)")
    }
}

extension UpdateChemistProfileVC: UIImagePickerControllerDelegate {
    
    @objc func touchHappen(_ sender: UITapGestureRecognizer) {
            print("Tap On Image")
            picker.delegate = self
        let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {
                action in
                
                self.picker.allowsEditing = true
                self.picker.sourceType = .camera
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                self.present(self.picker, animated: true, completion: nil)
                
            }))
            alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: {
                action in
                
                self.picker.allowsEditing = true
                self.picker.sourceType = .photoLibrary
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                self.present(self.picker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            
            present(alert, animated: true, completion: nil)
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.editedImage] as? UIImage else {
            return
        }
            profileImgData = image.jpegData(compressionQuality: 0.5)
            self.tblData.reloadData()
            dismiss(animated: true, completion: nil)
        }

        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
}


extension UpdateChemistProfileVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if addBranch || editBranch {
            return branchLabelArr.count
        }
        return editFlag ? editLabelArr.count : labelArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UpdateChemistProfileCell()
        
        switch indexPath.row {
            
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! UpdateChemistProfileCell
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchHappen(_:)))
            //tap.delegate = self
            if profileImgData != nil {
                cell.profileImg.image = UIImage(data: profileImgData!)
            } else if let img = chemistData?.shop_image {
                
                let url = chemistProfileUrl + img
                cell.profileImg.kf.setImage(with: URL(string: url))
            }
            
            if (editFlag || addBranch || editBranch) {
                cell.profileImg.addGestureRecognizer(tap)
                cell.profileImg.isUserInteractionEnabled = true
            }
            
            return cell
            
        case 1...:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "editBtnCell") as! UpdateChemistProfileCell
            
            cell.delegate = self
            
            cell.btnEdit.isHidden = true
            
            if indexPath.row == 1 {
                cell.btnEdit.accessibilityIdentifier = "edit"
                cell.btnEdit.isHidden = (editFlag || addBranch) ? true : false
                cell.btnEdit.addTarget(self, action: #selector(editAction(_:)), for: .touchUpInside)
            }
            
            cell.bottomTxtFld.isEnabled = (editFlag || addBranch) ? true : false
            
            var label = "" //= editFlag ? editLabelArr[indexPath.row] : labelArr[indexPath.row]
            
            if addBranch || editBranch {
                label = branchLabelArr[indexPath.row]
            } else {
                label = editFlag ? editLabelArr[indexPath.row] : labelArr[indexPath.row]
            }
            
            cell.lblTitle.text = label
            
            switch indexPath.row {
                
            case 1:
                cell.bottomTxtFld.accessibilityIdentifier = "name"
                cell.bottomTxtFld.text = (editFlag || editBranch) ? self.chemistData?.name : self.chemistData?.clinic_lab_hospital_name
                
            case 2:
                cell.bottomTxtFld.accessibilityIdentifier = "clinic_name"
                cell.bottomTxtFld.text = (editFlag || editBranch) ? self.chemistData?.clinic_lab_hospital_name :  self.chemistData?.name
                cell.delegate = self
                
            case 3:
                cell.bottomTxtFld.accessibilityIdentifier = "mobile"
                if let mob = self.chemistData?.mobile_no {
                    cell.bottomTxtFld.text = String(mob)
                }
                cell.delegate = self
                
            case 4:
                cell.bottomTxtFld.accessibilityIdentifier = "email"
                cell.bottomTxtFld.text = self.chemistData?.email
                cell.delegate = self
                
            case 5:
                cell.bottomTxtFld.accessibilityIdentifier = "license"
                cell.bottomTxtFld.keyboardType = .numberPad
                cell.bottomTxtFld.text = (editFlag || editBranch) ? self.chemistData?.registration_no : self.chemistData?.address
                cell.delegate = self
                
            case 6:
                cell.bottomTxtFld.keyboardType = .default
                cell.bottomTxtFld.accessibilityIdentifier = "gst"
                cell.bottomTxtFld.text = (editFlag || editBranch) ? self.chemistData?.gst_no : self.chemistData?.registration_no
                cell.delegate = self
                
            case 7:
                
                //let cell = tableView.dequeueReusableCell(withIdentifier: "editBtnCell") as! UpdateChemistProfileCell
                
                cell.btnEdit.isHidden = false
                cell.bottomTxtFld.keyboardType = .default
                cell.bottomTxtFld.accessibilityIdentifier = "address"
                cell.bottomTxtFld.text = (editFlag || editBranch) ? self.chemistData?.address : self.chemistData?.gst_no
                cell.delegate = self
                cell.btnEdit.setImage(UIImage(named: "map"), for: .normal)
                cell.btnEdit.accessibilityIdentifier = "map"
                cell.btnEdit.addTarget(self, action: #selector(editAction(_:)), for: .touchUpInside)
                
                //return cell
                
            case 8:
                cell.bottomTxtFld.keyboardType = .default
                cell.bottomTxtFld.accessibilityIdentifier = "address_type"
                cell.bottomTxtFld.text = (editFlag || editBranch) ? self.chemistData?.address : self.chemistData?.delivery_status
                cell.delegate = self
                
            case 9:
                cell.bottomTxtFld.accessibilityIdentifier = "pincode"
                cell.bottomTxtFld.keyboardType = .numberPad
                cell.bottomTxtFld.text = (editFlag || editBranch) ? self.chemistData?.pincode : self.chemistData?.off_day
                cell.delegate = self
                
            case 10:
                /*
                var time1 = ""
                var time2 = ""
                
                if let openTime = self.chemistData?.shop_opening_time {
                    
                    time1 = openTime
                }
                if let closeTime = self.chemistData?.shop_closing_time {
                    
                    if !time1.isEmpty {
                        time2 = " to "
                    }
                    time2 = time2 + closeTime
                }*/
                if editFlag || addBranch || editBranch {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "dropDownCell") as! UpdateChemistProfileCell
                    
                    cell.lblTitle.text = "State"
                    cell.setup(id: "state")
                    cell.pickerDataArr = statesArr
                    
                    cell.txtFld.text = self.chemistData?.state
                    
                    return cell
                    
                } else {
                    //cell.bottomTxtFld.text =  self.chemistData?.getShopTiming()
                    let opentime = self.chemistData?.shop_opening_time ?? ""
                    let closetime = self.chemistData?.shop_closing_time ?? ""
                    
                    cell.bottomTxtFld.text = opentime + " to " + closetime
                }
                cell.delegate = self
                
            case 11:
                cell.bottomTxtFld.keyboardType = .default
                cell.bottomTxtFld.accessibilityIdentifier = "city"
                cell.bottomTxtFld.text = self.chemistData?.city
                cell.delegate = self
                
            case 12:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "dropDownCell") as! UpdateChemistProfileCell
                
                cell.lblTitle.text = "Off Day"
                cell.setup(id: "offday")
                cell.txtFld.text = self.chemistData?.off_day
                cell.delegate = self
                
                return cell
                
            case 13:
                let cell = tableView.dequeueReusableCell(withIdentifier: "genderCell") as! UpdateChemistProfileCell
                
                cell.setup(id: "delivery")
                cell.blood_donor.selectedIndex = self.chemistData?.delivery_status == "Yes" ? 0 : 1
                cell.delegate = self
                
                return cell
                
            case 14:
                let cell = tableView.dequeueReusableCell(withIdentifier: "timeCell") as! UpdateChemistProfileCell
                
                cell.txtFld.text = self.chemistData?.shop_opening_time
                cell.txtFld1.text = self.chemistData?.shop_closing_time
                
                cell.setup(id: "openclosetime")
                cell.delegate = self
                
                return cell
                
            case 15:
                let cell = tableView.dequeueReusableCell(withIdentifier: "donorCell") as! UpdateChemistProfileCell
                
                cell.setup(id: "share_contact")
                cell.blood_donor.selectedIndex = self.another_contact ? 1 : 0
                cell.delegate = self
                
                return cell
               
            case 16:
                cell.bottomTxtFld.keyboardType = .default
                cell.bottomTxtFld.placeholder = "Contact Name"
                cell.bottomTxtFld.accessibilityIdentifier = "assistant_name"
                cell.bottomTxtFld.text = self.chemistData?.assistant_name
                cell.delegate = self
                
            case 17:
                cell.bottomTxtFld.keyboardType = .default
                cell.bottomTxtFld.placeholder = "Contact No"
                cell.bottomTxtFld.keyboardType = .numberPad
                cell.bottomTxtFld.accessibilityIdentifier = "alternate_no"
                cell.bottomTxtFld.text = self.chemistData?.alt_contact_no
                cell.delegate = self
                
            default:
                break
            }
            
            return cell
            
        default:
            break
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 3 {
            return addBranch ? 80 : 0
        }
        if indexPath.row == 16 || indexPath.row == 17 {
            return another_contact ? 50 : 0
        }
        if indexPath.row == 10 || indexPath.row == 12 {
            return 90
        }
        if indexPath.row == 15 {
            return 170
        }
        return indexPath.row == 0 ? 140 : 80
    }
    
}

protocol ChemistProfileCellDelegate {
    
    func optionValueChanged(id: String, value:Int)
    func onStateSelected(id: String)
    func onDataSelected(type: String, value: Any)
}

class UpdateChemistProfileCell: BaseCell {
    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var blood_donor: RadioGroup!
    @IBOutlet weak var txtPrefix: RightViewTextField!
    @IBOutlet weak var txtName: RightViewTextField!
    @IBOutlet weak var txtLoc:BottomLineTextField!
    @IBOutlet weak var txtFld: RightViewTextField!
    @IBOutlet weak var txtFld1: RightViewTextField!
    @IBOutlet weak var txtFld2: RightViewTextField!
    @IBOutlet weak var bottomTxtFld: BottomLineTextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    var delegate: ChemistProfileCellDelegate?
    var seldate: String?
    var seldate1: String?
    var selRow: Int = 0
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    lazy var datePicker1: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
    func setup(id: String) {
    
        if id == "delivery" {
            blood_donor.accessibilityIdentifier = id
            blood_donor.titles = ["Yes","No"]
            //blood_donor.selectedIndex = 0
            blood_donor.addTarget(self, action: #selector(optionSelected(radioGroup:)), for: .valueChanged)
        }
        
        if id == "openclosetime" {
            
            txtFld.inputView = datePicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "open")
            
            txtFld1.inputView = datePicker1
            txtFld1.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "close")
        }
        
        if id == "offday" {
            self.selType = "offday"
            cellPicker.accessibilityIdentifier = "weekday"
            txtFld.inputView = cellPicker
            cellPicker.dataSource = self
            cellPicker.delegate = self
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
        
        if id == "state" {
            self.selType = "state"
            cellPicker.accessibilityIdentifier = id
            cellPicker.dataSource = self
            cellPicker.delegate = self
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
        
        if id == "share_contact" {
            blood_donor.accessibilityIdentifier = id
            blood_donor.titles = ["Same as Above","Use another number"]
            //blood_donor.selectedIndex = 0
            blood_donor.addTarget(self, action: #selector(optionSelected(radioGroup:)), for: .valueChanged)
        }
    }
    
    @objc func dateSelected(_ picker: UIDatePicker){
        
        //let dateStr = picker.date.toString(format: df_dd_MM_yyyy)//df.string(from: picker.date)
        
        let dateStr = picker.date.toString(format: "HH:mm")
        
        if picker == datePicker {
            txtFld.text = dateStr
            seldate = dateStr
        }
        if picker == datePicker1 {
            txtFld1.text = dateStr
            seldate1 = dateStr
        }
    }
    
    @objc func optionSelected(radioGroup: RadioGroup){
        
        delegate?.optionValueChanged(id: radioGroup.accessibilityIdentifier!, value: radioGroup.selectedIndex)
    }
    
    @IBAction func textChanged(_ sender: UITextField) {
        delegate?.onDataSelected(type: sender.accessibilityIdentifier!, value: sender.text ?? "")
        print("id = \(sender.accessibilityIdentifier!)")
        print("value = \(sender.text)")
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selRow = row
        print("selType:\(selType)")
        
        switch selType {
            
        case "state":
            if let statesArr = self.pickerDataArr as? [State] {
                let state = statesArr[row]
                txtFld.text = state.state
                //delegate?.onStateSelected(id: state.state!)
            }
        case "offday":
            let day = Days.all[row]
            txtFld.text = day.rawValue
            //delegate?.onDataSelected(type: "offday", value: day.rawValue)
            
        default:
            break
        }
    }
    
    @objc override func doneCellPicker (sender: UIBarButtonItem) {
    
        let id = sender.accessibilityIdentifier
        
        switch id {
        case "open":
            seldate = datePicker.date.toString(format: "HH:mm")
            delegate?.onDataSelected(type: id!, value: seldate ?? "")
            
        case "close":
            seldate1 = datePicker1.date.toString(format: "HH:mm")
            delegate?.onDataSelected(type: id!, value: seldate1 ?? "")
            
        case "state":
            delegate?.onDataSelected(type: id!, value: txtFld.text ?? "")
            
        case "offday":
            delegate?.onDataSelected(type: id!, value: txtFld.text ?? "")
            
        default:
            break
        }
        
        self.endEditing(true)
        
    }
}
