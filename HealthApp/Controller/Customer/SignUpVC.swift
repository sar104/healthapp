//
//  SignUpVC.swift
//  HealthApp
//
//  Created by Apple on 22/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import IHProgressHUD

class SignUpVC: BaseVC {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtMob: UITextField!
    //@IBOutlet weak var txtEmail: UITextField!
       
    let picker = UIPickerView()
    var roleArr: [Role] = []
    var selType: Int = 0
    var termsFlag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //setup()
        getRoles()
        //fetchUser()
        
        //test()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setup()
    }
    
    func fetchUser(){
        let user = self.coreDataManager.fetchLocalUsers()
        print(user)
    }
    
    @IBAction func showTerms(_ sender: UIButton) {
        
        UIApplication.shared.open(URL(string: termsUrl)!, options: [:], completionHandler: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setup(){
        
        txtName.removeBottomBorder()
        txtName.addBottomBorder()
        
        txtType.removeBottomBorder()
        txtType.addBottomBorder()
        
        txtMob.removeBottomBorder()
        txtMob.addBottomBorder()
        
        txtName.setPlaceholder(str1: "*", str2: lblName)
        txtType.setPlaceholder(str1: "*", str2: lblUserType)
        txtMob.setPlaceholder(str1: "*", str2: lblMob)
        //txtEmail.setPlaceholder(str1: "*", str2: lblEmail)
        
        txtName.delegate = self
        txtType.delegate = self
        txtMob.delegate = self
        //txtEmail.delegate = self
        
        txtType.inputView = picker
        txtType.inputAccessoryView = self.addDoneButton(dontTxt: strNext)
        
        txtMob.inputAccessoryView = self.addDoneButton(dontTxt: strDone)
        picker.delegate = self
    }
    
    override func donePicker (sender: UIBarButtonItem) {
        if txtType.isFirstResponder {
            txtMob.becomeFirstResponder()
        }
        else if txtMob.isFirstResponder {
            txtMob.resignFirstResponder()
        }
    }
    
    func getRoles() {
        
        IHProgressHUD.show()
        apiManager.getRoles(params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
            case .success(let roledata):
                print("roledata: \(roledata.data)")
                self.roleArr = roledata.data!
            case .failure(let err):
                print("err: \(err)")
            }
        }
        
    }
    
    func validateForm() -> Bool {
        
        if let name = txtName.text, name.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: err_name)
            return false
        }
        if let mob = txtMob.text, mob.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: err_mob)
            return false
        }
        if let mob = txtMob.text, !mob.isValidContact {
            
            self.alert(strTitle: strErr, strMsg: "Please enter valid mobile number")
            return false
        }
        if selType == 0 {
            
            self.alert(strTitle: strErr, strMsg: err_role)
            return false
        }
        if !termsFlag {
            self.alert(strTitle: strErr, strMsg: err_terms)
            return false
        }
        return true
    }
    
    @IBAction func termsAction(_ sender: UIButton){
        
        sender.isSelected = !sender.isSelected
        termsFlag = !termsFlag
        
    }
    @IBAction func submitAction(_ sender: UIButton){
       
        if validateForm() {
            registerAPI()
        }
    }
    
    func registerAPI(){
        
        
        let roledata = roleArr[selType-1]
        let params = [PARAM_NAME: txtName.text!,
            PARAM_MOB: txtMob.text!,
        PARAM_ROLE: roledata.id!] as [String:Any]
        
        print(params)
        IHProgressHUD.show(withStatus: "Loading")
        apiManager.registerAPI(params: params) { (result) in
                 
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
                print("data: \(data)")
                if data.status {
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                    
                    //UserDefaults.standard.set(1, forKey: "newuser")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now()+1) { 
                        self.navToOTP(mob: self.txtMob.text!)
                    }
                } else {
                    self.alert(strTitle: strErr, strMsg: data.message!)
                }
            case .failure(let err):
                print("err: \(err)")
                self.alert(strTitle: strErr, strMsg: err.localizedDescription)
            }
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SignUpVC: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return roleArr.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if row == 0 {
            return "Select"
        }
        let roledata = roleArr[row-1]
        
        return roledata.role
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selType = row
        
    }
    
    
}
extension SignUpVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtName {
            txtType.becomeFirstResponder()
        }
        
        //textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtType {
            let roledata = roleArr[selType-1]
            let str = "*\(lblUserType) \(roledata.role!)"
            print(str)
            var newstr = NSMutableAttributedString(string: str)
            
            //newstr = str.setStringColors(str1: lblUserType, str2: roledata.role!, color1: UIColor.black, color2: UIColor.red)
            let range = (str as NSString).range(of: lblUserType)
            
            
            newstr = str.setStringColors(str1: roledata.role!, str2: "*", color1: UIColor.red, color2: UIColor.red)
            newstr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: range)
            
            txtType.attributedText = newstr
        }
        textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
       replacementString string: String) -> Bool {
        
           if textField == txtName {
               let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
               let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
               let typedCharacterSet = CharacterSet(charactersIn: string)
               let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
               return alphabet
           }
           return true
       }
    
}
