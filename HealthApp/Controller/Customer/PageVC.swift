//
//  PageVC.swift
//  HealthApp
//
//  Created by Apple on 21/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

enum Pages: CaseIterable {
    
    case zero
    case one
    case two
    case three
    
    var index: Int {
        
        switch self {
        case .zero:
            return 0
        case .one:
            return 1
        case .two:
            return 2
        case .three:
            return 3
        }
    }
    
    var title: String {
        
        switch self {
        case .zero:
            return "HEALTH RECORDS"
        case .one:
            return "ORDER MEDICINES"
        case .two:
            return "DOCTOR CONNECT"
        case .three:
            return "HEALTH & WELNESS"
        }
    }
    
    var content: String {
        switch self {
        case .zero:
            return "Manage all the health records in one place. Access your and your family's reports & prescriptions anytime anywhere."
        case .one:
            return "Get medicines from nearby chemists or order for your loved ones from our partner network. If you are a chemist, register and manage orders."
        case .two:
            return "Register as a doctor or patient, manage your appointments & reports hassle-free."
        case .three:
            return "Get latest on nutrition, physical & mental health and connect with various service providers."
        }
    }
    
    var image: String {
        switch self {
        case .zero:
            return "page1"
        case .one:
            return "page2"
        case .two:
            return "page3"
        case .three:
            return "page4"
        }
    }
}

protocol PageVCDelegate {
    func onSkip()
    func onNext(index: Int)
}

class PageVC: UIViewController {

    var page: Pages!
    var bgcolor = [UIColor.red, .orange, .yellow]
    var delegate:PageVCDelegate?
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //self.view.backgroundColor = bgcolor[page.index]
        imgView.image = UIImage(named: page.image)
        lblTitle.text = page.title
        lblContent.text = page.content
        pageControl.currentPage = page.index
    }
    
    @IBAction func skipAction(){
        delegate?.onSkip()
    }
    
    @IBAction func nextAction(){
        delegate?.onNext(index: page.index)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
