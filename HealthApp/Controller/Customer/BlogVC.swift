//
//  BlogVC.swift
//  HealthApp
//
//  Created by Apple on 23/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD
import WebKit

class BlogVC: BaseVC {

    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var lblHead: UILabel!
    
    
    var blogArr: [BannerData] = []
    var showFlag: Bool = false
    var selRow: Int = 0
    var selBanner: BannerData?
    var newsArr: [NewsData] = []
    var newsType: String?
    var showNews: Bool = false
    var webCellHeight: CGFloat = 200
    var finishLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if !showFlag && !showNews {
           
            getBlogs()
        }
        
        if showNews {
            
            switch newsType {
            case "fitness":
                lblHead.text = "FITNESS ARTICLES"
                
            case "nutri":
                lblHead.text = "NUTRITION ARTICLES"
                
            case "mental":
                lblHead.text = "NEWS ARTICLES"
                
            default:
                break
            }
        }
    }
    
    
    
    func getBlogs() {
        
        IHProgressHUD.show()
        
        apiManager.getUserFeed(params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                
            case .success(let data):
                
                self.blogArr = data
                
                DispatchQueue.main.async {
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print(err)
            }
        }
    }

    @IBAction func addAction(_ sender: UIButton) {
        
        let vc = getVC(with: "AddBlogVC", sb: IDMain) as! AddBlogVC
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        
        if showFlag {
            
            let content = selBanner?.title ?? "" + "\nTo read full aritlce, Please install Ctrlh App : https://www.ctrlh.in"
            let url = URL(string: "https://www.ctrlh.in")
            let items = [content] as [Any]//["This app is my favorite"]
            let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
            
            present(ac, animated: true)
        } else {
            let data = blogArr[sender.tag]
            let content = data.title ?? "" + "\nTo read full aritlce, Please install Ctrlh App : https://www.ctrlh.in"
            let url = URL(string: "https://www.ctrlh.in")
            let items = [content] as [Any]//["This app is my favorite"]
            let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
            
            present(ac, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BlogVC: UITableViewDataSource, UITableViewDelegate, WKNavigationDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return showFlag ? 2 : 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if showNews {
                return newsArr.count
            }
            return showFlag ? 1 : blogArr.count
        }
        return showFlag ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            if showNews {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "blogCell") as! BlogCell
                
                let data = newsArr[indexPath.row]
                
                cell.btnShare.isHidden = true
                
                cell.populateNewsData(data: data)
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "blogCell") as! BlogCell
                
                let data = showFlag ?  selBanner! : blogArr[indexPath.row]
                
                cell.btnShare.tag = indexPath.row
                
                cell.btnShare.addTarget(self, action: #selector(shareAction(_:)), for: .touchUpInside)
                cell.populateData(data: data)
                
                return cell
            }
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "webCell") as! BlogCell
            let data = selBanner!
            //cell.lblDesc.attributedText = data.description?.htmlAttributedString()
            if !finishLoading {
                cell.actView.startAnimating()
                
            } else {
                cell.actView.stopAnimating()
            }
            cell.actView.hidesWhenStopped = true
            cell.webView.loadHTMLString(data.description ?? "", baseURL: nil)
            cell.webView.navigationDelegate = self
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if showFlag {
            //return indexPath.row == selRow ? UITableView.automaticDimension : 0
            if indexPath.section == 1 && indexPath.row == 0 {
                return webCellHeight
            }
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
        self.selRow = indexPath.row
        self.showFlag = true
        self.tblData.reloadData()
 */
        if !showFlag && !showNews {
            let data = blogArr[indexPath.row]
            let vc = self.getVC(with: "BlogVC", sb: IDMain) as! BlogVC
            vc.selRow = indexPath.row
            vc.showFlag = true
            vc.selBanner = data
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
        if showNews {
            
            let data = newsArr[indexPath.row]
            
            if let strurl = data.url, let url = URL(string: strurl) {
                
                
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        finishLoading = true
        webCellHeight = webView.scrollView.contentSize.height
        self.tblData.reloadRows(at: [IndexPath(row: 0, section: 1)], with: .none)
    }
}

class BlogCell: BaseCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var imgBlog: UIImageView!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var actView: UIActivityIndicatorView!
    
    func populateData(data: BannerData) {
        
        lblTitle.text = data.title
        lblDesc.text = data.title
        lblDate.text = data.created_at
        let strurl = bannerURL + data.image_video!
        imgBlog.kf.setImage(with: URL(string: strurl))
    }
    
    func populateNewsData(data: NewsData) {
        
        lblTitle.text = data.name
        lblDesc.text = data.description
        if let strurl = data.imgurl, let url = URL(string: strurl) {
            imgBlog.kf.setImage(with: url)
        }
        lblDate.isHidden = true
    }
    
    
    
    @IBAction func shareAction(_ sender: UIButton) {
        
        
    }
}
extension String {
    func htmlAttributedString() -> NSAttributedString? {
        guard let data = self.data(using: .utf8) else {
            return nil
        }

        return try? NSAttributedString(
            data: data,
            options: [.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil
        )
    }
}
