//
//  AddReportVC.swift
//  HealthApp
//
//  Created by Apple on 07/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD
import Alamofire
import SwiftyJSON
import MobileCoreServices

class AddReportVC: BaseVC, AddReportCellDelegate {
    
    var recommBy: String = ""
    var sharingType: String = ""
    var dateOfReport: String = ""
    var doctorName: String = ""
    var reportImgArr:[Data] = []
    var reportPDFArr:[Data] = []
    var imageNameArr:[String] = []
    var upImageNameArr:[String] = []
    var user: User?
    var recordType: String = kMine
    var familyMembersArr: [FamilyMemberData] = []
    var selFamilyMember: FamilyMemberData?
    var editFlag: Bool = false
    var viewFlag: Bool = false
    var medicalHistoryData: MedicalHistoryData?
    var reportImgFlag: Bool = false
    let imgPicker = UIImagePickerController()
    //var pdfFlag: Bool = false
    //var pdfData: Data?
    
    @IBOutlet weak var tblData: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //print("recordType = \(recordType)")
        // Do any additional setup after loading the view.
        let users = self.fetchUser()
        user = users[0]
        
        if self.recordType == kFamily {
            getFamilyMembers()
        }
        
        if editFlag || viewFlag {
            populateReportData()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(segmentChanged(_:)), name: NSNotification.Name("SegmentDidChangeNotification"), object: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tblData.reloadData()
    }
    
    @objc func segmentChanged(_ notification: NSNotification){
        
        if let index = notification.object as? Int {
            print("index = \(index)")
            self.recordType = self.arrRecordType[index]
        }
        if self.recordType == kFamily {
            if familyMembersArr.count == 0 {
                getFamilyMembers()
            }
        }
        self.tblData.reloadData()
    }
    
    func populateReportData() {
        if let drname = medicalHistoryData?.doctor_name, !drname.isEmpty {
            recommBy = "Doctor"
            doctorName = drname
        } else {
            recommBy = "Myself"
        }
        dateOfReport = medicalHistoryData?.illness_date ?? ""
        sharingType = SHARINGTYPE_ARRAY[medicalHistoryData!.sharing_type!]
        
        if let images = medicalHistoryData?.images, images.count > 0 {
            
            for img in images {
                guard let imgname = img.image else {return}
                self.imageNameArr.append(imgname)
            }
        }
    }
    
    func getFamilyMembers() {
        
        IHProgressHUD.show()
        
        let params = ["user_id": user?.id] as [String: Any]
        apiManager.getFamilyMember(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    
                    self.familyMembersArr = memberData
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func uploadReports(){
        
        IHProgressHUD.show()
        apiManager.uploadImgArray(image: reportImgArr, name: "image", api: MedicalHistoryImgAPI) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let imgdata):
                    
                    if imgdata.status {
                        if let arr = imgdata.data {
                            //self.imageNameArr = arr
                            self.imageNameArr.append(contentsOf: arr)
                            if self.editFlag {
                                self.upImageNameArr.append(contentsOf: arr)
                            }
                            print("images: \(arr)")
                            //self.addMedicalHistory()
                            if self.reportPDFArr.count > 0 {
                                self.uploadPDFReport()
                            } else {
                                if self.editFlag {
                                    self.updateMedicalHistory()
                                } else {
                                    self.addMedicalHistory()
                                }
                            }
                        }
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func uploadPDFReport(){
        
        IHProgressHUD.show()
        apiManager.uploadPDFArray(image: reportPDFArr, name: "image", api: MedicalHistoryImgAPI) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let imgdata):
                    
                    if imgdata.status {
                        if let arr = imgdata.data {
                            self.imageNameArr.append(contentsOf: arr)
                            if self.editFlag {
                                self.upImageNameArr.append(contentsOf: arr)
                            }
                            print("images: \(arr)")
                            //self.addMedicalHistory()
                            if !self.reportImgFlag {
                                if self.editFlag {
                                    self.updateMedicalHistory()
                                } else {
                                    self.addMedicalHistory()
                                }
                            }
                        }
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    
    func addMedicalHistory(){
        
        IHProgressHUD.show()
        
        do {
            let sharing = SHARINGTYPE_ARRAY.firstIndex(of: sharingType)
            
            let userid = user!.id
            var childId = userid
            if self.recordType == kFamily {
                childId = Int32(Int(selFamilyMember!.id!))
            }
            let jsonImgs: Data = try JSONSerialization.data(withJSONObject: imageNameArr, options: [])
            //JSON(imageNameArr)
            
            let imgArrStr = String(data:jsonImgs,encoding: .utf8)
            
            let params: [String:Any] = [PARAM_USERID: userid,
                                          PARAM_CHILDID:  childId,
                PARAM_SHARINGTYPE: sharing!,
                PARAM_DOCTORNAME: doctorName.isEmpty ? strNull : doctorName,
            PARAM_ILLNESSTYPE: strOtherReport,
            PARAM_DESCRIPTION: strNull,
            PARAM_ILLNESSDATE: dateOfReport,
            PARAM_IMAGES: imgArrStr!]
            
            
            print("params: \(params)")
            
        let AppURL = baseURL + AddMedicalHistoryAPI
       
        let postData = try JSONSerialization.data(withJSONObject: params, options: [])
        var request = URLRequest(url: URL(string:AppURL)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = ["Content-Type":"application/json"]
        request.httpBody = postData as Data
            
            apiManager.addMedicalHistoryAPI(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.navReportsList()
                        }
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
        /*
            AF.request(request).responseString { (response) in
                print("res: \(response)")
            }
        */
            /*
            let session = URLSession.shared
                               let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                                   
                                  IHProgressHUD.dismiss()
                                
                                   if (error != nil) {
                                       print("err: \(error)")
                                   } else {
                                       let httpResponse = response as? HTTPURLResponse
                                       print("response:\(httpResponse)")
                                    let strdata = String(data:data!, encoding: .utf8)
                                       print("strdata: \(strdata)")
                                       var json: JSON = JSON(data!)
                                    print("json: \(json)")
                                }
                                })
            dataTask.resume()
            
            */
        } catch let err {
            print(err)
        }
        /*
        apiManager.addMedicalHistoryAPI(params: JSON(params)) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let data):
                    
                    if data.status {
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                    } else {
                        self.alert(strTitle: strErr, strMsg: data.message!)
                    }
                case .failure(let err):
                print("err: \(err)")
                self.alert(strTitle: strErr, strMsg: err.localizedDescription)
            }
        }*/
    }
    
    func updateMedicalHistory(){
        
        IHProgressHUD.show()
        
        do {
            let sharing = SHARINGTYPE_ARRAY.firstIndex(of: sharingType)
            
            let userid = user!.id
            var childId = userid
            if self.recordType == kFamily {
                childId = Int32(Int(selFamilyMember!.id!))
            }
            let jsonImgs: Data = try JSONSerialization.data(withJSONObject: upImageNameArr, options: [])
            //JSON(imageNameArr)
            
            let imgArrStr = String(data:jsonImgs,encoding: .utf8)
            
            let params: [String:Any] = [
                PARAM_ID: medicalHistoryData?.id,
                PARAM_USERID: userid,
                                          PARAM_CHILDID:  childId,
                PARAM_SHARINGTYPE: sharing!,
                PARAM_DOCTORNAME: doctorName.isEmpty ? strNull : doctorName,
            PARAM_ILLNESSTYPE: strOtherReport,
            PARAM_DESCRIPTION: strNull,
            PARAM_ILLNESSDATE: dateOfReport,
            PARAM_IMAGES: imgArrStr!]
            
            
            print("params: \(params)")
            
        let AppURL = baseURL + AddMedicalHistoryAPI
       
        let postData = try JSONSerialization.data(withJSONObject: params, options: [])
        var request = URLRequest(url: URL(string:AppURL)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = ["Content-Type":"application/json"]
        request.httpBody = postData as Data
            
            apiManager.updateMedicalHistory(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        if data.status {
                            self.alert(strTitle: strSuccess, strMsg: "Report updated successfully")
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.navReportsList()
                        }
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
        
        } catch let err {
            print(err)
        }
       
    }
    
    func navReportsList() {
        
        var dashboardVC: DashboardVC
        if let vc = (self.tabBarController?.viewControllers?.first as? UINavigationController)?.viewControllers.first as? DashboardVC {
            
            dashboardVC = vc
        } else {
            //dashboardVC = (self.navigationController?.viewControllers.first) as! DashboardVC
            
            dashboardVC = self.getVC(with: "DashboardVC", sb: "Main") as! DashboardVC
        }
            //
        let prescVC = self.getVC(with: "ReportsVC", sb: "Main") as! ReportsVC
        
        if self.recordType == kFamily {
            prescVC.type = "family"
        } else {
            prescVC.type = "self"
        }
               
        let vcArray = [dashboardVC, prescVC]
        self.navigationController?.setViewControllers(vcArray, animated: false)
    }
    
    func onValueSelected(type: String, value: String) {
        
        var row = 0
        
        switch type {
        case "recommBy":
            recommBy = value
            row = 2
        case "sharingType":
            sharingType = value
            row = 5
        case "date":
            dateOfReport = value
            row = 4
        case "drname":
            doctorName = value
            row = 3
        default:
            break
        }
        let indexpath = IndexPath(row: row, section: 0)
        self.tblData.reloadRows(at: [indexpath], with: .none)
    }
    
    func onDataSelected(type: String, value: Any) {
        
        if let data = value as? FamilyMemberData {
            
            selFamilyMember = data
            self.tblData.reloadData()
        }
    }
    
    func onSaveClicked(){
        
        if self.validate() {
            if reportImgArr.count > 0 {
                self.reportImgFlag = true
                self.uploadReports()
            }
            else if reportPDFArr.count > 0 {
                self.uploadPDFReport()
            }
            else {
                if editFlag {
                    self.updateMedicalHistory()
                } else {
                    self.addMedicalHistory()
                }
            }
        }
    }
    
    func validate() -> Bool{
        
        
        if self.recordType == kFamily && selFamilyMember == nil {
            
            self.alert(strTitle: strErr, strMsg: "Please select family member")
            return false
        }
        if recommBy.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select recommended by")
            return false
        }
        if dateOfReport.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select date")
            return false
        }
        if sharingType.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select sharing type")
            return false
        }
        if reportImgArr.count == 0 && imageNameArr.count == 0 && reportPDFArr.count == 0 {
            self.alert(strTitle: strErr, strMsg: "Please upload report image")
            return false
        }
        
        return true
    }
    
    func onAddClicked(){
        
        self.showImgPicker()
    }
    
    func onImgRemoved(id: Int) {
        
        if id < self.reportImgArr.count {
            self.reportImgArr.remove(at: id)
        }
        if id < imageNameArr.count {
            imageNameArr.remove(at: id)
        }
        self.tblData.reloadData()
    }

    @IBAction func editAction(_ sender: UIButton) {
        
        let vc = self.getVC(with: "UpdateReportVC", sb: IDMain) as! AddReportVC
        vc.editFlag = true
        vc.medicalHistoryData = medicalHistoryData
        vc.recordType = recordType
        //
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        
        let vc = self.getVC(with: "ShareEHRVC", sb: IDMain) as! ShareEHRVC
        vc.EHR_Id = medicalHistoryData?.id
        vc.type = "Report"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddReportVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.recordType == kFamily {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.recordType == kFamily {
            if familyMembersArr.count > 0 {
                if section == 0 {
                    return 8
                }
                return 0
            } else {
                if section == 0 {
                    return 0
                }
                return 1
            }
        }
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = AddReportCell()
        if indexPath.section == 0 {
        switch indexPath.row {
            
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! AddReportCell
            cell.delegate = self
            cell.setup(id: "familyMember", placeHolder: "Family Member")
            cell.pickerDataArr = self.familyMembersArr
            
        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: "labelCell") as! AddReportCell
            cell.delegate = self
            cell.setup(id: "relation", placeHolder: "")
            cell.lblRelation.text = self.selFamilyMember?.relation ?? ""
            
        case 2:
            if viewFlag {
                cell = tableView.dequeueReusableCell(withIdentifier: "btnDetailCell") as! AddReportCell
                
                if let lblhead = cell.contentView.viewWithTag(1) as? UILabel {
                 
                    lblhead.text = "Recommended By"
                }
                if let lblname = cell.contentView.viewWithTag(2) as? UILabel {
                    lblname.text = recommBy ?? ""
                }
                if let btnEdit = cell.contentView.viewWithTag(3) as? UIButton {
                    btnEdit.addTarget(self, action: #selector(editAction), for: .touchUpInside)
                }
                if let btnShare = cell.contentView.viewWithTag(4) as? UIButton {
                    btnShare.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
                }
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! AddReportCell
                cell.delegate = self
                cell.setup(id: "recommBy", placeHolder: "Recommended By")
                cell.txtFld.text = recommBy
            }
            
        case 3:
            if viewFlag {
                //btnDetailCell
                cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! AddReportCell
                
                if let lblhead = cell.contentView.viewWithTag(1) as? UILabel {
                    
                    lblhead.text = "Doctor Name"
                }
                if let lblname = cell.contentView.viewWithTag(2) as? UILabel {
                    lblname.text = (doctorName != "null") ? doctorName : "-"
                }
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! AddReportCell
                cell.delegate = self
                cell.setup(id: "drname", placeHolder: "doctor name")
                cell.txtFld.text = (doctorName != "null") ? doctorName : "-"
            }
            
        case 4:
            if viewFlag {
                //btnDetailCell
                cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! AddReportCell
                
                if let lblhead = cell.contentView.viewWithTag(1) as? UILabel {
                    
                    lblhead.text = "Date of Report"
                }
                if let lblname = cell.contentView.viewWithTag(2) as? UILabel {
                    lblname.text = dateOfReport
                }
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "dateCell") as! AddReportCell
                cell.delegate = self
                cell.setup(id: "date", placeHolder: "Date of Report")
                cell.txtFld.text = dateOfReport
            }
            
        case 5:
            if viewFlag {
                //btnDetailCell
                cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! AddReportCell
                
                if let lblhead = cell.contentView.viewWithTag(1) as? UILabel {
                    
                    lblhead.text = "Sharing Type"
                }
                if let lblname = cell.contentView.viewWithTag(2) as? UILabel {
                    lblname.text = sharingType
                }
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! AddReportCell
                cell.delegate = self
                cell.setup(id: "sharingType", placeHolder: "Sharing Type")
                cell.txtFld.text = sharingType
            }
            
        case 6:
            if self.reportImgArr.count > 0 {
                cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! AddReportCell
                
                cell.imgArr = self.reportImgArr
                cell.viewFlag = self.viewFlag
                cell.collView.reloadData()
                
            } else if self.reportPDFArr.count > 0 {
                cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! AddReportCell
                
                cell.pdfArr = self.reportPDFArr
                cell.viewFlag = self.viewFlag
                cell.collView.reloadData()
                
            } else if imageNameArr.count > 0 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! AddReportCell
                cell.imageNameArr = self.imageNameArr
                cell.viewFlag = self.viewFlag
                cell.collView.reloadData()
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "upCell") as! AddReportCell
            }
            
            cell.delegate = self
            
        case 7:
            cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! AddReportCell
            cell.delegate = self
        
        default:
            break
        }
        }
        if indexPath.section == 1 {
            cell = tableView.dequeueReusableCell(withIdentifier: "addFamilyCell") as! AddReportCell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.selectionStyle = .none
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 || indexPath.row == 1 {
                if recordType == kMine {
                    return 0
                }
            }
            if indexPath.row == 5 {
                return recordType == kMine ? 60 : 0
            }
            if indexPath.row == 3 && recommBy != "Doctor" {
                return 0
            }
            if indexPath.row == 6 {
                return 120
            }
            if indexPath.row == 7 {
                return viewFlag ? 0 : 80
            }
            return viewFlag ? 90 : 60
        }
        if self.recordType == kFamily {
            if indexPath.section == 1 && indexPath.row == 0 {
                return 500
            }
        }
        return 0
    }
        

}
extension AddReportVC: UIDocumentPickerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        //self.pdfFlag = true
        do {
            let pdfData = try Data(contentsOf: myURL)
            reportPDFArr.append(pdfData)
            
        } catch let err {
            print("pdf err: \(err.localizedDescription)")
        }
        print("import result : \(myURL)")
        self.tblData.reloadData()
    }


    public func documentMenu(_ documentMenu:UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }


    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}
extension AddReportVC: UIImagePickerControllerDelegate {
    
    func showImgPicker() {
        print("Tap On Image")
        imgPicker.delegate = self
    
        let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {
            action in
            
            self.imgPicker.allowsEditing = true
            self.imgPicker.sourceType = .camera
            self.imgPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            self.present(self.imgPicker, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: {
            action in
            
            self.imgPicker.allowsEditing = true
            self.imgPicker.sourceType = .photoLibrary
            self.imgPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(self.imgPicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "PDF", style: .default, handler: {
            action in
            let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
            importMenu.delegate = self
            importMenu.modalPresentationStyle = .formSheet
            self.present(importMenu, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
    guard let image = info[.editedImage] as? UIImage else {
        return
    }
        if let imgdata = image.jpegData(compressionQuality: 0.5){
            self.reportImgArr.append(imgdata)
        }
        self.tblData.reloadData()
        dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

protocol AddReportCellDelegate {
    func onValueSelected(type: String, value: String)
    func onDataSelected(type: String, value: Any)
    func onSaveClicked()
    func onAddClicked()
    func onImgRemoved(id: Int)
}

class AddReportCell: BaseCell {
    
    @IBOutlet weak var txtFld: RightViewTextField!
    var delegate: AddReportCellDelegate?
    @IBOutlet weak var collView: UICollectionView!
    var imgArr:[Data] = []
    var pdfArr:[Data] = []
    var selRow: Int = 0
    var seldate: String?
    var imageNameArr:[String] = []
    var viewFlag: Bool = false
    
    @IBOutlet weak var lblRelation: UILabel!
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.maximumDate = Date()
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
    
    
    func setup(id: String, placeHolder: String){
        
        if id == "familyMember" {
            
            self.selType = id
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            txtFld.placeholder = placeHolder
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
        }
        
        if id == "recommBy" {
            self.pickerDataArr = ["Myself","Doctor"]
            self.selType = id
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            
            txtFld.placeholder = placeHolder;
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
        }
        
        if id == "sharingType" {
            self.pickerDataArr = SHARINGTYPE_ARRAY
            self.selType = id
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            
            txtFld.placeholder = placeHolder;
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
        }
        if id == "date" {
            
            txtFld.inputView = datePicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
            txtFld.placeholder = placeHolder;
        }
        if id == "drname" {
            
            txtFld.rightImage = nil
            txtFld.addBottomBorder()
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            txtFld.placeholder = placeHolder;
        }
    }
 
    @objc func dateSelected(_ picker: UIDatePicker){
        
        let dateStr = picker.date.toString(format: df_dd_MM_yyyy)//df.string(from: picker.date)
        
        txtFld.text = dateStr
        
        let date = picker.date.toString(format: df_dd_MM_yyyy)
        //delegate?.onValueSelected(type: "date", value: date)
        seldate = date
        
    }
    
    @IBAction func addAction(_ sender: UIButton){
        
        delegate?.onAddClicked()
    }
    
    @IBAction func saveAction(_ sender: UIButton){
        
        delegate?.onSaveClicked()
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        print("selType:\(selType)")
        selRow = row
        
        switch selType {
            case "recommBy":
                txtFld.text = pickerDataArr[row] as? String
                
            case "sharingType":
                txtFld.text = pickerDataArr[row] as? String
            
            case "familyMember":
                let data = pickerDataArr[row] as? FamilyMemberData
                txtFld.text = data?.name
            
        default:
            break
        }
        
        //delegate?.onValueSelected(type: selType, value: txtFld.text ?? "")
    }
    
    @objc override func doneCellPicker (sender: UIBarButtonItem) {
        
        let id = sender.accessibilityIdentifier
        
        if id == "recommBy" {
            let value = (pickerDataArr[selRow] as? String)!
            txtFld.text = value
            delegate?.onValueSelected(type: id!, value: value)
        }
        if id == "sharingType" {
            let value = (pickerDataArr[selRow] as? String)!
            txtFld.text = value
            delegate?.onValueSelected(type: id!, value: value)
        }
        if id == "date" {
            seldate = datePicker.date.toString(format: df_dd_MM_yyyy)
            txtFld.text = seldate!
            delegate?.onValueSelected(type: id!, value: seldate!)
        }
        if id == "familyMember" {
            let data = pickerDataArr[selRow] as? FamilyMemberData
            txtFld.text = data?.name
            delegate?.onDataSelected(type: id!, value: data!)
        }
        if id == "drname" {
            
            delegate?.onValueSelected(type: id!, value: txtFld.text!)
        }
        
        txtFld.resignFirstResponder()
    }
}
extension AddReportCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ReportCollCellDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imgArr.count + pdfArr.count + imageNameArr.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = ReportCollCell()
        if indexPath.item == imgArr.count + pdfArr.count + imageNameArr.count {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addCell", for: indexPath) as! ReportCollCell
        } else {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath) as! ReportCollCell
            if imgArr.count > 0 && indexPath.item < imgArr.count {
                cell.imgview.image = UIImage(data: imgArr[indexPath.item])
            }
            else if pdfArr.count > 0 && (indexPath.item == imgArr.count) && (indexPath.item - imgArr.count)  < pdfArr.count + 1 {
                cell.imgview.image = UIImage(named: "pdf")
            }
            else if imageNameArr.count > 0 {
                let cnt = indexPath.item - imgArr.count
                
                if imageNameArr[cnt].contains("pdf") {
                    cell.imgview.image = UIImage(named: "pdf")
                } else {
                    let imgURL = reportImageURL + imageNameArr[cnt]
                    cell.imgview.kf.setImage(with: URL(string: imgURL))
                }
            }
            cell.btnDelete.tag = indexPath.item
            if viewFlag {
                cell.btnDelete.isHidden = true
            }
        }
        
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
      
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 90, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("didSelectItemAt \(indexPath.item)")
        if indexPath.item == imgArr.count {
            
            delegate?.onAddClicked()
        }
    }
    
    func onAddClicked(){
        delegate?.onAddClicked()
    }
    
    func onDeleteClicked(id: Int){
        
        if id < imgArr.count {
            imgArr.remove(at: id)
        }
        if id < imageNameArr.count {
            imageNameArr.remove(at: id)
        }
        delegate?.onImgRemoved(id: id)
        //self.collView.reloadData()
    }
    
}

protocol ReportCollCellDelegate {
    
    func onAddClicked()
    func onDeleteClicked(id: Int)
}
class ReportCollCell: UICollectionViewCell {
    
    @IBOutlet weak var imgview: UIImageView!
    @IBOutlet weak var btnDelete:UIButton!
    var delegate: ReportCollCellDelegate?
    
    @IBAction func addAction(_ sender: UIButton){
        delegate?.onAddClicked()
    }
    
    @IBAction func deleteAction(_ sender: UIButton){
        delegate?.onDeleteClicked(id: sender.tag)
    }
}
