//
//  OrdersVC.swift
//  HealthApp
//
//  Created by Apple on 02/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class OrdersVC: BaseVC, OrdersCellDelegate {

    @IBOutlet weak var tblData: UITableView!
    
    var arrOrders: [CustOrderData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getOrderList() 
    }
    

    func getOrderList() {
        
        let params = ["user_id": loggedUser?.id]
        
        IHProgressHUD.show()
        apiManager.getOrders(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    self.arrOrders.removeAll()
                    self.arrOrders = data
                    
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func onSaveClicked(id: Int) {
        //OrderDetailsVC
        
        let data = arrOrders[id]
        let vc = self.getVC(with: "OrderDetailsVC", sb: IDMain) as! OrderDetailsVC
        vc.orderData = data
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func addAction(_ sender: UIButton) {
        
        let vc = self.getVC(with: "ParallaxVC", sb: IDMain) as! ParallaxVC
        vc.type = "chemist"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    /*
     {
         address = "-";
         "auto_generated_date" = "2020-08-20 19:01:47";
         chemist = "Mr. Shsh";
         "chemist_id" = 403;
         "chemist_shop_name" = shdhd;
         "customer_address" = test;
         "delivery_type" = Delivery;
         description = "-";
         "doctor_name" = pqrs;
         "estimated_cost" = "<null>";
         "estimated_date" = "21-08-2020";
         "flat_house_no" = 401;
         id = 352;
         "mobile_no" = 9850059684;
         "order_details" =             (
                             {
                 "available_status" = "<null>";
                 "medicine_name" = "{\"medicine_name\":\"Paramol 500mg Tablet\",\"suggested_name\":\"\",\"medicine_type\":\"PM\"}";
                 "order_id" = 352;
                 quantity = 1;
                 unit = strip;
             },
                             {
                 "available_status" = "<null>";
                 "medicine_name" = "{\"medicine_name\":\"Paramet 5 mg\\/500 mg Tablet\",\"suggested_name\":\"\",\"medicine_type\":\"PM\"}";
                 "order_id" = 352;
                 quantity = 4;
                 unit = strip;
             },
                             {
                 "available_status" = "<null>";
                 "medicine_name" = "{\"medicine_name\":\"ALERID COLD\",\"suggested_name\":\"\",\"medicine_type\":\"NPM\"}";
                 "order_id" = 352;
                 quantity = 1;
                 unit = Tablet;
             }
         );
         pincode = 422101;
         "prescription_image" = "JOvXIlbM0kzESVDfZX4Z.jpeg";
         "prescription_type" = Prescribed;
         "recipient_name" = "{\"rec_id\":\"401\",\"rec_name\":\"Miss Sarika\",\"rec_mobile\":\"9850059684\"}";
         "reject_reason" = "<null>";
         slot = "09:00 AM To 12:00 PM";
         status = Pending;
         "user_name" = "Miss Sarika";
     }
     **/
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OrdersVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
        return arrOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! OrdersCell
        
        let data = arrOrders[indexPath.row]
        
        cell.row = indexPath.row
        cell.populateData(data: data)
        
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 450
    }
    
}

protocol OrdersCellDelegate {
    
    func onSaveClicked(id: Int)
}

class OrdersCell: BaseCell {
    
    @IBOutlet weak var bgView: UIView!
    
    var delegate: OrdersCellDelegate?
    var row = 0
    
    func populateData(data: CustOrderData) {
        
        let lbl1 = self.bgView.viewWithTag(1) as? UILabel
        lbl1?.text = data.chemist_shop_name
        
        let lbl2 = self.bgView.viewWithTag(2) as? UILabel
        lbl2?.text = data.chemist
        
        let lbl3 = self.bgView.viewWithTag(3) as? UILabel
        lbl3?.text = String(data.id!)
        
        let lbl4 = self.bgView.viewWithTag(4) as? UILabel
        lbl4?.text = data.auto_generated_date
        
        let lbl5 = self.bgView.viewWithTag(5) as? UILabel
        lbl5?.text = "Doctor Name"
        
        let lbl6 = self.bgView.viewWithTag(6) as? UILabel
        lbl6?.text = data.doctor_name
        
        let lbl7 = self.bgView.viewWithTag(7) as? UILabel
        lbl7?.text = "Requested On"
        
        let lbl8 = self.bgView.viewWithTag(8) as? UILabel
        lbl8?.text = data.estimated_date
        
        let lbl9 = self.bgView.viewWithTag(9) as? UILabel
        lbl9?.text = "Time"
        
        let lbl10 = self.bgView.viewWithTag(10) as? UILabel
        lbl10?.text = data.slot
        
        let lbl11 = self.bgView.viewWithTag(11) as? UILabel
        lbl11?.text = "Order Type"
        
        let lbl12 = self.bgView.viewWithTag(12) as? UILabel
        lbl12?.text = data.delivery_type
        
        let lbl13 = self.bgView.viewWithTag(13) as? UILabel
        lbl13?.text = "Order Status"
        
        let lbl14 = self.bgView.viewWithTag(14) as? UILabel
        if data.delivery_type == "Pickup" {
            if data.status == "Delivery Pending" {
                lbl14?.text = "Pickup Pending"//data.status
            } else if data.status == "Delivered" {
                lbl14?.text = "Picked Up"
            } else {
                lbl14?.text = data.status
            }
        }
        if data.delivery_type == "Delivery" {
            if data.status == "Delivery Pending" {
                lbl14?.text = "Delivery Pending"//data.status
            }  else {
                lbl14?.text = data.status
            }
        }
        
        if data.status == strPending {
            bgView.backgroundColor = BgPending
        }
        if data.status == strApprovalPending {
            bgView.backgroundColor = BgApprovalPending
        }
        if data.status == strCancelled || data.status == strRejected {
            bgView.backgroundColor = BgCancelled
        }
        if data.status == strDeliveryPending {
            bgView.backgroundColor = BgDelPending
        }
        if data.status == strDelivered {
            bgView.backgroundColor = BgDelivered
        }
        
        let lbl15 = self.bgView.viewWithTag(15) as? UILabel
        lbl15?.text = "Delivery Address"
        
        let lbl16 = self.bgView.viewWithTag(16) as? UILabel
        var address = data.customer_address
        /*
        if let city = data.city {
            address = address + ", " +city
        }
        if let state = data.state {
            address = address + ", " +state
        }*/
        if let pin = data.pincode {
            address = (address ?? "") + ", " + String(pin)
        }
        lbl16?.text = address
        
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        delegate?.onSaveClicked(id: row)
    }
}
