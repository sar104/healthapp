//
//  ProfileVC.swift
//  HealthApp
//
//  Created by Apple on 22/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class ProfileVC: BaseVC {

    @IBOutlet weak var tblData: UITableView!
    var user: User?
    var familyMembersArr: [FamilyMemberData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let tabbarvc = self.tabBarController as? HomeVC {
            
            tabbarvc.navigationController?.setNavigationBarHidden(false, animated: false)
        }
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        /*
        if let tabbarvc = self.tabBarController as? HomeVC {
            
            tabbarvc.navigationController?.setNavigationBarHidden(true, animated: false)
        }
        */
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setup() {
        
        let users = self.fetchUser()
        if users.count > 0 {
            user = users[0]
            
        }
        getProfile()
        getFamilyMembers()
    }
    
    func getProfile() {
        
        IHProgressHUD.show()
        
        let params = ["id": loggedUser?.id] as [String: Any]
        
        apiManager.getUserProfile(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    
                    memberData.populateCoreData(data: self.user!)
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func getFamilyMembers() {
        
        IHProgressHUD.show()
        
        let params = ["user_id": loggedUser?.id] as [String: Any]
        apiManager.getFamilyMember(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    
                    self.familyMembersArr = memberData
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }

    @IBAction func addAction(_ sender: UIButton) {
        
        let vc = self.getVC(with: "AddFamilyMemberVC", sb: IDMain) as! AddFamilyMemberVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func editAction(_ sender: UIButton) {
        
        let vc = self.getVC(with: "UpdateProfileVC", sb: IDMain) as! UpdateProfileVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ProfileVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 2 {
            return 1
        }
        return section == 0 ? 9 : familyMembersArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: ProfileCell = ProfileCell()
        
        if indexPath.section == 0 {
            
            
            switch indexPath.row {
                
            case 0:
                cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! ProfileCell
                if let img = user?.profile_image, img != "null" {
                    
                    let url = imgBaseURL + custProfileImg + img
                    cell.profileImg.kf.setImage(with: URL(string: url))
                } else {
                    cell.profileImg.image = UIImage(named: "User")
                }
                cell.profileImg.isUserInteractionEnabled = true
                return cell
                
            case 1:
                
                cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! ProfileCell
                cell.lblHead1.text = "NAME"
                cell.lblName1.text = user?.name
                cell.btnEdit.isHidden = false
                cell.btnEdit.addTarget(self, action: #selector(editAction(_:)), for: .touchUpInside)
                return cell
                
            case 2:
                
                cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ProfileCell
                cell.lblHead1.text = "MOBILE NO."
                cell.lblName1.text = user?.mobile_no
                
                cell.lblHead2.text = "GENDER"
                cell.lblName2.text = user?.gender
                
                return cell
                
            case 3:
                
                cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ProfileCell
                cell.lblHead1.text = "DATE OF BIRTH"
                cell.lblName1.text = user?.dob
                
                cell.lblHead2.text = "BLOOD GROUP"
                cell.lblName2.text = user?.bloodgroup
                
                
                return cell
                
            case 4:
                
                cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! ProfileCell
                cell.lblHead1.text = "DO YOU WANT TO DONATE BLOOD IN CASE OF NEED?"
                cell.lblName1.text = user?.is_blood_donor
                cell.btnEdit.isHidden = true
                return cell
                
            case 5:
                
                cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ProfileCell
                cell.lblHead1.text = "HEIGHT (FT)"
                cell.lblName1.text = user?.height ?? "" + " " + (user?.height_unit ?? "")
                
                cell.lblHead2.text = "WEIGHT (KG)"
                cell.lblName2.text = user?.weight ?? "" + " " + (user?.weight_unit ?? "")
                
                
                return cell
                
            case 6:
                
                cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ProfileCell
                cell.lblHead1.text = "EMAIL"
                cell.lblName1.text = user?.email
                
                cell.lblHead2.text = "PROFESSION"
                cell.lblName2.text = user?.profession
                
                return cell
                
            case 7:
                
                cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ProfileCell
                cell.lblHead1.text = "STATE"
                cell.lblName1.text = user?.state
                
                cell.lblHead2.text = "CITY"
                cell.lblName2.text = user?.city
                return cell
                
            case 8:
                
                cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! ProfileCell
                cell.lblHead1.text = "INTERESTS"
                cell.lblName1.text = user?.interest
                cell.btnEdit.isHidden = true
                return cell
                
            default:
                break
            }
            
        }
        
        if indexPath.section == 1 {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "familyCell") as! FamilyMemberListCell
            
            let data = self.familyMembersArr[indexPath.row]
            cell.populateMemberData(data: data)
            
            return cell
        }
        if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! ProfileCell
            cell.btnEdit.addTarget(self, action: #selector(addAction(_:)), for: .touchUpInside)
            
            return cell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "headCell") as! ProfileCell
            
            cell.lblHead1.text = "Family Members"
            cell.lblHead1.font = UIFont.boldSystemFont(ofSize: 17)
            cell.lblHead1.textColor = .black
            
            return cell.contentView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return section == 1 ? 50 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return indexPath.row == 0 ? 140 : 80
        }
        return UITableView.automaticDimension
    }
    
}
