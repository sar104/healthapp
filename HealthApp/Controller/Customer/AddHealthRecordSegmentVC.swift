//
//  AddHealthRecordSegmentVC.swift
//  HealthApp
//
//  Created by Apple on 07/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class AddHealthRecordSegmentVC: BaseVC {

    @IBOutlet weak var segmentedControl: MXSegmentedControl!
    var selIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.segmentedControl.removeAll()
        
        
        segmentedControl.append(title: kMine)
            //.set(titleColor: .black, for: .selected)
        segmentedControl.append(title: kFamily)
        //.set(titleColor: .black, for: .selected)
        
        segmentedControl.select(index: selIndex, animated: false)
        
        segmentedControl.addTarget(self, action: #selector(segmentValueChanged(_:)), for: .valueChanged)
    }
    
    @objc func segmentValueChanged(_ sender: MXSegmentedControl){
        print("selected value: \(sender.selectedIndex)")
        
        
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "SegmentDidChangeNotification"), object: sender.selectedIndex)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        segmentedControl.select(index: selIndex, animated: false)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
