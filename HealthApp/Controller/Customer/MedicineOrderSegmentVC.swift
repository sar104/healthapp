//
//  MedicineOrderSegmentVC.swift
//  HealthApp
//
//  Created by Apple on 15/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class MedicineOrderSegmentVC: BaseVC {

    @IBOutlet weak var segmentedControl: MXSegmentedControl!
    var selChemistData: ChemistData?
    var orderData: MedicineOrderData = MedicineOrderData()
    var selPrescription: PrescriptionData = PrescriptionData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationItem.hidesBackButton = true

        // Do any additional setup after loading the view.
        segmentedControl.append(title: "Prescribed")
            //.set(titleColor: .black, for: .selected)
        segmentedControl.append(title: "Non Prescribed")
        //.set(titleColor: .black, for: .selected)
        
        let selIndex = (UserDefaults.standard.value(forKey: "OrderSegment") as? Int) ?? 0
        segmentedControl.select(index: selIndex, animated: false)
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "OrderSegmentDidChangeNotification"), object: selIndex)
        
        segmentedControl.addTarget(self, action: #selector(segmentValueChanged(_:)), for: .valueChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(navOrder), name:  NSNotification.Name(rawValue: "navOrderNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(navPresc), name:  NSNotification.Name(rawValue: "navPrescNotification"), object: nil)
    }
    
    @objc func segmentValueChanged(_ sender: MXSegmentedControl){
           print("selected value: \(sender.selectedIndex)")
           
           UserDefaults.standard.set(sender.selectedIndex, forKey: "OrderSegment")
           
           NotificationCenter.default.post(name: Notification.Name(rawValue: "OrderSegmentDidChangeNotification"), object: sender.selectedIndex)
    }
    
    @objc func navOrder(notifObj: NSNotification){
        let orderObj = notifObj.object as! [String:Any]
        
        let vc = self.getVC(with: "ConfirmOrderVC", sb: IDMain) as! ConfirmOrderVC
        
        vc.orderData = orderObj["orderData"] as! MedicineOrderData
        if let orderTypeNP = orderObj["orderTypeNP"] as? Bool {
            
            vc.orderTypeNP = orderTypeNP
            
        } else {
            
            vc.selectedMedArr = orderObj["selectedMedArr"] as! [MedicineData]
            vc.prescriptionImg = orderObj["prescriptionImg"] as? Data
            
        }
        vc.prescData = orderObj["prescData"] as! PrescriptionData
        vc.selNonPreMedArr = orderObj["selNonPreMedArr"] as! [MedicineData]
        self.navigationController?.pushViewController(vc, animated: false)
    }

    @objc func navPresc(notifObj: NSNotification) {
        let orderObj = notifObj.object as! [String:Any]
        
        let prescVC = self.getVC(with: "PrescriptionListVC", sb: "Main") as! PrescriptionListVC
        prescVC.type = "self"
        prescVC.delegate = orderObj["delegate"] as? PrescriptionListDelegate
        prescVC.orderData = orderObj["orderData"] as! MedicineOrderData
        prescVC.flagOrder = true
        self.navigationController?.pushViewController(prescVC, animated: false)
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "medicineOrderSegue" {
            
            if let vc = segue.destination as? MedicineOrderTypeVC {
                
                vc.selChemistData = selChemistData
                vc.orderData = orderData
                vc.selPrescription = selPrescription
            }
        }
    }

}
