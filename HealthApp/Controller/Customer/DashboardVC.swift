//
//  DashboardVC.swift
//  HealthApp
//
//  Created by Apple on 05/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD
import Kingfisher

class DashboardVC: BaseVC {

    @IBOutlet weak var tblData: UITableView!
    
    
    var dashboardData: CustDashboardData?
    var reportsArr: [ReportData] = []
    var ordersArr: [OrderData] = []
    var reportDays:[Int] = []
    var reportChild_Name:[String] = []
    var orderDays:[Int] = []
    var arrBanner: [BannerData] = []
    var docArr: [DoctorData] = []
    var alertArr: [LocalNotification] = []
    var reportDate: Date = Date()
    var orderDate: Date = Date()
    var strReportDate: String = ""
    var strOrderDate: String = ""
    var user: User?
    var cntr = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let users = self.fetchUser()
        user = users[0]
        
        self.saveMedData()
        let cnt = CoreDataManager.shared.getMedCount()
        print("med cnt: \(cnt)")
        
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleCloseOrder), name: NSNotification.Name(rawValue: CloseOrderNotification), object: nil)
    }
    
    @objc func handleCloseOrder() {
        
        let vc = self.getVC(with: "OrdersVC", sb: IDMain) as! OrdersVC
        self.navigationController?.pushViewController(vc, animated: false)
    }

    override func viewWillAppear(_ animated: Bool) {
        
        if let tabbarvc = self.tabBarController as? HomeVC {
            
            tabbarvc.navigationController?.setNavigationBarHidden(false, animated: false)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //tblData.estimatedRowHeight = 300
        
        setup()
        getTodaysAlerts()
        getDashboardData()
        getBannersData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let tabbarvc = self.tabBarController as? HomeVC {
            
            tabbarvc.navigationController?.setNavigationBarHidden(true, animated: false)
        }
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setup(){
        
        
        strReportDate = String(reportDate.toString(format: df_MMMM_yyyy))
        strOrderDate = String(orderDate.toString(format: df_MMMM_yyyy))
    }
    
    func getDashboardData(){
        
        IHProgressHUD.show()
        //let params = ["customer_id":"405", "month":"7", "year":"2020"]
        let month = Date().get(.month)
        let year = Date().get(.year)
        let params = ["customer_id":user!.id, "month":month, "year":year] as [String : Any]
        apiManager.getCustDashboard(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                print("data: \(data)")
                self.dashboardData = data
                if let reports = self.dashboardData?.reports {
                    
                    self.reportsArr = reports
                    for report in reports {
                        if let reportdate = report.report_date?.toDate(format: df_yyyy_MM_dd) {
                            self.reportDays.append(reportdate.get(.day))
                            //if(((report as! ReportData).child_name?.count ?? 0) > 0)
                            //{
                            if report.child_name != nil && !report.child_name!.isEmpty { self.reportChild_Name.append(String(report.child_name!.prefix(1)))
                            } else {
                                self.reportChild_Name.append("")
                            }
                            //}
                        }
                    }
                }
                
                if let orders = self.dashboardData?.orders {
                    
                    self.ordersArr = orders
                    
                    for order in orders {
                        if let orderdate = order.order_date?.toDate(format: df_yyyy_MM_dd) {
                            self.orderDays.append(orderdate.get(.day))
                        }
                    }
                }
                if let doctors = self.dashboardData?.doctors {
                    
                    self.docArr = doctors
                }
                print("orderDays.........")
                print(self.orderDays)
                DispatchQueue.main.async {
                    self.tblData.dataSource = self
                    self.tblData.delegate = self
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    func getMonthlyReport(){
        
        IHProgressHUD.show()
        let month = reportDate.get(.month)
        let year = reportDate.get(.year)
        //let params = ["customer_id":"405", "month":month, "year":year] as [String : Any]
        let params = ["customer_id":user!.id, "month":month, "year":year] as [String : Any]
        apiManager.getMonthlyReports(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                print("data: \(data)")
                self.reportDays.removeAll()
                self.reportChild_Name.removeAll()
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
                    let reportsJson = json["reports"]
                    let reportData = try JSONSerialization.data(withJSONObject: reportsJson, options: .prettyPrinted)
                    let reports = try JSONDecoder().decode([ReportData].self, from: reportData)
                    if reports.count > 0 {
                        
                        self.reportsArr = reports
                        self.dashboardData?.reports = reports
                        for report in reports {
                            if let reportdate = report.report_date?.toDate(format: df_yyyy_MM_dd) {
                                self.reportDays.append(reportdate.get(.day))
                                //if(((report as! ReportData).child_name?.count ?? 0) > 0)
                                //{
                                    if report.child_name != nil && !report.child_name!.isEmpty { self.reportChild_Name.append(String(report.child_name!.prefix(1)))
                                        //self.reportChild_Name.append("s")
                                    } else {
                                        self.reportChild_Name.append("")
                                    }
                                //}
                            }
                        }
                    }
                    print("reportDays.........")
                    print(self.reportDays)
                    print(self.reportChild_Name)
                    
                } catch let err {
                    print(err)
                }
                DispatchQueue.main.async {
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    func getMonthlyOrder(){
        
        IHProgressHUD.show()
        let month = orderDate.get(.month)
        let year = orderDate.get(.year)
        //let params = ["customer_id":"405", "month":month, "year":year] as [String : Any]
        let params = ["customer_id":user!.id, "month":month, "year":year] as [String : Any]
        apiManager.getMonthlyOrders(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
                print("data: \(data)")
                self.orderDays.removeAll()
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
                    let ordersJson = json["orders"]
                    let orderData = try JSONSerialization.data(withJSONObject: ordersJson, options: .prettyPrinted)
                    let orders = try JSONDecoder().decode([OrderData].self, from: orderData)
                    if orders.count > 0 {
                        
                        self.dashboardData?.orders = orders
                        self.ordersArr = orders
                        
                        for order in orders {
                            if let orderdate = order.order_date?.toDate(format: df_yyyy_MM_dd) {
                                
                                self.orderDays.append(orderdate.get(.day))
                            }
                        }
                    }
                    print("orderDays.........")
                    print(self.orderDays)
                    
                } catch let err {
                    print(err)
                }
                DispatchQueue.main.async {
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
            
        }
    }
    
    func getBannersData(){
        
        IHProgressHUD.show()
        
        let age = user!.dob!.toDate(format:df_dd_MM_yyyy).getDiff()
        let interest = user?.interest ?? ""
        let gender = user?.gender ?? ""
        let params = ["user_id":user!.id, "interest": interest, "age": age,"gender": gender] as [String : Any]
        apiManager.getFilterBanners(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                print("data: \(data)")
                self.arrBanner = data.filter{ ( $0.image_video != "null")}
                
                DispatchQueue.main.async {
                    self.tblData.dataSource = self
                    self.tblData.delegate = self
                    self.tblData.reloadData()
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func getTodaysAlerts() {
        
        LocalNotificationManager().listTodaysNotifications {  (arr) in
            
            self.alertArr = arr
            DispatchQueue.main.async {
                self.tblData.dataSource = self
                self.tblData.delegate = self
                self.tblData.reloadData()
            }
            
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DashboardVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 4
        case 1:
            return 1
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = DashboardCell()
           
        switch indexPath.section {
        case 0:
            
            
            switch indexPath.row {
            case 0:break
                
            case 1:
                cell = tableView.dequeueReusableCell(withIdentifier: "healthRecordCell") as! DashboardCell
                
                cell.delegate = self
                
                if self.reportDays.count > 0 {
                    cell.months = self.getAllMonths()
                    cell.dateCollvw.reloadData()
                    cell.days = self.reportDays
                    cell.reportChild_Name = self.reportChild_Name
                    cell.dateCollvw.isHidden = false
                    cell.viewNoReports.isHidden = true
                    
                    if let btn = cell.contentView.viewWithTag(10) as? UIButton {
                        btn.isHidden = false
                    }
                    cell.btnTopAddReport.isHidden = false
                    
                } else {
                    if let btn = cell.contentView.viewWithTag(10) as? UIButton {
                        btn.isHidden = true
                    }
                    cell.btnTopAddReport.isHidden = true
                    cell.dateCollvw.isHidden = true
                    cell.viewNoReports.isHidden = false
                }
                cell.lblReportMonth.text = strReportDate
                
                if self.orderDays.count > 0 {
                    cell.months = self.getAllMonths()
                    cell.dateMedCollvw.reloadData()
                    cell.orderDays = self.orderDays
                    
                    cell.dateMedCollvw.isHidden = false
                    cell.viewNoOrders.isHidden = true
                    
                    if let btn = cell.contentView.viewWithTag(2) as? UIButton {
                        //btn.isHidden = false
                    }
                    cell.btnTopAddMed.isHidden = false
                    
                } else {
                    if let btn = cell.contentView.viewWithTag(2) as? UIButton {
                        //btn.isHidden = true
                    }
                    cell.btnTopAddMed.isHidden = true
                    cell.dateMedCollvw.isHidden = true
                    cell.viewNoOrders.isHidden = false
                }
                cell.lblMedMonth.text = strOrderDate
                
            case 2: break
                
            case 3:
                cell = tableView.dequeueReusableCell(withIdentifier: "doctorsCell") as! DashboardCell
                cell.delegate = self
                if docArr.count > 0 {
                    cell.docView.isHidden = false
                    cell.noDocView.isHidden = true
                    
                    for i in 1..<docArr.count+1 {
                        if i < 5{
                            let btn1 = cell.docView.viewWithTag(i) as! UIButton
                            btn1.isHidden = false
                            let docData = docArr[i-1]
                            let title = docData.doctor_name?.first?.uppercased()
                            btn1.setTitle(title, for: .normal)
                            if let imgurl = docData.shop_image {
                                let url = doctorImgUrl + imgurl
                                btn1.imageView?.kf.setImage(with: URL(string: url))
                            }
                        }
                    }
                    
                    
                    
                    
                } else {
                    cell.docView.isHidden = true
                    cell.noDocView.isHidden = false
                }
            
                if alertArr.count > 0 {
                    cell.alertView.isHidden = false
                    cell.noAlertView.isHidden = true
                    
                    
                    Timer.scheduledTimer(withTimeInterval: 10.0, repeats: true) { (timer) in
                               
                        let alertData = self.alertArr[self.cntr]
                        cell.lblAlert.text = alertData.title
                        
                        if self.cntr < self.alertArr.count-1 {
                            self.cntr = self.cntr + 1
                        } else {
                            self.cntr = 0
                        }
                    }
                    
                } else {
                    cell.alertView.isHidden = true
                    cell.noAlertView.isHidden = false
                }
            default:
                break
            }
        
        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: "bannerCell") as! DashboardCell
            
            cell.delegate = self
            cell.bannerArr = self.arrBanner
            cell.bannerCollView.reloadData()
            
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 1 || indexPath.row == 3 {
                
                (cell as! DashboardCell).layer.cornerRadius = 10
                (cell as! DashboardCell).layer.masksToBounds = true
                
                if indexPath.row == 1 {
                    (cell as! DashboardCell).layer.borderColor = RGB_251_3_79.cgColor
                    (cell as! DashboardCell).layer.borderWidth = 1
                    
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            var height: CGFloat = 20.0
            switch indexPath.row {
                
            case 1:
                height = 365
            case 3:
                height = 130
                
            default:
                break
            }
            
            return height
        }
        return 66
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "header1")
            
            return cell?.contentView
        }
        if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "header2")
            
            return cell?.contentView
        }
        return nil
    }
    
}

extension DashboardVC: DashboardCellDelegate{
    
    func onNextClicked(date: Date, type: String) {
        
        switch type {
        case "report":
            reportDate = date
            strReportDate = reportDate.toString(format: df_MMMM_yyyy)
            self.getMonthlyReport()
        case "order":
            orderDate = date
            strOrderDate = orderDate.toString(format: df_MMMM_yyyy)
            self.getMonthlyOrder()
        default: break
                
        }
    }
    
    func onPrevClicked(date: Date, type: String) {
        switch type {
        case "report":
            reportDate = date
            strReportDate = reportDate.toString(format: df_MMMM_yyyy)
            self.getMonthlyReport()
        case "order":
            orderDate = date
            strOrderDate = orderDate.toString(format: df_MMMM_yyyy)
            self.getMonthlyOrder()
        default: break
                
        }
    }
    
    func healthRecordAddAction(type: Int) {
        if type == 0 {
            
            let vc = self.getVC(with: "AddHealthRecordSegmentVC", sb: "Main") as! AddHealthRecordSegmentVC
            self.navigationController?.pushViewController(vc, animated: false)
            
        } else {
            let vc = self.getVC(with: "ParallaxVC", sb: "Main") as! ParallaxVC
            vc.type = "chemist"
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func setAlertAction() {
        
        let vc = self.getVC(with: "MyAlertsVC", sb: IDMain) as! MyAlertsVC
        vc.type = "Alerts"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func addDoctorAction() {
        
        let vc = self.getVC(with: "AddDoctorVC", sb: IDMain)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func viewDoctorAction() {
        
        let vc = self.getVC(with: "ChemistListVC", sb: IDMain) as! ChemistListVC
        vc.type = "doctor"
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func onBannerClick() {
        
        let vc = self.getVC(with: "BlogVC", sb: IDMain) as! BlogVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func onDateClicked(day: Int, type: String) {
        
        
        
        if type == "ehr" {
            let month = reportDate.get(.month)
            let year = reportDate.get(.year)
            var components = DateComponents(calendar: Calendar.current, year: year, month: month, day: day)
            
            let date = components.date
            let strdate = date?.toString(format: "yyyy-MM-dd")
            
            let records = self.reportsArr.filter({ $0.report_date == strdate })
            if records.count > 0 {
                if records.count > 1 {
                    
                    let vc = self.getVC(with: "MyAlertsVC", sb: IDMain) as! MyAlertsVC
                    vc.type = "Reports"
                    self.navigationController?.pushViewController(vc, animated: false)
                    
                } else {
                    
                    let record = records[0]
                    if record.type == "Report" {
                        let vc = self.getVC(with: "ReportsVC", sb: IDMain) as! ReportsVC
                        if record.child_name == loggedUser?.name {
                            vc.type = "self"
                        } else {
                            vc.type = "family"
                        }
                        
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                    
                    if record.type == "Prescription" {
                        
                        let vc = self.getVC(with: "PrescriptionListVC", sb: IDMain) as! PrescriptionListVC
                        if record.child_name == loggedUser?.name {
                            vc.type = "self"
                        } else {
                            vc.type = "family"
                        }
                        
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }
            }
            
        }
        
        if type == "order" {
            let month = orderDate.get(.month)
            let year = orderDate.get(.year)
            var components = DateComponents(calendar: Calendar.current, year: year, month: month, day: day)
            
            let date = components.date
            let strdate = date?.toString(format: "yyyy-MM-dd")
            
            let records = self.ordersArr.filter({ $0.order_date == strdate })
            if records.count > 0 {
               
                let vc = self.getVC(with: "OrdersVC", sb: IDMain) as! OrdersVC
                
                self.navigationController?.pushViewController(vc, animated: false)
                
            }
        }
    }
}


protocol DashboardCellDelegate {
    func onNextClicked(date: Date, type: String)
    func onPrevClicked(date: Date, type: String)
    
    func healthRecordAddAction(type: Int)
    func setAlertAction()
    func addDoctorAction()
    func viewDoctorAction()
    func onBannerClick()
    func onDateClicked(day: Int, type: String)
}

class DashboardCell: BaseCell {
    
    @IBOutlet weak var bgviewHealth: UIView!
    @IBOutlet weak var bgviewMedi: UIView!
    @IBOutlet weak var dateCollvw: UICollectionView!
    @IBOutlet weak var dateMedCollvw: UICollectionView!
    @IBOutlet weak var lblReportMonth: UILabel!
    @IBOutlet weak var lblMedMonth: UILabel!
    @IBOutlet weak var viewNoReports: UIView!
    @IBOutlet weak var btnAddReport: UIButton!
    @IBOutlet weak var btnTopAddReport: UIButton!
    @IBOutlet weak var btnTopAddMed: UIButton!
    @IBOutlet weak var viewNoOrders: UIView!
    @IBOutlet weak var bannerCollView: UICollectionView!
    
    @IBOutlet weak var noDocView: UIView!
    @IBOutlet weak var docView: UIView!
    
    @IBOutlet weak var noAlertView: UIView!
    @IBOutlet weak var alertView: UIView!
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblAlert: UILabel!
    
    var delegate: DashboardCellDelegate?
    var days: [Int]?
    var reportChild_Name:[String]?
    var orderDays: [Int]?
    var months: [String] = []
    var bannerArr: [BannerData] = []
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configCell(id: String, dataFlag: Bool){
        
    }
    
    @IBAction func nextAction(_ sender: UIButton){
        
        switch sender.tag {
        case 2:
            print("2")
            if var currdate = self.lblReportMonth.text?.toDate(format: df_MMMM_yyyy) {
                currdate = currdate.nextDate()
                self.lblReportMonth.text = currdate.toString(format: df_MMMM_yyyy)
                self.delegate?.onNextClicked(date: currdate, type: "report")
            }
        case 4:
            print("4")
            if var currdate = self.lblMedMonth.text?.toDate(format: df_MMMM_yyyy) {
                currdate = currdate.nextDate()
                self.lblMedMonth.text = currdate.toString(format: df_MMMM_yyyy)
                self.delegate?.onNextClicked(date: currdate, type: "order")
            }
        default:
            break
        }
    }
    
    @IBAction func prevAction(_ sender: UIButton){
        switch sender.tag {
        case 1:
            print("1")
            if var currdate = self.lblReportMonth.text?.toDate(format: df_MMMM_yyyy) {
                currdate = currdate.prevDate()
                self.lblReportMonth.text = currdate.toString(format: df_MMMM_yyyy)
                self.delegate?.onPrevClicked(date: currdate, type: "report")
            }
        case 3:
            print("3")
            if var currdate = self.lblMedMonth.text?.toDate(format: df_MMMM_yyyy) {
                currdate = currdate.prevDate()
                self.lblMedMonth.text = currdate.toString(format: df_MMMM_yyyy)
                self.delegate?.onPrevClicked(date: currdate, type: "order")
            }
        default:
            break
        }
    }
    @IBAction func getMedicinesAction(_ sender: UIButton){
        
        delegate?.healthRecordAddAction(type: 1)
    }
    
    @IBAction func addHealthRecordAction(_ sender: UIButton){
        
        delegate?.healthRecordAddAction(type: 0)
    }
    
    @IBAction func setAlertsAction(_ sender: UIButton) {
        delegate?.setAlertAction()
    }
    
    @IBAction func addDoctorAction(_ sender: UIButton) {
        delegate?.addDoctorAction()
    }
    
    @IBAction func viewDoctorAction(_ sender: UIButton) {
        delegate?.viewDoctorAction()
    }
}

extension DashboardCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == bannerCollView {
            return bannerArr.count
        }
        return 31
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView != bannerCollView {
            var reuseId = "dateCell"
            var days: [Int] = []
            if collectionView == dateMedCollvw {
                reuseId = "dateMedCell"
                days = self.orderDays ?? []
            } else {
                days = self.days ?? []
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath) as! DashboardCollectionCell
            cell.lblUser?.isHidden = true
            if days.count > 0, days.contains(indexPath.item + 1) {
                cell.lblDate.text = String(indexPath.item + 1)
                cell.lblUser?.isHidden = true
                if(indexPath.row < self.reportChild_Name!.count)
                {
                    let char : String = self.reportChild_Name?[indexPath.row] ?? ""
                    if(char.count > 0)
                    {
                        cell.lblUser?.isHidden = false
                        cell.lblUser?.text = char.uppercased()
                    }
                }
                if collectionView == dateMedCollvw {
                    cell.iconPill.image = UIImage(named: "pill_blue")
                    cell.iconPill.alpha = 1
                }
                
            } else {
                cell.lblDate.text = ""
                cell.lblUser?.text = ""
                if collectionView == dateMedCollvw {
                    cell.iconPill.image = UIImage(named: "pill_grey")
                    cell.iconPill.alpha = 0.1
                }
            }
            return cell
        }
        else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath) as! DashboardCollectionCell
            
            let bannerData = bannerArr[indexPath.row]
            if let imgname = bannerData.image_video {
                let url = bannerURL + imgname
                cell.bannerImg.kf.setImage(with: URL(string: url))
                cell.bannerImg.layer.masksToBounds = true
                cell.bannerImg.layer.cornerRadius = 10
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
      
        if collectionView != bannerCollView {
            var dashcell : DashboardCollectionCell = cell as! DashboardCollectionCell
            var days: [Int] = []
            cell.layer.backgroundColor = UIColor.clear.cgColor
            cell.contentView.backgroundColor = UIColor.clear
            if collectionView == dateCollvw {
                days = self.days ?? []
                if days.count > 0, days.contains(indexPath.item + 1) {
                    dashcell.lblDate.layer.backgroundColor = RGB_DashReport.cgColor //UIColor.blue.cgColor//#01B5CA
                } else {
                    dashcell.lblDate.layer.backgroundColor = UIColor.clear.cgColor
                }
                dashcell.lblDate.layer.masksToBounds = true
                dashcell.lblDate.layer.cornerRadius = 8
                dashcell.lblUser?.layer.masksToBounds = true
                dashcell.lblUser?.layer.cornerRadius = 6
            }
        } else {
            //cell.contentView.layer.masksToBounds = true
            //cell.layer.cornerRadius = 10
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView != bannerCollView {
            if collectionView == dateMedCollvw {
                return CGSize(width: 21.5, height: 21.5)
            }
            return CGSize(width: 21, height: 45)
        } else {
            return CGSize(width: 80, height: 70)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //BlogVC
        if collectionView == bannerCollView {
            delegate?.onBannerClick()
        }
        
        if collectionView == dateCollvw {
            let day = indexPath.item + 1
            /*
            if let days = self.days, days.count > 0, days.contains(indexPath.item + 1) {
                day = days[indexPath.item + 1]
            }*/
            delegate?.onDateClicked(day: day, type: "ehr")
        }
        
        if collectionView == dateMedCollvw {
            let day = indexPath.item + 1 //self.orderDays![indexPath.item]
            delegate?.onDateClicked(day: day, type: "order")
        }
    }
}
class DashboardCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblUser: UILabel?
    @IBOutlet weak var con_lblW: NSLayoutConstraint!
    @IBOutlet weak var con_lblH: NSLayoutConstraint!
    @IBOutlet weak var iconPill: UIImageView!
    @IBOutlet weak var bannerImg: UIImageView!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
}
/*
 //customer_id:124
 month:3
 year:2020
 {
     "reports": [
         {
             "report_date": "2020-03-14",
             "total_reports": 11
         }
     ],
     "orders": [
         {
             "order_date": "2020-03-16",
             "total_orders": 2
         },
         {
             "order_date": "2020-03-10",
             "total_orders": 4
         },
         {
             "order_date": "2020-03-06",
             "total_orders": 4
         },
         {
             "order_date": "2020-03-05",
             "total_orders": 1
         }
     ],
     "doctors": [
         {
             "id": 111,
             "doctor_name": "Suraj",
             "mobile_no": 9657136379,
             "email": "asd@asdf.sdd",
             "profile_image": "1bSTjJuxtYQXV9Zrc1lD.jpg"
         }
     ],
     "status": true,
     "message": "Record Found."
 }
 **/
