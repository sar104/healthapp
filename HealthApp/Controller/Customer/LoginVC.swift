//
//  LoginVC.swift
//  HealthApp
//
//  Created by Apple on 22/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class LoginVC: BaseVC {

    @IBOutlet weak var txtMob: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        UserDefaults.standard.set(1, forKey: "newuser")
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        txtMob.removeBottomBorder()
        txtMob.addBottomBorder()
    }

    func setup(){
        
        txtMob.removeBottomBorder()
        txtMob.addBottomBorder()
        txtMob.setPlaceholder(str1: "", str2: lblMobNum)
        txtMob.inputAccessoryView = self.addDoneButton(dontTxt: strDone)
    }
    
    override func donePicker (sender: UIBarButtonItem) {
        txtMob.resignFirstResponder()
    }
    
    @IBAction func getOTPAction() {
        
        //self.navToOTP(mob: "9850059684")
        
        if validateForm() {
            loginAPI()
        }
    }
    
    func validateForm() -> Bool {
        
        if let mob = txtMob.text, mob.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: err_mob)
            return false
        }
        return true
    }
    
    
    func loginAPI(){
        
        let params = [PARAM_USERNAME: txtMob.text!] as [String:Any]
        
        print(params)
        IHProgressHUD.show(withStatus: "Loading")
        
        apiManager.loginAPI(params: params) { (result) in
                 
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                print("data: \(data)")
                if data.status {
                    
                    DispatchQueue.main.async {
                        self.navToOTP(mob: self.txtMob.text!)
                    }
                    
                } else {
                    self.alert(strTitle: strErr, strMsg: data.message!)
                }
                
            case .failure(let err):
                print("err: \(err)")
                self.alert(strTitle: strErr, strMsg: err.localizedDescription)
            }
            
        }
        
        /*
         data =     {
             "about_clinic" = "<null>";
             "active_status" = 1;
             address = "<null>";
             "address_id" = "<null>";
             "address_type" = "<null>";
             "afternoon_closing_time" = "<null>";
             "afternoon_opening_time" = "<null>";
             "alt_contact_no" = "<null>";
             "assistant_name" = "<null>";
             bloodgroup = null;
             city = null;
             "clinic_lab_hospital_name" = "<null>";
             "clinic_lab_hospital_phone_no" = "<null>";
             "created_at" = "2020-09-27 15:05:00";
             "deleted_at" = "<null>";
             "delivery_status" = "<null>";
             dob = "1980-09-27";
             email = null;
             "fcm_token" = 1234567890;
             gender = Male;
             "gst_no" = "<null>";
             height = null;
             "height_unit" = null;
             "hospital_name" = "<null>";
             id = 496;
             interest = null;
             "is_blood_donor" = null;
             "is_login" = "<null>";
             "last_login" = "<null>";
             latitude = "<null>";
             longitude = "<null>";
             "mobile_no" = 9049231199;
             name = "Krishna Chemist";
             "off_day" = "<null>";
             otp = 469730;
             "parent_id" = "<null>";
             password = "<null>";
             pincode = "<null>";
             profession = null;
             "profile_completed" = 1;
             "profile_image" = null;
             qualification = "<null>";
             "registration_no" = "<null>";
             relation = "<null>";
             role = 3;
             "shop_closing_time" = "<null>";
             "shop_image" = "<null>";
             "shop_opening_time" = "<null>";
             specialization = "<null>";
             state = null;
             status = "<null>";
             "updated_at" = "2020-09-27 15:05:00";
             weight = null;
             "weight_unit" = null;
         };
         message = "OTP Sent successfully.";
         status = 1;
         **/
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LoginVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
