//
//  ParallaxVC.swift
//  HealthApp
//
//  Created by Apple on 14/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD
import CoreLocation
import MapKit

protocol ParallaxDelegate {
    
    func onSelectLocation(address: String, lat: CGFloat, lon: CGFloat)
}

class ParallaxVC: VFParallaxController, UITextFieldDelegate {

    let apiManager: WebServiceManager = WebServiceManager(service: WebService())
    var chemistArr: [ChemistData] = []
    var docArr: [FavDoctor] = []
    var diagnosticArr: [DiagnosticData] = []
    var type: String?
    var txtLocation: UITextField!
    var srchLat: CGFloat = 0.0
    var srchLon: CGFloat = 0.0
    var rightButton: UIButton!
    var user: User?
    var specialization: String?
    let defaults = UserDefaults.standard
    var delegate: ParallaxDelegate?
    
    lazy var coreDataManager: CoreDataManager = {
        let manager = CoreDataManager.shared
        return manager
    }()
    lazy var lblNoRecords: UILabel = {
        
        let label = UILabel()
        label.frame = CGRect(x: 10, y: 10, width: view.frame.width-40, height: 70)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = .darkGray
        label.isHidden = true
        return label
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let users = self.fetchUser()
        user = users[0]
        
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
        tableView.isHidden = true
        self.openShutter()
        tableView.estimatedRowHeight = 130
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleMapTap(gestureRecognizer:)))
        mapView.addGestureRecognizer(gestureRecognizer)
        
        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "ChemistCell", bundle: nil), forCellReuseIdentifier: "chemistCell")
         
        let width = view.frame.size.width - 50
        let x = (view.frame.size.width - width)/2
        
        let x1 = (view.frame.size.width - 160)/2
        
        let lblHead = UILabel(frame: CGRect(x: x1, y: 50, width:
        180, height: 80))
        lblHead.numberOfLines = 0
        lblHead.textAlignment = .center
        lblHead.lineBreakMode = .byWordWrapping
        
        var title = " NEAR BY"
        if type == "chemist" {
            title = title + " CHEMISTS"
        }
        if type == "doctor" {
            title = title + " DOCTORS"
        }
        if type == "diagnostic" {
           title = title + " DIAGNOSTIC"
        }
        if type == "service" {
           title = title + " SERVICE PROVIDERS"
        }
        lblHead.set(image: UIImage(named: "map")!, with: title)
       
        self.view.addSubview(lblHead)
        
        txtLocation = UITextField(frame: CGRect(x: x, y: 120, width:
            width, height: 40))
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 10, height: 2.0))
        txtLocation.leftView = leftView
        txtLocation.leftViewMode = .always
        setupTextField()
        
        txtLocation.backgroundColor = .white
        txtLocation.delegate = self
        self.view.addSubview(txtLocation)
        
        srchLat = defaults.value(forKey: kUserLat) as? CGFloat ?? 0.0
        srchLon = defaults.value(forKey: kUserLong) as? CGFloat ?? 0.0
        //getChemistList()
        self.getAddress(location: CLLocation(latitude: CLLocationDegrees(srchLat), longitude: CLLocationDegrees(srchLon)))
        searchChemists()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        if let tabbarvc = self.tabBarController as? HomeVC {
            
            tabbarvc.navigationController?.setNavigationBarHidden(true, animated: false)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if type != "service" {
            if let tabbarvc = self.tabBarController as? HomeVC {
                
                tabbarvc.navigationController?.setNavigationBarHidden(false, animated: false)
            }
        }
    }
    
    func fetchUser() -> [User] {
        let users = self.coreDataManager.fetchLocalUsers()
        //self.coreDataService?.fetchLocalUsers() ?? []
        //print(user)
        return users
    }
    
    func showNoRecords() {
        
        self.tableView.isHidden = true
        
        let label = self.lblNoRecords
        var msg = ""
        
        
        if type == "chemist" {
            msg = "Sorry! There is no Chemist available in your Area at this time. "
        }
        if type == "doctor" {
            msg = "Sorry! There is no Doctor available in your Area at this time."
        }
        if type == "diagnostic" {
            msg = "Sorry! There is no Path Lab available in your Area at this time."
        }
        if type == "service" {
            msg = "Sorry! There is no Service Provider available in your Area at this time. "
        }
        
        label.text = msg
        label.isHidden = false
        self.view.addSubview(label)
        label.center = view.center
    }
    
    func getChemistList() {
        
        let params = [PARAM_USERID: user?.id,
        PARAM_LATITUDE: srchLat,//19.980661,
        PARAM_LONGITUDE: srchLon] as [String:Any]
            //73.797836] as [String:Any]
        
        IHProgressHUD.show()
        
        apiManager.searchChemists(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    self.chemistArr = data
                    DispatchQueue.main.async {
                        
                        if data.count > 0 {
                            self.lblNoRecords.isHidden = true
                            self.tableView.isHidden = false
                            self.tableView.reloadData()
                            self.closeShutter()
                            self.showPins()
                        } else {
                            self.showNoRecords()
                        }
                        
                    }
                case .failure(let err):
                print("err: \(err)")
                self.showNoRecords()
            }
        }
    }
    
    func getDoctorList() {
        
        let params = [PARAM_USERID: user?.id,
        PARAM_LATITUDE: srchLat,//19.980661,
        PARAM_LONGITUDE: srchLon] as [String:Any]
        
        IHProgressHUD.show()
        
        apiManager.searchDoctors(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    self.docArr = data
                    print("doctors: \(data)")
                    DispatchQueue.main.async {
                        if data.count > 0 {
                            self.lblNoRecords.isHidden = true
                            self.tableView.isHidden = false
                            self.tableView.reloadData()
                            self.closeShutter()
                            self.showPins()
                        } else {
                            self.showNoRecords()
                        }
                    }
                case .failure(let err):
                print("err: \(err)")
                self.showNoRecords()
            }
        }
    }
    
    func getDiagnosticList() {
        
        let params = [PARAM_USERID: user?.id,
        PARAM_LATITUDE: srchLat,//19.980661,
        PARAM_LONGITUDE: srchLon] as [String:Any]
        
        IHProgressHUD.show()
        
        apiManager.searchDiagnostic(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    self.diagnosticArr = data
                    DispatchQueue.main.async {
                        if data.count > 0 {
                            self.lblNoRecords.isHidden = true
                            self.tableView.isHidden = false
                            self.tableView.reloadData()
                            self.closeShutter()
                            self.showPins()
                        } else {
                            self.showNoRecords()
                        }
                    }
                case .failure(let err):
                print("err: \(err)")
                self.showNoRecords()
            }
        }
    }
    
    //
    func getServiceProviderList() {
        
        let params = [PARAM_USERID: user?.id,
        PARAM_LATITUDE: srchLat,//19.980661,
        PARAM_LONGITUDE: srchLon,
        "specialization": specialization] as [String:Any]
        
        IHProgressHUD.show()
        
        apiManager.searchServiceProviders(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    self.diagnosticArr = data
                    DispatchQueue.main.async {
                        
                        if data.count > 0 {
                            self.lblNoRecords.isHidden = true
                            self.tableView.isHidden = false
                            self.tableView.reloadData()
                            self.closeShutter()
                            self.showPins()
                        } else {
                            self.showNoRecords()
                        }
                    }
                case .failure(let err):
                print("err: \(err)")
                self.showNoRecords()
            }
        }
    }

    func getLocation(address: String){
        
        var geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) {
            placemarks, error in
            let placemark = placemarks?.first
            let lat = placemark?.location?.coordinate.latitude
            let lon = placemark?.location?.coordinate.longitude
            print("Lat: \(lat), Lon: \(lon)")
            
            if let latitude = lat, let longitude = lon {
                
                self.srchLat = CGFloat(latitude)
                self.srchLon = CGFloat(longitude)
                let mklocation = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                self.zoomToLocation(mklocation, minLatitude: kOpenShutterLatitudeMinus, animated: true)
                
                //self.getChemistList()
            }
        }
    }
    
    func getAddress(location: CLLocation){
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { placemarks, error in
            
            let placemark = placemarks?.first
            let address = NSMutableString()
            
            if let subLocality = placemark?.subLocality {
                address.append(subLocality)
            }
            address.append(", ")
            if let locality = placemark?.locality {
                address.append(locality)
            }
            self.srchLat = CGFloat(location.coordinate.latitude)
            self.srchLon = CGFloat(location.coordinate.longitude)
            self.txtLocation.text = address as String
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        
        print("string: \(string)")
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(ParallaxVC.getCoordinate),
            object: textField)
        self.perform(
            #selector(ParallaxVC.getCoordinate),
            with: textField,
            afterDelay: 0.8)
        return true
    }

    @objc func getCoordinate(textField: UITextField) {
        
        if let address = textField.text, !address.isEmpty {
            //rightButton.setImage(UIImage(named: "checked"), for: .normal)
            //rightButton.frame = CGRect(x: CGFloat(txtLocation.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
            self.updateRightImage(name: "checked")
            self.getLocation(address: address)
        } else {
            //rightButton.setImage(UIImage(named: "Close"), for: .normal)
            self.updateRightImage(name: "Close")
        }
    }
    
    @objc func handleMapTap(gestureRecognizer: UITapGestureRecognizer){
        
        let location = gestureRecognizer.location(in: mapView)
        let coordinate = mapView.convert(location, toCoordinateFrom: mapView)

        // Add annotation:
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        mapView.addAnnotation(annotation)
        
        self.getAddress(location: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude))
    }
    func showPins() {
        
        for data in chemistArr {
            
            if let latitude = data.latitude, let longitude = data.longitude {
                let annotation = MKPointAnnotation()
                let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                annotation.coordinate = coordinate
                mapView.addAnnotation(annotation)
            }
        }
    }
    
    func setupTextField() {
        
        txtLocation.layer.cornerRadius = 15
        
        rightButton = UIButton(type: .custom)
        //rightButton.backgroundColor = .gray
        rightButton.setImage(UIImage(named: "Close"), for: .normal)
        rightButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        rightButton.contentMode = .scaleAspectFit
        rightButton.frame = CGRect(x: CGFloat(txtLocation.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        if type == "updateChemist" {
            rightButton.addTarget(self, action: #selector(self.checkAction(_:)), for: .touchUpInside)
        } else {
            rightButton.addTarget(self, action: #selector(self.searchChemists), for: .touchUpInside)
        }
        txtLocation.rightView = rightButton
        txtLocation.rightViewMode = .always
        
        //tableView.layer.cornerRadius = 20
        //tableView.layer.masksToBounds = false
    }
    
    @objc func checkAction(_ sender: UIButton) {
          
        delegate?.onSelectLocation(address: self.txtLocation.text ?? "", lat: srchLat, lon: srchLon)
        self.navigationController?.popViewController(animated: false)
        
    }
    
    func updateRightImage(name: String) {
        
        rightButton.setImage(UIImage(named: name), for: .normal)
        rightButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        rightButton.contentMode = .scaleAspectFit
        rightButton.frame = CGRect(x: CGFloat(txtLocation.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
    }
    
    @objc func searchChemists(){
        
        
        if type == "chemist" {
            self.getChemistList()
        }
        if type == "doctor" {
            self.getDoctorList()
        }
        if type == "diagnostic" {
            self.getDiagnosticList()
        }
        if type == "service" {
            self.getServiceProviderList()
        }
    }
    
    @objc func addressAction(_ sender: UITapGestureRecognizer) {
        
        let tag = sender.view?.tag ?? 0
        
        let data = self.docArr[tag]
        
        var strurl = "http://maps.google.com/maps?q="
        strurl = strurl + data.latitude! + ","
        strurl = strurl + data.longitude! + "&iwloc=A&hl=es"
        let url = URL(string: strurl)
        let items = [url] as [Any]//["This app is my favorite"]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        
        present(ac, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if type == "chemist" {
            return chemistArr.count
        }
        if type == "doctor" {
            return docArr.count
        }
        if type == "diagnostic" {
            return diagnosticArr.count
        }
        if type == "service" {
            return diagnosticArr.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = ChemistCell()
        if type == "chemist" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "chemistCell", for: indexPath) as! ChemistCell
            
            let data = self.chemistArr[indexPath.row]
            cell.btnFav.tag = indexPath.row
            cell.btnOrder.tag = indexPath.row
            //PLACE ORDER
            cell.btnOrder.setTitle("Place Order", for: .normal)
            cell.btnOrder.setTitleColor(RGB_btnReOrder, for: .normal)
            cell.delegate = self
            cell.populateData(data: data)
            //delivery_status
            if let status = data.delivery_status, status == "Yes" {
                cell.imgDelivery.image = UIImage(named: "green_bike")
            } else {
                cell.imgDelivery.image = UIImage(named: "gray_bike")
            }
            return cell
        }
        if type == "doctor" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "chemistCell", for: indexPath) as! ChemistCell
            
            let data = self.docArr[indexPath.row]
            cell.btnFav.tag = indexPath.row
            cell.btnOrder.tag = indexPath.row
            cell.btnOrder.setTitle("Share EHR", for: .normal)
            cell.btnOrder.setTitleColor(RGB_btnReOrder, for: .normal)
            //cell.btnOrder.isHidden = true
            cell.delegate = self
            cell.populateFavDocData(data: data)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.addressAction(_:)))
            cell.lblAddress.tag = indexPath.row
            cell.lblAddress.isUserInteractionEnabled = true
            cell.lblAddress.addGestureRecognizer(tap)
            tap.delegate = self
            cell.lblHour.text = String(format: "%.2f", data.distance ?? 0) + " Km away"
            cell.imgDelivery.isHidden = true
            
            return cell
        }
        if type == "diagnostic" || type == "service" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "chemistCell", for: indexPath) as! ChemistCell
            
            let data = self.diagnosticArr[indexPath.row]
            cell.btnFav.tag = indexPath.row
            cell.btnOrder.tag = indexPath.row
            //cell.btnOrder.setTitle("Share EHR", for: .normal)
            cell.delegate = self
            cell.populateDiagnosticData(data: data)
            cell.lblHour.text = String(format: "%.2f", data.distance ?? 0) + " Km away"
            cell.imgDelivery.isHidden = true
            cell.btnOrder.isHidden = true
            return cell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 130//UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
            
            //ChemistDetailsVC
        
        
        let vc = UIStoryboard(name: IDMain, bundle: nil).instantiateViewController(withIdentifier: "ChemistDetailsVC") as! ChemistDetailsVC
        vc.type = type
        if type == "doctor" {
            let data = docArr[indexPath.row]
            vc.doctorData = data
            vc.id = String(data.id!)
            vc.favorite = data.favourite
        }
        if type == "chemist" {
            let data = chemistArr[indexPath.row]
            vc.chemistData = data
            vc.id = String(data.id!)
            vc.favorite = data.favourite
        }
        if type == "diagnostic" || type == "service" {
            let data = diagnosticArr[indexPath.row]
            vc.diagnosticData = data
            vc.id = String(data.id!)
            vc.favorite = data.favourite
        }
        self.navigationController?.pushViewController(vc, animated: false)
        
        
    }
}
extension ParallaxVC: ChemistCellDelegate {
    
    func onPlaceOrderClicked(id: Int){
        
        if type == "chemist" {
            let chemistData = self.chemistArr[id]
            let sb = UIStoryboard(name: "Main", bundle: nil)
            
            let orderVC = sb.instantiateViewController(withIdentifier: "MedicineOrderSegmentVC") as! MedicineOrderSegmentVC
            orderVC.selChemistData = chemistData
            orderVC.orderData.chemist_id = chemistData.id
            self.navigationController?.pushViewController(orderVC, animated: false)
        }
        if type == "doctor" {
            
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "MyAlertsVC") as! MyAlertsVC
            vc.type = "Reports"
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func onFavCliked(id: Int){
    
    }
}
