//
//  ChemistDetailsVC.swift
//  HealthApp
//
//  Created by Apple on 04/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class ChemistDetailsVC: BaseVC, SwiftAlertViewDelegate {
    
    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var lblHead: UILabel!
    
    var type: String?
    var chemistData: ChemistData?
    var favDoctorData: DoctorData?
    var doctorData : FavDoctor?
    var diagnosticData: DiagnosticData?
    var detailData: DetailData?
    var familyMemberData: FamilyMemberData?
    var id: String?
    var favorite: Bool?
    var ratingAlertView: SwiftAlertView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if type == "chemist" {
            
            self.lblHead.text = "CHEMIST DETAILS"
        }
        if type == "favchemist" {
            self.lblHead.text = "CHEMIST DETAILS"
        }
        if type == "doctor" {
            
            self.lblHead.text = "DOCTOR DETAILS"
        }
        if type == "favdoctor" {
            self.lblHead.text = "DOCTOR DETAILS"
        }
        if type == "diagnostic" {
            self.lblHead.text = "DIAGNOSTIC CENTER DETAILS"
        }
        if type == "favdiagnostic" {
            self.lblHead.text = "DIAGNOSTIC CENTER DETAILS"
        }
        if type == "familymember" {
            
            self.lblHead.text = "FAMILY MEMBER DETAILS"
        }
        
        if type == "familymember" {
            
            
        } else {
            self.getDetails()
        }
    }
    
    func getDetails(){
        
        IHProgressHUD.show()
        
        let params = ["id": id!] as [String : Any]
        apiManager.getDoctorDetails(params: params ) { (result) in
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
                
                self.detailData = data
                DispatchQueue.main.async {
                    
                    self.tblData.reloadData()
                }
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func addToFavorite() {
        
        let params = ["user_id": loggedUser?.id,
                      "chemist_doctor_dignosetic_id": detailData?.id] as [String : Any]
        
        IHProgressHUD.show()
        
        apiManager.addFavorite(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
                
                DispatchQueue.main.async {
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                    self.tblData.reloadData()
                }
            case .failure(let err):
                print("err: \(err)")
            }
        }
        
    }
    
    @objc func addRating() {
        
        /*
         user_id:1
         feedback:good service
         rating:4
         chemist_doctor_diagnostic_id:
         */
        
        
        
        let ratingView = ratingAlertView?.viewWithTag(1) as! CosmosView
        let rating = ratingView.rating
        
        //self.detailData?.avg_rating = rating
        
        let txtfeed = ratingAlertView?.viewWithTag(2) as! BottomLineTextField
        let feedback = txtfeed.text ?? ""
        
        if feedback.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter feedback")
            
        } else {
            let params = ["user_id": loggedUser?.id,
                          "feedback": feedback,
                          "rating": rating,
                          "chemist_doctor_diagnostic_id": detailData?.id] as [String : Any]
            
            
            
            IHProgressHUD.show()
            
            apiManager.addRating(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                
                switch result {
                case .success(let data):
                    
                    DispatchQueue.main.async {
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                        self.ratingAlertView?.dismiss()
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                    print("err: \(err)")
                }
            }
        }
    }
    
    @objc func dismissAlert() {
        
        ratingAlertView?.dismiss()
    }
    
    func initAlert() {
        
        let nib = Bundle.main.loadNibNamed("AddMedAlertView", owner: self, options: nil)
        let contentView = nib?[1] as! UIView
    
        ratingAlertView = SwiftAlertView(contentView: contentView, delegate: self, cancelButtonTitle: nil)
        
        ratingAlertView?.dismissOnOutsideClicked = true
        
        let ratingView = ratingAlertView?.viewWithTag(1) as! CosmosView
        ratingView.rating = 0//detailData?.avg_rating ?? 0
        
        let btnAdd = ratingAlertView?.viewWithTag(3) as! UIButton
        btnAdd.addTarget(self, action: #selector(addRating), for: .touchUpInside)
        
        let btnClose = ratingAlertView?.viewWithTag(4) as! UIButton
        btnClose.addTarget(self, action: #selector(dismissAlert), for: .touchUpInside)
    }
    
    
    @IBAction func addRatingAction(_ sender: UIButton) {
        
        self.initAlert()
        ratingAlertView?.show()
    }
    
    @IBAction func addFavAction(_ sender: UIButton) {
        
        self.addToFavorite()
    }
    
    @IBAction func editAction(_ sender: UIButton){
        
        let vc = self.getVC(with: "AddFamilyMemberVC", sb: IDMain) as! AddFamilyMemberVC
        vc.editFlag = true
        vc.requestData = (familyMemberData?.getRequestData())!
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func reportAction(_ sender: UIButton){
        //
        let vc = self.getVC(with: "ReportsVC", sb: IDMain) as! ReportsVC
        vc.type = "familyMember"
        vc.child_id = String(familyMemberData!.id!)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func deleteAction(_ sender: UIButton){
     
        self.alertAction(strTitle: "Remove Family Member", strMsg: "Are you sure to remove family member?", completion: {
            (action) in
            
            if action.title == "Yes" {
                self.deleteFamilyMember()
            }
        })
    }
    
    func deleteFamilyMember() {
        
        let params = ["user_id": loggedUser!.id,
                      "member_id": loggedUser!.id,
                      "family_member_id": familyMemberData!.id] as [String : Any]
        
        IHProgressHUD.show()
        
        apiManager.deleteFamilyMember(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
            case .success(let data):
                
                DispatchQueue.main.async {
                    
                    self.navigationController?.popViewController(animated: false)
                }
            case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension ChemistDetailsVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if type == "doctor" {
            return 9
        }
        if type == "favdoctor" {
            return 3
        }
        if type == "familymember" {
            return 8
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = ChemistDetailsCell()
        if type == "familymember" {
            
            switch indexPath.row {
                
            case 0:
                cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! ChemistDetailsCell
                
                if let strurl = familyMemberData?.profile_image, strurl != "null" {
                    let url = imgBaseURL + custProfileImg + strurl
                    cell.imgView.kf.setImage(with: URL(string: url))
                } else {
                    cell.imgView.image = UIImage(named: "User")
                }
                
                cell.lblHead.text = familyMemberData?.name
                cell.starView.isHidden = true
                cell.btnFav.isHidden = true
                cell.btnRating.isHidden = true
                
                return cell
                
            case 1:
                cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ChemistDetailsCell
                
                cell.lblHead.text = "NAME"
                   
                cell.lblMedName.text = familyMemberData?.name
                
                cell.btnFav.isHidden = false
                cell.btnRating.isHidden = false
                
                if let fam_relation = familyMemberData?.fam_relation, !fam_relation.isEmpty {
                
                    cell.btnRating.isHidden = true
                }
                /*
                if #available(iOS 13.0, *) {
                    cell.btnFav.setImage(UIImage(systemName: "delete"), for: .normal)
                } else {*/
                    // Fallback on earlier versions
                    cell.btnFav.setImage(UIImage(named: "Close"), for: .normal)
                //}
                
                cell.btnFav.addTarget(self, action: #selector(deleteAction(_:)), for: .touchUpInside)
                cell.btnRating.addTarget(self, action: #selector(editAction(_:)), for: .touchUpInside)
                
                return cell
                
            case 2:
                cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! ChemistDetailsCell
                
                let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
                let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
                let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
                let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
                
                lbl1.text = "MOBILE NO"
                if let mobile = familyMemberData?.mobile_no {
                    
                    lbl2.text = String(mobile)
                }
                lbl3.text = "GENDER"
                lbl4.text = familyMemberData?.gender
                
                return cell
                
            case 3:
                cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! ChemistDetailsCell
                
                let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
                let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
                let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
                let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
                
                lbl1.text = "DATE OF BIRTH"
                if let dob = familyMemberData?.dob {
                    
                    lbl2.text = dob
                }
                lbl3.text = "BLOOD GROUP"
                lbl4.text = familyMemberData?.bloodgroup
                
                return cell
                
            
                
            case 4:
                cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! ChemistDetailsCell
                
                let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
                let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
                let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
                let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
                
                lbl1.text = "HEIGHT"
                if let height = familyMemberData?.height, let height_unit =  familyMemberData?.height_unit {
                    
                    lbl2.text = height + " " + height_unit
                }
                lbl3.text = "WEIGHT"
                if let weight = familyMemberData?.weight, let weight_unit =  familyMemberData?.weight_unit {
                    
                    lbl4.text = weight + " " + weight_unit
                }
                return cell
                
            case 5:
                cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! ChemistDetailsCell
                
                let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
                let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
                let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
                let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
                
                lbl1.text = "EMAIL"
                if let email = familyMemberData?.email {
                    
                    lbl2.text = email
                }
                lbl3.text = "PROFESSION"
                if let profession = familyMemberData?.profession {
                    
                    lbl4.text = profession
                }
                return cell
                
            case 6:
                cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! ChemistDetailsCell
                
                let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
                let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
                let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
                let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
                
                lbl1.text = "STATE"
                if let state = familyMemberData?.state {
                    
                    lbl2.text = state
                }
                lbl3.text = "CITY"
                if let city = familyMemberData?.city {
                    
                    lbl4.text = city
                }
                return cell
                
            case 7:
                cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! ChemistDetailsCell
                //view reports action btnrating
                cell.btnRating.addTarget(self, action: #selector(reportAction), for: .touchUpInside)
                
                return cell
                
            default:
                break
            }
        } else {
            
            if type == "doctor" {
                switch indexPath.row {
                    
                case 0:
                    cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! ChemistDetailsCell
                    
                    if let strurl = detailData?.shop_image, !strurl.isEmpty, strurl != "null" {
                        let url = doctorImgUrl + strurl
                        cell.imgView.kf.setImage(with: URL(string: url))
                    } else {
                        cell.imgView.contentMode = .scaleAspectFit
                        cell.imgView.image = UIImage(named: "ic_doctor1")
                    }
                    cell.lblHead.text = detailData?.clinic_lab_hospital_name
                    
                    if let rating = detailData?.avg_rating {
                        cell.starView.rating = rating
                    }
                    if #available(iOS 13.0, *) {
                        //cell.btnFav.setImage(UIImage(systemName: "heart"), for: .normal)
                        cell.btnFav.setBackgroundImage(UIImage(systemName: "heart"), for: .normal)
                    } else {
                        // Fallback on earlier versions
                    }
                    if let fav = favorite, fav {
                        
                        if #available(iOS 13.0, *) {
                            cell.btnFav.setBackgroundImage(UIImage(systemName: "heart.fill"), for: .normal)
                        } else {
                            // Fallback on earlier versions
                        }
                        cell.btnFav.tintColor = .red
                    }
                    cell.btnFav.addTarget(self, action: #selector(addFavAction), for: .touchUpInside)
                    
                    cell.btnRating.addTarget(self, action: #selector(addRatingAction), for: .touchUpInside)
                    
                    return cell
                    
                case 1:
                    cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! ChemistDetailsCell
                    
                    let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
                    let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
                    let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
                    let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
                    
                    
                    lbl1.text = "DOCTOR'S NAME"
                    lbl2.text = detailData?.name
                    
                    lbl3.text = "SPECIALIZATION"
                    lbl4.text = detailData?.specialization
                    
                    return cell
                    
                case 2:
                    cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ChemistDetailsCell
                       
                    cell.lblHead.text = "EMAIL"
                    cell.lblMedName.text = detailData?.email
                    
                    return cell
                    
                case 3:
                    cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! ChemistDetailsCell
                    
                    let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
                    let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
                    let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
                    let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
                    
                    
                    lbl1.text = "ASSISTANT NAME"
                    lbl2.text = detailData?.assistant_name
                    
                    lbl3.text = "ASSISTANT CONTACT NO."
                    lbl4.text = detailData?.alt_contact_no
                    
                    return cell
                    
                case 4:
                    cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! ChemistDetailsCell
                    
                    let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
                    let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
                    let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
                    let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
                    
                    
                    lbl1.text = "MCI REGISTRATION NUMBER"
                    lbl2.text = detailData?.registration_no
                    
                    lbl3.text = "GST NO."
                    lbl4.text = detailData?.gst_no
                    
                    return cell
                    
                case 5:
                    cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! ChemistDetailsCell
                    
                    let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
                    let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
                    let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
                    let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
                    
                    
                    lbl1.text = "SHOP TIMING"
                    if let shop_open = detailData?.shop_opening_time, let shop_close = detailData?.shop_closing_time {
                        
                        lbl2.text = String(shop_open) + "-" + String(shop_close)
                    }
                    
                    lbl3.text = "OFF DAY"
                    lbl4.text = detailData?.off_day
                    
                    return cell
                    
                case 6:
                    cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ChemistDetailsCell
                    
                    cell.lblHead.text = "ADDRESS"
                    cell.lblMedName.text = detailData?.address
                    
                    return cell
                    
                case 7:
                    cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! ChemistDetailsCell
                    
                    let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
                    let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
                    let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
                    let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
                    
                    
                    lbl1.text = "STATE"
                    lbl2.text = detailData?.state
                    
                    lbl3.text = "CITY"
                    lbl4.text = detailData?.city
                    
                    return cell
                    
                case 8:
                    cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ChemistDetailsCell
                    
                    cell.lblHead.text = "ABOUT CLINIC"
                    cell.lblMedName.text = detailData?.about_clinic
                    
                    return cell
                    
                default:
                    break
                }
            } else {
                switch indexPath.row {
                    
                case 0:
                    cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! ChemistDetailsCell
                    
                    if type == "chemist" {
                        
                        if let strurl = detailData?.shop_image {
                            let url = chemistProfileUrl + strurl
                            cell.imgView.kf.setImage(with: URL(string: url))
                        }
                        
                    } else {
                        if let strurl = detailData?.shop_image {
                            let url = doctorImgUrl + strurl
                            cell.imgView.kf.setImage(with: URL(string: url))
                        }
                    }
                    cell.lblHead.text = detailData?.clinic_lab_hospital_name
                    
                    if let rating = detailData?.avg_rating {
                        cell.starView.rating = rating
                    }
                    if #available(iOS 13.0, *) {
                        cell.btnFav.setImage(UIImage(systemName: "heart"), for: .normal)
                    } else {
                        // Fallback on earlier versions
                    }
                    if let fav = favorite, fav {
                        
                        if #available(iOS 13.0, *) {
                            cell.btnFav.setImage(UIImage(systemName: "heart.fill"), for: .normal)
                        } else {
                            // Fallback on earlier versions
                        }
                        cell.btnFav.tintColor = .red
                    }
                    cell.btnFav.addTarget(self, action: #selector(addFavAction), for: .touchUpInside)
                    
                    cell.btnRating.addTarget(self, action: #selector(addRatingAction), for: .touchUpInside)
                    
                    return cell
                    
                case 1:
                    cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ChemistDetailsCell
                    
                    if type == "doctor" {
                        
                        cell.lblHead.text = "Doctor's Name"
                        
                    } else if type == "favdoctor" {
                        
                        cell.lblHead.text = "Doctor's Name"
                        
                    }
                    else if type == "diagnostic" {
                        
                        cell.lblHead.text = "Diagnostic Name"
                    }
                    cell.lblMedName.text = detailData?.name
                    return cell
                    
                case 2:
                    cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ChemistDetailsCell
                    
                    if type == "doctor" || type == "favdoctor"{
                        cell.lblHead.text = "Specialization"
                        cell.lblMedName.text = detailData?.specialization
                    } else if type == "diagnostic" {
                        
                        cell.lblHead.text = "Diagnostic Name"
                        cell.lblMedName.text = detailData?.name
                    }
                    return cell
                    
                case 3:
                    cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! ChemistDetailsCell
                    
                    let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
                    let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
                    let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
                    let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
                    
                    
                    
                    if let mobile = detailData?.mobile_no {
                        lbl1.text = "Mobile No."
                        lbl2.text = String(mobile)
                    }
                    lbl3.text = "Email"
                    lbl4.text = detailData?.email
                    
                    
                    
                    return cell
                    
                case 4:
                    cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! ChemistDetailsCell
                    
                    let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
                    let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
                    let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
                    let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
                    
                    
                    if let regno = detailData?.registration_no {
                        lbl1.text = "Registration NO"
                        lbl2.text = regno
                    }
                    
                    if let gst = detailData?.gst_no {
                        lbl3.text = "GST NO"
                        lbl4.text = gst
                    }
                    
                    return cell
                    
                case 5:
                    cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! ChemistDetailsCell
                    
                    let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
                    let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
                    let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
                    let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
                    
                    if let shop_open = detailData?.shop_opening_time, let shop_close = detailData?.shop_closing_time {
                        lbl1.text = "Shop Timing"
                        lbl2.text = String(shop_open) + "-" + String(shop_close)
                    }
                    lbl3.text = "Off Day"
                    lbl4.text = detailData?.off_day
                    
                    
                    return cell
                    
                case 6:
                    cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ChemistDetailsCell
                    
                    
                    cell.lblHead.text = "Address"
                    cell.lblMedName.text = detailData?.address
                    
                    
                    return cell
                    
                case 7:
                    cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! ChemistDetailsCell
                    
                    let lbl1 = cell.contentView.viewWithTag(1) as! UILabel
                    let lbl2 = cell.contentView.viewWithTag(2) as! UILabel
                    let lbl3 = cell.contentView.viewWithTag(3) as! UILabel
                    let lbl4 = cell.contentView.viewWithTag(4) as! UILabel
                    
                    if let state = detailData?.state {
                        lbl1.text = "State"
                        lbl2.text = state
                    }
                    if let city = detailData?.city {
                        lbl3.text = "City"
                        lbl4.text = city
                    }
                    return cell
                    
                case 8:
                    cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! ChemistDetailsCell
                    
                    cell.lblHead.text = "About"
                    cell.lblMedName.text = detailData?.about_clinic
                    
                    return cell
                    
                default:
                    break
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if type == "doctor" {
            if indexPath.row == 0 {
                return 300
            } else {
                if indexPath.row == 3 {
                    return detailData?.assistant_name == nil ? 0 : UITableView.automaticDimension
                }
                return UITableView.automaticDimension
            }
            
        } else {
            if indexPath.row == 0 {
                return 300
            }
            if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 6 || indexPath.row == 8 {
                return 65
            }
            
            if indexPath.row == 5 {
                return UITableView.automaticDimension
            }
        }
        return 100
    }
}

class ChemistDetailsCell: BaseCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblMedName: UILabel!
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var starView: CosmosView!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btnRating: UIButton!
    
    
}
