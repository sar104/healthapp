//
//  AddDoctorAlertVC.swift
//  HealthApp
//
//  Created by Apple on 29/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class AddDoctorAlertVC: BaseVC, AddDoctorAlertCellDelegate {

    @IBOutlet weak var tblData: UITableView!
    
    var medAlertData: MedAlertData = MedAlertData()
    var alertTiming = AlertTiming()
    var docArr:[DoctorData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getDoctors()
    }
    
    func onValueChanged(id: String, value: String){
        
        if id == "date" {
            alertTiming.time_category = value
        }
        if id == "time" {
            alertTiming.timming = value
        }
        if id == "reason" {
            medAlertData.visit_reason = value
        }
        if id == "address" {
            medAlertData.doctor_address = value
        }
    }
    
    func onDataSelected(id: String, value: Any){
        medAlertData.medicine_name = (value as! DoctorData).user_name
        
    }
    func onSaveClicked() {
        
        if validate() {
            setNotifications()
            addDoctorAlert()
        }
    }
    
    func getDoctors() {
        
        let params: [String:Any] = ["user_id": loggedUser!.id]
        
        IHProgressHUD.show()
        apiManager.getMyDoctors(params: params) { (result) in
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    self.docArr.removeAll()
                    self.docArr = data
                    
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func validate() -> Bool {
        
        
        if medAlertData.medicine_name == nil || medAlertData.medicine_name!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please select doctor")
            return false
        }
        if medAlertData.visit_reason == nil || medAlertData.visit_reason!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please enter visit reason")
            return false
        }
        if medAlertData.doctor_address == nil || medAlertData.doctor_address!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please enter doctor address")
            return false
        }
        if alertTiming.time_category == nil || alertTiming.time_category!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please select date")
            return false
        }
        if alertTiming.timming == nil || alertTiming.timming!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please select time")
            return false
        }
        return true
    }
    
    func setNotifications() {
        
        var arrNotif: [LocalNotification] = []
        let manager = LocalNotificationManager()
        let notifId = kAlertTypeDoc + "_" + medAlertData.medicine_name! + "_" + alertTiming.time_category!  + "_" + alertTiming.timming!
        
        let date = alertTiming.time_category?.toDate(format: df_dd_MM_yyyy)
        let strTime = alertTiming.timming?.toDate(format: "HH:mm")
        
        let title = docAlertTitle.replacingOccurrences(of: "#", with: medAlertData.medicine_name!).replacingOccurrences(of: "@", with: alertTiming.timming!)
        
        let notif =
            LocalNotification(id: notifId, title: title,
                              datetime: DateComponents(calendar: Calendar.current,
                                                       year: date!.get(.year), month: date!.get(.month),
                                                       day:date!.get(.day),
                                                       hour: strTime?.get(.hour), minute: strTime?.get(.minute)))
                
        arrNotif.append(notif)
        
        let components = DateComponents(calendar: Calendar.current, year: date?.get(.year),
                                       month: date?.get(.month), day: date?.get(.day),
                                       hour: strTime?.get(.hour), minute: strTime?.get(.minute))
        let alertdate = Calendar.current.date(from: components)
        
        let notifId1 = kAlertTypeDoc + "_" + medAlertData.medicine_name! + "_" +
            alertTiming.time_category!  + "_" + alertTiming.timming! + "_24"
        
        
        let alertdate_24 = Calendar.current.date(byAdding: .hour, value: -24, to: alertdate!)
        
        let hour_24 = alertdate_24!.get(.hour)
        
        let title_24 = docAlertTitle_24.replacingOccurrences(of: "#", with: medAlertData.medicine_name!).replacingOccurrences(of: "@", with: alertTiming.timming!)
        
        let notif1 =
            LocalNotification(id: notifId1, title: title_24,
                              datetime: DateComponents(calendar: Calendar.current,
                                                       year: alertdate_24!.get(.year), month: alertdate_24!.get(.month),
                                                       day:alertdate_24!.get(.day),
                                                       hour: hour_24, minute: alertdate_24?.get(.minute)))
        
        arrNotif.append(notif1)
        
        let notifId2 = kAlertTypeDoc + "_" + medAlertData.medicine_name! + "_" +
            alertTiming.time_category!  + "_" + alertTiming.timming! + "_4"
        
        let alertdate_4 = Calendar.current.date(byAdding: .hour, value: -4, to: alertdate!)
        
        let hour_4 = alertdate_4!.get(.hour)
        let title_4 = docAlertTitle_4.replacingOccurrences(of: "#", with: medAlertData.medicine_name!)
        
        let notif2 =
            LocalNotification(id: notifId2, title: title_4,
                              datetime: DateComponents(calendar: Calendar.current,
                                                       year: alertdate_4!.get(.year), month: alertdate_4!.get(.month),
                                                       day:alertdate_4!.get(.day),
                                                       hour: hour_4, minute: alertdate_4?.get(.minute)))
        
        arrNotif.append(notif2)
        
        let notifId3 = kAlertTypeDoc + "_" + medAlertData.medicine_name! + "_" +
        alertTiming.time_category!  + "_" + alertTiming.timming! + "_2"
        
        let alertdate_2 = Calendar.current.date(byAdding: .hour, value: -2, to: alertdate!)
        
        let hour_2 = alertdate_2!.get(.hour)
        
        let title_2 = docAlertTitle_2.replacingOccurrences(of: "#", with: medAlertData.medicine_name!)
        
        let notif3 =
            LocalNotification(id: notifId3, title: title_2,
                              datetime: DateComponents(calendar: Calendar.current,
                                                       year: alertdate_2!.get(.year), month: alertdate_2!.get(.month),
                                                       day:alertdate_2!.get(.day),
                                                       hour: hour_2, minute: alertdate_2?.get(.minute)))
        
        arrNotif.append(notif3)
        
        print("notification\(arrNotif)")
        manager.notifications = arrNotif
        manager.schedule()
    }

    func addDoctorAlert() {
        
        /*
         user_id=405&
         medicine_name=Dr. akash&
         medicine_unit={"visit_reason":"general visit","doctor_address":"nashik"}&
         alert_timmings=[
         {"time_category":"before 24","timming":"15:00","time_in_miles":"1598434242782"},
         {"time_category":"before 4","timming":"11:00","time_in_miles":"1598506242782"},
         {"time_category":"before 2","timming":"13:00","time_in_miles":"1598513442782"},
         {"time_category":"27-08-2020","timming":"15:00","time_in_miles":"1598520642782"}
         ]&
         alert_type=doctor_visits
         **/
        do {
            var params: [String:Any] = [:]
            let med_unit = ["visit_reason": medAlertData.visit_reason,"doctor_address": medAlertData.doctor_address]
            let medunitData = try JSONSerialization.data(withJSONObject: med_unit, options: [])
            let jsonStrMedUnit = String(data: medunitData, encoding: .utf8)
            
            let mills = alertTiming.timming?.toDate(format: "HH:mm").convertToMilli()
            let alert_Timing = [["time_category": alertTiming.time_category,"timming": alertTiming.timming,"time_in_miles": mills]]
            let alertTimingData = try JSONSerialization.data(withJSONObject: alert_Timing, options: [])
            let jsonStrAlertTiming = String(data: alertTimingData, encoding: .utf8)
            
            params["user_id"] = loggedUser?.id
            params["medicine_name"] = medAlertData.medicine_name
            params["medicine_unit"] = jsonStrMedUnit
            params["alert_type"] = kAlertTypeDoc
            params["alert_timmings"] = jsonStrAlertTiming
            
            IHProgressHUD.show()
            
            apiManager.addMedicineAlert(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.navigationController?.popViewController(animated: false)
                    }
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
            
        } catch let err {
            print(err)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddDoctorAlertVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = AddDoctorAlertCell()
        
        if indexPath.row == 0 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! AddDoctorAlertCell
            cell.delegate = self
            cell.pickerDataArr = docArr
            cell.setup(id: "drname")
            
            return cell
        }
        if indexPath.row == 1 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! AddDoctorAlertCell
            cell.delegate = self
            cell.setup(id: "reason")
            
            return cell
        }
        if indexPath.row == 2 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! AddDoctorAlertCell
            cell.delegate = self
            cell.setup(id: "address")
            
            return cell
        }
        if indexPath.row == 3 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "dateTimeCell") as! AddDoctorAlertCell
            cell.delegate = self
            
            cell.setup(id: "date")
            cell.setup(id: "time")
            
            return cell
        }
        if indexPath.row == 4 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "btnAlertCell") as! AddDoctorAlertCell
            cell.delegate = self
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        
        if cell.reuseIdentifier == "dateTimeCell" {
            
            (cell as! AddDoctorAlertCell).txtFld.layer.borderColor = UIColor.red.cgColor
            (cell as! AddDoctorAlertCell).txtFld1.layer.borderColor = UIColor.red.cgColor
            
            (cell as! AddDoctorAlertCell).txtFld.layer.borderWidth = 1.0
            (cell as! AddDoctorAlertCell).txtFld1.layer.borderWidth = 1.0
            
            (cell as! AddDoctorAlertCell).txtFld.layer.cornerRadius = 4.0
            (cell as! AddDoctorAlertCell).txtFld1.layer.cornerRadius = 4.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 3 {
            return 180
        }
        return 50
    }
    
}

protocol AddDoctorAlertCellDelegate {
    
    func onValueChanged(id: String, value: String)
    func onDataSelected(id: String, value: Any)
    func onSaveClicked()
}
class AddDoctorAlertCell: BaseCell {
    
    @IBOutlet weak var txtFld: RightViewTextField!
    @IBOutlet weak var txtName: BottomLineTextField!
    @IBOutlet weak var txtFld1: RightViewTextField!
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.minimumDate = Date()
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    lazy var timePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        //picker.minimumDate = Date()
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    var seldate: String?
    var seltime: String?
    var delegate: AddDoctorAlertCellDelegate?
    var selRow: Int = 0
    
    func setup(id: String) {
        
        if id == "drname" {
            
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            txtFld.placeholder = "Doctor Name"
            txtFld.inputView = self.cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
        }
        if id == "reason" {
            txtName.accessibilityIdentifier = id
            txtName.delegate = self
            txtName.placeholder = "Reason to visit"
            txtName.removeBottomBorder()
            txtName.addBottomBorder()
        }
        if id == "address" {
            txtName.accessibilityIdentifier = id
            txtName.delegate = self
            txtName.placeholder = "Doctor address"
            txtName.removeBottomBorder()
            txtName.addBottomBorder()
        }
        if id == "date" {
            txtFld.inputView = datePicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
        if id == "time" {
            
            txtFld1.inputView = timePicker
            txtFld1.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        selRow = row
        let data = pickerDataArr[row] as? DoctorData
        txtFld.text = data?.user_name
    }
    
    @objc override func doneCellPicker (sender: UIBarButtonItem) {
        
        let id = sender.accessibilityIdentifier
        
        if id == "drname" {
            let data = pickerDataArr[selRow] as? DoctorData
            txtFld.text = data?.user_name
            delegate?.onDataSelected(id: id!, value: data!)
            txtFld.resignFirstResponder()
        }
        
        if id == "date" {
            seldate = datePicker.date.toString(format: df_dd_MM_yyyy)
            txtFld.text = seldate!
            delegate?.onValueChanged(id: id!, value: seldate!)
            txtFld.resignFirstResponder()
        }
        if id == "time" {
            seltime = timePicker.date.toString(format: "HH:mm")
            txtFld1.text = seltime!
            delegate?.onValueChanged(id: id!, value: seltime!)
            txtFld1.resignFirstResponder()
        }
        
    }
    @objc func dateSelected(_ picker: UIDatePicker){
        
        let dateStr = picker.date.toString(format: df_dd_MM_yyyy)//df.string(from: picker.date)
        
        txtFld.text = dateStr
        
        let date = picker.date.toString(format: "YYYY-MM-dd")
        seldate = date
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        delegate?.onSaveClicked()
    }
}
extension AddDoctorAlertCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        /*
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(valueChanged),
            object: textField)*/
        self.perform(
            #selector(valueChanged),
            with: textField,
            afterDelay: 0.8)
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @objc func valueChanged(textField: UITextField) {
        
        delegate?.onValueChanged(id: textField.accessibilityIdentifier!, value: textField.text!)
    }
}
