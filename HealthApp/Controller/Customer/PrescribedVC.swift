//
//  PrescribedVC.swift
//  HealthApp
//
//  Created by Apple on 15/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import RadioGroup
import IHProgressHUD
import SwiftyJSON

class PrescribedVC: BaseVC, PrescribedCellDelegate, SwiftAlertViewDelegate {
    
    @IBOutlet weak var tblData: UITableView!
    var addMedicineAlertView: SwiftAlertView!
    
    var selPrescFlag: Bool = false
    var selPrescription: PrescriptionData = PrescriptionData()
    var chemistArr: [ChemistData] = []
    var allMedicineArr: [MedicineData] = []
    var medicineArr: [MedicineData] = []
    var selectedMedArr: [MedicineData] = []
    var selNonPreMedArr: [MedicineData] = []
    var medObj = MedicineData()
    var nonMedObj = MedicineData()
    var selChemistData: ChemistData?
    var prescriptionImg:Data?
    var orderData: MedicineOrderData = MedicineOrderData()
    var slotArr: [SlotData] = []
    var familyMembersArr: [FamilyMemberData] = []
    var selFamilyMemberData: FamilyMemberData = FamilyMemberData()
    var recType: String = "Self"
    
    let recTypeArr = ["Self", "Family"]
    let deliveryTypeArr = ["Delivery","Pickup"]
    var user: User?
    let imgPicker = UIImagePickerController()
    var uploadNewFlag: Bool = false
    var verifiedFlag: Bool = false
    var orderTypeNP: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(segmentChanged(_:)), name: NSNotification.Name("OrderSegmentDidChangeNotification"), object: nil)

        let medArr = CoreDataManager.shared.fetchAllMedicines()
        
        for data in medArr {
            
            allMedicineArr.append(MedicineData(coreDataMed: data))
            
        }
        
        let users = self.fetchUser()
        user = users[0]
        
        // Do any additional setup after loading the view.
        /*
        let selMedicines = selPrescription.getDrugsArr()
        if selMedicines.count > 0 {
            selectedMedArr.append(contentsOf: selMedicines)
        }*/
        
        self.orderData.user_id = Int(user!.id)
        self.orderData.delivery_type = deliveryTypeArr[0]
        self.orderData.prescription_type = "Prescribed"
        let recipientData = RecipientData(rec_id: Int(user!.id), rec_name: user?.name, rec_mobile: user?.mobile_no)
        self.orderData.recipient_name = recipientData
        
        //self.tblData.reloadData()
        if orderData.doctor_name != nil || selPrescription.getDrugsArr().count > 0 {
            self.onSelectPrescription(orderData: orderData, preData: selPrescription)
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tblData.reloadData()
    }
    
    @objc func segmentChanged(_ notification: NSNotification){
        
        if let index = notification.object as? Int {
            print("index = \(index)")
            if index == 1 {
                orderTypeNP = true
                self.orderData.prescription_type = "Non Prescribed"
            } else {
                orderTypeNP = false
            }
            
            self.tblData.reloadData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getFamilyMembers()
        self.getChemistList()
        self.getSlotList()
        //self.getMedicines(name: "para")
    }
    
    func getChemistList() {
        
        let lat = defaults.value(forKey: kUserLat) as? CGFloat ?? 0.0
        let lon = defaults.value(forKey: kUserLong) as? CGFloat ?? 0.0
        
        /*
        let params = [PARAM_USERID: 401,
        PARAM_LATITUDE: 19.980661,
        PARAM_LONGITUDE: 73.797836] as [String:Any]
         */
        
        let params = [PARAM_USERID: loggedUser!.id,
        PARAM_LATITUDE: lat,
        PARAM_LONGITUDE: lon] as [String:Any]
        
        IHProgressHUD.show()
        
        apiManager.searchChemists(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    self.chemistArr.removeAll()
                    self.chemistArr = data
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func getSlotList() {
        
        /*
        IHProgressHUD.show()
        
        apiManager.getTimeSlots(params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    self.slotArr.removeAll()
                    self.slotArr = data.filter{ ( !$0.isEarlier()) }
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }*/
        
        let time1 = "09:00 AM To 12:00 PM"
        let slot1 = SlotData(id: 1, slot: time1)
        let time2 = "01:00 PM To 3:00 PM"
        let slot2 = SlotData(id: 2, slot: time2)
        let time3 = "03:00 PM To 5:00 PM"
        let slot3 = SlotData(id: 3, slot: time3)
        let time4 = "05:00 PM To 7:00 PM"
        let slot4 = SlotData(id: 4, slot: time4)
        
        if !slot1.isEarlierSlot() {
            self.slotArr.append(slot1)
        }
        if !slot2.isEarlierSlot() {
            self.slotArr.append(slot2)
        }
        if !slot3.isEarlierSlot() {
            self.slotArr.append(slot3)
        }
        if !slot4.isEarlierSlot() {
            self.slotArr.append(slot4)
        }
        
    }
    
    func getFamilyMembers() {
        
        IHProgressHUD.show()
        
        let params = ["user_id": user?.id] as [String: Any]
        apiManager.getFamilyMember(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    
                    self.familyMembersArr = memberData
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    
    
    func uploadPrescription(){
        
        IHProgressHUD.show()
        apiManager.uploadImg(image: prescriptionImg!, name: "image", api: PrescriptionImgAPI) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let imgdata):
                    
                    let imgstr = String(data: imgdata, encoding: .utf8)
                    print(imgstr)
                        
                        if((imgstr?.contains("jpeg"))!){
                            self.orderData.prescription_image = imgstr
                            if !self.uploadNewFlag {
                                self.addOrder()
                            } else {
                                self.processNPM()
                            }
                        }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }

    func initAlert(medtype: Int) {
    
        addMedicineAlertView = SwiftAlertView(nibName: "AddMedicineAlertView", delegate: self, cancelButtonTitle: nil, otherButtonTitles: nil)
        addMedicineAlertView.dismissOnOtherButtonClicked = true
        addMedicineAlertView.dismissOnOutsideClicked = true
        addMedicineAlertView.tag = 1
        
        let txtMedName = addMedicineAlertView.viewWithTag(2) as! SearchTextField
        
        txtMedName.startVisible = true
        
        
        if medtype == 1 {
            txtMedName.placeholder = "Medicine/item name"
        }
        
        let txtType = addMedicineAlertView.viewWithTag(3) as! RightViewTextField
        let stepper = addMedicineAlertView.viewWithTag(4) as! UIStepperController
        stepper.delegate = self
        stepper.tag = medtype
        stepper.count = 1
        
        
        
        txtMedName.addBottomBorder()
        
        txtMedName.userStoppedTypingHandler = {
            if let criteria = txtMedName.text {
                if criteria.count > 2 {

                    // Show the loading indicator
                    
                    txtMedName.showLoadingIndicator()
                    self.medicineArr = self.allMedicineArr.filter({ (data) -> Bool in
                        return data.medicine_name!.range(of: criteria) != nil
                    })
                    let medNames:[String] = self.medicineArr.map( {($0.medicine_name ?? "")} )
                    if medNames.count > 0 {
                        txtMedName.filterStrings(medNames)
                    } else {
                        txtType.text = "other"
                        if medtype == 0 {
                            self.medObj.medicine_name = criteria
                            self.medObj.quantity = Int(stepper.count)
                            self.medObj.type = "PM"
                            self.medObj.type_of_medicine = "other"
                        } else {
                            self.nonMedObj.medicine_name = criteria
                            self.nonMedObj.quantity = Int(stepper.count)
                            self.nonMedObj.type = "NPM"
                            self.nonMedObj.type_of_medicine = "other"
                        }
                    }
                    txtMedName.stopLoadingIndicator()
                        
                   
                }
            }
        }
        txtType.addBottomBorder()
        
        //let medNames:[String] = self.medicineArr.map( {($0.medicine_name ?? "")} )
        //txtMedName.filterStrings(medNames)//(["asdds","dsssfd","weew","bvmbvb"])
            txtMedName.itemSelectionHandler = {filteredResults, itemPosition in
            let item = filteredResults[itemPosition]
                txtMedName.text = item.title
                let meddata = self.medicineArr.filter{ $0.medicine_name == item.title}.first
                
                if meddata != nil {
                    let type = meddata?.type_of_medicine
                    txtType.text = type
                    if medtype == 0 {
                        self.medObj = meddata!
                        self.medObj.quantity = Int(stepper.count)
                        self.medObj.type = "PM"
                    } else {
                        self.nonMedObj = meddata!
                        self.nonMedObj.quantity = Int(stepper.count)
                        self.nonMedObj.type = "NPM"
                    }
                } else {
                    txtType.text = "other"
                    if medtype == 0 {
                        self.medObj.medicine_name = item.title
                        self.medObj.quantity = Int(stepper.count)
                        self.medObj.type = "PM"
                        self.medObj.type_of_medicine = "other"
                    } else {
                        self.nonMedObj.medicine_name = item.title
                        self.nonMedObj.quantity = Int(stepper.count)
                        self.nonMedObj.type = "NPM"
                        self.nonMedObj.type_of_medicine = "other"
                    }
                }
        }
        
        //txtMedName.delegate = self
        //txtType.delegate = self
        //let qtyBtn = addMedicineAlertView.viewWithTag(4) as! UIStepperController
        let addBtn = addMedicineAlertView.viewWithTag(5) as! UIButton
        let closeBtn = addMedicineAlertView.viewWithTag(6) as! UIButton
        
        closeBtn.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        
        if medtype == 0 {
            addBtn.addTarget(self, action: #selector(addMedicineAction), for: .touchUpInside)
        } else {
            addBtn.addTarget(self, action: #selector(addNonMedicineAction), for: .touchUpInside)
        }
    }
    
    @objc func addMedicineAction(){
        
        if medObj.medicine_name == nil {
            
            self.alert(strTitle: strErr, strMsg: "Please select/enter medicine name ")
            
        } else {
            self.selectedMedArr.append(medObj)
            print("qty: \(medObj.quantity)")
            
            addMedicineAlertView.dismiss()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.tblData.reloadData()
            }
        }
    }
    
    
    @objc func addNonMedicineAction(){
        
        
        self.selNonPreMedArr.append(nonMedObj)
        print("qty: \(medObj.quantity)")
        
        addMedicineAlertView.dismiss()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.tblData.reloadData()
        }
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
     
        addMedicineAlertView.dismiss()
    }
    
    @objc func dismissAlert(){
        addMedicineAlertView.dismiss()
    }
    
    // MARK: PrescribedCellDelegate
    
    func onValueSelected(type: String, value: String) {
        
        switch type {
        case "date":
            self.orderData.estimated_date = value
            
        case "deliveryYype":
            let index = Int(value)
            self.orderData.delivery_type = deliveryTypeArr[index!]
            
            let indx1 = IndexPath(row: 3, section: 3)
            let indx2 = IndexPath(row: 4, section: 3)
            let indx3 = IndexPath(row: 5, section: 3)
            
            self.tblData.reloadRows(at: [indx1,indx2,indx3], with: .none)
            
        case "radio":
            let index = Int(value)
            recType = recTypeArr[index!]
            
        case "recipient":
            self.orderData.recipient_name = RecipientData(rec_id: Int(user!.id), rec_name: value, rec_mobile: user?.mobile_no)
            
        case "doctor":
            self.orderData.doctor_name = value
            
        case "flat":
            self.orderData.flat_house_no = value
            
        case "address":
            self.orderData.customer_address = value
            
        case "pincode":
            self.orderData.pincode = value
            
        case "checkbox":
            self.verifiedFlag = Bool(value)!
            print("chk: \(value), \(self.verifiedFlag)")
            
        default:
            break
        }
        
        //self.tblData.reloadData()
    }
    
    func onDataSelected(type: String, value: Any) {
        
        switch type {
        case "chemist":
            let data = value as! ChemistData
            self.orderData.chemist_id = data.id
            
        case "time":
            let data = value as! SlotData
            self.orderData.slot = data.slot
            
        case "recipient_family":
            self.selFamilyMemberData = value as! FamilyMemberData
            
        default:
            break
        }
    }
    
    func addOrder() {
        
        IHProgressHUD.show()
        
        var drugArr: [DrugData] = []
        /*
        let drugArrPM = MedicineData.getDrugsData(self.selectedMedArr)
        drugArr.append(contentsOf: drugArrPM)
        let drugArrNPM = MedicineData.getDrugsData(self.selNonPreMedArr)
        drugArr.append(contentsOf: drugArrNPM)
        */
        //self.orderData.drug_list = drugArr
        
        do {
            let arrDrugs = NSMutableArray.init()
            
             if selectedMedArr.count > 0 {
                 
                 for med in selectedMedArr {
                     /*
                     let meddata = [ "medicine_name":med.medicine_name,"suggested_name": "", "medicine_type": med.type,"quantity" :med.quantity,"unit" :med.type_of_medicine!] as [String : Any]
                     arrDrugs.add(meddata)*/
                    
                    var medname = MedicineName()
                    medname.medicine_name = med.medicine_name
                    medname.medicine_type = med.type
                    medname.suggested_name = ""
                    
                    let mednameJSONData = try JSONEncoder().encode(medname)
                    let jsonStrMedname = String(data: mednameJSONData, encoding: .utf8)
                    //["medicine_name":med.medicine_name,"suggested_name": "", "medicine_type": med.type]
                    
                    let meddata = ["medicine_name":jsonStrMedname,"quantity" :med.quantity,"unit" :med.type_of_medicine!] as [String : Any]
                    arrDrugs.add(meddata)
                    
                    var  drugdata = DrugData()
                    
                    drugdata.medicine_name = medname
                    drugdata.unit = med.type_of_medicine!
                    drugdata.quantity = med.quantity
                    drugArr.append(drugdata)
                 }
                 print("drugs: \(arrDrugs)")
             }
            if selNonPreMedArr.count > 0 {
                
                for med in selNonPreMedArr {
                    /*
                    let meddata = [ "medicine_name":med.medicine_name,"suggested_name": "", "medicine_type": med.type,"quantity" :med.quantity,"unit" :med.type_of_medicine!] as [String : Any]
                    arrDrugs.add(meddata)*/
                    
                    var medname = MedicineName()
                    medname.medicine_name = med.medicine_name
                    medname.medicine_type = med.type
                    medname.suggested_name = ""
                    
                    let mednameJSONData = try JSONEncoder().encode(medname)
                    let jsonStrMedname = String(data: mednameJSONData, encoding: .utf8)
                    //["medicine_name":med.medicine_name,"suggested_name": "", "medicine_type": med.type]
                    
                    let meddata = ["medicine_name":jsonStrMedname,"quantity" :med.quantity,"unit" :med.type_of_medicine!] as [String : Any]
                    arrDrugs.add(meddata)
                    
                    var  drugdata = DrugData()
                    
                    drugdata.medicine_name = medname
                    drugdata.unit = med.type_of_medicine!
                    drugdata.quantity = med.quantity
                    drugArr.append(drugdata)
                }
                print("drugs: \(arrDrugs)")
            }
             
             //let jsonDrugs = JSON(arrDrugs)
             //let strDrugs = jsonDrugs.rawString()!
            /*
            let drugJSonData = try JSONSerialization.data(withJSONObject: arrDrugs, options: [])
            let jsonStrDrugs = String(data: drugJSonData, encoding: .utf8)
            */
            /*
            let jsonDrugs = JSON(arrDrugs)
            let jsonStrDrugs = jsonDrugs.rawString()!
            */
            let drugJSonData = try JSONEncoder().encode(drugArr)
            let jsonStrDrugs = String(data: drugJSonData, encoding: .utf8)
            
            let paramsData = try JSONEncoder().encode(self.orderData)
            
            guard var params = try? JSONSerialization.jsonObject(with: paramsData, options: []) as? [String:Any]
                else { return }
            
            params["drug_list"] = jsonStrDrugs
            
            let dataRec = try JSONEncoder().encode(self.orderData.recipient_name)
            let paramsRec =  String(data: dataRec, encoding: .utf8)
                //try? JSONSerialization.jsonObject(with: dataRec, options: []) as? [String:Any]
            
            params["recipient_name"] = paramsRec
            print("params: \(params)")
            
            apiManager.AddOrderForCustomer(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.navOrders()
                        }
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
        } catch let err {
            print(err)
        }
        
        
    }
    
    func navOrders() {
        
        //let dashboardVC = (self.navigationController?.viewControllers.first) as! DashboardVC
        let dashboardVC = self.getVC(with: "DashboardVC", sb: "Main") as! DashboardVC
        let ordersVC = self.getVC(with: "OrdersVC", sb: IDMain) as! OrdersVC
        
        let vcArray = [dashboardVC, ordersVC]//self.navigationController?.viewControllers
        //vcArray!.removeLast()
        //vcArray!.append(ordersVC)
        self.navigationController?.setViewControllers(vcArray, animated: false)
    }
    
    func validate() ->  Bool {
        
        if self.orderData.doctor_name == nil ||  self.orderData.doctor_name!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please enter doctor name")
            return false
        }
        if self.orderData.prescription_image == nil ||  self.orderData.prescription_image!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please upload prescription image")
            return false
        }
        if self.selectedMedArr.count == 0 {
            self.alert(strTitle: strErr, strMsg: "Please add medicines")
            return false
        }
        if self.selNonPreMedArr.count == 0 {
            self.alert(strTitle: strErr, strMsg: "Please add atleast one item")
            return false
        }
        if self.orderData.recipient_name?.rec_name == nil ||  (self.orderData.recipient_name?.rec_name ?? "" ).isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please enter recipient name")
            return false
        }
        if let delivery_type = self.orderData.delivery_type, delivery_type == "Delivery" {
            
            if self.orderData.flat_house_no == nil ||  self.orderData.flat_house_no!.isEmpty {
                self.alert(strTitle: strErr, strMsg: "Please enter flat/house number")
                return false
            }
            if self.orderData.customer_address == nil ||  self.orderData.customer_address!.isEmpty {
                self.alert(strTitle: strErr, strMsg: "Please enter delivery address")
                return false
            }
            if self.orderData.pincode == nil || self.orderData.pincode!.isEmpty {
                self.alert(strTitle: strErr, strMsg: "Please enter pincode")
                return false
            }
        }
        if self.orderData.estimated_date == nil || self.orderData.estimated_date!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please select estimated date")
            return false
        }
        if self.orderData.slot == nil || self.orderData.slot!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please select time slot")
            return false
        }
        
        return true
    }
    
    func validateNewPres() ->  Bool {
        
        if self.orderData.doctor_name == nil ||  self.orderData.doctor_name!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please enter doctor name")
            return false
        }
        if self.selectedMedArr.count == 0 {
            self.alert(strTitle: strErr, strMsg: "Please add medicines")
            return false
        }
        if (self.orderData.prescription_image == nil ||  self.orderData.prescription_image!.isEmpty) && self.prescriptionImg == nil {
            self.alert(strTitle: strErr, strMsg: "Please upload prescription image")
            return false
        }
        if !verifiedFlag {
            self.alert(strTitle: strErr, strMsg: "Please verifiy all the medicines")
            return false
        }
        
        /*
        if self.selNonPreMedArr.count == 0 {
            self.alert(strTitle: strErr, strMsg: "Please add atleast one item")
            return false
        }
        if self.orderData.recipient_name?.rec_name == nil ||  (self.orderData.recipient_name?.rec_name ?? "" ).isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please enter recipient name")
            return false
        }
        if let delivery_type = self.orderData.delivery_type, delivery_type == "Delivery" {
            
            if self.orderData.flat_house_no == nil ||  self.orderData.flat_house_no!.isEmpty {
                self.alert(strTitle: strErr, strMsg: "Please enter flat/house number")
                return false
            }
            if self.orderData.customer_address == nil ||  self.orderData.customer_address!.isEmpty {
                self.alert(strTitle: strErr, strMsg: "Please enter delivery address")
                return false
            }
            if self.orderData.pincode == nil || self.orderData.pincode!.isEmpty {
                self.alert(strTitle: strErr, strMsg: "Please enter pincode")
                return false
            }
        }
        if self.orderData.estimated_date == nil || self.orderData.estimated_date!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please select estimated date")
            return false
        }
        if self.orderData.slot == nil || self.orderData.slot!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please select time slot")
            return false
        }
        */
        return true
    }
    
    func validateNP() -> Bool {
        
        if self.selNonPreMedArr.count == 0 {
            self.alert(strTitle: strErr, strMsg: "Please add atleast one item")
            return false
        }
        return true
    }
    
    func onSaveClicked() {
        
        if orderTypeNP {
            if validateNP() {
                var prescData = PrescriptionData()
                prescData.chemist = selChemistData
                
                let data = ["orderData": self.orderData, "selNonPreMedArr":self.selNonPreMedArr,"orderTypeNP":orderTypeNP,"prescData":prescData] as [String : Any]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "navOrderNotification"), object: data)
            }
        }
        else if uploadNewFlag {
            
            if validateNewPres() {
                
                if prescriptionImg != nil {
                    
                    self.uploadPrescription()
                } else {
                    //self.addOrder()
                    self.processNPM()
                }
            }
            
        } else {
            if validate() {
                
                if prescriptionImg != nil {
                    
                    self.uploadPrescription()
                } else {
                    self.addOrder()
                    
                }
            }
        }
    }
    
    func processNPM() {
        var prescData = PrescriptionData()
        prescData.user_id = Int(user!.id)
        if self.recType == kFamily {
            prescData.child_id = selFamilyMemberData.id
            prescData.sharing_type = 1
            
        } else {
            prescData.child_id = Int(user!.id)
            prescData.sharing_type = 0
        }
        prescData.doctor_name = self.orderData.doctor_name
        
        let arrDrugs = NSMutableArray.init()
        if selectedMedArr.count > 0 {
            
            for data in selectedMedArr {
                
                let meddata = ["medicine_name": data.medicine_name!,"quantity" :data.quantity,"unit" :data.type_of_medicine!] as [String : Any]
                arrDrugs.add(meddata)
            }
            print("drugs: \(arrDrugs)")
        }
        
        let jsonDrugs = JSON(arrDrugs)
        let strDrugs = jsonDrugs.rawString()!
        
        prescData.description = strDrugs
        prescData.illness_type = "null"
        prescData.illness_date = self.orderData.estimated_date
        prescData.image = self.orderData.prescription_image
        prescData.chemist = selChemistData
        
        let data = ["orderData": self.orderData, "selectedMedArr":self.selectedMedArr,"selNonPreMedArr":self.selNonPreMedArr,"prescriptionImg":self.prescriptionImg ?? nil,"prescData":prescData] as [String : Any]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "navOrderNotification"), object: data)
    }
    
    func onUploadClicked(type: Int) {
            
        if type == 0 { // existing
            
            let data = ["orderData": self.orderData, "delegate":self] as [String : Any]
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "navPrescNotification"), object: data)
            
        } else { // new
            uploadNewFlag = true
            self.tblData.reloadData()
        }
    }
    func onAddClicked() {
        self.showImgPicker()
    }
    
    func onImgRemoved() {
        self.prescriptionImg = nil
        self.selPrescription.images.removeAll()
        self.tblData.reloadData()
    }
    
    func onMedClicked(type: Int) {
        
        initAlert(medtype: type)
        addMedicineAlertView.show()
    }
    
    func onQtyChanged(id: String, index: Int, value: Int) {
        
        switch id {
        case "PM":
            self.selectedMedArr[index].quantity = value
            
        case "NPM":
            self.selNonPreMedArr[index].quantity = value
            
        default:
            break
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PrescribedVC: UIImagePickerControllerDelegate {

    func showImgPicker() {
        print("Tap On Image")
        imgPicker.delegate = self
    
        let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {
            action in
            
            self.imgPicker.allowsEditing = true
            self.imgPicker.sourceType = .camera
            self.imgPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            self.present(self.imgPicker, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: {
            action in
            
            self.imgPicker.allowsEditing = true
            self.imgPicker.sourceType = .photoLibrary
            self.imgPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(self.imgPicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
    guard let image = info[.editedImage] as? UIImage else {
        return
    }
        if let imgdata = image.jpegData(compressionQuality: 0.5){
            self.prescriptionImg = imgdata
            
        }
        self.tblData.reloadData()
        dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
}
extension PrescribedVC: PrescriptionListDelegate {
    
    func onSelectPrescription(orderData: MedicineOrderData, preData: PrescriptionData) {
        
        self.orderData = orderData
        self.selPrescription = preData
        selPrescFlag = true
        let selMedicines = selPrescription.getDrugsArr()
        if selMedicines.count > 0 {
            selectedMedArr.append(contentsOf: selMedicines)
        }
        self.tblData.reloadData()
    }
}
extension PrescribedVC: UITableViewDelegate, UITableViewDataSource, UIStepperControllerDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if uploadNewFlag || orderTypeNP {
            return 1
        }
        return selPrescFlag ? 4 : 1
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if uploadNewFlag {
            
            
        } else {
            switch section {
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "labelCell") as! PrescribedCell
                cell.contentView.backgroundColor = .white
                cell.lblHead.text = "PRESCRIBED DRUGS"
                cell.textLabel?.textColor = .black
                return cell.contentView
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "labelCell")  as! PrescribedCell
                cell.contentView.backgroundColor = .white
                cell.lblHead.text = "NON PRESCRIBED ITEMS"
                cell.textLabel?.textColor = .black
                return cell.contentView
            default:
                break
            }
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 || section == 2 {
            return selPrescFlag ? 40 : 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if orderTypeNP {
            return selNonPreMedArr.count + 2
        }
        else if uploadNewFlag {
            
            return selectedMedArr.count + selNonPreMedArr.count + 6
            
        } else {
            switch section {
            case 0:
                return 4
            case 1:
                return selectedMedArr.count + 1
            case 2:
                return selNonPreMedArr.count + 1
            case 3:
                return 8
            default:
                break
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = PrescribedCell()
        if orderTypeNP {
            if indexPath.row == selNonPreMedArr.count {
                cell = tableView.dequeueReusableCell(withIdentifier: "addMedCell") as! PrescribedCell
                cell.setup(id: "addItem", placeHolder: "")
                cell.delegate = self
                
                return cell
            }
            else if indexPath.row <  selNonPreMedArr.count {
                cell = tableView.dequeueReusableCell(withIdentifier: "medListCell") as! PrescribedCell
                cell.delegate = self
                if selNonPreMedArr.count > 0 {
                   
                    let medicine = selNonPreMedArr[indexPath.row]
                    cell.lblMedName.text = medicine.medicine_name
                    cell.stepper.tag = indexPath.row
                    cell.stepper.accessibilityIdentifier = "NPM"
                    cell.stepper.count = CGFloat(medicine.quantity)
                    cell.stepper.delegate = self
                }
                
                return cell
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! PrescribedCell
                cell.delegate = self
                return cell
            }
        }
        else if uploadNewFlag {
            
            if indexPath.row == 0 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                cell.delegate = self
                cell.setup(id: "doctor", placeHolder: "Doctor Name")
                if let doctor = selPrescription.doctor_name {
                    cell.bottoTxtFld.text = doctor
                }
                return cell
            }
            if indexPath.row == selectedMedArr.count + 1 {
                cell = tableView.dequeueReusableCell(withIdentifier: "addMedCell") as! PrescribedCell
                cell.setup(id: "addMed", placeHolder: "")
                cell.delegate = self
                
                return cell
            }
            if selectedMedArr.count > 0 && indexPath.row > 0 && indexPath.row <= selectedMedArr.count {
                cell = tableView.dequeueReusableCell(withIdentifier: "medListCell") as! PrescribedCell
                cell.delegate = self
                if selectedMedArr.count > 0 {
                    let medicine = selectedMedArr[indexPath.row-1]
                    cell.lblMedName.text = medicine.medicine_name
                    cell.stepper.tag = indexPath.row
                    cell.stepper.accessibilityIdentifier = "PM"
                    cell.stepper.count = CGFloat(medicine.quantity)
                    cell.stepper.delegate = self
                }
                return cell
            }
            if indexPath.row == (selectedMedArr.count + selNonPreMedArr.count + 2)  {
                cell = tableView.dequeueReusableCell(withIdentifier: "addMedCell") as! PrescribedCell
                cell.setup(id: "addItem", placeHolder: "")
                cell.delegate = self
                
                return cell
            }
            if selNonPreMedArr.count > 0 && indexPath.row > (selectedMedArr.count+1) && indexPath.row < (selNonPreMedArr.count + selectedMedArr.count + 2) {
                cell = tableView.dequeueReusableCell(withIdentifier: "medListCell") as! PrescribedCell
                cell.delegate = self
                if selNonPreMedArr.count > 0 {
                    let cnt = selectedMedArr.count + 2
                    let medicine = selNonPreMedArr[indexPath.row-cnt]
                    cell.lblMedName.text = medicine.medicine_name
                    cell.stepper.tag = indexPath.row
                    cell.stepper.accessibilityIdentifier = "NPM"
                    cell.stepper.count = CGFloat(medicine.quantity)
                    cell.stepper.delegate = self
                }
                
                return cell
            }
            if indexPath.row == selectedMedArr.count + selNonPreMedArr.count + 3 {
                if self.selPrescription.images.count > 0 {
                    cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! PrescribedCell
                    
                    cell.prescriptionImgURL = self.selPrescription.images[0].image
                    
                    cell.collView.reloadData()
                }
                else if self.prescriptionImg != nil {
                    cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! PrescribedCell
                    cell.prescriptionImg = self.prescriptionImg
                    cell.collView.reloadData()
                }
                else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "upCell") as! PrescribedCell
                }
                
                cell.delegate = self
                return cell
            }
            if indexPath.row == selectedMedArr.count + selNonPreMedArr.count + 4 {
                cell = tableView.dequeueReusableCell(withIdentifier: "chkCell") as! PrescribedCell
                cell.delegate = self
                return cell
            }
            if indexPath.row == selectedMedArr.count + selNonPreMedArr.count + 5 {
                cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! PrescribedCell
                cell.delegate = self
                return cell
            }
            
        } else {
            switch indexPath.section {
            case 0:
                
                switch indexPath.row {
                    
                case 0:
                    cell = tableView.dequeueReusableCell(withIdentifier: "chooseBtnCell") as! PrescribedCell
                    cell.delegate = self
                    cell.setup(id: "newPresc", placeHolder: "")
                    
                case 1:
                    cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! PrescribedCell
                    cell.delegate = self
                    cell.setup(id: "chemist", placeHolder: "Chemist Name")
                    cell.pickerDataArr = self.chemistArr
                    let chemist = self.chemistArr.filter { (data) -> Bool in
                        return data.id == self.orderData.chemist_id
                    }
                    if chemist.count > 0 {
                        cell.txtFld.text = chemist[0].name
                    }//selChemistData?.name ?? ""
                    
                case 2:
                    cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                    cell.delegate = self
                    cell.setup(id: "doctor", placeHolder: "Doctor Name")
                    if let doctor = selPrescription.doctor_name {
                        cell.bottoTxtFld.text = doctor
                    }
                    
                case 3:
                    if self.selPrescription.images.count > 0 {
                        cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! PrescribedCell
                        
                        cell.prescriptionImgURL = self.selPrescription.images[0].image
                        
                        cell.collView.reloadData()
                    }
                    else if self.prescriptionImg != nil {
                        cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! PrescribedCell
                        cell.prescriptionImg = self.prescriptionImg
                        cell.collView.reloadData()
                    }
                    else {
                        cell = tableView.dequeueReusableCell(withIdentifier: "upCell") as! PrescribedCell
                    }
                    
                    cell.delegate = self
                    /*
                     case 0:
                     cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                     cell.delegate = self
                     cell.setup(id: "medicineName", placeHolder: "Medicine Name")
                     
                     case 1:
                     cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! PrescribedCell
                     cell.delegate = self
                     cell.setup(id: "doctor", placeHolder: "Doctor")
                     
                     case 2:
                     cell = tableView.dequeueReusableCell(withIdentifier: "chooseBtnCell") as! PrescribedCell
                     cell.delegate = self
                     cell.setup(id: "existingPresc", placeHolder: "")
                     
                     case 3:
                     cell = tableView.dequeueReusableCell(withIdentifier: "labelCell") as! PrescribedCell
                     
                     case 4:
                     cell = tableView.dequeueReusableCell(withIdentifier: "uploadBtnCell") as! PrescribedCell
                     cell.delegate = self
                     cell.setup(id: "newPresc", placeHolder: "")
                     
                     case 5:
                     cell = tableView.dequeueReusableCell(withIdentifier: "chkCell") as! PrescribedCell
                     cell.delegate = self
                     
                     case 6:
                     cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! PrescribedCell
                     cell.delegate = self
                     cell.setup(id: "proceed", placeHolder: "")
                     */
                default:
                    break
                }
            case 1:
                switch indexPath.row {
                case selectedMedArr.count:
                    cell = tableView.dequeueReusableCell(withIdentifier: "addMedCell") as! PrescribedCell
                    cell.setup(id: "addMed", placeHolder: "")
                    cell.delegate = self
                default:
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "medListCell") as! PrescribedCell
                    cell.delegate = self
                    if selectedMedArr.count > 0 {
                        let medicine = selectedMedArr[indexPath.row]
                        cell.lblMedName.text = medicine.medicine_name
                        cell.stepper.tag = indexPath.row
                        cell.stepper.count = CGFloat(medicine.quantity)
                        cell.stepper.delegate = self
                    }
                }
            case 2:
                switch indexPath.row {
                case selNonPreMedArr.count:
                    cell = tableView.dequeueReusableCell(withIdentifier: "addMedCell") as! PrescribedCell
                    cell.setup(id: "addItem", placeHolder: "")
                    cell.delegate = self
                default:
                    cell = tableView.dequeueReusableCell(withIdentifier: "medListCell") as! PrescribedCell
                    cell.delegate = self
                    if selNonPreMedArr.count > 0 {
                        let medicine = selNonPreMedArr[indexPath.row]
                        cell.lblMedName.text = medicine.medicine_name
                        cell.stepper.tag = indexPath.row
                        cell.stepper.count = CGFloat(medicine.quantity)
                        cell.stepper.delegate = self
                    }
                }
            case 3:
                switch indexPath.row {
                case 0:
                    cell = tableView.dequeueReusableCell(withIdentifier: "typeCell") as! PrescribedCell
                    cell.setup(id: "type", placeHolder: "")
                    cell.delegate = self
                    
                case 1:
                    cell = tableView.dequeueReusableCell(withIdentifier: "radioCell") as! PrescribedCell
                    cell.setup(id: "radio", placeHolder: "")
                    
                    cell.delegate = self
                case 2:
                    if self.recType == "Self" {
                        cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                        cell.setup(id: "recipient_self", placeHolder: "")
                        cell.bottoTxtFld.text = self.orderData.recipient_name?.rec_name ?? ""
                    } else {
                        cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! PrescribedCell
                        cell.pickerDataArr = self.familyMembersArr
                        cell.setup(id: "recipient_family", placeHolder: "")
                    }
                    
                    cell.delegate = self
                case 3:
                    cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                    cell.setup(id: "flat", placeHolder: "FLAT/HOUSE NUMBER")
                    cell.bottoTxtFld.text = self.orderData.flat_house_no ?? ""
                    cell.delegate = self
                case 4:
                    cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                    cell.setup(id: "address", placeHolder: "DELIVERY ADDRESS")
                    cell.bottoTxtFld.text = self.orderData.customer_address ?? ""
                    cell.delegate = self
                case 5:
                    cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                    cell.setup(id: "pincode", placeHolder: "PINCODE")
                    cell.bottoTxtFld.text = self.orderData.pincode ?? ""
                    
                    cell.delegate = self
                case 6:
                    cell = tableView.dequeueReusableCell(withIdentifier: "dateTimeCell") as! PrescribedCell
                    cell.pickerDataArr = self.slotArr
                    cell.setup(id: "date", placeHolder: "")
                    
                    cell.delegate = self
                    
                case 7:
                    cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! PrescribedCell
                    cell.delegate = self
                    
                default:
                    break
                }
            default:
                break
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if orderTypeNP {
            return 90
        }
        else if uploadNewFlag {
            if indexPath.row == selectedMedArr.count + selNonPreMedArr.count + 3 {
                return 160
            }
            return 90
        } else {
            if indexPath.section == 0 {
                if indexPath.row == 0 {
                    if selPrescFlag {
                        return 0
                    }
                    return 150
                }
                if indexPath.row == 3 {
                    return selPrescFlag ? 160 : 0
                }
            }
            if indexPath.section == 3 {
                
                if indexPath.row == 0 {
                    return selPrescFlag ? 90 : 0
                }
                if indexPath.row == 1 {
                    return selPrescFlag ? 160 : 0
                }
                if indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5 {
                    if self.orderData.delivery_type == "Delivery" {
                        return selPrescFlag ? 90 : 0
                    } else {
                        return 0
                    }
                    
                }
            }
        }
        return selPrescFlag ? 90 : 0
    }
    
    func stepperDidAddValues(stepper: UIStepperController) {
    
         print("Stepper value did change (Add) : \(stepper.count)")
        if stepper.tag == 0 {
            self.medObj.quantity = Int(stepper.count)
        } else {
            self.nonMedObj.quantity = Int(stepper.count)
        }
    }

    func stepperDidSubtractValues(stepper: UIStepperController) {
    
         print("Stepper value did change (Subtract) \(stepper.count)")
        if stepper.tag == 0 {
            self.medObj.quantity = Int(stepper.count)
        } else {
            self.nonMedObj.quantity = Int(stepper.count)
        }
         
    }
    
}
protocol PrescribedCellDelegate {
    func onValueSelected(type: String, value: String)
    func onDataSelected(type: String, value: Any)
    func onSaveClicked()
    func onUploadClicked(type: Int)
    func onAddClicked()
    func onImgRemoved()
    func onMedClicked(type: Int)
    func onQtyChanged(id: String, index: Int, value: Int)
}
class PrescribedCell: BaseCell, UIStepperControllerDelegate {
    
    @IBOutlet weak var txtFld: RightViewTextField!
    @IBOutlet weak var txtFld1: RightViewTextField!
    @IBOutlet weak var bottoTxtFld: BottomLineTextField!
    @IBOutlet weak var cellBtn: UIButton!
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var lblMedName: UILabel!
    @IBOutlet weak var recepient: RadioGroup!
    @IBOutlet weak var stepper: UIStepperController!
    @IBOutlet weak var lblHead: UILabel!
    
    var delegate: PrescribedCellDelegate?
    var prescriptionImg: Data?
    var prescriptionImgURL: String?
    var selRow: Int = 0
    var seldate: String?
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
    func setup(id: String, placeHolder: String){
         
        selType = id
        if id == "chemist" {
            cellPicker.dataSource = self
            cellPicker.delegate = self
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
        if id == "radio" {
            lblMedName.text = "ORDER RECIPIENT"
            recepient.titles = ["Self","Family"]
            recepient.addTarget(self, action: #selector(optionSelected(radioGroup:)), for: .valueChanged)
        }
        if id == "type" {
            lblMedName.text = "SELECT TYPE"
        }
        if id == "recipient_self" {
            lblMedName.text = "RECIPIENT NAME"
            bottoTxtFld.accessibilityIdentifier = "recipient"
            bottoTxtFld.delegate = self
        }
        if id == "recipient_family" {
            lblMedName.text = "RECIPIENT Member"
            cellPicker.dataSource = self
            cellPicker.delegate = self
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
        if id == "flat" {
            lblMedName.text = placeHolder
            bottoTxtFld.placeholder = placeHolder
            bottoTxtFld.delegate = self
            bottoTxtFld.accessibilityIdentifier = "flat"
        }
        if id == "address" {
            lblMedName.text = placeHolder
            bottoTxtFld.placeholder = placeHolder
            bottoTxtFld.delegate = self
            bottoTxtFld.accessibilityIdentifier = "address"
        }
        if id == "pincode" {
            lblMedName.text = placeHolder
            bottoTxtFld.placeholder = placeHolder
            bottoTxtFld.delegate = self
            bottoTxtFld.accessibilityIdentifier = "pincode"
        }
        if id == "date" {
            lblMedName.text = "EXPECTED DATE & TIME"
            txtFld.placeholder = "Date"
            txtFld1.placeholder = "Time Slot"
            txtFld.inputView = datePicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            cellPicker.dataSource = self
            cellPicker.delegate = self
            txtFld1.inputView = cellPicker
            txtFld1.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "time")
        }
        if id == "addMed" {
            cellBtn.setTitle("Add Medicines", for: .normal)
            cellBtn.addTarget(self, action: #selector(addMedAction(_:)), for: .touchUpInside)
        }
        if id == "addItem" {
            cellBtn.setTitle("Add Item", for: .normal)
            cellBtn.addTarget(self, action: #selector(addItemAction(_:)), for: .touchUpInside)
        }
        if id == "doctor" {
            lblMedName.text = "DOCTOR NAME"
            bottoTxtFld.delegate = self
            bottoTxtFld.accessibilityIdentifier = "doctor"
        }
        if id == "instruction" {
            lblMedName.text = "ORDER INSTRUCTIONS"
            bottoTxtFld.delegate = self
            bottoTxtFld.accessibilityIdentifier = id
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        print("selType:\(selType)")
        selRow = row
        
        switch selType {
        case "chemist":
            let type = self.pickerDataArr[selRow] as? ChemistData
            txtFld.text = type?.name
            
        case "date":
            let type = self.pickerDataArr[selRow] as? SlotData
            txtFld1.text = type?.slot
        
        case "recipient_family":
            let type = self.pickerDataArr[selRow] as? FamilyMemberData
            txtFld.text = type?.name
            
        default:
            break
        }
        
        //delegate?.onValueSelected(type: selType, value: txtFld.text ?? "")
    }
    
    @objc override func doneCellPicker (sender: UIBarButtonItem) {
        
        let id = sender.accessibilityIdentifier
        
        if id == "chemist" {
            let value = (pickerDataArr[selRow] as? ChemistData)!
            txtFld.text = value.name
            txtFld.resignFirstResponder()
            delegate?.onDataSelected(type: id!, value: value)
        }
        if id == "date" {
            seldate = datePicker.date.toString(format: df_dd_MM_yyyy)
            txtFld.text = seldate!
            txtFld.resignFirstResponder()
            delegate?.onValueSelected(type: id!, value: seldate!)
        }
        if id == "time" {
            let value = (pickerDataArr[selRow] as? SlotData)!
            txtFld1.text = value.slot
            txtFld1.resignFirstResponder()
            delegate?.onDataSelected(type: id!, value: value)
        }
        if id == "recipient_family" {
            let value = (pickerDataArr[selRow] as? FamilyMemberData)!
            txtFld.text = value.name
            txtFld.resignFirstResponder()
            delegate?.onDataSelected(type: id!, value: value)
        }
        
        
    }
    @objc func dateSelected(_ picker: UIDatePicker){
        
        let dateStr = picker.date.toString(format: df_dd_MM_yyyy)//df.string(from: picker.date)
        
        txtFld.text = dateStr
        
        let date = picker.date.toString(format: "YYYY-MM-dd")
        seldate = date
    }
    
    @objc func optionSelected(radioGroup: RadioGroup){
        
        delegate?.onValueSelected(type: "radio", value: String(radioGroup.selectedIndex))
        //optionValueChanged(value: radioGroup.selectedIndex)
    }
    func stepperDidAddValues(stepper: UIStepperController) {
        
        delegate?.onQtyChanged(id: stepper.accessibilityIdentifier!, index: stepper.tag, value: Int(stepper.count))
    }
    
    func stepperDidSubtractValues(stepper: UIStepperController) {
        
        delegate?.onQtyChanged(id: stepper.accessibilityIdentifier!, index: stepper.tag, value: Int(stepper.count))
    }
    
    @IBAction func uploadExistingAction(_ sender: UIButton){
        delegate?.onUploadClicked(type: 0)
    }
    
    @IBAction func uploadNewAction(_ sender: UIButton){
        delegate?.onUploadClicked(type: 1)
    }
    
    @IBAction func onDeliveryTypeChanged(_ sender: UISegmentedControl) {
        
        delegate?.onValueSelected(type: "deliveryYype", value: String(sender.selectedSegmentIndex))
    }
    @IBAction func onPrescTypeChanged(_ sender: UISegmentedControl) {
        
        delegate?.onValueSelected(type: "prescType", value: sender.titleForSegment(at: sender.selectedSegmentIndex)!)
    }
    
    @IBAction func addMedAction(_ sender: UIButton){
        
        delegate?.onMedClicked(type: 0)
    }
    
    @IBAction func addItemAction(_ sender: UIButton){
        
        delegate?.onMedClicked(type: 1)
    }
    @IBAction func addAction(_ sender: UIButton){
        
        delegate?.onAddClicked()
    }
    @IBAction func checkAction(_ sender: UIButton){
        
        sender.isSelected = !sender.isSelected
        
        delegate?.onValueSelected(type: "checkbox", value: String(sender.isSelected))
    }
    @IBAction func saveAction(_ sender: UIButton){
        
        delegate?.onSaveClicked()
    }
}
extension PrescribedCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        /*
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(valueChanged),
            object: textField)*/
        self.perform(
            #selector(valueChanged),
            with: textField,
            afterDelay: 0.8)
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @objc func valueChanged(textField: UITextField) {
        
        delegate?.onValueSelected(type: textField.accessibilityIdentifier!, value: textField.text!)
    }
}
extension PrescribedCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, PrescribedCollCellDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = PrescribedCollCell()
        if indexPath.item == 1 {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addCell", for: indexPath) as! PrescribedCollCell
        } else {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath) as! PrescribedCollCell
            
            if let imgData = prescriptionImg {
                cell.imgview.image = UIImage(data: imgData)
            } else if var url = prescriptionImgURL, !url.isEmpty {
                url = prescriptionImageURL + url
                cell.imgview.kf.setImage(with: URL(string: url))
            }
            cell.delegate = self
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
      
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 90, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if prescriptionImg == nil {
            
            delegate?.onAddClicked()
        }
    }
    
    func oDeleteClicked(){
        
        prescriptionImg = nil
        delegate?.onImgRemoved()
       
    }
}

protocol PrescribedCollCellDelegate {
    
    func oDeleteClicked()
}
class PrescribedCollCell: UICollectionViewCell {
    
    @IBOutlet weak var imgview: UIImageView!
    @IBOutlet weak var btnDelete:UIButton!
    var delegate: PrescribedCollCellDelegate?
    
 
    @IBAction func deleteAction(_ sender: UIButton){
        delegate?.oDeleteClicked()
    }
}
