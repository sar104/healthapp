//
//  UpdateOrderVC.swift
//  HealthApp
//
//  Created by Apple on 21/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD
import SwiftyJSON
import Alamofire

class UpdateOrderVC: BaseVC, SwiftAlertViewDelegate {

    @IBOutlet weak var tblData: UITableView!
    var addMedicineAlertView: SwiftAlertView!
    
    var selPrescription: PrescriptionData = PrescriptionData()
    var prescriptionImg:Data?
    var prescriptionImgUrl: String = ""
    
    var orderData: CustOrderData!
    var medOrderData: MedicineOrderData = MedicineOrderData()
    var recType: String = "Self"
    
    let recTypeArr = ["Self", "Family"]
    let deliveryTypeArr = ["Delivery","Pickup"]
    var familyMembersArr: [FamilyMemberData] = []
    var chemistArr: [ChemistData] = []
    var slotArr: [SlotData] = []
    var selFamilyMemberData: FamilyMemberData = FamilyMemberData()
    var allMedicineArr: [MedicineData] = []
    var selectedMedArr: [MedicineData] = []
    var selNonPreMedArr: [MedicineData] = []
    var medicineArr: [MedicineData] = []
    var medObj = MedicineData()
    var nonMedObj = MedicineData()
    let imgPicker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        medOrderData = orderData.getMedOrderData()
        prescriptionImgUrl = orderData.prescription_image ?? ""
        if orderData.getRecepient().isEmpty {
            self.recType = recTypeArr[0]
        } else {
            self.recType = recTypeArr[1]
        }
        
        let medArr = CoreDataManager.shared.fetchAllMedicines()
        
        for data in medArr {
            
            allMedicineArr.append(MedicineData(coreDataMed: data))
            
        }
        
        getChemistList()
        getSlotList()
        getFamilyMembers()
        parseMedData()
    }
    
    func initAlert(medtype: Int) {
    
        addMedicineAlertView = SwiftAlertView(nibName: "AddMedicineAlertView", delegate: self, cancelButtonTitle: nil, otherButtonTitles: nil)
        addMedicineAlertView.dismissOnOtherButtonClicked = true
        addMedicineAlertView.dismissOnOutsideClicked = true
        addMedicineAlertView.tag = 1
        
        let txtMedName = addMedicineAlertView.viewWithTag(2) as! SearchTextField
        let txtType = addMedicineAlertView.viewWithTag(3) as! RightViewTextField
        let stepper = addMedicineAlertView.viewWithTag(4) as! UIStepperController
        stepper.delegate = self
        stepper.tag = medtype
        stepper.count = 1
        
        txtMedName.addBottomBorder()
        
        txtMedName.userStoppedTypingHandler = {
            if let criteria = txtMedName.text {
                if criteria.count > 2 {

                    // Show the loading indicator
                    txtMedName.showLoadingIndicator()
                    self.medicineArr = self.allMedicineArr.filter({ (data) -> Bool in
                        return data.medicine_name!.range(of: criteria) != nil
                    })
                    let medNames:[String] = self.medicineArr.map( {($0.medicine_name ?? "")} )
                    if medNames.count > 0 {
                        txtMedName.filterStrings(medNames)
                    } else {
                        txtType.text = "other"
                        if medtype == 0 {
                            self.medObj.medicine_name = criteria
                            self.medObj.quantity = Int(stepper.count)
                            self.medObj.type = "PM"
                            self.medObj.type_of_medicine = "other"
                        } else {
                            self.nonMedObj.medicine_name = criteria
                            self.nonMedObj.quantity = Int(stepper.count)
                            self.nonMedObj.type = "NPM"
                            self.nonMedObj.type_of_medicine = "other"
                        }
                    }
                    txtMedName.stopLoadingIndicator()

                   
                }
            }
        }
        txtType.addBottomBorder()
        
        //let medNames:[String] = self.medicineArr.map( {($0.medicine_name ?? "")} )
        //txtMedName.filterStrings(medNames)//(["asdds","dsssfd","weew","bvmbvb"])
            txtMedName.itemSelectionHandler = {filteredResults, itemPosition in
            let item = filteredResults[itemPosition]
                txtMedName.text = item.title
                let meddata = self.medicineArr.filter{ $0.medicine_name == item.title}.first
                if meddata != nil {
                    let type = meddata?.type_of_medicine
                    txtType.text = type
                    if medtype == 0 {
                        self.medObj = meddata!
                        self.medObj.quantity = Int(stepper.count)
                        self.medObj.type = "PM"
                    } else {
                        self.nonMedObj = meddata!
                        self.nonMedObj.quantity = Int(stepper.count)
                        self.nonMedObj.type = "NPM"
                    }
                }
                else {
                    if medtype == 0 {
                        self.medObj.medicine_name = item.title
                        self.medObj.quantity = Int(stepper.count)
                        self.medObj.type = "PM"
                        self.medObj.type_of_medicine = "other"
                    } else {
                        self.nonMedObj.medicine_name = item.title
                        self.nonMedObj.quantity = Int(stepper.count)
                        self.nonMedObj.type = "NPM"
                        self.nonMedObj.type_of_medicine = "other"
                    }
                }
        }
        
        //txtMedName.delegate = self
        //txtType.delegate = self
        //let qtyBtn = addMedicineAlertView.viewWithTag(4) as! UIStepperController
        let addBtn = addMedicineAlertView.viewWithTag(5) as! UIButton
        let closeBtn = addMedicineAlertView.viewWithTag(6) as! UIButton
        
        closeBtn.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        
        if medtype == 0 {
            addBtn.addTarget(self, action: #selector(addMedicineAction), for: .touchUpInside)
        } else {
            addBtn.addTarget(self, action: #selector(addNonMedicineAction), for: .touchUpInside)
        }
    }
    
    @objc func addMedicineAction(){
        
        if medObj.medicine_name == nil {
            
            self.alert(strTitle: strErr, strMsg: "Please select/enter medicine name ")
            
        } else {
            self.selectedMedArr.append(medObj)
            print("qty: \(medObj.quantity)")
            
            addMedicineAlertView.dismiss()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.tblData.reloadData()
            }
        }
    }
    
    @objc func addNonMedicineAction(){
        
        
        self.selNonPreMedArr.append(nonMedObj)
        print("qty: \(medObj.quantity)")
        
        addMedicineAlertView.dismiss()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.tblData.reloadData()
        }
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
     
        addMedicineAlertView.dismiss()
    }
    
    @objc func dismissAlert(){
        addMedicineAlertView.dismiss()
    }
    
    func parseMedData() {
        
        if let order_details = orderData.order_details {
            
            for details in order_details {
                
                let med = details.getMedicineData()
                let type = med.medicine_type
                
                if type == "PM" {
                    var meddata = MedicineData()
                    meddata.medicine_name = med.medicine_name
                    meddata.quantity = details.quantity!
                    meddata.packaging_of_medicines = details.unit
                    meddata.type_of_medicine = details.unit
                    
                    selectedMedArr.append(meddata)
                    
                } else {
                    var meddata = MedicineData()
                    meddata.medicine_name = med.medicine_name
                    meddata.quantity = details.quantity!
                    meddata.packaging_of_medicines = details.unit
                    meddata.type_of_medicine = details.unit
                    
                    selNonPreMedArr.append(meddata)
                }
            }
            
            self.tblData.reloadData()
        }
    }
    
    func getChemistList() {
        
        /*
        let params = [PARAM_USERID: loggedUser?.id,
        PARAM_LATITUDE: 19.980661,
        PARAM_LONGITUDE: 73.797836] as [String:Any]
         */
        
        let lat = defaults.value(forKey: kUserLat) as? CGFloat ?? 0.0
        let lon = defaults.value(forKey: kUserLong) as? CGFloat ?? 0.0
        
        let params = [PARAM_USERID: loggedUser!.id,
        PARAM_LATITUDE: lat,
        PARAM_LONGITUDE: lon] as [String:Any]
        
        IHProgressHUD.show()
        
        apiManager.searchChemists(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    self.chemistArr.removeAll()
                    self.chemistArr = data
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func getSlotList() {
        
        /*
        IHProgressHUD.show()
        
        apiManager.getTimeSlots(params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    self.slotArr.removeAll()
                    self.slotArr = data
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }*/
        let time1 = "09:00 AM To 12:00 PM"
        let slot1 = SlotData(id: 1, slot: time1)
        let time2 = "01:00 PM To 3:00 PM"
        let slot2 = SlotData(id: 2, slot: time2)
        let time3 = "03:00 PM To 5:00 PM"
        let slot3 = SlotData(id: 3, slot: time3)
        let time4 = "05:00 PM To 7:00 PM"
        let slot4 = SlotData(id: 4, slot: time4)
        
        self.slotArr.append(slot1)
        self.slotArr.append(slot2)
        self.slotArr.append(slot3)
        self.slotArr.append(slot4)
    }
    
    func getFamilyMembers() {
        
        IHProgressHUD.show()
        
        let params = ["user_id": loggedUser?.id] as [String: Any]
        apiManager.getFamilyMember(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    
                    self.familyMembersArr = memberData
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }

    func uploadPrescription(){
        
        IHProgressHUD.show()
        apiManager.uploadImg(image: prescriptionImg!, name: "image", api: PrescriptionImgAPI) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let imgdata):
                    
                    let imgstr = String(data: imgdata, encoding: .utf8)
                    print(imgstr)
                        
                        if((imgstr?.contains("jpeg"))!){
                            self.orderData.prescription_image = imgstr
                            self.prescriptionImgUrl = imgstr!
                            self.updateOrder()
                        }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func validate() ->  Bool {
        
        /*
        if !orderTypeNP && (self.orderData.doctor_name == nil ||  self.orderData.doctor_name!.isEmpty) {
            self.alert(strTitle: strErr, strMsg: "Please enter doctor name")
            return false
        }*/
        
        if self.medOrderData.recipient_name == nil && self.orderData.getRecepient().isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please enter recipient name")
            return false
        }
        if let delivery_type = self.orderData.delivery_type, delivery_type == "Delivery" {
            
            if self.orderData.flat_house_no == nil ||  self.orderData.flat_house_no!.isEmpty {
                self.alert(strTitle: strErr, strMsg: "Please enter flat/house number")
                return false
            }
            if self.orderData.customer_address == nil ||  self.orderData.customer_address!.isEmpty {
                self.alert(strTitle: strErr, strMsg: "Please enter delivery address")
                return false
            }
            if self.orderData.pincode == nil ||
            String(self.orderData.pincode!).isEmpty {
                self.alert(strTitle: strErr, strMsg: "Please enter pincode")
                return false
            }
        }
        if self.orderData.estimated_date == nil || self.orderData.estimated_date!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please select estimated date")
            return false
        }
        if self.orderData.slot == nil || self.orderData.slot!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please select time slot")
            return false
        }
        
        return true
    }
    
    func updateOrder() {
           
           IHProgressHUD.show()
           
           var drugArr: [DrugData] = []
           let drugArrPM = MedicineData.getDrugsData(self.selectedMedArr)
           drugArr.append(contentsOf: drugArrPM)
           let drugArrNPM = MedicineData.getDrugsData(self.selNonPreMedArr)
           drugArr.append(contentsOf: drugArrNPM)
           
           //self.orderData.drug_list = drugArr
           
           do {
               let arrDrugs = NSMutableArray.init()
            if self.orderData.prescription_type == "Prescribed" {
                if selectedMedArr.count > 0 {
                    
                    for med in selectedMedArr {
                        
                        //let meddata = [ "medicine_name":med.medicine_name,"suggested_name": "", "medicine_type": med.type,"quantity" :med.quantity,"unit" :med.type_of_medicine ?? med.packaging_of_medicines] as [String : Any]
                        var medname = MedicineName()
                        medname.medicine_name = med.medicine_name
                        medname.medicine_type = med.type
                        medname.suggested_name = ""
                        
                        let mednameJSONData = try JSONEncoder().encode(medname)
                        let jsonStrMedname = String(data: mednameJSONData, encoding: .utf8)
                        //["medicine_name":med.medicine_name,"suggested_name": "", "medicine_type": med.type]
                        
                        let meddata = ["medicine_name":jsonStrMedname,"quantity" :med.quantity,"unit" :med.type_of_medicine!] as [String : Any]
                        arrDrugs.add(meddata)
                    }
                    print("drugs: \(arrDrugs)")
                }
            }
               if selNonPreMedArr.count > 0 {
                   
                   for med in selNonPreMedArr {
                       
                       //let meddata = [ "medicine_name":med.medicine_name,"suggested_name": "", "medicine_type": med.type,"quantity" :med.quantity,"unit" :med.type_of_medicine!] as [String : Any]
                       //let meddata = ["medicine_name":["medicine_name":med.medicine_name,"suggested_name": "", "medicine_type": med.type],"quantity" :med.quantity,"unit" :med.type_of_medicine!] as [String : Any]
                       //arrDrugs.add(meddata)
                    var medname = MedicineName()
                    medname.medicine_name = med.medicine_name
                    medname.medicine_type = med.type
                    medname.suggested_name = ""
                    
                    let mednameJSONData = try JSONEncoder().encode(medname)
                    let jsonStrMedname = String(data: mednameJSONData, encoding: .utf8)
                    //["medicine_name":med.medicine_name,"suggested_name": "", "medicine_type": med.type]
                    
                    let meddata = ["medicine_name":jsonStrMedname,"quantity" :med.quantity,"unit" :med.type_of_medicine!] as [String : Any]
                    arrDrugs.add(meddata)
                   }
                   print("drugs: \(arrDrugs)")
               }
                
                let jsonDrugs = JSON(arrDrugs)
                let jsonStrDrugs = jsonDrugs.rawString()!
               
               //let drugJSonData = try JSONSerialization.data(withJSONObject: arrDrugs, options: [])
                //let drugJSonData = try JSONEncoder().encode(drugArr)
               //let jsonStrDrugs = String(data: drugJSonData, encoding: .utf8)
               
               let paramsData = try JSONEncoder().encode(self.orderData)
               
               var params:[String:String] = [:]
               //guard var params = try? JSONSerialization.jsonObject(with: paramsData, options: []) as? [String:Any]
                  // else { return }
               
               params["customer_address"] = self.orderData.customer_address ?? ""
               params["delivery_type"] = self.orderData.delivery_type ?? ""
               params["chemist_id"] = String(self.orderData.chemist_id!)
               params["prescription_type"] = self.orderData.prescription_type ?? ""
               params["slot"] = self.orderData.slot ?? ""
               params["estimated_date"] = self.orderData.estimated_date ?? ""
               params["description"] = self.orderData.description
                if self.orderData.prescription_type == "Non Prescribed" {
                    params["doctor_name"] = "null"
                } else {
                    params["doctor_name"] = self.orderData.doctor_name ?? "-"
                }
               params["address_id"] = String(self.medOrderData.address_id)
               params["pincode"] = self.medOrderData.pincode ?? ""
                if self.orderData.prescription_type == "Non Prescribed" {
                    params["prescription_image"] = "null"
                } else {
                    params["prescription_image"] = self.orderData.prescription_image ?? "NA"
                }
            params["user_id"] = String(loggedUser!.id)
               params["flat_house_no"] = self.orderData.flat_house_no
               
               params["drug_list"] = jsonStrDrugs
            let orderId: Int = orderData.id!
            params["order_id"] = String(orderId)
               
               let dataRec = try JSONEncoder().encode(self.orderData.recipient_name)
               let paramsRec =  String(data: dataRec, encoding: .utf8)
                   //try? JSONSerialization.jsonObject(with: dataRec, options: []) as? [String:Any]
               
               params["recipient_name"] = self.orderData.recipient_name//paramsRec
               print("params: \(params)")
               
               
               apiManager.UpdateOrderForCustomer(params: params) { (result) in
                   
                   IHProgressHUD.dismiss()
                   switch result {
                       case .success(let data):
                           self.alert(strTitle: strSuccess, strMsg: data.message!)
                           if data.status {
                               DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                   //self.OrderCompleted()
                               }
                           }
                           
                       case .failure(let err):
                           print("err: \(err)")
                           self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                   }
               }
              
           } catch let err {
               print(err)
           }
           
           
       }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UpdateOrderVC: UIImagePickerControllerDelegate {

    func showImgPicker() {
        print("Tap On Image")
        imgPicker.delegate = self
    
        let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {
            action in
            
            self.imgPicker.allowsEditing = true
            self.imgPicker.sourceType = .camera
            self.imgPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            self.present(self.imgPicker, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: {
            action in
            
            self.imgPicker.allowsEditing = true
            self.imgPicker.sourceType = .photoLibrary
            self.imgPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(self.imgPicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
    guard let image = info[.editedImage] as? UIImage else {
        return
    }
        if let imgdata = image.jpegData(compressionQuality: 0.5){
            self.prescriptionImg = imgdata
            
        }
        self.tblData.reloadData()
        dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
}
extension UpdateOrderVC: PrescribedCellDelegate {
    
    
    func onValueSelected(type: String, value: String) {
        
        switch type {
        case "date":
            self.orderData.estimated_date = value
            
        case "deliveryYype":
            let index = Int(value)
            self.orderData.delivery_type = deliveryTypeArr[index!]
            self.tblData.reloadData()
            
        case "radio":
            let index = Int(value)
            recType = recTypeArr[index!]
            let indexpath = IndexPath(row: 3, section: 3)
            self.tblData.reloadRows(at: [indexpath], with: .none)
            
        case "recipient":
            self.medOrderData.recipient_name = RecipientData(rec_id: Int(loggedUser!.id), rec_name: value, rec_mobile: loggedUser?.mobile_no)
            
        case "doctor":
            self.orderData.doctor_name = value
            
        case "flat":
            self.orderData.flat_house_no = value
            
        case "address":
            self.orderData.customer_address = value
            
        case "pincode":
            self.medOrderData.pincode = value
            
        case "prescType":
            self.orderData.prescription_type = value
            self.tblData.reloadData()
        //case "checkbox":
            //self.verifiedFlag = Bool(value)!
            //print("chk: \(value), \(self.verifiedFlag)")
            
        default:
            break
        }
        
        //self.tblData.reloadData()
    }
    
    func onDataSelected(type: String, value: Any) {
        
        switch type {
        case "chemist":
            let data = value as! ChemistData
            self.orderData.chemist_id = data.id
            
        case "time":
            let data = value as! SlotData
            self.orderData.slot = data.slot
            
        case "recipient_family":
            self.selFamilyMemberData = value as! FamilyMemberData
            
        default:
            break
        }
    }
    
    func onSaveClicked() {
        
        if validate() {
            if prescriptionImg != nil {
                self.uploadPrescription()
            } else {
                self.updateOrder()
            }
        }
    }
    
    func onUploadClicked(type: Int) {
        
    }
    
    func onAddClicked() {
        self.showImgPicker()
    }
    
    func onImgRemoved() {
        self.prescriptionImg = nil
        //self.selPrescription.images.removeAll()
        self.prescriptionImgUrl = ""
        self.tblData.reloadData()
    }
    
    func onMedClicked(type: Int) {
        
        initAlert(medtype: type)
        addMedicineAlertView.show()
    }
    
    func onQtyChanged(id: String, index: Int, value: Int) {
        
        switch id {
        case "PM":
            self.selectedMedArr[index].quantity = value
            
        case "NPM":
            self.selNonPreMedArr[index].quantity = value
            
        default:
            break
        }
    }
    
}

extension UpdateOrderVC: UIStepperControllerDelegate {
    
    func stepperDidAddValues(stepper: UIStepperController) {
    
         print("Stepper value did change (Add) : \(stepper.count)")
        if stepper.tag == 0 {
            self.medObj.quantity = Int(stepper.count)
        } else {
            self.nonMedObj.quantity = Int(stepper.count)
        }
    }

    func stepperDidSubtractValues(stepper: UIStepperController) {
    
         print("Stepper value did change (Subtract) \(stepper.count)")
        if stepper.tag == 0 {
            self.medObj.quantity = Int(stepper.count)
        } else {
            self.nonMedObj.quantity = Int(stepper.count)
        }
         
    }
}
extension UpdateOrderVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 5
        case 1:
            return selectedMedArr.count + 1
        case 2:
            return selNonPreMedArr.count + 1
        case 3:
            return 9
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = PrescribedCell()
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! PrescribedCell
                
                let title1 = cell.contentView.viewWithTag(1) as! UILabel
                let name1 = cell.contentView.viewWithTag(2) as! UILabel
                
                let title2 = cell.contentView.viewWithTag(3) as! UILabel
                let name2 = cell.contentView.viewWithTag(4) as! UILabel
                
                title1.text = "ORDER ID"
                title2.text = "ORDER DATE"
                
                name1.text = String(orderData.id!)
                name2.text = orderData.estimated_date
                
                return cell
                
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "typeCell") as! PrescribedCell
                //prescription_type
                cell.delegate = self
                
                let segmentedControl = cell.contentView.viewWithTag(1) as! UISegmentedControl
                segmentedControl.setTitle("Prescribed", forSegmentAt: 0)
                segmentedControl.setTitle("Non Prescribed", forSegmentAt: 1)
                segmentedControl.selectedSegmentIndex = (orderData.prescription_type == "Prescribed") ? 0 : 1
                segmentedControl.addTarget(cell, action: #selector(cell.onPrescTypeChanged(_ :)), for:.valueChanged)
                return cell
                
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! PrescribedCell
                cell.delegate = self
                cell.setup(id: "chemist", placeHolder: "Chemist Name")
                cell.pickerDataArr = self.chemistArr
                
                if let chemistid = orderData.chemist_id {
                    let arr = self.chemistArr.filter{ $0.id == chemistid }
                    if arr.count > 0 {
                        cell.txtFld.text = arr[0].clinic_lab_hospital_name
                    }
                }
                return cell
                
            case 3:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                cell.delegate = self
                cell.setup(id: "doctor", placeHolder: "Doctor Name")
                
                if let drname = orderData.doctor_name {
                    
                    cell.bottoTxtFld.text = drname
                }
                return cell
                
            case 4:
                
                if self.prescriptionImg != nil {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! PrescribedCell
                    cell.prescriptionImg = self.prescriptionImg
                    cell.delegate = self
                    cell.collView.reloadData()
                    
                    return cell
                } else if !self.prescriptionImgUrl.isEmpty && self.prescriptionImgUrl != "null" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! PrescribedCell
                    cell.prescriptionImgURL = self.prescriptionImgUrl
                    cell.delegate = self
                    cell.collView.reloadData()
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "upCell") as! PrescribedCell
                    cell.delegate = self
                    return cell
                }
                
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case selectedMedArr.count:
                let cell = tableView.dequeueReusableCell(withIdentifier: "addMedCell") as! PrescribedCell
                cell.setup(id: "addMed", placeHolder: "")
                cell.delegate = self
                return cell
            default:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "medListCell") as! PrescribedCell
                cell.delegate = self
                if selectedMedArr.count > 0 {
                    let medicine = selectedMedArr[indexPath.row]
                    cell.lblMedName.text = medicine.medicine_name
                    cell.stepper.tag = indexPath.row
                    cell.stepper.count = CGFloat(medicine.quantity)
                    //cell.stepper.delegate = self
                }
                return cell
            }
        case 2:
            switch indexPath.row {
            case selNonPreMedArr.count:
                let cell = tableView.dequeueReusableCell(withIdentifier: "addMedCell") as! PrescribedCell
                cell.setup(id: "addItem", placeHolder: "")
                cell.delegate = self
                
                return cell
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "medListCell") as! PrescribedCell
                cell.delegate = self
                if selNonPreMedArr.count > 0 {
                    let medicine = selNonPreMedArr[indexPath.row]
                    cell.lblMedName.text = medicine.medicine_name
                    cell.stepper.tag = indexPath.row
                    cell.stepper.count = CGFloat(medicine.quantity)
                    //cell.stepper.delegate = self
                }
                return cell
            }
        case 3:
            
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                cell.delegate = self
                cell.setup(id: "instruction", placeHolder: "")
                
                return cell
                
            case 1:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "typeCell") as! PrescribedCell
                let segmentedControl = cell.contentView.viewWithTag(1) as! UISegmentedControl
                segmentedControl.setTitle("Delivery", forSegmentAt: 0)
                segmentedControl.setTitle("Pickup", forSegmentAt: 1)
                segmentedControl.selectedSegmentIndex = deliveryTypeArr.firstIndex(of: orderData!.delivery_type!)!
                cell.delegate = self
                segmentedControl.addTarget(cell, action: #selector(cell.onDeliveryTypeChanged(_ :)), for:.valueChanged)
                
                return cell
                
            case 2:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "radioCell") as! PrescribedCell
                cell.setup(id: "radio", placeHolder: "")
                
                if let recname = orderData.recipient_name, !recname.isEmpty {
                    
                    cell.recepient.selectedIndex = 1
                } else {
                    cell.recepient.selectedIndex = 0
                }
                cell.delegate = self
                
                return cell
                
            case 3:
                if self.recType == "Self" {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                    cell.delegate = self
                    cell.setup(id: "recipient_self", placeHolder: "")
                    cell.bottoTxtFld.text = self.orderData.getRecepient()
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! PrescribedCell
                    cell.pickerDataArr = self.familyMembersArr
                    cell.delegate = self
                    cell.setup(id: "recipient_family", placeHolder: "")
                    cell.txtFld.text = self.orderData.getRecepient()
                    return cell
                }
                
                
                
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                cell.setup(id: "flat", placeHolder: "FLAT/HOUSE NUMBER")
                cell.bottoTxtFld.text = self.orderData.flat_house_no ?? ""
                cell.delegate = self
                
                
                return cell
                
            case 5:
                let cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                cell.setup(id: "address", placeHolder: "DELIVERY ADDRESS")
                cell.bottoTxtFld.text = self.orderData.customer_address ?? ""
                cell.delegate = self
                return cell
                
            case 6:
                let cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                cell.setup(id: "pincode", placeHolder: "PINCODE")
                if let pincode = orderData.pincode {
                    cell.bottoTxtFld.text = String(pincode)
                }
                
                cell.delegate = self
                return cell
                
            case 7:
                let cell = tableView.dequeueReusableCell(withIdentifier: "dateTimeCell") as! PrescribedCell
                cell.pickerDataArr = self.slotArr
                cell.setup(id: "date", placeHolder: "")
                cell.txtFld.text = self.orderData.estimated_date
                cell.txtFld1.text = self.orderData.slot
                cell.delegate = self
                return cell
                
            case 8:
                let cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! PrescribedCell
                cell.delegate = self
                return cell
                
            default:
                break
            }
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            if indexPath.row == 3 {
                return self.orderData.prescription_type == "Non Prescribed" ? 0 : 80
            }
            if indexPath.row == 4 {
                return self.orderData.prescription_type == "Non Prescribed" ? 0 : 150
            }
        }
        if indexPath.section == 1 {
            return self.orderData.prescription_type == "Non Prescribed" ? 0 : 80
        }
        if indexPath.section == 3 {
            if indexPath.row == 2 {
                return 100
            }
            if indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6 {
                return self.orderData.delivery_type == "Delivery" ? 80 : 0
            }
        }
        return 80
    }
}
