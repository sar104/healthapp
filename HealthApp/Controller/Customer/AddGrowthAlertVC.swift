//
//  AddGrowthAlertVC.swift
//  HealthApp
//
//  Created by Apple on 07/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD


struct ChildGrowthData {
    
    var strAge: String?
    var age: Int?
    var weight: Double?
    var height: Double?
    
    static var arrBoyWeight = [3.3,6,7.8,9.2,10.2,12.3,14.6,16.7,18.7,20.7,22.9,25.3,28.1,31.4]
    static var arrGirlWeight = [3.3,5.4,7.2,8.6,9.5,11.8,14.1,16.0,17.7,19.5,21.8,24.5,28.5,32.5]
    static var arrBoyHeight = [50.5,61.1,67.8,72.3,76.1,85.6,94.9,102.9,109.9,116.1,121.7,127,132.2,137.5]
    static var arrGirlHeight = [49.9,60.2,66.6,71.1,75,84.5,93.9,101.6,108.4,114.6,120.6,126.4,132.2,138.3]
    //static var arrStrAge = ["birth","3 months","6 months","9 months","1 year"]
    
}
class AddGrowthAlertVC: BaseVC, AddGrowthAlertCellDelegate {
    
    @IBOutlet weak var tblData: UITableView!
    
    var familyMembersArr: [FamilyMemberData] = []
    var selMember: FamilyMemberData?
    var boyGrowthArr: [ChildGrowthData] = []
    var girlGrowthArr: [ChildGrowthData] = []
    var weight: String?
    var height: String?
    var medAlertData: MedAlertData = MedAlertData()
    var arrAlertTimings: [AlertTiming] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        for i in 0..<ChildGrowthData.arrBoyWeight.count {
            
            var growth = ChildGrowthData()
            
            if i == 0 {
                
                growth.age = 0
                growth.strAge = "Birth"
            } else if i < 4 {
                growth.age = i*3
                growth.strAge = String(i*3) + " months"
            } else {
                growth.age = (i-3)*12
                growth.strAge = String(i-3) + " year"
            }
            
            growth.weight = ChildGrowthData.arrBoyWeight[i]
            growth.height = ChildGrowthData.arrBoyHeight[i]
            
            boyGrowthArr.append(growth)
        }
        for i in 0..<ChildGrowthData.arrGirlWeight.count {
            
            var growth = ChildGrowthData()
            
            if i == 0 {
                
                growth.age = 0
                growth.strAge = "Birth"
            } else if i < 4 {
                growth.age = i*3
                growth.strAge = String(i*3) + " months"
            } else {
                growth.age = (i-3)*12
                growth.strAge = String(i-3) + " year"
            }
            
            growth.weight = ChildGrowthData.arrGirlWeight[i]
            growth.height = ChildGrowthData.arrGirlHeight[i]
            
            girlGrowthArr.append(growth)
        }
        getFamilyMembers()
    }
    

    func getFamilyMembers() {
        
        IHProgressHUD.show()
        
        let params = ["user_id": loggedUser?.id] as [String: Any]
        apiManager.getFamilyMember(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    
                    self.familyMembersArr = memberData.filter{ ($0.dob!.toDate(format:df_dd_MM_yyyy).getDiff() <= 10) }
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func validate() -> Bool {
        
        if selMember?.name == nil  {
            self.alert(strTitle: strErr, strMsg: "Please select child")
            return false
        }
        if weight == nil {
            self.alert(strTitle: strErr, strMsg: "Please enter weight")
            return false
        }
        if height == nil {
            self.alert(strTitle: strErr, strMsg: "Please enter height")
            return false
        }
        return true
    }
    
    func setGrowthAlertData(reportId: String) {
        
        medAlertData.user_id = Int(loggedUser!.id)
        medAlertData.alert_type = kAlertTypeChild
        medAlertData.medicine_name = kGrowthTracker
        
        do {
            let med_unit = ["member_id" : selMember?.id, "member_name": selMember?.name, "member_dob" : selMember?.dob, "report_id": reportId] as [String : Any]
            let medunitData = try JSONSerialization.data(withJSONObject: med_unit, options: [])
            let jsonStrMedUnit = String(data: medunitData, encoding: .utf8)
            medAlertData.medicine_unit = jsonStrMedUnit
            
            // prior 2 days
            let components = DateComponents(calendar: Calendar.current, year: Date().get(.year), month: Date().get(.month), day: Date().get(.day), hour: 10, minute: 0, second: 0)
            let date = Calendar.current.date(from: components)
            let alertdate = date!.dateByAdding(component: .month, value: 1)
            
            var alertTiming = AlertTiming()
            alertTiming.timming = alertdate.toString(format: df_dd_MM_yyyy)
            let millisec = alertdate.convertToMilli()
            alertTiming.time_in_miles = String(millisec)
            
            var timeCategory = TimeCategory()
            timeCategory.alert_name = growthAlertTitle + alertTiming.timming!
            timeCategory.alert_text = growthAlertText
            timeCategory.alert_status =   "pending"
            
            alertTiming.timecategory = timeCategory
            
            let timeData = try JSONEncoder().encode(timeCategory)
            let jsonStrTime = String(data: timeData, encoding: .utf8)
            
            alertTiming.time_category = jsonStrTime
            arrAlertTimings.append(alertTiming)
            
            
        } catch let err {
            
        }
    }
    
    func setNotifications() {
        
        var arrNotif: [LocalNotification] = []
        let manager = LocalNotificationManager()
        
        for alert in arrAlertTimings {
            
            let strdate = alert.timming
            let mutableStr = NSMutableString()
            
            mutableStr.append(kAlertTypeChild)
            mutableStr.append("_")
            mutableStr.append(strdate!)
            
            let notifId = mutableStr as String
            
            let alertTitle = alert.timecategory?.alert_text
            let alertDate = strdate?.toDate(format: df_dd_MM_yyyy)
            
            let notif =
                LocalNotification(id: notifId, title: alertTitle!,
                                  datetime: DateComponents(calendar: Calendar.current, year: alertDate!.get(.year), month: alertDate!.get(.month), day: alertDate!.get(.day)))
        
            arrNotif.append(notif)
        }
        
        //print("notification\(arrNotif)")
        manager.notifications = arrNotif
        manager.schedule()
    }
    
    func addGrowthTrackerAlert(){
        
        do {
            
            let alertTimingData = try JSONEncoder().encode(arrAlertTimings)
            
            let jsonStrAlertTiming = String(data: alertTimingData, encoding: .utf8)
            
            var params: [String: Any] = [:]
            
            params["user_id"] = loggedUser?.id
            params["medicine_name"] = medAlertData.medicine_name
            params["medicine_unit"] = medAlertData.medicine_unit
            params["alert_timmings"] = jsonStrAlertTiming
            params["alert_type"] = medAlertData.alert_type
            
            print("params: \(params)")
            
            IHProgressHUD.show()
            apiManager.addMedicineAlert(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
            
        } catch let err {
            print(err)
        }
    }
    
    func addMedicalHistory(){
         
         IHProgressHUD.show()
         
         do {
             let sharing = 1
             
             let userid = loggedUser!.id
            var childId = selMember?.id
            let dateOfReport = Date().toString(format: df_dd_MM_yyyy)
             
            /*
             jobj2.put("name", "Weight");
                            jobj2.put("name1", "Height");
                            jobj2.put("name2", "Age");
                            jobj2.put("name3", "");
                            jobj2.put("description", "" + weight);
                            jobj2.put("unit", "Kg");
                            jobj2.put("description1", "" + height);
                            jobj2.put("description2", "" + age_of_member);
                            jobj2.put("description3", "");
                            jobj2.put("unit1", "Centimeters");
                            jobj2.put("unit2", "Months");
                            jobj2.put("unit3", "");
             **/
            let age = (selMember?.dob?.toDate(format: df_dd_MM_yyyy).getDiff())! * 12
            let med_unit = ["name" : "Weight", "name1": "Height", "name2" : "Age", "name3" : "", "description": weight, "unit": "Kg","description1": height, "description2": age, "description3": "", "unit1": "Centimeters", "unit2": "Months","unit3": ""] as [String : Any]
            let medunitData = try JSONSerialization.data(withJSONObject: med_unit, options: [])
            let jsonStrMedUnit = String(data: medunitData, encoding: .utf8)
             
            let imageNameArr: [String] = []
             let jsonImgs: Data = try JSONSerialization.data(withJSONObject: imageNameArr, options: [])
             //JSON(imageNameArr)
             
             let imgArrStr = String(data:jsonImgs,encoding: .utf8)
             
             let params: [String:Any] = [PARAM_USERID: userid,
                                           PARAM_CHILDID:  childId,
                                           PARAM_SHARINGTYPE: sharing,
                 PARAM_DOCTORNAME: "-",
             PARAM_ILLNESSTYPE: "Growth Tracker Report",
             PARAM_DESCRIPTION: jsonStrMedUnit,
             PARAM_ILLNESSDATE: dateOfReport,
             PARAM_IMAGES: imgArrStr]
             
             
             print("params: \(params)")
             
         let AppURL = baseURL + AddMedicalHistoryAPI
        
         let postData = try JSONSerialization.data(withJSONObject: params, options: [])
         var request = URLRequest(url: URL(string:AppURL)!)
         request.httpMethod = "POST"
         request.setValue("application/json", forHTTPHeaderField: "Content-Type")
         request.allHTTPHeaderFields = ["Content-Type":"application/json"]
         request.httpBody = postData as Data
             
             apiManager.addMedicalHistoryAPI(params: params) { (result) in
                 
                 IHProgressHUD.dismiss()
                 switch result {
                     case .success(let data):
                        
                         self.alert(strTitle: strSuccess, strMsg: data.message!)
                         self.setGrowthAlertData(reportId: "1")//data.data!)
                            
                         self.setNotifications()
                         self.addGrowthTrackerAlert()
                    
                     case .failure(let err):
                         print("err: \(err)")
                         self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                 }
             }
    
         } catch let err {
             print(err)
         }
        
     }
    
    func onDataSelected(id: String, value: FamilyMemberData){
       
        self.selMember = value
        
        self.tblData.reloadData()
    }
    
    func onValueChanged(id: String, value: String) {
        
        if id == "weight" {
            weight = value
        }
        if id == "height" {
            height = value
        }
    }
    
    func onSaveClicked(id: Int) {
        
        /*
         float standard_height = getStandardHeight(age_of_member, "" + child_gender);
                     float standard_weight = getStandardWeight(age_of_member, "" + child_gender);
         
                     String category = "";
                     if (d_weight < standard_weight) {
                         category += "weight " + weight + "(Kg) is below Standard Weight " + standard_weight + "(Kg)";
                     } else if (d_weight > standard_weight) {
                         category += "weight " + weight + "(Kg) is above Standard Weight " + standard_weight + "(Kg)";
                     }
         
                     if (d_height < standard_height) {
                         category += "\nheight " + height + "(Cm) is below Standard Height " + standard_height + "(Cm)";
                     } else if (d_height > standard_height) {
                         category += "\nheight " + height + "(Cm) is above Standard Height " + standard_height + "(Cm)";
                     }
         
                     builder.setMessage("Your child's " + category + ".\n\n\nDo you want to save result?");
         **/
        
        if id == 0 {
            
            let vc = self.getVC(with: "AddFamilyMemberVC", sb: IDMain)
            self.navigationController?.pushViewController(vc, animated: false)
            
        } else {
            if validate() {
                
                let age = (selMember?.dob!.toDate(format:df_dd_MM_yyyy).getDiff())! * 12
                let stdWt = self.getStdWeight(gender: selMember!.gender!, age: age)
                let stdHt = self.getStdHeight(gender: selMember!.gender!, age: age)
                
                var category = ""
                if Double(weight!)! < stdWt {
                    
                    category = "weight " + weight!
                    category += "(Kg) is below Standard Weight " + String(stdWt) + "(Kg)"
                }
                if Double(weight!)! > stdWt {
                    category = "weight " + weight!
                    category += "(Kg) is above Standard Weight " + String(stdWt) + "(Kg)"
                }
                if (Double(height!)! < stdHt) {
                    category += "\nheight " + height!
                    category += "(Cm) is below Standard Height " + String(stdHt) + "(Cm)"
                    
                } else if (Double(height!)! > stdHt) {
                    category += "\nheight " + height!
                    category += "(Cm) is above Standard Height " + String(stdHt)  + "(Cm)"
                }
                
                let msg = "Your child's " + category + ".\n\n\nDo you want to save result?"
                
                self.alertAction(strTitle: "Growth Tracker Result", strMsg: msg) { (action) in
                    
                    self.addMedicalHistory()
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddGrowthAlertVC {
    
    func getStdWeight(gender: String, age: Int) -> Double {
        
        switch gender {
        case "Male":
            
            switch age {
            case 0...2:
                return Double(ChildGrowthData.arrBoyWeight[0])
            case 3...5:
                return Double(ChildGrowthData.arrBoyWeight[1])
            case 6...8:
                return Double(ChildGrowthData.arrBoyWeight[2])
            case 9...11:
                return Double(ChildGrowthData.arrBoyWeight[3])
            case 12...23:
                return Double(ChildGrowthData.arrBoyWeight[4])
            case 24...35:
                return Double(ChildGrowthData.arrBoyWeight[5])
            case 36...47:
                return Double(ChildGrowthData.arrBoyWeight[6])
            case 48...59:
                return Double(ChildGrowthData.arrBoyWeight[7])
            case 60...71:
                return Double(ChildGrowthData.arrBoyWeight[8])
            case 72...83:
                return Double(ChildGrowthData.arrBoyWeight[9])
            case 84...95:
                return Double(ChildGrowthData.arrBoyWeight[10])
            case 96...107:
                return Double(ChildGrowthData.arrBoyWeight[11])
            case 108...119:
                return Double(ChildGrowthData.arrBoyWeight[12])
            case 120...:
                return Double(ChildGrowthData.arrBoyWeight[13])
            default:
                break
            }
            
        case "Female":
            switch age {
            case 0...2:
                return Double(ChildGrowthData.arrGirlWeight[0])
            case 3...5:
                return Double(ChildGrowthData.arrGirlWeight[1])
            case 6...8:
                return Double(ChildGrowthData.arrGirlWeight[2])
            case 9...11:
                return Double(ChildGrowthData.arrGirlWeight[3])
            case 12...23:
                return Double(ChildGrowthData.arrGirlWeight[4])
            case 24...35:
                return Double(ChildGrowthData.arrGirlWeight[5])
            case 36...47:
                return Double(ChildGrowthData.arrGirlWeight[6])
            case 48...59:
                return Double(ChildGrowthData.arrGirlWeight[7])
            case 60...71:
                return Double(ChildGrowthData.arrGirlWeight[8])
            case 72...83:
                return Double(ChildGrowthData.arrGirlWeight[9])
            case 84...95:
                return Double(ChildGrowthData.arrGirlWeight[10])
            case 96...107:
                return Double(ChildGrowthData.arrGirlWeight[11])
            case 108...119:
                return Double(ChildGrowthData.arrGirlWeight[12])
            case 120...:
                return Double(ChildGrowthData.arrGirlWeight[13])
            default:
                break
            }
        default:
            break
        }
        
        return 0
    }
    
    func getStdHeight(gender: String, age: Int) -> Double {
        
        switch gender {
        case "Male":
            
            switch age {
            case 0...2:
                return Double(ChildGrowthData.arrBoyHeight[0])
            case 3...5:
                return Double(ChildGrowthData.arrBoyHeight[1])
            case 6...8:
                return Double(ChildGrowthData.arrBoyHeight[2])
            case 9...11:
                return Double(ChildGrowthData.arrBoyHeight[3])
            case 12...23:
                return Double(ChildGrowthData.arrBoyHeight[4])
            case 24...35:
                return Double(ChildGrowthData.arrBoyHeight[5])
            case 36...47:
                return Double(ChildGrowthData.arrBoyHeight[6])
            case 48...59:
                return Double(ChildGrowthData.arrBoyHeight[7])
            case 60...71:
                return Double(ChildGrowthData.arrBoyHeight[8])
            case 72...83:
                return Double(ChildGrowthData.arrBoyHeight[9])
            case 84...95:
                return Double(ChildGrowthData.arrBoyHeight[10])
            case 96...107:
                return Double(ChildGrowthData.arrBoyHeight[11])
            case 108...119:
                return Double(ChildGrowthData.arrBoyHeight[12])
            case 120...:
                return Double(ChildGrowthData.arrBoyHeight[13])
            default:
                break
            }
            
        case "Female":
            switch age {
            case 0...2:
                return Double(ChildGrowthData.arrGirlHeight[0])
            case 3...5:
                return Double(ChildGrowthData.arrGirlHeight[1])
            case 6...8:
                return Double(ChildGrowthData.arrGirlHeight[2])
            case 9...11:
                return Double(ChildGrowthData.arrGirlHeight[3])
            case 12...23:
                return Double(ChildGrowthData.arrGirlHeight[4])
            case 24...35:
                return Double(ChildGrowthData.arrGirlHeight[5])
            case 36...47:
                return Double(ChildGrowthData.arrGirlHeight[6])
            case 48...59:
                return Double(ChildGrowthData.arrGirlHeight[7])
            case 60...71:
                return Double(ChildGrowthData.arrGirlHeight[8])
            case 72...83:
                return Double(ChildGrowthData.arrGirlHeight[9])
            case 84...95:
                return Double(ChildGrowthData.arrGirlHeight[10])
            case 96...107:
                return Double(ChildGrowthData.arrGirlHeight[11])
            case 108...119:
                return Double(ChildGrowthData.arrGirlHeight[12])
            case 120...:
                return Double(ChildGrowthData.arrGirlHeight[13])
            default:
                break
            }
        default:
            break
        }
        
        return 0
    }
}
extension AddGrowthAlertVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if selMember != nil {
            return 3
        }
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }
        if section == 1 {
            return ChildGrowthData.arrBoyWeight.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = AddGrowthAlertCell()
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! AddGrowthAlertCell
                cell.delegate = self
                cell.pickerDataArr = familyMembersArr
                if let data = selMember {
                    cell.lblDob.text = data.dob! + "  (Age: " + String(data.dob!.toDate(format:df_dd_MM_yyyy).getDiff()) + ")"
                    cell.lblGender.text = data.gender
                }
                cell.setup(id: "child")
                
                return cell
            }
            if indexPath.row == 1 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! AddGrowthAlertCell
                cell.delegate = self
                cell.setup(id: "weight")
                
                return cell
            }
            if indexPath.row == 2 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! AddGrowthAlertCell
                cell.delegate = self
                cell.setup(id: "height")
                
                return cell
            }
            
        }
        if indexPath.section == 1 {
            //
            let cell = tableView.dequeueReusableCell(withIdentifier: "growthCell") as! AddGrowthAlertCell
                           cell.delegate = self
             
            if selMember?.gender == "Female" {
                let data = girlGrowthArr[indexPath.row]
                
                cell.lblAge.text = data.strAge
                cell.lblWeight.text = String(data.weight!)
                cell.lblHeight.text = String(data.height!)
            }
            if selMember?.gender == "Male" {
                let data = boyGrowthArr[indexPath.row]
                
                cell.lblAge.text = data.strAge
                cell.lblWeight.text = String(data.weight!)
                cell.lblHeight.text = String(data.height!)
            }
            return cell
        }
        if indexPath.section == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "btnAlertCell") as! AddGrowthAlertCell
            cell.delegate = self
            
            return cell
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "growthHeadCell") as! AddGrowthAlertCell
                           
            if selMember?.gender == "Female" {
                cell.lblGender.text = "Growth Tracker chart for girl"
            }
            if selMember?.gender == "Male" {
                cell.lblGender.text = "Growth Tracker chart for boy"
            }
            
            return cell.contentView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 1 {
            return 80
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return 200
            }
            return 55
        }
        return 50
    }
}

protocol AddGrowthAlertCellDelegate {
    
    func onDataSelected(id: String, value: FamilyMemberData)
    func onValueChanged(id: String, value: String)
    func onSaveClicked(id: Int)
}
class AddGrowthAlertCell: BaseCell {
    
    @IBOutlet weak var txtFld: RightViewTextField!
    @IBOutlet weak var lblDob: UILabel!
    
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var txtBtmLine: BottomLineTextField!
    
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    
    var delegate: AddGrowthAlertCellDelegate?
    var selRow: Int = 0
    
    func setup(id: String){
        
        if id == "child" {
            
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            txtFld.placeholder = "Select Child"
            txtFld.inputView = self.cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
        
        if id == "weight" {
            txtBtmLine.accessibilityIdentifier = id
            txtBtmLine.placeholder = "Weight"
            txtBtmLine.delegate = self
        }
        if id == "height" {
            txtBtmLine.accessibilityIdentifier = id
            txtBtmLine.placeholder = "Height"
            txtBtmLine.delegate = self
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        selRow = row
        let data = pickerDataArr[row] as? FamilyMemberData
        txtFld.text = data?.name
    }
    
    @objc override func doneCellPicker (sender: UIBarButtonItem) {
        
        let id = sender.accessibilityIdentifier
        
        if id == "child" {
            let data = pickerDataArr[selRow] as? FamilyMemberData
            txtFld.text = data?.name
            lblDob.text = data?.dob
            delegate?.onDataSelected(id: id!, value: data!)
            txtFld.resignFirstResponder()
        }
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        delegate?.onSaveClicked(id: sender.tag)
    }
}
extension AddGrowthAlertCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        /*
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(valueChanged),
            object: textField)*/
        self.perform(
            #selector(valueChanged),
            with: textField,
            afterDelay: 0.8)
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @objc func valueChanged(textField: UITextField) {
        
        delegate?.onValueChanged(id: textField.accessibilityIdentifier!, value: textField.text!)
    }
}
