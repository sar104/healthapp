//
//  AddFamilyHealthRecordVC.swift
//  HealthApp
//
//  Created by Apple on 07/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class AddFamilyHealthRecordVC: BaseVC {

    
    @IBOutlet weak var segmentedControl: MXSegmentedControl!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var addFamilyView: UIView!
    @IBOutlet weak var con_imgW: NSLayoutConstraint!
    @IBOutlet weak var con_btnW: NSLayoutConstraint!
    @IBOutlet weak var con_titleTop: NSLayoutConstraint!
    
    var familyMembersArr: [FamilyMemberData] = []
    var user: User?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let device = ""
        
        switch UIDevice.modelName {
            
        case device + "iPhone 5", device + "iPhone 5c", device + "iPhone SE", device + "iPhone 5s":
            con_imgW.constant = 150
            con_btnW.constant = 35
            
        default:
            break
        }
        // Do any additional setup after loading the view.
        let users = self.fetchUser()
        user = users[0]
        
        getFamilyMembers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        segmentedControl.removeAll()
        segmentedControl.isHidden = true
        addFamilyView.isHidden = true
        
        /*
        if let HealthRecordContainerVC = self.getVC(with: "HealthRecordContainerVC", sb: IDMain) as? HealthRecordContainerVC {
            
            HealthRecordContainerVC.willMove(toParent: self)
            self.containerView.addSubview(HealthRecordContainerVC.view)
            self.addChild(HealthRecordContainerVC)
            HealthRecordContainerVC.didMove(toParent: self)
        }*/
    }
    
    func setUp() {
        
        if self.familyMembersArr.count > 0 {
            
            addFamilyView.isHidden = true
            segmentedControl.isHidden = false
            
            segmentedControl.append(title: "Report")
                .set(titleColor: .white, for: .selected)
            segmentedControl.append(title: "Prescription")
                .set(titleColor: .white, for: .selected)
            
            segmentedControl.select(index: 0, animated: false)
        } else {
            addFamilyView.isHidden = false
            segmentedControl.isHidden = true
            
            if let btnAdd = view.viewWithTag(10) as? UIButton {
                
                btnAdd.addTarget(self, action: #selector(addFamilyMember(_:)), for: .touchUpInside)
            }
        }
    }
    
    @IBAction func addFamilyMember(_ sender: UIButton) {
        
        let vc = self.getVC(with: "AddFamilyMemberVC", sb: IDMain) as! AddFamilyMemberVC
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func getFamilyMembers() {
        
        IHProgressHUD.show()
        
        let params = ["user_id": user?.id] as [String: Any]
        apiManager.getFamilyMember(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    
                    self.familyMembersArr = memberData
                    self.setUp()
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segueHealthRecord" {
            
            if let vc = segue.destination as? HealthRecordContainerVC {
                
                //vc.recordType = "Family"
            }
        }
    }
    

}
