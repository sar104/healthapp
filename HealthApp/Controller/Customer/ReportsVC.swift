//
//  ReportsVC.swift
//  HealthApp
//
//  Created by Apple on 31/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD
import WebKit

class ReportsVC: BaseVC {

    @IBOutlet weak var tblData: UITableView!
    
    var familyMembersArr: [FamilyMemberData] = []
    var reportsArr: [MedicalHistoryData] = []
    var type: String?
    var child_id: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if type == "self" {
            getReportsList()
        } else if type == "family" {
            getFamilyMembers() 
            getFamilyReportsList()
        } else if type == "familyMember" {
            getFamilyMembers()
            getFamilyMemberReportsList()
        }
    }
    

    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        
        let id = sender.view?.accessibilityIdentifier
        if id == "pdf" {
            
            let tag = sender.view?.tag
            let data = self.reportsArr[tag!]
            if let strurl = data.images[0].image {
                
                if let url = URL(string: strurl) {
                    let webView = WKWebView(frame: view.frame)
                    let urlRequest = URLRequest(url: url)
                    webView.load(urlRequest)
                    let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
                    webView.addGestureRecognizer(tap)
                    view.addSubview(webView)
                }
            }
            
        } else {
            let imageView = sender.view as! UIImageView
            
            let newImageView = UIImageView(image: imageView.image)
            newImageView.frame = UIScreen.main.bounds
            newImageView.backgroundColor = .black
            newImageView.contentMode = .scaleAspectFit
            newImageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
            newImageView.addGestureRecognizer(tap)
            self.view.addSubview(newImageView)
            self.navigationController?.isNavigationBarHidden = true
        }
        //self.tabBarController?.tabBar.isHidden = true
    }

    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        //self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    
    func getReportsList() {
        
        let params = [PARAM_USERID: loggedUser?.id] as [String:Any]
        
        IHProgressHUD.show()
        
        apiManager.getMyReportsList(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    do {
                        let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                        
                        let jsonmed = jsondata["data"] as! [[String:Any]]
                        
                        for param in jsonmed {
                            
                            var reportData = MedicalHistoryData()
                            reportData.parseData(params: param)
                            self.reportsArr.append(reportData)
                        }
                        
                        
                    } catch let err {
                       
                    }
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    func getFamilyReportsList() {
        
        let params = [PARAM_USERID: loggedUser?.id] as [String:Any]
        
        IHProgressHUD.show()
        
        apiManager.getMyFamilyReportList(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    do {
                        let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                        
                        let jsonmed = jsondata["data"] as! [[String:Any]]
                        
                        for param in jsonmed {
                            
                            var reportData = MedicalHistoryData()
                            reportData.parseData(params: param)
                            self.reportsArr.append(reportData)
                        }
                        
                        
                    } catch let err {
                       
                    }
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func getFamilyMemberReportsList() {
        
        let params = ["child_id": child_id] as [String:Any]
        
        IHProgressHUD.show()
        
        apiManager.getFamilyReportList(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    do {
                        let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                        
                        let jsonmed = jsondata["data"] as! [[String:Any]]
                        
                        for param in jsonmed {
                            
                            var reportData = MedicalHistoryData()
                            reportData.parseData(params: param)
                            self.reportsArr.append(reportData)
                        }
                        
                        
                    } catch let err {
                       
                    }
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    @IBAction func addAction(_ sender: UIButton) {
        
        let vc = self.getVC(with: "AddHealthRecordSegmentVC", sb: "Main") as! AddHealthRecordSegmentVC
        vc.selIndex = 0
        UserDefaults.standard.set(0, forKey: "HealthRecordSegment")
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func viewAction(id: Int) {
        
        let data = reportsArr[id]
        
        let vc = self.getVC(with: "UpdateReportVC", sb: IDMain) as! AddReportVC
        vc.viewFlag = true
        vc.medicalHistoryData = data
        vc.recordType = (type == "self") ? kMine : kFamily
        //
        self.navigationController?.pushViewController(vc, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ReportsVC: UITableViewDataSource, UITableViewDelegate, PrescriptionCellDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.reportsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "prescriptionCell") as! PrescriptionCell
        
        let data = self.reportsArr[indexPath.row]
        //cell.btnFav.tag = indexPath.row
        //cell.btnOrder.tag = indexPath.row
        cell.delegatePresc = self
        cell.row = indexPath.row
        cell.imgVw.tag = indexPath.row
        cell.imgVw.accessibilityIdentifier = "img"
        
        if data.images.count > 0 {
            
            if let img = data.images[0].image {
                if img.contains("pdf") {
                    cell.imgVw.accessibilityIdentifier = "pdf"
                } else {
                    cell.imgVw.accessibilityIdentifier = "img"
                }
            }
        }
        cell.populateMedData(data: data)
        cell.imgVw.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        cell.imgVw.addGestureRecognizer(tap)
        
        
        if let btnShare = cell.contentView.viewWithTag(1) as? UIButton {
            btnShare.tag = indexPath.row
            btnShare.addTarget(cell, action: #selector(cell.shareAction), for: .touchUpInside)
        }
        cell.btnOrder.isHidden = true
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        //self.onEditCliked(id: indexPath.row)
        self.viewAction(id: indexPath.row)
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        (cell as! PrescriptionCell).bgView.setDashBorder(borderColor: UIColor.gray)
    }
    
    func onPlaceOrderClicked(id: Int){
        
       
    }
    
    func onShareCliked(id: Int) {
        
        let data = reportsArr[id]
        
        let vc = self.getVC(with: "ShareEHRVC", sb: IDMain) as! ShareEHRVC
        vc.EHR_Id = data.id
        vc.type = "Report"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func onEditCliked(id: Int) {
        
        if type != "self" {
            if self.familyMembersArr.count > 0 {
                let data = reportsArr[id]
                
                let vc = self.getVC(with: "UpdateReportVC", sb: IDMain) as! AddReportVC
                vc.editFlag = true
                vc.medicalHistoryData = data
                vc.recordType = (type == "self") ? kMine : kFamily
                //
                self.navigationController?.pushViewController(vc, animated: false)
            } else {
                let vc = self.getVC(with: "AddFamilyMemberVC", sb: IDMain) as! AddFamilyMemberVC
                self.navigationController?.pushViewController(vc, animated: false)
            }
        } else {
            let data = reportsArr[id]
            
            let vc = self.getVC(with: "UpdateReportVC", sb: IDMain) as! AddReportVC
            vc.editFlag = true
            vc.medicalHistoryData = data
            vc.recordType = (type == "self") ? kMine : kFamily
            //
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    
    
    func getFamilyMembers() {
        
        IHProgressHUD.show()
        
        let users = self.fetchUser()
        let user = users[0]
        let params = ["user_id": user.id] as [String: Any]
        apiManager.getFamilyMember(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    
                    self.familyMembersArr = memberData
                    
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
}

