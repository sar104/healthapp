//
//  AddVaccineAlertVC.swift
//  HealthApp
//
//  Created by Apple on 30/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class AddVaccineAlertVC: BaseVC, AddVaccineAlertCellDelegate {

    let alertTitleArr = [vaccineAlertTitle_0,vaccineAlertTitle_1,vaccineAlertTitle_2,vaccineAlertTitle_3]
    var vaccineArr: [VaccineAlertData] = []
    var filteredVaccineArr: [VaccineAlertData] = []
    var familyMembersArr: [FamilyMemberData] = []
    var selMember: FamilyMemberData = FamilyMemberData()
    
    @IBOutlet weak var tblData: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblData.estimatedRowHeight = 50
        getFamilyMembers()
        
    }
    
    func getFamilyMembers() {
        
        IHProgressHUD.show()
        
        let params = ["user_id": loggedUser?.id] as [String: Any]
        apiManager.getFamilyMember(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    
                    self.familyMembersArr = memberData.filter{ ($0.dob!.toDate(format:df_dd_MM_yyyy).getDiff() <= 3) }
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func parseVaccineData() {
        
        let dataStr = self.parseCSV(file: "vaccine")
        //print("datastr: \(dataStr)")
        
        for data in dataStr {
            
            if data[0] != "\n" {
                
                let date: String = data[0].replacingOccurrences(of: "\n", with: "")
                let arr = date.components(separatedBy: "_")
                let year = arr[0]
                let month = arr[1]
                let day = arr[2]
                
                let vac_name = data[1]
                let count = data[2]
                
                var vaccData = VaccineAlertData()
                vaccData.year = Int(year)
                vaccData.month = Int(month)
                vaccData.day = Int(day)
                vaccData.vaccine_name = vac_name
                vaccData.count = Int(count)
                vaccData.alertTitle = alertTitleArr[vaccData.count!]
                if Int(year)! >= 10 {
                    vaccData.gender = "Female"
                }
                
                vaccData.setAlertDate(dob: selMember.dob!.toDate(format: df_dd_MM_yyyy))
                vaccData.setStrAlertDate()
                vaccData.setAlertTitle(childName: selMember.name!)
                
                if vaccData.gender == selMember.gender! {
                    self.vaccineArr.append(vaccData)
                }
            }
        }
    }
    
    func setNotifications() {
        
        var arrNotif: [LocalNotification] = []
        let manager = LocalNotificationManager()
        
        for alert in vaccineArr {
            
            let strdate = alert.strAlertDate
            let mutableStr = NSMutableString()
            
            mutableStr.append(kAlertTypeVac)
            mutableStr.append("_")
            mutableStr.append(selMember.name!)
            mutableStr.append("_")
            mutableStr.append(alert.vaccine_name!)
            mutableStr.append("_")
            mutableStr.append(strdate!)
            
            let notifId = mutableStr as String
            
            let notif =
                LocalNotification(id: notifId, title: alert.alertTitle!,
                                  datetime: DateComponents(calendar: Calendar.current, year: alert.alertDate!.get(.year), month: alert.alertDate!.get(.month), day: alert.alertDate!.get(.day)))
        
            arrNotif.append(notif)
        }
        
        //print("notification\(arrNotif)")
        manager.notifications = arrNotif
        manager.schedule()
    }
    
    func addVaccineAlert() {
        
        do {
            var medAlertData = MedAlertData()
            
            medAlertData.medicine_name = "Vaccination Tracker"
            
            var alertTimingArr: [AlertTiming] = []
            
            for alert in vaccineArr {
                
                var alertTiming = AlertTiming()
                alertTiming.timming = alert.strAlertDate
                let millisec = alert.alertDate!.convertToMilli()
                alertTiming.time_in_miles = String(millisec)
                
                var timeCategory = TimeCategory()
                timeCategory.alert_name = alert.vaccine_name
                timeCategory.alert_text = alert.alertTitle
                timeCategory.alert_status = Int(alert.count!) != 2 ? "hidden" : "pending"
                
                let timeData = try JSONEncoder().encode(timeCategory)
                let jsonStrTime = String(data: timeData, encoding: .utf8)
                
                alertTiming.time_category = jsonStrTime
                alertTimingArr.append(alertTiming)
            }
            let med_unit = ["member_id": selMember.id!,"member_name": selMember.name!, "member_dob": selMember.dob!] as [String : Any]
            let medunitData = try JSONSerialization.data(withJSONObject: med_unit, options: [])
            let jsonStrMedUnit = String(data: medunitData, encoding: .utf8)
            medAlertData.medicine_unit = jsonStrMedUnit
            
            let alertTimingData = try JSONEncoder().encode(alertTimingArr)
            
            let jsonStrAlertTiming = String(data: alertTimingData, encoding: .utf8)
            
            var params: [String: Any] = [:]
            
            params["user_id"] = loggedUser?.id
            params["medicine_name"] = medAlertData.medicine_name
            params["medicine_unit"] = medAlertData.medicine_unit
            params["alert_timmings"] = jsonStrAlertTiming
            params["alert_type"] = kAlertTypeVac
            
            print("params: \(params)")
            
            IHProgressHUD.show()
            apiManager.addMedicineAlert(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
            
        } catch let err {
            print(err)
        }
    }
    
    func onDataSelected(id: String, value: FamilyMemberData){
       
        self.selMember = value
        parseVaccineData()
        filteredVaccineArr = vaccineArr.filter{ ($0.count == 2) }
        self.tblData.reloadData()
    }
    
    func validate() -> Bool {
        
        if selMember.name == nil || selMember.name!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please select child")
            return false
        }
        return true
    }
    
    func onSaveClicked(id: Int) {
        
        if id == 0 {
            
            let vc = self.getVC(with: "AddFamilyMemberVC", sb: IDMain)
            self.navigationController?.pushViewController(vc, animated: false)
        }
        if id == 1 {
            if validate() {
                setNotifications()
                addVaccineAlert()
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddVaccineAlertVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 || section == 2 {
            return 1
        }
        return filteredVaccineArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = AddVaccineAlertCell()
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! AddVaccineAlertCell
                cell.delegate = self
                cell.pickerDataArr = familyMembersArr
                cell.setup(id: "child")
                
                return cell
            }
        }
        if indexPath.section == 1 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "vaccineCell") as! AddVaccineAlertCell
            
            let vaccData = self.filteredVaccineArr[indexPath.row]
            cell.populateData(data: vaccData)
            
        }
        if indexPath.section == 2 {
            if indexPath.row == 0 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "btnAlertCell") as! AddVaccineAlertCell
                cell.delegate = self
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            (cell as! AddVaccineAlertCell).vacView.layer.borderColor = UIColor.gray.cgColor
            (cell as! AddVaccineAlertCell).dateView.layer.borderColor = UIColor.gray.cgColor
            
            (cell as! AddVaccineAlertCell).vacView.layer.borderWidth = 1.0
            (cell as! AddVaccineAlertCell).dateView.layer.borderWidth = 1.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 150
        }
        if indexPath.section == 2 {
            return 50
        }
        return UITableView.automaticDimension
    }
    
}

protocol AddVaccineAlertCellDelegate {
    
    func onDataSelected(id: String, value: FamilyMemberData)
    func onSaveClicked(id: Int)
}
class AddVaccineAlertCell: BaseCell {
    
    @IBOutlet weak var txtFld: RightViewTextField!
    @IBOutlet weak var lblDob: UILabel!
    
    @IBOutlet weak var lblVac: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var vacView: UIView!
    @IBOutlet weak var dateView: UIView!
    
    var delegate: AddVaccineAlertCellDelegate?
    var selRow: Int = 0
    
    func setup(id: String) {
        
        if id == "child" {
            
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            txtFld.placeholder = "Select Child"
            txtFld.inputView = self.cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
    }
    
    func populateData(data: VaccineAlertData){
        
        self.lblVac.text = data.vaccine_name
        self.lblDate.text = data.strAlertDate
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        selRow = row
        let data = pickerDataArr[row] as? FamilyMemberData
        txtFld.text = data?.name
    }
    
    @objc override func doneCellPicker (sender: UIBarButtonItem) {
        
        let id = sender.accessibilityIdentifier
        
        if id == "child" {
            let data = pickerDataArr[selRow] as? FamilyMemberData
            txtFld.text = data?.name
            lblDob.text = data?.dob
            delegate?.onDataSelected(id: id!, value: data!)
            txtFld.resignFirstResponder()
        }
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        delegate?.onSaveClicked(id: sender.tag)
    }
}
