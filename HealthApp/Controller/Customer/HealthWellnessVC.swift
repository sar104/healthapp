//
//  HealthWellnessVC.swift
//  HealthApp
//
//  Created by Apple on 01/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import IHProgressHUD

class HealthWellnessVC: BaseVC {

    @IBOutlet weak var tblData: UITableView!
    
    var healthArticleData: [NewsData] = []
    var fitnessArticleData: [NewsData] = []
    var newsData: [NewsData] = []
    
    let prefTitles = ["FITNESS","NUTRITION","MENTAL WELL-BEING"]
    let fitnessArr = ["Yoga","Zumba","Aerobis","Running","Trekking","Swimming","Cycling","Tennis"]
    let nutriArr = ["Any","Veg Diet","Non-Veg Diet","Egg Diet","No-Egg Diet","Vegan & Keto Diet","NINDiet"]
    let mentalArr = ["Pregnancy","Health Living","Awareness of Disease","Heart Health","Mental Health","Hygiene","Senior Citizen","Autism"]
    
    var selFitPrefArr: [String] = []
    var selNutriPrefArr: [String] = []
    var selPrefArr: [String] = []
    var prefFlag: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getNutriNews()
        getFitnessNews()
        getWellnessData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let tabbarvc = self.tabBarController as? HomeVC {
            
            tabbarvc.navigationController?.setNavigationBarHidden(false, animated: false)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        /*
        if let tabbarvc = self.tabBarController as? HomeVC {
            
            tabbarvc.navigationController?.setNavigationBarHidden(true, animated: false)
        }
        */
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    func getNutriNews() {
        
        IHProgressHUD.show()
        
        var strurl = newsAPI
        
        if selPrefArr.count > 0 {
            strurl = strurl + "q=" + selNutriPrefArr.joined(separator: "&").stringByAddingPercentEncodingForFormData()!
        } else {
            strurl = strurl + "q=diet"
        }
        
        let url = URL(string: strurl)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["x-rapidapi-host": "bing-news-search1.p.rapidapi.com", "x-rapidapi-key": "0b5821df5dmsh87476feada959c7p112786jsna3da8b71a176"]
        
        AF.request(request as URLRequestConvertible).responseJSON {
        (response) in
            
            IHProgressHUD.dismiss()
            
            print("wellnessdata:")
            print(response)
            print(response.result)
            guard let data = response.data else {
                //completion(.failure(WebServiceError.noData))
                return
            }
            if let error = response.error {
                //completion(.failure(error))
                return
            }
            //completion(.success(data))
            do {
                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                
                
                if let jsonmed = jsondata["value"] as? [[String:Any]] {
                    self.healthArticleData.removeAll()
                    
                    for param in jsonmed {
                        
                        var newsData = NewsData()
                        newsData.parseData(params: param)
                        self.healthArticleData.append(newsData)
                    }
                }
                DispatchQueue.main.async {
                    self.tblData.reloadData()
                }
                
            } catch let err {
                
            }
        }
    }
    
    func getFitnessNews() {
        
        IHProgressHUD.show()
        
        var strurl = newsAPI
        
        if selPrefArr.count > 0 {
            strurl = strurl + "q=" + selFitPrefArr.joined(separator: "&").stringByAddingPercentEncodingForFormData()!
        } else {
            strurl = strurl + "q=exercise&gym&fitness"
        }
        
        let url = URL(string: strurl)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["x-rapidapi-host": "bing-news-search1.p.rapidapi.com", "x-rapidapi-key": "0b5821df5dmsh87476feada959c7p112786jsna3da8b71a176"]
        
        AF.request(request as URLRequestConvertible).responseJSON {
        (response) in
            
            IHProgressHUD.dismiss()
            
            print("wellnessdata:")
            print(response)
            print(response.result)
            guard let data = response.data else {
                //completion(.failure(WebServiceError.noData))
                return
            }
            if let error = response.error {
                //completion(.failure(error))
                return
            }
            //completion(.success(data))
            do {
                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                
                
                if let jsonmed = jsondata["value"] as? [[String:Any]] {
                    self.fitnessArticleData.removeAll()
                    
                    for param in jsonmed {
                        
                        var newsData = NewsData()
                        newsData.parseData(params: param)
                        self.fitnessArticleData.append(newsData)
                    }
                }
                DispatchQueue.main.async {
                    self.tblData.reloadData()
                }
                
            } catch let err {
                
            }
        }
        
    }

    func getWellnessData() {
        /*
         https://bing-news-search1.p.rapidapi.com/news/search?

         parameters:
         q=diet

         headers:

         x-rapidapi-host=bing-news-search1.p.rapidapi.com
         x-rapidapi-key = 0b5821df5dmsh87476feada959c7p112786jsna3da8b71a176
         **/
        
        IHProgressHUD.show()
        
        var strurl = newsAPI
        
        if selPrefArr.count > 0 {
            strurl = strurl + "q=" + selPrefArr.joined(separator: "&").stringByAddingPercentEncodingForFormData()!
        } else {
            strurl = strurl + "q=health"
        }
        
        let url = URL(string: strurl)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["x-rapidapi-host": "bing-news-search1.p.rapidapi.com", "x-rapidapi-key": "0b5821df5dmsh87476feada959c7p112786jsna3da8b71a176"]
        
        AF.request(request as URLRequestConvertible).responseJSON {
        (response) in
            
            IHProgressHUD.dismiss()
            
            print("wellnessdata:")
            print(response)
            print(response.result)
            guard let data = response.data else {
                //completion(.failure(WebServiceError.noData))
                return
            }
            if let error = response.error {
                //completion(.failure(error))
                return
            }
            //completion(.success(data))
            do {
                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                
                
                if let jsonmed = jsondata["value"] as? [[String:Any]] {
                    self.newsData.removeAll()
                    
                    for param in jsonmed {
                        
                        var newsData = NewsData()
                        newsData.parseData(params: param)
                        self.newsData.append(newsData)
                    }
                }
                DispatchQueue.main.async {
                    self.tblData.reloadData()
                }
                
            } catch let err {
                
            }
        }
        
        
    }
    
    @IBAction func checkAction(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        let id = sender.accessibilityIdentifier
        
        if id == "fitness" {
            let data = fitnessArr[sender.tag]
            if let index = selFitPrefArr.firstIndex(of: data) {
                selFitPrefArr.remove(at: index)
            } else {
                selFitPrefArr.append(data)
            }
        }
        
        if id == "nutri" {
            let data = nutriArr[sender.tag]
            if let index = selNutriPrefArr.firstIndex(of: data) {
                selNutriPrefArr.remove(at: index)
            } else {
                selNutriPrefArr.append(data)
            }
        }
        
        if id == "mental" {
            let data = mentalArr[sender.tag]
            if let index = selPrefArr.firstIndex(of: data) {
                selPrefArr.remove(at: index)
            } else {
                selPrefArr.append(data)
            }
        }
        
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
    
        prefFlag = false
        
        self.tblData.reloadData()
        
        getNutriNews()
        getFitnessNews()
        getWellnessData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HealthWellnessVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
        if section == 4 || section == 5 || section == 6 {
            if section == 4 {
                return prefFlag ? fitnessArr.count/2 : 0
            }
            if section == 5 {
                return prefFlag ? nutriArr.count/2 : 0
            }
            if section == 6 {
                return prefFlag ? mentalArr.count/2 : 0
            }
        }
        
        if section == 1 || section == 2 || section == 3  {
            return prefFlag ? 0 : 1
        }
        if  section == 7 {
            return prefFlag ? 1 : 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = HealthWellnessCell()
        
        if indexPath.section == 0 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "prefCell") as! HealthWellnessCell
            
            return cell
        }
        if indexPath.section == 1 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "articleCell") as! HealthWellnessCell
            
            cell.delegate = self
            cell.newsData = newsData
            cell.collView.accessibilityIdentifier = "mental"
            cell.collView.reloadData()
            
            return cell
        }
        if indexPath.section == 2 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "articleCell") as! HealthWellnessCell
            
            cell.delegate = self
            cell.newsData = fitnessArticleData
            cell.collView.accessibilityIdentifier = "fitness"
            cell.collView.reloadData()
            
            return cell
        }
        if indexPath.section == 3 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "articleCell") as! HealthWellnessCell
            
            cell.delegate = self
            cell.newsData = healthArticleData
            cell.collView.accessibilityIdentifier = "nutri"
            cell.collView.reloadData()
            
            return cell
        }
        if indexPath.section == 4 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "prefChkCell") as! HealthWellnessCell
            
            let row = indexPath.row + indexPath.row
            let data = fitnessArr[row]
            cell.lbl1.text = data
            cell.chk1.tag = row
            cell.chk1.accessibilityIdentifier = "fitness"
            cell.chk1.addTarget(self, action: #selector(checkAction(_:)), for: .touchUpInside)
            
            if row < fitnessArr.count-1 {
                let data1 = fitnessArr[row + 1]
                cell.lbl2.text = data1
                cell.chk2.tag = row + 1
                cell.chk2.accessibilityIdentifier = "fitness"
                cell.chk2.addTarget(self, action: #selector(checkAction(_:)), for: .touchUpInside)
            }
        }
        if indexPath.section == 5 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "prefChkCell") as! HealthWellnessCell
            
            let row = indexPath.row + indexPath.row
            let data = nutriArr[row]
            cell.lbl1.text = data
            cell.chk1.tag = row
            cell.chk1.accessibilityIdentifier = "nutri"
            cell.chk1.addTarget(self, action: #selector(checkAction(_:)), for: .touchUpInside)
            
            if row < nutriArr.count-1 {
                let data1 = nutriArr[row + 1]
                cell.lbl2.text = data1
                cell.chk2.tag = row + 1
                cell.chk2.accessibilityIdentifier = "nutri"
                cell.chk2.addTarget(self, action: #selector(checkAction(_:)), for: .touchUpInside)
            }
        }
        if indexPath.section == 6 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "prefChkCell") as! HealthWellnessCell
            
            let row = indexPath.row + indexPath.row
            let data = mentalArr[row]
            cell.lbl1.text = data
            cell.chk1.tag = row
            cell.chk1.accessibilityIdentifier = "mental"
            cell.chk1.addTarget(self, action: #selector(checkAction(_:)), for: .touchUpInside)
            
            if row < mentalArr.count-1 {
                let data1 = mentalArr[row + 1]
                cell.lbl2.text = data1
                cell.chk2.tag = row + 1
                cell.chk2.accessibilityIdentifier = "mental"
                cell.chk2.addTarget(self, action: #selector(checkAction(_:)), for: .touchUpInside)
            }
        }
        if indexPath.section == 7 {
          
            cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! HealthWellnessCell
            
            cell.chk1.addTarget(self, action: #selector(saveAction(_:)), for: .touchUpInside)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            (cell as! HealthWellnessCell).bgView.layer.masksToBounds = true
            (cell as! HealthWellnessCell).bgView.layer.cornerRadius = 10
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 && indexPath.row == 0 {
            self.prefFlag = !self.prefFlag
            self.tblData.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 70
        }
        if indexPath.section == 4 || indexPath.section == 5 || indexPath.section == 6 || indexPath.section == 7 {
            return 40
        }
        return 190
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            return 0
        }
        if section == 4 || section == 5 || section == 6 {
            
            return prefFlag ? 50 : 0
        }
        
        return prefFlag ? 0 :  50
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 4 || section == 5 || section == 6 {
            
            let view = tableView.dequeueReusableCell(withIdentifier: "prefHeadCell") as! HealthWellnessCell
            if section == 4 {
                view.lbl1.text = prefTitles[0]
                view.imgVw.image = UIImage(named: "fitness")
            }
            if section == 5 {
                view.lbl1.text = prefTitles[1]
                view.imgVw.image = UIImage(named: "nutrition")
            }
            if section == 6 {
                view.lbl1.text = prefTitles[2]
                view.imgVw.image = UIImage(named: "mental")
            }
            
            return view.contentView
            
        } else {
            
            let view = tableView.dequeueReusableCell(withIdentifier: "headCell") as! HealthWellnessCell
            
            //view.backgroundColor = RGB_239
            //view.lbl1.backgroundColor = RGB_239
            
            if section == 1 {
                view.lbl1.text = "Health"
            }
            if section == 2 {
                view.lbl1.text = "Fitness"
            }
            if section == 3 {
                view.lbl1.text = "Diet"
            }
            
            return view
        }
    }
}

extension HealthWellnessVC: HealthWellnessCellDelegate {
    
    func onSelectCell(type: String) {
        
        let vc = self.getVC(with: "BlogVC", sb: IDMain) as! BlogVC
        
        vc.newsType = type
        vc.showNews = true
        
        if type == "fitness" {
            vc.newsArr = fitnessArticleData
        }
        if type == "nutri" {
            vc.newsArr = healthArticleData
        }
        if type == "mental" {
            vc.newsArr = newsData
        }
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
}

protocol HealthWellnessCellDelegate {
    
    func onSelectCell(type: String)
}
class HealthWellnessCell: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var chk1: UIButton!
    @IBOutlet weak var chk2: UIButton!
    
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    
    @IBOutlet weak var imgVw: UIImageView!
    
    var articleData: [ArticleData] = []
    var newsData: [NewsData] = []
    var delegate: HealthWellnessCellDelegate?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.accessibilityIdentifier == "fitness" || collectionView.accessibilityIdentifier == "nutri" || collectionView.accessibilityIdentifier == "mental" {
            return newsData.count >= 5 ? 5 : newsData.count
        }
        return articleData.count >= 5 ? 5 : articleData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newsCell", for: indexPath) as! HealthCollCell
        
        if collectionView.accessibilityIdentifier == "fitness" || collectionView.accessibilityIdentifier == "nutri" || collectionView.accessibilityIdentifier == "mental" {
            
            let data = newsData[indexPath.row]
            cell.populateNewsData(data: data)
            
        } else {
            
            let data = articleData[indexPath.row]
            cell.populateArticleData(data: data)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    
        (cell as! HealthCollCell).bgView.layer.masksToBounds = true
        (cell as! HealthCollCell).bgView.layer.cornerRadius = 10
        
        cell.contentView.layer.masksToBounds = true
        cell.layer.cornerRadius = 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        delegate?.onSelectCell(type: collectionView.accessibilityIdentifier!)
        
    }
    
}

class HealthCollCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    func populateArticleData(data: ArticleData) {
        
        if let strurl = data.urlToImage, let url = URL(string: strurl) {
            imgView.kf.setImage(with: url)
        }
        lblTitle.text = data.title
        lblContent.text = data.content
    }
    func populateNewsData(data: NewsData) {
        
        
        if let strurl = data.imgurl, let url = URL(string: strurl) {
            imgView.kf.setImage(with: url)
        }
        lblTitle.text = data.name
        lblContent.text = data.description
    }
}
