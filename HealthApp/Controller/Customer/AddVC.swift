//
//  AddVC.swift
//  HealthApp
//
//  Created by Apple on 29/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class AddVC: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func addAction(_ sender: UIButton) {
        
        let tag = sender.tag
        
        if tag == 0 {
            
            let vc = self.getVC(with: "AddHealthRecordSegmentVC", sb: "Main") as! AddHealthRecordSegmentVC
            self.navigationController?.pushViewController(vc, animated: false)
              
        }
        
        if tag == 1 {
            
            let vc = self.getVC(with: "ParallaxVC", sb: "Main") as! ParallaxVC
            vc.type = "chemist"
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        
        if tag == 2 {
            
            let vc = self.getVC(with: "AddDoctorVC", sb: IDMain)
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        
        if let tabbarvc = self.tabBarController as? HomeVC {
            
            tabbarvc.navigationController?.setNavigationBarHidden(false, animated: false)
        }
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        /*
        if let tabbarvc = self.tabBarController as? HomeVC {
            
            tabbarvc.navigationController?.setNavigationBarHidden(true, animated: false)
        }*/
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
