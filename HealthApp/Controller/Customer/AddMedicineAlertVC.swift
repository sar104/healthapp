//
//  AddMedicineAlertVC.swift
//  HealthApp
//
//  Created by Apple on 25/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

enum Days: String {
case Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
static var all = [Sunday, Monday, Tuesday, Wednesday, Thursday, Friday,Saturday]
}

class AddMedicineAlertVC: BaseVC, AddMedAlertCellDelegate, SwiftAlertViewDelegate {
    
    
    var arrAlertTimings: [AlertTiming] = []
    var arrTiming: [String] = ["Morning","Afternoon","Evening","Night"]
    var timerCellData = [false,false,false,false]
    var medAlertData: MedAlertData = MedAlertData()
    
    var addMedAlertView: SwiftAlertView!
    var alertWidth = 375.0
    let device = ""
    
    @IBOutlet weak var tblData: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
        switch UIDevice.modelName {
            
        case device + "iPhone 5", device + "iPhone 5c", device + "iPhone SE", device + "iPhone 5s":
            alertWidth = 320.0
            
        default:
            break
        }
        
        medAlertData.user_id = Int(loggedUser!.id)
        medAlertData.alert_type = kAlertTypeMed
        
        tblData.estimatedRowHeight = 60
        for val in arrTiming {
            var alertTiming = AlertTiming()
            alertTiming.category = val
            arrAlertTimings.append(alertTiming)
        }
    }
    
    func initAlert() {
        
        let nib = Bundle.main.loadNibNamed("AddMedAlertView", owner: self, options: nil)
        let contentView = nib?[0] as! UIView
        if UIDevice.modelName == device + "iPhone 5" ||  UIDevice.modelName ==  device + "iPhone 5c" || UIDevice.modelName ==  device + "iPhone SE" || UIDevice.modelName ==   device + "iPhone 5s" {
        contentView.frame = CGRect(x: 0, y: 0, width: 320, height: contentView.frame.size.height)
        }
        //addMedAlertView = SwiftAlertView(nibName: "AddMedAlertView", delegate: self, cancelButtonTitle: nil, otherButtonTitles: nil)
        addMedAlertView = SwiftAlertView(contentView: contentView, delegate: self, cancelButtonTitle: nil)
        addMedAlertView.dismissOnOtherButtonClicked = true
        addMedAlertView.dismissOnOutsideClicked = true
        addMedAlertView.kDefaultHeight = 375
        if UIDevice.modelName == device + "iPhone 5" ||  UIDevice.modelName ==  device + "iPhone 5c" || UIDevice.modelName ==  device + "iPhone SE" || UIDevice.modelName ==   device + "iPhone 5s" {
            
            addMedAlertView.kDefaultWidth = alertWidth
        }
        addMedAlertView.tag = 1
        
        let tblAlert = addMedAlertView.viewWithTag(10) as! UITableView
        tblAlert.dataSource = self
        tblAlert.delegate = self
        
        tblAlert.register(UINib(nibName: "TimeAlertCell", bundle: nil), forCellReuseIdentifier: "timeAlertCell")
        
        let btnAdd = addMedAlertView.viewWithTag(12) as! UIButton
        btnAdd.addTarget(self, action: #selector(timerAddAction), for: .touchUpInside)
        
        let btnClose = addMedAlertView.viewWithTag(11) as! UIButton
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
    }

    @IBAction func closeAction(_ sender: UIButton) {
     
        addMedAlertView.dismiss()
    }
    
    @objc func timerAddAction() {
        
        if validateTimer() {
            addMedAlertView.dismiss()
            self.tblData.reloadData()
        }
        
    }
    func onAlertClicked(id: Int) {
        
        if id == 1 {
            initAlert()
            addMedAlertView.show()
        }
        if id == 2 {
            if validate() {
                setNotifications()
                self.addMedicineAlert()
            }
        }
    }
    
    func onValueChanged(value: MedicineData) {
        medAlertData.medicine_name = value.medicine_name
        medAlertData.medicine_unit = value.packaging_of_medicines
        self.tblData.reloadData()
    }
    
    func validateTimer() -> Bool {
        
        let selArr = arrAlertTimings.filter{ ($0.selected == true) }
        let chkWeekArr = arrAlertTimings.filter{ ($0.arrWeeks.count > 0) }
        let chkTimingArr = arrAlertTimings.filter{ (!($0.timming ?? "").isEmpty) }
        
        if selArr.count == 0 {
            
            self.alert(strTitle: strErr, strMsg: "Please select category")
            return false
        }
        if chkWeekArr.count == 0 {
            self.alert(strTitle: strErr, strMsg: "Please select weekdays")
            return false
        }
        if chkTimingArr.count == 0 {
            self.alert(strTitle: strErr, strMsg: "Please select time")
            return false
        }
        return true
    }
    func validate() -> Bool {
        
        let selArr = arrAlertTimings.filter{ ($0.selected == true) }
        
        if medAlertData.medicine_name == nil || medAlertData.medicine_name!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter medicine name")
            return false
        }
        if selArr.count == 0 {
            self.alert(strTitle: strErr, strMsg: "Please set timer")
            return false
        }
        return true
    }
    
    func setNotifications() {
        
        var arrNotif: [LocalNotification] = []
        let manager = LocalNotificationManager()
        for alert in arrAlertTimings {
            
            for weekday in alert.arrWeeks {
                let notifId = kAlertTypeMed + "_" + medAlertData.medicine_name!  + "_" + alert.timming! + "_" + weekday
                
                let day = Days.all.firstIndex(of: Days(rawValue: weekday)!)! + 1
                print("day: \(day)")
                let strTime = alert.timming?.toDate(format: "HH:mm")
                let notif =
                    LocalNotification(id: notifId, title: medAlertData.medicine_name!,
                                      datetime: DateComponents(calendar: Calendar.current,
                                                               year: Date().get(.year),month: Date().get(.month),
                                                               hour: strTime?.get(.hour), minute: strTime?.get(.minute), weekday: day))
                        
                arrNotif.append(notif)
                
                let notifId1 = kAlertTypeMed + "_" + medAlertData.medicine_name!  + "_" + alert.timming! + "_" + weekday + "_before_5"
                let notif1 =
                    LocalNotification(id: notifId1, title: medAlertTitle,
                                      datetime: DateComponents(calendar: Calendar.current,
                                                               year: Date().get(.year),month: Date().get(.month),
                                                               hour: strTime?.get(.hour), minute: strTime!.get(.minute)-5, weekday: day))
                        
                arrNotif.append(notif1)
                
                let notifId2 = kAlertTypeMed + "_" + medAlertData.medicine_name!  + "_" + alert.timming! + "_" + weekday + "_after_5"
                let notif2 =
                    LocalNotification(id: notifId2, title: medAlertData.medicine_name!,
                                      datetime: DateComponents(calendar: Calendar.current,
                                                               year: Date().get(.year),month: Date().get(.month),
                                                               hour: strTime?.get(.hour), minute: strTime!.get(.minute)+5, weekday: day))
                        
                arrNotif.append(notif2)
            }
            
        }
        print("notification\(arrNotif)")
        manager.notifications = arrNotif
        manager.schedule()
    }
    
    func addMedicineAlert() {
        
        //{"time_category":"{"category":"Morning","weeks":"Sunday, Thursday, Friday"}","timming":"09:00","time_in_miles":"1598326254984"}
        do {
            var alertTimingArr: [AlertTiming] = []//NSMutableArray()
            let selArr = arrAlertTimings.filter{ ($0.selected == true) }
            for data in selArr {
                
                let strweeks = data.arrWeeks.joined(separator: ",")
                let millisec = data.timming?.toDate(format: "HH:mm").convertToMilli()
                
                var alertTiming = AlertTiming()
                alertTiming.timming = data.timming!
                alertTiming.time_in_miles = String(millisec!)
                
                var timeCategory = TimeCategory()
                timeCategory.category = data.category
                timeCategory.weeks = strweeks
                
                let timeData = try JSONEncoder().encode(timeCategory)
                let jsonStrTime = String(data: timeData, encoding: .utf8)
                
                alertTiming.time_category = jsonStrTime
                
                alertTimingArr.append(alertTiming)
            }
            
            let alertTimingData = try JSONEncoder().encode(alertTimingArr)
            //try JSONSerialization.data(withJSONObject: alertTimingArr, options: [])
            let jsonStrAlertTiming = String(data: alertTimingData, encoding: .utf8)
            medAlertData.alert_timmings = alertTimingArr
            /*
            let paramsData = try JSONEncoder().encode(medAlertData)
            
            guard var params = try? JSONSerialization.jsonObject(with: paramsData, options: []) as? [String:Any]
                else { return }
 */
            
            var params: [String: Any] = [:]
            
            params["user_id"] = medAlertData.user_id
            params["medicine_name"] = medAlertData.medicine_name
            params["medicine_unit"] = medAlertData.medicine_unit
            params["alert_timmings"] = jsonStrAlertTiming
            params["alert_type"] = medAlertData.alert_type
            
            print("params: \(params)")
            
            IHProgressHUD.show()
            apiManager.addMedicineAlert(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.navigationController?.popViewController(animated: false)
                        }
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
            
        } catch let err {
            print(err)
        }
    }
    
    @IBAction func timeClickAction(_ sender: UIButton){
        
        sender.isSelected = !sender.isSelected
        let tag = sender.tag
        var data = arrAlertTimings[tag]
        data.selected = sender.isSelected
        //timerCellData[tag] = !timerCellData[tag]
        arrAlertTimings[tag] = data
        
        let tblAlert = addMedAlertView.viewWithTag(10) as! UITableView
        tblAlert.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddMedicineAlertVC: TimeAlertCellDelegate {
    
    func onWeekClicked(row: Int, tag: Int) {
      
        let week = Days.all[tag].rawValue
        
        var data = arrAlertTimings[row]
        
        if !data.arrWeeks.contains(week) {
            data.arrWeeks.append(week)
        } else {
            data.arrWeeks.remove(at: data.arrWeeks.firstIndex(of: week)!)
        }
        arrAlertTimings[row] = data
        print(arrAlertTimings)
    }
    func onTimeSelected(category: String, time: String) {
        
        let arr = arrAlertTimings.filter{ ($0.category == category) }
        
        var data = arr[0]
        let index = arrAlertTimings.firstIndex { (timing) -> Bool in
            timing.category == data.category
        }
        data.timming = time
        arrAlertTimings[index!] = data
        
    }
}
extension AddMedicineAlertVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblData {
            return 3
        }
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblData {
            if section == 0 {
                return 1
            }
            if section == 1 {
                return arrAlertTimings.count
            }
            if section == 2 {
                return 2
            }
        }
        return 4
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            //cell.contentView.layer.borderColor = UIColor.gray.cgColor
            //cell.contentView.layer.borderWidth = 1.0
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblData {
            var cell = AddMedAlertCell()
            
            if indexPath.section == 0 {
                if indexPath.row == 0 {
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! AddMedAlertCell
                    cell.setup(id: "search")
                    cell.delegate = self
                    
                    cell.txtName.text = medAlertData.medicine_name
                    cell.lblType.text = medAlertData.medicine_unit
                    return cell
                }
            }
            if indexPath.section == 1 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "timeCell") as! AddMedAlertCell
                
                let data = arrAlertTimings[indexPath.row]
                cell.lblAlertTiming.text = (data.category ?? "") + "\n" + data.arrWeeks.joined(separator: ", ")
                cell.lblType.text = data.timming
                
                cell.setup(id: "time")
                
                
                return cell
                
            }
            if indexPath.section == 2 {
                if indexPath.row == 0 {
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "btnTimerCell") as! AddMedAlertCell
                    cell.delegate = self
                    return cell
                }
                if indexPath.row == 1 {
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "btnAlertCell") as! AddMedAlertCell
                    cell.delegate = self
                    return cell
                }
            }
            
            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "timeAlertCell") as! TimeAlertCell
            cell.row = indexPath.row
            cell.delegate = self
            let data = arrAlertTimings[indexPath.row]
            cell.setup(id: data.category!)
            
            cell.weekView.isHidden = !data.selected//timerCellData[indexPath.row]
            cell.txtTime.isEnabled = data.selected
            cell.btnChk.isSelected = data.selected
            cell.btnChk.tag = indexPath.row
            
            for i in 0..<Days.all.count {
                
                let btn =  cell.contentView.viewWithTag(i+4) as? UIButton
                btn?.isSelected = false
                
                if data.arrWeeks.contains(Days.all[i].rawValue) {
                    btn?.isSelected = true
                }
            }
            
            if let lblTitle = cell.contentView.viewWithTag(8) as? UILabel {
                lblTitle.text = arrTiming[indexPath.row]
            }
            cell.btnChk.addTarget(self, action: #selector(timeClickAction(_:)), for: .touchUpInside)
            
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblData {
            
            if indexPath.section == 1 {
                let data = arrAlertTimings[indexPath.row]
                if !data.selected {
                    return 0
                }
                return UITableView.automaticDimension
            }
            return 90//UITableView.automaticDimension
        } else {
            let data = arrAlertTimings[indexPath.row]
            if !data.selected {
                
                return 55
            }
            return 145
        }
    }
    
}

protocol AddMedAlertCellDelegate {
    
    func onAlertClicked(id: Int)
    func onValueChanged(value: MedicineData)
}
class AddMedAlertCell: BaseCell {
    
    @IBOutlet weak var txtName: SearchTextField!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblAlertTiming: UILabel!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    
    let apiManager: WebServiceManager = WebServiceManager(service: WebService())
    var delegate: AddMedAlertCellDelegate?
    var allMedicineArr: [MedicineData] = []
    var medArr: [MedicineData] = []
    
    func setup(id: String) {
        
        if id == "search" {
            
            let medArr = CoreDataManager.shared.fetchAllMedicines()
            
            for data in medArr {
                
                allMedicineArr.append(MedicineData(coreDataMed: data))
                
            }
            setupSearchField()
        }
        if id == "time" {
            
            view1.layer.borderWidth = 1.0
            view1.layer.borderColor = UIColor.gray.cgColor
            
            view2.layer.borderWidth = 1.0
            view2.layer.borderColor = UIColor.gray.cgColor
        }
    }
    func setupSearchField(){
        txtName.userStoppedTypingHandler = {
            if let criteria = self.txtName.text {
                if criteria.count > 2 {

                    // Show the loading indicator
                    self.txtName.showLoadingIndicator()
                    self.medArr = self.allMedicineArr.filter({ (data) -> Bool in
                        return data.medicine_name!.range(of: criteria) != nil
                    })
                    let medNames:[String] = self.medArr.map( {($0.medicine_name ?? "")} )
                    self.txtName.filterStrings(medNames)
                    self.txtName.stopLoadingIndicator()

                    /*
                    let params = ["medicine":criteria]
                    self.apiManager.getMedicines(params: params) { (result) in
                        
                        switch result {
                            case .success(let medData):
                                
                                self.medArr = medData
                                // Set new items to filter
                                let medNames:[String] = medData.map( {($0.medicine_name ?? "")} )
                                self.txtName.filterStrings(medNames)

                                // Hide loading indicator
                                self.txtName.stopLoadingIndicator()
                                
                            case .failure(let err):
                            print("err: \(err)")
                        }
                    }
                    */
                }
            }
        }
        txtName.itemSelectionHandler = {filteredResults, itemPosition in
            
            let item = filteredResults[itemPosition]
            self.txtName.text = item.title
            
            let data = self.medArr.filter{ ($0.medicine_name == item.title) }
            self.delegate?.onValueChanged(value: data[0])
        }
    }
    
    func populateData(data: MedAlertData) {
        
        
    }
    
    @IBAction func alertAction(_ sender: UIButton){
        
        delegate?.onAlertClicked(id: sender.tag)
    }
    
}

protocol TimeAlertCellDelegate {
    
    func onWeekClicked(row: Int, tag: Int)
    func onTimeSelected(category: String, time: String)
}

class TimeAlertCell: BaseCell {
    
    @IBOutlet weak var btnChk: UIButton!
    @IBOutlet weak var weekView: UIView!
    @IBOutlet weak var txtTime: UITextField!
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
    var row: Int = 0
    var delegate: TimeAlertCellDelegate?
    var seldate: String?
    
    func setup(id: String){
        
        datePicker.tag = row
        txtTime.inputView = datePicker
        txtTime.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
    }
    
    @IBAction func weekAction(_ sender: UIButton){
        
        sender.isSelected = !sender.isSelected
        delegate?.onWeekClicked(row: row, tag: sender.tag-4)
    }
    
    @objc func dateSelected(_ picker: UIDatePicker){
        
        let dateStr = picker.date.toString(format: "HH:mm")//df.string(from: picker.date)
        
        txtTime.text = dateStr
        
        let date = picker.date.toString(format: "HH:mm")
        seldate = date
    }
    
    @objc override func doneCellPicker (sender: UIBarButtonItem) {
        
        let id = sender.accessibilityIdentifier
        
        let dateStr = (seldate) ?? Date().toString(format: "HH:mm")//df.string(from: picker.date)
        
        txtTime.text = dateStr
        
        delegate?.onTimeSelected(category: id!, time: dateStr)
        txtTime.resignFirstResponder()
    }
}
