//
//  ShareEHRVC.swift
//  HealthApp
//
//  Created by Apple on 14/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import RadioGroup
import IHProgressHUD
import SwiftyJSON

class ShareEHRVC: BaseVC, ShareEHRCellDelegate {

    var familyMembersArr: [FamilyMemberData] = []
    var docArr:[DoctorData] = []
    var shareTypeArr = ["Family", "Doctor"]
    var shareType: String? = "Doctor"
    var EHR_Id: Int?
    var shareWith: Int?
    var remark: String?
    var type: String?
    
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var tblData: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let doc_id = UserDefaults.standard.value(forKey: "doctor_id") {
            shareType = "Doctor"
            shareWith = doc_id as? Int
            
            
        }
        if type == "Report" {
            lblHead.text = "SHARE REPORT"
        } else {
            lblHead.text = "SHARE PRESCRIPTION"
        }
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        //self.view.addGestureRecognizer(tapgesture)
        
        // Do any additional setup after loading the view.
        self.getDoctors()
        self.getFamilyMembers()
    }
    
    func getDoctors() {
        
        let params: [String:Any] = ["user_id": loggedUser!.id]
        
        IHProgressHUD.show()
        apiManager.getMyDoctors(params: params) { (result) in
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    self.docArr.removeAll()
                    self.docArr = data
                    
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }

    func getFamilyMembers() {
        
        IHProgressHUD.show()
        
        let params = ["user_id": loggedUser?.id] as [String: Any]
        apiManager.getFamilyMember(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    
                    self.familyMembersArr = memberData.filter{ (
                        $0.dob != nil &&
                        $0.dob!.toDate(format:df_dd_MM_yyyy).getDiff() <= 3) }
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func sharePresc() {
        
        IHProgressHUD.show()
        
        let param_remark = ["remark": remark ?? "-", "comment": ""]//"[\"remark\":\"test\", \"comment\": \"\"]"
        
        let jsonRemark = JSON(param_remark)
        let strRemark = jsonRemark.rawString()!
        
        let params = ["user_id": loggedUser!.id,
        "shared_with_id": [shareWith!],
        "prescription_id": EHR_Id!,
        "remark": param_remark] as [String: Any]
        
        print("params: \(params)")
        
        apiManager.sharePresc(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let data):
                    
                        DispatchQueue.main.async {
                            if data.status {
                                self.alert(strTitle: strSuccess, strMsg: "Prescription shared successfully.")
                            } else {
                                self.alert(strTitle: strSuccess, strMsg: data.message!)
                            }
                        }
                    
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func shareReport() {
        
        IHProgressHUD.show()
        
        let param_remark = ["remark": remark ?? "-", "comment": ""]
        
        let params = ["user_id": loggedUser!.id,
        "shared_with_id": shareWith!,
        "report_id": EHR_Id!,
        "remark": param_remark] as [String: Any]
        
        print("params: \(params)")
        
        apiManager.shareReport(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let data):
                    
                    if data.status {
                        self.alert(strTitle: strSuccess, strMsg: "Report shared successfully.")
                    } else {
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func onValueChanged(type: String, value: String) {
        
        if type == "radio" {
            
            shareType = shareTypeArr[Int(value)!]
            
            let indexpath1 = IndexPath(row: 0, section: 0)
            let indexpath2 = IndexPath(row: 1, section: 0)
            
            self.tblData.reloadRows(at: [indexpath1,indexpath2], with: .none)
        }
        if type == "Doctor" {
            shareWith = Int(value )
        }
        if type == "Family" {
            shareWith = Int(value)
        }
        if type == "remark" {
            remark = value
        }
        
        //self.tblData.reloadData()
    }
    
    func validate() -> Bool {
    
        if shareWith == nil {
            self.alert(strTitle: strErr, strMsg: "Please select Doctor/Family Member to share with")
            return false
        }
        return true
    }
    
    func onSaveClicked() {
        
        if validate() {
            if type == "Presciption" {
                self.sharePresc()
            }
            if type == "Report" {
                self.shareReport()
            }
        }
    }
    
    @IBAction func addAction(_ sender: UIButton) {
        
        //let tag = sender.tag
        
        if shareType == "Doctor" {
            
            let vc = self.getVC(with: "AddDoctorVC", sb: IDMain)
            self.navigationController?.pushViewController(vc, animated: false)
            
        } else if shareType == "Family" {
            let vc = self.getVC(with: "AddFamilyMemberVC", sb: IDMain)
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ShareEHRVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = ShareEHRCell()
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RadioCell") as! ShareEHRCell
            
            cell.delegate = self
            cell.radioGroup.titles = self.shareTypeArr
            cell.setup(id: "radio")
            if shareType == "Doctor" {
                cell.radioGroup.selectedIndex = 1
            } else {
                cell.radioGroup.selectedIndex = 0
            }
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! ShareEHRCell
            
            cell.delegate = self
            
            if shareType == "Doctor" {
                
                cell.pickerDataArr = self.docArr
                
                let data = self.docArr.filter{ $0.id == shareWith }
                
                if data.count > 0 {
                    cell.txtFld.text = data[0].user_name
                }
                
                if let lblHead = cell.contentView.viewWithTag(4) as? UILabel {
                 
                    lblHead.text = "DOCTOR"
                }
                if let btnAdd = cell.contentView.viewWithTag(5) as? UIButton {
                    btnAdd.setTitle("Add Doctor", for: .normal)
                    btnAdd.addTarget(self, action: #selector(addAction), for: .touchUpInside)
                    //
                }
            } else {
                cell.pickerDataArr = self.familyMembersArr
                
                let data = self.familyMembersArr.filter{ $0.id == shareWith }
                
                if data.count > 0 {
                    cell.txtFld.text = data[0].name
                }
                if let lblHead = cell.contentView.viewWithTag(4) as? UILabel {
                 
                    lblHead.text = "Family Member"
                }
                if let btnAdd = cell.contentView.viewWithTag(5) as? UIButton {
                    btnAdd.setTitle("Add Member", for: .normal)
                    btnAdd.addTarget(self, action: #selector(addAction), for: .touchUpInside)
                }
            }
            
            cell.setup(id: shareType!)
            
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! ShareEHRCell
            
            cell.delegate = self
            
            cell.setup(id: "remark")
            
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "btnAlertCell") as! ShareEHRCell
            cell.delegate = self
            
            if type == "Report" {
                cell.btnShare.setTitle("SHARE REPORT", for: .normal)
            } else {
                cell.btnShare.setTitle("SHARE PRESCRIPTION", for: .normal)
            }
            return cell
            
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return 120
        }
        if indexPath.row == 1 {
            return 150
        }
        return 55
    }
    
}

protocol ShareEHRCellDelegate {
    
    func onValueChanged(type: String, value: String)
    func onSaveClicked()
}

class ShareEHRCell: BaseCell, UITextFieldDelegate {
    
    @IBOutlet weak var txtFld: RightViewTextField!
    @IBOutlet weak var lblDob: UILabel!
    @IBOutlet weak var radioGroup: RadioGroup!
    @IBOutlet weak var txtName: BottomLineTextField!
    @IBOutlet weak var btnShare: UIButton!
    
    var delegate: ShareEHRCellDelegate?
    var selRow: Int = 0
    
    func setup(id: String) {
        
        if id == "radio" {
            
            radioGroup.addTarget(self, action: #selector(optionSelected(radioGroup:)), for: .valueChanged)
        }
        
        if id == "Doctor" || id == "Family" {
            
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            if id == "Doctor" {
                txtFld.placeholder = "Select doctor"
            } else {
                txtFld.placeholder = "Select family member"
            }
            txtFld.inputView = self.cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
        
        if id == "remark" {
            
            txtName.accessibilityIdentifier = id
            txtName.delegate = self
            txtName.placeholder = "Remark"
            txtName.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        selRow = row
        
        if pickerView.accessibilityIdentifier == "Doctor" {
         
            let data = pickerDataArr[row] as? DoctorData
            txtFld.text = data?.user_name
        }
        if pickerView.accessibilityIdentifier == "Family" {
     
            let data = pickerDataArr[row] as? FamilyMemberData
            txtFld.text = data?.name
        }
    }
    
    @objc override func doneCellPicker (sender: UIBarButtonItem) {
        
        let id = sender.accessibilityIdentifier
        
        if id == "Doctor" {
            let data = pickerDataArr[selRow] as? DoctorData
            txtFld.text = data?.user_name
            //delegate?.onDataSelected(id: id!, value: data!)
            delegate?.onValueChanged(type: id!, value: String(data!.id!))
            txtFld.resignFirstResponder()
        }
        if id == "Family" {
            let data = pickerDataArr[selRow] as? FamilyMemberData
            txtFld.text = data?.name
            //delegate?.onDataSelected(id: id!, value: data!)
            delegate?.onValueChanged(type: id!, value: String(data!.id!))
            txtFld.resignFirstResponder()
        }
        self.endEditing(true)
    }
    
    @objc func optionSelected(radioGroup: RadioGroup){
        
        delegate?.onValueChanged(type: "radio", value: String(radioGroup.selectedIndex))
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        
        delegate?.onSaveClicked()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
    
        self.perform(
            #selector(valueChanged),
            with: textField,
            afterDelay: 0.8)
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func valueChanged(textField: UITextField) {
        
        delegate?.onValueChanged(type: textField.accessibilityIdentifier!, value: textField.text!)
    }
    
}
