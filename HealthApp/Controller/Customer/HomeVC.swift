//
//  HomeVC.swift
//  HealthApp
//
//  Created by Apple on 04/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class HomeVC: UITabBarController {

    @IBOutlet weak var menuBtn: UIBarButtonItem!
    //@IBOutlet weak var rightBtn: UIBarButtonItem!
  
    var menuSlide : VKSideMenu =  VKSideMenu.init()
    var menuArray : [VKSideMenuItem] = []
    var menuSectionArr: [[VKSideMenuItem]] = []
    var sectionTitleArr: [String] = []
    var loggedUser: User?
    lazy var coreDataManager: CoreDataManager = {
        let manager = CoreDataManager.shared
        return manager
    }()
    lazy var notifButton: UIBarButtonItem = {
        
        let btn1 = BadgeButton()//UIButton(type: .custom)
        btn1.setImage(UIImage(named: "bell"), for: .normal)
        btn1.addTarget(self, action: #selector(self.showNotifications(sender:)), for: .touchUpInside)
        btn1.badgeEdgeInsets = UIEdgeInsets(top: 5, left: 20, bottom: 0, right: 0)
        
        let widthConstraint = btn1.widthAnchor.constraint(equalToConstant: 23)
        let heightConstraint = btn1.heightAnchor.constraint(equalToConstant: 20)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        
        let barButton =  UIBarButtonItem(customView: btn1)
        
        return barButton
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(setProfilePhoto), name: NSNotification.Name(rawValue: ProfileUpdateNotification), object: nil)
        
        self.setProfilePhoto()
        self.setBellIcon()
        self.initSideMenu()
        
    }
    
    func setBellIcon() {
        
        //notifButton.action = #selector(self.showNotifications(sender:))
        if let btn = notifButton.customView as? BadgeButton {
            
            btn.badge = String(UIApplication.shared.applicationIconBadgeNumber) //"2"
        }
        self.navigationItem.rightBarButtonItem = notifButton
    }
    
    @objc func showNotifications(sender: UIBarButtonItem) {
        
        let navVC = self.selectedViewController as! UINavigationController
        //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
        let sb = UIStoryboard(name: IDMain, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "NotificationsVC")
        navVC.pushViewController(vc, animated: false)
    }
    
    @objc func setProfilePhoto() {
        
        let users = self.coreDataManager.fetchLocalUsers()
        loggedUser = users[0]
        
        
        let logoImg = UIImage(named:"Logo")
        let imgView = UIImageView(image: logoImg)
        imgView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        imgView.center = (self.navigationController?.navigationBar.center)!
        
        imgView.contentMode = .scaleAspectFit
        navigationItem.titleView = imgView
        
        
        let image = UIImage(named: "Menu")
        let renderImg = image?.withRenderingMode(.alwaysOriginal)
        menuBtn.image = renderImg
        
        /*
        if let imgurl = loggedUser?.profile_image {
            let url = imgBaseURL + custProfileImg + imgurl
            let imgview = UIImageView()
            imgview.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            imgview.layer.masksToBounds = true
            imgview.layer.cornerRadius = 20
            imgview.kf.setImage(with: URL(string: url))
            
            let widthConstraint = imgview.widthAnchor.constraint(equalToConstant: 40)
            let heightConstraint = imgview.heightAnchor.constraint(equalToConstant: 40)
            heightConstraint.isActive = true
            widthConstraint.isActive = true
            
            let barButton = UIBarButtonItem(customView: imgview)
            //assign button to navigationbar
            self.navigationItem.rightBarButtonItem = barButton
        }
         */
    }
    
    @IBAction func menuAction(_ sender: Any) {
        menuSlide.show()
    }
    
    func initSideMenu(){
        
        if menuSlide != nil
        {
            //self.menuSlide =  VKSideMenu.init(direction: VKSideMenu.VKSideMenuDirection.fromLeft)
            menuSlide = VKSideMenu.init(size: 300, andDirection: VKSideMenu.VKSideMenuDirection.fromLeft)
            menuSlide.dataSource = self
            menuSlide.delegate = self
            menuSlide.textColor = UIColor.white
            menuSlide.backgroundColor = UIColor.red
            menuSlide.isEnableOverlay = false
            menuSlide.isHideOnSelection = true
            menuSlide.animationDuration = Int(0.8)
            menuSlide.selectionColor = UIColor(white: 0.0, alpha: 0.3)
            menuSlide.iconsColor = nil
            menuSlide.backgroundColor = UIColor.black
        }
        
        //if loggedUser?.role == 2 {
            var options = [AnyHashable]()
            options.append(MENU_REPORTS)
            options.append(MENU_ORDERS)
            
            
            var options1 = [AnyHashable]()
            options1.append(MENU_ADD_FAMILY_MEMBER)
            options1.append(MENU_VIEW_FAMILY_MEMBER)
            
            var options2 = [AnyHashable]()
            options2.append(MENU_DOCTOR)
            options2.append(MENU_MEDICAL)
            options2.append(MENU_DIAGNOSTICS)
            options2.append(MENU_SERVICE_PROVIDER)
            
            var options3 = [AnyHashable]()
            options3.append(MENU_HELP)
            options3.append(MENU_CONTACT)
            options3.append(MENU_LOGOUT)
            
            var sectionArr: [[AnyHashable]] = []
            sectionArr.append(options)
            sectionArr.append(options1)
            sectionArr.append(options2)
            sectionArr.append(options3)
            
            menuSectionArr.append([VKSideMenuItem()])
            for section in sectionArr {
                menuArray = []
                for option in section {
                    let item = VKSideMenuItem()
                    item.title = option as! String
                    if let icon = getMenuIcon(name: option as! String) {
                        item.icon = UIImage(named: icon)
                    }
                    menuArray.append(item)
                }
                menuSectionArr.append(menuArray)
            }
            sectionTitleArr.append("")
            sectionTitleArr.append("")
            sectionTitleArr.append("FAMILY")
            sectionTitleArr.append("FIND NEAR ME")
            sectionTitleArr.append("")
        //}
        
    }
    
    func getMenuIcon(name: String)-> String? {
        
        switch name {
        case MENU_REPORTS:
            return ICON_REPORTS
            
        case MENU_ORDERS:
            return ICON_ORDERS
            
        case MENU_ADD_FAMILY_MEMBER:
            return ICON_ADD_FAMILY_MEMBER
            
        case MENU_VIEW_FAMILY_MEMBER:
            return ICON_VIEW_FAMILY_MEMBER
            
        case MENU_DOCTOR:
            return ICON_DOCTOR
            
        case MENU_MEDICAL:
            return ICON_MEDICAL
            
        case MENU_DIAGNOSTICS:
            return ICON_DIAGNOSTICS
            
        case MENU_SERVICE_PROVIDER:
            return ICON_DOCTOR
        case MENU_HELP:
            return ICON_HELP
            
        case MENU_CONTACT:
            return ICON_CONTACT
            
        case MENU_LOGOUT:
            return ICON_LOGOUT
            
        default:
            break
        }
        
        return ""
    }
    /*
    func setupMenu(){
        
        self.menu = VKSideMenu(size: 220, andDirection: .fromLeft)
        self.menu?.dataSource = self
        self.menu?.delegate = self
        // Do any additional setup after loading the view.
        let tab1 = UITabBarItem(title: "", image: UIImage(named: "home"), selectedImage: UIImage(named: "home"))
        let tab2 = UITabBarItem(title: "", image: UIImage(named: "add"), selectedImage: UIImage(named: "add"))
        let tab3 = UITabBarItem(title: "", image: UIImage(named: "search"), selectedImage: UIImage(named: "search"))
    }
    */
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension HomeVC: VKSideMenuDelegate, VKSideMenuDataSource {
    
    //:MARKk -Side menu delegate
       
       func numberOfSections(in sideMenu: VKSideMenu) -> Int {
        return menuSectionArr.count
       }
       
       func sideMenu(_ sideMenu: VKSideMenu, numberOfRowsInSection section: Int) -> Int {
           let arr = menuSectionArr[section]
           return section == 0 ? 1 : arr.count
       }
       
        func sideMenu(_ sideMenu: VKSideMenu, titleForHeaderInSection section: Int) -> String {
        
            return sectionTitleArr[section]
        }
        
       func sideMenu(_ sideMenu: VKSideMenu, itemForRowAt indexPath: IndexPath) -> VKSideMenuItem {
           // This solution is provided for DEMO propose only
           // It's beter to store all items in separate arrays like you do it in your UITableView's. Right?
           
           var item = VKSideMenuItem()
           if indexPath.section == 0 {
            let title = loggedUser?.name
               let code = ""
            let mobile = loggedUser?.mobile_no
                
            item.title = title!
            item.desc = mobile!
            if let imgurl = loggedUser?.profile_image {
                let url = imgBaseURL + custProfileImg + imgurl
                item.imgurl = url
            }
            
                
           }
           else {
            let menuArray = menuSectionArr[indexPath.section]
               item = menuArray[indexPath.row]
           }
           return item
       }
       
       func sideMenu(_ sideMenu: VKSideMenu, heightForRowAt indexPath: IndexPath) -> CGFloat {
           if indexPath.section == 0 {
               return 100.0
           }
           else {
               return 40.0
           }
       }
       
       // MARK: - VKSideMenuDelegate
       @objc func sideMenu(_ sideMenu: VKSideMenu, didSelectRowAt indexPath: IndexPath) {
        print("Mene selected")
        var item = VKSideMenuItem()
        if indexPath.section == 1 {
            
            let menuArray = menuSectionArr[indexPath.section]
            item = menuArray[indexPath.row]
            
            if item.title == MENU_REPORTS {
                let navVC = self.selectedViewController as! UINavigationController
                //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
                let sb = UIStoryboard(name: IDMain, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "MyAlertsVC") as! MyAlertsVC
                vc.type = "Reports"
                navVC.pushViewController(vc, animated: false)
                
            }
            if item.title == MENU_ORDERS {
                let navVC = self.selectedViewController as! UINavigationController
                //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
                let sb = UIStoryboard(name: IDMain, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "OrdersVC") as! OrdersVC
                
                navVC.pushViewController(vc, animated: false)
                
            }
        }
        if indexPath.section == 2 {
            let menuArray = menuSectionArr[indexPath.section]
            item = menuArray[indexPath.row]
            
            if item.title == MENU_ADD_FAMILY_MEMBER {
                //let baseVC = self.selectedViewController as! BaseVC
                //baseVC.pushToVC(with: "AddFamilyMemberVC", sb: IDMain)
                let navVC = self.selectedViewController as! UINavigationController
                //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
                let sb = UIStoryboard(name: IDMain, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "AddFamilyMemberVC")
                navVC.pushViewController(vc, animated: false)
            }
            if item.title == MENU_VIEW_FAMILY_MEMBER {
                //let baseVC = self.selectedViewController as! BaseVC
                //baseVC.pushToVC(with: "AddFamilyMemberVC", sb: IDMain)
                let navVC = self.selectedViewController as! UINavigationController
                //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
                let sb = UIStoryboard(name: IDMain, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "FamilyMemberListVC")
                navVC.pushViewController(vc, animated: false)
            }
            //
        }
        if indexPath.section == 3 {
            let menuArray = menuSectionArr[indexPath.section]
            item = menuArray[indexPath.row]
            if item.title == MENU_DOCTOR {
                //MyAlertsVC
                let navVC = self.selectedViewController as! UINavigationController
                //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
                let sb = UIStoryboard(name: IDMain, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "ParallaxVC") as! ParallaxVC
                vc.type = "doctor"
               
                navVC.pushViewController(vc, animated: false)
            }
            if item.title == MENU_MEDICAL {
                let navVC = self.selectedViewController as! UINavigationController
                //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
                let sb = UIStoryboard(name: IDMain, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "ParallaxVC") as! ParallaxVC
                vc.type = "chemist"
                navVC.pushViewController(vc, animated: false)
            }
            if item.title == MENU_DIAGNOSTICS {
                //MyAlertsVC
                let navVC = self.selectedViewController as! UINavigationController
                //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
                let sb = UIStoryboard(name: IDMain, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "ParallaxVC") as! ParallaxVC
                vc.type = "diagnostic"
                
                navVC.pushViewController(vc, animated: false)
            }
            if item.title == MENU_SERVICE_PROVIDER {
                //MyAlertsVC
                let navVC = self.selectedViewController as! UINavigationController
                //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
                let sb = UIStoryboard(name: IDMain, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "MyAlertsVC") as! MyAlertsVC
                vc.type = "Service"
                navVC.pushViewController(vc, animated: false)
            }
        }
        if indexPath.section == 4 {
            let menuArray = menuSectionArr[indexPath.section]
            item = menuArray[indexPath.row]
            if item.title == MENU_HELP {
                //MyAlertsVC
                let navVC = self.selectedViewController as! UINavigationController
                //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
                let sb = UIStoryboard(name: IDMain, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "FAQVC") as! FAQVC
                navVC.pushViewController(vc, animated: false)
            }
            if item.title == MENU_CONTACT {
                //MyAlertsVC
                let navVC = self.selectedViewController as! UINavigationController
                //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
                let sb = UIStoryboard(name: IDMain, bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
                navVC.pushViewController(vc, animated: false)
            }
            if item.title == MENU_LOGOUT {
                
                self.alertAction(strTitle: "Alert", strMsg: "Do you want to logout?") { (action) in
                    
                    if let title = action.title, title == "Yes" {
                        self.clearUserData()
                        NotificationCenter.default.post(.init(name: Notification.Name(rawValue: LoginNotification)))
                    }
                }
            }
        }
        
       }
    
    func clearUserData() {
        
        UserDefaults.standard.set("0", forKey: "role")
        UserDefaults.standard.set("", forKey: "deviceId")
        UserDefaults.standard.set(0, forKey: "newuser")
        UserDefaults.standard.set("", forKey: "profile_completed")
        UserDefaults.standard.set(0, forKey: "HealthRecordSegment")
        UserDefaults.standard.set(0, forKey: "OrderSegment")
        UserDefaults.standard.set(0, forKey: "doctor_id")
        //
        
        
        
        let users = self.coreDataManager.fetchLocalUsers()
        let user = users[0]
        let id = user.id
        self.coreDataManager.removeUser(id: Int(id))
    }
       
       func editOnHeaderSideMenu(){

            let navVC = self.selectedViewController as! UINavigationController
            //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
            let sb = UIStoryboard(name: IDMain, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "UpdateProfileVC")
            navVC.pushViewController(vc, animated: false)
       }
    
    func viewOnHeaderSideMenu() {
        let navVC = self.selectedViewController as! UINavigationController
        //baseVC.pushToVC(with: "UpdateProfileVC", sb: IDMain)
        let sb = UIStoryboard(name: IDMain, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "ProfileVC")
        navVC.pushViewController(vc, animated: false)
        
    }

       @objc func sideMenuDidShow(_ sideMenu: VKSideMenu)
       {
           
       }
       @objc func sideMenuDidHide(_ sideMenu: VKSideMenu)
       {
       }
    
       func populateVKSlideMenuData(_ mArray: [String]) -> [VKSideMenuItem] {
           var temp = [VKSideMenuItem]()
           for menu: String in mArray {
               let item = VKSideMenuItem()

           }
           return temp
       }
}
