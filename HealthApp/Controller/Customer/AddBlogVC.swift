//
//  AddBlogVC.swift
//  HealthApp
//
//  Created by Apple on 24/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import RadioGroup
import IHProgressHUD
import CloudTagView
import RichEditorView
import SwiftyJSON
import MobileCoreServices

class AddBlogVC: BaseVC, SwiftAlertViewDelegate, UITextFieldDelegate, TagViewDelegate {

    @IBOutlet weak var tblData: UITableView!
    var addMedAlertView: SwiftAlertView!
    
    var blogType: String = ""
    var ageGroupArr = [5,12,18,21,25,40,50,60]
    var genderArr = ["Male","Female","Neutral"]
    var typeArr = ["Article","Media"]
    var interestArr:[InterestData] = []
    var blogTitle: String = ""
    var articleContent: String = ""
    var selInterestArr: [InterestData] = []
    var selGenderArr: [String] = []
    let imgPicker = UIImagePickerController()
    var blogImg:Data?
    var strBlogImg: String?
    var age: String?
    var pdfFlag: Bool = false
    var pdfData: Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblData.estimatedRowHeight = 50
        getInterests()
    }
    
    func getInterests() {
        
        IHProgressHUD.show()
        
        apiManager.getRequestData(api: InterestAPI, params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    do {
                        let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                        
                        let jsonmed = jsondata["data"]
                        
                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                        
                        let json = try JSONDecoder().decode([InterestData].self, from: meddata)
                        self.interestArr.removeAll()
                        self.interestArr = json
                        
                    } catch let err {
                        print(err)
                    }
                    
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func getAgeRange()-> (Int,Int) {
        
        var range: (Int,Int) = (0,0)
        //var cnt = 0
        for value in ageGroupArr {
            if String(value) == age {
                
                range = (value,100)
                
                break
            }
        }
        return range
    }
    
    func uploadMedia(){
        
        IHProgressHUD.show()
        apiManager.uploadImg(image: blogImg!, name: "image", api: UserFeedImageAPI) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let imgdata):
                    
                    let imgstr = String(data: imgdata, encoding: .utf8)
                    print(imgstr)
                        
                        if((imgstr?.contains("jpeg"))!){
                            self.strBlogImg = imgstr
                            self.addBlog()
                        }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    func uploadPDFMedia(){
        
        IHProgressHUD.show()
        apiManager.uploadPDF(image: pdfData!, name: "image", api: UserFeedImageAPI) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let imgdata):
                    
                    let imgstr = String(data: imgdata, encoding: .utf8)
                    print(imgstr)
                        
                        if((imgstr?.contains("pdf"))!){
                            
                            self.strBlogImg = imgstr
                            self.addBlog()
                           
                        }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func addBlog() {
        
        IHProgressHUD.show()
        
        let interests: [String] = selInterestArr.map{ ($0.interest ?? "") }
        var age_from: Int = 0
        var age_to: Int = 0
        
        do {
            let jsondata = try JSONSerialization.data(withJSONObject: interests, options: .prettyPrinted)
            let strjson = String(data: jsondata, encoding: .utf8)
            
            let genderdata = JSON(selGenderArr)
            let strGenderJson = genderdata.rawString()
            
            (age_from, age_to) = getAgeRange()
            
            let params = ["feed_type": blogType == "Article" ? "image/text": "image",
                          "image_video": self.strBlogImg ?? "",
                          "video_thumb": self.strBlogImg ?? "",
                          "title": blogTitle,
                          "description": articleContent ,
                          "sharing_type": "public",
                          "user_id": loggedUser!.id,
                          "age_from": age_from,
                          "age_to": age_to,
                          "interest": strjson,
                          "gender": strGenderJson
                ] as [String : Any]
            
            
            apiManager.addUserFeed(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                
                switch result {
                    
                case .success(let data):
                    
                    DispatchQueue.main.async {
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.navigationController?.popViewController(animated: false)
                    }
                case .failure(let err):
                    print(err)
                    DispatchQueue.main.async {
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                    }
                }
            }
        } catch {
            
        }
    }
    
    func validate() -> Bool {
        
        if blogTitle.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please enter blog title")
            return false
        }
        if blogType.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please select blog type")
            return false
        }
        if blogType == "Media" && blogImg == nil {
            self.alert(strTitle: strErr, strMsg: "Please upload image")
            return false
        }
        if blogType == "Article" && articleContent.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please enter article content")
            return false
        }
        if selInterestArr.count == 0 {
            self.alert(strTitle: strErr, strMsg: "Please select interest filter")
            return false
        }
        if selGenderArr.count == 0 {
            self.alert(strTitle: strErr, strMsg: "Please select gender filter")
            return false
        }
        if age == nil ||  age!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please select age group")
            return false
        }
        return true
    }
    
    func showAlert(id: String) {
    
        initAlert(id: id)
        addMedAlertView.show()
        
    }
    
    func initAlert(id: String) {
        
        let nib = Bundle.main.loadNibNamed("AddMedAlertView", owner: self, options: nil)
        let contentView = nib?[0] as! UIView
    
        //addMedAlertView = SwiftAlertView(nibName: "AddMedAlertView", delegate: self, cancelButtonTitle: nil, otherButtonTitles: nil)
        addMedAlertView = SwiftAlertView(contentView: contentView, delegate: self, cancelButtonTitle: nil)
        addMedAlertView.dismissOnOtherButtonClicked = true
        addMedAlertView.dismissOnOutsideClicked = true
        addMedAlertView.kDefaultHeight = 375
        addMedAlertView.tag = 1
        
        let tblAlert = addMedAlertView.viewWithTag(10) as! UITableView
        tblAlert.dataSource = self
        tblAlert.delegate = self
        tblAlert.accessibilityIdentifier = id
        
        let lblTitle = addMedAlertView.viewWithTag(13) as! UILabel
        let lblTitle1 = addMedAlertView.viewWithTag(14) as! UILabel
        
        lblTitle1.text = ""
        
        if id == "interest" {
            lblTitle.text = "PLEASE SELECT INTERESTS"
        }
        if id == "gender" {
            lblTitle.text = "PLEASE SELECT GENDER"
        }
        
        tblAlert.register(UINib(nibName: "InterestCell", bundle: nil), forCellReuseIdentifier: "interestCell")
        
        let btnAdd = addMedAlertView.viewWithTag(12) as! UIButton
        btnAdd.addTarget(self, action: #selector(alertAddAction), for: .touchUpInside)
        btnAdd.accessibilityIdentifier = id
        
        let btnClose = addMedAlertView.viewWithTag(11) as! UIButton
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
     
        addMedAlertView.dismiss()
    }
    
    @IBAction func alertAddAction(_ sender: UIButton) {
        
        if sender.accessibilityIdentifier == "interest" {
            let indexpath = IndexPath(row: 0, section: 1)
            self.tblData.reloadRows(at: [indexpath], with: .none)
        }
        if sender.accessibilityIdentifier == "gender" {
            let indexpath = IndexPath(row: 1, section: 1)
            self.tblData.reloadRows(at: [indexpath], with: .none)
        }
        addMedAlertView.dismiss()
    }
    
    @IBAction func checkAction(_ sender: UIButton) {
        
        let id = sender.accessibilityIdentifier
        sender.isSelected = !sender.isSelected
        
        if id == "interest" {
            if sender.isSelected {
                selInterestArr.append(interestArr[sender.tag])
            } else {
                let data = interestArr[sender.tag]
                if let index = selInterestArr.firstIndex(where: { (interest) -> Bool in
                    return interest.id == data.id
                }) {
                    selInterestArr.remove(at: index)
                }
            }
            print(selInterestArr)
        }
        if id == "gender" {
            if sender.isSelected {
                selGenderArr.append(genderArr[sender.tag])
            } else {
                let data = genderArr[sender.tag]
                if let index = selGenderArr.firstIndex(of: data) {
                    selGenderArr.remove(at: index)
                }
            }
            print(selGenderArr)
        }
    }
    
    @objc func tagCellAction(_ sender: UITapGestureRecognizer) {
        
        if sender.accessibilityHint  == "interest" || sender.accessibilityHint == "gender" {
            self.showAlert(id: sender.accessibilityHint!)
        }
    }
    
    func tagTouched(_ tag: TagView) {
        print("tagTouched")
    }
    
    func tagDismissed(_ tag: TagView) {
        print("tagDismissed")
    }
    
    @objc func mediaAction(_ sender: UITapGestureRecognizer) {
        
        self.showImgPicker()
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.accessibilityIdentifier == "interest" || textField.accessibilityIdentifier == "gender" {
            
            self.showAlert(id: textField.accessibilityIdentifier!)
            return false
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddBlogVC: UIDocumentPickerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        self.pdfFlag = true
        do {
            self.pdfData = try Data(contentsOf: myURL)
        } catch let err {
            print("pdf err: \(err.localizedDescription)")
        }
        print("import result : \(myURL)")
        self.tblData.reloadData()
    }


    public func documentMenu(_ documentMenu:UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }


    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}
extension AddBlogVC: AddBlogCellDelegate {
    
    func onValueChanged(id: String, value: Any) {
        switch id {
            
        case "title":
            blogTitle = value as! String
            
        case "text":
            articleContent = value as! String
            
        case "type":
            blogType = typeArr[value as! Int]
            
            self.tblData.reloadData()
            
        case "interest":
            print("interest: \(value as! InterestData)")
            
        case "age":
            age = value as? String
            
        default:
            break
        }
    }
    
    func onSaveClicked() {
        
        if validate() {
            if blogType == "Media" {
                if pdfData != nil {
                    self.uploadPDFMedia()
                } else {
                    self.uploadMedia()
                }
            }
            else {
                if blogImg != nil {
                    self.uploadMedia()
                } else if pdfData != nil {
                    self.uploadPDFMedia()
                } else {
                    self.addBlog()
                }
            }
        }
    }
}
extension AddBlogVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == tblData {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblData {
            return section == 0 ? 4 : 3
        }
        if tableView.accessibilityIdentifier == "interest" {
            return interestArr.count
        }
        if tableView.accessibilityIdentifier == "gender" {
            return genderArr.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            if indexPath.row == 0 && indexPath.row == 1 {
                
                (cell as! AddBlogCell).tagView.layer.borderColor = UIColor.gray.cgColor
                (cell as! AddBlogCell).tagView.layer.borderWidth = 1.0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = AddBlogCell()
        if tableView == tblData {
            switch indexPath.section {
                
            case 0:
                
                switch indexPath.row {
                    
                case 0:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! AddBlogCell
                    cell.delegate = self
                    cell.setup(id: "title", placeHolder: "CTRL-H BLOG TITLE")
                    
                    return cell
                    
                case 1:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "radioCell") as! AddBlogCell
                    
                    cell.delegate = self
                    
                    let tap = UITapGestureRecognizer(target: cell, action: #selector(cell.radioAction(_:)))
                    cell.radioBtn1.addGestureRecognizer(tap)
                    
                    let tap1 = UITapGestureRecognizer(target: cell, action: #selector(cell.radioAction(_:)))
                    cell.radioBtn2.addGestureRecognizer(tap1)
                    
                    return cell
                case 2:
                    
                    //if blogType == typeArr[1] {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "mediaCell") as! AddBlogCell
                        
                        cell.imgBlog.isUserInteractionEnabled = true
                        
                        
                        if blogImg == nil {
                            
                            let tap = UITapGestureRecognizer(target: self, action: #selector(mediaAction))
                            cell.imgBlog.addGestureRecognizer(tap)
                        } else {
                            if let img = blogImg {
                                cell.imgBlog.image = UIImage(data: img)
                            }
                        }
                        return cell
                    //}
                 
                case 3:
                
                //if blogType == typeArr[0] {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "textCell") as! AddBlogCell
                    
                    cell.delegate = self
                    cell.setup(id: "text", placeHolder: "")
                    //let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 44))
                    //toolbar.options = RichEditorDefaultOption.all
                    //toolbar.editor = cell.editor
                    
                    return cell
                //}
                    
                default:
                    break
                }
            
            
                
            case 1:
                
                switch indexPath.row {
                    
                case 0:
                    
                    /*
                    let cell = tableView.dequeueReusableCell(withIdentifier: "dropDownCell") as! AddBlogCell
                    cell.pickerDataArr = interestArr
                    cell.delegate = self
                    cell.setup(id: "interest", placeHolder: "INTEREST FILTER")
                    cell.txtFld.accessibilityIdentifier = "interest"
                    cell.txtFld.delegate = self
                    */
                    let cell = tableView.dequeueReusableCell(withIdentifier: "tagCell") as! AddBlogCell
                    
                    
                    
                    let tap = UITapGestureRecognizer(target: self, action: #selector(tagCellAction))
                    tap.accessibilityHint = "interest"
                    cell.tagView.addGestureRecognizer(tap)
                    
                    if selInterestArr.count > 0 {
                        for data in selInterestArr {
                            cell.tagView.tags.append(TagView(text: data.interest!))
                        }
                        cell.lblTitle.text = ""
                        cell.tagView.layer.borderColor = UIColor.gray.cgColor
                        cell.tagView.layer.borderWidth = 1.0
                        cell.lblTitle.layer.borderWidth = 0.0
                        
                    } else {
                        cell.lblTitle.text = " INTEREST FILTER"
                        cell.lblTitle.layer.borderColor = UIColor.gray.cgColor
                        cell.lblTitle.layer.borderWidth = 1.0
                    }
                    
                    
                    return cell
                    
                case 1:
                    /*
                    let cell = tableView.dequeueReusableCell(withIdentifier: "selectCell") as! AddBlogCell
                    cell.pickerDataArr = genderArr
                    cell.delegate = self
                    cell.setup(id: "select", placeHolder: "GENDER FILTER")
                    cell.txtFld.accessibilityIdentifier = "gender"
                    cell.txtFld.delegate = self
                    */
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ageTagCell") as! AddBlogCell
                    
                    cell.pickerDataArr = genderArr
                    cell.delegate = self
                    cell.setup(id: "select", placeHolder: "GENDER FILTER")
                    
                    let tap = UITapGestureRecognizer(target: self, action: #selector(tagCellAction))
                    tap.accessibilityHint = "gender"
                    cell.tagView.addGestureRecognizer(tap)
                    
                    if selGenderArr.count > 0 {
                        for data in selGenderArr {
                            cell.tagView.tags.append(TagView(text: data))
                        }
                        cell.lblTitle.text = ""
                        cell.tagView.layer.borderColor = UIColor.gray.cgColor
                        cell.tagView.layer.borderWidth = 1.0
                        cell.lblTitle.layer.borderWidth = 0.0
                        
                    } else {
                        cell.lblTitle.text = " GENDER FILTER"
                        cell.lblTitle.layer.borderColor = UIColor.gray.cgColor
                        cell.lblTitle.layer.borderWidth = 1.0
                    }
                    
                    return cell
                    
                case 2:
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! AddBlogCell
                    cell.delegate = self
                    
                    return cell
                    
                default:
                    break
                }
                
            default:
                break
            }
        }
        if tableView.accessibilityIdentifier == "interest" || tableView.accessibilityIdentifier == "gender" {
         
            let cell = tableView.dequeueReusableCell(withIdentifier: "interestCell") as! AddBlogCell
            
            if tableView.accessibilityIdentifier == "interest" {
                let data = interestArr[indexPath.row]
                cell.lblTitle.text = data.interest
                
                cell.btnChk.isSelected = false
                
                let arr = selInterestArr.filter{ $0.id == data.id}
                if arr.count > 0 {
                    cell.btnChk.isSelected = true
                }
            }
            if tableView.accessibilityIdentifier == "gender" {
                let data = genderArr[indexPath.row]
                cell.lblTitle.text = data
                
                cell.btnChk.isSelected = false
                
                if selGenderArr.contains(data) {
                    cell.btnChk.isSelected = true
                }
            }
            cell.btnChk.tag = indexPath.row
            cell.btnChk.accessibilityIdentifier = tableView.accessibilityIdentifier
            cell.btnChk.addTarget(self, action: #selector(checkAction(_:)), for: .touchUpInside)
            
            return cell
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView()
        if tableView == tblData {
            if section == 1 {
                
                view.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50)
                
                let label = UILabel()
                label.frame = CGRect(x: 20, y: 0, width: tableView.frame.width, height: 40)
                
                label.text = "BLOG ATTRIBUTES"
                view.addSubview(label)
                
            }
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if tableView == tblData {
            return section == 1 ? 50 : 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblData {
            if indexPath.section == 0 {
                
                if indexPath.row == 0 {
                    return 45
                }
                if indexPath.row == 1 {
                    return 160
                }
                if indexPath.row == 2 {
                    return blogType.isEmpty ? 0 : 160
                }
                if indexPath.row == 3 {
                    return (!(blogType.isEmpty) && blogType == typeArr[0]) ? 160 : 0
                }
            }
            if indexPath.section == 1 {
                return UITableView.automaticDimension
            }
            return 45
        }
        return 40
    }
}
extension AddBlogVC: UIImagePickerControllerDelegate {

    func showImgPicker() {
        print("Tap On Image")
        imgPicker.delegate = self
    
        let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {
            action in
            
            self.imgPicker.allowsEditing = true
            self.imgPicker.sourceType = .camera
            self.imgPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            self.present(self.imgPicker, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: {
            action in
            
            self.imgPicker.allowsEditing = true
            self.imgPicker.sourceType = .photoLibrary
            self.imgPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(self.imgPicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "PDF", style: .default, handler: {
            action in
            let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
            importMenu.delegate = self
            importMenu.modalPresentationStyle = .formSheet
            self.present(importMenu, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
    guard let image = info[.editedImage] as? UIImage else {
        return
    }
        if let imgdata = image.jpegData(compressionQuality: 0.5){
            self.blogImg = imgdata
            
        }
        self.tblData.reloadData()
        dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
}
protocol AddBlogCellDelegate {
    
    func onValueChanged(id: String, value: Any)
    func onSaveClicked()
}
class AddBlogCell: BaseCell {
    
    @IBOutlet weak var txtFld: RightViewTextField!
    @IBOutlet weak var txtFld1: RightViewTextField!
    @IBOutlet weak var bottomTxtFld: UITextField!//BottomLineTextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var radioBtn1: RadioButton!
    @IBOutlet weak var radioBtn2: RadioButton!
    @IBOutlet weak var imgBlog: UIImageView!
    @IBOutlet weak var btnChk: UIButton!
    @IBOutlet weak var tagView: CloudTagView!
    @IBOutlet weak var editor: RichEditorView!
    
    var delegate: AddBlogCellDelegate?
    var selRow: Int = 0
    
    func setup(id: String, placeHolder: String) {
        
        if id == "title" {
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            bottomTxtFld.removeBottomBorder()
            bottomTxtFld.addBottomBorder()
        }
        
        if id == "text" {
            editor.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
        
        if id == "interest" {
            txtFld.placeholder = placeHolder
            /*
            txtFld.inputView = cellPicker
            cellPicker.dataSource = self
            cellPicker.delegate = self
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
             */
        }
        if id == "select" {
            //txtFld.placeholder = placeHolder
            //txtFld1.placeholder = "Select"
            
            /*
            txtFld.inputView = cellPicker
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = "gender"
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "gender")
            */
            
            
            
            let agePicker = UIPickerView()
            agePicker.accessibilityIdentifier = "age"
            agePicker.dataSource = self
            agePicker.delegate = self
            
            txtFld1.inputView = agePicker
            txtFld1.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "age")
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        selRow = row
        let id = pickerView.accessibilityIdentifier
        
        if id == "gender" {
            let data = pickerDataArr[row] as? String
            txtFld.text = data
        }
        if id == "age" {
            let data = ageGroupArr[row]
            txtFld1.text = String(data)
        }
        if id == "interest" {
            let data = pickerDataArr[row] as? InterestData
            txtFld.text = data?.interest
        }
    }
    
    override func doneCellPicker(sender: UIBarButtonItem) {
        
        let id = sender.accessibilityIdentifier
        
        if id == "title" {
            delegate?.onValueChanged(id: id!, value: bottomTxtFld.text ?? "")
            bottomTxtFld.resignFirstResponder()
        }
        
        if id == "text" {
            delegate?.onValueChanged(id: id!, value: editor.html ?? "")
            self.endEditing(true)
        }
        
        if id == "gender" {
            let data = pickerDataArr[selRow] as? String
            txtFld.text = data
            delegate?.onValueChanged(id: id!, value: data ?? "")
            txtFld.resignFirstResponder()
        }
        if id == "age" {
            let data = ageGroupArr[selRow]
            txtFld1.text = String(data)
            delegate?.onValueChanged(id: id!, value: String(data))
            txtFld1.resignFirstResponder()
        }
        if id == "interest" {
            let data = pickerDataArr[selRow] as? InterestData
            txtFld.text = data?.interest
            delegate?.onValueChanged(id: id!, value: data)
            txtFld.resignFirstResponder()
        }
    }
    
    @IBAction func radioAction(_ sender: UITapGestureRecognizer) {
        
        radioBtn1.isSelected = false
        radioBtn2.isSelected = false
        
        if sender.view == radioBtn1 {
            radioBtn1.isSelected = true
            delegate?.onValueChanged(id: "type", value: 0)
        }
        if sender.view == radioBtn2 {
            radioBtn2.isSelected = true
            delegate?.onValueChanged(id: "type", value: 1)
        }
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        
        delegate?.onSaveClicked()
    }
}
