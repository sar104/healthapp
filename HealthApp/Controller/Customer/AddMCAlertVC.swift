//
//  AddMCAlertVC.swift
//  HealthApp
//
//  Created by Apple on 31/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class AddMCAlertVC: BaseVC, AddMCAlertCellDelegate, UIStepperControllerDelegate {
    
    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var dateContainer: UIView!
    
    var periodDate: Date?
    var cycleDays: Int = 28
    var mcPeriod: Int = 5
    var ovulPeriod = 5
    
    var nextPeriodDate: Date?
    var ovulationDate: Date?
    var strPeriod: String = ""
    var strOvulation: String = ""
    
    var medAlertData: MedAlertData = MedAlertData()
    var arrAlertTimings: [AlertTiming] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        medAlertData.user_id = Int(loggedUser!.id)
        medAlertData.alert_type = kAlertTypeMC
        medAlertData.medicine_name = kMCTracker
        
    }
    

    func onValueSelected(id: String, value: String) {
        
    }
    
    @IBAction func doneAction(_ sender: UIButton) {
        
        let tag = sender.tag
        
        if tag == 1 {
            periodDate = datePicker.date
        }
        if tag == 2 {
            
        }
        dateContainer.isHidden = true
        self.tblData.reloadData()
    }
    
    func onSaveClicked(id: Int, type: String) {
        
        if id == 0 {
            
            dateContainer.isHidden = false
            
        } else if id == 1 {
            
            if type == "track" {
                if validate() {
                    setMCAlertData()
                }
            }
            if type == "setAlert" {
                if validate() {
                    setNotifications()
                    addMCTrackerAlert()
                }
            }
        }
    }
    
    func setMCAlertData() {
        
        print("\(periodDate), \(mcPeriod), \(cycleDays)")
        
        nextPeriodDate = periodDate?.dateByAdding(component: .day, value: cycleDays)
        ovulationDate = nextPeriodDate?.dateByAdding(component: .day, value: 10)
        let nextPeriodEndDate = nextPeriodDate?.dateByAdding(component: .day, value: mcPeriod)
        let ovulationEndDate = ovulationDate?.dateByAdding(component: .day, value: 5)
        
        let strfrom = nextPeriodDate?.toString(format: "dd MMM")
        let strto = nextPeriodEndDate?.toString(format: "dd MMM")
        
        strPeriod = strfrom! + " - " + strto!
        
        let strfrom1 = ovulationDate?.toString(format: "dd MMM")
        let strto1 = ovulationEndDate?.toString(format: "dd MMM")
        
        strOvulation = strfrom1! + " - " + strto1!
        print("\(nextPeriodDate), \(ovulationDate)")
        
        do {
            let med_unit = ["member_id" : "", "member_name": "", "member_dob" : periodDate?.toString(format: df_dd_MM_yyyy)]
            let medunitData = try JSONSerialization.data(withJSONObject: med_unit, options: [])
            let jsonStrMedUnit = String(data: medunitData, encoding: .utf8)
            medAlertData.medicine_unit = jsonStrMedUnit
            
            // prior 2 days
            let nextPeriod2days = nextPeriodDate?.dateByAdding(component: .day, value: -2)
            var alertTiming = AlertTiming()
            alertTiming.timming = nextPeriod2days?.toString(format: df_dd_MM_yyyy)
            let millisec = nextPeriod2days!.convertToMilli()
            alertTiming.time_in_miles = String(millisec)
            
            var timeCategory = TimeCategory()
            timeCategory.alert_name = mcPrePeriodTitle
            timeCategory.alert_text = mcAlertTitle_2
            timeCategory.alert_status =   "pending"
            
            alertTiming.timecategory = timeCategory
            
            let timeData = try JSONEncoder().encode(timeCategory)
            let jsonStrTime = String(data: timeData, encoding: .utf8)
            
            alertTiming.time_category = jsonStrTime
            arrAlertTimings.append(alertTiming)
            
            // prior 1 day
            let nextPeriod1days = nextPeriodDate?.dateByAdding(component: .day, value: -1)
            var alertTiming1 = AlertTiming()
            alertTiming1.timming = nextPeriod1days?.toString(format: df_dd_MM_yyyy)
            let millisec1 = nextPeriod2days!.convertToMilli()
            alertTiming1.time_in_miles = String(millisec1)
            
            var timeCategory1 = TimeCategory()
            timeCategory1.alert_name = mcPrePeriodTitle
            timeCategory1.alert_text = mcAlertTitle_1
            timeCategory1.alert_status =   "pending"
            
            alertTiming1.timecategory = timeCategory1
            
            let timeData1 = try JSONEncoder().encode(timeCategory1)
            let jsonStrTime1 = String(data: timeData1, encoding: .utf8)
            
            alertTiming1.time_category = jsonStrTime1
            arrAlertTimings.append(alertTiming1)
            
            for i in 0..<mcPeriod {
                let nextPerioddays = nextPeriodDate?.dateByAdding(component: .day, value: i)
                var alertTiming1 = AlertTiming()
                alertTiming1.timming = nextPerioddays?.toString(format: df_dd_MM_yyyy)
                let millisec1 = nextPerioddays!.convertToMilli()
                alertTiming1.time_in_miles = String(millisec1)
                
                var timeCategory1 = TimeCategory()
                timeCategory1.alert_name = mcPeriodDayTitle
                timeCategory1.alert_text = mcAlertTitle_today
                timeCategory1.alert_status =   "pending"
                
                alertTiming1.timecategory = timeCategory1
                
                let timeData1 = try JSONEncoder().encode(timeCategory1)
                let jsonStrTime1 = String(data: timeData1, encoding: .utf8)
                
                alertTiming1.time_category = jsonStrTime1
                arrAlertTimings.append(alertTiming1)
            }
            
            for i in 10..<15 {
                let nextPerioddays = nextPeriodDate?.dateByAdding(component: .day, value: i)
                var alertTiming1 = AlertTiming()
                alertTiming1.timming = nextPerioddays?.toString(format: df_dd_MM_yyyy)
                let millisec1 = nextPerioddays!.convertToMilli()
                alertTiming1.time_in_miles = String(millisec1)
                
                var timeCategory1 = TimeCategory()
                timeCategory1.alert_name = mcOvulTitle
                timeCategory1.alert_text = mcOvulAlertTitle
                timeCategory1.alert_status =   "pending"
                
                alertTiming1.timecategory = timeCategory1
                
                let timeData1 = try JSONEncoder().encode(timeCategory1)
                let jsonStrTime1 = String(data: timeData1, encoding: .utf8)
                
                alertTiming1.time_category = jsonStrTime1
                arrAlertTimings.append(alertTiming1)
            }
            
        } catch let err {
            
        }
        self.tblData.reloadData()
    }
    
    func setNotifications() {
        
        var arrNotif: [LocalNotification] = []
        let manager = LocalNotificationManager()
        
        for alert in arrAlertTimings {
            
            let strdate = alert.timming
            let mutableStr = NSMutableString()
            
            mutableStr.append(kAlertTypeMC)
            mutableStr.append("_")
            mutableStr.append(strPeriod)
            mutableStr.append("_")
            mutableStr.append(strdate!)
            
            let notifId = mutableStr as String
            
            let alertTitle = alert.timecategory?.alert_text
            let alertDate = strdate?.toDate(format: df_dd_MM_yyyy)
            
            let notif =
                LocalNotification(id: notifId, title: alertTitle!,
                                  datetime: DateComponents(calendar: Calendar.current, year: alertDate!.get(.year), month: alertDate!.get(.month), day: alertDate!.get(.day)))
        
            arrNotif.append(notif)
        }
        
        //print("notification\(arrNotif)")
        manager.notifications = arrNotif
        manager.schedule()
    }
    
    func addMCTrackerAlert(){
        
        do {
            
            let alertTimingData = try JSONEncoder().encode(arrAlertTimings)
            
            let jsonStrAlertTiming = String(data: alertTimingData, encoding: .utf8)
            
            var params: [String: Any] = [:]
            
            params["user_id"] = loggedUser?.id
            params["medicine_name"] = medAlertData.medicine_name
            params["medicine_unit"] = medAlertData.medicine_unit
            params["alert_timmings"] = jsonStrAlertTiming
            params["alert_type"] = medAlertData.alert_type
            
            print("params: \(params)")
            
            IHProgressHUD.show()
            apiManager.addMedicineAlert(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
            
        } catch let err {
            print(err)
        }
    }
    
    func validate() -> Bool {
        
        if periodDate == nil {
            self.alert(strTitle: strSuccess, strMsg: "Please select last period date")
            return false
        }
        if mcPeriod == 0 {
            self.alert(strTitle: strSuccess, strMsg: "Please enter days it last")
            return false
        }
        if cycleDays == 0 {
            self.alert(strTitle: strSuccess, strMsg: "Please enter menstrual cycle period")
            return false
        }
        
        return true
    }
    
    func stepperDidAddValues(stepper: UIStepperController) {
        
        if stepper.tag == 0 {
            mcPeriod = Int(stepper.count)
        }
        if stepper.tag == 1 {
            cycleDays = Int(stepper.count)
        }
    }
    
    func stepperDidSubtractValues(stepper: UIStepperController) {
        
        if stepper.tag == 0 {
            mcPeriod = Int(stepper.count)
        }
        if stepper.tag == 1 {
            cycleDays = Int(stepper.count)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddMCAlertVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = AddMCAlertCell()
        
        if indexPath.row == 0 {
            
            if nextPeriodDate == nil {
                cell = tableView.dequeueReusableCell(withIdentifier: "mcAlertCell") as! AddMCAlertCell
                cell.delegate = self
                cell.setup(id: "mcAlertCell")
                
                if periodDate != nil {
                    cell.lblDate.text = periodDate?.toString(format: df_dd_MM_yyyy)
                    cell.lblDate.isHidden = false
                    cell.btnCal.isHidden = true
                } else {
                    cell.lblDate.isHidden = true
                    cell.btnCal.isHidden = false
                }
                
                cell.stepper1.count = 5
                cell.stepper2.count = 28
                
                cell.stepper1.delegate = self
                cell.stepper2.delegate = self
            } else {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "mcCell") as! AddMCAlertCell
                cell.delegate = self
                cell.setup(id: "mcCell")
                
                cell.lblDate.isHidden = false
                cell.lblDate.text = strPeriod
                cell.lblOvulDate.text = strOvulation
            }
        }
        if indexPath.row == 1 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "btnAlertCell") as! AddMCAlertCell
            cell.delegate = self
            cell.setup(id: "btnAlertCell")
            let btnSave = cell.contentView.viewWithTag(1) as! UIButton
            
            if nextPeriodDate == nil {
                btnSave.setTitle("TRACK NOW", for: .normal)
            } else {
                btnSave.setTitle("ADD ALERT", for: .normal)
            }
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
         if indexPath.row == 0 {
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            if nextPeriodDate == nil {
               // return 400
            }
            return 400
        }
        return 50
    }
    
}

protocol AddMCAlertCellDelegate {
    
    func onValueSelected(id: String, value: String)
    func onSaveClicked(id: Int, type: String)
}

class AddMCAlertCell: BaseCell {
    
    var delegate: AddMCAlertCellDelegate?
    @IBOutlet weak var alertBgView: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblOvulDate: UILabel!
    @IBOutlet weak var btnCal: UIButton!
    @IBOutlet weak var stepper1: UIStepperController!
    @IBOutlet weak var stepper2: UIStepperController!
    
    var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        //gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [RGB_254_0_75.cgColor, RGB_0_197_223.cgColor]
        gradientLayer.locations = [0.0, 1.0]
       return gradientLayer
    }()
    
    func setup(id: String) {
        
        if id == "mcAlertCell" || id == "mcCell" {
            gradientLayer.frame = alertBgView.bounds
            self.alertBgView.layer.insertSublayer(gradientLayer, at: 0)
            self.alertBgView.layer.cornerRadius = 20
            self.alertBgView.layer.masksToBounds = true
        }
        if id == "btnAlertCell" {
            
            
        }
    }
    
    @IBAction func saveAction(_ sender: UIButton){
        
        if sender.tag == 0 {
            delegate?.onSaveClicked(id: sender.tag, type: "")
        } else {
            //
            if let title = sender.title(for: .normal), title == "TRACK NOW" {
                delegate?.onSaveClicked(id: sender.tag, type: "track")
            }
            if let title = sender.title(for: .normal), title == "ADD ALERT" {
                delegate?.onSaveClicked(id: sender.tag, type: "setAlert")
            }
        }
    }
}
