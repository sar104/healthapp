//
//  AddDoctorVC.swift
//  HealthApp
//
//  Created by Apple on 11/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class AddDoctorVC: BaseVC, AddDoctorCellDelegate, UITextFieldDelegate {
    

    @IBOutlet weak var tblData: UITableView!
    
    var doctorName: String = ""
    var mobile: String = ""
    var hospital: String = ""
    var specialization: String = ""
    var address: String = ""
    var user: User?
    var doctorData: DetailData?
    var doctorExistFlag: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblData.estimatedRowHeight = 90
        let users = self.fetchUser()
        user = users[0]
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func addDoctor(){
        /*
         user_id - login user_id
         name - name of doctor
         clinic_lab_hospital_name
         specialization
         address
         mobile_no - the number which successfully gets doctor profile

         **/
        IHProgressHUD.show()
        
        let params = ["name": doctorName,
        "clinic_lab_hospital_name": hospital,
        "mobile_no": mobile,
        "address":address,
        "specialization": specialization,
        "user_id":user!.id] as [String:Any]
        
        apiManager.addDoctorAPI(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let data):
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                case .failure(let err):
                    print("err: \(err)")
                    self.alert(strTitle: strErr, strMsg: err.localizedDescription)
            }
        }
    }

    
    func onValueSelected(type: String, value: String) {
        
        var row = 0
        
        switch type {
        case "drname":
            doctorName = value
            row = 0
        case "clinic":
            hospital = value
            row = 1
        case "special":
            specialization = value
            row = 2
        case "address":
            address = value
            row = 3
        case "phone":
            mobile = value
            row = 4
        default:
            break
        }
        
        let indexpath = IndexPath(row: row, section: 0)
        self.tblData.reloadRows(at: [indexpath], with: .none)
    }
    func onSaveClicked(){
        
        if self.validate() {
           
            self.addDoctor()
            
        }
    }
    
    func validate() -> Bool{
        
        
        if self.doctorName.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter doctor name")
            return false
        }
        if self.hospital.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter hospital/clinic name")
            return false
        }
        if self.specialization.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select specialization")
            return false
        }
        if self.address.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter address")
            return false
        }
        if self.mobile.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter mobile number")
            return false
        }
        
        return true
    }
    
    @objc func getDoctorByMobile(mob: String) {
        
        //if let mobile = mob {
             
            /*if mobile.count < 10 {
                
                self.alert(strTitle: strErr, strMsg: "Please enter valid mobile number")
                
            } else {*/
                IHProgressHUD.show()
                self.mobile = mob
                
                let params = ["mobile_no": mobile]  as [String : Any]
                
                apiManager.getDocotrByMobile(params: params) { (result) in
                    
                    IHProgressHUD.dismiss()
                    switch result {
                    case .success(let data):
                        print("data: \(data)")
                        self.doctorData = data
                        self.doctorName = data.name ?? ""
                        self.hospital = data.clinic_lab_hospital_name ?? ""
                        self.specialization = data.specialization ?? ""
                        self.address = data.address ?? ""
                        
                        
                        self.doctorExistFlag = true
                        DispatchQueue.main.async {
                            
                            self.tblData.reloadData()
                        }
                        
                    case .failure(let err):
                        print("err: \(err)")
                        
                        DispatchQueue.main.async {
                            self.alert(strTitle: strErr, strMsg: "Doctor does not exists")
                            //self.tblData.reloadData()
                        }
                    }
                }
            //}
        //}
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        
        if textField.accessibilityIdentifier == "mobile" {
            print("string: \(string)")
            /*
            NSObject.cancelPreviousPerformRequests(
                withTarget: self,
                selector: #selector(getDoctorByMobile),
                object: textField)
            self.perform(
                #selector(getDoctorByMobile),
                with: textField,
                afterDelay: 1.2)
 */
             
            if range.location == 9 && range.length != 1 {
                let mob = textField.text! + string
                self.getDoctorByMobile(mob: mob)

            }
        }
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddDoctorVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = AddDoctorCell()
        switch indexPath.row {
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: "simpleTxtFldCell") as! AddDoctorCell
            cell.delegate = self
            cell.setup(id: "mobile", placeHolder: "Mobile No")
            cell.bottomTxtFld.delegate = self
            //cell.pickerDataArr = self.familyMembersArr
            
        case 1:
            /*
            cell = tableView.dequeueReusableCell(withIdentifier: "simpleTxtFldCell") as! AddDoctorCell
            cell.delegate = self
            cell.setup(id: "drname", placeHolder: "Doctor Name")
            //cell.pickerDataArr = self.familyMembersArr
            cell.bottomTxtFld.text = doctorData?.name ?? doctorName
             */
            cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! AddDoctorCell
            
            let lbltitle = cell.contentView.viewWithTag(1) as! UILabel
            let lblname = cell.contentView.viewWithTag(2) as! UILabel
            
            lbltitle.text = "DOCTOR NAME"
            lblname.text = doctorData?.name ?? doctorName
            
        case 2:
            /*
            cell = tableView.dequeueReusableCell(withIdentifier: "simpleTxtFldCell") as! AddDoctorCell
            cell.delegate = self
            cell.setup(id: "clinic", placeHolder: "Hospital/Clinic name")
            cell.bottomTxtFld.text = doctorData?.clinic_lab_hospital_name ?? hospital
 */
            cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! AddDoctorCell
            
            let lbltitle = cell.contentView.viewWithTag(1) as! UILabel
            let lblname = cell.contentView.viewWithTag(2) as! UILabel
            
            lbltitle.text = "HOSPITAL/CLINIC NAME"
            lblname.text = doctorData?.clinic_lab_hospital_name ?? hospital
            
            
        case 3:
            /*
            cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! AddDoctorCell
            cell.delegate = self
            cell.setup(id: "special", placeHolder: "Specialization")
            cell.pickerDataArr = specilizationArr
            cell.txtFld.text = doctorData?.specialization ?? specialization
 */
            cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! AddDoctorCell
            
            let lbltitle = cell.contentView.viewWithTag(1) as! UILabel
            let lblname = cell.contentView.viewWithTag(2) as! UILabel
            
            lbltitle.text = "SPECIALIZATION"
            lblname.text = doctorData?.specialization ?? specialization
            
        case 4:
            /*
            cell = tableView.dequeueReusableCell(withIdentifier: "simpleTxtFldCell") as! AddDoctorCell
            cell.delegate = self
            cell.setup(id: "address", placeHolder: "Address")
            cell.bottomTxtFld.text = doctorData?.address ?? address
 */
            cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! AddDoctorCell
            
            let lbltitle = cell.contentView.viewWithTag(1) as! UILabel
            let lblname = cell.contentView.viewWithTag(2) as! UILabel
            
            lbltitle.text = "ADDRESS"
            lblname.text = doctorData?.address ?? address
        
            
        case 5:
            cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! AddDoctorCell
            cell.delegate = self
            
        default:
            break
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row > 0 {
            return doctorExistFlag ? 100 : 0
        }
        return 70
    }
    
    //simpleTxtFldCell
}
protocol AddDoctorCellDelegate {
    
    func onValueSelected(type: String, value: String)
    func onSaveClicked()
}
class AddDoctorCell: BaseCell {

    @IBOutlet weak var txtFld: RightViewTextField!
    @IBOutlet weak var bottomTxtFld: BottomLineTextField!
    
    var delegate: AddDoctorCellDelegate?
    var selRow: Int = 0
    
    func setup(id: String, placeHolder: String){
           
            
        
           if id == "special" {
               
            self.selType = id
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            txtFld.placeholder = placeHolder
           }
        if id == "mobile" {
            
            self.selType = id
            bottomTxtFld.accessibilityIdentifier = id
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.keyboardType = .numberPad
            bottomTxtFld.removeBottomBorder()
            bottomTxtFld.addBottomBorder()
        }
        if id == "drname" {
            
            self.selType = id
            
            bottomTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.removeBottomBorder()
            bottomTxtFld.addBottomBorder()
        }
        if id == "clinic" {
            
            self.selType = id
            
            bottomTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.removeBottomBorder()
            bottomTxtFld.addBottomBorder()
        }
        if id == "address" {
            
            self.selType = id
            
            bottomTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.removeBottomBorder()
            bottomTxtFld.addBottomBorder()
        }
        if id == "phone" {
            
            self.selType = id
            bottomTxtFld.keyboardType = .numberPad
            bottomTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.removeBottomBorder()
            bottomTxtFld.addBottomBorder()
        }
        
        if id == "email" {
            
            self.selType = id
            
            bottomTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.removeBottomBorder()
            bottomTxtFld.addBottomBorder()
        }
        
        if id == "comment" {
            
            self.selType = id
            
            bottomTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.removeBottomBorder()
            bottomTxtFld.addBottomBorder()
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        print("selType:\(selType)")
        selRow = row
        
        switch selType {
            case "special":
                txtFld.text = pickerDataArr[row] as? String
            
        default:
            break
        }
        
        //delegate?.onValueSelected(type: selType, value: txtFld.text ?? "")
    }
    
    @objc override func doneCellPicker (sender: UIBarButtonItem) {
        
        let id = sender.accessibilityIdentifier
        
        if id == "special" {
            let value = (pickerDataArr[selRow] as? String)!
            txtFld.text = value
            delegate?.onValueSelected(type: id!, value: value)
        }
        if id == "drname" {
            
            delegate?.onValueSelected(type: id!, value: bottomTxtFld.text!)
        }
        if id == "clinic" {
            
            delegate?.onValueSelected(type: id!, value: bottomTxtFld.text!)
        }
        if id == "address" {
            
            delegate?.onValueSelected(type: id!, value: bottomTxtFld.text!)
        }
        if id == "phone" {
            
            delegate?.onValueSelected(type: id!, value: bottomTxtFld.text!)
        }
        if id == "comment" {
            
            delegate?.onValueSelected(type: id!, value: bottomTxtFld.text!)
        }
        
    }
    
    @IBAction func saveAction(_ sender: UIButton){
        
        delegate?.onSaveClicked()
    }
}
