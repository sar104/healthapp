//
//  PrescriptionListVC.swift
//  HealthApp
//
//  Created by Apple on 16/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD
import WebKit

protocol PrescriptionListDelegate {
    
    func onSelectPrescription(orderData: MedicineOrderData, preData: PrescriptionData)
}

class PrescriptionListVC: BaseVC {

    var prescriptionArr: [PrescriptionData] = []
    var delegate: PrescriptionListDelegate?
    var orderData: MedicineOrderData = MedicineOrderData()
    var type: String?
    var flagOrder: Bool = false
    
    @IBOutlet weak var tblData: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if type == "self" {
            getPrescriptionList()
        } else if type == "family" {
            getFamilyPrescriptionList()
        }
        
    }
    func getPrescriptionList() {
        
        let params = [PARAM_USERID: loggedUser?.id] as [String:Any]
        
        IHProgressHUD.show()
        
        apiManager.getPrescriptionList(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    self.prescriptionArr = data
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func getFamilyPrescriptionList() {
        
        let params = [PARAM_USERID: loggedUser?.id] as [String:Any]
        
        IHProgressHUD.show()
        
        apiManager.getFamilyPrescriptionList(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    self.prescriptionArr = data
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    @IBAction func addAction(_ sender: UIButton) {
        
        let vc = self.getVC(with: "AddHealthRecordSegmentVC", sb: "Main") as! AddHealthRecordSegmentVC
        vc.selIndex = 0
        UserDefaults.standard.set(1, forKey: "HealthRecordSegment")
        self.navigationController?.pushViewController(vc, animated: false)
    }

    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        
        let id = sender.view?.accessibilityIdentifier
        if id == "pdf" {
            
            let tag = sender.view?.tag
            let data = self.prescriptionArr[tag!]
            if let strurl = data.images[0].image {
                
                if let url = URL(string: strurl) {
                    let webView = WKWebView(frame: view.frame)
                    let urlRequest = URLRequest(url: url)
                    webView.load(urlRequest)
                    let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
                    webView.addGestureRecognizer(tap)
                    view.addSubview(webView)
                }
            }
            
        } else {
            let imageView = sender.view as! UIImageView
            
            let newImageView = UIImageView(image: imageView.image)
            newImageView.frame = UIScreen.main.bounds
            newImageView.backgroundColor = .black
            newImageView.contentMode = .scaleAspectFit
            newImageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
            newImageView.addGestureRecognizer(tap)
            self.view.addSubview(newImageView)
            self.navigationController?.isNavigationBarHidden = true
        }
        //self.tabBarController?.tabBar.isHidden = true
    }

    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        //self.tabBarController?.tabBar.isHidden = false
        sender.view?.removeFromSuperview()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PrescriptionListVC: UITableViewDataSource, UITableViewDelegate, PrescriptionCellDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.prescriptionArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "prescriptionCell") as! PrescriptionCell
        
        let data = self.prescriptionArr[indexPath.row]
        //cell.btnFav.tag = indexPath.row
        //cell.btnOrder.tag = indexPath.row
        cell.delegatePresc = self
        cell.row = indexPath.row
        cell.populateData(data: data)
        cell.imgVw.tag = indexPath.row
        cell.imgVw.accessibilityIdentifier = "img"
        
        if data.images.count > 0 {
            
            if let img = data.images[0].image {
                if img.contains("pdf") {
                    cell.imgVw.accessibilityIdentifier = "pdf"
                } else {
                    cell.imgVw.accessibilityIdentifier = "img"
                }
            }
        }
        cell.imgVw.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        cell.imgVw.addGestureRecognizer(tap)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        //self.onEditCliked(id: indexPath.row)
        self.viewAction(id: indexPath.row)
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        (cell as! PrescriptionCell).bgView.setDashBorder(borderColor: UIColor.gray)
    }
    
    func onPlaceOrderClicked(id: Int){
        
        let data = prescriptionArr[id]
        //if orderData != nil {
        if flagOrder {
            orderData.doctor_name = data.doctor_name
            //orderData.drug_list = data.getMedicines()
            orderData.prescription_image = data.images[0].image
            delegate?.onSelectPrescription(orderData: orderData, preData: data)
            self.navigationController?.popViewController(animated: false)
        } else {
            
            let orderVC = self.getVC(with: "MedicineOrderSegmentVC", sb: IDMain) as! MedicineOrderSegmentVC
            var orderData = MedicineOrderData()
            orderData.doctor_name = data.doctor_name
            //orderData.drug_list = data.getMedicines()
            orderData.prescription_image = data.images[0].image
            orderVC.orderData = orderData
            orderVC.selPrescription = data
            
            self.navigationController?.pushViewController(orderVC, animated: false)
        }
    }
    
    func onShareCliked(id: Int) {
        
        let data = prescriptionArr[id]
        
        let vc = self.getVC(with: "ShareEHRVC", sb: IDMain) as! ShareEHRVC
        vc.EHR_Id = data.id
        vc.type = "Presciption"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func onEditCliked(id: Int) {
        
        let data = prescriptionArr[id]
        
        let vc = self.getVC(with: "UpdatePrescription", sb: IDMain) as! AddPrescription
        vc.editFlag = true
        vc.prescriptionData = data
        vc.recordType = (type == "self") ? kMine : kFamily
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func viewAction(id: Int) {
        
        let data = prescriptionArr[id]
        
        let vc = self.getVC(with: "UpdatePrescription", sb: IDMain) as! AddPrescription
        vc.viewFlag = true
        vc.prescriptionData = data
        vc.recordType = (type == "self") ? kMine : kFamily
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
}

protocol PrescriptionCellDelegate {
    
    func onPlaceOrderClicked(id: Int)
    func onShareCliked(id: Int)
    func onEditCliked(id: Int)
}
class PrescriptionCell: ChemistCell {
    
    var delegatePresc: PrescriptionCellDelegate?
    
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    var row: Int = 0
    
    func populateData(data: PrescriptionData){
        
        if data.images.count > 0 {
            
            if let img = data.images[0].image {
                if img.contains("pdf") {
                    
                    imgVw.image = UIImage(named: "pdf")
                    
                } else {
                    let imgURL = prescriptionImageURL + img
                    imgVw.kf.setImage(with: URL(string: imgURL))
                }
            }
        }
        self.lblName.text = data.illness_type
        self.lblDesc.text = data.getDrugs()
        self.lblAddress.text = data.illness_date
        //self.lblHour.text = data.getShopTiming()
        
        if let btnShare = self.contentView.viewWithTag(1) as? UIButton {
            btnShare.tag = row
            btnShare.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
        }
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        delegatePresc?.onShareCliked(id: sender.tag)
    }
    
    @IBAction func editAction(_ sender: UIButton) {
        delegatePresc?.onEditCliked(id: row)
    }
    
    func populateMedData(data: MedicalHistoryData){
        
        if data.images.count > 0 {
            
            if let img = data.images[0].image {
                if img.contains("pdf") {
                    
                    imgVw.image = UIImage(named: "pdf")
                    
                } else {
                    let imgURL = reportImageURL + img
                    imgVw.kf.setImage(with: URL(string: imgURL))
                }
            }
        }
        self.lblName.text = data.illness_type
        self.lblDesc.text = "Sharing Status: " + data.getSharingStatus()
        //self.lblAddress.text = data.illness_date
        self.lblHour.text = data.getDate()
        
        if let btnShare = self.contentView.viewWithTag(1) as? UIButton {
            btnShare.tag = row
            btnShare.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
        }
    }
    
    @IBAction override func placeOrderAction(_ sender: UIButton){
        
        //delegatePresc?.onPlaceOrderClicked(id: sender.tag)
        delegatePresc?.onPlaceOrderClicked(id: row)
    }
}
