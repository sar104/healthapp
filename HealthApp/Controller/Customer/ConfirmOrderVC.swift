//
//  ConfirmOrderVC.swift
//  HealthApp
//
//  Created by Apple on 22/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD
import Alamofire
import SwiftyJSON

class ConfirmOrderVC: BaseVC {

    @IBOutlet weak var tblData: UITableView!
    
    var orderData: MedicineOrderData = MedicineOrderData()
    var prescData: PrescriptionData = PrescriptionData()

    var selectedMedArr: [MedicineData] = []
    var selNonPreMedArr: [MedicineData] = []
    var slotArr: [SlotData] = []
    var familyMembersArr: [FamilyMemberData] = []
    var selFamilyMember: FamilyMemberData = FamilyMemberData()
    var recType: String = "Self"
    
    let recTypeArr = ["Self", "Family"]
    let deliveryTypeArr = ["Delivery","Pickup"]
    var user: User?
    var prescriptionImg:Data?
    var orderTypeNP: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let users = self.fetchUser()
        user = users[0]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationItem.hidesBackButton = true
        
        self.getFamilyMembers()
        self.getSlotList()
    }
    
    

    func getSlotList() {
        
        /*
        IHProgressHUD.show()
        
        apiManager.getTimeSlots(params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    self.slotArr.removeAll()
                    self.slotArr = data.filter{ ( !$0.isEarlier()) }
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }*/
        let time1 = "09:00 AM To 12:00 PM"
        let slot1 = SlotData(id: 1, slot: time1)
        let time2 = "01:00 PM To 3:00 PM"
        let slot2 = SlotData(id: 2, slot: time2)
        let time3 = "03:00 PM To 5:00 PM"
        let slot3 = SlotData(id: 3, slot: time3)
        let time4 = "05:00 PM To 7:00 PM"
        let slot4 = SlotData(id: 4, slot: time4)
        
        if !slot1.isEarlierSlot() {
            self.slotArr.append(slot1)
        }
        if !slot2.isEarlierSlot() {
            self.slotArr.append(slot2)
        }
        if !slot3.isEarlierSlot() {
            self.slotArr.append(slot3)
        }
        if !slot4.isEarlierSlot() {
            self.slotArr.append(slot4)
        }
    }
    
    func getFamilyMembers() {
        
        IHProgressHUD.show()
        
        let params = ["user_id": user?.id] as [String: Any]
        apiManager.getFamilyMember(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    self.familyMembersArr.removeAll()
                    self.familyMembersArr = memberData
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    
    
    func addPrescription(){
       
        do {
            
            let params: [String:Any] = [PARAM_USERID: prescData.user_id!,
                                        PARAM_CHILDID:  prescData.child_id!,
                                        PARAM_SHARINGTYPE: prescData.sharing_type!,
                                        PARAM_DOCTORNAME: prescData.doctor_name!,
         PARAM_ILLNESSTYPE: prescData.illness_type ?? "",
         PARAM_DESCRIPTION: prescData.description!,
         PARAM_ILLNESSDATE: prescData.illness_date ?? self.orderData.estimated_date,
         PARAM_IMAGES: [prescData.image]]
         
         
         print("params: \(params)")
            apiManager.addPrecriptionAPI(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
                
            }
          
        } catch let err {
            
        }
    }
    func addOrder() {
        
        IHProgressHUD.show()
        
        var drugArr: [DrugData] = []
        /*
        let drugArrPM = MedicineData.getDrugsData(self.selectedMedArr)
        drugArr.append(contentsOf: drugArrPM)
        let drugArrNPM = MedicineData.getDrugsData(self.selNonPreMedArr)
        drugArr.append(contentsOf: drugArrNPM)
        */
        //self.orderData.drug_list = drugArr
        
        do {
            let arrDrugs = NSMutableArray.init()
            
             if selectedMedArr.count > 0 {
                 
                 for med in selectedMedArr {
                     /*
                     let meddata = [ "medicine_name":med.medicine_name,"suggested_name": "", "medicine_type": med.type,"quantity" :med.quantity,"unit" :med.type_of_medicine!] as [String : Any]
               
                     arrDrugs.add(meddata)*/
                    
                    var medname = MedicineName()
                    medname.medicine_name = med.medicine_name
                    medname.medicine_type = med.type
                    medname.suggested_name = ""
                    
                    let mednameJSONData = try JSONEncoder().encode(medname)
                    let jsonStrMedname = String(data: mednameJSONData, encoding: .utf8)
                    //["medicine_name":med.medicine_name,"suggested_name": "", "medicine_type": med.type]
                    
                    let meddata = ["medicine_name":jsonStrMedname,"quantity" :med.quantity,"unit" :med.type_of_medicine!] as [String : Any]
                    
                    var  drugdata = DrugData()
                    
                    drugdata.medicine_name = medname
                    drugdata.unit = med.type_of_medicine!
                    drugdata.quantity = med.quantity
                    drugArr.append(drugdata)
                    
                    arrDrugs.add(meddata)
                 }
                 print("drugs: \(arrDrugs)")
             }
            if selNonPreMedArr.count > 0 {
                
                for med in selNonPreMedArr {
                    /*
                    let meddata = [ "medicine_name":med.medicine_name,"suggested_name": "", "medicine_type": med.type,"quantity" :med.quantity,"unit" :med.type_of_medicine!] as [String : Any]
               
                    arrDrugs.add(meddata)*/
                    
                    var medname = MedicineName()
                    medname.medicine_name = med.medicine_name
                    medname.medicine_type = med.type
                    medname.suggested_name = ""
                    
                    let mednameJSONData = try JSONEncoder().encode(medname)
                    let jsonStrMedname = String(data: mednameJSONData, encoding: .utf8)
                    //["medicine_name":med.medicine_name,"suggested_name": "", "medicine_type": med.type]
                    
                    let meddata = ["medicine_name":jsonStrMedname,"quantity" :med.quantity,"unit" :med.type_of_medicine!] as [String : Any]
                    arrDrugs.add(meddata)
                    
                    var  drugdata = DrugData()
                    
                    drugdata.medicine_name = medname
                    drugdata.unit = med.type_of_medicine!
                    drugdata.quantity = med.quantity
                    drugArr.append(drugdata)
                }
                print("drugs: \(arrDrugs)")
            }
             
             //let jsonDrugs = JSON(arrDrugs)
             //let strDrugs = jsonDrugs.rawString()!
            /*
            let drugJSonData = try JSONSerialization.data(withJSONObject: arrDrugs, options: [])
            let jsonStrDrugs = String(data: drugJSonData, encoding: .utf8)
            */
            /*
            let jsonDrugs = JSON(arrDrugs)
            let jsonStrDrugs = jsonDrugs.rawString()!
            let paramsData = try JSONEncoder().encode(self.orderData)
            */
            let drugJSonData = try JSONEncoder().encode(drugArr)
            let jsonStrDrugs = String(data: drugJSonData, encoding: .utf8)
            
            var params:[String:String] = [:]
            //guard var params = try? JSONSerialization.jsonObject(with: paramsData, options: []) as? [String:Any]
               // else { return }
            
            params["customer_address"] = self.orderData.customer_address ?? ""
            params["delivery_type"] = self.orderData.delivery_type ?? ""
            params["chemist_id"] = String(self.orderData.chemist_id!)
            params["prescription_type"] = self.orderData.prescription_type ?? ""
            params["slot"] = self.orderData.slot ?? ""
            params["estimated_date"] = self.orderData.estimated_date ?? ""
            params["description"] = self.orderData.description
            params["doctor_name"] = self.orderData.doctor_name ?? "-"
            params["address_id"] = String(self.orderData.address_id)
            params["pincode"] = self.orderData.pincode ?? ""
            params["prescription_image"] = self.orderData.prescription_image ?? "NA"
            params["user_id"] = String(self.orderData.user_id!)
            params["flat_house_no"] = self.orderData.flat_house_no
            
            params["drug_list"] = jsonStrDrugs
            
            let dataRec = try JSONEncoder().encode(self.orderData.recipient_name)
            let paramsRec =  String(data: dataRec, encoding: .utf8)
                //try? JSONSerialization.jsonObject(with: dataRec, options: []) as? [String:Any]
            
            params["recipient_name"] = paramsRec
            print("params: \(params)")
            
            
            apiManager.AddOrderForCustomer(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        //self.alert(strTitle: strSuccess, strMsg: data.message!)
                        if data.status {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                self.OrderCompleted()
                            }
                        }
                        
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
            /*
             ["address_id": "0", "prescription_image": "NA", "flat_house_no": "410", "delivery_type": "Delivery", "user_id": "401", "prescription_type": "Non Prescribed", "description": "null", "customer_address": "Nasik", "pincode": "422101", "drug_list": "[{\"medicine_name\":\"{\\\"medicine_type\\\":\\\"NPM\\\",\\\"medicine_name\\\":\\\"Paracetamol O Tablet\\\",\\\"suggested_name\\\":\\\"\\\"}\",\"quantity\":1,\"unit\":\"bottle\"},{\"medicine_name\":\"{\\\"medicine_type\\\":\\\"NPM\\\",\\\"medicine_name\\\":\\\"Vitamin C Injection\\\",\\\"suggested_name\\\":\\\"\\\"}\",\"unit\":\"vial\",\"quantity\":1}]", "estimated_date": "02-10-2020", "doctor_name": "-", "recipient_name": "{\"rec_mobile\":\"9850059684\",\"rec_id\":401,\"rec_name\":\"Miss Sarika\"}", "chemist_id": "281", "slot": "6 to 9"]
             
             ["description": "null", "estimated_date": "02-10-2020", "slot": "6 to 9", "customer_address": "Nasik", "flat_house_no": "410", "recipient_name": "{\"rec_mobile\":\"9850059684\",\"rec_id\":401,\"rec_name\":\"Miss Sarika\"}", "delivery_type": "Delivery", "order_id": "457", "chemist_id": "281", "pincode": "422101", "address_id": "0", "prescription_image": "null", "user_id": "401", "drug_list": "[\n  {\n    \"quantity\" : 1,\n    \"medicine_name\" : \"{\\\"medicine_type\\\":\\\"NPM\\\",\\\"medicine_name\\\":\\\"Nurovita G 300 mg\\\\\\/500 mcg Tablet\\\",\\\"suggested_name\\\":\\\"\\\"}\",\n    \"unit\" : \"strip\"\n  }\n]", "doctor_name": "null", "prescription_type": "Non Prescribed"]
             **/
           /*
            let qryStr: NSMutableString = ""
             
            for (key, value) in params {
                qryStr.append(key)
                qryStr.append("=")
                if let str = value as? Int {
                    qryStr.append(String(str))
                } else {
                    qryStr.append(value )
                }
                qryStr.append("&")
            }
            guard let encodedStr = (qryStr as String).stringByAddingPercentEncodingForFormData() else { return  }
            //let strURL = (qryStr as String).buildURL(api: AddOrderForCustomerAPI, queryParams: params)
            let strURL = baseURL + AddOrderForCustomerAPI + "?" + encodedStr
            //let url = URL(string: strURL)!
            let request = NSMutableURLRequest(url: URL(string: strURL)!)
            request.httpMethod = "POST"
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                
                IHProgressHUD.dismiss()
                
              if (error != nil) {
                print(error)
              } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
                
                guard let data = data else {
                    //completion(.failure(WebServiceError.noData))
                    DispatchQueue.main.async {
                        self.alert(strTitle: strErr, strMsg: WebServiceError.noData.localizedDescription)
                    }
                    
                    return
                }
                if let error = error {
                    DispatchQueue.main.async {
                        self.alert(strTitle: strErr, strMsg: error.localizedDescription)
                    }
                    return
                }
                //completion(.success(data))
                do {
                    let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                    print("success: \(json)")
                    DispatchQueue.main.async {
                        self.alert(strTitle: strSuccess, strMsg: json.message!)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.OrderCompleted()
                    }
                } catch let err {
                    DispatchQueue.main.async {
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                    }
                }
              }
            })

            dataTask.resume()
 */
            /*
            AF.request(url, method: .post, parameters: [:], encoding: URLEncoding.default).responseJSON { (response) in
                
                print(response)
                print(response.result)
                guard let data = response.data else {
                    //completion(.failure(WebServiceError.noData))
                    self.alert(strTitle: strErr, strMsg: WebServiceError.noData.localizedDescription)
                    return
                }
                if let error = response.error {
                    self.alert(strTitle: strErr, strMsg: error.localizedDescription)
                    return
                }
                //completion(.success(data))
                do {
                    let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                    self.alert(strTitle: strSuccess, strMsg: json.message!)
                } catch let err {
                    self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
                 
            }*/
        } catch let err {
            print(err)
        }
        
        
    }
    
    @objc func OrderCompleted(){
        
        let vc = self.getVC(with: "OrderSuccessVC", sb: IDMain) as! OrderSuccessVC
        vc.chemistName = prescData.chemist?.name ?? ""
        self.navigationController?.pushViewController(vc, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ConfirmOrderVC: UITableViewDelegate, UITableViewDataSource, UIStepperControllerDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 4
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
            switch section {
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "labelCell") as! PrescribedCell
                cell.contentView.backgroundColor = .white
                cell.lblHead.text = "PRESCRIBED DRUGS"
                cell.textLabel?.textColor = .black
                return cell.contentView
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "labelCell")  as! PrescribedCell
                cell.contentView.backgroundColor = .white
                cell.lblHead.text = "NON PRESCRIBED ITEMS"
                cell.textLabel?.textColor = .black
                return cell.contentView
            default:
                break
            }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if orderTypeNP {
            
            if section == 1 {
                return 0
            }
            if section == 2 {
                return 40
            }
        }
        else if section == 1 || section == 2 {
            
            return 40 
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
            switch section {
            case 0:
                return 3
            case 1:
                return selectedMedArr.count
            case 2:
                return selNonPreMedArr.count
            case 3:
                return 8
            default:
                break
            }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = PrescribedCell()
        
            switch indexPath.section {
            case 0:
                
                switch indexPath.row {
                    
                case 0:
                    cell = tableView.dequeueReusableCell(withIdentifier: "labelCell") as! PrescribedCell
                    cell.lblHead.text = "Order Type"
                    cell.lblMedName.text = self.orderData.prescription_type
                    
                case 1:
                    cell = tableView.dequeueReusableCell(withIdentifier: "labelCell") as! PrescribedCell
                    cell.lblHead.text = "Doctor Name"
                    cell.lblMedName.text = self.orderData.doctor_name
                    
                case 2:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! ImageCell
                    if let imgData = prescriptionImg {
                        cell.imgview.image = UIImage(data: imgData)
                    } else if var url = self.orderData.prescription_image, !url.isEmpty {
                        url = prescriptionImageURL + url
                        cell.imgview.kf.setImage(with: URL(string: url))
                    }
                    
                    return cell
                default:
                    break
                }
            case 1:
                cell = tableView.dequeueReusableCell(withIdentifier: "medListCell") as! PrescribedCell
                cell.delegate = self
                if selectedMedArr.count > 0 {
                    let medicine = selectedMedArr[indexPath.row]
                    cell.lblMedName.text = medicine.medicine_name
                    cell.stepper.tag = indexPath.row
                    cell.stepper.count = CGFloat(medicine.quantity)
                    cell.stepper.delegate = self
                }
            case 2:
               
                cell = tableView.dequeueReusableCell(withIdentifier: "medListCell") as! PrescribedCell
                    cell.delegate = self
                    if selNonPreMedArr.count > 0 {
                        let medicine = selNonPreMedArr[indexPath.row]
                        cell.lblMedName.text = medicine.medicine_name
                        cell.stepper.tag = indexPath.row
                        cell.stepper.count = CGFloat(medicine.quantity)
                        cell.stepper.delegate = self
                    }
                
            case 3:
                switch indexPath.row {
                case 0:
                    cell = tableView.dequeueReusableCell(withIdentifier: "typeCell") as! PrescribedCell
                    cell.setup(id: "type", placeHolder: "")
                    cell.delegate = self
                    
                case 1:
                    cell = tableView.dequeueReusableCell(withIdentifier: "radioCell") as! PrescribedCell
                    cell.setup(id: "radio", placeHolder: "")
                    
                    cell.delegate = self
                case 2:
                    if self.recType == "Self" {
                        cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                        cell.setup(id: "recipient_self", placeHolder: "")
                        cell.bottoTxtFld.text = self.orderData.recipient_name?.rec_name ?? ""
                    } else {
                        cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! PrescribedCell
                        cell.pickerDataArr = self.familyMembersArr
                        cell.setup(id: "recipient_family", placeHolder: "")
                    }
                    
                    cell.delegate = self
                case 3:
                    cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                    cell.setup(id: "flat", placeHolder: "FLAT/HOUSE NUMBER")
                    cell.bottoTxtFld.text = self.orderData.flat_house_no ?? ""
                    cell.delegate = self
                case 4:
                    cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                    cell.setup(id: "address", placeHolder: "DELIVERY ADDRESS")
                    cell.bottoTxtFld.text = self.orderData.customer_address ?? ""
                    cell.delegate = self
                case 5:
                    cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! PrescribedCell
                    cell.setup(id: "pincode", placeHolder: "PINCODE")
                    cell.bottoTxtFld.text = self.orderData.pincode ?? ""
                    
                    cell.delegate = self
                case 6:
                    cell = tableView.dequeueReusableCell(withIdentifier: "dateTimeCell") as! PrescribedCell
                    cell.pickerDataArr = self.slotArr
                    cell.setup(id: "date", placeHolder: "")
                    
                    cell.delegate = self
                    
                case 7:
                    cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! PrescribedCell
                    cell.delegate = self
                    
                default:
                    break
                }
            default:
                break
            }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            
            if orderTypeNP && (indexPath.row == 1 || indexPath.row == 2 ){
                return 0
            }
            if indexPath.row == 2 {
                
                return  160
            }
        }
        if indexPath.section == 3 {
            
            if indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5 {
                if self.orderData.delivery_type == "Delivery" {
                    return 90
                } else {
                    return 0
                }
                
            }
        }
        return 90
    }
    
    func stepperDidAddValues(stepper: UIStepperController) {
    
         print("Stepper value did change (Add) : \(stepper.count)")
        
    }

    func stepperDidSubtractValues(stepper: UIStepperController) {
    
         print("Stepper value did change (Subtract) \(stepper.count)")
        
    }
    
}
extension ConfirmOrderVC: PrescribedCellDelegate {
    
    func validate() ->  Bool {
        
        if !orderTypeNP && (self.orderData.doctor_name == nil ||  self.orderData.doctor_name!.isEmpty) {
            self.alert(strTitle: strErr, strMsg: "Please enter doctor name")
            return false
        }
        
        if self.orderData.recipient_name?.rec_name == nil ||  (self.orderData.recipient_name?.rec_name ?? "" ).isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please enter recipient name")
            return false
        }
        if let delivery_type = self.orderData.delivery_type, delivery_type == "Delivery" {
            
            if self.orderData.flat_house_no == nil ||  self.orderData.flat_house_no!.isEmpty {
                self.alert(strTitle: strErr, strMsg: "Please enter flat/house number")
                return false
            }
            if self.orderData.customer_address == nil ||  self.orderData.customer_address!.isEmpty {
                self.alert(strTitle: strErr, strMsg: "Please enter delivery address")
                return false
            }
            if self.orderData.pincode == nil || self.orderData.pincode!.isEmpty {
                self.alert(strTitle: strErr, strMsg: "Please enter pincode")
                return false
            }
        }
        if self.orderData.estimated_date == nil || self.orderData.estimated_date!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please select estimated date")
            return false
        }
        if self.orderData.slot == nil || self.orderData.slot!.isEmpty {
            self.alert(strTitle: strErr, strMsg: "Please select time slot")
            return false
        }
        
        return true
    }
    
    func onQtyChanged(id: String, index: Int, value: Int) {
        
        switch id {
        case "PM":
            self.selectedMedArr[index].quantity = value
            
        case "NPM":
            self.selNonPreMedArr[index].quantity = value
            
        default:
            break
        }
    }
    
    func onValueSelected(type: String, value: String) {
        
        switch type {
        case "date":
            self.orderData.estimated_date = value
            
        case "deliveryYype":
            let index = Int(value)
            self.orderData.delivery_type = deliveryTypeArr[index!]
            self.tblData.reloadData()
            
        case "radio":
            let index = Int(value)
            recType = recTypeArr[index!]
            self.tblData.reloadData()
            
        case "recipient":
            self.orderData.recipient_name = RecipientData(rec_id: Int(user!.id), rec_name: value, rec_mobile: user?.mobile_no)
            
        case "doctor":
            self.orderData.doctor_name = value
            
        case "flat":
            self.orderData.flat_house_no = value
            
        case "address":
            self.orderData.customer_address = value
            
        case "pincode":
            self.orderData.pincode = value
            
           
        default:
            break
        }
    }
    
    func onDataSelected(type: String, value: Any) {
        
        switch type {
        case "chemist":
            let data = value as! ChemistData
            self.orderData.chemist_id = data.id
            
        case "time":
            let data = value as! SlotData
            self.orderData.slot = data.slot
            
        case "recipient_family":
            self.selFamilyMember = value as! FamilyMemberData
            
        default:
            break
        }
    }
    
    func onSaveClicked() {
        
        if validate() {
            self.alertAction(strTitle: "Alert", strMsg: "Do you want to add prescription in EHR?") { (action) in
                
                print("action: \(action)")
                
                self.addPrescription()
            }
            self.addOrder()
        }
    }
    
    func onUploadClicked(type: Int) {
        
    }
    
    func onAddClicked() {
        
    }
    
    func onImgRemoved() {
        
    }
    
    func onMedClicked(type: Int) {
        
    }
    
}
class ImageCell: BaseCell {
    
    @IBOutlet weak var imgview: UIImageView!
}
