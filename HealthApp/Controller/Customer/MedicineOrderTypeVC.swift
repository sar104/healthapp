//
//  MedicineOrderTypeVC.swift
//  HealthApp
//
//  Created by Apple on 17/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class MedicineOrderTypeVC: BaseVC {

    var selChemistData: ChemistData?
    var orderData: MedicineOrderData = MedicineOrderData()
    var selPrescription: PrescriptionData = PrescriptionData()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.

        if segue.identifier == "seguePrescribed" {
            
            if let vc = segue.destination as? PrescribedVC {
                
                vc.selChemistData = selChemistData
                vc.orderData = orderData
                vc.selPrescription = selPrescription
                
            }
        }
    }
    

}
