//
//  AddPrescription.swift
//  HealthApp
//
//  Created by Apple on 07/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit
import IHProgressHUD
import SwiftyJSON
import Alamofire
import MobileCoreServices

class AddPrescription: BaseVC, AddPrescriptionCellDelegate, UITextFieldDelegate, SwiftAlertViewDelegate {

    var recommBy: String = "Myself"
    var sharingType: String = "Private"
    var dateOfReport: String = ""
    var prescriptionImg:Data?
    var allMedicineArr: [MedicineData] = []
    var medicineArr: [MedicineData] = []
    var selectedMedArr: [MedicineData] = []
    var medObj = MedicineData()
    var addMedicineAlertView: SwiftAlertView!
    var prescImgArr:[Data] = []
    var imageNameArr:[String] = []
    var recordType: String = kMine
    var familyMembersArr: [FamilyMemberData] = []
    var selFamilyMember: FamilyMemberData?
    var purposeVisitArr: [IllnessTypeData] = []
    var user: User?
    var doctorName: String = ""
    var selIllnessType: IllnessTypeData?
    var prescriptionData: PrescriptionData?
    var editFlag: Bool = false
    var viewFlag: Bool = false
    var pdfFlag: Bool = false
    var pdfData: Data?
    
    let imgPicker = UIImagePickerController()
    @IBOutlet weak var tblData: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("recordType = \(recordType)")
        self.tblData.estimatedRowHeight = 90

        //coreDataStack = CoreDataStack(model: "HealthApp")
        //coreDataService = CoreDataService(stack: coreDataStack)
        
        let medArr = CoreDataManager.shared.fetchAllMedicines()
        
        for data in medArr {
            
            allMedicineArr.append(MedicineData(coreDataMed: data))
            
        }
        
        let users = self.fetchUser()
        user = users[0]
        // Do any additional setup after loading the view.
        getIllnessTypes()
        
        //self.medicineArr = self.getMedicineArr()
        //getMedicines(name: "para")
        /*
        if coreDataService?.getMedCount() == 0 {
            //self.saveMedicines()
        }
        
        getMedicines()*/
        
        
        if self.recordType == kFamily {
            //getFamilyMembers()
        }
        
        if editFlag || viewFlag {
            populatePrescData()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(segmentChanged(_:)), name: NSNotification.Name("SegmentDidChangeNotification"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tblData.reloadData()
    }
    
    func populatePrescData() {
        
        let arrmember = familyMembersArr.filter{ $0.id == prescriptionData?.child_id }
        if arrmember.count > 0 {
            selFamilyMember = arrmember[0]
            recordType = kFamily
        }
        let purposeArr = purposeVisitArr.filter{ $0.illness_type == prescriptionData?.illness_type }
        //
        if purposeArr.count > 0 {
            selIllnessType = purposeArr[0]
        }
        dateOfReport = prescriptionData!.illness_date!
        doctorName = prescriptionData!.doctor_name!
        selectedMedArr = prescriptionData!.getDrugsArr()
        sharingType =  prescriptionData!.sharing_type == 0 ? "Private" : "Family"
        if let images = prescriptionData?.images, images.count > 0 {
            self.imageNameArr.append(images[0].image!)
        }
        self.tblData.reloadData()
    }
    
    @objc func segmentChanged(_ notification: NSNotification){
        
        if let index = notification.object as? Int {
            print("index = \(index)")
            self.recordType = self.arrRecordType[index]
            
        }
        if self.recordType == kFamily {
            if familyMembersArr.count == 0 {
                getFamilyMembers()
            }
        }
        self.tblData.reloadData()
    }
    /*
    func getMedicines() {
        
        let arrMed = coreDataService?.fetchAllMedicines()
        self.medicineArr = MedicineData.getMedicineData(arrMed!)
    }*/
    
    func getIllnessTypes() {
        
        IHProgressHUD.show()
        
        
        apiManager.getIllnessType(params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    
                    self.purposeVisitArr = memberData
                    if self.editFlag {
                        let purposeArr = self.purposeVisitArr.filter{ $0.illness_type == self.prescriptionData?.illness_type }
                        //
                        if purposeArr.count > 0 {
                            self.selIllnessType = purposeArr[0]
                        }
                    }
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    func getFamilyMembers() {
        
        IHProgressHUD.show()
        
        let params = ["user_id": user?.id] as [String: Any]
        apiManager.getFamilyMember(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    
                    self.familyMembersArr = memberData
                    if self.editFlag {
                        let arrmember = self.familyMembersArr.filter{ $0.id == self.prescriptionData?.child_id }
                        if arrmember.count > 0 {
                            self.selFamilyMember = arrmember[0]
                            self.recordType = kFamily
                        }
                    }
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func uploadPrescription(){
        
        IHProgressHUD.show()
        apiManager.uploadImg(image: prescriptionImg!, name: "image", api: PrescriptionImgAPI) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let imgdata):
                    
                    let imgstr = String(data: imgdata, encoding: .utf8)
                    print(imgstr)
                        
                        if((imgstr?.contains("jpeg"))!){
                            
                            //self.addPrescription()
                            if self.editFlag {
                                self.imageNameArr.removeAll()
                                self.imageNameArr.append(imgstr!)
                               self.updatePrescription()
                           } else {
                                self.imageNameArr.append(imgstr!)
                               self.addPrescription()
                           }
                        }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func uploadPDFPrescription(){
        
        IHProgressHUD.show()
        apiManager.uploadPDF(image: pdfData!, name: "image", api: PrescriptionImgAPI) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let imgdata):
                    
                    let imgstr = String(data: imgdata, encoding: .utf8)
                    print(imgstr)
                        
                        if((imgstr?.contains("pdf"))!){
                            
                            //self.addPrescription()
                            if self.editFlag {
                                self.imageNameArr.removeAll()
                                self.imageNameArr.append(imgstr!)
                               self.updatePrescription()
                           } else {
                                self.imageNameArr.append(imgstr!)
                               self.addPrescription()
                           }
                        }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func addPrescription(){
       
        let arrImgs = NSMutableArray.init()
        arrImgs.addObjects(from: imageNameArr)
        
        let arrDrugs = NSMutableArray.init()
       
        if selectedMedArr.count > 0 {
            
            for data in selectedMedArr {
                
                let meddata = ["medicine_name": data.medicine_name!,"quantity" :data.quantity,"unit" :data.type_of_medicine!] as [String : Any]
                arrDrugs.add(meddata)
            }
            print("drugs: \(arrDrugs)")
        }
        
        let jsonDrugs = JSON(arrDrugs)
        let strDrugs = jsonDrugs.rawString()!
        print("strDrugs: \(strDrugs)")
        
        let sharing = SHARINGTYPE_ARRAY.firstIndex(of: sharingType)
        
        let userid = user!.id
        var childId = userid
        if self.recordType == kFamily {
            childId = Int32(Int(selFamilyMember!.id!))
        }
        
        do {
            
         let params: [String:Any] = [PARAM_USERID: userid,
                                       PARAM_CHILDID:  childId,
             PARAM_SHARINGTYPE: sharing!,
         PARAM_DOCTORNAME: doctorName,
         PARAM_ILLNESSTYPE: selIllnessType!.illness_type!,
         PARAM_DESCRIPTION: strDrugs,
         PARAM_ILLNESSDATE: dateOfReport,
         PARAM_IMAGES: arrImgs]
         
         
         print("params: \(params)")
            apiManager.addPrecriptionAPI(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.navPrescList()
                        }
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
            
        } catch let err {
            
        }
    }
    func updatePrescription(){
       
        IHProgressHUD.show()
        
        let arrImgs = NSMutableArray.init()
        arrImgs.addObjects(from: imageNameArr)
        
        let arrDrugs = NSMutableArray.init()
       
        if selectedMedArr.count > 0 {
            
            for data in selectedMedArr {
                
                let meddata = ["medicine_name": data.medicine_name!,"quantity" :data.quantity,"unit" :data.type_of_medicine!] as [String : Any]
                arrDrugs.add(meddata)
            }
            print("drugs: \(arrDrugs)")
        }
        
        let jsonDrugs = JSON(arrDrugs)
        let strDrugs = jsonDrugs.rawString()!
        print("strDrugs: \(strDrugs)")
        
        let sharing = SHARINGTYPE_ARRAY.firstIndex(of: sharingType)
        
        let userid = user!.id
        var childId = userid
        if self.recordType == kFamily {
            childId = Int32(Int(selFamilyMember!.id!))
        }
        
        do {
            let id: Int = prescriptionData!.id!
            let params: [String:Any] = [PARAM_ID: id,
            PARAM_USERID: userid,
            PARAM_CHILDID:  childId,
            PARAM_SHARINGTYPE: sharing!,
            PARAM_DOCTORNAME: doctorName,
            PARAM_ILLNESSTYPE: selIllnessType!.illness_type!,
            PARAM_DESCRIPTION: strDrugs,
            PARAM_ILLNESSDATE: dateOfReport,
            PARAM_IMAGES: arrImgs]
         
         
         print("params: \(params)")
            apiManager.updatePrecription(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.navPrescList()
                        }
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
            
        } catch let err {
            
        }
    }
    
    func navPrescList() {
        
        var dashboardVC: DashboardVC
        if let vc = (self.tabBarController?.viewControllers?.first as? UINavigationController)?.viewControllers.first as? DashboardVC {
            
            dashboardVC = vc
            
        } else {
            
            dashboardVC = self.getVC(with: "DashboardVC", sb: "Main") as! DashboardVC
        }
        
        let prescVC = self.getVC(with: "PrescriptionListVC", sb: "Main") as! PrescriptionListVC
        
        if self.recordType == kFamily {
            prescVC.type = "family"
        } else {
            prescVC.type = "self"
        }
        //self.navigationController?.pushViewController(prescVC, animated: false)
        let vcArray = [dashboardVC, prescVC]
        self.navigationController?.setViewControllers(vcArray, animated: false)
    }
    /*
    func getMedicines(name: String) {
        
        IHProgressHUD.show()
        
        let params = ["medicine":name]
        apiManager.getMedicines(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let medData):
                    
                    self.medicineArr = medData
                    
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }*/
    
    func onValueSelected(type: String, value: String) {
        
        switch type {
        case "recommBy":
            recommBy = value
        case "sharingType":
            sharingType = value
        case "date":
            dateOfReport = value
        case "drname":
            doctorName = value
            
        default:
            break
        }
        self.tblData.reloadData()
    }
    
    func onDataSelected(type: String, value: Any) {
        
        if let data = value as? FamilyMemberData {
            
            selFamilyMember = data
            self.tblData.reloadData()
        }
        if let data = value as? IllnessTypeData {
            selIllnessType = data
        }
    }
    
    func validate() -> Bool{
        
        
        if self.recordType == kFamily && selFamilyMember == nil {
            
            self.alert(strTitle: strErr, strMsg: "Please select family member")
            return false
        }
        if selIllnessType == nil {
            
            self.alert(strTitle: strErr, strMsg: "Please select pupose of visit")
            return false
        }
        if dateOfReport.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select date")
            return false
        }
        if doctorName.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter doctor name")
            return false
        }
        /*
        if selectedMedArr.count == 0{
            self.alert(strTitle: strErr, strMsg: "Please add medicines")
            return false
        }*/
        if prescriptionImg == nil && prescriptionData == nil && pdfData == nil {
            self.alert(strTitle: strErr, strMsg: "Please upload prescription")
            return false
        }
        
        return true
    }
    
    func onSaveClicked(){
        
        //self.testAPI()
        if viewFlag {
            
            let orderVC = self.getVC(with: "MedicineOrderSegmentVC", sb: IDMain) as! MedicineOrderSegmentVC
            var orderData = MedicineOrderData()
            orderData.doctor_name = prescriptionData?.doctor_name
            //orderData.drug_list = data.getMedicines()
            orderData.prescription_image = prescriptionData?.images[0].image
            orderVC.orderData = orderData
            orderVC.selPrescription = prescriptionData!
            
            self.navigationController?.pushViewController(orderVC, animated: false)
            
        } else {
            if validate() {
                if let img = prescriptionImg {
                    self.uploadPrescription()
                }
                else if let pdf = pdfData {
                    self.uploadPDFPrescription()
                }
                else {
                    // call add prescription api
                    if editFlag {
                        self.updatePrescription()
                    } else {
                        self.addPrescription()
                    }
                }
            }
        }
    }
    
    func onAddClicked(){
        
        self.showImgPicker()
    }
    
    func onAddMedClicked(){
        
        initAlert()
        addMedicineAlertView.show()
    }
    
    func onImgRemoved(){
        self.prescriptionImg = nil
        if prescriptionData != nil {
            prescriptionData?.images = []
        }
        self.tblData.reloadData()
    }
   
    func onQtyChanged(index: Int, value: Int){
        
        if value == 0 {
            self.selectedMedArr.remove(at: index)
        } else {
            var medObj = self.selectedMedArr[index]
            medObj.quantity = value
        }
        
        self.tblData.reloadSections([1], with: .none)
    }

    func initAlert() {
    
        addMedicineAlertView = SwiftAlertView(nibName: "AddMedicineAlertView", delegate: self, cancelButtonTitle: nil, otherButtonTitles: nil)
        addMedicineAlertView.dismissOnOtherButtonClicked = true
        addMedicineAlertView.dismissOnOutsideClicked = true
        addMedicineAlertView.tag = 1
        
        let txtMedName = addMedicineAlertView.viewWithTag(2) as! SearchTextField
        let txtType = addMedicineAlertView.viewWithTag(3) as! RightViewTextField
        let stepper = addMedicineAlertView.viewWithTag(4) as! UIStepperController
        stepper.delegate = self
        
        txtMedName.addBottomBorder()
        txtType.addBottomBorder()
        
        txtMedName.userStoppedTypingHandler = {
            if let criteria = txtMedName.text {
                if criteria.count > 2 {

                    // Show the loading indicator
                    txtMedName.showLoadingIndicator()
                    self.medicineArr = self.allMedicineArr.filter({ (data) -> Bool in
                        return data.medicine_name!.range(of: criteria) != nil
                    })
                    let medNames:[String] = self.medicineArr.map( {($0.medicine_name ?? "")} )
                    if medNames.count > 0 {
                        txtMedName.filterStrings(medNames)
                    } else {
                        self.medObj.medicine_name = criteria
                        self.medObj.type_of_medicine = "other"
                        txtType.text = "other"
                    }
                    txtMedName.stopLoadingIndicator()
                    
                    
                }
            }
        }
        //let medNames:[String] = self.medicineArr.map( {($0.medicine_name ?? "")} )
        //txtMedName.filterStrings(medNames)//(["asdds","dsssfd","weew","bvmbvb"])
        txtMedName.itemSelectionHandler = {filteredResults, itemPosition in
            
            let item = filteredResults[itemPosition]
            txtMedName.text = item.title
            let meddata = self.medicineArr.filter{ $0.medicine_name == item.title}.first
            if meddata != nil {
                let type = meddata?.type_of_medicine
                txtType.text = type
                
                self.medObj = meddata!
                
            } else {
                self.medObj.medicine_name = item.title
                self.medObj.type_of_medicine = "other"
            }
        }
        
        txtMedName.delegate = self
        txtType.delegate = self
        let qtyBtn = addMedicineAlertView.viewWithTag(4) as! UIStepperController
        let addBtn = addMedicineAlertView.viewWithTag(5) as! UIButton
        let closeBtn = addMedicineAlertView.viewWithTag(6) as! UIButton
        closeBtn.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        addBtn.addTarget(self, action: #selector(addMedicineAction), for: .touchUpInside)
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
     
        addMedicineAlertView.dismiss()
    }
    
    @objc func addMedicineAction(){
        
        if medObj.medicine_name == nil {
            
            self.alert(strTitle: strErr, strMsg: "Please select/enter medicine name ")
            
        } else {
            self.selectedMedArr.append(medObj)
            print("qty: \(medObj.quantity)")
            
            addMedicineAlertView.dismiss()
            self.tblData.reloadData()
        }
        
    }
    
    @IBAction func editAction(_ sender: UIButton) {
        
        let vc = self.getVC(with: "UpdatePrescription", sb: IDMain) as! AddPrescription
        vc.editFlag = true
        vc.prescriptionData = prescriptionData
        vc.recordType = recordType
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        
        let vc = self.getVC(with: "ShareEHRVC", sb: IDMain) as! ShareEHRVC
        vc.EHR_Id = prescriptionData?.id
        vc.type = "Presciption"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddPrescription: UIStepperControllerDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 2 {
           // self.getMedicines(name: textField.text ?? "")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.accessibilityIdentifier == "drName" {
            
            self.doctorName = textField.text ?? ""
        }
    }
    
    func stepperDidAddValues(stepper: UIStepperController) {
    
         print("Stepper value did change (Add) : \(stepper.count)")
        self.medObj.quantity = Int(stepper.count)
    }

    func stepperDidSubtractValues(stepper: UIStepperController) {
    
         print("Stepper value did change (Subtract) \(stepper.count)")
        self.medObj.quantity = Int(stepper.count)
         
    }
}

extension AddPrescription: UIDocumentPickerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        self.pdfFlag = true
        do {
            self.pdfData = try Data(contentsOf: myURL)
        } catch let err {
            print("pdf err: \(err.localizedDescription)")
        }
        print("import result : \(myURL)")
        self.tblData.reloadData()
    }


    public func documentMenu(_ documentMenu:UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }


    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}
extension AddPrescription: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 5
        case 1:
            return selectedMedArr.count
        case 2:
            return 3
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = AddPrescriptionCell()
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                if viewFlag {
                    //btnDetailCell
                    cell = tableView.dequeueReusableCell(withIdentifier: "btnDetailCell") as! AddPrescriptionCell
                    
                    if let lblhead = cell.contentView.viewWithTag(1) as? UILabel {
                     
                        lblhead.text = "Purpose of Visit"
                    }
                    if let lblname = cell.contentView.viewWithTag(2) as? UILabel {
                        lblname.text = prescriptionData?.illness_type ?? ""
                    }
                    if let btnEdit = cell.contentView.viewWithTag(3) as? UIButton {
                        btnEdit.addTarget(self, action: #selector(editAction), for: .touchUpInside)
                    }
                    if let btnShare = cell.contentView.viewWithTag(4) as? UIButton {
                        btnShare.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
                    }
                    
                } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! AddPrescriptionCell
                cell.delegate = self
                cell.setup(id: "purpose", placeHolder: "Purpose of Visit")
                cell.pickerDataArr = self.purposeVisitArr
                //if editFlag {
                cell.txtFld.text = selIllnessType?.illness_type ??  prescriptionData?.illness_type ?? ""
                //}
            }
                
            case 1:
                if viewFlag {
                    //btnDetailCell
                    cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! AddPrescriptionCell
                    
                    if let lblhead = cell.contentView.viewWithTag(1) as? UILabel {
                        
                        lblhead.text = "Prescription Date"
                    }
                    if let lblname = cell.contentView.viewWithTag(2) as? UILabel {
                        lblname.text = dateOfReport
                    }
                    
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "dateCell") as! AddPrescriptionCell
                    cell.delegate = self
                    cell.setup(id: "date", placeHolder: "Prescription Date")
                    //if editFlag {
                    cell.txtFld.text = dateOfReport//prescriptionData!.illness_date!
                    //}
                }
                
            case 2:
                cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! AddPrescriptionCell
                cell.delegate = self
                if self.familyMembersArr.count > 0 {
                    cell.setup(id: "familyMember", placeHolder: "Family Member")
                    cell.pickerDataArr = self.familyMembersArr
                }
                
            case 3:
                cell = tableView.dequeueReusableCell(withIdentifier: "labelCell") as! AddPrescriptionCell
                cell.delegate = self
                cell.setup(id: "relation", placeHolder: "")
                cell.lblRelation.text = self.selFamilyMember?.relation ?? ""
                
            case 4:
                if viewFlag {
                    //btnDetailCell
                    cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! AddPrescriptionCell
                    
                    if let lblhead = cell.contentView.viewWithTag(1) as? UILabel {
                        
                        lblhead.text = "Doctor Name"
                    }
                    if let lblname = cell.contentView.viewWithTag(2) as? UILabel {
                        lblname.text = prescriptionData?.doctor_name
                    }
                    
                } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "docCell") as! AddPrescriptionCell
                cell.delegate = self
                cell.setup(id: "drname", placeHolder: "Doctor Name")
                cell.txtFld.delegate = self
                cell.txtFld.text = self.doctorName
                }
                
            default:
                break
            }
        }
        if indexPath.section == 1 {
            
            if viewFlag {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "drugCell") as! AddPrescriptionCell
                if selectedMedArr.count > 0 {
                    let medicine = selectedMedArr[indexPath.row]
                    cell.lblHead.text = medicine.medicine_name
                    cell.lblMedName.text = String(medicine.quantity)
                }
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "medListCell") as! AddPrescriptionCell
                if selectedMedArr.count > 0 {
                    let medicine = selectedMedArr[indexPath.row]
                    cell.lblMedName.text = medicine.medicine_name
                    cell.stepper.tag = indexPath.row
                    cell.stepper.count = CGFloat(medicine.quantity)
                    cell.stepper.delegate = self
                }
                cell.delegate = self
            }
        }
        if indexPath.section == 2 {
            
            switch indexPath.row {
                
                
            case 0:
                cell = tableView.dequeueReusableCell(withIdentifier: "addMedCell") as! AddPrescriptionCell
                cell.delegate = self
                
                
            case 1:
                if self.prescriptionImg != nil {
                    cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! AddPrescriptionCell
                    
                    cell.prescriptionImg = self.prescriptionImg
                    cell.viewFlag = self.viewFlag
                    cell.collView.reloadData()
                }
                else if self.pdfData != nil {
                    cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! AddPrescriptionCell
                    
                    //cell.prescriptionImg = self.prescriptionImg
                    cell.pdfImg = self.pdfData
                    cell.viewFlag = self.viewFlag
                    cell.collView.reloadData()
                }
                else if let prescData =  prescriptionData, !prescData.getImage().isEmpty {
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell") as! AddPrescriptionCell
                    
                    cell.presImgUrl = prescData.getImage()
                    cell.viewFlag = self.viewFlag
                    cell.collView.reloadData()
                    
                } else {
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "upCell") as! AddPrescriptionCell
                }
                
                cell.delegate = self
                
            case 2:
                cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! AddPrescriptionCell
                cell.delegate = self
                if viewFlag {
                    cell.btnAdd.setTitle("PLACE ORDER", for: .normal)
                } else if editFlag {
                    cell.btnAdd.setTitle("UPDATE PRESCRIPTION", for: .normal)
                } else {
                    cell.btnAdd.setTitle("ADD PRESCRIPTION", for: .normal)
                }
            default:
                break
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            
            if indexPath.row == 2 || indexPath.row == 3 {
                if (recordType == kMine) || viewFlag {
                    return 0
                }
            }
        }
        if indexPath.section == 1 {
            if selectedMedArr.count == 0 {
                return 0
            }
            return UITableView.automaticDimension
        }
        if indexPath.section == 2 {
            if indexPath.row == 0 {
                return viewFlag ? 0 : 60
            }
            if indexPath.row == 1 {
                return 120
            }
            if indexPath.row == 2 {
                return 80
            }
        }
        return viewFlag ? 90 : 60
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "Drug List"
        }
        return ""
    }
}

extension AddPrescription: UIImagePickerControllerDelegate {
    
    func showImgPicker() {
        print("Tap On Image")
        imgPicker.delegate = self
    
        let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {
            action in
            
            self.imgPicker.allowsEditing = true
            self.imgPicker.sourceType = .camera
            self.imgPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            self.present(self.imgPicker, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: {
            action in
            
            self.imgPicker.allowsEditing = true
            self.imgPicker.sourceType = .photoLibrary
            self.imgPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(self.imgPicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "PDF", style: .default, handler: {
            action in
            let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
            importMenu.delegate = self
            importMenu.modalPresentationStyle = .formSheet
            self.present(importMenu, animated: true, completion: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
    guard let image = info[.editedImage] as? UIImage else {
        return
    }
        if let imgdata = image.jpegData(compressionQuality: 0.5){
            self.prescriptionImg = imgdata
        }
        self.tblData.reloadData()
        self.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}

protocol AddPrescriptionCellDelegate {
    func onValueSelected(type: String, value: String)
    func onDataSelected(type: String, value: Any)
    func onSaveClicked()
    func onAddClicked()
    func onImgRemoved()
    func onAddMedClicked()
    func onQtyChanged(index: Int, value: Int)
}

class AddPrescriptionCell: BaseCell, UIStepperControllerDelegate {
    
    @IBOutlet weak var txtFld: RightViewTextField!
    var delegate: AddPrescriptionCellDelegate?
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var lblMedName: UILabel!
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var stepper: UIStepperController!
    @IBOutlet weak var lblRelation: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    
    var prescriptionImg:Data?
    var pdfImg: Data?
    var presImgUrl: String?
    var selRow: Int = 0
    var seldate: String?
    var viewFlag: Bool = false
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
    
    
    func setup(id: String, placeHolder: String){
        
        
        if id == "familyMember" {
            
            self.selType = id
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            txtFld.placeholder = placeHolder
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
        
        if id == "purpose" {
           // self.pickerDataArr = ["Myself","Doctor"]
            self.selType = id
            cellPicker.dataSource = self
            cellPicker.delegate = self
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            txtFld.addBottomBorder()
            txtFld.placeholder = placeHolder;
        }
        
        if id == "date" {
            
            txtFld.inputView = datePicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            txtFld.addBottomBorder()
            txtFld.placeholder = placeHolder;
        }
        if id == "drname" {
            
            txtFld.accessibilityIdentifier = id
            txtFld.rightImage = nil
            txtFld.addBottomBorder()
            txtFld.placeholder = placeHolder;
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
        }
    }
 
    @objc func dateSelected(_ picker: UIDatePicker){
        
        let dateStr = picker.date.toString(format: df_dd_MM_yyyy)//df.string(from: picker.date)
        
        txtFld.text = dateStr
        
        let date = picker.date.toString(format: "YYYY-MM-dd")
        //delegate?.onValueSelected(type: "date", value: date)
        seldate = date
    }
    
    @IBAction func addAction(_ sender: UIButton){
        
        delegate?.onAddClicked()
    }
    
    @IBAction func addMedAction(_ sender: UIButton){
        
        delegate?.onAddMedClicked()
    }
    
    @IBAction func saveAction(_ sender: UIButton){
        
        delegate?.onSaveClicked()
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        print("selType:\(selType)")
        selRow = row
        
        switch selType {
            case "purpose":
                let type = self.pickerDataArr[selRow] as? IllnessTypeData
                txtFld.text = type?.illness_type
                
            case "sharingType":
                txtFld.text = pickerDataArr[row] as? String
            
            case "familyMember":
                let data = pickerDataArr[row] as? FamilyMemberData
                txtFld.text = data?.name
            
            
        default:
            break
        }
        
        //delegate?.onValueSelected(type: selType, value: txtFld.text ?? "")
    }
    
    func stepperDidAddValues(stepper: UIStepperController) {
    
         print("Stepper value did change (Add) : \(stepper.count)")
        let tag = stepper.tag
        delegate?.onQtyChanged(index: tag, value: Int(stepper.count))
    }

    func stepperDidSubtractValues(stepper: UIStepperController) {
    
         print("Stepper value did change (Subtract) \(stepper.count)")
        let tag = stepper.tag
        delegate?.onQtyChanged(index: tag, value: Int(stepper.count))
         
    }
    
    @objc override func doneCellPicker (sender: UIBarButtonItem) {
        
        let id = sender.accessibilityIdentifier
        
        if id == "purpose" {
            let value = (pickerDataArr[selRow] as? IllnessTypeData)!
            txtFld.text = value.illness_type
            delegate?.onDataSelected(type: id!, value: value)
        }
        if id == "date" {
            seldate = datePicker.date.toString(format: df_dd_MM_yyyy)
            txtFld.text = seldate!
            delegate?.onValueSelected(type: id!, value: seldate!)
        }
        if id == "familyMember" {
            if pickerDataArr.count > 0 {
                let data = pickerDataArr[selRow] as? FamilyMemberData
                txtFld.text = data?.name
                delegate?.onDataSelected(type: id!, value: data!)
            }
        }
        if id == "drname" {
            delegate?.onValueSelected(type: id!, value: txtFld.text ?? "")
        }
        txtFld.resignFirstResponder()
    }
}
extension AddPrescriptionCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, PrescriptionCollCellDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = PrescriptionCollCell()
        if indexPath.item == 1 {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addCell", for: indexPath) as! PrescriptionCollCell
        } else {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imgCell", for: indexPath) as! PrescriptionCollCell
            
            if let imgData = prescriptionImg {
                cell.imgview.image = UIImage(data: imgData)
            } else if pdfImg != nil {
                cell.imgview.image = UIImage(named: "pdf")
            } else if presImgUrl != nil {
                if presImgUrl!.contains("pdf") {
                    cell.imgview.image = UIImage(named: "pdf")
                } else {
                    let imgURL = prescriptionImageURL + presImgUrl!
                    cell.imgview.kf.setImage(with: URL(string: imgURL))
                }
            }
            if viewFlag {
                cell.btnDelete.isHidden = true
            }
            cell.delegate = self
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
      
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 90, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if prescriptionImg == nil {
            
            delegate?.onAddClicked()
        }
    }
    
    func oDeleteClicked(){
        
        prescriptionImg = nil
        delegate?.onImgRemoved()
       
    }
}

protocol PrescriptionCollCellDelegate {
    
    func oDeleteClicked()
}
class PrescriptionCollCell: UICollectionViewCell {
    
    @IBOutlet weak var imgview: UIImageView!
    @IBOutlet weak var btnDelete:UIButton!
    var delegate: PrescriptionCollCellDelegate?
    
 
    @IBAction func deleteAction(_ sender: UIButton){
        delegate?.oDeleteClicked()
    }
}
