//
//  FamilyMemberListVC.swift
//  HealthApp
//
//  Created by Apple on 10/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class FamilyMemberListVC: BaseVC {

    var familyMembersArr: [FamilyMemberData] = []
    
    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var lblHead: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblData.estimatedRowHeight = 150
        getFamilyMembers()
    }
    
    func getFamilyMembers() {
        
        IHProgressHUD.show()
        
        let params = ["user_id": loggedUser?.id] as [String: Any]
        apiManager.getFamilyMember(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let memberData):
                    
                    self.familyMembersArr = memberData
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FamilyMemberListVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return familyMembersArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //let cell = FamilyMemberListCell()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "familyCell") as! FamilyMemberListCell
        
        let data = self.familyMembersArr[indexPath.row]
        cell.populateData(data: data)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = self.familyMembersArr[indexPath.row]
        //ChemistDetailsVC
        
        let vc = self.getVC(with: "ChemistDetailsVC", sb: IDMain) as! ChemistDetailsVC
        vc.familyMemberData = data
        vc.type = "familymember"
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
}
class FamilyMemberListCell: BaseCell {
    
    @IBOutlet weak var imgView: CircularImg!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblRel: UILabel!
    
    func populateData(data: FamilyMemberData) {
        
        self.lblName.text = data.name
        
        let relation = "Relationship: " + data.relation!
        self.lblRel.attributedText = relation.setAttributes(str1: "Relationship: ", str2: data.relation!, color1: .black, color2: .black, font1: SFProBold, font2: SFProRegular)
        
        let details = "Details: " + data.getDetails()
        self.lblDesc.attributedText = details.setAttributes(str1: "Details: ", str2: data.getDetails(), color1: .black, color2: .black, font1: SFProBold, font2: SFProRegular)
        
        if let strurl = data.profile_image, strurl != "null", let url = URL(string: imgBaseURL + custProfileImg + strurl) {
            self.imgView.kf.setImage(with: url)
        }
    }
    
    func populateMemberData(data: FamilyMemberData) {
        
        self.lblName.text = data.name
        
        //let relation = "Relationship: " + data.relation!
        //self.lblRel.attributedText = relation.setAttributes(str1: "Relationship: ", str2: data.relation!, color1: .black, color2: .black, font1: SFProBold, font2: SFProRegular)
        self.lblRel.text = ""
        
        let details = "Details: " + data.getDetails()
        self.lblDesc.text = details//details.setAttributes(str1: "Details: ", str2: data.getDetails(), color1: .black, color2: .black, font1: SFProBold, font2: SFProRegular)
        
        if let strurl = data.profile_image, strurl != "null", let url = URL(string: imgBaseURL + custProfileImg + strurl) {
            self.imgView.kf.setImage(with: url)
        }
    }
}
