//
//  NearByVC.swift
//  HealthApp
//
//  Created by Apple on 10/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class NearByVC: BaseVC {

    var nearbyCellData = ["Doctors","Chemists","Diagnostics","Service Providers"]
    var nearby_keys = ["near_doc","near_chem","near_diag","near_service"]
    var nearbyIcons = ["doctors1","medicines1","monitor","doctors1"]
    var arrCellData: [AlertCellData] = []
    
    @IBOutlet weak var lblHead: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        var cnt = 0
        for i in 0..<nearbyCellData.count {
            
            
            let data = AlertCellData(id: cnt,title: nearbyCellData[i], img: nearbyIcons[i], tag: nearby_keys[i])
            cnt = cnt + 1
            arrCellData.append(data)
            
        }
        
        lblHead.text = "NEAR BY"
        
        arrCellData.sort { (a, b) -> Bool in
           return a.id < b.id
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let tabbarvc = self.tabBarController as? HomeVC {
            
            tabbarvc.navigationController?.setNavigationBarHidden(false, animated: false)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        /*
        if let tabbarvc = self.tabBarController as? HomeVC {
            
            tabbarvc.navigationController?.setNavigationBarHidden(true, animated: false)
        }
        */
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    @objc @IBAction func clickAction(_ sender: UIButton) {
    
        let id = sender.accessibilityIdentifier
        
        if id == "near_doc" {
            let vc = self.getVC(with: "ParallaxVC", sb: "Main") as! ParallaxVC
            vc.type = "doctor"
            self.navigationController?.pushViewController(vc, animated: false)
        }
        if id == "near_chem" {
            let vc = self.getVC(with: "ParallaxVC", sb: "Main") as! ParallaxVC
            vc.type = "chemist"
            self.navigationController?.pushViewController(vc, animated: false)
        }
        if id == "near_diag" {
            let vc = self.getVC(with: "ParallaxVC", sb: "Main") as! ParallaxVC
            vc.type = "diagnostic"
            self.navigationController?.pushViewController(vc, animated: false)
        }
        if id == "near_service" {
            
            let vc = self.getVC(with: "MyAlertsVC", sb: "Main") as! MyAlertsVC
            vc.type = "Service"
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NearByVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "alertCell") as! AlertCell
        
        
        let data = arrCellData[indexPath.row + indexPath.row]
        cell.title1.text = data.title
        
        let data1 = arrCellData[indexPath.row + indexPath.row + 1]
        cell.title2.text = data1.title
        
        if let img1 = data.img, !img1.isEmpty {
            cell.img1.image = UIImage(named: img1)
        }
        if let img2 = data1.img, !img2.isEmpty {
            cell.img2.image = UIImage(named: img2)
        }
        
        cell.btn1.accessibilityIdentifier = data.tag
        cell.btn2.accessibilityIdentifier = data1.tag
        
        cell.btn1.addTarget(self, action: #selector(clickAction(_:)), for: .touchUpInside)
        cell.btn2.addTarget(self, action: #selector(clickAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
}
