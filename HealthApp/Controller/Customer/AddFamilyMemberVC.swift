//
//  AddFamilyMemberVC.swift
//  HealthApp
//
//  Created by Apple on 11/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class AddFamilyMemberVC: BaseVC, AddFamilyMemberCellDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var tblData: UITableView!
    let picker = UIImagePickerController()
    
    var relationArr: [RelationData] = []
    var professionArr: [ProfessionData] = []
    var memberTypeArr: [String] = ["Adult","Child"]
    var selRelation: RelationData?
    var selProfession: ProfessionData?
    var requestData: AddFamilyRequestData = AddFamilyRequestData()
    var selFt: Int?
    var selInch: Int?
    var selPrefix: String?
    var profileImgData: Data?
    var editFlag: Bool = false
    var selMemberType: String = "Adult"
    var selFamilyMember: FamilyMemberData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.requestData.parent_id = Int(self.loggedUser!.id)
        
        if editFlag {
            
            let arr = requestData.name.components(separatedBy: ".")
            if arr.count > 0 {
                selPrefix = arr[0]
                requestData.name = arr.count > 1 ? arr[1] : arr[0]
            }
            selMemberType = "Child"
            self.tblData.reloadData()
            
        } else {
            self.requestData.height_unit = "Feets"
            self.requestData.weight_unit = "Kg"
        }
        
        self.getRelationAPI()
        self.getProfessionAPI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tblData.reloadData()
    }
    
    func getRelationAPI(){
        
        IHProgressHUD.show()
        apiManager.getRelations(params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let relData):
                    
                    self.relationArr = relData
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func getProfessionAPI(){
        
        IHProgressHUD.show()
        apiManager.getProfession(params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let profData):
                    
                    self.professionArr = profData
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func uploadImage(){
        
        IHProgressHUD.show()
        
        apiManager.uploadImg(image: profileImgData!, name: "image", api: ProfileImage) { (result) in
         
            IHProgressHUD.dismiss()
            switch result {
                case .success(let data):
                    let imgstr = String(data: data, encoding: .utf8)
                print(imgstr)
                    
                    if((imgstr?.contains("jpeg"))!){
                        self.requestData.profile_image = imgstr!
                        
                        if self.editFlag {
                            self.updateFamilyMember()
                        } else {
                            self.addFamilyMember()
                        }
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
        
    }
    
    func addFamilyMember(){
            
        IHProgressHUD.show()
        
        if self.requestData.height_unit == "Feets" {
            if let feet = selFt, let inch = selInch {
                
                let height = "\(feet):\(inch)"
                self.requestData.height = height
            }
        }
       
        do {
            let paramsData = try JSONEncoder().encode(self.requestData)
            
            let params = try? JSONSerialization.jsonObject(with: paramsData, options: []) as? [String:Any]
            print("params: \(params)")
            apiManager.addFamilyMemberAPI(params: params!) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                case .success(let data):
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                        
                        self.navFamilyList()
                    }
                case .failure(let err):
                    print("err: \(err)")
                }
            }
        } catch let err {
            
        }
        
    }
    
    //FamilyMemberListVC
    func navFamilyList() {
        
        //let dashboardVC = (self.navigationController?.viewControllers.first) as! DashboardVC
        
        let dashboardVC = self.getVC(with: "DashboardVC", sb: "Main") as! DashboardVC
        
        let familyVC = self.getVC(with: "FamilyMemberListVC", sb: IDMain) as! FamilyMemberListVC
        
        let vcArray = [dashboardVC, familyVC]
        self.navigationController?.setViewControllers(vcArray, animated: false)
        //self.navigationController?.pushViewController(vc, animated: false)
    }
    //
    func updateFamilyMember(){
            
        IHProgressHUD.show()
        
        if self.requestData.height_unit == "Feets" {
            if let feet = selFt, let inch = selInch {
                
                let height = "\(feet):\(inch)"
                self.requestData.height = height
            }
        }
        if (self.requestData.name.range(of: selPrefix!) == nil) {
            self.requestData.name = selPrefix! + ". " + self.requestData.name
        }
        do {
            let paramsData = try JSONEncoder().encode(self.requestData)
            
            let params = try? JSONSerialization.jsonObject(with: paramsData, options: []) as? [String:Any]
            print("params: \(params)")
            apiManager.updateFamilyMember(params: params!) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                case .success(let data):
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                    
                case .failure(let err):
                    print("err: \(err)")
                }
            }
        } catch let err {
            
        }
        
    }
    
    func onValueSelected(type: String, value: String) {
        
        switch type {
            
        case "selectCell":
            self.selMemberType = value
            if value == "Child" {
                self.selFamilyMember = nil
            }
            self.tblData.reloadData()
            self.tblData.reloadData()
        case "prefix":
            self.selPrefix = value
        case "name":
            self.requestData.name = value
        case "mobile":
            self.requestData.mobile_no = value
        case "email":
            self.requestData.email = value
        case "gender":
            self.requestData.gender = value
        case "height_unit":
            self.requestData.height_unit = value
        case "height":
            self.requestData.height = value
        case "weight_unit":
            self.requestData.weight_unit = value
        case "weight":
            self.requestData.weight = value
        case "date":
            self.requestData.dob = value
        case "feet":
            self.selFt = Int(value)
        case "inch":
            self.selInch = Int(value)
        default:
            break
        }
        
        print("requestdata: \(requestData)")
    }
    
    func onDataSelected(type: String, value: Any) {
        
        switch type {
        case "relation":
            self.requestData.relation = (value as! RelationData).id
        case "profession":
            self.requestData.profession = (value as! ProfessionData).id
        
        default:
            break
        }
    }
    
    func validate() -> Bool{
        
        /*
        if (self.selPrefix ?? "").isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select prefix")
            
            return false
        }*/
        if self.requestData.name.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter name")
            return false
        }
        if self.requestData.gender.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select gender")
            return false
        }
        if self.requestData.relation == nil {
            
            self.alert(strTitle: strErr, strMsg: "Please select relation")
            return false
        }
        if self.requestData.dob.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select date of birth")
            return false
        }
        if  self.requestData.dob.toDate(format:df_dd_MM_yyyy).getDiff() > 18 {
            
            if String(self.requestData.mobile_no).isEmpty {
                self.alert(strTitle: strErr, strMsg: "Please enter mobile no")
                return false
            }
        }
        return true
    }
    
    func onSaveClicked() {
        if validate() {
            if profileImgData != nil {
                self.uploadImage()
            } else {
                if editFlag {
                    self.updateFamilyMember()
                } else {
                    self.addFamilyMember()
                }
            }
        }
    }
    
    func onSendInviteClicked(tag: Int) {
        
        if tag == 1 {
            // send req
            IHProgressHUD.show()
            
            let params = ["member_id": loggedUser!.id,
                          "family_member_id": selFamilyMember!.id, "relation": self.requestData.relation!]  as [String : Any]
            
            apiManager.requestFamilyMember(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                case .success(let data):
                    print("data: \(data)")
                    
                    DispatchQueue.main.async {
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                    }
                case .failure(let err):
                    print("err: \(err)")
                    DispatchQueue.main.async {
                        self.alert(strTitle: strSuccess, strMsg: err.localizedDescription)
                    }
                }
            }
        }
        if tag == 2 {
            // invite
            IHProgressHUD.show()
            
            
            let params = ["invited_by": loggedUser?.id,
                          "mobile_no": self.requestData.mobile_no]  as [String : Any]
            
            apiManager.inviteMember(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                case .success(let data):
                    print("data: \(data)")
                    
                    DispatchQueue.main.async {
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                    }
                case .failure(let err):
                    print("err: \(err)")
                    DispatchQueue.main.async {
                        self.alert(strTitle: strSuccess, strMsg: err.localizedDescription)
                    }
                }
            }
        }
    }

    @objc func getUserByMobile(text: String) {
        
        //if let mobile = text {
        
            let mobile = text
            self.requestData.mobile_no = mobile
            
            IHProgressHUD.show()
            
            let params = ["mobile_no": mobile]  as [String : Any]
            print("params:\(params)")
            
            apiManager.getUserByMobile(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                case .success(let data):
                    print("data: \(data)")
                    self.selFamilyMember = data
                    
                    DispatchQueue.main.async {
                        var msg = ""
                        if Int(self.selFamilyMember!.role!) != 2 {
                            switch self.selFamilyMember?.role {
                            case "3":
                                msg = "This Mobile is registered as Chemist! You cannot add it as Family member"
                            case "4":
                                msg = "This Mobile is registered as Path Lab! You cannot add it as Family member."
                            case "5":
                                msg = "This Mobile is registered as Doctor! You cannot add it as Family member."
                            case "6":
                                msg = "This Mobile is registered as Service Provider! You cannot add it as Family member."
                            default:
                                break
                            }
                            self.alert(strTitle: "Error", strMsg: msg)
                        } else {
                            self.tblData.reloadData()
                        }
                    }
                case .failure(let err):
                    print("err: \(err)")
                    self.selFamilyMember = nil
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                }
            }
        //}
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        
        if textField.tag == 111 {
            print("string: \(string)")
            /*
            NSObject.cancelPreviousPerformRequests(
                withTarget: self,
                selector: #selector(getUserByMobile),
                object: textField)
            self.perform(
                #selector(getUserByMobile),
                with: textField,
                afterDelay: 1.2)
 */
            if range.location == 9 && range.length != 1 {
             
                let mob = textField.text! + string
                if mob == loggedUser!.mobile_no! {
                    
                    self.alert(strTitle: strErr, strMsg: "Enter other mobile no.")
                    
                } else {
                    getUserByMobile(text: mob)
                }
            }
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddFamilyMemberVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                return self.selMemberType == "Child" ? 140: 50
            }
            if self.selMemberType == "Adult" && indexPath.row == 1 {
                return !self.requestData.mobile_no.isEmpty ? 50 : 0
            }
            if self.selMemberType == "Adult" && indexPath.row == 2 {
                return (self.selFamilyMember != nil)  ? 50 : 0
            }
            if self.selMemberType == "Adult" && indexPath.row == 3 {
                return !self.requestData.mobile_no.isEmpty ? 50 : 0
            }
            
            return self.selMemberType == "Child" ? 60 : 0
        }
        return editFlag ? 0 : 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 1 {
            return self.selMemberType == "Child" ?  11 : 4
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = AddFamilyMemberCell()
        
        if indexPath.section == 0 {
            
            if indexPath.row == 0 {
                cell = tableView.dequeueReusableCell(withIdentifier: "selectCell") as! AddFamilyMemberCell
                cell.delegate = self
                cell.pickerDataArr = memberTypeArr
                cell.setup(id: "selectCell", placeHolder: "")
                cell.txtFld.text = self.selMemberType
            }
            
        }
        if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                if self.selMemberType == "Child" {
                    cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! AddFamilyMemberCell
                    cell.delegate = self
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchHappen(_:)))
                    tap.delegate = self
                    if profileImgData != nil {
                        cell.profileImg.image = UIImage(data: profileImgData!)
                    } else if !requestData.profile_image.isEmpty && requestData.profile_image != "null" {
                        let url = imgBaseURL + custProfileImg + requestData.profile_image
                        cell.profileImg.kf.setImage(with: URL(string: url))
                    } else {
                        cell.profileImg.image = UIImage(named: "User")
                    }
                    
                    cell.profileImg.addGestureRecognizer(tap)
                    cell.profileImg.isUserInteractionEnabled = true
                } else {
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! AddFamilyMemberCell
                    cell.delegate = self
                    cell.setup(id: "mobile", placeHolder: "Mobile No")
                    cell.bottomTxtFld.tag = 111
                    cell.bottomTxtFld.delegate = self
                }
                
            case 1:
                if self.selMemberType == "Child" {
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AddFamilyMemberCell
                    cell.delegate = self
                    cell.setup(id: "name", placeHolder: "Name")
                    //cell.bottomTxtFld.text = hospital
                    //if editFlag {
                        cell.txtFld.text = selPrefix
                        cell.bottomTxtFld.text = requestData.name
                    //}
                } else {
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! AddFamilyMemberCell
                    cell.delegate = self
                    cell.bottomTxtFld.text = selFamilyMember?.name
                    
                    
                }
                
            case 2:
                if self.selMemberType == "Child" {
                    cell = tableView.dequeueReusableCell(withIdentifier: "dropDownCell") as! AddFamilyMemberCell
                    cell.delegate = self
                    cell.setup(id: "gender", placeHolder: "Gender")
                    if editFlag {
                        cell.txtFld.text = requestData.gender
                        
                    } else {
                        cell.txtFld.text = requestData.gender
                    }
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "dropDownCell") as! AddFamilyMemberCell
                    cell.delegate = self
                    cell.setup(id: "relation", placeHolder: "Relationship")
                    cell.pickerDataArr = self.relationArr
                    
                    
                }
                
            case 3:
                if self.selMemberType == "Child" {
                    cell = tableView.dequeueReusableCell(withIdentifier: "dateCell") as! AddFamilyMemberCell
                    cell.delegate = self
                    cell.setup(id: "date", placeHolder: "DOB")
                    
                    if editFlag {
                        cell.txtFld.text = requestData.dob
                    }
                }
                else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! AddFamilyMemberCell
                    cell.delegate = self
                    if self.selFamilyMember != nil {
                        cell.btnSave.setTitle("SEND REQUEST", for: .normal)
                        cell.btnSave.tag = 1
                    } else {
                        cell.btnSave.setTitle("INVITE ON CTRLH", for: .normal)
                        cell.btnSave.tag = 2
                    }
                    //cell.btnSave.addTarget(self, action: #selector(cell.sendInviteAction), for: .touchUpInside)
                    cell.setup(id: "sendreq", placeHolder: "")
                }
                
            case 4:
                cell = tableView.dequeueReusableCell(withIdentifier: "dropDownCell") as! AddFamilyMemberCell
                cell.delegate = self
                cell.setup(id: "relation", placeHolder: "Relationship")
                cell.pickerDataArr = self.relationArr
                if editFlag {
                    cell.txtFld.text = requestData.strRelation
                }
                
            case 5:
                cell = tableView.dequeueReusableCell(withIdentifier: "heightCell") as! AddFamilyMemberCell
                cell.delegate = self
                cell.setup(id: "height", placeHolder: "")
                cell.txtFld2.text = self.requestData.height_unit
                
                if editFlag {
                    if requestData.height_unit == "Feets" {
                        let arr = requestData.height.components(separatedBy: ":")
                        if arr.count > 0 {
                            cell.txtFld.text = arr[0]
                            cell.txtFld1.text = arr[1]
                            cell.txtFld2.text = "Feets"
                        }
                    } else {
                        cell.txtFld1.text = requestData.height
                    }
                }
                
            case 6:
                cell = tableView.dequeueReusableCell(withIdentifier: "weightCell") as! AddFamilyMemberCell
                cell.delegate = self
                cell.setup(id: "weight", placeHolder: "Weight")
                cell.txtFld.text = self.requestData.weight_unit
                cell.bottomTxtFld.text = requestData.weight
                
            case 7:
                cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! AddFamilyMemberCell
                cell.delegate = self
                cell.setup(id: "mobile", placeHolder: "Mobile No")
                //if editFlag {
                    cell.bottomTxtFld.text = requestData.mobile_no
                //}
                
            case 8:
                cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! AddFamilyMemberCell
                cell.delegate = self
                cell.setup(id: "email", placeHolder: "EmailId")
                //if editFlag {
                    cell.bottomTxtFld.text = requestData.email
                //}
                
            case 9:
                cell = tableView.dequeueReusableCell(withIdentifier: "dropDownCell") as! AddFamilyMemberCell
                cell.delegate = self
                cell.setup(id: "profession", placeHolder: "Profession")
                cell.pickerDataArr = self.professionArr
                if editFlag {
                    let prof = self.professionArr.filter{ ($0.id == requestData.profession) }
                    if prof.count > 0 {
                        cell.txtFld.text = prof[0].profession
                    }
                }
                
            case 10:
                cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! AddFamilyMemberCell
                cell.delegate = self
                
                if editFlag {
                    cell.btnSave.setTitle("UPDATE MEMBER", for: .normal)
                } else {
                    cell.btnSave.setTitle("ADD MEMBER", for: .normal)
                }
                cell.setup(id: "save", placeHolder: "")
                
            default:
                break
            }
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //simpleTxtFldCell
}
extension AddFamilyMemberVC: UIImagePickerControllerDelegate {
    
    @objc func touchHappen(_ sender: UITapGestureRecognizer){
            print("Tap On Image")
            picker.delegate = self
        let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {
                action in
                
                self.picker.allowsEditing = true
                self.picker.sourceType = .camera
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                self.present(self.picker, animated: true, completion: nil)
                
            }))
            alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: {
                action in
                
                self.picker.allowsEditing = true
                self.picker.sourceType = .photoLibrary
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                self.present(self.picker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            
            present(alert, animated: true, completion: nil)
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.editedImage] as? UIImage else {
            return
        }
            profileImgData = image.jpegData(compressionQuality: 0.5)
            self.tblData.reloadData()
            dismiss(animated: true, completion: nil)
        }

        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
}
protocol AddFamilyMemberCellDelegate {
    
    func onValueSelected(type: String, value: String)
    func onDataSelected(type: String, value: Any)
    func onSaveClicked()
    func onSendInviteClicked(tag: Int)
}
class AddFamilyMemberCell: BaseCell {

    @IBOutlet weak var txtFld: RightViewTextField! //BorderRightViewTextFld!
    @IBOutlet weak var txtFld1: RightViewTextField!
    @IBOutlet weak var txtFld2: RightViewTextField!
    @IBOutlet weak var bottomTxtFld: UITextField!//BottomLineTextField!
    @IBOutlet weak var profileImg: UIImageView!
    
    @IBOutlet weak var con_zeroW_btmTxt: NSLayoutConstraint!
    @IBOutlet weak var con_W_btmTxt: NSLayoutConstraint!
    @IBOutlet weak var con_zeroW_Txt: NSLayoutConstraint!
    @IBOutlet weak var con_W_Txt: NSLayoutConstraint!
    @IBOutlet weak var con_zeroW_Txt1: NSLayoutConstraint!
    @IBOutlet weak var con_W_Txt1: NSLayoutConstraint!
    
    @IBOutlet weak var btnSave: UIButton!
    
    var delegate: AddFamilyMemberCellDelegate?
    var selRow: Int = 0
    var seldate: String?
    var feetPickerArr: [Int] = []
    var inchPickerArr: [Int] = []
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
    func setup(id: String, placeHolder: String){
           
        if id == "selectCell" {
            self.selType = id
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
        }
        if id == "name" {
            
            self.pickerDataArr = ["Mr","Miss","Mrs"]
            self.selType = "prefix"
            cellPicker.dataSource = self
            cellPicker.delegate = self
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "prefix")
            //bottomTxtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.accessibilityIdentifier = id
            
            txtFld.setPlaceholder(str1: "*", str2: "Prefix")
            bottomTxtFld.setPlaceholder(str1: "*", str2: placeHolder)
            
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
            bottomTxtFld.removeBottomBorder()
            bottomTxtFld.addBottomBorder()
        }
        if id == "gender" {
            
            self.selType = id
            self.pickerDataArr = ["Male","Female"]
            cellPicker.dataSource = self
            cellPicker.delegate = self
            txtFld.accessibilityIdentifier = id
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
            //txtFld.addBottomBorder()
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            txtFld.placeholder = placeHolder
            //bottomTxtFld.setPlaceholder(str1: "*", str2: placeHolder)
            
        }
        if id == "date" {
            
            txtFld.inputView = datePicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            //txtFld.placeholder = placeHolder
            //txtFld.addBottomBorder()
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
            txtFld.setPlaceholder(str1: "*", str2: placeHolder)
            
        }
        if id == "relation" {
            
            self.selType = id
            
            cellPicker.dataSource = self
            cellPicker.delegate = self
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            txtFld.placeholder = placeHolder
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
        }
        if id == "height" {
            
            self.selType = id
            self.pickerDataArr = ["Feets","Centimeters"]
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            txtFld2.addBottomBorder()
            txtFld2.inputView = cellPicker
            txtFld2.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "height_unit")
            
            let feetPicker =  UIPickerView()
            feetPicker.accessibilityIdentifier = "feet"
            feetPicker.dataSource = self
            feetPicker.delegate = self
            txtFld.inputView = feetPicker
            //txtFld.addBottomBorder()
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "feet")
            txtFld.placeholder = "Feet"
            
            let inchPicker =  UIPickerView()
            inchPicker.accessibilityIdentifier = "inch"
            inchPicker.dataSource = self
            inchPicker.delegate = self
            txtFld1.inputView = inchPicker
            txtFld1.addBottomBorder()
            txtFld1.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "inch")
            txtFld1.placeholder = "Inch"
            
            bottomTxtFld.accessibilityIdentifier = id
            bottomTxtFld.placeholder = "Height"
            bottomTxtFld.removeBottomBorder()
            bottomTxtFld.addBottomBorder()
        }
        if id == "weight" {
            
            self.selType = id
            self.pickerDataArr = ["Kg","lbs"]
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            //txtFld.addBottomBorder()
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "weight_unit")
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.accessibilityIdentifier = id
            bottomTxtFld.removeBottomBorder()
            bottomTxtFld.addBottomBorder()
            
        }
        if id == "mobile" {
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.accessibilityIdentifier = id
            bottomTxtFld.removeBottomBorder()
            bottomTxtFld.addBottomBorder()
        }
        if id == "email" {
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.accessibilityIdentifier = id
            bottomTxtFld.removeBottomBorder()
            bottomTxtFld.addBottomBorder()
        }
        if id == "profession" {
            
            self.selType = id
            
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            //txtFld.addBottomBorder()
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            txtFld.placeholder = placeHolder
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
        }
        
        if id == "save" {
            btnSave.removeTarget(self, action: #selector(sendInviteAction), for: .touchUpInside)
            btnSave.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        }
        
        if id == "sendreq" {
            btnSave.removeTarget(self, action: #selector(saveAction), for: .touchUpInside)
            btnSave.addTarget(self, action: #selector(sendInviteAction), for: .touchUpInside)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
        print("selType:\(selType)")
        selRow = row
        if pickerView.accessibilityIdentifier == "feet" {
            txtFld.text = String(row)
        }
        else if pickerView.accessibilityIdentifier == "inch" {
            txtFld1.text = String(row+1)
            
        } else {
            
            switch selType {
                
            case "prefix":
                txtFld.text = pickerDataArr[row] as? String
            case "gender":
                txtFld.text = pickerDataArr[row] as? String
                
            case "height":
                if selRow == 0 {
                    self.removeConstraint(con_W_btmTxt)
                    self.removeConstraint(con_zeroW_Txt)
                    self.removeConstraint(con_zeroW_Txt1)
                    txtFld1.rightView?.isHidden = false
                } else {
                    self.addConstraint(con_W_btmTxt)
                    self.addConstraint(con_zeroW_Txt)
                    self.addConstraint(con_zeroW_Txt1)
                    txtFld1.rightView?.isHidden = true
                }
                let value = (pickerDataArr[selRow] as? String)!
                txtFld2.text = value
                
            case "weight":
                let value = (pickerDataArr[selRow] as? String)!
                txtFld.text = value
                
            case "relation":
                let relationdata = pickerDataArr[row] as? RelationData
                txtFld.text = relationdata?.relation
                
            case "profession":
                let profdata = pickerDataArr[row] as? ProfessionData
                txtFld.text = profdata?.profession
                
            default:
                break
            }
        }
        
        //delegate?.onValueSelected(type: selType, value: txtFld.text ?? "")
    }
    
    @objc override func doneCellPicker (sender: UIBarButtonItem) {
        
        
        let id = sender.accessibilityIdentifier
        
        if id == "selectCell" {
            let value = (pickerDataArr[selRow] as? String)!
            txtFld.text = value
            delegate?.onValueSelected(type: id!, value: value)
            txtFld.resignFirstResponder()
            
        }
        if id == "prefix" {
            let value = (pickerDataArr[selRow] as? String)!
            txtFld.text = value
            delegate?.onValueSelected(type: id!, value: value)
            txtFld.resignFirstResponder()
        }
        
        if id == "gender" {
            let value = (pickerDataArr[selRow] as? String)!
            txtFld.text = value
            delegate?.onValueSelected(type: id!, value: value)
            txtFld.resignFirstResponder()
        }
        if id == "height_unit" {
            let value = (pickerDataArr[selRow] as? String)!
            txtFld2.text = value
            delegate?.onValueSelected(type: id!, value: value)
            txtFld2.resignFirstResponder()
        }
        if id == "weight_unit" {
            let value = (pickerDataArr[selRow] as? String)!
            txtFld.text = value
            delegate?.onValueSelected(type: id!, value: value)
            txtFld.resignFirstResponder()
        }
        if id == "weight" {
            let value = (pickerDataArr[selRow] as? String)!
            txtFld.text = value
            delegate?.onValueSelected(type: id!, value: value)
            txtFld.resignFirstResponder()
            txtFld.resignFirstResponder()
        }
        if id == "relation" {
            let value = (pickerDataArr[selRow] as? RelationData)!
            txtFld.text = value.relation
            delegate?.onDataSelected(type: id!, value: value)
            txtFld.resignFirstResponder()
        }
        if id == "profession" {
            let value = (pickerDataArr[selRow] as? ProfessionData)!
            txtFld.text = value.profession
            delegate?.onDataSelected(type: id!, value: value)
            txtFld.resignFirstResponder()
        }
        if id == "date" {
            seldate = datePicker.date.toString(format: df_dd_MM_yyyy)
            let age = datePicker.date.getDiff()
            txtFld.text = "\(seldate!), \(age)"
            delegate?.onValueSelected(type: id!, value: seldate!)
            txtFld.resignFirstResponder()
        }
        if id == "feet" {
            txtFld.text = String(selRow)
            delegate?.onValueSelected(type: id!, value: txtFld.text!)
            txtFld.resignFirstResponder()
        }
        if id == "inch" {
            txtFld1.text = String(selRow+1)
            delegate?.onValueSelected(type: id!, value: txtFld1.text!)
            txtFld1.resignFirstResponder()
        }
        selRow = 0
    }
    
    @IBAction func saveAction(_ sender: UIButton){
        
        delegate?.onSaveClicked()
    }
    
    @IBAction func sendInviteAction(_ sender: UIButton){
        
        delegate?.onSendInviteClicked(tag: sender.tag)
    }
    
    
    @objc func dateSelected(_ picker: UIDatePicker){
        
        let dateStr = picker.date.toString(format: df_dd_MM_yyyy)//df.string(from: picker.date)
        
        txtFld.text = dateStr
        
        let date = picker.date.toString(format: "YYYY-MM-dd")
        seldate = date
    }
    
    @IBAction func textChanged(_ sender: UITextField) {
        delegate?.onValueSelected(type: sender.accessibilityIdentifier!, value: sender.text ?? "")
        print("id = \(sender.accessibilityIdentifier!)")
        print("value = \(sender.text)")
    }
}
