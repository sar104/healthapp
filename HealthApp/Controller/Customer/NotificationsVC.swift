//
//  NotificationsVC.swift
//  HealthApp
//
//  Created by Apple on 03/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class NotificationsVC: BaseVC {

    @IBOutlet weak var tblData: UITableView!
    
    var notifArr: [RemoteNotification] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        UIApplication.shared.applicationIconBadgeNumber = 0
        notifArr = CoreDataManager.shared.fetchNotifications()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NotificationsVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return notifArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "notifCell") as! NotifCell
        
        let data = notifArr[indexPath.row]
        
        cell.lblTitle.text = data.title
        cell.lblDate.text = data.currdate
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
}

class NotifCell: BaseCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
}
