//
//  HealthRecordContainerVC.swift
//  HealthApp
//
//  Created by Apple on 10/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class HealthRecordContainerVC: BaseVC {

    @IBOutlet weak var reportContainer: UIView!
    @IBOutlet weak var presContainer: UIView!
    
    //var recordType: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //print("recordType = \(recordType)")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        /*
        if let ReportVC = self.getVC(with: "AddReportVC", sb: IDMain) as? AddReportVC {
            
            
            ReportVC.willMove(toParent: self)
            self.reportContainer.addSubview(ReportVC.view)
            self.addChild(ReportVC)
            ReportVC.didMove(toParent: self)
            
            
            ReportVC.view.translatesAutoresizingMaskIntoConstraints = false
            ReportVC.view.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
            ReportVC.view.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
            ReportVC.view.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
            ReportVC.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
            //ReportVC.view.heightAnchor.constraint(equalToConstant: height).isActive = true
 
        }
        
        if let PrescriptionVC = self.getVC(with: "AddPrescription", sb: IDMain) as? AddPrescription {
                   
            PrescriptionVC.willMove(toParent: self)
            self.presContainer.addSubview(PrescriptionVC.view)
            self.addChild(PrescriptionVC)
            PrescriptionVC.didMove(toParent: self)
            
            
            PrescriptionVC.view.translatesAutoresizingMaskIntoConstraints = false
            PrescriptionVC.view.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
            PrescriptionVC.view.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
            PrescriptionVC.view.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
            PrescriptionVC.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
 
        }
 */
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segueReport" || segue.identifier == "seguePrescription"  {
            
            if let vc = segue.destination as? HealthRecordContainerVC {
                
                //vc.recordType = self.recordType
            }
        }
    }
    

}
