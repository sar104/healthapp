//
//  IntroVC.swift
//  HealthApp
//
//  Created by Apple on 21/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Foundation

class IntroVC: BaseVC {

    private var pageController: UIPageViewController?
    private var currIndex: Int = 0
    private var pages: [Pages] = Pages.allCases
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setPageController()
    }
    

    private func setPageController(){
        
        self.pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        self.pageController?.dataSource = self
        self.pageController?.delegate = self
        
        self.pageController?.view.backgroundColor = .clear
        self.pageController?.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.addChild(self.pageController!)
        self.view.addSubview(self.pageController!.view)
        
        let initialVC: PageVC = self.getVC(with: IDPageVC, sb: IDMain) as! PageVC
        initialVC.page = pages[0]
        initialVC.delegate = self
        self.pageController?.setViewControllers([initialVC], direction: .forward, animated: true, completion: nil)
        self.pageController?.didMove(toParent: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension IntroVC: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let currentVC = viewController as? PageVC else {
            return nil
        }
        
        var index = currentVC.page.index
        
        if index == 0 {
            return nil
        }
        
        index -= 1
        
        let pageVC: PageVC = self.getVC(with: IDPageVC, sb: IDMain) as! PageVC
        pageVC.page = pages[index]
        
        return pageVC
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let currentVC = viewController as? PageVC else {
            return nil
        }
        
        var index = currentVC.page.index
        
        if index >= 3 {
            return nil
        }
        
        index += 1
        
        let pageVC: PageVC = self.getVC(with: IDPageVC, sb: IDMain) as! PageVC
        pageVC.page = pages[index]
        
        return pageVC
    }
    
    
    
}
extension IntroVC: PageVCDelegate {
    
    
    func onSkip() {
        
        NotificationCenter.default.post(.init(name: Notification.Name(rawValue: LoginNotification)))
        
    }
    
    func onNext(index: Int) {
        
        currIndex = index
        if index >= 3 {
            //NotificationCenter.default.post(.init(name: Notification.Name(rawValue: LoginNotification)))
            NotificationCenter.default.post(.init(name: Notification.Name(rawValue: LoginNotification)))
            //
            return
        }
        
        currIndex += 1
        
        let initialVC: PageVC = self.getVC(with: IDPageVC, sb: IDMain) as! PageVC
        initialVC.delegate = self
        initialVC.page = pages[currIndex]
        self.pageController?.setViewControllers([initialVC], direction: .forward, animated: true, completion: nil)
        self.pageController?.didMove(toParent: self)
    }
    
    
    
}
