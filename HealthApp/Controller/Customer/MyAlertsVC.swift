//
//  MyAlertsVC.swift
//  HealthApp
//
//  Created by Apple on 24/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

struct AlertCellData {
    
    var id: Int!
    var title: String?
    var desc: String = ""
    var img: String?
    var tag: String?
}

class MyAlertsVC: BaseVC {

    @IBOutlet weak var lblHead: UILabel!
    
    var alertCellData = [ "MEDICINE", "DOCTOR VISIT", "CHILD GROWTH", "VACCINATION","MENSTRUAL CYCLE","PREGNANCY TRACKER"]
    var descData = ["Set alerts for your medicines", "Set alerts for doctor appointments", "Set alerts & track your child's growth", "Set alerts for vaccination","",""]
    var cellIcons = ["medicines1","doctors1","childgrowth","syringe","sanitary-pad","pregnant"]
    var keys = ["medicine", "doctor","child", "vaccin","period","pregnancy"]
    
    var reportsCellData = [ "My Prescription", "My Reports", "Family Prescription", "Family Reports"]
    var report_keys = ["my_pre", "my_re","fam_pre", "fam_re"]
    var ehrIcons = ["medicines_clr","reports-black","medicines_clr","reports-black"]
    
    var docCellData = ["Find Doctor","Favorite Doctor"]
    var doc_keys = ["find_doc","fav_doc"]
    
    var diagnosticCellData = ["Find Diagnostic Center","Favorite Diagnostic Center"]
    var diagnostic_keys = ["find_diag","fav_diag"]
    
    var serviceCellData = ["Fitness/Yoda Instructor","Care Giver","Nutritionist/Dietician","Nurse","Special Service Provider","Other Service Providers"]
    var service_keys = [kSpecFitness,kSpecCareGiver,kSpecNutritionist,kSpecNurse,kSpecService,kSpecOther]
    //var serviceIcons = ["doctors1","doctors1","medicines_clr","reports"]
    
    var nearbyCellData = ["Doctors","Chemists","Diagnostics","Service Providers"]
    var nearby_keys = ["near_doc","near_chem","near_diag","near_service"]
    var nearbyIcons = ["doctors1","medicines1","monitor","doctors1"]
    
    var arrCellData: [AlertCellData] = []
    var type: String = "NearBy"
    
    var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        //gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [RGB_gradPink.cgColor, RGB_gradBlue.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        //gradientLayer.locations = [0.0, 1.1]
       return gradientLayer
    }()
    
    
            
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        
        setup()
    }
    
    func setup() {
        
        let icon = self.view.viewWithTag(10) as? UIImageView
        var cnt = 0
        if type == "Alerts" {
            
            if icon != nil {
                icon?.image = UIImage(named: "reminder")
            }
            for i in 0..<alertCellData.count {
                
                
                let data = AlertCellData(id: cnt,title: alertCellData[i], desc: descData[i], img: cellIcons[i], tag: keys[i])
                cnt = cnt + 1
                if keys[i] == "period" || keys[i] == "pregnancy" {
                    if loggedUser?.gender == "Female" {
                        arrCellData.append(data)
                    }
                } else {
                    arrCellData.append(data)
                }
            }
            lblHead.text = "MY ALERTS"
        } else if type == "Reports" {
            
            if icon != nil {
                icon?.image = UIImage(named: "HealthRecord")
            }
            
            for i in 0..<reportsCellData.count {
                
                
                let data = AlertCellData(id: cnt,title: reportsCellData[i], img: ehrIcons[i], tag: report_keys[i])
                cnt = cnt + 1
                arrCellData.append(data)
                
            }
            lblHead.text = "REPORTS"
            
        } else if type == "Doctor" {
            
            for i in 0..<docCellData.count {
                
                
                let data = AlertCellData(id: cnt,title: docCellData[i], img: doc_keys[i], tag: doc_keys[i])
                cnt = cnt + 1
                arrCellData.append(data)
                
            }
            lblHead.text = "DOCTORS"
            
        } else if type == "Diagnostic" {
            
            if icon != nil {
                icon?.image = UIImage(named: "map")
            }
            
            for i in 0..<diagnosticCellData.count {
                
                
                let data = AlertCellData(id: cnt,title: diagnosticCellData[i], img: diagnostic_keys[i], tag: diagnostic_keys[i])
                cnt = cnt + 1
                arrCellData.append(data)
                
            }
            lblHead.text = "DIAGNOSTIC CENTERS"
            
        } else if type == "Service" {
            
            if icon != nil {
                icon?.image = UIImage(named: "map")
            }
            
            for i in 0..<serviceCellData.count {
                
                
                let data = AlertCellData(id: cnt,title: serviceCellData[i], img: "doctors1", tag: service_keys[i])
                cnt = cnt + 1
                arrCellData.append(data)
                
            }
            lblHead.text = "SERVICE PROVIDERS"
            
        } else if type == "NearBy" {
            
            if icon != nil {
                icon?.image = UIImage(named: "map")
            }
            
            for i in 0..<nearbyCellData.count {
                
                
                let data = AlertCellData(id: cnt,title: nearbyCellData[i], img: nearbyIcons[i], tag: nearby_keys[i])
                cnt = cnt + 1
                arrCellData.append(data)
                
            }
            lblHead.text = "NEAR BY"
        }
        
        arrCellData.sort { (a, b) -> Bool in
           return a.id < b.id
            
        }
    }
    
    @objc @IBAction func clickAction(_ sender: UIButton) {
        
        let id = sender.accessibilityIdentifier
        
        if id == "medicine" {
            let vc = self.getVC(with: "MedicineAlertVC", sb: IDMain) as! MedicineAlertVC
            vc.alert_type = kAlertTypeMed
            self.navigationController?.pushViewController(vc, animated: false)
        }
        if id == "doctor" {
           //AddDoctorAlertVC
            let vc = self.getVC(with: "MedicineAlertVC", sb: IDMain) as! MedicineAlertVC
            vc.alert_type = kAlertTypeDoc
            self.navigationController?.pushViewController(vc, animated: false)
        }
        if id == "child" {
            //AddGrowthAlertVC
            let vc = self.getVC(with: "MedicineAlertVC", sb: IDMain) as! MedicineAlertVC
            vc.alert_type = kAlertTypeChild
            self.navigationController?.pushViewController(vc, animated: false)
        }
        if id == "vaccin" {
           let vc = self.getVC(with: "MedicineAlertVC", sb: IDMain) as! MedicineAlertVC
           vc.alert_type = kAlertTypeVac
           self.navigationController?.pushViewController(vc, animated: false)
        }
        if id == "period" {
           let vc = self.getVC(with: "MedicineAlertVC", sb: IDMain) as! MedicineAlertVC
           vc.alert_type = kAlertTypeMC
           self.navigationController?.pushViewController(vc, animated: false)
        }
        if id == "pregnancy" {
         
            let vc = self.getVC(with: "MedicineAlertVC", sb: IDMain) as! MedicineAlertVC
            vc.alert_type = kAlertTypePreg
            self.navigationController?.pushViewController(vc, animated: false)
        }
        //
        if id == "my_pre" {
            let prescVC = self.getVC(with: "PrescriptionListVC", sb: "Main") as! PrescriptionListVC
            prescVC.type = "self"
            self.navigationController?.pushViewController(prescVC, animated: false)
        }
        if id == "fam_pre" {
            let prescVC = self.getVC(with: "PrescriptionListVC", sb: "Main") as! PrescriptionListVC
            prescVC.type = "family"
            self.navigationController?.pushViewController(prescVC, animated: false)
        }
        if id == "my_re" {
            let prescVC = self.getVC(with: "ReportsVC", sb: "Main") as! ReportsVC
            prescVC.type = "self"
            self.navigationController?.pushViewController(prescVC, animated: false)
        }
        if id == "fam_re" {
            let prescVC = self.getVC(with: "ReportsVC", sb: "Main") as! ReportsVC
            prescVC.type = "family"
            self.navigationController?.pushViewController(prescVC, animated: false)
        }
        
        if id == "find_doc" {
            let vc = self.getVC(with: "ParallaxVC", sb: "Main") as! ParallaxVC
            vc.type = "doctor"
            self.navigationController?.pushViewController(vc, animated: false)
        }
        if id == "fav_doc" {
            let docVC = self.getVC(with: "ChemistListVC", sb: "Main") as! ChemistListVC
            docVC.type = "doctor"
            self.navigationController?.pushViewController(docVC, animated: false)
        }
        if id == "find_diag" {
            let vc = self.getVC(with: "ParallaxVC", sb: "Main") as! ParallaxVC
            vc.type = "diagnostic"
            self.navigationController?.pushViewController(vc, animated: false)
        }
        if id == "fav_diag" {
            let docVC = self.getVC(with: "ChemistListVC", sb: "Main") as! ChemistListVC
            docVC.type = "diagnostic"
            self.navigationController?.pushViewController(docVC, animated: false)
        }
        if type == "Service" {
            let vc = self.getVC(with: "ParallaxVC", sb: "Main") as! ParallaxVC
            vc.type = "service"
            vc.specialization = id
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MyAlertsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if type == "Doctor" || type == "Diagnostic" {
            return 1
        }
        if type == "Reports" {
            return 2
        }
        if type == "Alerts" {
            return loggedUser?.gender == "Female" ? 3 : 2
        }
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if type == "Alerts" {
            
            var cell = tableView.dequeueReusableCell(withIdentifier: "alertCell") as! AlertCell
            
            if indexPath.row == 2 {
            
                cell = tableView.dequeueReusableCell(withIdentifier: "alertFemCell") as! AlertCell
                 
            }
            let data = arrCellData[indexPath.row + indexPath.row]
            cell.title1.text = data.title
            if indexPath.row != 2 {
                cell.desc1.text = data.desc
            }
            if let img1 = data.img, !img1.isEmpty {
                cell.img1.image = UIImage(named: img1)
            }
            
            let data1 = arrCellData[indexPath.row + indexPath.row + 1]
            cell.title2.text = data1.title
            if indexPath.row != 2 {
                cell.desc2.text = data1.desc
            }
            if let img2 = data1.img, !img2.isEmpty {
                cell.img2.image = UIImage(named: img2)
            }
            
            if type == "Reports" {
                
                cell.title1.font = UIFont(name: SFProBold, size: 17)
                cell.title2.font = UIFont(name: SFProBold, size: 17)
            }
            
            cell.btn1.accessibilityIdentifier = data.tag
            cell.btn2.accessibilityIdentifier = data1.tag
            
            cell.btn1.addTarget(self, action: #selector(clickAction(_:)), for: .touchUpInside)
            cell.btn2.addTarget(self, action: #selector(clickAction(_:)), for: .touchUpInside)
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "alertCell") as! AlertCell
            
            
            let data = arrCellData[indexPath.row + indexPath.row]
            cell.title1.text = data.title
            cell.desc1.text = data.desc
            if let img1 = data.img, !img1.isEmpty {
                cell.img1.image = UIImage(named: img1)
            }
            
            let data1 = arrCellData[indexPath.row + indexPath.row + 1]
            cell.title2.text = data1.title
            cell.desc2.text = data1.desc
            if let img2 = data1.img, !img2.isEmpty {
                cell.img2.image = UIImage(named: img2)
            }
            
            cell.btn1.accessibilityIdentifier = data.tag
            cell.btn2.accessibilityIdentifier = data1.tag
            
            cell.btn1.addTarget(self, action: #selector(clickAction(_:)), for: .touchUpInside)
            cell.btn2.addTarget(self, action: #selector(clickAction(_:)), for: .touchUpInside)
            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if type == "Alerts" {
            if indexPath.row == 2 {
                /*
                let frame = (cell as! AlertCell).bgView.bounds//self.view.frame
                gradientLayer.frame = frame //CGRect(x: 0, y: 0, width: frame.width-50, height: cell.bgView.frame.height+5)
                (cell as! AlertCell).bgView.layer.insertSublayer(gradientLayer, at: 0)
                (cell as! AlertCell).bgView.layer.cornerRadius = 20
                (cell as! AlertCell).bgView.layer.masksToBounds = true
                
                cell.layoutIfNeeded()*/
                
                let bg1 = cell.contentView.viewWithTag(1) as! UIView
                let bg2 = cell.contentView.viewWithTag(2) as! UIView
                
                bg1.backgroundColor = RGB_gradPink
                bg2.backgroundColor = RGB_gradBlue
            }
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 170
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
}

class AlertCell: BaseCell {
    
    @IBOutlet weak var img1: CircularImg!
    @IBOutlet weak var img2: CircularImg!
    
    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var title2: UILabel!
    
    @IBOutlet weak var desc1: UILabel!
    @IBOutlet weak var desc2: UILabel!
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    
    @IBOutlet weak var view1: RoundedView!
    @IBOutlet weak var view2: RoundedView!
    
    @IBOutlet weak var bgView: UIView!
    
    var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        //gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [RGB_gradPink.cgColor, RGB_gradBlue.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        //gradientLayer.locations = [0.0, 1.1]
       return gradientLayer
    }()
    
    override func awakeFromNib() {
    
        super.awakeFromNib()
        
        if self.reuseIdentifier == "alertFemCell" {
            //self.bgView.addGradientBackground(firstColor: RGB_gradPink, secondColor: RGB_gradBlue)
            
        }
    }
}


extension UIView{
    func addGradientBackground(firstColor: UIColor, secondColor: UIColor){
        clipsToBounds = true
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        print(gradientLayer.frame)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}

class GradientView: UIView {
    
    override func awakeFromNib() {
    
        super.awakeFromNib()
        
        clipsToBounds = true
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [RGB_gradPink.cgColor, RGB_gradBlue.cgColor]
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        print(gradientLayer.frame)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
