//
//  MedicineAlertVC.swift
//  HealthApp
//
//  Created by Apple on 24/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class MedicineAlertVC: BaseVC, MedAlertCellDelegate {

    var user: User?
    var alert_type: String?
    var arrMedAlert: [MedAlertData] = []
    
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var tblData: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let users = self.fetchUser()
        user = users[0]
        tblData.estimatedRowHeight = 140
        
        switch alert_type {
        case kAlertTypeMed:
            lblHead.text = kMedAlert
        case kAlertTypeDoc:
            lblHead.text = kDocAlert
        case kAlertTypeChild:
            lblHead.text = kChildAlert
        case kAlertTypeVac:
            lblHead.text = kVacAlert
        case kAlertTypeMC:
            lblHead.text = kMCAlert
        case kAlertTypePreg:
            lblHead.text = kPregAlert
        case kAlertTypeChild:
            lblHead.text = kChildAlert
        default:
            break
        }
        self.getMedicineAlerts()
    }
    

    @IBAction func addAction(_ sender: UIButton){
        
        if alert_type == kAlertTypeMed {
            let vc = self.getVC(with: "AddMedicineAlertVC", sb: IDMain)
            self.navigationController?.pushViewController(vc, animated: false)
        }
        if alert_type == kAlertTypeDoc {
            let vc = self.getVC(with: "AddDoctorAlertVC", sb: IDMain)
            self.navigationController?.pushViewController(vc, animated: false)
        }
        if alert_type == kAlertTypeVac {
            let vc = self.getVC(with: "AddVaccineAlertVC", sb: IDMain) as! AddVaccineAlertVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
        if alert_type == kAlertTypeMC {
            let vc = self.getVC(with: "AddMCAlertVC", sb: IDMain) as! AddMCAlertVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
        if alert_type == kAlertTypePreg {
            let pregVC = self.getVC(with: "AddPregAlertVC", sb: "Main") as! AddPregAlertVC
            self.navigationController?.pushViewController(pregVC, animated: false)
        }
        if alert_type == kAlertTypeChild {
        let vc = self.getVC(with: "AddGrowthAlertVC", sb: IDMain) as! AddGrowthAlertVC
        //vc.alert_type = kAlertTypeDoc
        self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func getMedicineAlerts() {
        
        IHProgressHUD.show()
        
        let params = ["user_id": user!.id, "alert_type": alert_type] as [String : Any]
        apiManager.getMedicineAlerts(params: params) { (result) in
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    self.arrMedAlert.removeAll()
                    self.arrMedAlert = data
                    
                    if self.alert_type != kAlertTypeVac {
                        var cnt = 0
                        for var alert in self.arrMedAlert {
                            
                            if self.alert_type == kAlertTypeMC || self.alert_type == kAlertTypePreg || self.alert_type == kAlertTypeChild {
                                alert.parseMCData()
                                self.arrMedAlert[cnt].alert_timmings = alert.alert_timmings
                            } else {
                                alert.parseTimeCategory()
                                self.arrMedAlert[cnt].strAlertTime = alert.strAlertTime
                            }
                            cnt = cnt + 1
                        }
                    } else {
                        var cnt = 0
                        for var alert in self.arrMedAlert {
                            do {
                                
                                alert.parseVaccineData()
                                
                                self.arrMedAlert[cnt].alert_timmings = alert.alert_timmings
                                self.arrMedAlert[cnt].child_name = alert.child_name
                                
                                cnt = cnt + 1
                                
                            } catch let err {
                                print(err)
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func onDeleteClicked(id: Int) {
        
        let params = ["id": id]
        IHProgressHUD.show()
        
        apiManager.deleteAlert(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                
            case .success(let data):
                DispatchQueue.main.async {
                    //self.alert(strTitle: strSuccess, strMsg: data.message!)
                    self.getMedicineAlerts()
                }
            case .failure(let err):
                DispatchQueue.main.async {
                    self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MedicineAlertVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if alert_type == kAlertTypeMC || alert_type == kAlertTypePreg {
            return arrMedAlert.count
        }
        if alert_type == kAlertTypeChild {
            return arrMedAlert.count + 1
        }
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if alert_type == kAlertTypeMC || alert_type == kAlertTypePreg {
            let arrTiming = arrMedAlert[section].alert_timmings
            return arrTiming.count
        }
        if alert_type == kAlertTypeChild {
            
            if section == arrMedAlert.count {
                return 1
            } else {
                let arrTiming = arrMedAlert[section].alert_timmings
                return arrTiming.count
            }
        }
        if alert_type == kAlertTypeVac {
            return arrMedAlert.count + (26 * arrMedAlert.count)
        }
        return arrMedAlert.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if alert_type == kAlertTypeMC {
            
            let data = arrMedAlert[indexPath.section]
            let alertTiming = data.alert_timmings[indexPath.row]
            
            if alertTiming.timecategory?.alert_name == mcPrePeriodTitle {
                cell.backgroundColor = RGB_prePeriod
            }
            if alertTiming.timecategory?.alert_name == mcPeriodDayTitle {
                cell.backgroundColor = RGB_Period
            }
            if alertTiming.timecategory?.alert_name == mcOvulTitle {
                cell.backgroundColor = RGB_Ovul
            }
            
            (cell as! MedAlertCell).lblAlertTiming.textColor = .white
            (cell as! MedAlertCell).lblTime.textColor = .white
            
        } else {
            if cell.reuseIdentifier == "vacAlertCell" {
                (cell as! MedAlertCell).daysView.layer.borderColor = UIColor.gray.cgColor
                (cell as! MedAlertCell).timeView.layer.borderColor = UIColor.gray.cgColor
                
                (cell as! MedAlertCell).daysView.layer.borderWidth = 1.0
                (cell as! MedAlertCell).timeView.layer.borderWidth = 1.0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
   
        if alert_type != kAlertTypeVac {
            
            if alert_type == kAlertTypeMC || alert_type == kAlertTypePreg {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "vacAlertCell") as! MedAlertCell
                
                let data = arrMedAlert[indexPath.section]
                let alertTiming = data.alert_timmings[indexPath.row]
                cell.populateMCData(data: alertTiming)
                
                return cell
                
            }
            else if alert_type == kAlertTypeChild {
                
                if indexPath.section == arrMedAlert.count {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "chartCell") as! MedAlertCell
                    
                    return cell
                    
                } else {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "vacAlertCell") as! MedAlertCell
                    
                    let data = arrMedAlert[indexPath.section]
                    let alertTiming = data.alert_timmings[indexPath.row]
                    cell.populateMCData(data: alertTiming)
                    
                    return cell
                }
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "medAlertCell") as! MedAlertCell
                let data = arrMedAlert[indexPath.row]
                cell.populateData(data: data)
                cell.delegate = self
                
                return cell
            }
            
            
        } else {
            if indexPath.row % 27 == 0 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "medAlertCell") as! MedAlertCell
                
                let data = arrMedAlert[arrMedAlert.count-1]
                cell.populateData(data: data)
                cell.delegate = self
                
                return cell
                
            } else {
            //vacAlertCell
                let cell = tableView.dequeueReusableCell(withIdentifier: "vacAlertCell") as! MedAlertCell
                
                let meddata = arrMedAlert[arrMedAlert.count-1]
                let row = (indexPath.row % 27)-1
                let alertTiming = meddata.alert_timmings[row]
                cell.lblAlertTiming.text = alertTiming.timecategory?.alert_name
                cell.lblTime.text = alertTiming.timming
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if alert_type == kAlertTypeVac {
            if indexPath.row == 0 {
                return 100
            }
        }
        if alert_type == kAlertTypeChild {
            if indexPath.section == arrMedAlert.count {
                return 450
            }
        }
        return UITableView.automaticDimension//140
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if alert_type == kAlertTypeMC || alert_type == kAlertTypePreg {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "medAlertCell") as! MedAlertCell
            let data = arrMedAlert[section]
            cell.populateData(data: data)
            
            cell.contentView.backgroundColor = .white
            
            return cell.contentView
        }
        if alert_type == kAlertTypeChild {
            
            if section == arrMedAlert.count {
                
                return nil
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "medAlertCell") as! MedAlertCell
                let data = arrMedAlert[section]
                cell.populateData(data: data)
                
                cell.contentView.backgroundColor = .white
                
                return cell.contentView
            }
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if alert_type == kAlertTypeMC || alert_type == kAlertTypePreg  {
            return 50
        }
        if alert_type == kAlertTypeChild {
            if section == arrMedAlert.count {
                return 0
            }
            return 80
        }
        return 0
    }
}

protocol MedAlertCellDelegate {
    
    func onDeleteClicked(id: Int)
}
class MedAlertCell: BaseCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblAlertTiming: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var daysView: UIView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var btnDelete: UIButton!
    
    var delegate: MedAlertCellDelegate?
    
    func populateData(data: MedAlertData) {
        
        if data.alert_type == kAlertTypeMed {
            
            self.btnDelete.tag = data.id ?? 0
            self.lblName.text = data.medicine_name
            self.lblType.text = data.medicine_unit
            self.lblAlertTiming.text = data.strAlertTime.joined(separator: "")
            let strTime = (data.alert_timmings.map { (alert) -> String in
                if !alert.time_category!.contains("before") &&  !alert.time_category!.contains("after")  {
                    return alert.timming!
                }
                return ""
            }).filter{ (!$0.isEmpty) }.joined(separator: "\n \n")
            self.lblTime.text = strTime
            
        } else if data.alert_type == kAlertTypeDoc {
            
            self.btnDelete.tag = data.id ?? 0
            self.lblName.text = data.medicine_name
            self.lblType.text = ""
            let timing = data.alert_timmings[0]
            self.lblAlertTiming.text = timing.time_category
            self.lblTime.text = timing.timming
            
        } else if data.alert_type == kAlertTypeVac {
        
            self.btnDelete.tag = data.id ?? 0
            self.lblName.text = data.medicine_name
            self.lblType.text = data.child_name
        }
        else if data.alert_type == kAlertTypeMC || data.alert_type == kAlertTypePreg  || data.alert_type == kAlertTypeChild{
        
            self.btnDelete.tag = data.id ?? 0
            self.lblName.text = data.medicine_name
            
            if data.alert_type == kAlertTypeChild {
                self.lblType.text = data.getMemberName()
            }
        }
    }
    
    func populateMCData(data: AlertTiming) {
        
        lblAlertTiming.text = data.timecategory?.alert_name
        lblTime.text = data.timming
        
    }
    @IBAction func deleteAction(_ sender: UIButton){
        
        delegate?.onDeleteClicked(id: sender.tag)
    }
}
