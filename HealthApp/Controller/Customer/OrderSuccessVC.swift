//
//  OrderSuccessVC.swift
//  HealthApp
//
//  Created by Apple on 23/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class OrderSuccessVC: BaseVC {

    var chemistName: String?
    @IBOutlet weak var lblChemist: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationItem.hidesBackButton = true
        
        lblChemist.text = chemistName
        /*
        let menuBtn = (self.tabBarController as! HomeVC).menuBtn
        let image = UIImage(named: "Back")
        let renderImg = image?.withRenderingMode(.alwaysOriginal)
        menuBtn!.image = renderImg
 */
    }
    
    @IBAction func closeAction(_ sender: UIButton){
        
        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: CloseOrderNotification), object: nil)
        
        //self.tabBarController?.selectedIndex = 0
        //self.navigationController?.popToRootViewController(animated: false)
        
        //let dashboardVC = (self.navigationController?.viewControllers.first) as! DashboardVC
        let dashboardVC = self.getVC(with: "DashboardVC", sb: "Main") as! DashboardVC
        let ordersVC = self.getVC(with: "OrdersVC", sb: IDMain) as! OrdersVC
        
        let vcArray = [dashboardVC, ordersVC]//self.navigationController?.viewControllers
        //vcArray!.removeLast()
        //vcArray!.append(ordersVC)
        self.navigationController?.setViewControllers(vcArray, animated: false)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
