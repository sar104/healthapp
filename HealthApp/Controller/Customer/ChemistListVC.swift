//
//  ChemistListVC.swift
//  HealthApp
//
//  Created by Apple on 14/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class ChemistListVC: BaseVC {

    var chemistArr: [ChemistData] = []
    var docArr: [FavDoctor] = []
    var diagnosticArr: [DiagnosticData] = []
    var type: String?
    
    var user: User?
    
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var tblData: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let users = self.fetchUser()
        user = users[0]
        
        // Do any additional setup after loading the view.
        tblData.estimatedRowHeight = 150
        
        switch type {
        case "chemist":
            lblHead.text = ""
            getChemistList()
        case "doctor":
            lblHead.text = "MY FAVORITE DOCTORS"
            getDoctorList()
        case "diagnostic":
            lblHead.text = "MY FAVORITE DIAGNOSTIC CENTERS"
            getDiagnosticList()
        default:
            break
        }
        
    }
    
    func getChemistList() {
        
        //let params = [PARAM_USERID: user?.id, PARAM_LATITUDE: 19.980661,PARAM_LONGITUDE: 73.797836] as [String:Any]
        let params = [PARAM_USERID: user?.id] as [String:Any]
        
        IHProgressHUD.show()
        
        apiManager.searchChemists(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    self.chemistArr = data
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    func getDoctorList() {
        
        let params = [PARAM_USERID: user?.id] as [String:Any]
        
        IHProgressHUD.show()
        
        apiManager.getFavDoctors(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    self.docArr = data
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    //getFavDiagnostic
    func getDiagnosticList() {
        
        let params = [PARAM_USERID: user?.id] as [String:Any]
        
        IHProgressHUD.show()
        
        apiManager.getFavDiagnostic(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    self.diagnosticArr = data
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChemistListVC: UITableViewDataSource, UITableViewDelegate, ChemistCellDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch type {
        case "chemist":
            return self.chemistArr.count
        case "doctor":
            return self.docArr.count
        case "diagnostic":
            return self.diagnosticArr.count
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        switch type {
        case "chemist":
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "chemistCell") as! ChemistCell
            
            let data = self.chemistArr[indexPath.row]
            cell.btnFav.tag = indexPath.row
            cell.btnOrder.tag = indexPath.row
            cell.delegate = self
            cell.populateData(data: data)
            return cell
            
        case "doctor":
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "doctorCell") as! ChemistCell
            
            let data = self.docArr[indexPath.row]
            cell.btnFav.tag = indexPath.row
            cell.btnOrder.tag = indexPath.row
            cell.delegate = self
            cell.populateFavDocData(data: data)
            return cell
            
            
        case "diagnostic":
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "doctorCell") as! ChemistCell
            
            let data = self.diagnosticArr[indexPath.row]
            cell.btnFav.tag = indexPath.row
            cell.btnOrder.tag = indexPath.row
            cell.delegate = self
            cell.populateDiagnosticData(data: data)
            return cell
            
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //ChemistDetailsVC
        
        let vc = self.getVC(with: "ChemistDetailsVC", sb: IDMain) as! ChemistDetailsVC
        if type == "doctor" {
            let data = docArr[indexPath.row]
            vc.type = "favdoctor"
            vc.doctorData = data
            vc.favorite = data.favourite
            vc.id = String(data.id!)
        }
        if type == "diagnostic" {
            let data = diagnosticArr[indexPath.row]
            vc.type = "favdiagnostic"
            vc.diagnosticData = data
            vc.favorite = data.favourite
            vc.id = String(data.id!)
        }
        if type == "chemist" {
            let data = chemistArr[indexPath.row]
            vc.type = "favchemist"
            vc.chemistData = data
            vc.favorite = data.favourite
            vc.id = String(data.id!)
        }
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func onPlaceOrderClicked(id: Int){
     
        if type == "doctor" {
            
            //ShareEHRVC
            let data = self.docArr[id]
            /*
            let vc = self.getVC(with: "ShareEHRVC", sb: IDMain) as! ShareEHRVC
            vc.id = data.id
            self.navigationController?.pushViewController(vc, animated: false)
             */
            
            UserDefaults.standard.set(data.id, forKey: "doctor_id")
            
            let vc = self.getVC(with: "MyAlertsVC", sb: IDMain) as! MyAlertsVC
            vc.type = "Reports"
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
    }
    
    func onFavCliked(id: Int){
    
    }
    
}

protocol ChemistCellDelegate {
    
    func onPlaceOrderClicked(id: Int)
    func onFavCliked(id: Int)
}

class ChemistCell: BaseCell {
    
    @IBOutlet weak var imgView: CircularImg!
    @IBOutlet weak var imgDelivery: UIImageView!
    @IBOutlet weak var starView: CosmosView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblHour: UILabel!
    
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btnOrder: UIButton!
    
    var delegate: ChemistCellDelegate?
    
    func populateData(data: ChemistData){
        
        if let shopimg = data.shop_image, !shopimg.isEmpty, shopimg != "null" {
            
            let imgURL = chemistProfileUrl + shopimg
            //imgView.kf.setImage(with: URL(string: imgURL))
            imgView.kf.setImage(with: URL(string: imgURL), placeholder: UIImage(named: "User"), options: nil, progressBlock: nil) { (result) in
                
                self.imgView.setNeedsDisplay()
            }
        } else {
            imgView.image = UIImage(named: "User")
        }
        self.lblName.text = data.clinic_lab_hospital_name
        self.lblDesc.text = data.name
        self.lblAddress.text = data.address
        self.lblHour.text = data.getShopTiming()
        
        if #available(iOS 13.0, *) {
            self.btnFav.setImage(UIImage(systemName: "heart"), for: .normal)
        } else {
            // Fallback on earlier versions
        }
        
        if let fav = data.favourite, fav {
            if #available(iOS 13.0, *) {
                self.btnFav.setImage(UIImage(systemName: "heart.fill"), for: .normal)
            } else {
                // Fallback on earlier versions
            }
            self.btnFav.tintColor = .red
        }
        
        self.imgDelivery.isHidden = true
        
        if data.delivery_status == "Yes" {
            self.imgDelivery.isHidden = false
        }
        self.starView.rating = data.avg_rating ?? 0
    }
    
    func populateChemistData(data: ChemistData){
        
        if let shopimg = data.shop_image {
            
            let imgURL = chemistProfileUrl + shopimg
            //imgView.kf.setImage(with: URL(string: imgURL))
            imgView.kf.setImage(with: URL(string: imgURL), placeholder: UIImage(named: "User"), options: nil, progressBlock: nil) { (result) in
                
                self.imgView.setNeedsDisplay()
            }
        }
        self.lblName.text = data.clinic_lab_hospital_name
        self.lblDesc.text = data.name
        self.lblAddress.text = data.address
        self.lblHour.text = data.getShopTiming()
        
        
    }
    
    func populateDocData(data: DoctorData){
        
        if let shopimg = data.shop_image {
            
            let imgURL = doctorImgUrl + shopimg
            imgView.kf.setImage(with: URL(string: imgURL))
        }
        self.lblName.text = data.user_name
        //self.lblDesc.text = data.clinic_lab_hospital_name
        //self.lblAddress.text = data.address
        //self.lblHour.text = data.getShopTiming()
        //self.starView.rating = data.avg_rating ?? 0
    }
    func populateFavDocData(data: FavDoctor){
        
        if let shopimg = data.shop_image, !shopimg.isEmpty, shopimg != "null" {
            
            let imgURL = doctorImgUrl + shopimg
            imgView.kf.setImage(with: URL(string: imgURL))
        } else {
            imgView.image = UIImage(named: "ic_doctor1")
        }
        self.lblName.text = data.clinic_lab_hospital_name
        self.lblDesc.text = data.name ?? data.user_name
        
        self.lblAddress.text = data.address
        //self.lblHour.text = String(data.distance ?? 0) + " Km away"
        self.starView.rating = data.avg_rating ?? 0
    }
    func populateDiagnosticData(data: DiagnosticData){
        
        if let shopimg = data.shop_image, !shopimg.isEmpty, shopimg != "null" {
            
            let imgURL = diagnosticUrl + shopimg
            imgView.kf.setImage(with: URL(string: imgURL))
        } else {
            imgView.image = UIImage(named: "User")
        }
        self.lblName.text = data.clinic_lab_hospital_name
        self.lblDesc.text = data.name
        self.lblAddress.text = data.address
        //self.lblHour.text = String(data.distance ?? 0) + " Km away"
        
        self.btnOrder.isHidden = true
        self.btnFav.isHidden = false
        self.starView.rating = data.avg_rating ?? 0
    }
    @IBAction func placeOrderAction(_ sender: UIButton){
        
        delegate?.onPlaceOrderClicked(id: sender.tag)
    }
    
    @IBAction func favoriteAction(_ sender: UIButton){
        
        delegate?.onFavCliked(id: sender.tag)
    }
    
    
}
