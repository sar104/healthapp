//
//  OrderDetailsVC.swift
//  HealthApp
//
//  Created by Apple on 02/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class OrderDetailsVC: BaseVC {

    @IBOutlet weak var tblData: UITableView!
    @IBOutlet weak var btnEdit: UIButton!
    
    var orderData: CustOrderData?
    var arrPM: [OrderDetails] = []
    var arrNPM: [OrderDetails] = []
    var medCellHgt: CGFloat = 20
    var itemCellHgt: CGFloat = 20
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        guard let arrDetails = orderData?.order_details else { return }
        
        arrPM = arrDetails.filter{ ($0.getMedicineData().medicine_type == "PM") }
        arrNPM = arrDetails.filter{ ($0.getMedicineData().medicine_type == "NPM") }
        
        btnEdit.isHidden = true
        
        if orderData?.status != strDeliveryPending && orderData?.status != strDelivered {
            btnEdit.isHidden = false
            //btnEdit.addTarget(self, action: #selector(editAction), for: .touchUpInside)
        }
        tblData.reloadData()
    }
    
    func rejectOrder() {
        
        let notifId = kAlertTypeMed + "_Order"
        LocalNotificationManager().deleteNotifications(notifIds: [notifId])
        
        let params = ["order_id": orderData?.id]
        
        IHProgressHUD.show()
        
        apiManager.cancelOrder(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                
            case .success(let data):
                
                DispatchQueue.main.async {
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                }
                
            case .failure(let err):
                
                DispatchQueue.main.async {
                    self.alert(strTitle: strSuccess, strMsg: err.localizedDescription)
                }
            }
        }
    }
//
    func confirmOrder() {
           
        let notifId = kAlertTypeMed + "_Order"
        LocalNotificationManager().deleteNotifications(notifIds: [notifId])
        
           let params = ["order_id": orderData?.id]
        
            IHProgressHUD.show()
        
           apiManager.acceptOrder(params: params) { (result) in
               
            IHProgressHUD.dismiss()
            
               switch result {
                   
               case .success(let data):
                   
                   DispatchQueue.main.async {
                       self.alert(strTitle: strSuccess, strMsg: data.message!)
                   }
                   
               case .failure(let err):
                   
                   DispatchQueue.main.async {
                       self.alert(strTitle: strSuccess, strMsg: err.localizedDescription)
                   }
               }
           }
       }
    
    @IBAction func editAction(_ sender: UIButton) {
        
        //UpdateOrderVC
        
        let vc = self.getVC(with: "UpdateOrderVC", sb: IDMain) as! UpdateOrderVC
        
        vc.orderData = orderData
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func confirmRejectAction(_ sender: UIButton) {
        
        if sender.tag == 0 {
            
            self.confirmOrder()
            
        } else {
            
            self.alertAction(strTitle: "", strMsg: "Do you really want to cancel the order?") { (action) in
                
                if action.title == "Yes" {
                    self.rejectOrder()
                }
            }
        }
    }
    
    @IBAction func reOrderAction(_ sender: UIButton) {
        
        addOrder()
    }
    
    func addOrder() {
           
           IHProgressHUD.show()
           
           var drugArr: [DrugData] = []
           
        do {
            let arrDrugs = NSMutableArray.init()
            
            if arrPM.count > 0 {
                
                for med in arrPM {
                    
                    var  drugdata = DrugData()
                    
                    drugdata.medicine_name = med.getMedicineData()
                    drugdata.unit = med.unit!
                    drugdata.quantity = med.quantity
                    drugArr.append(drugdata)
                    
                }
                print("drugs: \(arrDrugs)")
            }
            if arrNPM.count > 0 {
                
                for med in arrNPM {
                    
                    var  drugdata = DrugData()
                    
                    drugdata.medicine_name = med.getMedicineData()
                    drugdata.unit = med.unit!
                    drugdata.quantity = med.quantity
                    drugArr.append(drugdata)
                }
                print("drugs: \(arrDrugs)")
            }
            
            let drugJSonData = try JSONEncoder().encode(drugArr)
            let jsonStrDrugs = String(data: drugJSonData, encoding: .utf8)
            
            var params:[String:String] = [:]
            
            params["customer_address"] = self.orderData?.customer_address ?? ""
            params["delivery_type"] = self.orderData?.delivery_type ?? ""
            if let chemistId = self.orderData?.chemist_id {
                params["chemist_id"] = String(chemistId)
            } else {
                params["chemist_id"] = "0"
            }
            params["prescription_type"] = self.orderData?.prescription_type ?? ""
            params["slot"] = self.orderData?.slot ?? ""
            params["estimated_date"] = self.orderData?.estimated_date ?? ""
            params["description"] = self.orderData?.description
            params["doctor_name"] = self.orderData?.doctor_name ?? "-"
            params["address_id"] = "0"
            if let pincode = self.orderData?.pincode {
                params["pincode"] = String(pincode)
            } else {
                params["pincode"] = ""
            }
            params["prescription_image"] = self.orderData?.prescription_image ?? "NA"
            params["user_id"] = String(loggedUser!.id)
            params["flat_house_no"] = self.orderData?.flat_house_no
            
            params["drug_list"] = jsonStrDrugs
            
            let dataRec = try JSONEncoder().encode(self.orderData?.recipient_name)
            let paramsRec =  String(data: dataRec, encoding: .utf8)
            
            
            params["recipient_name"] = paramsRec
            print("params: \(params)")
            
            
            apiManager.AddOrderForCustomer(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                case .success(let data):
                    //self.alert(strTitle: strSuccess, strMsg: data.message!)
                    if data.status {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.navOrders()
                        }
                    }
                    
                case .failure(let err):
                    print("err: \(err)")
                    self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
            
        } catch let err {
            print(err)
        }
           
           
       }
    
    func navOrders() {
        
       
        let dashboardVC = self.getVC(with: "DashboardVC", sb: "Main") as! DashboardVC
        let ordersVC = self.getVC(with: "OrdersVC", sb: IDMain) as! OrdersVC
        
        let vcArray = [dashboardVC, ordersVC]
        self.navigationController?.setViewControllers(vcArray, animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OrderDetailsVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 5
        }
        if section == 1 {
            return 1//arrPM.count
        }
        if section == 2 {
            return 1//arrNPM.count
        }
        if section == 3 {
            return 7
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = OrderDetailsCell()
        
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                
                cell = tableView.dequeueReusableCell(withIdentifier: "labelCell") as! OrderDetailsCell
                
                let strdata = "ORDER #" + String(orderData!.id!)
                cell.lblHead.attributedText = strdata.setAttributes(str1: "ORDER #", str2: String(orderData!.id!), color1: .black, color2: .black, font1: SFProRegular, font2: SFProBold)
                cell.lblMedName.text = ""
                //cell.btnEdit.addTarget(self, action: #selector(editAction), for: .touchUpInside)
                //cell.btnEdit.isHidden = true
                
                if orderData?.status != strDeliveryPending && orderData?.status != strDelivered {
                    //cell.btnEdit.isHidden = false
                    //cell.btnEdit.addTarget(self, action: #selector(editAction), for: .touchUpInside)
                }
                return cell
                
            case 1:
                
                cell = tableView.dequeueReusableCell(withIdentifier: "labelCell") as! OrderDetailsCell
                
                cell.lblHead.text = orderData?.chemist_shop_name
                cell.lblHead.textColor = .black
                
                cell.lblHead.font = UIFont.boldSystemFont(ofSize: 17)
                cell.lblMedName.text = orderData?.chemist
                
                return cell
                
            case 2:
                
                cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! OrderDetailsCell
                
                let lbl1 = cell.viewWithTag(1) as? UILabel
                lbl1?.text = "ORDER TYPE"
                
                let lbl2 = cell.viewWithTag(2) as? UILabel
                lbl2?.text = orderData?.prescription_type
                
                let lbl3 = cell.viewWithTag(3) as? UILabel
                lbl3?.text = "ORDER DATE"
                
                let lbl4 = cell.viewWithTag(4) as? UILabel
                lbl4?.text = orderData?.estimated_date
                
                let lbl5 = cell.viewWithTag(5) as? UILabel
                lbl5?.text = ""
                
                return cell
                
            case 3:
                
                cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! OrderDetailsCell
                
                let lbl1 = cell.viewWithTag(1) as? UILabel
                lbl1?.text = "DOCTOR NAME"
                
                let lbl2 = cell.viewWithTag(2) as? UILabel
                lbl2?.text = orderData?.doctor_name
                
                return cell
                
            case 4:
                
                cell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! OrderDetailsCell
                
                if let strurl = orderData?.prescription_image, let url = URL(string: prescriptionImageURL + strurl) {
                    cell.imgview.kf.setImage(with: url)
                }
                return cell
                
            default:
                break
            }
        }
            if indexPath.section == 1 {
                cell = tableView.dequeueReusableCell(withIdentifier: "medListCell") as! OrderDetailsCell
                /*
                let data = arrPM[indexPath.row]
                cell.lblHead.text = (data.getMedicineData().medicine_name)! + "(" + data.unit! + ")"
                cell.lblMedName.text = String(data.quantity!)
                */
                cell.bgView.layer.borderColor = UIColor.gray.cgColor
                cell.bgView.layer.borderWidth = 1.0
                cell.bgView.layer.cornerRadius = 5
                cell.bgView.layer.masksToBounds = false
                
                cell.arrMed = arrPM
                cell.tblMed.separatorStyle = .none
                cell.tblMed.reloadData()
                cell.con_tblHgt.constant = cell.tblMed.contentSize.height
                medCellHgt = cell.tblMed.contentSize.height
                print("medCellHgt: \(medCellHgt)")
                
                return cell
            }
            if indexPath.section == 2 {
                cell = tableView.dequeueReusableCell(withIdentifier: "medListCell") as! OrderDetailsCell
                /*
                let data = arrNPM[indexPath.row]
                cell.lblHead.text = (data.getMedicineData().medicine_name)! + "(" + data.unit! + ")"
                cell.lblMedName.text = String(data.quantity!)
                */
                cell.bgView.layer.borderColor = UIColor.gray.cgColor
                cell.bgView.layer.borderWidth = 1.0
                cell.bgView.layer.cornerRadius = 5
                cell.bgView.layer.masksToBounds = false
                
                cell.arrMed = arrNPM
                cell.tblMed.separatorStyle = .none
                cell.tblMed.reloadData()
                cell.con_tblHgt.constant = cell.tblMed.contentSize.height
                itemCellHgt = cell.tblMed.contentSize.height
                print("itemCellHgt: \(itemCellHgt)")
                
                return cell
            }
            if indexPath.section == 3 {
                
                switch indexPath.row {
                case 0:
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! OrderDetailsCell
                    
                    let lbl1 = cell.viewWithTag(1) as? UILabel
                    lbl1?.font = UIFont.systemFont(ofSize: 17)
                    lbl1?.text = "ORDER INSTRUCTIONS"
                    
                    let lbl2 = cell.viewWithTag(2) as? UILabel
                    lbl2?.text = orderData?.description
                    
                    return cell
                    
                case 1:
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! OrderDetailsCell
                    
                    let lbl1 = cell.viewWithTag(1) as? UILabel
                    lbl1?.text = "DELIVERY/PICKUP"
                    
                    let lbl2 = cell.viewWithTag(2) as? UILabel
                    lbl2?.text = orderData?.delivery_type
                    
                    let lbl3 = cell.viewWithTag(3) as? UILabel
                    lbl3?.text = "ORDER STATUS"
                    
                    let lbl4 = cell.viewWithTag(4) as? UILabel
                    //lbl4?.text = orderData?.status
                    if orderData?.delivery_type == "Pickup" {
                        if orderData?.status == "Delivery Pending" {
                            lbl4?.text = "Pickup Pending"//data.status
                        } else if orderData?.status == "Delivered" {
                            lbl4?.text = "Picked Up"
                        } else {
                            lbl4?.text = orderData?.status
                        }
                    } else {
                        lbl4?.text = orderData?.status
                    }
                    
                    let lbl5 = cell.viewWithTag(5) as? UILabel
                    lbl5?.text = "Delivery Details"
                    lbl5?.font = UIFont.boldSystemFont(ofSize: 17)
                    
                    return cell
                    
                case 2:
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "labelCell") as! OrderDetailsCell
                    
                    cell.lblHead.textColor = UIColor.gray
                    cell.lblHead.text = "RECIPIENT NAME"
                    cell.lblMedName.text = orderData?.getRecepient()
                    
                    return cell
                    
                case 3:
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "labelCell") as! OrderDetailsCell
                    
                    cell.lblHead.text = "DELIVERY ADDRESS"
                    cell.lblMedName.text = orderData?.getDeliveryAddress()
                    cell.lblMedName.font = UIFont.boldSystemFont(ofSize: 13)
                    
                    return cell
                    
                case 4:
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "deliveryDetailCell") as! OrderDetailsCell
                    
                    let lbl1 = cell.viewWithTag(1) as? UILabel
                    lbl1?.text = "REQUESTED ON"
                    
                    let lbl2 = cell.viewWithTag(2) as? UILabel
                    lbl2?.text = orderData?.estimated_date
                    
                    let lbl3 = cell.viewWithTag(3) as? UILabel
                    lbl3?.text = "TIME"
                    
                    let lbl4 = cell.viewWithTag(4) as? UILabel
                    lbl4?.text = orderData?.slot
                    
                    let lbl5 = cell.viewWithTag(5) as? UILabel
                    lbl5?.text = "Delivery Date & Time"
                    lbl5?.font = UIFont.boldSystemFont(ofSize: 17)
                    
                case 5:
                    if orderData?.status == strCancelled || orderData?.status == strRejected {
                        
                        cell = tableView.dequeueReusableCell(withIdentifier: "reasonCell") as! OrderDetailsCell
                        
                        let lbl1 = cell.viewWithTag(1) as? UILabel
                        lbl1?.text = "ORDER REJECTED/CANCELLED FOR"
                        
                        let lbl2 = cell.viewWithTag(2) as? UILabel
                        lbl2?.text = orderData?.reject_reason
                        
                        let lbl3 = cell.viewWithTag(3) as? UILabel
                        lbl3?.text = "ORDER REJECTED/CANCELLED BY"
                        
                        let lbl4 = cell.viewWithTag(4) as? UILabel
                        if orderData?.status == strCancelled {
                            lbl4?.text = orderData?.user_name
                        }
                        if orderData?.status == strRejected {
                            lbl4?.text = orderData?.chemist
                        }
                        
                        return cell
                        
                    } else {
                        cell = tableView.dequeueReusableCell(withIdentifier: "amtCell") as! OrderDetailsCell
                        
                        let lbl1 = cell.viewWithTag(1) as? UILabel
                        lbl1?.text = "BILL AMOUNT"
                        
                        let lbl2 = cell.viewWithTag(2) as? UILabel
                        if let amt = orderData?.estimated_cost {
                            lbl2?.text = String(amt)
                        }
                        
                        return cell
                    }
                    
                case 6:
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "btnCell") as! OrderDetailsCell
                    
                    cell.btnConfirm.tag = 0
                    cell.btnReject.tag = 1
                    cell.btnConfirm.isHidden = true
                    cell.btnReject.isHidden = true
                    
                    if orderData?.status == strApprovalPending {
                        
                        cell.btnConfirm.isHidden = false
                        cell.btnReject.isHidden = false
                        cell.btnConfirm.setTitle("Accept Order", for: .normal)
                        cell.btnReject.setTitle("Reject Order", for: .normal)
                        cell.btnReject.backgroundColor = RGB_btnReject
                        
                        cell.btnConfirm.addTarget(self, action: #selector(confirmRejectAction(_:)), for: .touchUpInside)
                        cell.btnReject.addTarget(self, action: #selector(confirmRejectAction), for: .touchUpInside)
                    }
                    if orderData?.status == strPending {
                        cell = tableView.dequeueReusableCell(withIdentifier: "reOrderCell") as! OrderDetailsCell
                        
                        cell.btnReject.isHidden = false
                        cell.btnReject.setTitle("Reject Order", for: .normal)
                        cell.btnReject.backgroundColor = RGB_btnReject
                        cell.btnReject.addTarget(self, action: #selector(confirmRejectAction), for: .touchUpInside)
                        cell.btnReject.backgroundColor = UIColor(red: 136.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1)
                        
                        return cell
                    }
                    if orderData?.status == strDelivered ||  orderData?.status == strCancelled || orderData?.status == strRejected {
                        cell = tableView.dequeueReusableCell(withIdentifier: "reOrderCell") as! OrderDetailsCell
                        
                        cell.btnReject.isHidden = false
                        cell.btnReject.setTitle("Re Order", for: .normal)
                        cell.btnReject.backgroundColor = RGB_btnReOrder
                        cell.btnReject.addTarget(self, action: #selector(reOrderAction), for: .touchUpInside)
                        return cell
                    }
                    
                default:
                    break
                }
            }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.selectionStyle = .none
        
        if indexPath.section == 0 {
            if indexPath.row == 4 {
                (cell as! OrderDetailsCell).bgView.setDashBorder(borderColor: UIColor.gray)
            }
        }
        if indexPath.section == 1 || indexPath.section == 2 {
            
           // (cell as! OrderDetailsCell).lblHead.textColor = .black
        }
        /*
        if cell.reuseIdentifier == "deliveryDetailCell" {
            
            let view1 = cell.contentView.viewWithTag(6)
            let view2 = cell.contentView.viewWithTag(7)
            
            view1?.layer.borderWidth = 1.0
            view1?.layer.borderColor = UIColor.gray.cgColor
            
            view2?.layer.borderWidth = 1.0
            view2?.layer.borderColor = UIColor.gray.cgColor
        }
        if cell.reuseIdentifier == "detailCell" {
            
            let view1 = cell.contentView.viewWithTag(3)
            
            view1?.layer.borderWidth = 1.0
            view1?.layer.borderColor = UIColor.gray.cgColor
        }
 */
    }
    
    /*
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 1 {
            return "MEDICINE DETAILS"
        }
        if section == 2 {
            return "ITEM DETAILS"
        }
        return ""
    }*/
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "headCell") as! OrderDetailsCell
        
        var title = ""
        
        if section == 1 {
             title = "MEDICINE DETAILS"
        }
        if section == 2 {
            title = "ITEM DETAILS"
        }
        
        cell.lblHead.text = title
        
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return (section == 1 || section == 2)  ? 40 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 1 || indexPath.section == 2 {
            
            return (indexPath.section == 1) ? medCellHgt : itemCellHgt //UITableView.automaticDimension
        }
        if indexPath.row == 0 {
            return 50
        }
        if indexPath.section == 0 && indexPath.row == 4 {
            return 170
        }
        if indexPath.section == 0 && indexPath.row == 2 {
            return 80
        }
        
        if indexPath.section == 3 {
            if indexPath.row == 1 || indexPath.row == 4  {
                return 120
            }
            if indexPath.row == 3 {
                return orderData?.delivery_type == "Delivery" ? 110 : 0
            }
            if indexPath.row == 5 {
                if orderData?.status == strCancelled || orderData?.status == strRejected {
                    return 120
                } else {
                    return orderData?.estimated_cost == nil ? 0 : 70
                }
            }
            if indexPath.row == 6 {
                //return orderData?.status == strApprovalPending ? 70 : 0
            }
        }
        return 70
    }
    
}

class OrderDetailsCell: BaseCell, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var lblMedName: UILabel!
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var lblCnt: UILabel!
    @IBOutlet weak var imgview: UIImageView!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var tblMed: MedTableView!
    @IBOutlet weak var con_tblHgt: NSLayoutConstraint!
    
    var arrMed: [OrderDetails] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "medCell") as! OrderDetailsCell
        
        let data = arrMed[indexPath.row]
        cell.lblHead.text = (data.getMedicineData().medicine_name)!
        cell.lblMedName.text =  "(" + data.unit! + ")"
        cell.lblCnt.text = String(data.quantity!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
}
