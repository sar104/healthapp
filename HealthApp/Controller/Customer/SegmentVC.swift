//
//  SegmentVC.swift
//  HealthApp
//
//  Created by Apple on 22/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class SegmentVC: UIViewController {

    @IBOutlet weak var segmentedControl: MXSegmentedControl!
    @IBOutlet weak var con_logoW: NSLayoutConstraint!
    @IBOutlet weak var con_segTop: NSLayoutConstraint!
    
    @IBOutlet weak var con_txtHgt1: NSLayoutConstraint!
    @IBOutlet weak var con_txtHgt2: NSLayoutConstraint!
    @IBOutlet weak var con_txtHgt3: NSLayoutConstraint!
    @IBOutlet weak var con_txtHgt4: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        /*
        for family: String in UIFont.familyNames
        {
            print(family)
            for names: String in UIFont.fontNames(forFamilyName: family)
            {
                print("== \(names)")
            }
        }
        */
        
        setup()
    }
    
    func setup() {
        
        print("device model: \(UIDevice.modelName)")
       
        let device = ""
        
        switch UIDevice.modelName {
            
        case device + "iPhone 5", device + "iPhone 5c", device + "iPhone SE", device + "iPhone 5s":
            con_logoW.constant = 150
            con_segTop.constant = 150
            
        default:
            break
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        segmentedControl.removeAll()
        
        segmentedControl.append(title: "SIGNUP")
            .set(titleColor: .black, for: .selected)
        segmentedControl.append(title: "LOGIN")
        .set(titleColor: .black, for: .selected)
        
        segmentedControl.select(index: 0, animated: false)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
