//
//  OTPVC.swift
//  HealthApp
//
//  Created by Apple on 23/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class OTPVC: BaseVC {

    var mobile: String?
    
    @IBOutlet weak var txt1: UITextField!
    @IBOutlet weak var txt2: UITextField!
    @IBOutlet weak var txt3: UITextField!
    @IBOutlet weak var txt4: UITextField!
    @IBOutlet weak var txt5: UITextField!
    @IBOutlet weak var txt6: UITextField!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var con_lead: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setup(){
        
        let device = ""
        
        switch UIDevice.modelName {
            
        case device + "iPhone 5", device + "iPhone 5c", device + "iPhone SE", device + "iPhone 5s":
            con_lead.constant = 15
            
        default:
            break
        }
        
        txt1.inputAccessoryView = self.addDoneButton(dontTxt: strNext)
        txt2.inputAccessoryView = self.addDoneButton(dontTxt: strNext)
        txt3.inputAccessoryView = self.addDoneButton(dontTxt: strNext)
        txt4.inputAccessoryView = self.addDoneButton(dontTxt: strNext)
        txt5.inputAccessoryView = self.addDoneButton(dontTxt: strNext)
        txt6.inputAccessoryView = self.addDoneButton(dontTxt: strDone)
        
        txt1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txt2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txt3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txt4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txt5.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txt6.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        
        lblTitle.attributedText = lblOTP.setAttributes(str1: lblOTP, str2: mobile!, color1: RGB_82, color2: UIColor.black, font1: SFProRegular, font2: SFProBold)
    }
    
    override func donePicker (sender: UIBarButtonItem) {
        
        if txt1.isFirstResponder {
            //txt1.resignFirstResponder()
            txt2.becomeFirstResponder()
        } else if txt2.isFirstResponder {
            //txt2.resignFirstResponder()
            txt3.becomeFirstResponder()
        } else if  txt3.isFirstResponder {
            //txt3.resignFirstResponder()
            txt4.becomeFirstResponder()
        } else if  txt4.isFirstResponder {
            //txt4.resignFirstResponder()
            txt5.becomeFirstResponder()
        } else if  txt5.isFirstResponder {
            //txt4.resignFirstResponder()
            txt6.becomeFirstResponder()
        } else if txt6.isFirstResponder {
            txt6.resignFirstResponder()
        }
    }
    
    @IBAction func backAction() {
        
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitAction() {
        
        if validateForm() {
            verifyOTP()
        }
        //self.dismiss(animated: true, completion: nil)
    }
    
    func validateForm() -> Bool {
        
        var valid = true
        if let digit1 = txt1.text, digit1.isEmpty  {
            
            valid = false
            
        } else if let digit2 = txt2.text, digit2.isEmpty {
            valid = false
        } else if let digit3 = txt3.text, digit3.isEmpty {
            valid = false
        } else if let digit4 = txt4.text, digit4.isEmpty {
            valid = false
        } else if let digit5 = txt5.text, digit5.isEmpty {
            valid = false
        } else if let digit6 = txt6.text, digit6.isEmpty {
            valid = false
        }
        if !valid {
            self.alert(strTitle: strErr, strMsg: err_digits)
            
        }
        return valid
    }
    
    func verifyOTP(){
        /*username:9766453326
        otp:285671
        fcm_token:sfsdfgjsdfsdf
        latitude:19.06514651
        longitude:20.16546546**/
        
        var otp = txt1.text!
        otp.append(txt2.text!)
        otp.append(txt3.text!)
        otp.append(txt4.text!)
        otp.append(txt5.text!)
        otp.append(txt6.text!)
        
        let lat = defaults.value(forKey: kUserLat)
        let lng = defaults.value(forKey: kUserLong)
        let fcmtoken = defaults.value(forKey: "deviceId") as! String
        
        let params = [PARAM_USERNAME: mobile!,
            PARAM_OTP: otp,
            PARAM_FCMTOKEN: fcmtoken.isEmpty ? "abcdefg" : fcmtoken,
        PARAM_LATITUDE: lat ?? 0.0,
        PARAM_LONGITUDE: lng ?? 0.0] as [String:Any]
        
        print(params)
        IHProgressHUD.show(withStatus: "Loading")
        apiManager.verifyOTPAPI(params: params) { (result) in
                 
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                print("data: \(data)")
                
                do {
                    let userData = try JSONDecoder().decode(UserData.self, from: data)
                    let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
                    let user = jsonObj["data"] as! [String:Any]
                    if let role = jsonObj["role"] as? String {
                        UserDefaults.standard.set(role, forKey: "role")
                    }
                    
                    //UserDefaults.standard.set(1, forKey: "newuser")
                    
                    if userData.status {
                        self.alert(strTitle: strSuccess, strMsg: userData.message!)
                        self.coreDataManager.syncUser(json: [user]) { (res) in
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)  {
                                //let newuser = UserDefaults.standard.value(forKey: "newuser") as? Int
                                
                                var profile_completed = 0
                                if let val = jsonObj["profile_completed"] as? String {
                                    UserDefaults.standard.set(val, forKey: "profile_completed")
                                    profile_completed = Int(val)!
                                }
                                if profile_completed != 1 {
                                    self.navToUpdateProfile()
                                } else {
                                    NotificationCenter.default.post(.init(name: Notification.Name(rawValue: LoginDidNotification)))
                                    
                                }
                            }
                        }
                        
                    }else {
                        self.alert(strTitle: strErr, strMsg: userData.message!)
                    }
                    
                } catch let err {
                    print("err: \(err)")
                }
                
                
            case .failure(let err):
                print("err: \(err)")
                self.alert(strTitle: strErr, strMsg: err.localizedDescription)
            }
            
        }
        /*
         "alt_contact_no" = "<null>";
         "assistant_name" = "<null>";
         data =     {
             "address_id" = "<null>";
             city = null;
             "clinic_lab_hospital_name" = "<null>";
             "delivery_status" = "<null>";
             email = null;
             "gst_no" = "<null>";
             id = 496;
             "mobile_no" = 9049231199;
             name = "Krishna Chemist";
             "parent_id" = "<null>";
             "registration_no" = "<null>";
             "shop_image" = "<null>";
             state = null;
         };
         message = "Login successfully.";
         "profile_completed" = 1;
         role = 3;
         status = 1;
         **/
    }
    
    func fetchUser(){
        let user = self.coreDataManager.fetchLocalUsers()
        print(user)
    }
    
    @IBAction func resendAction(){
        
        resendOTP()
    }
    
    func resendOTP(){
        
        let params = [PARAM_USERNAME: mobile!] as [String:Any]
        
        print(params)
        IHProgressHUD.show(withStatus: "Loading")
        
        apiManager.loginAPI(params: params) { (result) in
                 
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                print("data: \(data)")
                if data.status {
                    self.alert(strTitle: strSuccess, strMsg: data.message!)
                } else {
                    self.alert(strTitle: strErr, strMsg: data.message!)
                }
                
            case .failure(let err):
                print("err: \(err)")
                self.alert(strTitle: strErr, strMsg: err.localizedDescription)
            }
            
        }
    }
    func navToUpdateProfile(){
        
        //UpdateProfile
        let vc: UpdateProfileVC = self.getVC(with: IDUpdateProfile, sb: IDMain) as! UpdateProfileVC
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension OTPVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currText = textField.text ?? ""
        
        guard let strRange = Range(range, in: currText) else {return false}
        
        let updatedTxt = currText.replacingCharacters(in: strRange, with: string)
        /*
        if updatedTxt.count > 1 {
            
            if txt1.isFirstResponder {
                txt2.becomeFirstResponder()
            } else if txt2.isFirstResponder {
                txt3.becomeFirstResponder()
            } else if  txt3.isFirstResponder {
                txt4.becomeFirstResponder()
            } else if  txt4.isFirstResponder {
                txt5.becomeFirstResponder()
            } else if txt5.isFirstResponder {
                txt5.resignFirstResponder()
            }
        }*/
        return updatedTxt.count <= 1
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        
        let text = textField.text ?? ""
        
        if text.count == 1 {
            
            switch textField {
            case txt1:
                txt2.becomeFirstResponder()
            case txt2:
                txt3.becomeFirstResponder()
            case txt3:
                txt4.becomeFirstResponder()
            case txt4:
                txt5.becomeFirstResponder()
            case txt5:
                txt6.becomeFirstResponder()
            case txt6:
                txt6.resignFirstResponder()
            default:
                break
            }
        }
        if text.count == 0 {
            
            switch textField {
            case txt1: break
                //txt1.resignFirstResponder()
            case txt2:
                txt1.becomeFirstResponder()
            case txt3:
                txt2.becomeFirstResponder()
            case txt4:
                txt3.becomeFirstResponder()
            case txt5:
                txt4.becomeFirstResponder()
            case txt6:
                txt5.becomeFirstResponder()
            default:
                break
            }
        }
    }
}
