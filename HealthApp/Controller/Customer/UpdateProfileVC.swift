//
//  UpdateProfileVC.swift
//  HealthApp
//
//  Created by Apple on 27/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import RadioGroup
import IHProgressHUD
import Alamofire
import Kingfisher

class UpdateProfileVC: BaseVC, SwiftAlertViewDelegate {

    
    @IBOutlet weak var tblData: UITableView!
    var addMedAlertView: SwiftAlertView!
    
    var bloodDonarFlag: Int = 1
    var statesArr:[State] = []
    var cityArr:[City] = []
    var professionArr: [ProfessionData] = []
    var interestArr: [InterestData] = []
    var selInterestArr: [InterestData] = []
    var selState: String = ""
    var selCity: String = ""
    var pincode: String = ""
    var name: String = ""
    var user: User?
    var selPrefix: String = ""
    var profileImgData: Data?
    let picker = UIImagePickerController()
    var profile_image: String?
    var selFt: Int?
    var selInch: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
        self.tblData.rowHeight = UITableView.automaticDimension
        
        
    }
    
    func setup(){
        //let coredataService = CoreDataService(stack: CoreDataStack(model: "HealthApp"))
        
        let users = self.fetchUser()
        if users.count > 0 {
            user = users[0]
            if let profile_completed = UserDefaults.standard.value(forKey: "profile_completed") as? String {
                user?.profile_completed = profile_completed
            }
            print("user:\(user)")
            
            
        }
        selState = user?.state ?? ""
        selCity = user?.city ?? ""
        bloodDonarFlag = (user?.is_blood_donor == nil || user!.is_blood_donor!.isEmpty || user?.is_blood_donor == "No") ? 1:0
        
        if let username = user?.name{
            
            /*
            let strname = username.components(separatedBy: " ")
            
            if strname.count > 1 {
                self.selPrefix = strname[0]
                self.name = strname[1]
            } else {
                self.name = username
            }*/
            self.name = username
            
        } else {
            self.name = user?.name ?? ""
        }
        self.user?.height_unit = (self.user?.height_unit == "null") ?  "Feet" : self.user?.height_unit
        self.user?.weight_unit = (self.user?.weight_unit == "null") ?  "Kg" : self.user?.weight_unit
        
        getInterests()
        getProfessionAPI()
        fetchStates()
    }
    
    func getInterests() {
        
        IHProgressHUD.show()
        
        apiManager.getRequestData(api: InterestAPI, params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            
            switch result {
                case .success(let data):
                    
                    do {
                        let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                        
                        let jsonmed = jsondata["data"]
                        
                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                        
                        let json = try JSONDecoder().decode([InterestData].self, from: meddata)
                        self.interestArr.removeAll()
                        self.interestArr = json
                        
                        if let interests = self.user?.interest {
                            
                            let interestArr = interests.components(separatedBy: ",")
                            let arr = self.interestArr.filter{ (interestArr.contains($0.interest!) ) }
                            self.selInterestArr.append(contentsOf: arr)
                        }
                        
                    } catch let err {
                        print(err)
                    }
                    
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func fetchStates(){
        
        IHProgressHUD.show()
        apiManager.getStates(params: [:]) { (result) in
         
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                
                do {
                    let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
                    let status = jsonObj["status"] as! Int
                    if status == 1 {
                        if let states = jsonObj["data"]{
                            let statesData = try JSONSerialization.data(withJSONObject: states, options: .prettyPrinted)
                            let json = try JSONDecoder().decode([State].self, from: statesData)
                            self.statesArr = json
                            print("states: \(json)")
                        }
                    } else {
                        
                    }
                } catch let err {
                    print("err: \(err)")
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
            
        }
    }
    func fetchCities(){
        
        IHProgressHUD.show()
        let params = ["state": selState]
        apiManager.getCities(params: params) { (result) in
         
            IHProgressHUD.dismiss()
            switch result {
            case .success(let data):
                
                do {
                    let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
                    let status = jsonObj["status"] as! Int
                    if status == 1 {
                        if let states = jsonObj["data"]{
                            let statesData = try JSONSerialization.data(withJSONObject: states, options: .prettyPrinted)
                            let json = try JSONDecoder().decode([City].self, from: statesData)
                            self.cityArr = json
                            print("cities: \(json)")
                            DispatchQueue.main.async {
                                self.tblData.reloadData()
                            }
                        }
                    } else {
                        
                    }
                } catch let err {
                    print("err: \(err)")
                }
                
            case .failure(let err):
                print("err: \(err)")
            }
            
        }
    }
    
    func fetchDetailsByPincode(pincode: String){
        
        IHProgressHUD.show()
        let params = ["pincode": pincode]
        apiManager.getDetailsByPincode(params: params) { (result) in
        
           IHProgressHUD.dismiss()
           switch result {
            
           case .success(let data):
            
            do {
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:AnyObject]
                let status = jsonObj["status"] as! Int
                if status == 1 {
                    if let states = jsonObj["data"]{
                        let statesData = try JSONSerialization.data(withJSONObject: states, options: .prettyPrinted)
                        let json = try JSONDecoder().decode([PincodeData].self, from: statesData)
                        if json.count > 0 {
                        if let state = json[0].state {
                            self.selState = state
                            self.user?.state = state
                        }
                        if let city = json[0].sub_district {
                            self.selCity = city
                            self.user?.city = city
                        }
                        }
                        print("cities: \(json)")
                        DispatchQueue.main.async {
                            self.tblData.reloadData()
                        }
                    }
                } else {
                    
                }
            } catch let err {
                print("err: \(err)")
            }
           case .failure(let err):
           print("err: \(err)")
            
            }
        }
    }
    
    func getProfessionAPI(){
        
        IHProgressHUD.show()
        apiManager.getProfession(params: [:]) { (result) in
            
            IHProgressHUD.dismiss()
            switch result {
                case .success(let profData):
                    
                    self.professionArr = profData
                    DispatchQueue.main.async {
                        self.tblData.reloadData()
                }
                case .failure(let err):
                print("err: \(err)")
            }
        }
    }
    
    func registerUser(){
        
        /*
         name:Mayuri K
         gender:Female
         dob:1994-12-17
         email:mk@gmail.com
         profession:2
         bloodgroup:A+
         state:MAHARASTRA
         city:NASHIK
         height:5
         weight:52
         is_blood_donor:Yes
         height_unit:cm
         weight_unit:kg
         */
        IHProgressHUD.show()
        
        do {
            
            if self.user?.height_unit == "Feet" {
                if let feet = selFt, let inch = selInch {
                    
                    let height = "\(feet):\(inch)"
                    self.user?.height = height
                }
            }
            var strInterest = ""
            if selInterestArr.count > 0 {
                let arr = selInterestArr.map({ $0.interest!})
                strInterest = arr.joined(separator: ",")
                self.user?.interest = strInterest
            }
            
            let username = "\(selPrefix) \(self.name)"
            self.user?.name = username
            
            let params:[String:Any] = [
                "id": Int(self.user!.id),
                "name":self.user?.name ?? "-",
                "dob":self.user?.dob ?? "-",
                "email":self.user?.email ?? "-",
                "city":self.user?.city ?? "-",
                "state":self.user?.state ?? "-",
                "bloodgroup":self.user?.bloodgroup ?? "-",
                "gender":self.user?.gender ?? "-",
                "is_blood_donor":self.user?.is_blood_donor ?? "-",
                "mobile_no":self.user?.mobile_no ?? "-",
                "parent_id":self.user?.parent_id ?? "-",
                "profession":self.user?.profession ?? "-",
                "profile_image":self.profile_image ?? self.user?.profile_image ?? "null",
                "weight":self.user?.weight ?? "-",
                "weight_unit":self.user?.weight_unit ?? "-",
                "height":self.user?.height ?? "-",
                "height_unit":self.user?.height_unit ?? "-",
                "interest":self.user?.interest ?? "-"
                ]
            print("params: \(params)")
            let userModel = UserModel(user: self.user!)
            let jsonData = try JSONEncoder().encode(userModel)
            
            let jsonObj = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
            
            //let coredataService = CoreDataService(stack: CoreDataStack(model: "HealthApp"))
            //coredataService.saveUser(userData: userModel)
            
            self.coreDataManager.syncUser(json: [params]) { (res) in
                print("syncuser result: \(res)")
                
                NotificationCenter.default.post(.init(name: Notification.Name(rawValue: ProfileUpdateNotification)))
            }
            
            apiManager.userRegAPI(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                
                switch result {
                    case .success(let data):
                        //self.alert(strTitle: strSuccess, strMsg: data.message!)
                        
                        //self.saveUser(userArr: params)
                            
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)  {
                            self.alert(strTitle: strSuccess, strMsg: data.message!)
                        }
                        
                        
                        UserDefaults.standard.set("1", forKey: "profile_completed")
                        
                        //if let prof = self.user?.profile_completed {
                            
                            //if !prof.isEmpty || Int(prof) == 1 {
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)  {
                                    
                                    NotificationCenter.default.post(.init(name: Notification.Name(rawValue: LoginDidNotification)))
                                }
                            //}
                        //}
                        
                    print(data)
                    case .failure(let err):
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                    print("err: \(err)")
                }
            }
            
            print("json: \(jsonObj)")
        } catch let err {
            print("err: \(err)")
        }
        
        
    }
    
    func uploadImage(){
        
        IHProgressHUD.show()
        
        apiManager.uploadImg(image: profileImgData!, name: "image", api: ProfileImage) { (result) in
         
            IHProgressHUD.dismiss()
            switch result {
                case .success(let data):
                    let imgstr = String(data: data, encoding: .utf8)
                print(imgstr)
                    
                    if((imgstr?.contains("jpeg"))!){
                        self.profile_image = imgstr
                        self.registerUser()
                    }
                case .failure(let err):
                print("err: \(err)")
            }
        }
        
    }
    
    func initAlert(id: String) {
        
        let nib = Bundle.main.loadNibNamed("AddMedAlertView", owner: self, options: nil)
        let contentView = nib?[0] as! UIView
    
        //addMedAlertView = SwiftAlertView(nibName: "AddMedAlertView", delegate: self, cancelButtonTitle: nil, otherButtonTitles: nil)
        addMedAlertView = SwiftAlertView(contentView: contentView, delegate: self, cancelButtonTitle: nil)
        addMedAlertView.dismissOnOtherButtonClicked = true
        addMedAlertView.dismissOnOutsideClicked = true
        addMedAlertView.kDefaultHeight = 375
        addMedAlertView.tag = 1
        
        let tblAlert = addMedAlertView.viewWithTag(10) as! UITableView
        tblAlert.dataSource = self
        tblAlert.delegate = self
        tblAlert.accessibilityIdentifier = id
        
        let lblTitle = addMedAlertView.viewWithTag(13) as! UILabel
        let lblTitle1 = addMedAlertView.viewWithTag(14) as! UILabel
        
        lblTitle1.text = ""
        
        if id == "interest" {
            lblTitle.text = "PLEASE SELECT INTERESTS"
        }
       
        tblAlert.register(UINib(nibName: "InterestCell", bundle: nil), forCellReuseIdentifier: "interestCell")
        
        let btnAdd = addMedAlertView.viewWithTag(12) as! UIButton
        btnAdd.addTarget(self, action: #selector(alertAddAction), for: .touchUpInside)
        btnAdd.accessibilityIdentifier = id
        
        let btnClose = addMedAlertView.viewWithTag(11) as! UIButton
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
     
        addMedAlertView.dismiss()
    }
    
    @IBAction func alertAddAction(_ sender: UIButton) {
        
        let indexpath = IndexPath(row: 14, section: 0)
        self.tblData.reloadRows(at: [indexpath], with: .none)
        
        addMedAlertView.dismiss()
    }
    
    @IBAction func checkAction(_ sender: UIButton) {
        
       
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            selInterestArr.append(interestArr[sender.tag])
        } else {
            let data = interestArr[sender.tag]
            if let index = selInterestArr.firstIndex(where: { (interest) -> Bool in
                return interest.id == data.id
            }) {
                selInterestArr.remove(at: index)
            }
        }
        print(selInterestArr)
        
    }
    
    func validate() -> Bool {
        
        if self.user?.name == nil || self.user!.name!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please enter name")
            return false
        }
        if self.user?.dob == nil || self.user!.dob!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select date of birth")
            return false
        }
        if self.user?.gender == nil || self.user!.gender!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select gender")
            return false
        }
        if let donor = self.user?.is_blood_donor, donor == "Yes" {
            
            if self.user?.bloodgroup == nil || self.user!.bloodgroup!.isEmpty {
                self.alert(strTitle: strErr, strMsg: "Please select blood group")
                return false
            }
            if self.user?.pincode == nil || self.user!.pincode!.isEmpty {
                
                self.alert(strTitle: strErr, strMsg: "Please enter pincode")
                return false
            }
            if self.user?.state == nil || self.user!.state!.isEmpty {
                
                self.alert(strTitle: strErr, strMsg: "Please select state")
                return false
            }
            if self.user?.city == nil || self.user!.city!.isEmpty {
                
                self.alert(strTitle: strErr, strMsg: "Please enter city")
                return false
            }
        }
        if self.user?.email == nil || self.user!.email!.isEmpty {
            
            //self.alert(strTitle: strErr, strMsg: "Please enter email")
            //return false
        }
        return true
    }
    
    @IBAction func UpdateAction() {
        
        if validate() {
            if profileImgData != nil {
                uploadImage()
            } else {
                registerUser()
            }
        }
        //registerUser()
    }
    
    func showAlert(id: String) {
    
        initAlert(id: id)
        addMedAlertView.show()
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UpdateProfileVC: UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblData {
            return 15
        }
        if tableView.accessibilityIdentifier == "interest" {
            return interestArr.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblData {
            
            var cell: ProfileCell = tableView.dequeueReusableCell(withIdentifier: "imgCell") as! ProfileCell
            
            if indexPath.row == 0 {
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchHappen(_:)))
                tap.delegate = self
                if profileImgData != nil {
                    cell.profileImg.image = UIImage(data: profileImgData!)
                } else if let img = user?.profile_image{
                    
                    let url = imgBaseURL + custProfileImg + img
                    cell.profileImg.kf.setImage(with: URL(string: url))
                }
                cell.profileImg.addGestureRecognizer(tap)
                cell.profileImg.isUserInteractionEnabled = true
                
            }
            if indexPath.row == 1 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "nameCell") as! ProfileCell
                cell.setup(id: "name", placeHolder: "Prefix")
                cell.delegate = self
                //cell.txtPrefix.text = selPrefix
                cell.txtLoc.addBottomBorder()
                cell.txtLoc.text = self.name//user?.name
                cell.txtLoc.accessibilityIdentifier = "name"
                cell.txtLoc.delegate = self
            }
            if indexPath.row == 2 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "genderCell") as! ProfileCell
                cell.setup(id: "gender", placeHolder: "Gender")
                cell.delegate = self
                if let gender = user?.gender {
                    cell.txtName.text = gender
                }
            }
            if indexPath.row == 3 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "dateCell") as! ProfileCell
                cell.setup(id: "date", placeHolder: "Date of Birth")
                cell.delegate = self
                let date = user?.dob?.toDate(format: df_dd_MM_yyyy)
                if let dob = date?.toString(format: df_dd_MM_yyyy) {
                    cell.txtName.text = dob
                }
            }
            if indexPath.row == 4 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "donorCell") as! ProfileCell
                cell.setup(id: "donor", placeHolder: "")
                cell.delegate = self
                
                if let blood_donor = user?.is_blood_donor, blood_donor == "Yes" {
                    cell.blood_donor.selectedIndex = 0//= user?.is_blood_donor == "Yes" ? 0:1
                } else {
                    cell.blood_donor.selectedIndex = 1
                }
            }
            if indexPath.row == 5 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "genderCell") as! ProfileCell
                cell.setup(id: "blood_group", placeHolder: "Blood Group")
                cell.delegate = self
                cell.txtName.text = user?.bloodgroup
            }
            if indexPath.row == 6 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "locationCell") as! ProfileCell
                cell.setup(id: "location", placeHolder: "Pincode")
                cell.txtLoc.accessibilityIdentifier = "pincode"
                cell.txtLoc.delegate = self
                cell.txtLoc.text = user?.pincode ?? ""
            }
            if indexPath.row == 7 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "genderCell") as! ProfileCell
                cell.setup(id: "state", placeHolder: "Select State")
                cell.pickerDataArr = statesArr
                cell.txtName.text = selState
                cell.delegate = self
            }
            if indexPath.row == 8 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "genderCell") as! ProfileCell
                cell.setup(id: "city", placeHolder: "Select City")
                cell.pickerDataArr = cityArr
                cell.txtName.text = selCity
                cell.delegate = self
            }
            
            if indexPath.row == 9 {
                cell = tableView.dequeueReusableCell(withIdentifier: "heightCell") as! ProfileCell
                cell.delegate = self
                cell.setup(id: "height", placeHolder: "")
                cell.txtFld2.text = self.user?.height_unit ?? ""
                if self.user?.height_unit == "Feet" {
                    if let arr = self.user?.height?.components(separatedBy: ":"), arr.count > 0 {
                        
                        cell.txtFld.text = arr[0]//self.user?.height
                        if arr.count > 1 {
                            cell.txtFld1.text = arr[1]
                        }
                    }
                } else {
                    cell.bottomTxtFld.text = self.user?.height ?? ""
                }
            }
            
            if indexPath.row == 10 {
                cell = tableView.dequeueReusableCell(withIdentifier: "weightCell") as! ProfileCell
                cell.delegate = self
                cell.setup(id: "weight", placeHolder: "Weight")
                cell.txtFld.text = self.user?.weight_unit ?? ""
                cell.bottomTxtFld.text = self.user?.weight ?? ""
            }
            
            if indexPath.row == 11 {
                cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! ProfileCell
                cell.delegate = self
                cell.setup(id: "mobile", placeHolder: "Mobile No")
                cell.bottomTxtFld.text = self.user?.mobile_no ?? ""
                //cell.bottomTxtFld.isEnabled = false
            }
            
            if indexPath.row == 12 {
                cell = tableView.dequeueReusableCell(withIdentifier: "bottomLineCell") as! ProfileCell
                cell.delegate = self
                cell.setup(id: "email", placeHolder: "EmailId")
                cell.bottomTxtFld.text = self.user?.email ?? ""
            }
            if indexPath.row == 13 {
                cell = tableView.dequeueReusableCell(withIdentifier: "dropDownCell") as! ProfileCell
                cell.delegate = self
                cell.setup(id: "profession", placeHolder: "Profession")
                if let prof = self.user?.profession {
                    let profdata = professionArr.first { (data) -> Bool in
                        data.id == Int(prof)
                    }
                    cell.txtFld.text = profdata?.profession//self.user?.profession
                }
                cell.pickerDataArr = self.professionArr
            }
            if indexPath.row == 14 {
                cell = tableView.dequeueReusableCell(withIdentifier: "dropDownCell") as! ProfileCell
                cell.delegate = self
                cell.txtFld.delegate = self
                cell.setup(id: "interest", placeHolder: "Interest")
                //cell.pickerDataArr = self.interestArr
                if selInterestArr.count > 0 {
                    let arr = selInterestArr.map({ $0.interest!})
                    let strInterest = arr.joined(separator: ",")
                    cell.txtFld.text = strInterest
                } else {
                    cell.txtFld.text = self.user?.interest
                }
            }
            
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "interestCell") as! AddBlogCell
            
            if tableView.accessibilityIdentifier == "interest" {
                let data = interestArr[indexPath.row]
                cell.lblTitle.text = data.interest
                
                cell.btnChk.isSelected = false
                
                let arr = selInterestArr.filter{ $0.id == data.id}
                if arr.count > 0 {
                    cell.btnChk.isSelected = true
                }
            }
            cell.btnChk.tag = indexPath.row
            cell.btnChk.accessibilityIdentifier = tableView.accessibilityIdentifier
            cell.btnChk.addTarget(self, action: #selector(checkAction(_:)), for: .touchUpInside)
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblData {
            
            if indexPath.row == 11 {
                return 0
            }
            if indexPath.row > 5 {
                return self.user?.profile_completed == "1" ? 50 : 0
            }
            if indexPath.row == 0 || indexPath.row == 4 {
                
                return 140
            }
            /*
            if indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 8  {
                if bloodDonarFlag == 1 {
                    return 0
                }
            }*/
            if indexPath.row == 5 {
                
                return bloodDonarFlag == 1 ? 0 : 50
            }
            return 50
        }
        return 40
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
    }
}
extension UpdateProfileVC: ProfileCellDelegate, UITextFieldDelegate {
    
    func onPrefixSelected(id: String) {
        self.selPrefix = id
        print("prefix: \(id)")
    }
    
    func onBloodGroupSelected(id: String) {
        self.user?.bloodgroup = id
        print("bloodgroup: \(id)")
    }
    
    func onDateSelected(id: String) {
        self.user?.dob = id
        print("dob: \(id)")
    }
    
    func onGenderSelected(id: String) {
        self.user?.gender = id
        print("gender: \(id)")
    }
    
    func onCitySelected(id: String) {
     
        self.user?.city = id
        print("city: \(id)")
    }
    
    func onStateSelected(id: String) {
        
        self.user?.state = id
        selState = id
        self.fetchCities()
    }
    
    
    func optionValueChanged(value: Int) {
        
         print("is_blood_donor: \(value)")
        if value == 0 {
            self.user?.is_blood_donor = "Yes"
        } else {
            self.user?.is_blood_donor = "No"
        }
        self.bloodDonarFlag = value
        tblData.reloadData()
    }
    
    func onValueSelected(type: String, value: String) {
        
        switch type {
            
        
        case "mobile":
            self.user?.mobile_no = value
        case "email":
            self.user?.email = value
        case "height_unit":
            self.user?.height_unit = value
        case "height":
            self.user?.height = value
        case "weight_unit":
            self.user?.weight_unit = value
        case "weight":
            self.user?.weight = value
        case "feet":
            self.selFt = Int(value)
        case "inch":
            self.selInch = Int(value)
        default:
            break
        }
        
        print("requestdata: \(self.user)")
    }
    
    func onDataSelected(type: String, value: Any) {
        
        switch type {
        
        case "profession":
            self.user?.profession = String((value as! ProfessionData).id ?? 0)
        
        case "interest":
            self.user?.interest = String((value as! InterestData).id ?? 0)
        
        default:
            break
        }
    }
    //textField delegate methods
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.accessibilityIdentifier == "interest" {
            
            self.showAlert(id: textField.accessibilityIdentifier!)
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("textfield value: \(textField.text)")
        if textField.accessibilityIdentifier == "name" {
            self.name = textField.text ?? ""
        }
        if textField.accessibilityIdentifier == "pincode" {
            self.user?.pincode = textField.text ?? ""
            
            if let value = textField.text {
                
                self.fetchDetailsByPincode(pincode:value)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        
        if textField.accessibilityIdentifier == "pincode" {
            self.user?.pincode = textField.text ?? ""
            
            if range.location > 4 && range.length != 1 {
                
                self.fetchDetailsByPincode(pincode: textField.text ?? "")

            }
        }
        return true
    }
}

extension UpdateProfileVC: UIImagePickerControllerDelegate {
    
    @objc func touchHappen(_ sender: UITapGestureRecognizer) {
            print("Tap On Image")
            picker.delegate = self
        let alert = UIAlertController(title: "Add Photo!", message: "", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: {
                action in
                
                self.picker.allowsEditing = true
                self.picker.sourceType = .camera
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                self.present(self.picker, animated: true, completion: nil)
                
            }))
            alert.addAction(UIAlertAction(title: "Choose from Gallery", style: .default, handler: {
                action in
                
                self.picker.allowsEditing = true
                self.picker.sourceType = .photoLibrary
                self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                self.present(self.picker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            
            present(alert, animated: true, completion: nil)
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.editedImage] as? UIImage else {
            return
        }
            profileImgData = image.jpegData(compressionQuality: 0.5)
            self.tblData.reloadData()
            dismiss(animated: true, completion: nil)
        }

        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
}

class ProfileCell: BaseCell {
    
    @IBOutlet weak var profileImg: CircularImg!
    @IBOutlet weak var blood_donor: RadioGroup!
    @IBOutlet weak var txtPrefix: RightViewTextField!
    @IBOutlet weak var txtName: RightViewTextField!
    @IBOutlet weak var txtLoc:UITextField!
    @IBOutlet weak var txtFld: RightViewTextField!
    @IBOutlet weak var txtFld1: RightViewTextField!
    @IBOutlet weak var txtFld2: RightViewTextField!
    @IBOutlet weak var bottomTxtFld: UITextField!//BottomLineTextField!
    
    @IBOutlet weak var lblHead1: UILabel!
    @IBOutlet weak var lblHead2: UILabel!
    @IBOutlet weak var lblName1: UILabel!
    @IBOutlet weak var lblName2: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var con_zeroW_btmTxt: NSLayoutConstraint!
    @IBOutlet weak var con_W_btmTxt: NSLayoutConstraint!
    @IBOutlet weak var con_zeroW_Txt: NSLayoutConstraint!
    @IBOutlet weak var con_W_Txt: NSLayoutConstraint!
    @IBOutlet weak var con_zeroW_Txt1: NSLayoutConstraint!
    @IBOutlet weak var con_W_Txt1: NSLayoutConstraint!
    
    var selRow: Int = 0
    
    var delegate: ProfileCellDelegate?
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
        return picker
        
    }()
    
   
    
    func setup(id: String,placeHolder: String){
        
        if id == "name" {
            txtPrefix.placeholder = placeHolder
            self.pickerDataArr = ["Mr","Miss","Mrs"]
            self.selType = "prefix"
            cellPicker.dataSource = self
            cellPicker.delegate = self
            txtPrefix.inputView = cellPicker
            
        }
        if id == "donor" {
            blood_donor.titles = ["Yes","No"]
            blood_donor.addTarget(self, action: #selector(optionSelected(radioGroup:)), for: .valueChanged)
        }
        if id == "gender" || id == "date" || id == "state" || id == "city" || id == "blood_group" {
            txtName.placeholder = placeHolder;
            txtName.removeBottomBorder()
            txtName.addBottomBorder()
            
            if id == "blood_group" {
                self.pickerDataArr = ["A+","A-","B-","B+","AB+"]
                self.selType = "blood_group"
                cellPicker.dataSource = self
                cellPicker.delegate = self
                txtName.inputView = cellPicker
            }
            if id == "gender" {
                self.pickerDataArr = ["Male","Female"]
                self.selType = "gender"
                cellPicker.dataSource = self
                cellPicker.delegate = self
                txtName.inputView = cellPicker
                
            }
            if id == "date" {
                
                txtName.inputView = datePicker
            }
            if id == "state" {
                self.selType = "state"
                cellPicker.dataSource = self
                cellPicker.delegate = self
                txtName.inputView = cellPicker
            }
            if id == "city" {
                self.selType = "city"
                cellPicker.dataSource = self
                cellPicker.delegate = self
                txtName.inputView = cellPicker
            }
            
        }
        if id == "location" {
            txtLoc.placeholder = placeHolder
            txtLoc.removeBottomBorder()
            txtLoc.addBottomBorder()
        }
        if id == "height" {
            
            self.selType = id
            self.pickerDataArr = ["Feet","Centimeters"]
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            txtFld2.addBottomBorder()
            txtFld2.inputView = cellPicker
            txtFld2.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "height_unit")
            
            let feetPicker =  UIPickerView()
            feetPicker.accessibilityIdentifier = "feet"
            feetPicker.dataSource = self
            feetPicker.delegate = self
            txtFld.inputView = feetPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "feet")
            txtFld.placeholder = "Feet"
            //txtFld.addBottomBorder()
            
            let inchPicker =  UIPickerView()
            inchPicker.accessibilityIdentifier = "inch"
            inchPicker.dataSource = self
            inchPicker.delegate = self
            txtFld1.inputView = inchPicker
            txtFld1.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "inch")
            txtFld1.placeholder = "Inch"
            txtFld1.addBottomBorder()
            
            bottomTxtFld.addBottomBorder()
            bottomTxtFld.accessibilityIdentifier = id
            bottomTxtFld.placeholder = "Height"
            
            if txtFld.text == self.pickerDataArr[0] as? String {
                self.removeConstraint(con_W_btmTxt)
                self.removeConstraint(con_zeroW_Txt)
                self.removeConstraint(con_zeroW_Txt1)
                txtFld1.rightView?.isHidden = false
            } else {
                self.addConstraint(con_W_btmTxt)
                self.addConstraint(con_zeroW_Txt)
                self.addConstraint(con_zeroW_Txt1)
                txtFld1.rightView?.isHidden = true
            }
        }
        if id == "weight" {
            
            self.selType = id
            self.pickerDataArr = ["Kg","lbs"]
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            txtFld.addBottomBorder()
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: "weight_unit")
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.accessibilityIdentifier = id
            
        }
        if id == "mobile" {
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.accessibilityIdentifier = id
        }
        if id == "email" {
            bottomTxtFld.placeholder = placeHolder
            bottomTxtFld.accessibilityIdentifier = id
            bottomTxtFld.removeBottomBorder()
            bottomTxtFld.addBottomBorder()
        }
        if id == "profession" {
            
            self.selType = id
            
            cellPicker.dataSource = self
            cellPicker.delegate = self
            cellPicker.accessibilityIdentifier = id
            //txtFld.addBottomBorder()
            txtFld.inputView = cellPicker
            txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            txtFld.placeholder = placeHolder
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
        }
        if id == "interest" {
            
            self.selType = id
            
            //cellPicker.dataSource = self
            //cellPicker.delegate = self
            //cellPicker.accessibilityIdentifier = id
            txtFld.accessibilityIdentifier = id
            //txtFld.addBottomBorder()
            //txtFld.inputView = cellPicker
            //txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done", id: id)
            txtFld.placeholder = placeHolder
            txtFld.removeBottomBorder()
            txtFld.addBottomBorder()
        }
    }
    
    @objc func optionSelected(radioGroup: RadioGroup){
        
        delegate?.optionValueChanged(value: radioGroup.selectedIndex)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selRow = row
        print("selType:\(selType)")
        if pickerView.accessibilityIdentifier == "feet" {
            txtFld.text = String(row)
        }
        else if pickerView.accessibilityIdentifier == "inch" {
            txtFld1.text = String(row+1)
            
        } else {
            
            switch selType {
            case "prefix":
                txtPrefix.text = pickerDataArr[row] as? String
                delegate?.onPrefixSelected(id: pickerDataArr[row] as! String)
            case "blood_group":
                txtName.text = pickerDataArr[row] as? String
                delegate?.onBloodGroupSelected(id: (pickerDataArr[row] as? String)!)
            case "gender":
                txtName.text = pickerDataArr[row] as? String
                delegate?.onGenderSelected(id: (pickerDataArr[row] as? String)!)
            case "state":
                if let statesArr = self.pickerDataArr as? [State] {
                    let state = statesArr[row]
                    txtName.text = state.state
                    delegate?.onStateSelected(id: state.state!)
                }
            case "city":
                if let cityArr = self.pickerDataArr as? [City] {
                    let city = cityArr[row]
                    txtName.text = city.sub_district
                    delegate?.onCitySelected(id: city.sub_district!)
                }
            case "profession":
                let profdata = pickerDataArr[row] as? ProfessionData
                txtFld.text = profdata?.profession
                
            case "interest":
                let data = pickerDataArr[row] as? InterestData
                txtFld.text = data?.interest
            
            case "height":
                if selRow == 0 {
                    self.removeConstraint(con_W_btmTxt)
                    self.removeConstraint(con_zeroW_Txt)
                    self.removeConstraint(con_zeroW_Txt1)
                    txtFld1.rightView?.isHidden = false
                } else {
                    self.addConstraint(con_W_btmTxt)
                    self.addConstraint(con_zeroW_Txt)
                    self.addConstraint(con_zeroW_Txt1)
                    txtFld1.rightView?.isHidden = true
                }
                let value = (pickerDataArr[selRow] as? String)!
                txtFld2.text = value
                
            case "weight":
                let value = (pickerDataArr[selRow] as? String)!
                txtFld.text = value
                
            default:
                break
            }
        }
    }
    
    @objc override func doneCellPicker (sender: UIBarButtonItem) {
        
        
        let id = sender.accessibilityIdentifier
        
        
        if id == "height_unit" {
            let value = (pickerDataArr[selRow] as? String)!
            txtFld2.text = value
            delegate?.onValueSelected(type: id!, value: value)
            txtFld2.resignFirstResponder()
        }
        if id == "height" {
            if selRow == 0 {
                let value = (pickerDataArr[selRow] as? String)!
                txtFld.text = value
                delegate?.onValueSelected(type: id!, value: value)
                txtFld.resignFirstResponder()
                txtFld.resignFirstResponder()
            } else {
                
            }
        }
        
        if id == "weight_unit" {
            let value = (pickerDataArr[selRow] as? String)!
            txtFld.text = value
            delegate?.onValueSelected(type: id!, value: value)
            txtFld.resignFirstResponder()
        }
        if id == "weight" {
            let value = (pickerDataArr[selRow] as? String)!
            txtFld.text = value
            delegate?.onValueSelected(type: id!, value: value)
            txtFld.resignFirstResponder()
            txtFld.resignFirstResponder()
        }
        
        if id == "profession" {
            let value = (pickerDataArr[selRow] as? ProfessionData)!
            txtFld.text = value.profession
            delegate?.onDataSelected(type: id!, value: value)
            txtFld.resignFirstResponder()
        }
        
        if id == "interest" {
            let value = (pickerDataArr[selRow] as? InterestData)!
            txtFld.text = value.interest
            delegate?.onDataSelected(type: id!, value: value)
            txtFld.resignFirstResponder()
        }
        
        if id == "feet" {
            txtFld.text = String(selRow)
            delegate?.onValueSelected(type: id!, value: txtFld.text!)
            txtFld.resignFirstResponder()
        }
        if id == "inch" {
            txtFld1.text = String(selRow+1)
            delegate?.onValueSelected(type: id!, value: txtFld1.text!)
            txtFld1.resignFirstResponder()
        }
        if id == "gender" {
            
            txtName.text = pickerDataArr[selRow] as? String
            delegate?.onGenderSelected(id: (pickerDataArr[selRow] as? String)!)
        }
        selRow = 0
    }
    
    @objc func dateSelected(_ picker: UIDatePicker){
        
        let dateStr = picker.date.toString(format: df_dd_MM_yyyy)//df.string(from: picker.date)
        
        txtName.text = dateStr
        
        let date = picker.date.toString(format: "YYYY-MM-dd")
        delegate?.onDateSelected(id: date)
        
    }
    
    @IBAction func textChanged(_ sender: UITextField) {
        delegate?.onValueSelected(type: sender.accessibilityIdentifier!, value: sender.text ?? "")
        print("id = \(sender.accessibilityIdentifier!)")
        print("value = \(sender.text)")
    }
}
protocol ProfileCellDelegate {
    
    func optionValueChanged(value:Int)
    func onStateSelected(id: String)
    func onCitySelected(id: String)
    func onGenderSelected(id: String)
    func onBloodGroupSelected(id: String)
    func onDateSelected(id: String)
    func onPrefixSelected(id: String)
    func onValueSelected(type: String, value: String)
    func onDataSelected(type: String, value: Any)
}
