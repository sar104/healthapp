//
//  AddMyHealthRecordVC.swift
//  HealthApp
//
//  Created by Apple on 07/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class AddMyHealthRecordVC: BaseVC {

    @IBOutlet weak var segmentedControl: MXSegmentedControl!
    
    @IBOutlet weak var containerView: UIView!
    var selIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        segmentedControl.removeAll()
        
        segmentedControl.append(title: "Report")
            .set(titleColor: .white, for: .selected)
        segmentedControl.append(title: "Prescription")
        .set(titleColor: .white, for: .selected)
        
        UserDefaults.standard.set(0, forKey: "HealthRecordSegment")
        segmentedControl.addTarget(self, action: #selector(segValueChanged(_:)), for: .valueChanged)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        
        selIndex = (UserDefaults.standard.value(forKey: "HealthRecordSegment") as? Int) ?? 0
        segmentedControl.select(index: selIndex, animated: false)
        /*
        if let HealthRecordContainerVC = self.getVC(with: "HealthRecordContainerVC", sb: IDMain) as? HealthRecordContainerVC {
            
            HealthRecordContainerVC.willMove(toParent: self)
            self.containerView.addSubview(HealthRecordContainerVC.view)
            self.addChild(HealthRecordContainerVC)
            HealthRecordContainerVC.didMove(toParent: self)
        }*/
    }
    
    @objc func segValueChanged(_ sender: MXSegmentedControl){
        print("selected value: \(sender.selectedIndex)")
    
        UserDefaults.standard.set(sender.selectedIndex, forKey: "HealthRecordSegment")
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "segueHealthRecord" {
            
            if let vc = segue.destination as? HealthRecordContainerVC {
                
                //vc.recordType = "Mine"
            }
        }
    }
    

}
