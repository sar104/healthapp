//
//  BaseVC.swift
//  HealthApp
//
//  Created by Apple on 21/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD
import CoreData


class BaseVC: UIViewController, UINavigationControllerDelegate {

    let apiManager: WebServiceManager = WebServiceManager(service: WebService())
    let defaults = UserDefaults.standard
    var locationService: LocationService?
    
    lazy var coreDataManager: CoreDataManager = {
        let manager = CoreDataManager.shared
        return manager
    }()
    
    var loggedUser: User?
    //var recordType: String?
    var arrRecordType = [kMine,kFamily]
    
    var specilizationArr = ["Family Physicial","Internal Medicine Physician","Pediatrician","Obtestrician (OB)","Gynecologist (GYN)","Surgeon","Psychiatrist","Cardiologist"]
    
    
    
    var feetArr = [0...7]
    var inchArr = [1...11]
    var privateContext: NSManagedObjectContext!
    var coreDataStack: CoreDataStack!
    var coreDataService: CoreDataService?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        //self.view.addGestureRecognizer(tapgesture)
        
        locationService = LocationService(delegate: self)
        
        
        
        //privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        //privateContext.parent = coreDataStack.mainContext
       
        
        
        let users = self.fetchUser()
        if users.count > 0 {
            loggedUser = users[0]
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.isKind(of: DashboardVC.self) && !self.isKind(of: HealthWellnessVC.self) && !self.isKind(of: NearByVC.self) && !self.isKind(of: ProfileVC.self) && !self.isKind(of: AddVC.self) {
            
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            
            setNavItems()
            if let tabbarvc = self.tabBarController as? HomeVC {
                
                tabbarvc.navigationController?.setNavigationBarHidden(true, animated: false)
                
            }
        } else {
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            
            showBackButton()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if !self.isKind(of: DashboardVC.self) && !self.isKind(of: HealthWellnessVC.self) && !self.isKind(of: NearByVC.self) && !self.isKind(of: ProfileVC.self) && !self.isKind(of: AddVC.self)  {
            
            //self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.navigationController?.toolbarItems = nil
            if let tabbarvc = self.tabBarController as? HomeVC {
                
                tabbarvc.navigationController?.setNavigationBarHidden(false, animated: false)
            }
        } else {
            
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
    }
    
    func setNavItems() {
        let logoImg = UIImage(named:"Logo")
        let imgView = UIImageView(image: logoImg)
        imgView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        imgView.center = (self.navigationController?.navigationBar.center)!
        
        imgView.contentMode = .scaleAspectFit
        navigationItem.titleView = imgView
        
        setBellIcon()
    }
    
    func setBellIcon() {
        let imgview = UIImageView()
        imgview.frame = CGRect(x: 0, y: 0, width: 23, height: 20)
        imgview.image = UIImage(named: "bell")
        
        let widthConstraint = imgview.widthAnchor.constraint(equalToConstant: 23)
        let heightConstraint = imgview.heightAnchor.constraint(equalToConstant: 20)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        
        let barButton = UIBarButtonItem(customView: imgview)
        //assign button to navigationbar
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    func showBackButton() {
        
        let imgview = UIImage()
        
        self.navigationController?.navigationBar.backIndicatorImage = imgview
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgview
        //self.navigationController?.navigationBar.backItem?.title = ""
        
        //self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        
        //self.navigationController?.navigationBar.tintColor = UIColor.clear
    }
    
    func pushToVC(with id: String, sb: String) {
        
        let vc = self.getVC(with: id, sb: sb)
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
    @objc func tapAction(){
        self.view.endEditing(true)
    }
    
    func getVC(with id: String, sb: String) -> UIViewController {
        
        let sb = UIStoryboard(name: sb, bundle: nil)
        
        let vc = sb.instantiateViewController(withIdentifier: id)
        return vc
    }
    func addDoneButton(dontTxt: String) -> UIToolbar {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let lblDone = UIButton()
        lblDone.frame = CGRect(x: 0, y: 0, width: 80, height: 30)
        lblDone.setTitle(dontTxt, for: .normal)
        lblDone.setTitleColor(.blue, for: .normal)
        lblDone.addTarget(self, action: #selector(self.donePicker(sender:)), for: .touchUpInside)
        //lblDone.accessibilityIdentifier = id
        
        let doneButton = UIBarButtonItem(customView: lblDone)
        //UIBarButtonItem(title: dontTxt, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let lblCancel = UIButton()
        lblCancel.frame = CGRect(x: 0, y: 0, width: 80, height: 30)
        lblCancel.setTitle(strCancel, for: .normal)
        lblCancel.setTitleColor(.blue, for: .normal)
        lblCancel.addTarget(self, action: #selector(self.cancelPicker(sender:)), for: .touchUpInside)
        
        let cancelButton = UIBarButtonItem(customView: lblCancel)
        //let cancelButton = UIBarButtonItem(title: strCancel, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelPicker))
        
        
        
        let btndone = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: Selector(("donePicker")))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.backgroundColor = UIColor.blue
        

        return toolBar
    }
    
    @objc func donePicker (sender: UIBarButtonItem) {
        
    }
    
    @objc func cancelPicker (sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }
    
    func navToOTP(mob: String){
        
        //OTPVC
        let vc: OTPVC = self.getVC(with: "OTPVC", sb: IDMain) as! OTPVC
        vc.mobile = mob
        //self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func fetchUser() -> [User] {
        let users = self.coreDataManager.fetchLocalUsers()
        //self.coreDataService?.fetchLocalUsers() ?? []
        //print(user)
        return users
    }
    
    func saveUser(userArr: [String: Any]) {
        
        self.coreDataManager.syncUser(json: [userArr]) { (res) in
            
           
        }
    }
    
    func openCSV(fileName:String, fileType: String)-> String!{
        guard let filepath = Bundle.main.path(forResource: fileName, ofType: fileType)
            else {
                return nil
        }
        do {
            var contents = "" // = try String(contentsOfFile: filepath, encoding: .utf8)
            
            if fileName == "Medicines" {
                contents = try String(contentsOfFile: filepath, encoding: .isoLatin1)
            } else {
                contents = try String(contentsOfFile: filepath, encoding: .utf8)
            }
            
            return contents
        } catch let err {
            print("File Read Error for file \(filepath): \(err)")
            return nil
        }
    }
    
    func parseCSV(file: String)-> [[String]]{
        
        
        let content: String! = openCSV(fileName: file, fileType: "csv")
        if content != nil {
            let parsedCSV: [[String]] = content.components(separatedBy: "\r").filter{ $0.isEmpty == false }.map{ $0.components(separatedBy: ";") }
            
            //print("medicines csv: \(parsedCSV)")
            print(parsedCSV.count)
            return parsedCSV
        }
        return []
    }
    
    func parseMedCSV(file: String)-> [[String]]{
        
        
        let content: String! = openCSV(fileName: file, fileType: "csv")
        if content != nil {
            let parsedCSV: [[String]] = content.components(separatedBy: "\r").filter{ $0.isEmpty == false }.map{ $0.components(separatedBy: ",") }
            
            //print("medicines csv: \(parsedCSV)")
            print(parsedCSV.count)
            return parsedCSV
        }
        return []
    }
    
    func getMedicines(name: String, completion: @escaping ((Result<[MedicineData]>) -> Void)) {
        
        IHProgressHUD.show()
        
        let params = ["medicine":name]
        apiManager.getMedicines(params: params) { (result) in
            
            IHProgressHUD.dismiss()
            
            
            
            switch result {
                case .success(let medData):
                    
                    //self.medicineArr = medData
                    completion(.success(medData))
                    
                case .failure(let err):
                print("err: \(err)")
                completion(.failure(err))
            }
        }
    }
    
    func fetchMedicines(name: String, completion: @escaping (([MedicineData]) -> Void)) {
        
        IHProgressHUD.show()
        
        let predicate = NSPredicate(format: "medicine_name BEGINSWITH %@", name)
        
        let medArr = CoreDataManager.shared.fetchMedicines(predicate: predicate)
        //let medArr = CoreDataManager.shared.fetchAllMedicines()
        
        var arrMed: [MedicineData] = []
        
        for data in medArr {
            
            arrMed.append(MedicineData(coreDataMed: data))
        }
        completion(arrMed)
        IHProgressHUD.dismiss()
    }
    
    
    func getMedicineArr()-> [MedicineData] {
        let arrMed = self.parseCSV(file: "Medicines")
        var medArr: [MedicineData] = []
        for row in arrMed {
            if row.count == 3 {
                var medData = MedicineData()
                medData.medicine_name = row[0]
                medData.packaging_of_medicines = row[1]
                medData.type_of_medicine = row[2]
                
                medArr.append(medData)
            }
        }
        return medArr
    }
    
    func saveMedData() {
        
        let medcnt = CoreDataManager.shared.getMedCount()
        
        if medcnt == 0 {
            saveMedicines()
        }
    }
    
    func saveMedicines(){
        
        
        let arrMed = self.parseMedCSV(file: "Medicines")
        /*
        self.coreDataService?
            .saveMedicineData(data: arrMed){
                (res) in
        }
        */
        
        
        var jsonMed: [[String:Any]] = []
        var cnt: Int = 0
        for row in arrMed {
            
            if cnt == 0 {
                cnt = cnt + 1
                continue
            } else {
                cnt = cnt + 1
            }
            
            
            if row.count == 3 {
                let medData = ["id": cnt, "medicine_name": row[0], "medicine_package": row[1], "medicine_type": row[2]] as [String : Any]
                jsonMed.append(medData)
            }
        }
        
        coreDataManager.syncMedicines(json: jsonMed){
            (result) in
        }
        
    }
    
    
    func getAllMonths()->[String]{
        
        let arr = Calendar.current.shortMonthSymbols
        return arr
    }
    
    @objc func didDismissAlertView(alertView: SwiftAlertView) {
        
    }
    
    func addRightButton()
    {
        
        let imgbck = UIButton(frame: CGRect(x: CGFloat(15), y: CGFloat(10), width: CGFloat(25), height: CGFloat(25)))
        imgbck.backgroundColor = .blue
        
        if let imgurl = loggedUser?.profile_image {
            let url = imgBaseURL + custProfileImg + imgurl
            imgbck.imageView!.kf.setImage(with: URL(string: url))
        }
        //imgbck.setImage(UIImage(named: img), for: .normal)
        //imgbck.addTarget(self, action:#selector(BaseVC.refreshDataApiCalling) , for: UIControlEvents.touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: imgbck)
    }
    
    func addBackButton() {
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BaseVC: LocationServiceDelegate {
    
    func didFetchCurrentLocation(_ location: Location) {
            
        defaults.set(location.latitude, forKey: kUserLat)
        defaults.set(location.longitude, forKey: kUserLong)
    }
    
    func fetchCurrentLocationFailed(error: Error) {
        
    }
    
}


