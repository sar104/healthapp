//
//  AddPregAlertVC.swift
//  HealthApp
//
//  Created by Apple on 03/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import IHProgressHUD

class AddPregAlertVC: BaseVC, AddPregAlertCellDelegate {

    var lastPeriod: Date?
    var strLastPeriod: String?
    var bornDate: Date?
    var strBornDate: String?
    var medAlertData: MedAlertData = MedAlertData()
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(AddPregAlertVC.dateSelected), for: .valueChanged)
        return picker
        
    }()
    var arrWeeks = [2,4,10,13,15,16,18,21,28,32,37,39]
    var arrTitles = ["Conception","Positive pregnancy test","Your first scan","Your baby has got fingerprints","Choose the hospital where you will give birth","Feeling your baby move","Your anomaly scan","Your baby can hear","You're in the third trimester!","Your baby is gaining weight","Your baby is full-term","You'll soon meet your baby!"]
    
    var arrAlertTexts = ["Your doctor or midwife will calculate your due date by working back to the first day of your last period. So at conception you are considered two weeks pregnant.", "If you haven not already tried a home pregnancy test, taking one now will confirm you are pregnant.", "You will be able to see your baby for the first time when you have your dating scan between 10 weeks and 14 weeks.","Tiny fingerprints unique to your baby are forming on his fingers. If your baby iss a boy, his testicles have now formed, and his penis is growing. If you are having a girl, her ovaries are developing.", "You may want to think ahead and book antenatal classes. A class can help prepare you for the rigours of labour and birth. You can also sign up for BabyCentres free online classes.", "Your baby should be able to hear your voice at around 16 weeks, so talk to him, sing or read aloud! He can also hear your heartbeat and any noises made by your digestive system.", "Most mums-to-be feel their babies move between 18 and 20 weeks. But it may take a while to recognise those early fluttering sensations.", "Although your babys eyes have formed, the colour of his eyes will continue to develop after he is born. Your baby continues to swallow amniotic fluid, which is good practice for his digestive system.", "Your baby has a well-developed sense of taste. His response to sound is also becoming more sophisticated. He is able to hear more distinctly, and may be able to pick out the different voices of you and your partner.", "By this stage, your babys fingernails have reached the tips of his fingers. Also this week, you may feel your baby hiccup! He is not breathing air but amniotic fluid and an episode is usually over quickly.", "You are probably gaining about 450g a week, largely because your baby is likely to gain more than half his birthweight during the third trimester.","Your pregnancy is now considered to be full term. Your baby could be born any day now.","Your baby is due this week! Keep in mind, the average pregnancy lasts between 37 weeks and 42 weeks."]
    
    var arrAlertDates: [String] = []
    var arrAlertTimings: [AlertTiming] = []
    
    @IBOutlet weak var tblData: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        medAlertData.user_id = Int(loggedUser!.id)
        medAlertData.user_name = loggedUser?.name
        medAlertData.alert_type = kAlertTypePreg
        medAlertData.medicine_name = kPregTracker
    }
    
    
    @objc func dateSelected(_ picker: UIDatePicker){
        
        let dateStr = picker.date.toString(format: df_dd_MM_yyyy)//df.string(from: picker.date)
        
        lastPeriod = picker.date
        strLastPeriod = dateStr
        
    }
    
     @objc override func donePicker (sender: UIBarButtonItem) {
        
        self.view.endEditing(true)
        self.tblData.reloadData()
        //let section = IndexPath(row: 0, section: 0)
        //self.tblData.reloadRows(at: [section], with: .automatic)
    }
    
    func onSaveClicked(id: Int) {
        
        if strLastPeriod == nil || strLastPeriod!.isEmpty {
            
            self.alert(strTitle: strErr, strMsg: "Please select last period date")
        } else {
            if id == 0 {
                calculatePregDate()
            }
            if id == 1 {
                setNotifications()
                addPregTrackerAlert()
            }
        }
    }
    
    func calculatePregDate() {
        
        do {
            
            let med_unit = ["member_id" : "", "member_name": "", "member_dob" : strLastPeriod] as [String : Any]
            let medunitData = try JSONSerialization.data(withJSONObject: med_unit, options: [])
            let jsonStrMedUnit = String(data: medunitData, encoding: .utf8)
            medAlertData.medicine_unit = jsonStrMedUnit
            
            bornDate = lastPeriod?.dateByAdding(component: .month, value: 9).dateByAdding(component: .day, value: 7)
            strBornDate = bornDate?.toString(format: "EEEE, dd MMM yyyy")
            
            for i in 0..<arrWeeks.count {
                
                //weeks to days
                
                let days = arrWeeks[i]*7
                let title = arrTitles[i]
                let alertText = arrAlertTexts[i]
                
                let nextDate = lastPeriod?.dateByAdding(component: .day, value: days)
                let strNext = nextDate?.toString(format: df_dd_MM_yyyy)
                print("nextDate: \(strNext)")
                arrAlertDates.append(strNext!)
                
                var alertTiming1 = AlertTiming()
                
                alertTiming1.timming = nextDate?.toString(format: df_dd_MM_yyyy)
                let millisec1 = nextDate!.convertToMilli()
                alertTiming1.time_in_miles = String(millisec1)
                
                var timeCategory1 = TimeCategory()
                timeCategory1.alert_name = title
                timeCategory1.alert_text = alertText
                timeCategory1.alert_status =   "pending"
                
                alertTiming1.timecategory = timeCategory1
                
                let timeData1 = try JSONEncoder().encode(timeCategory1)
                let jsonStrTime1 = String(data: timeData1, encoding: .utf8)
                
                alertTiming1.time_category = jsonStrTime1
                arrAlertTimings.append(alertTiming1)
                
            }
        } catch let err {
            
        }
        self.tblData.reloadData()
    }
    
    func setNotifications() {
        
        var arrNotif: [LocalNotification] = []
        let manager = LocalNotificationManager()
        
        for alert in arrAlertTimings {
            
            let strdate = alert.timming
            let mutableStr = NSMutableString()
            
            mutableStr.append(kAlertTypePreg)
            mutableStr.append("_")
            mutableStr.append(strdate!)
            
            let notifId = mutableStr as String
            
            let alertTitle = alert.timecategory?.alert_text
            let alertDate = strdate?.toDate(format: df_dd_MM_yyyy)
            
            let notif =
                LocalNotification(id: notifId, title: alertTitle!,
                                  datetime: DateComponents(calendar: Calendar.current, year: alertDate!.get(.year), month: alertDate!.get(.month), day: alertDate!.get(.day)))
        
            arrNotif.append(notif)
        }
        
        //print("notification\(arrNotif)")
        manager.notifications = arrNotif
        manager.schedule()
    }

    func addPregTrackerAlert(){
        
        do {
            
            let alertTimingData = try JSONEncoder().encode(arrAlertTimings)
            
            let jsonStrAlertTiming = String(data: alertTimingData, encoding: .utf8)
            
            var params: [String: Any] = [:]
            
            params["user_id"] = medAlertData.user_id
            params["user_name"] = medAlertData.user_name
            params["medicine_name"] = medAlertData.medicine_name
            params["medicine_unit"] = medAlertData.medicine_unit
            params["alert_timmings"] = jsonStrAlertTiming
            params["alert_type"] = medAlertData.alert_type
            
            print("params: \(params)")
            
            IHProgressHUD.show()
            apiManager.addMedicineAlert(params: params) { (result) in
                
                IHProgressHUD.dismiss()
                switch result {
                    case .success(let data):
                        self.alert(strTitle: strSuccess, strMsg: data.message!)
                    case .failure(let err):
                        print("err: \(err)")
                        self.alert(strTitle: strErr, strMsg: err.localizedDescription)
                }
            }
            
        } catch let err {
            print(err)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddPregAlertVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }
        if section == 1 {
            return bornDate == nil ? 0 : 1
        }
        if section == 2 {
            return bornDate == nil ? 0 : arrTitles.count
        }
        if section == 3 {
            return bornDate == nil ? 0 : 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = AddPregAlertCell()
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "dropdownCell") as! AddPregAlertCell
                cell.txtFld.inputView = datePicker
                cell.txtFld.inputAccessoryView = self.addDoneButton(dontTxt: "Done")
                cell.txtFld.text = strLastPeriod
                cell.delegate = self
            }
        }
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "labelCell") as! AddPregAlertCell
                cell.lblDate.text = strBornDate
            }
        }
        if indexPath.section == 2 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "pregCell") as! AddPregAlertCell
            
            if arrTitles.count > 0 {
                let title = arrTitles[indexPath.row]
                cell.lblTitle.text = title
            }
            
            if arrAlertDates.count > 0 {
                cell.lblDate.text = arrAlertDates[indexPath.row]
            }
            
            if arrAlertTexts.count > 0 {
                let content = arrAlertTexts[indexPath.row]
                cell.lblContent.text = content
            }
            
            let img = "pregnancy-week-" + String(arrWeeks[indexPath.row])
            cell.imgView.image = UIImage(named: img)
        }
        if indexPath.section == 3 {
        
            cell = tableView.dequeueReusableCell(withIdentifier: "btnAlertCell") as! AddPregAlertCell
            cell.delegate = self
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView()
        if bornDate != nil && section == 2 {
            
            let label = UILabel()
            label.textColor = .black
            view.backgroundColor = .white
            label.backgroundColor = .white
            
            label.text = "Pregnancy Milestones"
            view.frame = CGRect(x: 0, y: 0, width: tableView.frame.width-10, height: 30)
            label.sizeToFit()
            
            view.addSubview(label)
            
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 100
        }
        if indexPath.section == 1 {
            return 145
        }
        if indexPath.section == 2 {
            return 170
        }
        return 50
    }
}

protocol AddPregAlertCellDelegate {
    
    func onSaveClicked(id: Int)
}
class AddPregAlertCell: BaseCell {
    
    var delegate: AddPregAlertCellDelegate?
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var txtFld: UITextField!
    
    
    func setup(id: String) {
        
        
    }
    
    
    @IBAction func saveAction(_ sender: UIButton) {
        
        delegate?.onSaveClicked(id: sender.tag)
    }
}
/*
                 {
             "alert_timmings" =             (
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2862;
                     "time_category" = "{\"alert_name\":\"Conception\",\"alert_text\":\"Your doctor or midwife will calculate your due date by working back to the first day of your last period. So at conception you are considered two weeks pregnant.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1598243400278;
                     timming = "24-08-2020";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2863;
                     "time_category" = "{\"alert_name\":\"Positive pregnancy test\",\"alert_text\":\"If you haven not already tried a home pregnancy test, taking one now will confirm you are pregnant.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1599453000278;
                     timming = "07-09-2020";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2864;
                     "time_category" = "{\"alert_name\":\"Your dating scan\",\"alert_text\":\"You will be able to see your baby for the first time when you have your dating scan between 10 weeks and 14 weeks.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1603081800278;
                     timming = "19-10-2020";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2865;
                     "time_category" = "{\"alert_name\":\"Your babys fingerprints are forming\",\"alert_text\":\"Tiny fingerprints unique to your baby are forming on his fingers. If your baby iss a boy, his testicles have now formed, and his penis is growing. If you are having a girl, her ovaries are developing.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1604896200278;
                     timming = "09-11-2020";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2866;
                     "time_category" = "{\"alert_name\":\"Booking your antenatal class\",\"alert_text\":\"You may want to think ahead and book antenatal classes. A class can help prepare you for the rigours of labour and birth. You can also sign up for BabyCentres free online classes.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1606105800278;
                     timming = "23-11-2020";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2867;
                     "time_category" = "{\"alert_name\":\"Your baby can hear\",\"alert_text\":\"Your baby should be able to hear your voice at around 16 weeks, so talk to him, sing or read aloud! He can also hear your heartbeat and any noises made by your digestive system.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1606710600278;
                     timming = "30-11-2020";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2868;
                     "time_category" = "{\"alert_name\":\"Feeling your baby move\",\"alert_text\":\"Most mums-to-be feel their babies move between 18 and 20 weeks. But it may take a while to recognise those early fluttering sensations.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1607920200278;
                     timming = "14-12-2020";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2869;
                     "time_category" = "{\"alert_name\":\"Your babys eyes have developed\",\"alert_text\":\"Although your babys eyes have formed, the colour of his eyes will continue to develop after he is born. Your baby continues to swallow amniotic fluid, which is good practice for his digestive system.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1610339400278;
                     timming = "11-01-2021";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2870;
                     "time_category" = "{\"alert_name\":\"Your babys tastebuds are fully formed\",\"alert_text\":\"Your baby has a well-developed sense of taste. His response to sound is also becoming more sophisticated. He is able to hear more distinctly, and may be able to pick out the different voices of you and your partner.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1612758600278;
                     timming = "08-02-2021";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2871;
                     "time_category" = "{\"alert_name\":\"Your baby has fingernails\",\"alert_text\":\"By this stage, your babys fingernails have reached the tips of his fingers. Also this week, you may feel your baby hiccup! He is not breathing air but amniotic fluid and an episode is usually over quickly.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1616387400278;
                     timming = "22-03-2021";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2872;
                     "time_category" = "{\"alert_name\":\"Your baby is gaining weight\",\"alert_text\":\"You are probably gaining about 450g a week, largely because your baby is likely to gain more than half his birthweight during the third trimester.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1616992200278;
                     timming = "29-03-2021";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2873;
                     "time_category" = "{\"alert_name\":\"Your baby is full term\",\"alert_text\":\"Your pregnancy is now considered to be full term. Your baby could be born any day now.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1619411400278;
                     timming = "26-04-2021";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2874;
                     "time_category" = "{\"alert_name\":\"You will soon meet your baby!\",\"alert_text\":\"Your baby is due this week! Keep in mind, the average pregnancy lasts between 37 weeks and 42 weeks.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1620621000278;
                     timming = "10-05-2021";
                 }
             );
             "alert_type" = "pregnancy_tracker";
             id = 262;
             "medicine_name" = "Pregnancy Tracker";
             "medicine_unit" = "{\"member_id\":\"\",\"member_name\":\"\",\"member_dob\":\"10-08-2020\"}";
             "user_name" = "Miss Sarika";
         }
     );
     message = "Record Found.";
     status = 1;
 })
 success({
     data =     (
                 {
             "alert_timmings" =             (
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2862;
                     "time_category" = "{\"alert_name\":\"Conception\",\"alert_text\":\"Your doctor or midwife will calculate your due date by working back to the first day of your last period. So at conception you are considered two weeks pregnant.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1598243400278;
                     timming = "24-08-2020";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2863;
                     "time_category" = "{\"alert_name\":\"Positive pregnancy test\",\"alert_text\":\"If you haven not already tried a home pregnancy test, taking one now will confirm you are pregnant.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1599453000278;
                     timming = "07-09-2020";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2864;
                     "time_category" = "{\"alert_name\":\"Your dating scan\",\"alert_text\":\"You will be able to see your baby for the first time when you have your dating scan between 10 weeks and 14 weeks.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1603081800278;
                     timming = "19-10-2020";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2865;
                     "time_category" = "{\"alert_name\":\"Your babys fingerprints are forming\",\"alert_text\":\"Tiny fingerprints unique to your baby are forming on his fingers. If your baby iss a boy, his testicles have now formed, and his penis is growing. If you are having a girl, her ovaries are developing.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1604896200278;
                     timming = "09-11-2020";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2866;
                     "time_category" = "{\"alert_name\":\"Booking your antenatal class\",\"alert_text\":\"You may want to think ahead and book antenatal classes. A class can help prepare you for the rigours of labour and birth. You can also sign up for BabyCentres free online classes.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1606105800278;
                     timming = "23-11-2020";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2867;
                     "time_category" = "{\"alert_name\":\"Your baby can hear\",\"alert_text\":\"Your baby should be able to hear your voice at around 16 weeks, so talk to him, sing or read aloud! He can also hear your heartbeat and any noises made by your digestive system.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1606710600278;
                     timming = "30-11-2020";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2868;
                     "time_category" = "{\"alert_name\":\"Feeling your baby move\",\"alert_text\":\"Most mums-to-be feel their babies move between 18 and 20 weeks. But it may take a while to recognise those early fluttering sensations.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1607920200278;
                     timming = "14-12-2020";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2869;
                     "time_category" = "{\"alert_name\":\"Your babys eyes have developed\",\"alert_text\":\"Although your babys eyes have formed, the colour of his eyes will continue to develop after he is born. Your baby continues to swallow amniotic fluid, which is good practice for his digestive system.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1610339400278;
                     timming = "11-01-2021";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2870;
                     "time_category" = "{\"alert_name\":\"Your babys tastebuds are fully formed\",\"alert_text\":\"Your baby has a well-developed sense of taste. His response to sound is also becoming more sophisticated. He is able to hear more distinctly, and may be able to pick out the different voices of you and your partner.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1612758600278;
                     timming = "08-02-2021";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2871;
                     "time_category" = "{\"alert_name\":\"Your baby has fingernails\",\"alert_text\":\"By this stage, your babys fingernails have reached the tips of his fingers. Also this week, you may feel your baby hiccup! He is not breathing air but amniotic fluid and an episode is usually over quickly.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1616387400278;
                     timming = "22-03-2021";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2872;
                     "time_category" = "{\"alert_name\":\"Your baby is gaining weight\",\"alert_text\":\"You are probably gaining about 450g a week, largely because your baby is likely to gain more than half his birthweight during the third trimester.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1616992200278;
                     timming = "29-03-2021";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2873;
                     "time_category" = "{\"alert_name\":\"Your baby is full term\",\"alert_text\":\"Your pregnancy is now considered to be full term. Your baby could be born any day now.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1619411400278;
                     timming = "26-04-2021";
                 },
                                 {
                     "alert_status" = "<null>";
                     "medicine_alert_id" = 2874;
                     "time_category" = "{\"alert_name\":\"You will soon meet your baby!\",\"alert_text\":\"Your baby is due this week! Keep in mind, the average pregnancy lasts between 37 weeks and 42 weeks.\",\"alert_status\":\"Pending\"}";
                     "time_in_miles" = 1620621000278;
                     timming = "10-05-2021";
                 }
             );
             "alert_type" = "pregnancy_tracker";
             id = 262;
             "medicine_name" = "Pregnancy Tracker";
             "medicine_unit" = "{\"member_id\":\"\",\"member_name\":\"\",\"member_dob\":\"10-08-2020\"}";
             "user_name" = "Miss Sarika";
         }
     );
     message = "Record Found.";
     status = 1;
 })
 */
