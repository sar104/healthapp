//
//  AppStyleConstants.swift
//  HealthApp
//
//  Created by Apple on 22/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit
// Fonts

let SFProRegular = "SFProDisplay-Regular"
let SFProMed = "SFProDisplay-Medium"
let SFProBold = "SFProDisplay-Bold"
let SFProSemibold = "SFProDisplay-Semibold"

// Colors
//#B9B9B9
//#FB034F
//#FE004B
//#00C5DF

let RGB_251_3_79 = UIColor(red: 251.0/255.0, green: 3.0/255.0, blue: 79.0/255.0, alpha: 1)//
let RGB_82 = UIColor(red: 82.0/255.0, green: 82.0/255.0, blue: 82.0/255.0, alpha: 1)
let RGB_185 = UIColor(red: 185.0/255.0, green: 185.0/255.0, blue: 185.0/255.0, alpha: 1)
let RGB_254_0_75 = UIColor(red: 254.0/255.0, green: 0.0/255.0, blue: 75.0/255.0, alpha: 1)
let RGB_0_197_223 = UIColor(red: 0.0/255.0, green: 197.0/255.0, blue: 223.0/255.0, alpha: 1)
let RGB_prePeriod  = UIColor(red: 159.0/255.0, green: 152.0/255.0, blue: 223.0/255.0, alpha: 1)
let RGB_Period  = UIColor(red: 234.0/255.0, green: 33.0/255.0, blue: 140.0/255.0, alpha: 1)
let RGB_Ovul  = UIColor(red: 0.0/255.0, green: 174.0/255.0, blue: 239.0/255.0, alpha: 1)
let RGB_239 = UIColor(red: 239.0/255.0, green: 239.0/255.0, blue: 239.0/255.0, alpha: 1)

let RGB_DashReport = UIColor(red: 1.0/255.0, green: 181.0/255.0, blue: 202.0/255.0, alpha: 1)
let RGB_gradBlue = UIColor(red: 3.0/255.0, green: 190.0/255.0, blue: 251.0/255.0, alpha: 1)
let RGB_gradPink = UIColor(red: 254.0/255.0, green: 0.0/255.0, blue: 75.0/255.0, alpha: 1)//254,0,75
let RGB_btnReject = UIColor(red: 136.0/255.0, green: 5.0/255.0, blue: 0.0/255.0, alpha: 1)
let RGB_btnReOrder = RGB_251_3_79
let BgPending = UIColor(red: 228.0/255.0, green: 238.0/255.0, blue: 250.0/255.0, alpha: 1)//228,238,250
let BgApprovalPending = UIColor(red: 252.0/255.0, green: 249.0/255.0, blue: 232.0/255.0, alpha: 1)//252,249,232
let BgCancelled = UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1)//228,238,250
let BgDelPending = UIColor(red: 252.0/255.0, green: 249.0/255.0, blue: 255.0/255.0, alpha: 1)//252,249,255
let BgDelivered = UIColor(red: 233.0/255.0, green: 230.0/255.0, blue: 251.0/255.0, alpha: 1)//233,230,251
//#9f98df
//#ea218c 234,33,140
//#00aeef 0,174,239
