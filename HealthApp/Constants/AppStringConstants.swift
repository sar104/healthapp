//
//  AppStringConstants.swift
//  HealthApp
//
//  Created by Apple on 22/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit

let lblName = "Name"
let lblUserType = "I'm a "
let lblMob = "Mobile"
let lblEmail = "Email"
let lblMobNum = "Mobile Number"
let lblOTP = "Enter the 6-digit Code sent to you at "

let strDone = "Done"
let strNext = "Done"
let strCancel = "Cancel"
let strErr = "Error"
let strSuccess = "Success"
let strHealthRecord = "HEALTH RECORD"
let strAddRecords = "ADD RECORDS"
let strPrescContent = "Prescribed medicines, diagnosis, lab reports, etc..."
let strMedicines = "MEDICINES"
let strGetMedicines = "GET MEDICINES"
let strBuyMediContent = "Buy medicines from your local chemist or from our partner network"
let strOtherReport = "Other Report"
let strNull = "null"
let strApprovalPending = "Approval Pending"
let strPending = "Pending"
let strDeliveryPending = "Delivery Pending"
let strRejected = "Rejected"
let strCancelled = "Cancelled"
let strDelivered = "Delivered"

// PARAMS

let PARAM_NAME = "name"
let PARAM_MOB = "mobile_no"
let PARAM_ROLE = "role"
let PARAM_EMAIL = "email"
let PARAM_USERNAME = "username"
let PARAM_OTP = "otp"
let PARAM_FCMTOKEN = "fcm_token"
let PARAM_LATITUDE = "latitude"
let PARAM_LONGITUDE = "longitude"

let PARAM_ID = "id"
let PARAM_USERID = "user_id"
let PARAM_CHILDID = "child_id"
let PARAM_DOCTORID = "doctor_id"
let PARAM_DOCTORNAME = "doctor_name"
let PARAM_SHARINGTYPE = "sharing_type"
let PARAM_ILLNESSTYPE = "illness_type"
let PARAM_DESCRIPTION = "description"
let PARAM_ILLNESSDATE = "illness_date"
let PARAM_IMAGES = "images"


let err_name = "Please enter name"
let err_mob = "Please enter mobile number"
let err_role = "Please select role"
let err_email = "Please enter email"
let err_terms = "Please accept terms & conditions"
let err_digits = "Please enter all digits"


let InitialVCNotification = "setInitialVCNotification"
let LoginNotification = "setLoginVCNotification"
let LoginDidNotification = "LoginDidNotification"
let ProfileUpdateNotification = "ProfileUpdateNotification"
let CloseOrderNotification = "closeOrderNotification"

let df_MMM_yyyy = "MMM yyyy"
let df_MMMM_yyyy = "MMMM yyyy"
let df_yyyy_MM_dd = "yyyy-MM-dd"
let df_dd_MM_yyyy = "dd-MM-yyyy"
let df_hh_mm_a = "hh:mm a"


let kUserLat = "latitude"
let kUserLong = "longitude"
let kMine = "Mine"
let kFamily = "Family"

let kAlertTypeMed = "medicine_alerts"
let kAlertTypeMC = "mc_tracker"
let kAlertTypeDoc = "doctor_visits"
let kAlertTypeVac = "vaccination_tracker"
let kAlertTypePreg = "pregnancy_tracker"
let kAlertTypeChild = "growth_tracker"

let kSpecFitness = "Fitness/Yoga Instructor"
let kSpecCareGiver = "Care Giver"
let kSpecNutritionist = "Nutritionist/Dietition"
let kSpecNurse = "Nurse"
let kSpecService = "Special Service Provider"
let kSpecOther = "Other"


let MENU_REPORTS = "REPORTS"
let MENU_ORDERS = "ORDERS"
let MENU_ADD_FAMILY_MEMBER = "ADD FAMILY MEMBER"
let MENU_VIEW_FAMILY_MEMBER = "VIEW FAMILY MEMBER"
let MENU_DOCTOR = "DOCTOR"
let MENU_MEDICAL = "MEDICAL"
let MENU_DIAGNOSTICS = "DIAGNOSTICS"
let MENU_SERVICE_PROVIDER = "SERVICE PROVIDER"
let MENU_HELP = "FAQ"
let MENU_LOGOUT = "LOGOUT"

let MENU_HOME = "HOME"
let MENU_PROFILE = "PROFILE"
let MENU_BRANCHES = "MANAGE BRANCHES"
let MENU_CLINIC_BRANCHES = "MANAGE CLICNIC/HOSPITAL BRANCHES"
let MENU_RATINGS = "USER REVIEWS & RATINGS"
let MENU_CONTACT = "CONTACT US"
let MENU_APPOINTMENTS = "MANAGE APPOINTMENTS"

let ICON_REPORTS = "reports"
let ICON_ORDERS = "orders"
let ICON_ADD_FAMILY_MEMBER = "userplus"
let ICON_VIEW_FAMILY_MEMBER = "eye"
let ICON_DOCTOR = "doctors"
let ICON_MEDICAL = "medicines-menu"
let ICON_DIAGNOSTICS = "dignostics"
let ICON_SERVICE_PROVIDER = "userplus"
let ICON_HELP = "help"
let ICON_LOGOUT = "logout"
let ICON_PROFILE = "profile_icon"
let ICON_HOME = "home"
let ICON_CONTACT = "mail"
let ICON_RATING = "user_rating"
let ICON_BRANCH = "branch"

let systemVersion = (UIDevice.current.systemVersion as NSString).floatValue

let medAlertTitle = "It's medicine time! Come on pop in your medicine"
let docAlertTitle_24 = "Hey! Hope you haven't forgetten about visiting Dr #, tommrow @ time"
let docAlertTitle_4 = "Hope You're ready to visit Dr # in 4 hrs. Take Care!"
let docAlertTitle_2 = "Hope You're ready to visit Dr # in 2 hrs. Take Care!"
let docAlertTitle = "Hello! A gentle reminder about your plan of visiting Dr  #, @ time today!"

let vaccineAlertTitle_0 = "Hello! We have popped up to remind you that # needs to be taken for @"
let vaccineAlertTitle_1 = "Here is a gentle reminder for # needs to be taken for @"
let vaccineAlertTitle_2 = "Hello ! It is # Vaccination day today"
let vaccineAlertTitle_3 = "Please confirm if # has taken for @"

let mcAlertTitle_2 = "Hey! We estimate that your period is due in 2 days. Take care"
let mcAlertTitle_1 = "Hello! You might get your period tomorrow."
let mcAlertTitle_today = "Today might be the day for your period! Take care"
let mcOvulAlertTitle = "Today might be the day for your Peak Ovulation! Take care"

let mcPrePeriodTitle = "Tentative Pre-Period Day"
let mcPeriodDayTitle = "Tentative Period Day"
let mcOvulTitle = "Tentative Peak Ovulation Date"

let kMCTracker = "Menstrual Cycle Tracker"
let kPregTracker = "Pregnancy Tracker"
let kGrowthTracker = "Growth Tracker"

let kMedAlert = "MEDICINE ALERTS"
let kDocAlert = "DOCTOR VISIT ALERTS"
let kVacAlert = "VACCINATION TRACKER ALERTS"
let kMCAlert = "MENSTRUAL CYCLE ALERTS"
let kPregAlert = "PREGNANCY TRACKER ALERTS"
let kChildAlert = "CHILD GROWTH TRACKER ALERTS"

let growthAlertTitle = "Next Tracker alert date is "
let growthAlertText = "Please update the growth report of your child, ignore if already given. Please update the records."

let strKeepYourMobile = "Want to keep your contact number for Appointment"
let strAddAssistant = "Add an Assistant for Appointment, Prescription Record"
