//
//  AppConstants.swift
//  HealthApp
//
//  Created by Apple on 21/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

//let baseURL = "http://doctoronhire.com/health/api/"
//let baseURL = "http://teacheronhire.com/health/api/"
let  baseURL =  "http://server2.hohtechlabs.com/~doctoronhire/health/api/"
let imgBaseURL = "http://www.doctoronhire.com/health/public/upload/"
let chemistProfileUrl = imgBaseURL + "chemist/"
let prescriptionImageURL = imgBaseURL + "prescription/"
let reportImageURL = imgBaseURL + "medical_history_images/"
let doctorImgUrl = imgBaseURL + "doctor/"
let diagnosticUrl = imgBaseURL + "diagnostic_center/"
let bannerURL = imgBaseURL + "user_feed_image_video/"
let shopImgUrl = imgBaseURL + "shop/"

let termsUrl = "http://doctoronhire.com/health/terms_condition"

let healthNewsAPI = "http://newsapi.org/v2/everything?q=health%20news&from=&language=en&sortBy=publishedAt&apiKey=c1ba4825be714288bad011ff3fba1b84"

let fitnessNewsAPI = "http://newsapi.org/v2/everything?q=exercise%2C%20gym%2C%20fitness%20article&from=&language=en&sortBy=publishedAt&apiKey=c1ba4825be714288bad011ff3fba1b84"

let newsAPI = "https://bing-news-search1.p.rapidapi.com/news/search?"

let RoleAPI = "role"
let RegAPI = "common_registation"
let LoginAPI = "mobile_login"
let VerifyAPI = "verify"
let CustomerProfileAPI = "get_customer_profile"
let StateAPI = "state"
let CityAPI = "city"
let UserRegistationAPI = "user_registation"
let GetDetailsByPincode = "get_details_by_pincode"
let ProfileImage =  "profile_image"
let custProfileImg = "profile/"
let CustDashboard = "get_customer_dashboard"
let GetMonthlyReport = "get_monthly_report"
let GetMonthlyOrder = "get_monthly_order"
let AddMedicalHistoryAPI = "add_medical_history"
let MedicalHistoryImgAPI = "medical_history_image"
let MedicalLiveSearch = "search_medicine"
let PrescriptionImgAPI = "add_prescription_image"
let AddPrescriptionAPI = "add_prescription"
let GetFamilyMemberAPI = "get_family_member"
let GetIllnessAPI = "get_illness"
let AddDoctorAPI = "add_doctor"
let RelationAPI = "relation"
let ProfessionAPI = "profession"
let AddFamilyMemberAPI = "add_family_member"
let DeleteFamilyMemberAPI = "delete_family_member"
let UpdateFamilyMemberAPI = "update_family_member"
let SearchChemistAPI = "search_chemist"
let GetPrescriptionAPI = "get_prescription"
let GetFamilyPrescriptionAPI = "get_my_family_prescription"
let GetMedicalHistoryAPI = "get_medical_history"
let GetMyFamilyMedicalHistoryAPI = "get_my_family_medical_history"
let GetFamilyMedicalHistoryAPI = "get_family_medical_history"
let TimeSlotAPI = "time_slot"
let AddOrderForCustomerAPI = "add_order_for_customer_ios"
let GetMedicineAlertAPI = "get_medicine_alert"
let AddMedicineAlertAPI = "add_medicine_alert"
let GetBannersAPI = "get_banners"
let GetFilteredBanner = "get_filtered_banner"
let GetFilteredUserFeed = "get_filtered_user_feed"
let GetUserFeedAPI = "get_user_feed"
let GetFavDoctor = "get_my_favourite_doctor"
let GetDoctorAPI = "get_doctor"
let SearchDoctorAPI = "search_doctor"
let SearchDiagnosticAPI = "search_diagnostic_center"
let SearchServiceProviderAPI = "search_service_provider"
let GetFavDiagnosticAPI = "get_my_favourite_diagnostic_center"
let GetDetailsAPI = "get_doctor_profile"
let InterestAPI = "get_interest"
let OrderAPI = "get_order_list_for_customer"
let AddFavoriteAPI = "add_to_favourite"
let AddRatingAPI = "add_rating"
let SharePrescAPI = "share_prescription_ios"
let ShareReportAPI = "share_report_ios"
let GetUserByMobileAPI = "get_user_by_mobile"
let RequestFamilyMemberAPI = "request_family_member"
let InviteUserAPI = "invite_user"
let UpdatePrescriptionAPI = "update_prescription"
let UpdateMediHistoryAPI = "update_medical_history"
let UpdateCustOrderAPI = "update_order_for_customer_ios"
let AddUserFeedAPI = "add_user_feed"
let UserFeedImageAPI = "user_feed_image_video"
let GetChemistDashboardAPI = "get_chemist_dashboard"
let GetOrderListChemist = "get_order_list_for_chemist"
let GetOrderListChemistOwner = "get_order_list_for_chemist_owner"
let ApproveFamilyRequestAPI = "approve_family_request"
let RejectFamilyRequestAPI = "reject_family_request"
let CancelOrderAPI = "update_order_cancel"
let ConfirmOrderAPI = "accept_order"
let FAQAPI = "get_faqs"
let DeleteAlertAPI = "delete_medicine_alert"
let GetDoctorByMobileAPI = "get_doctor_by_mobile"
let ChemistProfileAPI = "get_chemist_profile"
let UpdateChemistProfileAPI = "update_chemist_branch"
let UploadChemistImgAPI = "chemist_image"
let ChemistBranchRegisterAPI = "chemist_branch_registration"
let GetChemistBranchesAPI = "get_chemist_branches"
let GetAcceptedOrdersOwnerAPI = "get_accepted_order_list_for_chemist_owner"
let GetAcceptedOrdersAPI = "get_accepted_order_list_for_chemist"
let GetDeliveredOrdersOwnerAPI = "get_delivered_order_list_for_chemist_owner"
let GetDeliveredOrdersAPI = "get_delivered_order_list_for_chemist"
let UpdateOrderChemistAPI = "update_order_for_chemist_ios"
let UpdateOrderPickedAPI = "update_order_for_chemist_picked"
let UpdateOrderDeliveredAPI = "update_order_for_chemist_delivered"
let UpdateOrderRejectedAPI = "update_order_for_chemist_reject"
let ContactUsAPI = "add_contact_us"
let DoctorDashboardAPI = "get_doctor_dashboard"
let GetDoctorApptAPI = "get_doctor_appointments"
let AddApptAPI = "add_appointment"
let GetDoctorProfileAPI = "get_doctor_profile"
let GetSpecializationAPI = "get_specialization"
let UpdateDoctorBranchAPI = "update_doctor_branch"
let AddBranchAPI = "doctor_branch_registration"
let GetDoctorBranchesAPI = "get_doctor_branches"
let GetApptDetailsAPI = "get_appointment_details"
let GetAttchedPrescAPI = "get_medical_history_details"
let GetPatientHistoryAPI = "get_prescription_for_doctor"
let UpdateAptAPI = "update_appointment"
let UpdateDiagnoBranchAPI = "update_diagnostic_branch"
let AddDiagnoBranchAPI = "diagnostic_branch_registration"
let UploadShopImageAPI = "shop_image"
let GetMedicalHistoryForDocAPI = "get_medical_history_for_doctor"
let UpdateSPShopBranchAPI = "update_shop_branch"
let GetSPBranchesAPI = "get_shop_branches"
let AddSPBranchAPI = "shop_branch_registration"
let GetAllRatingsAPI = "get_all_ratings"

/*
 customer_profile_url="http://www.doctoronhire.com/health/public/upload/profile/";
 public static final String doctor_profile_url="http://www.doctoronhire.com/health/public/upload/doctor/";
 public static final String chemist_profile_url="http://www.doctoronhire.com/health/public/upload/chemist/";
 public static final String diagnostic_center_profile_url="http://www.doctoronhire.com/health/public/upload/diagnostic_center/";
 public static final String shop_photo_url="http://www.doctoronhire.com/health/public/upload/shop/";
 public static final String user_feed_image_video="http://www.doctoronhire.com/health/public/upload/user_feed_image_video/";
 public static final String user_feed_videothumb="http://www.doctoronhire.com/health/public/upload/user_feed_videothumb/";
 public static final String medical_report_images="http://doctoronhire.com/health/public/upload/medical_history_images/";
 public static final String prescription_images="http://doctoronhire.com/health/public/upload/prescription/";



 **/
// storyboard ids

let IDMain = "Main"
let IDChemist = "Chemist"
let IDDoctor = "Doctor"
let IDDiagno = "Diagnostic"
let IDServicePro = "ServicePro"
let IDPageVC = "PageVC"
let IDUpdateProfile = "UpdateProfileVC"
let IDNavSegment = "NavSegment"
let IDNavChemist = "navChemist"
let IDChemistDashboard = "ChemistDashboard"
let IDNavDoctor = "navDoctor"
let IDDrUpdateProfile = "DrUpdateProfile"
let IDDrAppointment = "DrAppointment"
let IDNavDiagno = "navDiagno"
let IDNavServicePro = "navServicePro"

let SHARINGTYPE_ARRAY = ["Private","Family"]


/*
 pritam.hoh@gmail.com
 password: hoh@2020
 **/
