//
//  AppDelegate.swift
//  HealthApp
//
//  Created by Apple on 21/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
//import SwiftAlertView

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate, SwiftAlertViewDelegate {

    var window: UIWindow?
    let barBtnAppearance = UIBarButtonItem.appearance()
    var navigationBarAppearace = UINavigationBar.appearance()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let imgBack = UIImage(named: "back")
        let backBtnImg = imgBack?.stretchableImage(withLeftCapWidth: 0, topCapHeight: 10)
        barBtnAppearance.setBackButtonBackgroundImage(backBtnImg, for: .normal, barMetrics: .default)
    
        barBtnAppearance.tintColor = .clear
        navigationBarAppearace.barTintColor = UIColor.black
        
        //UINavigationBar.appearance().backIndicatorImage = imgBack
        //UINavigationBar.appearance().backIndicatorTransitionMaskImage = imgBack
        
        
        FirebaseApp.configure()//Passw0rd
        
        let newuser = UserDefaults.standard.value(forKey: "newuser") as? Int
        
        if newuser == nil {
            UserDefaults.standard.set(0, forKey: "newuser")
        }
        if CoreDataManager.shared.fetchLocalUsers().count > 0 {
            
            /*
            let role = UserDefaults.standard.value(forKey: "role") as? String
            
            var vc: UIViewController?
            
            if Int(role!) == 2 {
                vc = sb.instantiateViewController(withIdentifier: "NavIntro")
            }
            if Int(role!) == 3 {
                sb = UIStoryboard(name: IDChemist, bundle: nil)
                //vc = sb.instantiateViewController(withIdentifier: IDNavChemist)
                vc = sb.instantiateViewController(withIdentifier: "ChemistOrdersVC")
                
            }
            
            
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
            
            */
            setHomeVC()
            
        } else {
            setInitialVC()
        }
        
        
        IQKeyboardManager.shared.enable = true
        setNavTrans()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setInitialVC), name: NSNotification.Name(rawValue: InitialVCNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setHomeVC), name: NSNotification.Name(rawValue: LoginDidNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setLoginVC), name: NSNotification.Name(rawValue: LoginNotification), object: nil)
        
        setLocalNotification()
        setFirebaseNotification(application)
        
        
        
        return true
    }
    
    
    
    func setFirebaseNotification(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        print("fcmtoken: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: "deviceId")
    }
    
    @available(iOS 10, *)
       // Receive displayed notifications for iOS 10 devices.
       func userNotificationCenter(_ center: UNUserNotificationCenter,
                                   willPresent notification: UNNotification,
                                   withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
           let userInfo = notification.request.content.userInfo
           
           
           
           // Print full message.
           print("willPresent notification: \(userInfo)")
           
           // Change this to your preferred presentation option
           completionHandler(UNNotificationPresentationOptions.alert)
       }
       
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        application.applicationIconBadgeNumber = application.applicationIconBadgeNumber + 1
        print("didReceiveRemoteNotification: \(userInfo)")
        
        var notifData =  NotificationData()
        notifData.populateNotifData(dic: userInfo)
        
        handlePush(userInfo: userInfo, app: application)
    }
       
       // The callback to handle data message received via FCM for devices running iOS 10 or above.
       func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        
           print(remoteMessage)
           
           
           
       }
       
       @available(iOS 10.0, *)
       func userNotificationCenter(_ center: UNUserNotificationCenter,
                                   didReceive response: UNNotificationResponse,
                                   withCompletionHandler completionHandler: @escaping () -> Void) {
           let userInfo = response.notification.request.content.userInfo
           
           
           // Print full message.
           print("userNotificationCenter didReceive response: \(userInfo)")
        /*
          [AnyHashable("channel"): family_request, AnyHashable("mobile_no"): 7385043852, AnyHashable("google.c.sender.id"): 606581097387, AnyHashable("gcm.notification.channel"): family_request, AnyHashable("username"): Dr.  Pritamsing, AnyHashable("google.c.a.e"): 1, AnyHashable("title"): You have been added as Family member by Dr.  Pritamsing. Please ACCEPT to allow health records sharing, or REJECT to cancel..Team Ctrlh, AnyHashable("userid"): 404, AnyHashable("gcm.notification.username"): Dr.  Pritamsing, AnyHashable("aps"): {
             alert =     {
                 title = "You have been added as Family member by Dr.  Pritamsing. Please ACCEPT to allow health records sharing, or REJECT to cancel..Team Ctrlh";
             };
         }, AnyHashable("gcm.message_id"): 1601292828274978, AnyHashable("gcm.notification.mobile_no"): 7385043852, AnyHashable("gcm.notification.userid"): 404]
         **/
        
        /*
         [AnyHashable("gcm.notification.username"): Miss Sarika, AnyHashable("username"): Miss Sarika, AnyHashable("gcm.notification.channel"): Prescription, AnyHashable("google.c.a.e"): 1, AnyHashable("gcm.notification.mobile_no"): 9850059684, AnyHashable("userid"): 401, AnyHashable("mobile_no"): 9850059684, AnyHashable("gcm.notification.prescription_id"): 622, AnyHashable("prescription_id"): 622, AnyHashable("gcm.notification.userid"): 401, AnyHashable("google.c.sender.id"): 606581097387, AnyHashable("gcm.message_id"): 1602161546474794, AnyHashable("title"): Prescription shared, AnyHashable("channel"): Prescription, AnyHashable("gcm.notification.Remark"): {"remark":"-","comment":null}, AnyHashable("Remark"): {"remark":"-","comment":null}, AnyHashable("aps"): {
             alert =     {
                 title = "Prescription shared";
             };
         }]
         **/
        
           
           
        handlePush(userInfo: userInfo, app: nil)
        
           completionHandler()
       }
       
    
    func handlePush(userInfo: [AnyHashable:Any], app: UIApplication?) {
       
        var notifDic = userInfo
        var notifData =  NotificationData()
        notifData.populateNotifData(dic: userInfo)
        
        if notifData.channel == "family_request" {
            
            CoreDataManager.shared.saveNotifications(data: userInfo)
            
            UserDefaults.standard.set(notifData.userid, forKey: "notif_userid")
            let alert: SwiftAlertView = SwiftAlertView(title: "", message: notifData.title!, delegate: self, cancelButtonTitle: "Reject", otherButtonTitles: ["Accept"])
            //alert.dismissOnOtherButtonClicked = true
            alert.dismissOnOutsideClicked = true
            alert.show()
            
        }
        if notifData.channel == "Prescription" {
            
            var title = ""
            
            if notifData.shop_name == nil {
                
                title = notifData.username! + " has shared the prescription"
                
            } else {
                title = notifData.shop_name! + " has shared the prescription"
            }
            
            notifDic["title"] = title
            CoreDataManager.shared.saveNotifications(data: notifDic)
            
            let alert: SwiftAlertView = SwiftAlertView(title: "", message: title, delegate: self, cancelButtonTitle: "Ok")
            alert.dismissOnOutsideClicked = true
            alert.show()
            
        }
        if notifData.channel == "Report" {
            
            var title = ""
            
            if notifData.shop_name == nil {
                
                title = notifData.username! + " has shared the report"
                
            } else {
                title = notifData.shop_name! + " has shared the report"
            }
            
            notifDic["title"] = title
            CoreDataManager.shared.saveNotifications(data: notifDic)
            
            let alert: SwiftAlertView = SwiftAlertView(title: "", message: title, delegate: self, cancelButtonTitle: "Ok")
            alert.dismissOnOutsideClicked = true
            alert.show()
        }
        if notifData.channel == "Appointment" {
            
            var title = ""
            
            if notifData.shop_name == nil {
                
                title = notifData.username! + " booked an Appointment"
                
            } else {
                title = notifData.shop_name! + " booked an Appointment"
            }
            
            notifDic["title"] = title
            CoreDataManager.shared.saveNotifications(data: notifDic)
            
            let alert: SwiftAlertView = SwiftAlertView(title: "", message: title, delegate: self, cancelButtonTitle: "Ok")
            alert.dismissOnOutsideClicked = true
            alert.show()
        }
        if notifData.channel == "medicine_order" {
            
            if var title = notifData.title {
                
                if title.contains("waiting to be picked") {
                    
                    if notifData.shop_name == nil {
                        title = title + " from " + notifData.username!
                    } else {
                        title = title + " from " + notifData.shop_name!
                    }
                } else {
                    if notifData.shop_name == nil  {
                        title = title + " by " + notifData.username!
                    } else {
                        title = title + " by " + notifData.shop_name!
                    }
                }
                
                notifDic["title"] = title
                CoreDataManager.shared.saveNotifications(data: notifDic)
                
                if (title.lowercased().contains("new") || title.lowercased().contains("confirmed")) {
                    
                    setOrderNotification(notifData: notifData)
                }
                
                let alert: SwiftAlertView = SwiftAlertView(title: "", message: title, delegate: self, cancelButtonTitle: "Ok")
                alert.dismissOnOutsideClicked = true
                alert.show()
            }
        }
    }
    
    func setOrderNotification(notifData: NotificationData) {
        
        var arrNotif: [LocalNotification] = []
        let manager = LocalNotificationManager()
        let med_name = "You have an order pending action"
        //let med_unit = "Order Pending Action"
        
        let notifId = kAlertTypeMed + "_Order"
        
        let notif =
            LocalNotification(id: notifId, title: med_name,
                              datetime: DateComponents(calendar: Calendar.current,
                                                       year: Date().get(.year),month: Date().get(.month),
                                                       hour: Date().get(.hour), minute: Date().get(.minute) + 30))
                
        arrNotif.append(notif)
        print("notification\(arrNotif)")
        manager.notifications = arrNotif
        manager.schedule()
    }
 
    func alertAction(strTitle: String, strMsg: String, vc: UIViewController, completion: @escaping ((UIAlertAction)->Void))
    {
        let alert = UIAlertController(title: strTitle, message: strMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Accept", style: .default, handler: completion))
        alert.addAction(UIAlertAction(title: "Reject", style: .default, handler: completion))
        
        vc.present(alert, animated: true, completion: nil)
    }
    
    func setLocalNotification() {
            
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert]) { (granted, error) in
            if granted {
                print("yes")
                
            } else {
                print("no")
            }
        }
    }
    
    
    
    
    func setNavTrans() {
        
        // Sets background to a blank/empty image
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = .clear
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        UINavigationBar.appearance().isTranslucent = true
    }
    
    @objc func setInitialVC(){
        
        let sb = UIStoryboard(name: IDMain, bundle: nil)
        
        //let vc = sb.instantiateViewController(withIdentifier: IDNavSegment)
        let vc = sb.instantiateViewController(withIdentifier: "NavIntro")
        
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
    }
    
    @objc func setLoginVC(){
        
        let sb = UIStoryboard(name: IDMain, bundle: nil)
        
        let vc = sb.instantiateViewController(withIdentifier: IDNavSegment)
        
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
    }
    
    @objc func setHomeVC(){
        let role = UserDefaults.standard.value(forKey: "role") as? String
        
        if Int(role!) == 2 {
            
            let newuser = UserDefaults.standard.value(forKey: "newuser") as? Int
            let sb = UIStoryboard(name: IDMain, bundle: nil)
            var vc: UIViewController?
            
            /*
            if newuser == 1 {
                UserDefaults.standard.set(0, forKey: "newuser")
                vc = sb.instantiateViewController(withIdentifier: "NavIntro")
            } else {
                vc = sb.instantiateViewController(withIdentifier: "NavFrontVC")
            }
            */
            vc = sb.instantiateViewController(withIdentifier: "NavFrontVC")
            
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
        if Int(role!) == 3 {
            let sb = UIStoryboard(name: IDChemist, bundle: nil)
            
            let vc = sb.instantiateViewController(withIdentifier: IDNavChemist)
            
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
        if Int(role!) == 4 {
            let sb = UIStoryboard(name: IDDiagno, bundle: nil)
            
            let vc = sb.instantiateViewController(withIdentifier: IDNavDiagno)
            
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
        if Int(role!) == 5 {
            //let sb = UIStoryboard(name: IDDoctor, bundle: nil)
            let sb = UIStoryboard(name: IDDiagno, bundle: nil)
            
            //let vc = sb.instantiateViewController(withIdentifier: IDNavDoctor)
            let vc = sb.instantiateViewController(withIdentifier: IDNavDiagno)
            
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
        if Int(role!) == 6 {
            
            let sb = UIStoryboard(name: IDServicePro, bundle: nil)
            
            let vc = sb.instantiateViewController(withIdentifier: IDNavServicePro)
            
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
        //
        //navDoctor
        
    }
    
    // MARK: SwiftAlertView Delegate methods
    func alertView(alertView: SwiftAlertView, clickedButtonAtIndex buttonIndex: Int) {
        
        print("button: \(buttonIndex)")
        
        let users = CoreDataManager.shared.fetchLocalUsers()
        let user = users[0]
        let family_member_id = UserDefaults.standard.value(forKey: "notif_userid")
        let params = ["member_id": user.id,
                      "family_member_id": family_member_id!] as [String : Any]
        let apiManager = WebServiceManager(service: WebService())
        
        if buttonIndex == 1 {
            // callAPI
            
            apiManager.approveFamilyRequest(params: params) { (result) in
                
            }
        } else {
            // callAPI
            apiManager.rejectFamilyRequest(params: params) { (result) in
                
            }
        }
    }
    
    func didDismissAlertView(alertView: SwiftAlertView) {
        
    }

    // MARK: UISceneSession Lifecycle
/*
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
*/
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "HealthApp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

