//
//  WebServiceManager.swift
//  POCApp
//
//  Created by Apple on 1/27/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation


final class WebServiceManager {
    
    private let webService: WebService!
    
    init(service: WebService) {
        self.webService = service
    }
    
    func getData(api: String, params: [String:Any], completion: @escaping ((Result<Data>) -> Void)){
        
        let AppURL = baseURL + api
        webService.fetchData(api: AppURL, params: params) { (result) in
            
            switch result {
                
            case .success(let data):
                
                print("Success")
                
                completion(.success(data))
                
            case .failure(let error):
                completion(.failure(error))
            }
            
            
        }
    }
    
    func getRequestData(api: String, params: [String:Any], completion: @escaping ((Result<Data>) -> Void)){
        
        let AppURL = baseURL + api
        webService.getRequest(api: AppURL, params: params) { (result) in
            
            switch result {
                
            case .success(let data):
                
                print("Success")
                
                completion(.success(data))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func uploadImg(image: Data, name: String, api: String, completion :@escaping ((Result<Data>) -> Void)){
        
        let AppURL = baseURL + api
        
        webService.multiPartUpload(image: image, name: name, appURL: AppURL) { (result) in
            switch result {
                
            case .success(let data):
                
                print("Success")
                
                completion(.success(data))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    //multiPartUploadPDF
    
    func uploadPDF(image: Data, name: String, api: String, completion :@escaping ((Result<Data>) -> Void)){
        
        let AppURL = baseURL + api
        
        webService.multiPartUploadPDF(image: image, name: name, appURL: AppURL) { (result) in
            switch result {
                
            case .success(let data):
                
                print("Success")
                
                completion(.success(data))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func uploadImgArray(image: [Data], name: String, api: String, completion :@escaping ((Result<ImgResponseData>) -> Void)){
        
        let AppURL = baseURL + api
        
        webService.multiPartArrayUpload(images: image, name: name, appURL: AppURL) { (result) in
            switch result {
                
            case .success(let data):
                
                print("Success")
                
                do {
                    let json = try JSONDecoder().decode(ImgResponseData.self, from: data)
                    completion(.success(json))
                } catch let err {
                    completion(.failure(err))
                }
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    //
    func uploadPDFArray(image: [Data], name: String, api: String, completion :@escaping ((Result<ImgResponseData>) -> Void)){
        
        let AppURL = baseURL + api
        
        webService.multiPartArrayUploadPDF(images: image, name: name, appURL: AppURL) { (result) in
            switch result {
                
            case .success(let data):
                
                print("Success")
                
                do {
                    let json = try JSONDecoder().decode(ImgResponseData.self, from: data)
                    completion(.success(json))
                } catch let err {
                    completion(.failure(err))
                }
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    
    func getRoles(params: [String:Any], completion: @escaping ((Result<RoleData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: RoleAPI, params: [:]) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(RoleData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func registerAPI(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: RegAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    func loginAPI(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: LoginAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    
    func verifyOTPAPI(params: [String:Any], completion: @escaping ((Result<Data>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: VerifyAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                               // let json = try JSONDecoder().decode(UserData.self, from: data)
                                
                                
                                completion(.success(data))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
 
    func getStates(params: [String:Any], completion: @escaping ((Result<Data>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: StateAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            
                            completion(.success(data))
                            
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func getCities(params: [String:Any], completion: @escaping ((Result<Data>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: CityAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            
                            completion(.success(data))
                            
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func userRegAPI(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: UserRegistationAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func getDetailsByPincode(params: [String:Any], completion: @escaping ((Result<Data>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetDetailsByPincode, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            
                            completion(.success(data))
                            
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func uploadProfileImageAPI(params: [String:Any], completion: @escaping ((Result<Data>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: ProfileImage, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                               // let json = try JSONDecoder().decode(UserData.self, from: data)
                                
                                
                                completion(.success(data))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func getCustDashboard(params: [String:Any], completion: @escaping ((Result<CustDashboardData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: CustDashboard, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let json = try JSONDecoder().decode(CustDashboardData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                            
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func getMonthlyReports(params: [String:Any], completion: @escaping ((Result<Data>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetMonthlyReport, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            completion(.success(data))
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    func getMonthlyOrders(params: [String:Any], completion: @escaping ((Result<Data>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetMonthlyOrder, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            completion(.success(data))
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func addMedicalHistoryAPI(params: [String:Any], completion: @escaping ((Result<MedResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: AddMedicalHistoryAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(MedResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func updateMedicalHistory(params: [String:Any], completion: @escaping ((Result<MedResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: UpdateMediHistoryAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(MedResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func addPrecriptionAPI(params: [String:Any], completion: @escaping ((Result<PrescData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: AddPrescriptionAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                
                                let json = try JSONDecoder().decode(PrescData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    //
    func updatePrecription(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: UpdatePrescriptionAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func getMedicines(params: [String:Any], completion: @escaping ((Result<[MedicineData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: MedicalLiveSearch, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([MedicineData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func getFamilyMember(params: [String:Any], completion: @escaping ((Result<[FamilyMemberData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetFamilyMemberAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([FamilyMemberData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func getUserProfile(params: [String:Any], completion: @escaping ((Result<UserModel>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: CustomerProfileAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode(UserModel.self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    
    func getIllnessType(params: [String:Any], completion: @escaping ((Result<[IllnessTypeData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetIllnessAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([IllnessTypeData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func addDoctorAPI(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: AddDoctorAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func getRelations(params: [String:Any], completion: @escaping ((Result<[RelationData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: RelationAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([RelationData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    func getProfession(params: [String:Any], completion: @escaping ((Result<[ProfessionData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: ProfessionAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([ProfessionData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    func addFamilyMemberAPI(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: AddFamilyMemberAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    //
    func deleteFamilyMember(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: DeleteFamilyMemberAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func updateFamilyMember(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: UpdateFamilyMemberAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func searchChemists(params: [String:Any], completion: @escaping ((Result<[ChemistData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: SearchChemistAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([ChemistData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func getPrescriptionList(params: [String:Any], completion: @escaping ((Result<[PrescriptionData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetPrescriptionAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([PrescriptionData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    func getFamilyPrescriptionList(params: [String:Any], completion: @escaping ((Result<[PrescriptionData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetFamilyPrescriptionAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([PrescriptionData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    
    func getMyReportsList(params: [String:Any], completion: @escaping ((Result<Data>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetMedicalHistoryAPI, params: params) {  (result) in
                switch result {
                    
                case .success(let data):
                    completion(.success(data))
                case .failure(let error):
                    
                    print("Error \(error)")
                    completion(.failure(error))
                }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func getMyFamilyReportList(params: [String:Any], completion: @escaping ((Result<Data>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetMyFamilyMedicalHistoryAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                               completion(.success(data))
                           case .failure(let error):
                               
                               print("Error \(error)")
                               completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func getFamilyReportList(params: [String:Any], completion: @escaping ((Result<Data>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetFamilyMedicalHistoryAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                               completion(.success(data))
                           case .failure(let error):
                               
                               print("Error \(error)")
                               completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func getTimeSlots(params: [String:Any], completion: @escaping ((Result<[SlotData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: TimeSlotAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([SlotData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func AddOrderForCustomer(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: AddOrderForCustomerAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    //UpdateCustOrderAPI
    func UpdateOrderForCustomer(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: UpdateCustOrderAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    //ConfirmOrderAPI
    func acceptOrder(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: ConfirmOrderAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    //GetMedicineAlertAPI
    func getMedicineAlerts(params: [String:Any], completion: @escaping ((Result<[MedAlertData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetMedicineAlertAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([MedAlertData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func addMedicineAlert(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: AddMedicineAlertAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    func getBanners(params: [String:Any], completion: @escaping ((Result<[BannerData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetBannersAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([BannerData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    //GetFilteredBanner
    func getFilterBanners(params: [String:Any], completion: @escaping ((Result<[BannerData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetFilteredBanner, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([BannerData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //GetFilteredUserFeed
    
    func getFilterUserFeed(params: [String:Any], completion: @escaping ((Result<[BannerData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetFilteredUserFeed, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([BannerData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func getUserFeed(params: [String:Any], completion: @escaping ((Result<[BannerData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetUserFeedAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            //print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([BannerData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func searchDoctors(params: [String:Any], completion: @escaping ((Result<[FavDoctor]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: SearchDoctorAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([FavDoctor].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    func getDoctors(params: [String:Any], completion: @escaping ((Result<[FavDoctor]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetDoctorAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([FavDoctor].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    func getFavDoctors(params: [String:Any], completion: @escaping ((Result<[FavDoctor]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetFavDoctor, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([FavDoctor].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func getMyDoctors(params: [String:Any], completion: @escaping ((Result<[DoctorData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetFavDoctor, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([DoctorData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    func searchDiagnostic(params: [String:Any], completion: @escaping ((Result<[DiagnosticData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: SearchDiagnosticAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([DiagnosticData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func getFavDiagnostic(params: [String:Any], completion: @escaping ((Result<[DiagnosticData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetFavDiagnosticAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([DiagnosticData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //
    func searchServiceProviders(params: [String:Any], completion: @escaping ((Result<[DiagnosticData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: SearchServiceProviderAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([DiagnosticData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    //GetDetailsAPI
    func getDoctorDetails(params: [String:Any], completion: @escaping ((Result<DetailData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetDetailsAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode(DetailData.self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func getInterests(params: [String:Any], completion: @escaping ((Result<[InterestData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getRequestData(api: InterestAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([InterestData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func getSpecialization(params: [String:Any], completion: @escaping ((Result<[SpecializationData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getRequestData(api: GetSpecializationAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                print("getSpecialization: \(jsondata)")
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([SpecializationData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func getOrders(params: [String:Any], completion: @escaping ((Result<[CustOrderData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: OrderAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([CustOrderData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func addFavorite(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: AddFavoriteAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func addRating(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: AddRatingAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //SharePrescAPI
    func sharePresc(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: SharePrescAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    func shareReport(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: ShareReportAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func getUserByMobile(params: [String:Any], completion: @escaping ((Result<FamilyMemberData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetUserByMobileAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                if let status = jsondata["status"] as? Int {
                                    if status == 1 {
                                        let jsonmed = jsondata["data"]
                                        
                                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                        
                                        let json = try JSONDecoder().decode(FamilyMemberData.self, from: meddata)
                                        completion(.success(json))
                                    } else {
                                        completion(.failure(WebServiceError.NoRecords))
                                    }
                                }
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    //
    func getDocotrByMobile(params: [String:Any], completion: @escaping ((Result<DetailData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetDoctorByMobileAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                if let status = jsondata["status"] as? Int {
                                    if status == 1 {
                                        let jsonmed = jsondata["data"]
                                        
                                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                        
                                        let json = try JSONDecoder().decode(DetailData.self, from: meddata)
                                        completion(.success(json))
                                    } else {
                                        completion(.failure(WebServiceError.NoRecords))
                                    }
                                }
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //RequestFamilyMember
    func requestFamilyMember(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: RequestFamilyMemberAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func inviteMember(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: InviteUserAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //
    func addUserFeed(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
           
           if Connectivity.isConnectedToInternet(){
               
               self.getData(api: AddUserFeedAPI, params: params) {  (result) in
                              switch result {
                                  
                              case .success(let data):
                               do {
                                   let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                   completion(.success(json))
                               } catch let err {
                                   completion(.failure(err))
                               }
                              case .failure(let error):
                                  
                               print("Error \(error)")
                               completion(.failure(error))
                              }
               }
               
           } else {
               completion(.failure(WebServiceError.NoInternet))
           }
       }
    
    //
    func getChemistDashboard(params: [String:Any], completion: @escaping ((Result<ChemistDashData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetChemistDashboardAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                if let status = jsondata["status"] as? Int {
                                    if status == 1 {
                                        let jsonmed = jsondata
                                        
                                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed, options: .prettyPrinted)
                                        
                                        let json = try JSONDecoder().decode(ChemistDashData.self, from: meddata)
                                        completion(.success(json))
                                    } else {
                                        completion(.failure(WebServiceError.NoRecords))
                                    }
                                }
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //GetOrderListChemist
    
    func getOrderListChemist(api: String, params: [String:Any], completion: @escaping ((Result<[ChemistOrderData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: api, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                let jsonmed = jsondata["data"]
                                
                                let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                
                                let json = try JSONDecoder().decode([ChemistOrderData].self, from: meddata)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    //
    
    //ApproveFamilyRequestAPI
    
    func approveFamilyRequest(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: ApproveFamilyRequestAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func rejectFamilyRequest(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: RejectFamilyRequestAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func cancelOrder(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: CancelOrderAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //
    func getFAQ(params: [String:Any], completion: @escaping ((Result<[FAQData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: FAQAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                if let status = jsondata["status"] as? Int {
                                    if status == 1 {
                                        let jsonmed = jsondata["data"]
                                        
                                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                        
                                        let json = try JSONDecoder().decode([FAQData].self, from: meddata)
                                        completion(.success(json))
                                    } else {
                                        completion(.failure(WebServiceError.NoRecords))
                                    }
                                }
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    //DeleteAlertAPI
    func deleteAlert(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: DeleteAlertAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func getChemistProfile(params: [String:Any], completion: @escaping ((Result<ChemistData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: ChemistProfileAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                if let status = jsondata["status"] as? Int {
                                    if status == 1 {
                                        let jsonmed = jsondata["data"]
                                        
                                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                        
                                        //let json = try JSONDecoder().decode(ChemistData.self, from: meddata)
                                        var json = ChemistData()
                                        json.parseData(dic: jsonmed as! [String : Any])
                                        completion(.success(json))
                                    } else {
                                        completion(.failure(WebServiceError.NoRecords))
                                    }
                                }
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func updateChemistProfile(params: [String:Any], completion: @escaping ((Result<CommonResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: UpdateChemistProfileAPI, params: params) {  (result) in
                switch result {
                    
                case .success(let data):
                    do {
                        let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                        completion(.success(json))
                    } catch let err {
                        completion(.failure(err))
                    }
                case .failure(let error):
                    
                    print("Error \(error)")
                    completion(.failure(error))
                }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //ChemistBranchRegisterAPI
    func addMedicalBranch(params: [String:Any], completion: @escaping ((Result<CommonResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: ChemistBranchRegisterAPI, params: params) {  (result) in
                switch result {
                    
                case .success(let data):
                    do {
                        let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                        completion(.success(json))
                    } catch let err {
                        completion(.failure(err))
                    }
                case .failure(let error):
                    
                    print("Error \(error)")
                    completion(.failure(error))
                }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //GetChemistBranches
    func getChemistBranches(params: [String:Any], completion: @escaping ((Result<[ChemistData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetChemistBranchesAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            print("data: \(data)")
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                if let status = jsondata["status"] as? Int {
                                    if status == 1 {
                                        let jsonmed = jsondata["data"]
                                        
                                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed!, options: .prettyPrinted)
                                        
                                        let json = try JSONDecoder().decode([ChemistData].self, from: meddata)
                                        //var json = ChemistData()
                                        //json.parseData(dic: jsonmed as! [String : Any])
                                        completion(.success(json))
                                    } else {
                                        completion(.failure(WebServiceError.NoRecords))
                                    }
                                }
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    //ContactUsAPI
    func contactUs(params: [String:Any], completion: @escaping ((Result<CommonResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: ContactUsAPI, params: params) {  (result) in
                switch result {
                    
                case .success(let data):
                    do {
                        let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                        completion(.success(json))
                    } catch let err {
                        completion(.failure(err))
                    }
                case .failure(let error):
                    
                    print("Error \(error)")
                    completion(.failure(error))
                }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func updateOrderForChemist(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: UpdateOrderChemistAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func updateDelOrderForChemist(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: UpdateOrderDeliveredAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func updatePickOrderForChemist(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: UpdateOrderPickedAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func updateRejectOrderForChemist(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: UpdateOrderRejectedAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    func getDoctorDashboard(params: [String:Any], completion: @escaping ((Result<Data>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: DoctorDashboardAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            
                            completion(.success(data))
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    //GetDoctorApptAPI
    func getDoctorAppt(params: [String:Any], completion: @escaping ((Result<[AppointmentData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetDoctorApptAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                print("appointments: \(jsondata)")
                                
                                if let status = jsondata["status"] as? Int {
                                    if status == 1 {
                                        let jsonmed = jsondata["data"]
                                        
                                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed, options: .prettyPrinted)
                                        
                                        let json = try JSONDecoder().decode([AppointmentData].self, from: meddata)
                                        completion(.success(json))
                                    } else {
                                        completion(.failure(WebServiceError.NoRecords))
                                    }
                                }
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //AddApptAPI
    func addAppointment(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: AddApptAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    //UpdateAptAPI
    func updateAppointment(params: [String:Any], completion: @escaping ((Result<ResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: UpdateAptAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            do {
                                let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                                completion(.success(json))
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
        
    }
    
    //GetDoctorProfileAPI
    func getDoctorProfile(params: [String:Any], completion: @escaping ((Result<DetailData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetDoctorProfileAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                print("doctor profile: \(jsondata)")
                                
                                if let status = jsondata["status"] as? Int {
                                    if status == 1 {
                                        let jsonmed = jsondata["data"]
                                        
                                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed, options: .prettyPrinted)
                                        
                                        let json = try JSONDecoder().decode(DetailData.self, from: meddata)
                                        completion(.success(json))
                                    } else {
                                        completion(.failure(WebServiceError.NoRecords))
                                    }
                                }
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //
    func updateDoctorProfile(params: [String:Any], completion: @escaping ((Result<CommonResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: UpdateDoctorBranchAPI, params: params) {  (result) in
                switch result {
                    
                case .success(let data):
                    do {
                        let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                        completion(.success(json))
                    } catch let err {
                        completion(.failure(err))
                    }
                case .failure(let error):
                    
                    print("Error \(error)")
                    completion(.failure(error))
                }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    //UpdateDiagnoBranch
    
    func updateDiagnoProfile(params: [String:Any], completion: @escaping ((Result<CommonResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: UpdateDiagnoBranchAPI, params: params) {  (result) in
                switch result {
                    
                case .success(let data):
                    do {
                        let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                        completion(.success(json))
                    } catch let err {
                        completion(.failure(err))
                    }
                case .failure(let error):
                    
                    print("Error \(error)")
                    completion(.failure(error))
                }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func updateSPProfile(params: [String:Any], completion: @escaping ((Result<CommonResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: UpdateSPShopBranchAPI, params: params) {  (result) in
                switch result {
                    
                case .success(let data):
                    do {
                        let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                        completion(.success(json))
                    } catch let err {
                        completion(.failure(err))
                    }
                case .failure(let error):
                    
                    print("Error \(error)")
                    completion(.failure(error))
                }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //AddBranchAPI
    func addBranch(params: [String:Any], completion: @escaping ((Result<CommonResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: AddBranchAPI, params: params) {  (result) in
                switch result {
                    
                case .success(let data):
                    do {
                        let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                        completion(.success(json))
                    } catch let err {
                        completion(.failure(err))
                    }
                case .failure(let error):
                    
                    print("Error \(error)")
                    completion(.failure(error))
                }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //GetDoctorBranchesAPI
    func getDoctorBranches(params: [String:Any], completion: @escaping ((Result<[DetailData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetDoctorBranchesAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                print("doctor profile: \(jsondata)")
                                
                                if let status = jsondata["status"] as? Int {
                                    if status == 1 {
                                        let jsonmed = jsondata["data"]
                                        
                                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed, options: .prettyPrinted)
                                        
                                        let json = try JSONDecoder().decode([DetailData].self, from: meddata)
                                        completion(.success(json))
                                    } else {
                                        completion(.failure(WebServiceError.NoRecords))
                                    }
                                }
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    // add service provider branch
    func addSPBranch(params: [String:Any], completion: @escaping ((Result<CommonResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: AddSPBranchAPI, params: params) {  (result) in
                switch result {
                    
                case .success(let data):
                    do {
                        let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                        completion(.success(json))
                    } catch let err {
                        completion(.failure(err))
                    }
                case .failure(let error):
                    
                    print("Error \(error)")
                    completion(.failure(error))
                }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    // get service provider branches
    func getSPBranches(params: [String:Any], completion: @escaping ((Result<[DetailData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetSPBranchesAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                print("doctor profile: \(jsondata)")
                                
                                if let status = jsondata["status"] as? Int {
                                    if status == 1 {
                                        let jsonmed = jsondata["data"]
                                        
                                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed, options: .prettyPrinted)
                                        
                                        let json = try JSONDecoder().decode([DetailData].self, from: meddata)
                                        completion(.success(json))
                                    } else {
                                        completion(.failure(WebServiceError.NoRecords))
                                    }
                                }
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    //GetApptDetailsAPI
    func getAppointmentDetails(params: [String:Any], completion: @escaping ((Result<AppointmentData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetApptDetailsAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                print("doctor profile: \(jsondata)")
                                
                                if let status = jsondata["status"] as? Int {
                                    if status == 1 {
                                        let jsonmed = jsondata["data"]
                                        
                                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed, options: .prettyPrinted)
                                        
                                        let json = try JSONDecoder().decode(AppointmentData.self, from: meddata)
                                        completion(.success(json))
                                    } else {
                                        completion(.failure(WebServiceError.NoRecords))
                                    }
                                }
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    //GetAttchedPrescAPI
    func getAttachedPresc(params: [String:Any], completion: @escaping ((Result<PrescriptionData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetAttchedPrescAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                print("doctor profile: \(jsondata)")
                                
                                if let status = jsondata["status"] as? Int {
                                    if status == 1 {
                                        let jsonmed = jsondata["data"]
                                        
                                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed, options: .prettyPrinted)
                                        
                                        let json = try JSONDecoder().decode(PrescriptionData.self, from: meddata)
                                        completion(.success(json))
                                    } else {
                                        completion(.failure(WebServiceError.NoRecords))
                                    }
                                }
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //GetPatientHistoryAPI
    
    func getPatientHistory(params: [String:Any], completion: @escaping ((Result<[PrescriptionData]>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetPatientHistoryAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                            
                            
                            do {
                                let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                                
                                print("doctor profile: \(jsondata)")
                                
                                if let status = jsondata["status"] as? Int {
                                    if status == 1 {
                                        let jsonmed = jsondata["data"]
                                        
                                        let meddata = try JSONSerialization.data(withJSONObject: jsonmed, options: .prettyPrinted)
                                        
                                        let json = try JSONDecoder().decode([PrescriptionData].self, from: meddata)
                                        completion(.success(json))
                                    } else {
                                        completion(.failure(WebServiceError.NoRecords))
                                    }
                                }
                            } catch let err {
                                completion(.failure(err))
                            }
                           case .failure(let error):
                               
                            print("Error \(error)")
                            completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //GetMedicalHistoryForDocAPI
    
    func getReportsForDoctor(params: [String:Any], completion: @escaping ((Result<Data>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetMedicalHistoryForDocAPI, params: params) {  (result) in
                           switch result {
                               
                           case .success(let data):
                               completion(.success(data))
                           case .failure(let error):
                               
                               print("Error \(error)")
                               completion(.failure(error))
                           }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    //AddDiagnoBranchAPI
    func addDiagnoBranch(params: [String:Any], completion: @escaping ((Result<CommonResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: AddDiagnoBranchAPI, params: params) {  (result) in
                switch result {
                    
                case .success(let data):
                    do {
                        let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                        completion(.success(json))
                    } catch let err {
                        completion(.failure(err))
                    }
                case .failure(let error):
                    
                    print("Error \(error)")
                    completion(.failure(error))
                }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
    
    func getAllRatings(params: [String:Any], completion: @escaping ((Result<CommonResponseData>) -> Void)){
        
        if Connectivity.isConnectedToInternet(){
            
            self.getData(api: GetAllRatingsAPI, params: params) {  (result) in
                switch result {
                    
                case .success(let data):
                    do {
                        
                        let jsondata = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]
                        
                        print("getAllRatings: \(jsondata)")
                        
                        let json = try JSONDecoder().decode(CommonResponseData.self, from: data)
                        completion(.success(json))
                    } catch let err {
                        completion(.failure(err))
                    }
                case .failure(let error):
                    
                    print("Error \(error)")
                    completion(.failure(error))
                }
            }
            
        } else {
            completion(.failure(WebServiceError.NoInternet))
        }
    }
}
