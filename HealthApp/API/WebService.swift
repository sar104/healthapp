//
//  WebService.swift
//  POCApp
//
//  Created by Apple on 1/27/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

final class WebService {
    
    
    func fetchData(api: String,  params: [String:Any], completion :@escaping ((Result<Data>) -> Void)) {
        
        let url: URL = URL(string: api)!
        //let params1 = ["name": "xyz", "mobile_no": "55555555", "role": 2] as [String:Any]


        /*
        var request = URLRequest(url: url)
        
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "POST"
        
        let data = try! JSONSerialization.data(withJSONObject: params1, options: JSONSerialization.WritingOptions.fragmentsAllowed)

        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            print("json=\(json)")
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard let data = data else {
                completion(.failure(WebServiceError.noData))
                return
            }
            if let error = error {
                completion(.failure(error))
                return
            }
            let str = String(data: data, encoding: .utf8)
            
            print(str)
          
        }
        task.resume()
        */
        /*
        
        do {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let data = try! JSONSerialization.data(withJSONObject: params1, options: JSONSerialization.WritingOptions.prettyPrinted)

            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            if let json = json {
                print(json)
            }
            request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
            print("request: \(request)")
           AF.request(request as URLRequestConvertible).responseJSON {
               (response) in

               print(response)
            guard let data = response.data else {
                completion(.failure(WebServiceError.noData))
                return
            }
            if let error = response.error {
                completion(.failure(error))
                return
            }
            
            //let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
            //print("json = \(json)")
            
            completion(.success(data))
           }
            
           } catch {
               
           }
        */
        
         /*
        AF.request(url, method: .post, parameters: params)
            .responseJSON { response in

                print(response)
                print(response.result)
                guard let data = response.data else {
                    completion(.failure(WebServiceError.noData))
                    return
                }
                if let error = response.error {
                    completion(.failure(error))
                    return
                }
                
                //let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                //print("json = \(json)")
                
                completion(.success(data))

        }
        */
        
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { (response) in
            
            //print(response)
            //print(response.result)
            if Connectivity.isConnectedToInternet(){
                guard let data = response.data else {
                    completion(.failure(WebServiceError.noData))
                    return
                }
                if let error = response.error {
                    completion(.failure(error))
                    return
                }
                completion(.success(data))
            }  else {
                completion(.failure(WebServiceError.NoInternet))
            }
        }
    }
    
    func getRequest(api: String,  params: [String:Any], completion :@escaping ((Result<Data>) -> Void)) {
           
           let url: URL = URL(string: api)!
          
           
           AF.request(url, method: .get, parameters: params).responseJSON { (response) in
               
               //print(response)
               //print(response.result)
            if Connectivity.isConnectedToInternet(){
                guard let data = response.data else {
                    completion(.failure(WebServiceError.noData))
                    return
                }
                if let error = response.error {
                    completion(.failure(error))
                    return
                }
                completion(.success(data))
            } else {
                completion(.failure(WebServiceError.NoInternet))
            }
        }
    
       }
    
    func multiPartUpload(image: Data, name: String, appURL: String, completion :@escaping ((Result<Data>) -> Void)){
        
       
        let headers: HTTPHeaders
        headers = ["Content-type": "multipart/form-data",
                   "Content-Disposition" : "form-data"]
        AF.upload(multipartFormData: { (multipartFormData) in
            
            //multipartFormData.append(image, withName: name)
            multipartFormData.append(image, withName: name, fileName: "image.jpeg", mimeType: "image/jpeg")
            
        }, to: appURL, usingThreshold: UInt64.init(), method: .post, headers: headers).response {(response) in
            
            print(response)
            print(response.result)
            guard let data = response.data else {
                completion(.failure(WebServiceError.noData))
                return
            }
            if let error = response.error {
                completion(.failure(error))
                return
            }
            completion(.success(data))
        }
        
    }
    
    func multiPartUploadPDF(image: Data, name: String, appURL: String, completion :@escaping ((Result<Data>) -> Void)){
        
       
        let headers: HTTPHeaders
        headers = ["Content-type": "multipart/form-data",
                   "Content-Disposition" : "form-data"]
        AF.upload(multipartFormData: { (multipartFormData) in
            
            //multipartFormData.append(image, withName: name)
            multipartFormData.append(image, withName: name, fileName: "image.pdf", mimeType: "application/pdf")
            
        }, to: appURL, usingThreshold: UInt64.init(), method: .post, headers: headers).response {(response) in
            
            print(response)
            print(response.result)
            guard let data = response.data else {
                completion(.failure(WebServiceError.noData))
                return
            }
            if let error = response.error {
                completion(.failure(error))
                return
            }
            completion(.success(data))
        }
        
    }
    
    func multiPartArrayUpload(images: [Data], name: String, appURL: String, completion :@escaping ((Result<Data>) -> Void)){
        
       
        let headers: HTTPHeaders
        headers = ["Content-type": "multipart/form-data",
                   "Content-Disposition" : "form-data"]
        AF.upload(multipartFormData: { (multipartFormData) in
            
            //multipartFormData.append(image, withName: name)
            for image in images {
                multipartFormData.append(image, withName: "\(name)[]", fileName: "image.jpeg", mimeType: "image/jpeg")
            }
            
        }, to: appURL, usingThreshold: UInt64.init(), method: .post, headers: headers).response {(response) in
            
            print(response)
            print(response.result)
            guard let data = response.data else {
                completion(.failure(WebServiceError.noData))
                return
            }
            if let error = response.error {
                completion(.failure(error))
                return
            }
            completion(.success(data))
        }
        
    }
    
    func multiPartArrayUploadPDF(images: [Data], name: String, appURL: String, completion :@escaping ((Result<Data>) -> Void)){
        
       
        let headers: HTTPHeaders
        headers = ["Content-type": "multipart/form-data",
                   "Content-Disposition" : "form-data"]
        AF.upload(multipartFormData: { (multipartFormData) in
            
            //multipartFormData.append(image, withName: name)
            for image in images {
                multipartFormData.append(image, withName: "\(name)[]", fileName: "image.pdf", mimeType: "application/pdf")
            }
            
        }, to: appURL, usingThreshold: UInt64.init(), method: .post, headers: headers).response {(response) in
            
            print(response)
            print(response.result)
            guard let data = response.data else {
                completion(.failure(WebServiceError.noData))
                return
            }
            if let error = response.error {
                completion(.failure(error))
                return
            }
            completion(.success(data))
        }
        
    }
}
