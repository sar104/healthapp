//
//  String+Ext.swift
//  HealthApp
//
//  Created by Apple on 22/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

extension String {
    
    func setAttributes(str1: String, str2: String, color1: UIColor, color2: UIColor)-> NSMutableAttributedString{
        
        let mutableStr = NSMutableAttributedString()
           
        let newStr1 = NSMutableAttributedString(string: str1, attributes: [NSAttributedString.Key.foregroundColor: color1])
        let newStr2 = NSMutableAttributedString(string: str2, attributes: [NSAttributedString.Key.foregroundColor: color2])
            
        mutableStr.append(newStr1)
        mutableStr.append(newStr2)
             // Color
        return mutableStr
    }
    
    func setStringColors(str1: String, str2: String, color1: UIColor, color2: UIColor) -> NSMutableAttributedString {
        
        let attributedStr = NSMutableAttributedString(string: self)
       
        let range = (self as NSString).range(of: str1)
        let range1 = (self as NSString).range(of: str2)
        
        attributedStr.addAttribute(NSAttributedString.Key.foregroundColor, value: color1, range: range)
        attributedStr.addAttribute(NSAttributedString.Key.foregroundColor, value: color2, range: range1)
        return attributedStr
    }
    func setAttributes(str1: String, str2: String, color1: UIColor, color2: UIColor, font1: String, font2: String)-> NSMutableAttributedString{
        
        let mutableStr = NSMutableAttributedString()
           
        let newStr1 = NSMutableAttributedString(string: str1, attributes: [NSAttributedString.Key.foregroundColor: color1, NSAttributedString.Key.font: UIFont(name: font1, size: 16)! ])
        let newStr2 = NSMutableAttributedString(string: str2, attributes: [NSAttributedString.Key.foregroundColor: color2, NSAttributedString.Key.font: UIFont(name: font2, size: 16)!])
            
        mutableStr.append(newStr1)
        mutableStr.append(newStr2)
             // Color
        return mutableStr
    }
    
    func toDate(format: String)-> Date {
        
        let df = DateFormatter()
        df.dateFormat = format//"dd/MM/YYYY"
        let date = df.date(from: self) ?? Date()
        return date
    }
    
    func stringByAddingPercentEncodingForFormData(plusForSpace: Bool=false) -> String? {
      let unreserved = "*-._[{}]:="
      let allowed = NSMutableCharacterSet.alphanumeric()
      allowed.addCharacters(in: unreserved)

      if plusForSpace {
        allowed.addCharacters(in: " ")
      }

      var encoded = addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
      if plusForSpace {
        encoded = encoded?.replacingOccurrences(of: " ", with: "+")
      }
      return encoded
    }
    
    func buildURL(api: String, queryParams: [String:Any]) -> URL {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "server2.hohtechlabs.com"
        urlComponents.path = "/~doctoronhire/health/api/" + api
        urlComponents.setQueryItems(with: queryParams as! [String : String])
        
        return urlComponents.url!
    }
    
    var isValidContact: Bool {
        let phoneNumberRegex = "^[6-9]\\d{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        let isValidPhone = phoneTest.evaluate(with: self)
        return isValidPhone
    }
    
    func isNull() -> Bool {
        
        if self == "null" || self.isEmpty {
            return true
        }
        return false
    }
    
}
extension URLComponents {
    
    mutating func setQueryItems(with parameters: [String: String]) {
        self.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
    }
}
