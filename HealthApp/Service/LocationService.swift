//
//  LocationService.swift
//  HealthApp
//
//  Created by Apple on 09/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationServiceDelegate {
    
    func didFetchCurrentLocation(_ location: Location)
    func fetchCurrentLocationFailed(error: Error)
}

class LocationService: NSObject {
    
    let locationManager = CLLocationManager()
    var delegate: LocationServiceDelegate
    
    init(delegate: LocationServiceDelegate) {
        self.delegate = delegate
        super.init()
        self.setupLocationManager()
    }
    
    private func setupLocationManager(){
        
        if canUserLocationManager(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestLocation()
        }
    }
    
    private func canUserLocationManager() -> Bool {
        
        return CLLocationManager.locationServicesEnabled()
    }
    
    deinit {
        locationManager.stopUpdatingLocation()
    }
}
extension LocationService: CLLocationManagerDelegate {
 
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currlocation = locations.last {
            let location = Location(latitude: currlocation.coordinate.latitude, longitude: currlocation.coordinate.longitude)
            delegate.didFetchCurrentLocation(location)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        delegate.fetchCurrentLocationFailed(error: error)
    }
}
