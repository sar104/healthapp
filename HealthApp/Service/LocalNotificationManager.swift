//
//  LocalNotificationManager.swift
//  HealthApp
//
//  Created by Apple on 24/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UserNotifications

class LocalNotificationManager {
    
    var notifications = [LocalNotification]()
    
    init() {
        
    }
    
    func listScheduledNotifications()
    {
        UNUserNotificationCenter.current().getPendingNotificationRequests { notifications in

            for notification in notifications {
                //print(notification)
            }
        }
    }
    
    func listTodaysNotifications(completion: @escaping (([LocalNotification]) -> Void))
    {
       
        var arrTodaysNotif: [LocalNotification] = []
    
        UNUserNotificationCenter.current().getPendingNotificationRequests { notifications in

            for notification in notifications {
                //print(notification)
                
                let date = notification.identifier
                let today = Days.all[Date().get(.weekday) - 1]
                
                if date.contains(today.rawValue) {
                    if !notification.identifier.contains("_before_5") && !notification.identifier.contains("_after_5") {
                        arrTodaysNotif.append(self.getLocalNotification(_from: notification))
                    }
                }
            }
            completion(arrTodaysNotif)
        }
        //return arrTodaysNotif
    }
    func getLocalNotification(_from req: UNNotificationRequest) -> LocalNotification {
        
        let localNotif = LocalNotification(id: req.identifier, title: req.content.title, datetime: DateComponents(weekday: Date().get(.weekday)))
        return localNotif
       
    }
    
    private func requestAuthorization()
    {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in

            if granted == true && error == nil {
                self.scheduleNotifications()
            }
        }
    }
    func schedule()
    {
        UNUserNotificationCenter.current().getNotificationSettings { settings in

            switch settings.authorizationStatus {
            case .notDetermined:
                self.requestAuthorization()
            case .authorized, .provisional:
                self.scheduleNotifications()
            default:
                break // Do nothing
            }
        }
    }
    private func scheduleNotifications()
    {
        for notification in notifications
        {
            let content      = UNMutableNotificationContent()
            content.title    = notification.title
            content.sound    = .default

            let trigger = UNCalendarNotificationTrigger(dateMatching: notification.datetime, repeats: true)

            let request = UNNotificationRequest(identifier: notification.id, content: content, trigger: trigger)

            UNUserNotificationCenter.current().add(request) { error in

                guard error == nil else { return }

                //print("Notification scheduled! --- ID = \(notification.id)")
            }
        }
    }
    
    func deleteNotifications(notifIds: [String]) {
        
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: notifIds)
    }
}
