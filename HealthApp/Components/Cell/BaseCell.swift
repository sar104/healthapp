//
//  BaseCell.swift
//  HealthApp
//
//  Created by Apple on 28/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class BaseCell: UITableViewCell {

    let cellPicker = UIPickerView()
    var pickerDataArr = Array<Any>()
    var selType: String = ""
    
    var ageGroupArr = [5,12,18,21,25,40,50,60]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func addDoneButton(dontTxt: String, id: String) -> UIToolbar {
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let lblDone = UIButton()
        lblDone.frame = CGRect(x: 0, y: 0, width: 80, height: 30)
        lblDone.setTitle(dontTxt, for: .normal)
        lblDone.setTitleColor(.blue, for: .normal)
        lblDone.addTarget(self, action: #selector(self.doneCellPicker(sender:)), for: .touchUpInside)
        lblDone.accessibilityIdentifier = id
        
        let doneButton = UIBarButtonItem(customView: lblDone)
        //let doneButton = UIBarButtonItem(title: dontTxt, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.doneCellPicker))
        doneButton.accessibilityIdentifier = id
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let lblCancel = UIButton()
        lblCancel.frame = CGRect(x: 0, y: 0, width: 80, height: 30)
        lblCancel.setTitle(strCancel, for: .normal)
        lblCancel.setTitleColor(.blue, for: .normal)
        lblCancel.addTarget(self, action: #selector(self.cancelCellPicker(sender:)), for: .touchUpInside)
        
        let cancelButton = UIBarButtonItem(customView: lblCancel)
        //let cancelButton = UIBarButtonItem(title: strCancel, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelCellPicker))
        
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.backgroundColor = UIColor.blue
        

        return toolBar
    }
    
    @objc func doneCellPicker (sender: UIBarButtonItem) {
        
    }
    
    @objc func cancelCellPicker (sender: UIBarButtonItem) {
        
        self.endEditing(true)
    }
}
extension BaseCell: UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.accessibilityIdentifier == "feet" {
            return 8
        }
        if pickerView.accessibilityIdentifier == "inch" {
            return 11
        }
        if pickerView.accessibilityIdentifier == "age" {
            return ageGroupArr.count
        }
        if pickerView.accessibilityIdentifier == "weekday" {
            return 7
        }
        
        return self.pickerDataArr.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.accessibilityIdentifier == "feet" {
            return String(row)
        }
        else if pickerView.accessibilityIdentifier == "inch" {
            return String(row+1)
        } else if pickerView.accessibilityIdentifier == "age" {
            return String(ageGroupArr[row]) + "+"
        } else if pickerView.accessibilityIdentifier == "weekday" {
            let day = Days.all[row]//DateFormatter().weekdaySymbols[Date().get(.weekday)]
            return day.rawValue
        }
        else {
            if let strArr = self.pickerDataArr as? [String] {
                let title = strArr[row]
                return title
            }
            if let statesArr = self.pickerDataArr as? [State] {
                let state = statesArr[row]
                return state.state
            }
            if let citiesArr = self.pickerDataArr as? [City] {
                let city = citiesArr[row]
                return city.sub_district
            }
            if let arr = self.pickerDataArr as? [FamilyMemberData] {
                let title = arr[row].name
                return title
            }
            if let arr = self.pickerDataArr as? [IllnessTypeData] {
                let title = arr[row].illness_type
                return title
            }
            if let arr = self.pickerDataArr as? [RelationData] {
                let title = arr[row].relation
                return title
            }
            if let arr = self.pickerDataArr as? [ProfessionData] {
                let title = arr[row].profession
                return title
            }
            if let arr = self.pickerDataArr as? [ChemistData] {
                let title = arr[row].clinic_lab_hospital_name
                return title
            }
            if let arr = self.pickerDataArr as? [SlotData] {
                let title = arr[row].slot
                return title
            }
            if let arr = self.pickerDataArr as? [DoctorData] {
                let title = arr[row].user_name
                return title
            }
            if let arr = self.pickerDataArr as? [InterestData] {
                let title = arr[row].interest
                return title
            }
            if let arr = self.pickerDataArr as? [SpecializationData] {
                let title = arr[row].specialization
                return title
            }
        }
        return ""
    }
    
    
}
