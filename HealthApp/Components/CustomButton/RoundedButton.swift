//
//  RoundedButton.swift
//  HealthApp
//
//  Created by Apple on 07/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
@IBDesignable class RoundedButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    @IBInspectable var cornerRadius: CGFloat = 10 {
        
        didSet {
            updateCornerRadius(value: cornerRadius)
        }
    }
    
    @IBInspectable var borderColor: UIColor = .black {
        
        didSet {
            setBorderColor(color: borderColor)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        
        didSet {
            setBorderWidth(value: borderWidth)
        }
    }
    
    func sharedInit(){
        updateCornerRadius(value: cornerRadius)
        setBorderColor(color: borderColor)
        setBorderWidth(value: borderWidth)
    }
    
    func updateCornerRadius(value: CGFloat){
        layer.cornerRadius = value
    }
    
    func setBorderColor(color: UIColor){
        layer.borderColor = color.cgColor
    }
    func setBorderWidth(value: CGFloat) {
        layer.borderWidth = value
    }
}

class DashBorderButton: RoundedButton {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        sharedInit()
    }
    
    func setDashBorder(){
        
        let border = CAShapeLayer();
        border.strokeColor = borderColor.cgColor //UIColor.black.cgColor;
        border.fillColor = nil;
        border.lineDashPattern = [2, 2];
        border.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 5).cgPath
        border.frame = self.bounds;
        self.layer.addSublayer(border);
    }
    
    override func sharedInit(){
        super.sharedInit()
        setDashBorder()
    }
}
