//
//  CircularImg.swift
//  HealthApp
//
//  Created by Apple on 14/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class CircularImg: UIImageView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }
}
