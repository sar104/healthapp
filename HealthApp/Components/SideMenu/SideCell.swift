//
//  SideCell.swift
//  Radian
//
//  Created by Andrei Boulgakov on 23/11/17.
//  Copyright © 2017 Andrei Boulgakov. All rights reserved.
//

import UIKit
protocol SideCellDelegate: class {
    func onClick_EditProfile(_ obj: Any?)
}

class SideCell : BaseCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var viewSeparator: UIView!
    
    @IBOutlet weak var cons_SideIconHeight: NSLayoutConstraint!
    @IBOutlet weak var cons_SideIconWidth: NSLayoutConstraint!
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnTxtEdit: UIButton!
    @IBOutlet weak var btnProf: UIButton!
    @IBOutlet weak var btnView: UIButton!
    
    var delegate: SideCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func viewProfileClick(_ sender: Any) {
        
    }
    @IBAction func onClick_EditProfile(_ sender: Any) {
        self.delegate?.onClick_EditProfile(sender)
    }
    
}
