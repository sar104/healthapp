//
//  VKSideMenu.swift
//  Radian
//
//  Created by Andrei Boulgakov on 23/11/17.
//  Copyright © 2017 Andrei Boulgakov. All rights reserved.
//

import UIKit
//import SDWebImage

//  Converted with Swiftify v1.0.6491 - https://objectivec2swift.com/

protocol VKSideMenuDataSource: NSObjectProtocol {
    func numberOfSections(in sideMenu: VKSideMenu) -> Int
    
    func sideMenu(_ sideMenu: VKSideMenu, numberOfRowsInSection section: Int) -> Int
    
    func sideMenu(_ sideMenu: VKSideMenu, itemForRowAt indexPath: IndexPath) -> VKSideMenuItem
    
    func sideMenu(_ sideMenu: VKSideMenu, heightForRowAt indexPath: IndexPath) -> CGFloat
}

protocol VKSideMenuDelegate: NSObjectProtocol {
    
    func sideMenuDidShow(_ sideMenu: VKSideMenu)
    
    func sideMenuDidHide(_ sideMenu: VKSideMenu)
    
    func sideMenu(_ sideMenu: VKSideMenu, didSelectRowAt indexPath: IndexPath)
    
    func sideMenu(_ sideMenu: VKSideMenu, titleForHeaderInSection section: Int) -> String
    
    func editOnHeaderSideMenu()
    
    func viewOnHeaderSideMenu()

}
class VKSideMenuItem: NSObject {
    var icon: UIImage?
    var imgurl = ""
    var desc = ""
    var title = ""
    var Position = ""
    var page_id = ""
    var tag: Int?
    var imgIcon : UIImageView = UIImageView.init(frame: UIScreen.main.bounds)
}

class VKSideMenu : NSObject , UITableViewDelegate, UITableViewDataSource,SideCellDelegate
{
    @objc func onClick_EditProfile(_ obj: Any?) {
        self.hide()
        delegate?.editOnHeaderSideMenu()
    }
    
    @objc func onClick_ViewProfile(_ obj: Any?) {
        self.hide()
        delegate?.viewOnHeaderSideMenu()
    }
    

    weak var delegate: VKSideMenuDelegate?
    weak var dataSource: VKSideMenuDataSource?
    var direction = VKSideMenuDirection(rawValue: 0)
    var size: CGFloat = 0.0
    var view: UIView?
    var blurEffectStyle = UIBlurEffect.Style(rawValue: 0)!
    var tableView: UITableView?
    var rowHeight: CGFloat = 0.0
    var animationDuration: Int = 0
    var isEnableOverlay = false
    var isAutomaticallyDeselectRow = false
    var isHideOnSelection = false
    var isEnableGestures = true
    var backgroundColor: UIColor?
    var selectionColor: UIColor?
    var textColor: UIColor?
    var sectionTitleFont: UIFont?
    var sectionTitleColor: UIColor?
    var iconsColor: UIColor?
    
    var overlay: UIView?
    
    private var tapGesture: UITapGestureRecognizer?
    
    let ROOTVC = UIApplication.shared.delegate?.window??.rootViewController
    //var profile : WSProfile?
    override init() {
        super.init()
        baseInit()
        
    }
    
    enum VKSideMenuDirection : Int {
        case fromLeft
        case fromRight
        case fromTop
        case fromBottom
    }
    
    init(direction: VKSideMenuDirection) {
        super.init()
        baseInit()
        self.direction = direction
    }
    
    init(size: CGFloat, andDirection direction: VKSideMenuDirection) {
        super.init()
        baseInit()
        self.size = size
        self.direction = direction
    }
    
    func baseInit() {
        size = 220
        direction = VKSideMenuDirection.fromLeft
        rowHeight = 44
        isEnableOverlay = true
        isAutomaticallyDeselectRow = true
        isHideOnSelection = true
        isEnableGestures = true
        animationDuration = Int(1.5 as? CFTimeInterval ?? CFTimeInterval())
        tableView?.showsVerticalScrollIndicator = false
        tableView?.showsHorizontalScrollIndicator = false
        //sectionTitleFont = montserrat_Bold(r: 18.1)
        selectionColor = UIColor(white: 1.0, alpha: 0.5)
        backgroundColor = UIColor(white: 1.0, alpha: 0.8)
        textColor = UIColorFromRGB(rgbValue: 0x252525) as? UIColor
        iconsColor = UIColorFromRGB(rgbValue: 0x8f8f8f) as? UIColor
        sectionTitleColor = UIColorFromRGB(rgbValue: 0x8f8f8f) as? UIColor
        
        if !(systemVersion < 8.0) {
            //SystemVersion.SYSTEM_VERSION_LESS_THAN(version: "8.0") {
            blurEffectStyle = .dark
        }
    }
    
    func initViews() {
        // Setup overlay
        overlay = UIView(frame: UIScreen.main.bounds)
        overlay?.alpha = 0
        overlay?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        if isEnableOverlay {
            overlay?.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        }
        // Setup gestures
        if isEnableGestures {
            tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
            overlay?.addGestureRecognizer(tapGesture!)
        }
        let frame: CGRect = frameHidden()
        view = UIView()
        if systemVersion < 8.0 {
            //SystemVersion.SYSTEM_VERSION_LESS_THAN(version: "8.0") {
            view = UIView(frame: frame)
            //clang diagnostic push
            //clang diagnostic ignored "-Wdeprecated-declarations"
            view?.backgroundColor = backgroundColor
            //clang diagnostic pop
        }
        else {
            blurEffectStyle = .dark
            let blurEffect: UIVisualEffect? = UIBlurEffect(style: blurEffectStyle)
            if systemVersion < 11.0
                //SystemVersion.SYSTEM_VERSION_LESS_THAN(version: "11.0")
            {
                view = UIVisualEffectView(effect: blurEffect)
                view?.frame = frame
            }
            else {
                if !UIAccessibility.isReduceTransparencyEnabled {
                    view?.backgroundColor = UIColor.white
                    let blurEffect = UIBlurEffect(style: .dark)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    //blurEffectView.frame = self.view.bounds;
                    blurEffectView.frame = UIScreen.main.bounds
                    blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                    view?.insertSubview(blurEffectView, at: 0)
                    view = UIView()
                    view?.frame = frame
                }
            }
        }
        if #available(iOS 13.0, *){
            //self.overlay?.overrideUserInterfaceStyle = .light
        }

        tableView = UITableView(frame: (view?.bounds)!, style: .grouped)
        tableView = UITableView(frame :CGRect(x: 0, y:0, width: (tableView?.frame.size.width)!, height: (tableView?.frame.size.height)!) )

        tableView?.delegate = self
        tableView?.dataSource = self
        //tableView?.separatorColor = UIColor.gray
        tableView?.backgroundColor = UIColor.black
        tableView?.isScrollEnabled = false
        if #available(iOS 13.0, *){
            //self.tableView?.overrideUserInterfaceStyle = .light
        }

//        var cell: SideCell?
//        cell = tableView!.dequeueReusableCell(withIdentifier: "SideLogoutCell") as? SideCell
//        if cell == nil
//        {
//            let nib = Bundle.main.loadNibNamed("SideCell", owner: self, options: nil)
//            cell = nib?[2] as? SideCell
//        }
        tableView?.tableFooterView = UIView.init()

        tableView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        /*
        if AppDeviceConstant.DeviceType.IS_IPHONE_5_OR_LESS {
            tableView?.isScrollEnabled = true
        }*/
        tableView?.isScrollEnabled = true
        view?.addSubview(tableView!)

        if #available(iOS 13.0, *){
            //self.view?.overrideUserInterfaceStyle = .light
        }


        tableView!.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
        tableView!.layer.shadowOffset = CGSize(width: 0.0, height: -4.0)
        tableView!.layer.shadowOpacity = 0.30
        tableView!.layer.shadowRadius = 0.0
        tableView!.layer.masksToBounds = false
        tableView?.showsVerticalScrollIndicator = false
        tableView?.showsHorizontalScrollIndicator = false


    }
    
    
    func show() {
        initViews()
       // loggedUser = AppUserService.defaultInstance().currentLoggedUser

        //self.profile  = CoreDataOpration.sharedInstance.fetchProfileData()
        view?.tag = 100
        overlay?.tag = 100
        ROOTVC?.view?.addSubview(overlay!)
        ROOTVC?.view?.addSubview(view!)
        let frame: CGRect = frameShowed()
        UIView.animate(withDuration: 0.575, animations: {() -> Void in
            self.view?.frame = frame
            self.overlay?.alpha = 1.0
        }, completion: {(_ finished: Bool) -> Void in
            if (self.delegate != nil) && self.delegate!.responds(to: Selector("sideMenuDidShow:")){
                self.delegate?.sideMenuDidShow(self)
            }
        })
    }
    
    func show(withSize size: CGFloat) {
        self.size = size
        show()
    }
    
    func hide() {
        self.overlay = nil

        UIView.animate(withDuration: 0.575, animations: {() -> Void in
            self.view?.frame = self.frameHidden()
            for vw  in (self.ROOTVC?.view?.subviews)!
            {
                if vw.tag == 100
                {
                    vw.frame = self.frameHidden()
                }
            }
            self.overlay = nil
        }, completion: {(_ finished: Bool) -> Void in
            
            if self.delegate!.responds(to: Selector("sideMenuDidHide:")) {
                self.delegate?.sideMenuDidHide(self)
            }
            self.overlay?.alpha = 0.0
            for vw  in (self.ROOTVC?.view?.subviews)!
            {
                if vw.tag == 100
                {
                    vw.removeFromSuperview()
                }
            }
            self.view?.removeFromSuperview()
            self.overlay?.removeFromSuperview()
            self.overlay?.removeGestureRecognizer(self.tapGesture!)
            self.overlay = nil
            self.tableView = nil
            self.view = nil
            
        })
    }
    
    func getProfilePic(fromFBId fbid: String) -> URL {
        var picURL: URL?
        /*let url: String = AppFBBigImage(fbid)
        picURL = URL(string: url)*/
        return picURL!
    }

    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return (dataSource?.numberOfSections(in: self))!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (dataSource?.sideMenu(self, numberOfRowsInSection: section))!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item: VKSideMenuItem? = dataSource?.sideMenu(self, itemForRowAt: indexPath)
        var cell: SideCell?
        let sideNibName = "SideCell"
        if indexPath.section == 0 && indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "SideProfileCell") as? SideCell
            if cell == nil {
                let nib = Bundle.main.loadNibNamed(sideNibName, owner: self, options: nil)
                cell = nib?[0] as? SideCell
                /*
                let Fname = self.profile.Fname.isEmpty ? " " : self.profile!.Fname.capitalized
                let Lname = self.profile!.Lname.isEmpty ? " " : self.profile!.Lname.capitalized
                cell?.lblTitle.text = "\(Fname)" + " " + "\(Lname)"
                cell?.lblDesc.text = self.profile?.Email
 */
                cell?.lblTitle.text = item?.title
                cell?.lblDesc.text = item?.desc
                cell?.imgIcon.layer.cornerRadius = (cell?.imgIcon.frame.width)! / 2.0
                cell?.imgIcon.layer.masksToBounds = true
                cell?.imgIcon.layer.borderColor = UIColor.lightGray.cgColor
                cell?.imgIcon.layer.borderWidth = 1.5
//                cell?.lblTitle?.font = sectionTitleFont
                //cell?.lblTitle?.setLineSpacing(text: (item?.title )!, lbl: (cell?.lblTitle)!)
                if let imgurl = item?.imgurl, !imgurl.isEmpty {
                    let imageUrl : URL = URL.init(string: imgurl)!
                    cell?.imgIcon.kf.setImage(with: imageUrl)
                }
                /*
                if self.profile!.Profile_pic.isEmpty
                {
                    cell?.imgIcon.image = UIImage.init(named:PLACEHOLDER_AVATAR)
                }
                else{
                    let imageUrl : URL = URL.init(string: ("\(self.profile!.Profile_pic)"))!
                    cell?.imgIcon.sd_setShowActivityIndicatorView(true)
                    cell?.imgIcon.sd_setImage(with: imageUrl, placeholderImage: UIImage.init(named:PLACEHOLDER_AVATAR), options: .refreshCached) { (image, error, imageCache, url) in
                    }
                }*/
                cell?.delegate = self
                
                cell?.btnEdit.addTarget(self, action: #selector(onClick_EditProfile), for: .touchUpInside)
                cell?.btnTxtEdit.addTarget(self, action: #selector(onClick_EditProfile), for: .touchUpInside)
                cell?.btnProf.addTarget(self, action: #selector(onClick_EditProfile), for: .touchUpInside)
                cell?.btnView.addTarget(self, action: #selector(onClick_ViewProfile), for: .touchUpInside)
            }
        }            
        else
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "SideCell") as? SideCell
            if cell == nil
            {
                let nib = Bundle.main.loadNibNamed(sideNibName, owner: self, options: nil)
                cell = nib?[1] as? SideCell
            }
            cell?.lblTitle.text = item?.title
            cell?.cons_SideIconWidth.constant = 22
            cell?.cons_SideIconHeight.constant = 22

           cell?.imgIcon.image = item?.icon
            if (item?.imgurl.isEmpty)!
            {
               // cell?.imgIcon.image = UIImage.init(named:PLACEHOLDER_IMAGE)
            }
            else{
                let imageUrl : URL = URL.init(string: ("\(item?.imgurl ?? "")"))!
                cell?.imgIcon.kf.setImage(with: imageUrl)
            }
            /*
            cell?.imgIcon.contentMode = UIViewContentMode.scaleAspectFit
            cell?.view_base_Header.backgroundColor = UIColor.clear
            let index : Int = UserDefaults.standard.value(forKey: "SELECTED") as? Int ?? -1
            if(indexPath.row == index)
            {
                cell?.view_base_Header.backgroundColor = UIColor.init(red: 187.0/255.0, green: 225.0/255.0, blue: 218.0/255.0, alpha: 1.0)
            }*/
//            if indexPath.row == 1
//            {
//                cell?.lblTitle.textColor = UIColor.black
//                cell?.view_base_Header.backgroundColor = UIColor.init(red: 187.0/255.0, green: 225.0/255.0, blue: 218.0/255.0, alpha: 1.0)//UIColor.init(red: 174.0/255.0, green: 214.0/255.0, blue: 205.0/255.0, alpha: 1.0)
//            }
        }
        cell?.selectionStyle = .none
        //tableView.separatorColor = UIColor.white//APP_WHITE_COLOR
        cell!.preservesSuperviewLayoutMargins = true
        cell!.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        cell!.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        return (cell)!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (delegate != nil) && (delegate?.responds(to: Selector(("sideMenu:didSelectRowAt:"))))! {
            delegate?.sideMenu(self, didSelectRowAt: indexPath)
        }
        if isAutomaticallyDeselectRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        if isHideOnSelection {
            hide()
        }
        UserDefaults.standard.set(indexPath.row, forKey: "SELECTED")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0 {
            return 120;
        }
        rowHeight = 50
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let title = delegate?.sideMenu(self, titleForHeaderInSection: section)
        if (title ?? "").isEmpty {
            return 10
        }
        return 50.0
    }

//    SideLogoutCell
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //if (delegate != nil) && (delegate?.responds(to: Selector(("sideMenu:titleForHeaderInSection:"))))! {
            //return delegate?.sideMenu(self, titleForHeaderInSection: section).uppercased()
        //}
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let nib = Bundle.main.loadNibNamed("SideCell", owner: self, options: nil)
        let cell = nib?[3] as? SideCell
        
        let title = delegate?.sideMenu(self, titleForHeaderInSection: section)
        
        cell?.lblTitle.text = title
        
        return cell?.contentView
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
       // view.tintColor = UIColor.clear
        let header = view as? UITableViewHeaderFooterView
        header?.textLabel?.textColor = UIColor.white
        //header?.textLabel?.font = sectionTitleFont
        header?.backgroundColor = UIColor.black
        header?.contentView.backgroundColor = UIColor.black
    }
    
    // MARK: - GestureRecognition
    func addSwipeGestureRecognition(_ view: UIView) {
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.didSwap))
        switch direction {
        case .fromTop?:
            swipe.direction = .down
        case .fromLeft?:
            swipe.direction = .right
        case .fromBottom?:
            swipe.direction = .up
        case .fromRight?:
            swipe.direction = .left
        case .none:
            swipe.direction = .down
        }
        view.addGestureRecognizer(swipe)
    }
    
    @objc func didTap(_ gesture: UIGestureRecognizer) {
        if gesture.state == .ended {
            hide()
        }
    }
    
    @objc func didSwap(_ gesture: UISwipeGestureRecognizer) {
        if gesture.state == .ended && isEnableGestures {
            show(withSize: size)
        }
    }
    
    // MARK: - Helpers
    func frameHidden() -> CGRect {
        var frame: CGRect = CGRect.zero
        switch direction {
        case .fromTop?:
            frame = CGRect(x: 0, y: -size, width: UIScreen.main.bounds.size.width, height: size)
        case .fromLeft?:
            frame = CGRect(x: -size, y: 0, width: size, height: UIScreen.main.bounds.size.height)
        case .fromBottom?:
            frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: size)
        case .fromRight?:
            frame = CGRect(x: UIScreen.main.bounds.size.width + size, y: 0, width: size, height: UIScreen.main.bounds.size.height)
        case .none:
            frame = CGRect(x: 0, y: -size, width: UIScreen.main.bounds.size.width, height: size)
        }
        return frame
    }

    func frameShowed() -> CGRect {
        var frame: CGRect = view!.frame
        switch direction {
        case .fromTop?:
            frame.origin.y = 0
        case .fromLeft?:
            frame.origin.x = 0
        case .fromBottom?:
            frame.origin.y = UIScreen.main.bounds.size.height - size
        case .fromRight?:
            frame.origin.x = UIScreen.main.bounds.size.width - size
        case .none:
            frame.origin.y = 0

        }
        return frame
    }

    /*func SYSTEM_VERSION_LESS_THAN(v: Any) -> Any {
        return (UIDevice.current.systemVersion.compare(v, options: .numeric, range: nil, locale: .current) == .orderedAscending)
    }*/
    func UIColorFromRGB(rgbValue: UInt) -> Any {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
