//
//  RoundedView.swift
//  HealthApp
//
//  Created by Apple on 02/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

@IBDesignable class RoundedLabel: UILabel {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    @IBInspectable var cornerRadius: CGFloat = 10 {
        
        didSet {
            updateCornerRadius(value: cornerRadius)
        }
    }
    
    @IBInspectable var borderColor: UIColor = .black {
        
        didSet {
            setBorderColor(color: borderColor)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        
        didSet {
            setBorderWidth(value: borderWidth)
        }
    }
    
    func sharedInit(){
        updateCornerRadius(value: cornerRadius)
        setBorderColor(color: borderColor)
        setBorderWidth(value: borderWidth)
    }
    
    func updateCornerRadius(value: CGFloat){
        layer.cornerRadius = value
    }
    
    func setBorderColor(color: UIColor){
        layer.borderColor = color.cgColor
    }
    func setBorderWidth(value: CGFloat) {
        layer.borderWidth = value
    }
}

@IBDesignable class RoundedView: UILabel {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        sharedInit()
    }
    
    override func prepareForInterfaceBuilder() {
        sharedInit()
    }
    
    @IBInspectable var cornerRadius: CGFloat = 10 {
        
        didSet {
            updateCornerRadius(value: cornerRadius)
        }
    }
    
    @IBInspectable var borderColor: UIColor = .black {
        
        didSet {
            setBorderColor(color: borderColor)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        
        didSet {
            setBorderWidth(value: borderWidth)
        }
    }
    
    func sharedInit(){
        updateCornerRadius(value: cornerRadius)
        setBorderColor(color: borderColor)
        setBorderWidth(value: borderWidth)
    }
    
    func updateCornerRadius(value: CGFloat){
        layer.cornerRadius = value
    }
    
    func setBorderColor(color: UIColor){
        layer.borderColor = color.cgColor
    }
    func setBorderWidth(value: CGFloat) {
        layer.borderWidth = value
    }
}
