//
//  BottomLineTextField.swift
//  HealthApp
//
//  Created by Apple on 22/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class BottomLineTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func awakeFromNib() {
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: self.frame.height - 1, width: self.frame.width, height: 0.7)
        bottomLine.backgroundColor = UIColor.gray.cgColor
        self.borderStyle = UITextField.BorderStyle.none
        self.layer.addSublayer(bottomLine)
    }
    
    
}

extension UITextField {
    
    func setPlaceholder(str1: String, str2: String){
        
        let placeholder = self.placeholder?.setAttributes(str1: str1, str2: str2, color1: .red, color2: .black)
        placeholder?.addAttribute(NSAttributedString.Key.font, value: UIFont(name: SFProRegular, size: 16)!, range: NSRange(location: 0, length: self.placeholder!.count))
        self.attributedPlaceholder = placeholder
        
    }
}
