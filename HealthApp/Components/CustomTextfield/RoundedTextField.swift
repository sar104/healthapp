//
//  RoundedTextField.swift
//  HealthApp
//
//  Created by Apple on 23/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class RoundedTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func awakeFromNib() {
     
        self.layer.cornerRadius = 15
        self.layer.borderColor = RGB_185.cgColor
        self.layer.borderWidth = 1.0
    }
    
    override func layoutSubviews() {
        
        self.layer.cornerRadius = 15
        self.layer.borderColor = RGB_185.cgColor
        self.layer.borderWidth = 1.0
    }
}
