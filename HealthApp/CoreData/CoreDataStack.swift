//
//  CoreDataStack.swift
//  RainbowWords
//
//  Created by Apple on 6/8/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack {
    
    private let modelName: String
    
    init(model: String) {
        self.modelName = model
    }
    
    lazy var mainContext: NSManagedObjectContext = {
        return self.storeContainer.viewContext
    }()
    
    lazy var storeContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: self.modelName)
        container.loadPersistentStores(completionHandler: { (storeDesc, error) in
            if let error = error as NSError? {
                fatalError("fatal error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
}

extension CoreDataStack {
    
    func saveContext() {
        
        mainContext.perform {
            
            guard self.mainContext.hasChanges else { print("no changes"); return }
            
            do {
                try self.mainContext.save()
                print("saveContext called")
            } catch let nserror as NSError {
                fatalError("error: \(nserror)")
            }
        }
        
    }
}
