//
//  CoreDataManager.swift
//  HealthApp
//
//  Created by Apple on 25/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import CoreData
import Sync

class CoreDataManager {
    
    static let shared = CoreDataManager()
    
    private let dataStack: DataStack
    
    private init() {
        
        self.dataStack = DataStack(modelName: "HealthApp")
    }
    
    func syncUser(json: [[String:Any]],completion: @escaping (_ result: VoidResult) -> ()){
        
        self.dataStack.sync(json, inEntityNamed: User.entity().name!) { error in
            completion(.success)
        }
    }
    func syncAppUser(json: [[String:Any]],completion: @escaping (_ result: VoidResult) -> ()){
        
        self.dataStack.sync(json, inEntityNamed: AppUser.entity().name!) { error in
            completion(.success)
        }
    }
    func fetchLocalUsers() -> [User] {
        let request: NSFetchRequest<User> = User.fetchRequest()

        return try! self.dataStack.viewContext.fetch(request)
    }
    
    func removeUser(id: Int) {
        
        do {
            try self.dataStack.delete(id, inEntityNamed: "User")
        } catch let err {
            print(err)
        }
    }
    
    func saveNotifications(data: [AnyHashable: Any]) {
       /*
        let data =
        [AnyHashable("channel"): "family_request", AnyHashable("mobile_no"): 7385043852, AnyHashable("google.c.sender.id"): 606581097387, AnyHashable("gcm.notification.channel"): "family_request", AnyHashable("username"): "Dr.  Pritamsing", AnyHashable("google.c.a.e"): 1, AnyHashable("title"): "You have been added as Family member by Dr.  Pritamsing. Please ACCEPT to allow health records sharing, or REJECT to cancel..Team Ctrlh", AnyHashable("userid"): 404, AnyHashable("gcm.notification.username"): "Dr.  Pritamsing", AnyHashable("aps"): [
            "alert" :     [
                "title" : "You have been added as Family member by Dr.  Pritamsing. Please ACCEPT to allow health records sharing, or REJECT to cancel..Team Ctrlh"
            ]
            ], AnyHashable("gcm.message_id"): 1601292828274978, AnyHashable("gcm.notification.mobile_no"): 7385043852, AnyHashable("gcm.notification.userid"): 404] as [AnyHashable : Any]
        */
        
        let info = ["userid": data["userid"] ?? "","mobile_no": data["mobile_no"] ?? "", "username": data["username"] ?? "", "channel": data["channel"] ?? "", "title": data["title"] ?? "", "shop_name": data["shop_name"] ?? "", "id": "", "remark": "", "sender_id": "","currdate":Date().toString(format: df_dd_MM_yyyy), "currtime": Date().toString(format: "HH:mm")]
        
        CoreDataManager.shared.syncNotification(json: [info]) { (res) in
            print(res)
            let arr = CoreDataManager.shared.fetchNotifications()
            print("arr: \(arr)")
        }
    }
    
    func syncNotification(json: [[String:Any]],completion: @escaping (_ result: VoidResult) -> ()){
        
        self.dataStack.sync(json, inEntityNamed: RemoteNotification.entity().name!) { error in
            completion(.success)
        }
    }
    func fetchNotifications() -> [RemoteNotification] {
        let request: NSFetchRequest<RemoteNotification> = RemoteNotification.fetchRequest()

        return try! self.dataStack.viewContext.fetch(request)
    }
    
    func syncMedicines(json: [[String:Any]],completion: @escaping (_ result: VoidResult) -> ()){
        
        self.dataStack.sync(json, inEntityNamed: Medicines.entity().name!) { error in
            completion(.success)
        }
    }
    func fetchAllMedicines() -> [Medicines] {
        let request: NSFetchRequest<Medicines> = Medicines.fetchRequest()

        return try! self.dataStack.viewContext.fetch(request)
    }
    func fetchMedicines(predicate: NSPredicate) -> [Medicines] {
        let request: NSFetchRequest<Medicines> = Medicines.fetchRequest()
        request.predicate = predicate
        
        return try! self.dataStack.viewContext.fetch(request)
    }
    
    func saveMedicineData(data: [[String]]) {
        
        let context = self.dataStack.viewContext
        for row in data {
            let medicine = Medicines(context: context)
            //print(row[0])
            if row.count == 3 {
                medicine.medicine_name = row[0].components(separatedBy: "\n").joined()
                medicine.medicine_package = row[1]
                medicine.medicine_type = row[2]
                
                if context.hasChanges {
                    context.perform {
                        do {
                            try context.save()
                            //self.coreDataStack.saveContext()
                            print("context saved")
                        } catch let err as NSError {
                            fatalError("error saving \(err)")
                        }
                    }
                    
                }
            }
        }
    }
    
    func getMedCount()-> Int {
    
        let arr = self.fetchAllMedicines()
        return arr.count
    }
    
    
}
enum VoidResult {
    case success
    case failure(NSError)
}
