//
//  CoreDataService.swift
//  RainbowWords
//
//  Created by Apple on 6/14/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import CoreData

class CoreDataService {
    
    private let coreDataStack: CoreDataStack
    var context: NSManagedObjectContext!
    
    init(stack: CoreDataStack) {
        self.coreDataStack = stack
        //self.context = self.coreDataStack.mainContext
        self.context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        self.context.parent = coreDataStack.mainContext
    }
    
    func saveContext(){
        
        self.context.perform {
            
            guard self.context.hasChanges else { print("no changes"); return }
            
            do {
                try self.context.save()
                print("saveContext called")
                self.coreDataStack.saveContext()
                
            } catch let nserror as NSError {
                fatalError("error: \(nserror)")
              
            }
        }
        
       
    }
    
    func saveMedicineData(data: [[String]], handler: @escaping (Bool) -> Void) {
        
        
        for row in data {
            
            
            let medicine = Medicines(context: context) //NSEntityDescription.insertNewObject(forEntityName: "Medicines", into: self.context) as! Medicines
                //
            //print(row[0])
            if row.count == 3 {
                medicine.medicine_name = row[0]
                medicine.medicine_package = row[1]
                medicine.medicine_type = row[2]
                
                self.saveContext()
                /*
                if context.hasChanges {
                    context.perform {
                        do {
                            try self.context.save()
                            //self.coreDataStack.saveContext()
                            print("context saved")
                        } catch let err as NSError {
                            fatalError("error saving \(err)")
                        }
                    }
                }*/
            }
        }
        handler(true)
    }
    
    func fetchAllMedicines() -> [Medicines] {
        let fetchRequest = NSFetchRequest<Medicines>(entityName: "Medicines")
        var data: [Medicines] = []
        
        do {
            data = try context.fetch(fetchRequest)
            
        } catch let error as NSError? {
            print("\(error)")
        }
        return data
    }
    func fetchMedicines(predicate: NSPredicate) -> [Medicines] {
        
        let fetchRequest = NSFetchRequest<Medicines>(entityName: "Medicines")
        fetchRequest.predicate = predicate
        var data: [Medicines] = []
        
        do {
            data = try context.fetch(fetchRequest)
            
        } catch let error as NSError? {
            print("\(error)")
        }
        return data
    }
    
    func getMedCount()-> Int {
    
        let arr = self.fetchAllMedicines()
        return arr.count
    }
    
    
    func fetchLocalUsers() -> [User] {
        let fetchRequest = NSFetchRequest<User>(entityName: "User")
        var data: [User] = []
        
        do {
            data = try context.fetch(fetchRequest)
            
        } catch let error as NSError? {
            print("\(error)")
        }
        return data
    }
    
    func saveUser(userData: UserModel) {
        let user = User(context: context)
        
        userData.populateCoreData(data: user)
        
        self.saveContext()
    }
}
